
def HistogramPlot(        in: CSV,
                          plotAnnotator: RSource,
                          values: String = "*",
                          xLabel: String = "",
                          yLabel: String = "Frequency",
                          title: String = "",
                          bins: String = "10",
                          imageType: String = "single",
                          yLow: Double  = -0.000001,
                          yHigh: Double = -0.000001,
                          xLow: Double  = -0.000001,
                          xHigh: Double = -0.000001,
                          legendPosition: String = "off",
                          plotPar: String = "",
                          plotType: String = "hist",
                          pngImage: Boolean = true,
                          commonScale: Boolean = true,
                          width: Double = 14D,
                          height: Double = 11D,
                          dpCm: Double = 60D,
                          sectionTitle: String = "Two dimensional plot",
                          sectionType: String = "subsection",
                          caption: String = "Histogram (%s)",
                          yTransformation: String = "",
                          xTransformation: String = "",
                          drawX: Boolean = true,
                          digits: Int = 2,
                          frequencyIsRatio: Boolean = false ): (CSV,CSV,Latex) = 
{

  val cScale = 
    if (imageType=="single" && !commonScale) {
      println("HistogramPlot: [NOTE] Plotting single figure, " + 
              "changing commonScale to TRUE")
      true
    }
    else false
  val histdata = HistogramPlot_FrequencyCalculator(in,
                        values=values,
                        bins=bins,
                        digits=digits,
                        frequencyIsRatio=frequencyIsRatio,
                        commonScale=cScale)
  
  val xvalues = plotType match {
    case "bar"  => histdata.midPointsFormatted
    case "hist" => histdata.breakPoints
    case _      => histdata.midPoints
  }

  val plot2D = Plot2D(  x               = xvalues,
                      y               = histdata.out,
                      plotAnnotator   = plotAnnotator,
                      yColumns        = "*", 
                      xColumns        = "*",
                      plotType        = plotType,
                      commonScale     = commonScale,
                      imageType       = imageType,
                      yLabel          = yLabel,
                      xLabel          = xLabel,
                      title           = title,
                      pngImage        = pngImage,
                      yHigh           = yHigh,
                      yLow            = yLow,
                      xHigh           = xHigh,
                      xLow            = xLow,
                      drawX           = drawX,
                      legendPosition  = legendPosition,
                      width           = width,
                      height          = height,
                      dpCm            = dpCm,
                      sectionTitle    = sectionTitle,
                      sectionType     = sectionType,
                      caption         = caption,
                      plotPar         = plotPar,
                      xTransformation = xTransformation,
                      yTransformation = yTransformation)

  (histdata.out,
   histdata.midPoints,
   plot2D.out)

}
