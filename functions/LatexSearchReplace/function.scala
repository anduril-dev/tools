
def LatexSearchReplace( in: Latex,
                        rules: Properties = null,
                        filePattern: String = "(.*)\\.(txt|tex)",
                        key00: String = "",
                        key01: String = "",
                        key02: String = "",
                        key03: String = "",
                        key04: String = "",
                        key05: String = "",
                        key06: String = "",
                        key07: String = "",
                        key08: String = "",
                        key09: String = "",
                        value00: String = "",
                        value01: String = "",
                        value02: String = "",
                        value03: String = "",
                        value04: String = "",
                        value05: String = "",
                        value06: String = "",
                        value07: String = "",
                        value08: String = "",
                        value09: String = ""): Latex = {
                            
    import org.anduril.runtime._
    val toBeProcessed=Folder2Array(in,
        filePattern=filePattern,
        keyMode="filename"
        )
    val notProcessed=Folder2Array(in,
        filePattern=".*",
        excludePattern=filePattern,//"(\\..*|.*\\.tex|.*\\.txt)",
        keyMode="filename"
    )
    val initialArray=Folder2Array(in,
        keyMode="filename"
    )
    val template=Array2CSV(initialArray,relative=in)
    val processed=NamedMap[BinaryFile]("processed")
    for ( (key,f) <- iterArray(toBeProcessed.out) ) {
        processed(key)=SearchReplace(in=toBeProcessed.out(key),
                        rules=rules,
                        key00=key00,
                        key01=key01,
                        key02=key02,
                        key03=key03,
                        key04=key04,
                        key05=key05,
                        key06=key06,
                        key07=key07,
                        key08=key08,
                        key09=key09,
                        value00=value00,
                        value01=value01,
                        value02=value02,
                        value03=value03,
                        value04=value04,
                        value05=value05,
                        value06=value06,
                        value07=value07,
                        value08=value08,
                        value09=value09
                    ).out
    }
    val processedArray=makeArray(processed)
    val outArray=ArrayCombiner(
        processedArray,
        notProcessed.out)
    val out=Array2Folder(in=outArray,template=template).out.asInstanceOf[Latex]
    out
}
