
def QueryReport(    query: SQL,
                            table1: CSV,
                            table2: CSV = null,
                            table3: CSV = null,
                            table4: CSV = null,
                            table5: CSV = null,
                            table6: CSV = null,
                            table7: CSV = null,
                            table8: CSV = null,
                            refs: CSV = null,
                            section: String = "Query results",
                            tableCaption: String = "Results for the query",
                            colFormat: String = "center",
                            showQuery: Boolean = true,
                            showCols: String = "",
                            listCols: String = "",
                            instanceName: String): (Latex, CSV) =
{
    val eqData = TableQuery(queryFile  = query,
                          table1 = table1,
                          table2 = table2,
                          table3 = table3,
                          table4 = table4,
                          table5 = table5,
                          table6 = table6,
                          table7 = table7,
                          table8 = table8)

    val eqTable = CSV2Latex(tabledata = eqData,
                           refs      = refs,
                           caption   = tableCaption,
                           colFormat = colFormat,
                           section   = section,
                           columns   = showCols,
                           listCols  = listCols)

    val (report, data) = if (showQuery) {        
        val eqSQLDoc = SourceCode2Latex(  query,
                                          language = "SQL",                                          
                                          caption  = "SQL query that was used to generate Table~\\ref{table:"+
                                                      instanceName+"-eqTable}.")
        val eqReport = LatexCombiner( eqTable, 
                                      eqSQLDoc)

        ( eqReport.document, eqData.table ) 
    } else {
        ( eqTable.report, eqData.table )
    } 
    
  (report, data)   
  
}
