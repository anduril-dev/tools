#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=javatools
cd "$( dirname $0)/../../"
[[ -e "lib/java/csbl-javatools.jar" ]] && {
    echo "${NAME} already installed"
    exit 0
}
ant tools.jar
