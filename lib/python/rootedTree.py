import json
import networkx as nx
import math
from math import log
import random

from anduril.constants import is_nan_object,DEFAULT_NAN_OBJECT,NAN_STRING

def substract_counts(x,y):
    """ x-y """
    result={}
    items=set(x.keys()).union(set(y.keys()))
    for item in items:
        result[item]=x.get(item,0)-y.get(item,0)
        assert result[item]>=0, (str(x),str(y))
    return result

def sum_counts(x,y):
    """ x-y """
    result={}
    items=set(x.keys()).union(set(y.keys()))
    for item in items:
        result[item]=x.get(item,0)+y.get(item,0)
        assert result[item]>=0, (str(x),str(y))
    return result



def counts_to_freqs(counts,total_size):
    freqs={}
    for item,count in counts.iteritems():
        freqs[item]=count/float(total_size)
    return freqs

def get_entropy(px):
    h=0.0
    for x in px.keys():
        if px[x]>0:
            h-=px[x]*log(px[x])
    return h

def get_mi(px,py,pxy):
    mi=0.0
    for x in px.keys():
        for y in py.keys():
            if px[x]>0 and py[y]>0 and x in pxy and y in pxy[x] and pxy[x][y]>0:
                mi+=pxy[x][y]*log(pxy[x][y]/float(px[x]*py[y]) )
    return mi

def get_enriched(px,py,pxy,xvar):
    e=[]
    for y in py.keys():
        if px[xvar]>0 and py[y]>0:
            if y in pxy[xvar] and pxy[xvar][y]>px[xvar]*py[y]:
                mi=0 if pxy[xvar][y]==0.0 else pxy[xvar][y]*log(pxy[xvar][y]/float(px[xvar]*py[y]) )
                e.append((mi,y))
    e.sort(key=lambda x:-x[0])
    return map(lambda x:x[1],e)

rounding_tolerance=10**-6


class RootedTree(object):
    def __init__(self):
        self.children={}
        self.father={}
        self.metadata={}
        self.root=None
    
    @classmethod
    def init_graphML(self,inputfile):
        tree=RootedTree()
        net=nx.read_graphml(inputfile)
        for node in net.nodes_iter():
            tree.add_node(node)
            tree.add_join([node]+map(lambda x:x[1],net.out_edges(node)))
            for key,val in net.node[node].items():
                tree.add_metadata(node,key,val)
        return tree

    @classmethod
    def init_balancedTree(self,levels,degree):
        tree=RootedTree()
        tree.add_node("l0n0")
        for level in range(1,levels):
            for node in list(tree.iter_leaves()):
                tree.add_join([node]+map(lambda x:"l"+str(level)+"n"+str(int(node.split("n")[-1])*degree+x),range(degree)))
        return tree

    def __contains__(self,item):
        return self.metadata.__contains__(item)

    def get_as_net(self):
        net=nx.DiGraph()
        for node in self.iter_preorder():
            strippedDict={}
            for key,val in self.metadata[node].iteritems():
                if isinstance(val,str) or isinstance(val,unicode) or isinstance(val,int) or isinstance(val,float):
                    if is_nan_object(val):
                        strippedDict[key]=NAN_STRING
                    else:
                        strippedDict[key]=str(val)
            net.add_node(node,attr_dict=strippedDict)
            for child in self.children[node]:
                net.add_edge(node,child)
        return net

    def write_graphML(self,filename):
        net=self.get_as_net()
        nx.write_graphml(net,filename)

    def add_metadata(self,node,label,data):
        assert node in self.metadata
        self.metadata[node][label]=data

    def add_node(self,node):
        if node not in self.metadata:
            self.children[node]=set()
            self.father[node]=None
            self.metadata[node]={}
    
    def add_join(self,join):
        """ Adds a join in format (father, child1, child2, ...)
        """
        for node in join:
            self.add_node(node)
        self.children[join[0]].update(join[1:])
        for child in join[1:]:
            assert self.father[child]==None, "Child %s already has a parent: %s"%(child,self.father[child])
            self.father[child]=join[0]

    def remove_leaf(self,leaf):
        assert self.is_leaf(leaf)
        father=self.father[leaf]
        if father != None: #its not a root
            self.children[father].remove(leaf)
        del self.metadata[leaf]
        del self.children[leaf]
        del self.father[leaf]


    def is_leaf(self,node):
        return len(self.children[node])==0

    def is_connected(self):
        size=len(self.children)
        count=0
        for node in self.iter_preorder():
            count+=1
        return size==count

    def find_root(self,node=None):
        if node==None:
            node=self.metadata.iterkeys().next()
        father=self.father[node]
        if father==None:
            self.root=node
            return node
        else:
            return self.find_root(father)

    def iter_nodes(self,node=None):
        return self.iter_preorder(node=node)

    def iter_internals(self,root=None):
        """Iterate over interal nodes."""
        for node in self.iter_preorder(root):
            if not self.is_leaf(node):
                yield node

    def iter_leaves(self,root=None):
        """Iterate over leaf nodes."""
        for node in self.iter_preorder(root):
            if self.is_leaf(node):
                yield node

    def get_root(self):
        if self.root==None:
            self.find_root()
        return self.root

    def get_json_tree(self,tree=None):
        def expand_node(node):
            subtree={}
            subtree["name"]=node
            if len(self.children[node])>0:
                subtree["children"]=self.children[node]
            for key,val in self.metadata[node].items():
                subtree[key]=val
            return subtree
                    
        if tree==None:
            tree=expand_node(self.root)
        
        if "children" in tree:
            children=tree["children"]
            tree["children"]=[]
            for child in children:
                subtree=expand_node(child)
                tree["children"].append(subtree)
                self.get_json_tree(subtree)
            
        if tree["name"]==self.root:
            return tree

    def get_json(self):
        self.find_root()
        return json.dumps(self.get_json_tree())


    def calculate_size_metadata(self,node=None):
        if node==None:
            node=self.get_root()
        size=1
        leaves=1 if self.is_leaf(node) else 0
        for child in self.children[node]:
            size_s,leaves_s=self.calculate_size_metadata(child)
            size+=size_s
            leaves+=leaves_s
        self.metadata[node]["subtree_size"]=size
        self.metadata[node]["subtree_leaves"]=leaves
        return size,leaves

    def iter_preorder(self,node=None):
        if node==None:
            node=self.get_root()
        yield node
        for child in self.children[node]:
                for grandchild in self.iter_preorder(child):
                    yield grandchild
                    
    def iter_reversed_preorder(self,node=None):
        """Almost same as postorder
        """
        for rnode in reversed(list(self.iter_preorder(node))):
            yield rnode

    def count_metadata(self,data_label,countMissingData=True):
        """Counts the number of occurances of metadata values of leaves
        in subtrees of inner nodes. For each inner node a dictionary is
        constructed where keys are the leaf metadata values and values 
        are the counts in the subtrees.

        Missing data is represented by None.
        """
        def update_counts(counts,new_counts):
            for item,new_count in new_counts.iteritems():
                counts[item]=counts.get(item,0)+new_count

        for node in self.iter_reversed_preorder():
            if not data_label in self.metadata[node]: #it should be a leaf
                self.metadata[node][data_label]={}
                for child in self.children[node]:
                    child_data=self.metadata[child][data_label]
                    if isinstance(child_data,dict):
                        update_counts(self.metadata[node][data_label],child_data)
                    elif countMissingData or (child_data!=None and not is_nan_object(child_data)):
                        update_counts(self.metadata[node][data_label],{child_data:1})

    def shuffle_metadata(self,data_label,new_data_label=None):
        """Shuffle the metadata in leaves. If new data label is not given
        the shuffling is done in-place."""
        if new_data_label==None:
            new_data_label=data_label
        metadata_vals=[]
        for node in self.iter_leaves():
            metadata_vals.append(self.metadata[node][data_label])
        random.shuffle(metadata_vals)
        for i,node in enumerate(self.iter_leaves()):
            self.metadata[node][new_data_label]=metadata_vals[i]

    def remove_metadata(self,data_label,allow_missing=False):
        """Removes metadata from all nodes."""
        for node in self.iter_nodes():
            if allow_missing:
                self.metadata[node].pop(data_label,None)
            else:
                del self.metadata[node][data_label]

    def find_split(self,data_label,root=None,useMissingData=True,localMI=False):
        """Finds the split site for the tree yielding the largest mutual 
        information with the given data.

        The mutual information is calculated between the two random variables
        defined below. A leaf of the tree is selected uniformly random from
        the tree. Then a random variable X is defined as having two values
        depending on which component the randomly picked node is, and another
        random variable Y is defined as the data value of the leaf node.
        
        The mutual information is normalized with the entropy of the 
        data, i.e. the entropy of X. This normalization is equivalent 
        to unnormalized MI when splitting the tree, but gives values between 
        0 and 1, where 1 means that the split explains fully the entropy of 
        the data. Note that if the data has more than 2 possible values, the MI
        can never reach the entropy of the data.

        Args:
            data_label (str):  the metadata field which is used to calculate
            the mutual information.
            root: the root of the tree. If None, the global root is used, if
            something else, only the subtree spanned by the root is considered.
            useMissingData: if a node with None value can be selected.
            localMI: If true, the random node can only be selected from the subtree
            spanned by the father of the split node.


        Returns:        
            A tuple: (node,mi,st_enriched,other_enriched)
            node: the node label for the split site. The optimal split
            is done between the node and its father in the tree.
            mi: the normalized mutual information value of the split.
            st_enriched: the data values which are enriched in the
            subtree of the node.
            other_enriched: the data values which are enriched in the
            tree that remains when the subtree defined by the node is
            removed.
        """
        self.count_metadata(data_label,countMissingData=useMissingData)
        self.calculate_size_metadata()

        if root==None:
            root=self.get_root()

        root_data=self.metadata[root][data_label]
        root_size=sum(self.metadata[root][data_label].values())#["subtree_leaves"]

        py_root=counts_to_freqs(root_data,root_size)
        if sum(py_root.values())==0: #there is no data available!
            return 0.0,None,None,None
        assert abs(sum(py_root.values())-1.0)<rounding_tolerance,str(sum(py_root.values()))

        maxmi_value=0
        maxmi_node=None
        maxmi_subtree_enriched=None
        maxmi_other_enriched=None
        for node in self.iter_internals(root=root):
            subtree_data=self.metadata[node][data_label]
            if localMI and self.father[node]!=None:
                father_subtree_data=self.metadata[self.father[node]][data_label]
                other_data=substract_counts(father_subtree_data,subtree_data)
                all_size=sum(father_subtree_data.values()) #precalc this?
                py=counts_to_freqs(father_subtree_data,all_size)
            else:
                other_data=substract_counts(root_data,subtree_data)
                all_size=root_size
                py=py_root

            if all_size>0:
                px={"subtree":sum(self.metadata[node][data_label].values())/float(all_size),"other":1.-sum(self.metadata[node][data_label].values())/float(all_size)}    
                assert abs(sum(px.values())-1.0)<rounding_tolerance,str(sum(px.values()))

                pxy={}
                pxy["subtree"]=counts_to_freqs(subtree_data,all_size)
                pxy["other"]=counts_to_freqs(other_data,all_size)

                assert abs(sum(map(lambda x:sum(x.values()),pxy.values()))-1.0)<rounding_tolerance, str(sum(map(lambda x:sum(x.values()),pxy.values())))

                if get_mi(px,py,pxy)>rounding_tolerance:
                    mi=get_mi(px,py,pxy)/float(get_entropy(py))
                else:
                    mi=0.0

                if mi>maxmi_value:
                    maxmi_value=mi
                    maxmi_node=node
                    maxmi_subtree_enriched=get_enriched(px,py,pxy,"subtree")
                    maxmi_other_enriched=get_enriched(px,py,pxy,"other")

        return maxmi_value,maxmi_node,maxmi_subtree_enriched,maxmi_other_enriched

    def split(self,node):
        def copy_node(node,fr_tree,to_tree):
            to_tree.add_node(node)
            to_tree.add_join([node]+list(fr_tree.children[node]))
            for key,val in fr_tree.metadata[node].iteritems():
                if not key in ["subtree_leaves","subtree_size"]:
                    to_tree.add_metadata(node,key,val)
        subtree=self.__class__()
        otree=self.__class__()
        for subnode in self.iter_nodes(node=node):
            copy_node(subnode,self,subtree)
        for onode in self.iter_nodes():
            if not onode in subtree:
                copy_node(onode,self,otree)

        if node!=self.get_root():
            otree.remove_leaf(node)

        return subtree,otree
        

    def find_splits_iterative(self,data_label,mi_threshold,p_threshold=None,rounds=None,root=None,localMI=False):

        def clear_data(tree,allowedData):
            allowedData=set(allowedData)
            for node in tree.iter_nodes():
                if isinstance(tree.metadata[node][data_label],dict):
                    if tree.is_leaf(node):
                        tree.metadata[node][data_label]=DEFAULT_NAN_OBJECT
                    else:
                        del tree.metadata[node][data_label]
                elif tree.metadata[node][data_label] not in allowedData:
                    tree.metadata[node][data_label]=DEFAULT_NAN_OBJECT

        #this is implemented by copying the tree every time it splits,
        #which can take a lot of memory. This could be improved.
        splits=[]
        mi,node,ste,oe=self.find_split(data_label,root=root,useMissingData=False,localMI=localMI)
        if node==None:
            return splits        
        pval_str=DEFAULT_NAN_OBJECT
        if p_threshold != None:
            if rounds==None:
                rounds=int(math.ceil(10.0/p_threshold))
            pval_str=self.find_split_get_mi_p_val_estimate(data_label,rounds,mi,useMissingData=False,localMI=localMI)
            pval=0.0 if "<" in pval_str else float(pval_str)
        if mi > mi_threshold and (p_threshold==None or pval<p_threshold):
            if localMI and self.father[node]!=None:
                subtree,otree=self.split(self.father[node])
                subsubtree,subotree=subtree.split(node)
                clear_data(subsubtree,ste)
                clear_data(subotree,oe)
                clear_data(otree,oe)
                subsubsplits=subsubtree.find_splits_iterative(data_label,mi_threshold,p_threshold,rounds,localMI=localMI)
                subothersplits=subotree.find_splits_iterative(data_label,mi_threshold,p_threshold,rounds,localMI=localMI)
                othersplits=otree.find_splits_iterative(data_label,mi_threshold,p_threshold,rounds,localMI=localMI)

                splits.append( (mi,node,ste,oe,pval_str,data_label) )
                splits.extend(subsubsplits)
                splits.extend(subothersplits)
                splits.extend(othersplits)
            else:
                subtree,otree=self.split(node)
                clear_data(subtree,ste)
                clear_data(otree,oe)
                subsplits=subtree.find_splits_iterative(data_label,mi_threshold,p_threshold,rounds,localMI=localMI)
                othersplits=otree.find_splits_iterative(data_label,mi_threshold,p_threshold,rounds,localMI=localMI)
                splits.append( (mi,node,ste,oe,pval_str,data_label) )
                splits.extend(subsplits)
                splits.extend(othersplits)
        return splits

    def find_split_get_random_mi(self,data_label,rounds,useMissingData=True,localMI=False):
        rand_mi=[]
        for i in range(rounds):
            self.shuffle_metadata(data_label,"random_data")
            maxmi_value_r,maxmi_node_r,maxmi_subtree_enriched_r,maxmi_other_enriched_r=self.find_split("random_data",useMissingData=useMissingData,localMI=localMI)
            rand_mi.append(maxmi_value_r)
            self.remove_metadata("random_data")
        return rand_mi

    def find_split_get_mi_p_val_estimate(self,data_label,rounds,mi,useMissingData=True,localMI=False):
        if rounds==0:
            return None
        rand_mi=self.find_split_get_random_mi(data_label,rounds,useMissingData=useMissingData,localMI=localMI)
        count=0
        for rmi in rand_mi:
            if rmi>mi:
                count+=1
        if count==0:
            return "<"+str(1./rounds)
        else:
            return str(count/float(rounds))



    def average_metadata(self,data_label):
        if not "subtree_size" in self.metadata[self.get_root()]:
            self.calculate_size_metadata()
        for node in self.iter_reversed_preorder():
            if not data_label in self.metadata[node]:
                #rounding errors will propagate
                child_data=map(lambda x:self.metadata[x][data_label],self.children[node])
                child_size=map(lambda x:self.metadata[x]["subtree_size"],self.children[node])
                self.metadata[node][data_label]=sum(map(lambda x,y:x*y,child_data,child_size))/float(sum(child_size))
	#---> tree y-coords
	
    def calculate_ycoords(self,coord_data_type,data_label):
        """ Calculate the y-coordinates (heights) for the nodes in the 
        tree based on height/distance data given for the branching 
        points.

        |---E---|   
        |-D-|     C
        |   |     
        A   B
        
        Distances:      Heights:    Y-coords:
        AB 2            AB 2        E     0
        DC 1            DC 3        D,C    1     
                                    A,B    3
        """
        if coord_data_type=="height":
            self.heights2ycoords(data_label)
        elif coord_data_type=="distance":
            self.distances2ycoords(data_label)
        else:
            raise Exception("Invalid coord_data_type: %s"%coord_data_type)

    def heights2ycoords(self,data_label):
        """Calculate y coordinates using metadata field on height"""
        total_height=self.metadata[self.get_root()][data_label]
        for node in self.metadata:
            self.metadata[node]["ycoord"]=total_height-self.metadata[node].get(data_label,0)

    def distances2ycoords(self,data_label):
        """Calculate y coordinates using metadata field on branch distance"""
        self.metadata[self.get_root()]["ycoord"]=0
        for node in self.iter_preorder():
            #print node,self.children[node]
            if data_label in self.metadata[node]:
                if self.father[node]!=None:
                    #print node,self.father[node]
                    child_y=self.metadata[node][data_label]+self.metadata[self.father[node]]["ycoord"]
                else:
                    child_y=self.metadata[node][data_label]
                for child in self.children[node]:
                    self.metadata[child]["ycoord"]=child_y
            else:
                assert self.is_leaf(node)
        
        #Calculate the total tree height
        #total_height=self._get_tree_height(self.get_root())
                
    #<--- tree y-coords


if __name__=="__main__":
    rt=RootedTree()
    rt.add_join((1,2,3))
    rt.add_join((2,4,5))
    print rt.get_json()
    assert rt.get_json()=='{"name": 1, "children": [{"name": 2, "children": [{"name": 4}, {"name": 5}]}, {"name": 3}]}'
    print list(rt.iter_preorder())

