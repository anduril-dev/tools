private abstract class ArgumentFormatter {

    /**
     * Return the input port name for given 1-based port index.
     */
    protected def formatPortName(portIndex: Int): String

    /**
     * Get the parameter name for given 1-based parameter index.
     * Return null if the XEvaluate component does not have have
     * string parameters.
     */
    protected def formatParameterName(parameterIndex: Int): String

    /**
     * Return the value that replaces a port reference in the script.
     */
    protected def transformPortArgument(portIndex: Int): String

    /**
      * Return the value that replaces a non-port value (string or integer)
      * in the script.
      */
    protected def transformValueArgument(valueIndex: Int, value: Any): String

    /**
     * The parameter name of XEvaluate that holds the script code.
     */
    protected def scriptParameterName: String

    /**
     * The input port name of XEvaluate that holds the script code. This is
     * only used if scriptParameterName is null.
     */
    protected def scriptPortName: String = null

    protected def stripMargin: Boolean = false

    @org.anduril.runtime.IgnoreName
    final def processArgs(sc: StringContext, args: Any*): Map[String, Any] = {
        val argMap = scala.collection.mutable.Map[String, Any]()
        var portIndex = 0
        var valueIndex = 0

        val formattedArgs = args map (arg => {
            arg match {
                case component: Product1[Port] =>
                    portIndex += 1
                    val portName = formatPortName(portIndex)
                    if (portName != null) {
                        argMap(portName) = component._1
                    }
                    transformPortArgument(portIndex)
                case port: Port =>
                    portIndex += 1
                    val portName = formatPortName(portIndex)
                    if (portName != null) {
                        argMap(portName) = port
                    }
                    transformPortArgument(portIndex)
                case any =>
                    valueIndex += 1
                    val parameterName = formatParameterName(valueIndex)
                    if (parameterName != null) {
                        argMap(parameterName) = any.toString
                    }
                    transformValueArgument(valueIndex, any)
            }
        })

        // Invoke regular string interpolator to obtain script
        var transformedScript = sc.s(formattedArgs: _*)
        if (this.stripMargin) {
            transformedScript = transformedScript.stripMargin
        }

        if (this.scriptPortName == null) {
            argMap(this.scriptParameterName) = transformedScript
        } else {
            val script = anduril.builtin.StringInput(content = transformedScript)
            argMap(this.scriptPortName) = script.out
        }

        argMap.toMap
    }
}

private object BashFormatter extends ArgumentFormatter {

    protected def formatPortName(portIndex: Int)        = "var%d".format(portIndex)
    protected def transformPortArgument(portIndex: Int) = "@var%d@".format(portIndex)

    protected def formatParameterName(parameterIndex: Int)            = "param%d".format(parameterIndex)
    protected def transformValueArgument(valueIndex: Int, value: Any) = "@param%d@".format(valueIndex)

    protected val scriptParameterName = "script"

}

private object PythonFormatter extends ArgumentFormatter {

    protected def formatPortName(portIndex: Int)        = "var%d".format(portIndex)
    protected def transformPortArgument(portIndex: Int) = "var%d".format(portIndex)

    protected def formatParameterName(parameterIndex: Int)            = null
    protected def transformValueArgument(valueIndex: Int, value: Any) = value.toString

    protected val scriptParameterName = "script"
    protected override val stripMargin = true

}

private object RFormatter extends ArgumentFormatter {

    protected def formatPortName(portIndex: Int)        = "var%d".format(portIndex)
    protected def transformPortArgument(portIndex: Int) = "var%d".format(portIndex)

    protected def formatParameterName(parameterIndex: Int)            = null
    protected def transformValueArgument(valueIndex: Int, value: Any) = value.toString

    protected val scriptParameterName: String = null
    protected override val scriptPortName = "script"

}

private object ScalaFormatter extends ArgumentFormatter {

    protected def formatPortName(portIndex: Int)        = "var%d".format(portIndex)
    protected def transformPortArgument(portIndex: Int) = "var%d".format(portIndex)

    protected def formatParameterName(parameterIndex: Int)            = null
    protected def transformValueArgument(valueIndex: Int, value: Any) = value.toString

    protected val scriptParameterName = "scriptStr"

}

private object SQLFormatter extends ArgumentFormatter {

    protected def formatPortName(portIndex: Int)        = "table%d".format(portIndex)
    protected def transformPortArgument(portIndex: Int) = "table%d".format(portIndex)

    protected def formatParameterName(parameterIndex: Int)            = null
    protected def transformValueArgument(valueIndex: Int, value: Any) = value.toString

    protected val scriptParameterName = "query"

}

implicit class StringInterpolator(val sc: StringContext) extends AnyVal {

    def bash(args: Any*): anduril.tools.BashEvaluate = {
        val componentArgMap = BashFormatter.processArgs(sc, args: _*)
        val bash = anduril.tools.BashEvaluate(componentArgMap)
        bash
    }

    def python(args: Any*): anduril.tools.PythonEvaluate = {
        val componentArgMap = PythonFormatter.processArgs(sc, args: _*)
        val python = anduril.tools.PythonEvaluate(componentArgMap)
        python
    }

    def R(args: Any*): anduril.tools.REvaluate = {
        // We name the result as 'script' instead of 'componentArgMap' because
        // we want proper name for the StringInput instance.
        val script = RFormatter.processArgs(sc, args: _*)
        val r = anduril.tools.REvaluate(script)
        r
    }

    def scala(args: Any*): anduril.tools.ScalaEvaluate = {
        val componentArgMap = ScalaFormatter.processArgs(sc, args: _*)
        val scala = anduril.tools.ScalaEvaluate(componentArgMap)
        scala
    }

    def sql(args: Any*): anduril.tools.TableQuery = {
        val componentArgMap = SQLFormatter.processArgs(sc, args: _*)
        val sql = anduril.tools.TableQuery(componentArgMap)
        sql
    }
}
