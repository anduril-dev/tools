import anduril.tools._
import anduril.builtin._
import org.anduril.runtime._

object CSVSplitNetwork {
// CSVSplit testcases

/** Define a CSV */
val input = INPUT(path="csv.csv")

/** Simple splitting to two parts, filling the first file fully */
val split_order_first = CSVSplit(in=input)

val rejoin_splitted = CSVListJoin( in1=split_order_first.out("1"),
                                   in2=split_order_first.out("2"),
                                   fileCol="")

/*  Now,  input should equal  rejoin_splitted */

/** Sparse filling */
val split_order_sparse = CSVSplit(in=input,
                                  order="sparse")
                             
/** Split file by unique values in a column */
val split_column = CSVSplit(in=input,
                            labelCol="Well")

/** Split file by unique regexp in a column */
val split_regexp = CSVSplit(in=input,
                             labelCol="File",
                             regexp=".*_(.*).tif")

/** Join two of the outputs of the splitting. Note, the keys follow 
 *  unique values in the labelCol */
val rejoin_regexp_splitted = CSVListJoin( in1=split_regexp.out("15ADF48D5BC479534681"),
                                          in2=split_regexp.out("E7359B7DCAADD496A950"),
                                          fileCol="")

OUTPUT(split_column)
}
