import anduril.tools._
import anduril.builtin._
import org.anduril.runtime._

object SimpleWebPageNetwork {
val CSVs=Record[CSV]("CSVs")
val SNIPPETs=Record[BinaryFile]("SNIPPETs")
val HTMLs=Record[HTMLFile]("HTMLs")
val PLOTs=Record[Latex]("PLOTs")
val ANNOTs=Record[TextFile]("ANNOTs")
for (i <- 1 to 10)  
{
    // Generate CSV data
    CSVs("csv"+i)=Randomizer(columns=2, rows=5).out
    // Plot it
    PLOTs("plot"+i)=Plot2D(x=CSVs("csv"+i),y=CSVs("csv"+i),
                           xColumns="Column1", yColumns="Column2", plotType="l").out

    // CSV to HTML snippet
    SNIPPETs("csv"+i)=Pandoc(in=makeArray(CSVs("csv"+i)), CSVFormat="%0.3f",
                       from="CSV", to="html", basename="csv"+i ).out("csv"+i)
    
    // CSV to full HTML page
    HTMLs("csv"+i)=HTMLTable(in=CSVs("csv"+i),
                    title="Table"+i).out

    ANNOTs(i.toString)=StringInput("<h3>This is file "+i+" annotation</h3> <a href=\"http://www.anduril.org\">Created with Anduril</a>").out

}
// Combine report files as folders
val SNIPPETFiles=Array2Folder(rec2Array(SNIPPETs), fileMode="@key@.htmlsnippet")
val HTMLFiles=Array2Folder(rec2Array(HTMLs), fileMode="@key@.html")
val PLOTFiles=FolderCombiner(in=rec2Array(PLOTs), exclude=".*tex")
val ANNOTFiles=Array2Folder(rec2Array(ANNOTs), fileMode="@key@.txt")

// Combine plots, HTML tables, and snippets on one web page
val WebPage=SimpleWebPage(tableArray=makeArray("Plot"->PLOTFiles,
                                  "Table"->HTMLFiles,
                                  "Content"->SNIPPETFiles,
                                  "Annotations"->ANNOTFiles),
                      tableArrayExpand="Content,Annotations",
                      thumbnails=true)

}
