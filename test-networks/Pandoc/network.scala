import anduril.tools._
import anduril.builtin._
import org.anduril.runtime._

object PandocNetwork {
// Generate CSV data
val csv_data=Randomizer(columns=5, rows=4)

// CSV to markdown conversion (good for e-mails)
val csv_to_markdown=Pandoc(in=makeArray(csv_data), CSVFormat="%0.3f",
                       from="CSV", to="markdown" ) // or to="md"
// CSV to HTML conversion, for web pages
val csv_to_HTML=Pandoc(in=makeArray(csv_data), 
                   from="CSV", to="HTML" )
// Add some static markdown text, and a caption for the future inserted table
val markdown_example=StringInput("""
# Markdown example

See [Markdown syntax](http://daringfireball.net/projects/markdown/syntax)
for further information.

![](http://anduril.org/favicon.ico)

* list of things

We claim that $$ E^2 = (pc)^2 + (m_0c^2)^2 $$, but you already knew it.

Table: Table of random numbers
""")

// Concatenate and convert markdown documents to single self contained HTML page
val markdown_to_standalone_HTML=Pandoc(in=makeArray(markdown_example, csv_to_markdown.out("document")), 
                   header=makeArray(StringInput("<style>table { border-collapse:collapse; } table, th, td { border: 1px solid black; }</style>"),
                                    StringInput("<style>body { margin: 2em; }</style>")),
                   after=makeArray(StringInput("<footer>footer part</footer>")),
                   switches="-s -T Example_table --self-contained --webtex",
                   from="markdown", to="html" )

// Use the document in another component

val file_usage=FolderCombiner(file1=markdown_to_standalone_HTML.out("document"),
                          fname1="standalone_page.html")


val multiple_HTML_outputs=Pandoc(in=makeArray("doc1"->csv_to_markdown.out("document"),
                                              "doc2"->markdown_example),
                        switches="-s -T Example_table --self-contained --webtex",
                        from="markdown",to="HTML",
                        concatenate=false)
                        
val multiple_CSV_HTML_outputs=Pandoc(in=makeArray("doc1"->csv_data,
                                                  "doc2"->csv_data),
                        from="CSV",to="HTML",
                        concatenate=false)

}
