package org.anduril.javatools.pathway;

/*
// This class is disabled for now.
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.sbml.libsbml.ASTNode;
import org.sbml.libsbml.Compartment;
import org.sbml.libsbml.KineticLaw;
import org.sbml.libsbml.Parameter;
import org.sbml.libsbml.Reaction;
import org.sbml.libsbml.SBMLDocument;
import org.sbml.libsbml.Species;
import org.sbml.libsbml.SpeciesReference;
import org.sbml.libsbml.libsbml;
import org.csml.parser.csml19.CSML19IO;
import org.csml.parser.csml19.base.CSML19ConnectorElement;
import org.csml.parser.csml19.base.CSML19EntityElement;
import org.csml.parser.csml19.base.CSML19EntityParameterElement;
import org.csml.parser.csml19.base.CSML19KineticElement;
import org.csml.parser.csml19.base.CSML19ProcessElement;
import org.csml.parser.csml19.base.CSML19StartModel;
import org.csml.parser.csml19.base.ICSML19InnerNetElementChoice;

public class CSML2SBML {
    private SBMLDocument sbml;
    private File csmlFile;
    private String compartment = "cell";
    private String parameterRE;
    
    private Map<String, String> entityMap;
    private Set<String> parameters;
    
    public CSML2SBML(File csmlFile, String parameterRE) {
        this.csmlFile = csmlFile;
        this.parameterRE = parameterRE;
        
        this.sbml = new SBMLDocument(2, 1);
        this.sbml.createModel("model");
        Compartment comp = new Compartment(this.compartment);
        comp.setVolume(1);
        this.sbml.getModel().addCompartment(comp);
        
        this.entityMap = new HashMap<String, String>();
        this.parameters = new HashSet<String>();
    }
    
    public SBMLDocument convert() {
        CSML19StartModel csml = CSML19IO.loadCSMLModel(
                this.csmlFile.getAbsolutePath());
        
        for (ICSML19InnerNetElementChoice el: csml.getNetElement().getInnerNetElement()) {
            if (el instanceof CSML19EntityElement) {
                handleEntity((CSML19EntityElement)el);
            }
        }
        
        for (ICSML19InnerNetElementChoice el: csml.getNetElement().getInnerNetElement()) {
            if (el instanceof CSML19ProcessElement) {
                handleProcess((CSML19ProcessElement)el);
            }
        }
        
        this.sbml.checkConsistency();
        if (this.sbml.getErrorLog().getNumErrors() > 0) {
            sbml.printErrors();
        }
        return sbml;
    }
    
    private void handleEntity(CSML19EntityElement entity) {
        if (!entity.getType().equals(CSML19EntityElement.TYPE_CONTINUOUS)) {
            System.out.println("Warning: unsupported type for entity "+entity.getName());
            return;
        }

        CSML19EntityParameterElement parameter = entity.getEntityParameterElement();
        this.entityMap.put(entity.getLabel(), parameter.getLabel());
        
        String initStr = parameter.getInitialValue();
        Double initDouble = null;
        if (initStr != null && initStr.length() > 0) {
            initDouble = Double.parseDouble(initStr);
        }
        
        final String label = parameter.getLabel();
        if (this.parameterRE != null && label.matches(this.parameterRE)) {
            Parameter param = new Parameter(label, initDouble);
            parameters.add(label);
            sbml.getModel().addParameter(param);
        } else {
            Species species = new Species(parameter.getLabel(), entity.getName());
            species.setCompartment(this.compartment);
            if (initDouble != null) {
                species.setInitialConcentration(initDouble);
            }
            sbml.getModel().addSpecies(species);
        }
    }
    
    private void handleProcess(CSML19ProcessElement process) {
        CSML19KineticElement kinetic = process.getSimulationConditionElement().getKineticElement();
        String formula = null;
        if (kinetic.getKineticStyle().equals("connectorcustom")) {
            CSML19ConnectorElement conn = process.getFunctionElement().getConnectorElement(0);
            formula = conn.getKineticElement().getKineticParameterElementParameter(0).getValue();
        } else {
            formula = kinetic.getKineticParameterElementParameter(0).getValue();
        }
        if (formula == null) {
            System.out.println("Warning: formula could not be determined for process "+process.getLabel());
            return;
        }

        ASTNode ast = libsbml.parseFormula(formula);
        KineticLaw law = new KineticLaw(ast);
        Reaction reaction = new Reaction(process.getLabel(),
                process.getLabel(), law);
        
        for (String reactant: SBMLTools.getReferences(ast, null)) {
            if (!this.parameters.contains(reactant)) {
                reaction.addReactant(new SpeciesReference(reactant, 1));
            }
        }
        
        for (CSML19ConnectorElement conn: process.getFunctionElement().getConnectorElement()) {
            String to = conn.getTo();
            if (to.equals(process.getLabel())) continue;
            if (this.entityMap.containsKey(to)) {
                to = this.entityMap.get(to);
            }
            reaction.addProduct(new SpeciesReference(to, 1));
        }
        
        this.sbml.getModel().addReaction(reaction);
    }

    public static void main(String[] args) throws IOException {
        boolean ok = SBMLTools.initLibSBML();
        if (!ok) System.exit(1);
        
        // BIOMD0000000175.xml
        // ErbB-Chen_et_al_2008-A431-norules.xml
        SBML sbml = new SBML(new File("/home/kovaska/bioserver/analyses/pathway/ErbB-Chen_et_al_2008-A431-norules.xml"));
        
        File outDir = new File("/home/kovaska/sbml");
        SBMLTable table = new SBMLTable(new File(outDir, "speciesTable.csv"),
            new File(outDir, "reaction.csv"),
            new File(outDir, "parameter.csv"),
            new File(outDir, "compartment.csv"),
            new File(outDir, "rule.csv"),
            new File(outDir, "events.csv"),
            new File(outDir, "annotations.csv"));
//        table.writeTables(sbml);
        table.readTables(sbml);
        libsbml.writeSBML(sbml.getDocument(), "/home/kovaska/model.xml");
        
        SBML2HTML html = new SBML2HTML(sbml);
        html.writeHTML(new File("/home/kovaska/model.html"), true);
    }
}
*/