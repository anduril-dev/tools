package org.anduril.javatools.pathway;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.sbml.libsbml.ASTNode;
import org.sbml.libsbml.Compartment;
import org.sbml.libsbml.Event;
import org.sbml.libsbml.EventAssignment;
import org.sbml.libsbml.Model;
import org.sbml.libsbml.Parameter;
import org.sbml.libsbml.Reaction;
import org.sbml.libsbml.Rule;
import org.sbml.libsbml.SBase;
import org.sbml.libsbml.Species;
import org.sbml.libsbml.UnitDefinition;

import edu.uci.ics.jung.algorithms.cluster.WeakComponentClusterer;

public class SBML2HTML {
    public static final String SECT_COMPARTMENTS = "section-compartments";
    public static final String SECT_UNITS = "section-units";
    public static final String SECT_SPECIES = "section-species";
    public static final String SECT_REACTIONS = "section-reactions";
    public static final String SECT_PARAMETERS = "section-parameters";
    public static final String SECT_RULES = "section-rules";
    public static final String SECT_EVENTS = "section-events"; 
    public static final String SECT_DETAILS = "section-details";
    public static final String SECT_DETAILS_PREFIX = "details-";
    
    private static final String CSS_STYLE = "a:hover { background-color: #fcc }"
        + "a.tooltip span {display:none; padding:2px 3px; margin-left:8px;} "
        + "a.tooltip:hover span{display:inline; position:absolute; background:#fff; "
        + " border:1px solid #000; color:#000;}";
    
    private SBML sbml;
    private String newline = "\n";
    private String bgcolor = "#ddd";
    private boolean includeModifiers = true;
    
    public SBML2HTML(SBML sbml) {
        this.sbml = sbml;
    }
    
    public String generateHTML(boolean headerFooter) {
        StringBuffer sb = new StringBuffer();
        
        if (headerFooter) {
            sb.append("<html>"+newline);
            sb.append("<head>"+newline);
            sb.append(String.format("<title>%s</title>", sbml.getLabel())+newline);
            sb.append("<style type=\"text/css\"><!--"+newline);
            sb.append(CSS_STYLE+newline);
            sb.append("--></style>"+newline);
            sb.append("</head>"+newline);
            sb.append("<body>"+newline);
        }
        
        generateTOC(sb);
        generateSummary(sb);
        generateCompartments(sb);
        generateUnits(sb);
        generateSpecies(sb);
        generateGraphMetrics(sb);
        generateReactions(sb);
        generateParameters(sb);
        generateRules(sb);
        generateEvents(sb);
        generateDetails(sb);
        
        if (headerFooter) {
            sb.append("</body>"+newline);
            sb.append("</html>"+newline);
        }
        
        return sb.toString();
    }
    
    public void writeHTML(File target, boolean headerFooter) throws IOException {
        FileWriter writer = new FileWriter(target);
        writer.write(generateHTML(headerFooter));
        writer.close();
    }
    
    private boolean hasDetails(SBase obj) {
        return obj.getAnnotation() != null || obj.getNotes() != null;
    }
    
    private String formatTableHeader(String[] headers) {
        StringBuffer sb = new StringBuffer();
        sb.append("<table>"+newline);
        if (headers != null && headers.length > 0) {
            sb.append("<tr>"+newline);
            for (String header: headers) {
                sb.append(String.format("<th align=\"left\">%s</th>", header));
            }
            sb.append("</tr>"+newline);
        }
        return sb.toString();
    }
    
    private String formatTR(String id, int index) {
        String s = "<tr";
        if (id != null) s += String.format(" id=\"%s\"", id);
        if (index % 2 != 0) s += String.format(" style=\"background-color: %s\"", this.bgcolor);
        s += ">"+newline;
        return s;
    }
    
    private String formatTD(Object contents) {
        return String.format("<td>%s</td>", contents)+newline;
    }
    
    public static String formatLink(SBase obj, String tooltip) {
        if (obj == null) return "<i>(null object)</i>";
        String name;
        if (obj.getName() != null && obj.getName().length() > 0) {
            name = obj.getName();
        } else {
            name = obj.getId();
        }
        if (tooltip != null && tooltip.length() > 0) {
            return String.format("<a class=\"tooltip\" href=\"#%s\">%s<span>%s</span></a>",
                    obj.getId(), name, tooltip);
        } else {
            return String.format("<a href=\"#%s\">%s</a>", obj.getId(), name);
        }
    }
    
    public static String formatLink(SBase obj) {
        return formatLink(obj, getTooltip(obj));
    }
    
    public static String getTooltip(SBase obj) {
        if (obj instanceof Species) {
            Species species = (Species)obj;
            String tooltip = String.format("IV: %.3e",
                    SBMLTools.getInitial(species));
            if (obj.getModel().getNumCompartments() > 1) {
                final String comp = species.getCompartment();
                if (comp != null && comp.length() > 0) tooltip += ", "+comp;
            }
            return tooltip;
        } else if (obj instanceof Parameter) {
            Parameter param = (Parameter)obj;
            return String.format("%.3e", param.getValue());
        } else if (obj instanceof Reaction) {
            Reaction reaction = (Reaction)obj;
            StringBuffer sb = new StringBuffer();
            final Model model = obj.getModel();

            for (int j=0; j<reaction.getNumReactants(); j++) {
                String id = reaction.getReactant(j).getSpecies();
                Species species = model.getSpecies(id);
                if (j > 0) sb.append(" + ");
                double stoi = reaction.getReactant(j).getStoichiometry();
                if (stoi != 1) sb.append(String.format("%.1f&times;", stoi));
                sb.append(species.getName());
            }
            
            sb.append(reaction.getReversible() ? "&hArr;" : "&rArr;");
            
            for (int j=0; j<reaction.getNumProducts(); j++) {
                String id = reaction.getProduct(j).getSpecies();
                Species species = model.getSpecies(id);
                if (j > 0) sb.append(" + ");
                double stoi = reaction.getProduct(j).getStoichiometry();
                if (stoi != 1) sb.append(String.format("%.1f&times;", stoi));
                sb.append(species.getName());
            }
            
            return sb.toString();
        } else {
            return null;
        }
    }
    
    private String formatTruncatedSpecies(Iterable<Species> speciesList, int limit) {
        StringBuffer sb = new StringBuffer();
        int num = 0;
        for (Species species: speciesList) {
            num++;
            if (num <= limit) {
                if (num > 1) sb.append(", ");
                sb.append(formatLink(species));
            } else if (num == limit+1) {
                sb.append(", ...");
            }
        }
        sb.append(String.format(" (%d)", num));
        return sb.toString();
    }
    
    private void generateTOC(StringBuffer sb) {
        sb.append("<p>");
        sb.append(String.format("<a href=\"#%s\">Compartments</a>", SECT_COMPARTMENTS));
        sb.append(String.format(" | <a href=\"#%s\">Units</a>", SECT_UNITS));
        sb.append(String.format(" | <a href=\"#%s\">Species</a>", SECT_SPECIES));
        sb.append(String.format(" | <a href=\"#%s\">Reactions</a>", SECT_REACTIONS));
        sb.append(String.format(" | <a href=\"#%s\">Parameters</a>", SECT_PARAMETERS));
        sb.append(String.format(" | <a href=\"#%s\">Rules</a>", SECT_RULES));
        sb.append(String.format(" | <a href=\"#%s\">Events</a>", SECT_EVENTS));
        sb.append(String.format(" | <a href=\"#%s\">Details</a>", SECT_DETAILS));
        sb.append("</p>"+newline);
    }
    
    private void generateSummary(StringBuffer sb) {
        final Model model = this.sbml.getModel();
        sb.append("<p>");
        
        if (model.getName() != null && model.getName().length() > 0) {
            sb.append(model.getName());
        } else {
            sb.append(model.getId());
        }
        sb.append("<br />"+newline);
        
        sb.append(String.format("SBML level %d version %d<br />",
                sbml.getDocument().getLevel(), sbml.getDocument().getVersion())+newline);
        sb.append(String.format("%d species, %d reactions, %d global parameters, "
                +"%d rules, %d events, %d constraints, %d initial assignments\n",
                model.getNumSpecies(), model.getNumReactions(),
                model.getNumParameters(), model.getNumRules(),
                model.getNumEvents(), model.getNumConstraints(),
                model.getNumInitialAssignments()));
        
        sb.append("</p>"+newline);
    }
    
    private void generateCompartments(StringBuffer sb) {
        sb.append(String.format("<h1 id=\"%s\">Compartments</h1>",
                SECT_COMPARTMENTS)+newline);
        sb.append("<table>"+newline);
        sb.append("<tr>"+newline);
        sb.append("  <th>ID</th><th>Name</th><th>Size</th><th>Species</th>"+newline);
        sb.append("</tr>"+newline);
        
        final Model model = this.sbml.getModel();
        for (int i=0; i<model.getNumCompartments(); i++) {
            Compartment comp = model.getCompartment(i);
            sb.append(formatTR(comp.getId(), i));
            sb.append(String.format("  <td>%s</td>", comp.getId())+newline);
            sb.append(String.format("  <td>%s</td>", comp.getName())+newline);
            sb.append(String.format("  <td>%f</td>", comp.getSize())+newline);
            
            final int LIMIT = 10;
            int num = 0;
            sb.append("  <td>");
            for (Species species: sbml.iterSpecies()) {
                if (species.getCompartment().equals(comp.getId())) {
                    num++;
                    if (num < LIMIT) {
                        if (num > 1) sb.append(", ");
                        sb.append(formatLink(species));
                    } else if (num == LIMIT) {
                        sb.append(", ...");
                    }
                }
            }
            sb.append(String.format(" (%d)", num));
            sb.append("  </td>"+newline);
            
            sb.append("</tr>"+newline);
        }
        
        sb.append("</table>"+newline);
    }
    
    private void generateUnits(StringBuffer sb) {
        final Model model = this.sbml.getModel();
        if (model.getNumUnitDefinitions() == 0) return;
        
        sb.append(String.format("<h1 id=\"%s\">Unit definitions</h1>",
                SECT_UNITS)+newline);
        sb.append(formatTableHeader(new String[] {"ID", "Definition"}));
        
        for (int i=0; i<model.getNumUnitDefinitions(); i++) {
            UnitDefinition ud = model.getUnitDefinition(i);
            String id = ud.getId();
            if (ud.getName() != null && ud.getName().length()>0) {
                id += String.format(" (%s)", ud.getName());
            }
            sb.append(formatTR(null, i));
            sb.append(String.format("  <td>%s</td>", id)+newline);
            sb.append(String.format("  <td>%s</td>", UnitDefinition.printUnits(ud)));
            sb.append("</tr>"+newline);
        }
        
        sb.append("</table>"+newline);
    }    
    
    private void generateSpecies(StringBuffer sb) {
        sb.append(String.format("<h1 id=\"%s\">Species</h1>",
                SECT_SPECIES)+newline);
        sb.append(formatTableHeader(
                new String[] {"Name", "ID", "Compartment", "Initial value", "Reactions"}));
        
        Map<Species, List<Reaction>> reactionMap = sbml.getSpeciesReactionMap();
        
        final Model model = this.sbml.getModel();
        for (int i=0; i<model.getNumSpecies(); i++) {
            Species species = model.getSpecies(i);
            sb.append(formatTR(species.getId(), i));
            String name = species.getName();
            if (species.getBoundaryCondition()) name += " <i>(boundary)</i>";
            if (hasDetails(species)) {
                name += String.format(" <i><a href=\"#%s%s\">d</a></i>",
                        SECT_DETAILS_PREFIX, species.getId());
            }
            sb.append(String.format("  <td>%s</td>", name)+newline);
            sb.append(String.format("  <td>%s</td>", species.getId())+newline);
            sb.append(String.format("  <td>%s</td>",
                    formatLink(model.getCompartment(species.getCompartment())))+newline);
            
            double initial = SBMLTools.getInitial(species);
            String initialStr;
            if (initial < 0.001 || initial > 1e7) initialStr = String.format("%.3e", initial);
            else initialStr = String.format("%.3f", initial);
            sb.append(String.format("  <td>%s %s</td>",
                    initialStr, species.getUnits())+newline);
            
            sb.append("  <td>");
            for (int j=0; j<reactionMap.get(species).size(); j++) {
                Reaction reaction = reactionMap.get(species).get(j);
                if (j>0) sb.append(", ");
                sb.append(formatLink(reaction));
                
                String qualifier = "";
                boolean isLeft = reaction.getReactant(species.getId()) != null;
                boolean isRight = reaction.getProduct(species.getId()) != null;
                boolean isMod = reaction.getModifier(species.getId()) != null;
                if (isLeft) qualifier += "L";
                if (isRight) qualifier += "R";
                if (isMod) qualifier += "M";
                sb.append(String.format(" (%s)", qualifier));
            }
            sb.append("</td>"+newline);
            sb.append("</tr>"+newline);
        }
        
        sb.append("</table>"+newline);
    }
    
    private void generateGraphMetrics(StringBuffer sb) {
        sb.append("<h2 id=\"%s\">Network analysis</h2>"+newline);
        sb.append("<p>Source species (in-degree=0): ");
        boolean first = true;
        for (Species species: sbml.getSourceSpecies()) {
            if (!first) sb.append(", ");
            first = false;
            sb.append(formatLink(species));
        }
        sb.append("</p>");
        
        sb.append("<p>Sink species (out-degree=0): ");
        first = true;
        for (Species species: sbml.getSinkSpecies()) {
            if (!first) sb.append(", ");
            first = false;
            sb.append(formatLink(species));
        }
        sb.append("</p>");
        
        Set<Set<SBMLVertex>> components =
            new WeakComponentClusterer<SBMLVertex, SBMLEdge>().transform(sbml.asJUNG());
        sb.append("<p>Number of weakly connected network components: "+components.size());
        if (components.size() > 1) {
            sb.append(formatTableHeader(new String[] {"Component", "Species"}));
            int compNum = 1;
            for (Set<SBMLVertex> comp: components) {
                sb.append(formatTR(null, compNum-1));
                sb.append(formatTD(compNum));
                List<Species> speciesList = new ArrayList<Species>();
                for (SBMLVertex v: comp) {
                    if (v.isSpecies()) {
                        speciesList.add(((SBMLSpeciesVertex)v).getSpecies());
                    }
                }
                sb.append(formatTD(formatTruncatedSpecies(speciesList, 10)));
                sb.append("</tr>"+newline);
                compNum++;
            }
            sb.append("</table>"+newline);
        }
        
        sb.append("</p>");
        
    }
    
    private void generateReactions(StringBuffer sb) {
        sb.append(String.format("<h1 id=\"%s\">Reactions</h1>",
                SECT_REACTIONS)+newline);
        sb.append(formatTableHeader(
                new String[] {"ID", "Reaction", "Rate"}));
        
        final Model model = this.sbml.getModel();
        for (int i=0; i<model.getNumReactions(); i++) {
            Reaction reaction = model.getReaction(i);
            sb.append(formatTR(reaction.getId(), i));
            
            String reactionID = reaction.getId();
            if (hasDetails(reaction)) {
                reactionID += String.format(" <i><a href=\"#%s%s\">d</a></i>",
                        SECT_DETAILS_PREFIX, reaction.getId());
            }
            if (reaction.getFast()) {
                reactionID += " <i>(fast)</i>";
            }
            sb.append(String.format("  <td>%s</td>", reactionID)+newline);
            
            sb.append("  <td>");
            for (int j=0; j<reaction.getNumReactants(); j++) {
                String id = reaction.getReactant(j).getSpecies();
                Species species = model.getSpecies(id);
                if (j > 0) sb.append(" <b>+</b> ");
                double stoi = reaction.getReactant(j).getStoichiometry();
                if (stoi != 1) sb.append(String.format("%.1f&times;", stoi));
                sb.append(formatLink(species));
            }
            
            String ent = reaction.getReversible() ? "hArr" : "rArr";
            sb.append(String.format(" &%s; ", ent)+newline);
            
            for (int j=0; j<reaction.getNumProducts(); j++) {
                String id = reaction.getProduct(j).getSpecies();
                Species species = model.getSpecies(id);
                if (j > 0) sb.append(" <b>+</b> ");
                double stoi = reaction.getProduct(j).getStoichiometry();
                if (stoi != 1) sb.append(String.format("%.1f&times;", stoi));
                sb.append(formatLink(species));
            }
            
            if (this.includeModifiers && reaction.getNumModifiers() > 0) {
                sb.append(" (<b>mod</b> ");
                for (int j=0; j<reaction.getNumModifiers(); j++) {
                    String id = reaction.getModifier(j).getSpecies();
                    Species species = model.getSpecies(id);
                    if (j > 0) sb.append(", ");
                    sb.append(formatLink(species));
                }
                sb.append(")");
            }
            
            sb.append("</td>"+newline);
            
            String formula = "";
            if (reaction.getKineticLaw() != null) {
                formula = sbml.formulaToHTML(reaction.getKineticLaw().getMath());
            }
            sb.append(String.format("  <td>%s</td>", formula)+newline);
            
            sb.append("</tr>"+newline);
        }
        
        sb.append("</table>"+newline);
    }
    
    private void generateParameter(StringBuffer sb, Parameter param,
            Map<Parameter,List<Reaction>> reactionMap, Reaction ownerReaction, int index) {
        String name = param.getName();
        sb.append(formatTR(param.getId(), index));
        String id = param.getId();
        if (ownerReaction != null) id = ownerReaction.getId()+":"+id;
        if (name != null && name.length() > 0 && !name.equals(id)) {
            id += String.format(" (%s)", name);
        }
        if (!param.getConstant()) {
            id += " <i>(non-constant)</i>";
        }
        if (hasDetails(param)) {
            id += String.format(" <i><a href=\"#%s%s\">d</a></i>",
                    SECT_DETAILS_PREFIX, param.getId());
        }
        
        sb.append(String.format("  <td>%s</td>", id)+newline);
            
        final double value = param.getValue();
        final String valueStr;
        if (value<0.001 || value>1e7) valueStr = String.format("%.3e", value);
        else valueStr = String.format("%.3f", value);
        sb.append(String.format("  <td>%s %s</td>", valueStr, param.getUnits())+newline);
        
        sb.append("  <td>");
        List<Reaction> reactions = reactionMap.get(param);
        if (reactions == null) reactions = new ArrayList<Reaction>();
        if (ownerReaction != null) reactions.add(ownerReaction);
        for (int j=0; j<reactions.size(); j++) {
            Reaction reaction = reactions.get(j);
            if (j>0) sb.append(", ");
            sb.append(formatLink(reaction));
        }
        sb.append("</td>"+newline);
        
        sb.append("</tr>"+newline);
    }
    
    private void generateParameters(StringBuffer sb) {
        final Model model = this.sbml.getModel();
        sb.append(String.format("<h1 id=\"%s\">Parameters</h1>",
                SECT_PARAMETERS)+newline);
        sb.append(formatTableHeader(
                new String[] {"ID", "Value", "Reactions"}));
        
        Map<Parameter,List<Reaction>> reactionMap = sbml.getParameterReactionMap();
        
        int paramCount = 0;
        for (int i=0; i<model.getNumParameters(); i++) {
            Parameter param = model.getParameter(i);
            generateParameter(sb, param, reactionMap, null, paramCount);
            paramCount++;
        }
        
        for (int i=0; i<model.getNumReactions(); i++) {
            Reaction reaction = model.getReaction(i);
            if (reaction.getKineticLaw() != null) {
                for (int j=0; j<reaction.getKineticLaw().getNumParameters(); j++) {
                    Parameter param = reaction.getKineticLaw().getParameter(j);
                    generateParameter(sb, param, reactionMap, reaction, paramCount);
                    paramCount++;
                }
            }
        }
        
        sb.append("</table>"+newline);
    }

    private void generateRules(StringBuffer sb) {
        final Model model = this.sbml.getModel();
        if (model.getNumRules() == 0) return;
        
        sb.append(String.format("<h1 id=\"%s\">Rules</h1>",
                SECT_RULES)+newline);
        sb.append("<table>"+newline);
        
        for (int i=0; i<model.getNumRules(); i++) {
            Rule rule = model.getRule(i);
            
            sb.append(formatTR(null, i));
            String left;
            if (rule.isAlgebraic()) {
                left = "0";
            } else if (rule.isRate()) {
                left = String.format("d%s/dt", formatLink(sbml.getSBase(rule.getVariable())));
            } else {
                left = formatLink(sbml.getSBase(rule.getVariable()));
            }
            
            sb.append(String.format(" <td>%s</td>"+newline, left));
            sb.append("<td>=</td>"+newline);
            sb.append(String.format(" <td>%s</td>",
                    sbml.formulaToHTML(rule.getMath()))
                    +newline);
            sb.append("</tr>"+newline);
        }
        
        sb.append("</table>"+newline);
    }
    
    private void generateEvents(StringBuffer sb) {
        final Model model = this.sbml.getModel();
        if (model.getNumEvents() == 0) return;
        
        sb.append(String.format("<h1 id=\"%s\">Events</h1>",
                SECT_EVENTS)+newline);
        sb.append(formatTableHeader(
                new String[] {"Trigger", "Delay", "Assigments"}));
        
        for (int i=0; i<model.getNumEvents(); i++) {
            Event event = model.getEvent(i);
            
            String s = "&nbsp;";
            ASTNode math = event.getTrigger().getMath();
            if (math != null) s = sbml.formulaToHTML(math);
            sb.append(String.format(" <td>%s</td>"+newline, s));
         
            s = "&nbsp;";
            if (event.getDelay() != null) {
                math = event.getDelay().getMath();
                if (math != null) {
                    s = sbml.formulaToHTML(math);
                }
            }
            sb.append(String.format(" <td>%s</td>"+newline, s));
            
            sb.append("<td>");
            for (int eaIndex=0; eaIndex<event.getNumEventAssignments(); eaIndex++) {
                EventAssignment ea = event.getEventAssignment(eaIndex);
                if (eaIndex > 0) sb.append(" ; ");
                sb.append(formatLink(sbml.getSBase(ea.getVariable())));
                sb.append(" = ");
                math = ea.getMath();
                if (math != null) sb.append(sbml.formulaToHTML(math));
            }
            sb.append("</td>"+newline);
            
            sb.append("</tr>"+newline);
        }
        
        sb.append("</table>"+newline);
    }
    
    private void generateDetails(StringBuffer sb) {
        final Model model = this.sbml.getModel();
        sb.append("<p><hr /></p>");
        sb.append(String.format("<h1 id=\"%s\">Details</h1>",
                SECT_DETAILS)+newline);

        String s = model.getNotesString();
        if (s != null && s.length() > 0) {
            sb.append("<h2>Model notes</h2>");
            sb.append(s);
        }
        
        for (Species species: sbml.iterSpecies()) {
            sb.append(generateSBaseDetails(species, "Species"));
        }

        for (int i=0; i<model.getNumReactions(); i++) {
            sb.append(generateSBaseDetails(model.getReaction(i), "Reaction"));
        }
        
        for (int i=0; i<model.getNumParameters(); i++) {
            sb.append(generateSBaseDetails(model.getParameter(i), "Parameter"));
        }
    }
    
    private String generateSBaseDetails(SBase obj, String kind) {
        StringBuffer sb = new StringBuffer();
        String s;
        
        s = obj.getNotesString();
        if (s.startsWith("<notes>")) s = s.substring(7, s.length());
        if (s.endsWith("</notes>")) s = s.substring(0, s.length()-8);
        s = s.trim();
        if (s != null && s.length() > 0) sb.append(s);
        generateMiriam(sb, obj);
        
        if (sb.length() > 0) {
            sb.insert(0, kind+": "+formatLink(obj)+"<br/>"+newline);
            sb.insert(0, String.format("<h2 id=\"%s%s\">Details: %s</h2>"+newline,
                    SECT_DETAILS_PREFIX, obj.getId(), obj.getId()));
        }
        
        return sb.toString();
    }
    
    private void generateMiriam(StringBuffer sb, SBase obj) {
        List<MiriamEntry> anns = SBMLTools.getRDFAnnotations(obj);
        if (anns.isEmpty()) return;
        sb.append(formatTableHeader(
                new String[] {"Type", "Key", "Qualifier"}));
        
        for (MiriamEntry ann: anns) {
            for (int i=0; i<ann.getNumResources(); i++) {
                sb.append("<tr>"+newline);
                sb.append(String.format("  <td>%s</td>", ann.getResourcePrefix(i)));
                sb.append(String.format("  <td>%s</td>", ann.getResourcePostfix(i)));
                sb.append(String.format("  <td>%s</td>", ann.getQualifier()));
                sb.append("</tr>"+newline);
            }
        }
        
        sb.append("</table>"+newline);
    }
    
}
