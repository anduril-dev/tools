package org.anduril.javatools.pathway;

public interface SBMLVisitor {
    public void visitSpecies(SBMLSpeciesVertex vertex);
    public void visitReaction(SBMLReactionVertex vertex);
    public void visitEdge(SBMLEdge edge);
}
