package org.anduril.javatools.pathway.KEGG;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.xml.rpc.ServiceException;

import keggapi.KEGGLocator;
import keggapi.KEGGPortType;
import keggapi.LinkDBRelation;

/**
 * Client class for querying proteins and pathways from KEGG.
 * 
 * @since 9.10.2007 
 * @author Sirkku Karinen
 * @author Kristian Ovaska
 * */

public class KEGG {
	
	public static final String uniprotRegexp = 
		"\\b(([A-NR-Z][0-9][A-Z])|([OPQ][0-9][0-9A-Z]))[A-Z0-9]{2}?[0-9]\\b";
	
	/**
	 * Cache URL for Homo sapiens Uniprot mapping.
	 */
	public static final String CACHE_UNIPROT_HUMAN =
	    "ftp://ftp.genome.jp/pub/kegg/genes/organisms/hsa/hsa_uniprot.list";
	
	private static final boolean DEBUG = false;
	
	private KEGGLocator locator;
    private KEGGPortType serv;
    private String species = "";
    private String allPaths = "";
    private String uniprotCacheURL;
    private Map<String, List<String>> annotationCache;
    private Map<String, String> uniprot2keggCache;

    /**
     * Constructor
     * 
     * @param species for instance "homo sapiens"
     * @param uniprotCacheURL URL containing all Uniprot annotations for 
     *  a given genome. Used to map between Uniprot and KEGG identifiers.
     * */
	public KEGG(String species, String uniprotCacheURL) throws IOException, MalformedURLException {
		try{
			this.species = species;
			this.uniprotCacheURL = uniprotCacheURL;
			this.annotationCache = new HashMap<String, List<String>>();
			this.uniprot2keggCache = new HashMap<String, String>();
			
			locator = new KEGGLocator();
			serv    = locator.getKEGGPort();
			//get all pathways
			try{
				this.allPaths = serv.bfind("path "+this.species);
			}catch(RemoteException e){
				System.err.println("Error loading KEGG pathways for "+this.species+"\n"+e);
				e.printStackTrace();
			}
			
			if (this.uniprotCacheURL != null) loadUniprotCache(this.uniprotCacheURL);
		}catch(ServiceException e){
			System.err.println(e);
		}
	}
	
	public KEGG(String species) throws IOException, MalformedURLException {
	    this(species, null);
	}
	
	/**
	 * Gets all genes from pathway
	 * 
	 * @param pathwayID KEGG internal pathway name
	 * @return List object of arrays that have uniprot id and gene name
	 * */
	public List<String[]> getGenesByPathwayID(String pathwayID) throws RemoteException{
		Vector <String[]> res = new Vector<String[]>();
		
        String[] results = serv.get_genes_by_pathway(pathwayID);

        for (int i = 0; i < results.length; i++) {
        	String name = getGeneName(results[i]);
        	//System.out.println(name);
        	
        	List<String> ids = resolveUniprotID(results[i]);
        	
        	//System.out.println(results[i]+" : "+ids.size()+" Uniprot references");
            
        	if(ids.size() == 0){
        	    if (DEBUG) System.out.println("UniProt not found: "+results[i]);
            	ids = resolveNCBIID(results[i]);
            }
            
            if(ids.size() > 0){	
            	for(String id: ids){
            		String[] s = {id, name};
            		res.add(s);
            		//protId.setProperty(((String)id), name);
            		//System.out.println("	"+(String)id);
            	}
            }else{
            	String[] s = {results[i], name};
        		res.add(s);
            }    
            
        }
        return res;
	}
	/**
	 * Loads all pathways and finds the id of te pathway and then gets all genes from
	 * that pathway
	 * 
	 * @param pathwayName name that descripes pathways to be found
	 * @return List object of arrays that have uniprot id and gene name
	 */
	public List<String[]> getGenesByPathwayName(String pathwayName) throws RemoteException{
		List <String[]> res = new Vector<String[]>();

		Vector<String> selPaths = new Vector<String>();

		Pattern p = Pattern.compile(pathwayName, Pattern.CASE_INSENSITIVE);

		System.out.println(p.pattern());

		Scanner sc = new Scanner(allPaths);
		sc.useDelimiter("\n");
		

		while(sc.hasNext()){
			String line = sc.next();
			//System.out.println("process "+line);

			Matcher m = p.matcher(line);

			if(m.find()){
				selPaths.add(line.substring(0, line.indexOf(" ")));
				//System.out.println("match: "+selPaths.lastElement());
			}
		}

		for(String path : selPaths){
			res.addAll(getGenesByPathwayID(path));
		}

		return res;
	}
	/**
	 * Gets all proteins from all the pathways that query protein is present
	 * 
	 * @param uniprotID UniProt (or ncbi gene id) identifier for protein
	 * @return List object of arrays that have uniprot id and gene name
	 * */
	public List<String[]> getInteractionsForProtein(String uniprotID)throws RemoteException{
		List <String[]> res = new Vector<String[]>();
		String[] results = getPathwayIdsForProtein(uniprotID); 
		
		if(results != null){
			//System.out.println(results.length+" pathways for "+uniprotID);
	    
			for(String s: results){
				//System.out.println("participians in pathway "+s+":");
				res.addAll(getGenesByPathwayID(s));
			}
		}
	    return res;	
	}
	/**
	 * Gets all pathway ids where protein is involved
	 * 
	 * @param uniprotID UniProt (or ncbi gene id) identifier for protein
	 * @return String array pathway ids
	 * */
	public String[] getPathwayIdsForProtein(String uniprotID) throws RemoteException{
	    //first get kegg id for protein
	    String keggID = convUniprot2Kegg(uniprotID);
	    if (keggID == null) keggID = convNCBI2Kegg(uniprotID);
	    if (keggID == null) return new String[] {};
	    String[] results = serv.get_pathways_by_genes(new String[]{keggID});
	    return results;
	}
	
	/**
	 * Gets all pathway ids and names for protein
	 * 
	 * @param uniprotID UniProt (or ncbi gene id) identifier for protein
	 * @return List object of arrays that have pathway id, name and description 
	 * */
	public List<String[]> getPathwaysForProtein(String uniprotID) throws RemoteException{
		
		String[] results = getPathwayIdsForProtein(uniprotID);

	    Vector<String[]> pathwayNames = new Vector<String[]>();
	    
	    if(results != null){
	    	for(int i = 0; i < results.length; i++){
	    		//then get the name which is somewhere in return string
	    		String res = serv.bget("pathway "+results[i]);
	    		String [] vals = res.split("\n");
	    		
	    		String name = "NA";
	    		String description = "NA";
	    		String pathClass = "NA";
	    		int numOfGenes = 0;
	    		String group = null;
	    		
	    		String key = "^([A-Z,_]+?)\\s+?(\\b[a-zA-Z0-9\\p{Punct}\\s]+?)$";
	    		String val = "^\\s+?(\\b[a-zA-Z0-9\\p{Punct}\\s]+?)$";
	            
	    		Pattern k = Pattern.compile(key);
	    		Pattern v = Pattern.compile(val);

	    		for(String s : vals){
	    			Matcher keyM = k.matcher(s);
	    			Matcher valM = v.matcher(s);
	    			if(keyM.matches()){
	    				if(keyM.group(1).equalsIgnoreCase("name")){
	    					name = keyM.group(2);
	    				}else if(keyM.group(1).equalsIgnoreCase("description")){
	    					description = keyM.group(2);
	    				}else if(keyM.group(1).equalsIgnoreCase("class")){
	    					pathClass = keyM.group(2);
	    				}else if(keyM.group(1).equalsIgnoreCase("gene")){
	    					numOfGenes ++;
	    				}
	    				group = keyM.group(1);
	    			}else if(valM.matches() && group != null){
	    				if(group.equalsIgnoreCase("gene")){
	    					numOfGenes ++;
	    				}
	    			}
	    		}

	    		String[] temp = {results[i], name, description, pathClass, numOfGenes+""};
	    		
	    		pathwayNames.add(temp);

	    	}
	    }
		return pathwayNames;
	}

	/**
	 * Helper methods for converting KEGG ids to uniprot or ncbi ids
	 * */
	private List<String> resolveUniprotID(String keggID) throws RemoteException{
	    List<String> uniprot = this.annotationCache.get(keggID);
	    if (uniprot != null) return uniprot;
	    else if (this.uniprotCacheURL != null) {
	        /* Cache URL is used but no results are found */
	        return new ArrayList<String>();
	    } else {
	        uniprot = resolveReference(keggID, "UniProt");
	        this.annotationCache.put(keggID, uniprot);
	        return uniprot;
	    }
	}
	
	private List<String> resolveNCBIID(String keggID) throws RemoteException{
	    List<String> ncbi = this.annotationCache.get(keggID);
	    if (ncbi != null) return ncbi;
	    else {
	        ncbi = resolveReference(keggID, "ncbi-geneid");
	        this.annotationCache.put(keggID, ncbi);
	        return ncbi;
	    }
	}
	
	private List<String> resolveReference(String keggID, String reference) throws RemoteException{
		LinkDBRelation[] rel =  serv.get_linkdb_by_entry(keggID, reference, 1, 100);

		ArrayList<String> resNames = new ArrayList<String>();
		for(Object o : rel){
			String[]names = ((LinkDBRelation)o).getEntry_id2().split(":");

			if(names.length >= 2){
				resNames.add(names[1]);
			}	
		}
		return resNames;
	}
	
	private String convUniprot2Kegg(String uniprotID) throws RemoteException {
	    String keggID = this.uniprot2keggCache.get(uniprotID);
	    if (keggID != null) return keggID;
	    
        String res = serv.bconv("uniprot:"+uniprotID);
        String[] query = res.split("[ \t]+");
        if(query.length <= 1) return null;
        else {
            keggID = query[1].trim();
            this.uniprot2keggCache.put(uniprotID, keggID);
            return keggID;
        }
	}
	
	private String convNCBI2Kegg(String ncbiID) throws RemoteException {
        String res = serv.bconv("ncbi-geneid:"+ncbiID);
        String[] query = res.split("[ \t]+");
        if(query.length <= 1) return null;    
        else return query[1].trim();
	}
	
	/**
	 * Helper method for geting name for KEGG data object
	 * */
	private String getGeneName(String keggID) throws RemoteException{
		
		String res = serv.btit(keggID);
		//System.out.print(res);
		int s = res.indexOf(" ");
		int e = res.indexOf(";");
		
		if(s != -1 && e != -1 && e > s)
			return res.substring(s+1, e);
		else
			return "no name";
	}
	
	private void loadUniprotCache(String cacheURL) throws MalformedURLException, IOException {
	    if (DEBUG) System.out.println("Loading Uniprot cache...");
	    URL url = new URL(cacheURL);
	    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
	    try {
	        String line;
	        while ((line = reader.readLine()) != null) {
	            String[] tokens = line.split("[ \t]");
	            if (tokens.length >= 2) {
	                final String keggID = tokens[0].trim();
	                String uniprotID = tokens[1].trim();
	                if (uniprotID.startsWith("up:")) uniprotID = uniprotID.substring(3);
	                
	                List<String> annotation = this.annotationCache.get(keggID);
	                if (annotation == null) {
	                    annotation = new ArrayList<String>();
	                    this.annotationCache.put(keggID, annotation);
	                }
	                annotation.add(uniprotID);
	                
	                String cachedKegg = this.uniprot2keggCache.get(uniprotID);
	                if (cachedKegg == null) this.uniprot2keggCache.put(uniprotID, keggID);
	            }
	        }
	    } finally {
	        reader.close();
	    }
	    if (DEBUG) System.out.println("Done");
	}
	
	enum SearchType {
		PROTEINS, PATHWAYS;
	};
	
	public static void main(String[] args) throws Exception {
		KEGG kegg = new KEGG("homo sapiens");
		
		for (String s: kegg.getPathwayIdsForProtein("P04626")) System.out.println(s);
		
	    return;
	}

	/*
	private static void proteinSearch(BufferedWriter out, BufferedReader in, KEGG k, String uniprotRegexp) throws FileNotFoundException, IOException{
		LinkedHashSet<String> proteins = new LinkedHashSet<String>();

		String line = in.readLine();
		while(line != null){
			List<String[]> res = null;
			//uniprot id
			if(line.matches(uniprotRegexp)){
				res = k.getInteractionsForProtein(line);
			}
			else{
				//pathway name
				res = k.getGenesByPathwayName(line);
			}
			for(String[] ss : res){
				String temp = "";

				for(String s: ss){
					temp = temp+s+"\t";
				}
				//get rid of dublicates
				proteins.add(temp);

			}

			line = in.readLine();
		}
		//finally print results
		for(String s : proteins){
			out.write(s+"\t");
			out.newLine();
		}
		
	}
	
	private static void pathwaySearch(BufferedWriter out, BufferedReader in, KEGG k) throws FileNotFoundException, IOException{

		Pattern p = Pattern.compile(".*("+uniprotRegexp+").*");
		String line = in.readLine();
		while(line != null){
			String[] vals = line.split("\t");
			out.write("\"");
			for(String val : vals){
				Matcher m = p.matcher(val);
				if(m.matches()){
					List<String[]> res = null;
					String id = m.group(1);
					//System.out.println("search with "+id);
					res = k.getPathwaysForProtein(id);
//					String[] res = null;
//					res = k.getPathwayIdsForProtein(id);
					StringBuffer sb = new StringBuffer();

					for(String[] ss : res){

						for(String s: ss){
							sb.append(s.replaceAll("\n", ""));
							sb.append(" ");
						}
						sb.replace(sb.length()-1, sb.length()-1, ";");
					}

//					if(res != null){
//					for(String s : res){

//					temp = temp+s+",";


//					}
//					}
					out.write(sb.toString());
					

				}
			}
			out.write("\"");
			out.write("\t");
			out.write(line);
			out.newLine();
			
			line = in.readLine();
		}
	}
	*/
}
