package org.anduril.javatools.pathway.KEGG;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.TestCase;

public class TestKEGGOnline extends TestCase {
	
	public void testKeggDebug(){
		try{
//		KEGGLocator  locator = new KEGGLocator();
//        KEGGPortType serv    = locator.getKEGGPort();

        
    /*    URLConnection.setContentHandlerFactory(new ContentHandlerFactory(){
        	public ContentHandler createContentHandler(String mimetype){
        		return null;
        	}
        });*/
        /*
        String res = serv.bfind("pathway aeropyrum pernix");
        System.out.println("result: "+res);
        */
        //String res = serv.bfind("pathway gallus gallus");
        //String res = serv.bget("pathway path:hsa05322");
        String res = "ENTRY       hsa05322                    Pathway\n"+
        "NAME        Systemic lupus erythematosus - Homo sapiens (human)\n"+
        "DESCRIPTION Systemic lupus erythematosus (SLE) is characterized by circulating IgG autoantibodies that are specific for self-antigens, such as DNA, nuclear proteins and certain cytoplasmic components. Immune complexes comprising autoantibody and self-antigen is deposited particulary in the renal glomeruli and mediate a systemic inflammatory response by activating complement or via Fc-gamma-R-mediated neutrophil and macrophage activation.  Activation of complement leads to injury both through formation of the membrane attack complex (C5b-9) or by generation of the anaphylatoxin and cell activator C5a. Neutrophils and macrophages cause tissue injury by the release of oxidants and proteases.\n"+
        "CLASS       Human Diseases; Immune Disorders\n"+
        "PATHWAY_MAP hsa05322\n"+
        "DBLINKS     DS: H00080\n"+
        "ORGANISM    Homo sapiens (human) [GN:hsa]\n"+
        "GENE        121504  HIST4H4 [KO:K11254]\n"+
        "            126961  HIST2H3C, H3F2, H3FM [KO:K11253]\n"+
        "            128312  HIST3H2BB [KO:K11252]\n"+
        "REFERENCE   PMID:15380523\n"+
        "  AUTHORS   Hoffman RW.\n"+
        "  TITLE     T cells in the pathogenesis of systemic lupus erythematosus.\n"+
        "  JOURNAL   Clin Immunol 113:4-13 (2004)\n"+
        "REL_PATHWAY hsa04060  Cytokine-cytokine receptor interaction\n"+
        "            hsa04514  Cell adhesion molecules (CAMs)\n"+
        "            hsa04610  Complement and coagulation cascades\n"+
        "            hsa04612  Antigen processing and presentation\n"+
        "            hsa04630  Jak-STAT signaling pathway\n"+
        "            hsa04660  T cell receptor signaling pathway\n"+
        "            hsa04662  B cell receptor signaling pathway\n"+
        "            hsa04670  Leukocyte transendothelial migration\n"+
        "KO_PATHWAY  ko05322";
        System.out.println(res);
        String [] vals = res.split("\n");
        String name = "NA";
        int numOfGenes = 0;
        
        String regex = "^([A-Z,_]+?)\\s+?(\\b[a-zA-Z0-9\\p{Punct}\\s]+?)$";
        String prev = "^\\s+?(\\b[a-zA-Z0-9\\p{Punct}\\s]+?)$";
        
        String group = null;
        String [] keys = {"name", "description", "class", "gene", "reference", "rel_pathway"};

        HashMap<String, String> parsed = new HashMap<String, String>();
        Pattern p = Pattern.compile(regex);
        Pattern pre = Pattern.compile(prev);
        
        for(String s : vals){
        	Matcher m = p.matcher(s);
        	Matcher processing = pre.matcher(s);
        	if(m.matches()){
        		for(String key : keys){
        			if(m.group(1).equalsIgnoreCase(key)){
        				if(!parsed.containsKey(key)){
        					parsed.put(key, "");
        				}
        				parsed.put(key, parsed.get(key)+m.group(2)+",");
        				group = key;
        				
        				if(group.equals("gene"))
                			numOfGenes++;
            		}	
        		}

        	}else if(processing.matches() && group != null){
        		if(group.equals("gene"))
        			numOfGenes++;
        		System.out.println(processing.group(1));
				parsed.put(group, parsed.get(group)+processing.group(1)+",");        		
        	}else{
        		group = null;
        	}
        }
        
        for(String key : parsed.keySet()){
        	System.out.println(key+" : "+parsed.get(key));
        }
        
        System.out.println("name : "+name);
//  System.out.println("result: "+res);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	public void testPathID() throws Exception{
		KEGG k = new KEGG("homo sapiens", KEGG.CACHE_UNIPROT_HUMAN);
		//List<String[]> res = k.getGenesByPathwayID("path:hsa01430");
		List<String[]> res = k.getGenesByPathwayID("path:hsa04010");
		
		printRes(res);
	}
	public void testKegg() throws Exception{


		String inFile = "kegg_test_file.txt";
		String outFile = "kegg_test_file_output.txt";
		String species = "homo sapiens";
		
		String uniprotRegexp = 
			"\\b(([A-N,R-Z][0-9][A-Z])||([O,P,Q][0-9][A-Z,0-9]))[A-Z,0-9][A-Z,0-9][0-9]\\b";
		
		BufferedReader in = new BufferedReader(new FileReader(inFile));
		BufferedWriter out = new BufferedWriter(new FileWriter(outFile));
		
		LinkedHashSet<String> proteins = new LinkedHashSet<String>();
		
		KEGG k = new KEGG(species);
		String line = in.readLine();
		while(line != null){
			List<String[]> res = null;
			//uniprot id
			if(line.matches(uniprotRegexp)){
				res = k.getInteractionsForProtein(line);
			}
			else{
			//pathway name
				res = k.getGenesByPathwayName(line);
			}
			for(String[] ss : res){
				String temp = "";
				
				for(String s: ss){
					temp = temp+s+"\t";
				}
				//get rid of dublicates
				proteins.add(temp);
				
			}
			printRes(res);
			line = in.readLine();
		}
		//finally print results
		for(String s : proteins){
			out.write(s+"\t");
			out.newLine();
		}
		in.close();
		out.close();
	}
	
	public static void testPathwayNameElements() throws Exception {
		KEGG k = new KEGG("drosophila melanogaster");
		
		List<String[]> res = k.getGenesByPathwayName("Jak-STAT");
		printRes(res);
	}
	public static void testInteractionsForProt() throws Exception {
		KEGG k = new KEGG("homo sapiens");
		
		List<String[]> res = k.getInteractionsForProtein("P56975");;
		printRes(res);
	}
	
	public static void testPathwaysForProt() throws Exception {
		KEGG k = new KEGG("homo sapiens");
		
		List<String[]> res = k.getPathwaysForProtein("P01375");
		printRes(res);
	}

    public static void testPathwaysForProtCache() throws Exception {
        KEGG k = new KEGG("homo sapiens", KEGG.CACHE_UNIPROT_HUMAN);
        
        List<String[]> res = k.getPathwaysForProtein("P01375");
        printRes(res);
    }
	
	private static void printRes(List <String[]> res){
		for(String[] ss : res){
			for(String s : ss){
				System.out.print(s+"\t");
			}
			System.out.println("");
		}
	}
	
	public static void testProtNeibourghs() throws Exception {
		KEGG k = new KEGG("homo sapiens");
		//Vector<String> proteins = new Vector<String>();
		String[] files = {"D:\\projects\\Reactome\\PAP_UniProtAcces.txt", "D:\\projects\\Reactome\\PAP_UniProtIDs.txt"};
		try{
			for(String f : files){
				System.out.println("\nProcess file "+f+"\n");
				BufferedReader in = 
					new BufferedReader(new FileReader(f));
				String line = in.readLine();
				while(line != null){
					String[] data = line.split("\t");
					//add all ids -> if there is at least some hits
					for(String d : data){
						k.getInteractionsForProtein(d);// getGenesByPathwayName("Apoptosis", "D:\\DBs\\kgml\\hsa");
					}
					line = in.readLine();
				}
				
			}
		}catch(IOException e){
			
		}
		
	
		
	//	k.getInteractionsForProtein("P04637");// getGenesByPathwayName("Apoptosis", "D:\\DBs\\kgml\\hsa");
	//	k.printResults("D:\\Duuni\\data\\kegg_ptpi.txt");
	}
	
}
