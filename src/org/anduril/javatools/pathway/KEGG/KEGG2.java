package org.anduril.javatools.pathway.KEGG;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.rpc.ServiceException;

import keggapi.KEGGLocator;
import keggapi.KEGGPortType;
import keggapi.LinkDBRelation;

/**
 * Fetch annotations from KEGG. The following
 * conversions are supported: KEGG gene ID to/from
 * Uniprot; KEGG gene/Uniprot ID to/from KEGG pathways;
 * KEGG pathway ID to/from pathway name; KEGG gene ID
 * to gene name.
 * 
 * <p>
 * Querying can be done using the SOAP API of KEGG,
 * or an annotation table loaded over network. The
 * latter is faster for batch queries (over 10-20
 * proteins). Only Uniprot IDs and pathway annotations
 * can be fetched using the annotation table.
 * </p>
 * 
 * @author Kristian Ovaska
 * @author Sirkku Karinen 
 */
public class KEGG2 {
    
    /** Cache URL for Homo sapiens */
    public static final String CACHE_HUMAN =
        "ftp://ftp.genome.jp/pub/kegg/genes/organisms/hsa/hsa_xrefall.list";
    
    public final static String PATHWAY_PREFIX = "path:";    
    
    private String species;
    private KEGGPortType soap;
    private boolean useCache;

    private Map<String, List<String>> kegg2uniprot;
    private Map<String, String> uniprot2kegg;
    private Map<String, List<String>> pathway2kegg;
    private Map<String, List<String>> kegg2pathway;
    private Map<String, String> kegg2name;
    private Map<String, String> pathwayID2Name;
    private Map<String, String> pathwayID2Description;
    
    /**
     * Initialize.
     * @param species KEGG species, such as "homo sapiens".
     * @param cacheURL If non-null, this is a URL to a KEGG
     *  annotation file (xrefall) that is used to obtain
     *  mappings to Uniprot IDs and pathway IDs. For human,
     *  the URL is CACHE_HUMAN. If null, the cache is not
     *  used.
     */
    public KEGG2(String species, String cacheURL) throws ServiceException, IOException {
        this.species = species;
        this.kegg2uniprot = new HashMap<String, List<String>>();
        this.uniprot2kegg = new HashMap<String, String>();
        this.pathway2kegg = new HashMap<String, List<String>>();
        this.kegg2pathway = new HashMap<String, List<String>>();
        this.kegg2name = new HashMap<String, String>();
        this.pathwayID2Description = new HashMap<String, String>();
        this.pathwayID2Name = null; // Initialized lazily

        KEGGLocator locator = new KEGGLocator();
        this.soap = locator.getKEGGPort();
        
        if (cacheURL != null && !cacheURL.isEmpty()) {
            loadCache(cacheURL);
            this.useCache = true;
        } else {
            this.useCache = false;
        }
    }
    
    private void loadCache(String cacheURL) throws IOException {
        URL url = new URL(cacheURL);
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split("\t");
                if (tokens.length >= 4) {
                    final String keggID = tokens[0].trim();

                    String uniprotToken = tokens[3];
                    final String UNIPROT_PREFIX = "up:";
                    if (uniprotToken.startsWith(UNIPROT_PREFIX)) {
                        uniprotToken = uniprotToken.substring(UNIPROT_PREFIX.length());
                    }
                    List<String> uniprotIDs = new ArrayList<String>();
                    for (String uniprotID: uniprotToken.split(" ")) {
                        if (!uniprotID.isEmpty()) {
                            uniprotIDs.add(uniprotID);
                            this.uniprot2kegg.put(uniprotID, keggID);
                        }
                    }
                    this.kegg2uniprot.put(keggID, uniprotIDs);

                    if (tokens.length >= 7) {
                        String pathwayToken = tokens[6];
                        List<String> pathways = new ArrayList<String>();
                        final String PATHWAY_PREFIX = "path:";
                        for (String pathwayID: pathwayToken.split(" ")) {
                            if (!pathwayID.startsWith(PATHWAY_PREFIX)) pathwayID = PATHWAY_PREFIX + pathwayID;
                            if (!pathwayID.isEmpty()) {
                                pathways.add(pathwayID);
                                List<String> pathwayGenes = this.pathway2kegg.get(pathwayID);
                                if (pathwayGenes == null) {
                                    pathwayGenes = new ArrayList<String>();
                                    this.pathway2kegg.put(pathwayID, pathwayGenes);
                                }
                                pathwayGenes.add(keggID);
                            }
                        }
                        this.kegg2pathway.put(keggID, pathways);
                    }
                }
            }
        } finally {
            reader.close();
        }
    }
    
    private void initPathwayNames() throws RemoteException {
        if (this.pathwayID2Name != null) return;
        
        this.pathwayID2Name = new HashMap<String, String>();
        
        String allPaths = this.soap.bfind("path "+this.species);
        for (String line: allPaths.split("[\n\r]+")) {
            line = line.trim();
            String[] tokens = line.split("[ \t]+", 2);
            if (tokens.length >= 2) {
                final String pathwayID = tokens[0].trim();
                
                String nameDesc = tokens[1];
                String name;
                int pos = nameDesc.indexOf(';');
                if (pos > 0) {
                    name = nameDesc.substring(0, pos).trim();
                } else {
                    name = nameDesc;
                }
                
                pos = name.indexOf(" - ");
                if (pos > 3) name = name.substring(0, pos).trim();
                this.pathwayID2Name.put(pathwayID, name);
            }
        }
    }
    
    /**
     * Return the (possibly empty) list of Uniprot IDs for a KEGG gene ID.
     */
    public List<String> getUniprotByGene(String keggID) throws RemoteException {
        List<String> uniprots = this.kegg2uniprot.get(keggID);
        if (uniprots != null) {
            return uniprots;
        } else if (this.useCache) {
            // Cache is in use but keggID was not found in cache table
            return new ArrayList<String>();
        } else {
            uniprots = resolveReference(keggID, "UniProt");
            this.kegg2uniprot.put(keggID, uniprots);
            for (String uniprotID: uniprots) {
                this.uniprot2kegg.put(uniprotID, keggID);
            }
            return uniprots;
        }
    }

    /**
     * Return lists of Uniprot IDs for a collection of
     * KEGG gene ID. Each KEGG gene ID may map to zero or more
     * Uniprot IDs.
     */
    public List<List<String>> getUniprotByGene(List<String> keggIDs) throws RemoteException {
        List<List<String>> result = new ArrayList<List<String>>();
        for (String keggID: keggIDs) {
            result.add(getUniprotByGene(keggID));
        }
        return result;
    }
    
    /**
     * Return KEGG gene ID for a Uniprot ID. If the
     * Uniprot ID is not found, return null.
     */
    public String getKeggGene(String uniprotID) throws RemoteException {
        if (uniprotID == null) return null;
        if (this.uniprot2kegg.containsKey(uniprotID) || this.useCache) {
            return this.uniprot2kegg.get(uniprotID);
        } else {
            String res = this.soap.bconv("uniprot:"+uniprotID);
            String[] query = res.split("[ \t]+");
            final String keggID = (query.length <= 1) ? null : query[1].trim();
            this.uniprot2kegg.put(uniprotID, keggID);
            return keggID;
        }
    }

    /**
     * Return gene name for a KEGG gene ID. If the gene
     * is not found, return null.
     */
    public String getGeneName(String keggID) throws RemoteException {
        if (keggID == null) return null;
        if (this.kegg2name.containsKey(keggID)) {
            return this.kegg2name.get(keggID);
        } else {
            String res = this.soap.btit(keggID);
            int s = res.indexOf(" ");
            int e = res.indexOf(";");

            final String name = (s != -1 && e != -1 && e > s) ?
                    res.substring(s+1, e) : null; 
            this.kegg2name.put(keggID, name);
            return name;
        }
    }
    
    /**
     * Return pathway IDs by path name, using partial and case insensitive
     * matching. For example, "cycle" would match "Cell Cycle" and
     * "Citrate cycle".
     * 
     * @param pathwayName Pathway name (complete or partial).
     * @param stripPrefix If true, "path:" prefix is removed from
     *  pathway IDs. Otherwise, pathway IDs are returned as they are.
     */
    public List<String> getPathwaysByName(String pathwayName, boolean stripPrefix) throws RemoteException {
        initPathwayNames();
        
        Pattern pattern = Pattern.compile(pathwayName, Pattern.CASE_INSENSITIVE);
        List<String> pathwayIDs = new ArrayList<String>();
        for (Map.Entry<String, String> entry: this.pathwayID2Name.entrySet()) {
            Matcher matcher = pattern.matcher(entry.getValue());
            if (matcher.find()) {
                String pathwayID = entry.getKey();
                if (stripPrefix && pathwayID.startsWith(PATHWAY_PREFIX)) {
                    pathwayID = pathwayID.substring(PATHWAY_PREFIX.length());
                }
                pathwayIDs.add(pathwayID);
            }
        }
        return pathwayIDs;
    }
    
    /**
     * Return pathway IDs by path name, using partial and case insensitive
     * matching. The "path:" prefix is stripped from pathway IDs.
     */
    public List<String> getPathwaysByName(String pathwayName) throws RemoteException {
        return getPathwaysByName(pathwayName, true);
    }
    
    /**
     * Return the pathway name for a pathway ID.
     */
    public String getPathwayName(String pathwayID) throws RemoteException {
        initPathwayNames();
        if (!pathwayID.startsWith(PATHWAY_PREFIX)) {
            pathwayID = PATHWAY_PREFIX + pathwayID;
        }
        return this.pathwayID2Name.get(pathwayID);
    }

    /**
     * Return the pathway description for a pathway ID.
     * Description may be null.
     */
    public String getPathwayDescription(String pathwayID) throws RemoteException {
        if (!pathwayID.startsWith(PATHWAY_PREFIX)) {
            pathwayID = PATHWAY_PREFIX + pathwayID;
        }
        if (this.pathwayID2Description.containsKey(pathwayID)) {
            return this.pathwayID2Description.get(pathwayID);
        }
        
        String content = this.soap.bget(pathwayID);
        Pattern pattern = Pattern.compile("^DESCRIPTION[ \t]+(.*)", Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(content);
        final String description;
        if (matcher.find()) {
            description = matcher.group(1);
        } else {
            description = null;
        }
        
        this.pathwayID2Description.put(pathwayID, description);
        return this.pathwayID2Description.get(pathwayID);
    }
    
    /**
     * Return the set of genes on the pathway as KEGG gene IDs.
     * The IDs can be mapped to Uniprot IDs using getUniprotByGene.
     * If the pathway is not found, return an empty list.
     */
    public List<String> getGenesByPathwayID(String pathwayID) throws RemoteException {
        if (pathwayID == null) return new ArrayList<String>();
        if (!pathwayID.startsWith(PATHWAY_PREFIX)) {
            pathwayID = PATHWAY_PREFIX + pathwayID;
        }
        List<String> keggIDs = this.pathway2kegg.get(pathwayID);
        if (keggIDs != null) return keggIDs;
        
        // Was not cached => fetch using SOAP and add to cache
        keggIDs = new ArrayList<String>();
        String[] output = this.soap.get_genes_by_pathway(pathwayID);
        for (String keggID: output) {
            keggIDs.add(keggID);
        }
        this.pathway2kegg.put(pathwayID, keggIDs);
        return keggIDs;
    }
    
    /**
     * Return the set of pathway IDs for a Uniprot ID. Pathway
     * IDs can be converted to pathway names using getPathwayName.
     * 
     * @param uniprotID Uniprot ID
     * @param stripPrefix If true, "path:" prefix is removed from
     *  pathway IDs. Otherwise, pathway IDs are returned as they are.
     */
    public List<String> getPathwayIdsForProtein(String uniprotID, boolean stripPrefix) throws RemoteException {
        String keggID = getKeggGene(uniprotID);
        List<String> pathways;
        if (keggID == null) {
            return new ArrayList<String>();
        }
        
        pathways = this.kegg2pathway.get(keggID);
        if (pathways == null && this.useCache) {
            return new ArrayList<String>();
        } else if (pathways != null) {
            if (stripPrefix) {
                pathways = new ArrayList<String>(pathways); // Create copy
                for (int i=0; i<pathways.size(); i++) {
                    if (pathways.get(i).startsWith(PATHWAY_PREFIX)) {
                        final String newID = pathways.get(i).substring(PATHWAY_PREFIX.length());
                        pathways.set(i, newID);
                    }
                }
            }
            return pathways;
        } else {
            String[] results = this.soap.get_pathways_by_genes(new String[]{keggID});
            pathways = new ArrayList<String>();
            for (String pathwayID: results) {
                if (stripPrefix && pathwayID.startsWith(PATHWAY_PREFIX)) {
                    pathwayID = pathwayID.substring(PATHWAY_PREFIX.length());
                }
                pathways.add(pathwayID);
            }
            this.kegg2pathway.put(keggID, pathways);
            return pathways;
        }
    }
    
    /**
     * Return the set of pathway IDs for a Uniprot ID, stripping the
     * "path:" prefix if present.
     */
    public List<String> getPathwayIdsForProtein(String uniprotID) throws RemoteException {
        return getPathwayIdsForProtein(uniprotID, true);
    }

    /**
     * Return all proteins, as KEGG gene IDs, that are located
     * on the same pathway as the given protein.
     */
    public Set<String> getInteractionsForProtein(String uniprotID) throws RemoteException{
        Set<String> interactors = new HashSet<String>();
        List<String> pathways = getPathwayIdsForProtein(uniprotID); 
        
        if(pathways != null){
            for(String pathwayID: pathways){
                interactors.addAll(getGenesByPathwayID(pathwayID));
            }
        }
        return interactors; 
    }
    
    private List<String> resolveReference(String keggID, String reference) throws RemoteException {
        LinkDBRelation[] rel = this.soap.get_linkdb_by_entry(keggID, reference, 1, 100);

        ArrayList<String> resNames = new ArrayList<String>();
        for(Object o : rel){
            String[]names = ((LinkDBRelation)o).getEntry_id2().split(":");

            if(names.length >= 2){
                resNames.add(names[1]);
            }   
        }
        return resNames;
    }
    
    public static void main(String[] args) throws Exception {
        KEGG2 kegg = new KEGG2("homo sapiens", null);
        System.out.println(kegg.getPathwayName("hsa04012"));
        System.out.println(kegg.getPathwayDescription("hsa04012"));
        System.out.println(kegg.getPathwayName("hsa04012"));
        System.out.println(kegg.getPathwayDescription("hsa04012"));
    }
}