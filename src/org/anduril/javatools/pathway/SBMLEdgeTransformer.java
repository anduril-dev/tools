package org.anduril.javatools.pathway;

import org.apache.commons.collections15.Transformer;

public class SBMLEdgeTransformer implements Transformer<SBMLEdge, String> {
    private String attribute;
    
    public SBMLEdgeTransformer(String attribute) {
        this.attribute = attribute;
    }
    
    public String transform(SBMLEdge edge) {
        return edge.getAttribute(this.attribute);
    }
}