package org.anduril.javatools.pathway;

import java.io.BufferedWriter;
import java.io.IOException;

import edu.uci.ics.jung.io.GraphMLMetadata;
import edu.uci.ics.jung.io.GraphMLWriter;

public class CSBLGraphMLWriter<V,E> extends GraphMLWriter<V, E>  {
        /* Modified from JUNG 2 source code to add "attr.name"
         * and "attr.type" for "key". */
        protected void writeKeySpecification(String key, String type, 
                GraphMLMetadata<?> ds, BufferedWriter bw) throws IOException
        {
            bw.write("<key id=\"" + key + "\" for=\"" + type + "\" attr.name=\""+key+"\" attr.type=\"string\"");
            boolean closed = false;
            // write out description if any
            String desc = ds.description;
            if (desc != null)
            {
                if (!closed)
                {
                    bw.write(">\n");
                    closed = true;
                }
                bw.write("<desc>" + desc + "</desc>\n");
            }
            // write out default if any
            Object def = ds.default_value;
            if (def != null)
            {
                if (!closed)
                {
                    bw.write(">\n");
                    closed = true;
                }
                bw.write("<default>" + def.toString() + "</default>\n");
            }
            if (!closed)
                bw.write("/>\n");
            else
                bw.write("</key>\n");
        }
}
