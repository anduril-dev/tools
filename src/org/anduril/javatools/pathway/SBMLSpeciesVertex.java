package org.anduril.javatools.pathway;

import org.sbml.libsbml.Species;

public class SBMLSpeciesVertex implements SBMLVertex {
    private Species species;
    
    public SBMLSpeciesVertex(Species species) {
        this.species = species;
    }
    
    public String toString() {
        return this.species.getId();
    }

    public Species getSpecies() {
        return this.species;
    }
    
    public String getId() {
        return this.species.getId();
    }
    
    public String getAttribute(String name) {
        if (name.equals("fontsize")) {
            return "48";
        } else if (name.equals("label")) {
            return this.species.getName();
            /*
            String s = String.format("%s (%s)", this.species.getName(), this.species.getId());
            s = SBMLTools.insertNewlines(s, " @", "\\n", 16);
            return s;
            */
        } else if (name.equals("shape")) {
            return "ellipse";
        } else {
            return null;
        }
    }
    
    public boolean isSpecies() {
        return true;
    }
    
    public void visit(SBMLVisitor visitor) {
        visitor.visitSpecies(this);
    }
}
