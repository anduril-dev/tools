package org.anduril.javatools.pathway;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.sbml.libsbml.ASTNode;
import org.sbml.libsbml.Model;
import org.sbml.libsbml.Reaction;
import org.sbml.libsbml.SBMLDocument;
import org.sbml.libsbml.SBase;
import org.sbml.libsbml.Species;
import org.sbml.libsbml.XMLNode;
import org.sbml.libsbml.libsbml;
import org.sbml.libsbml.libsbmlConstants;

public class SBMLTools {
    /**
     * Loads the SWIG-generated libSBML Java module, or reports a sensible
     * diagnostic message about why it failed. The code is from libSBML.
     * @return True if loading was successful, false otherwise.
     */
    public static boolean initLibSBML() {
        String varname;
        String shlibname;

        if (System.getProperty("mrj.version") != null) {
            varname = "DYLD_LIBRARY_PATH";    // We're on a Mac.
            shlibname = "libsbmlj.jnilib and/or libsbml.dylib";
        } else {
            varname = "LD_LIBRARY_PATH";      // We're not on a Mac.
            shlibname = "libsbmlj.so and/or libsbml.so";
        }
        
        try {
            System.loadLibrary("sbmlj");
            // For extra safety, check that the jar file is in the classpath.
            Class.forName("org.sbml.libsbml.libsbml");
        } catch (SecurityException e) {
            e.printStackTrace();
            System.err.println("Could not load the libSBML library files due to a"+
            " security exception.\n");
            return false;
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
            System.err.println("Error: could not link with the libSBML library files."+
                    " It is likely\nyour " + varname +
                    " environment variable does not include the directories\n"+
                    "containing the " + shlibname + " library files.\n");
            return false;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.err.println("Error: unable to load the file libsbmlj.jar."+
                    " It is likely\nyour -classpath option and CLASSPATH" +
                    " environment variable\n"+
            "do not include the path to libsbmlj.jar.\n");
            return false;
        }
        
        return true;
    }
    
    public static List<String> getReferences(ASTNode node, List<String> names) {
        if (names == null) names = new ArrayList<String>();
        if (node.getType() == libsbmlConstants.AST_NAME && node.getName() != null) {
            names.add(node.getName());
        }
        for (int i=0; i<node.getNumChildren(); i++) {
            ASTNode child = node.getChild(i);
            getReferences(child, names);
        }
        return names;
    }
    
    public static String insertNewlines(String str, String splitChars, String newline, int maxLineLength) {
        StringBuffer sb = new StringBuffer(str);
        
        int lineLength = 0; 
        for (int i=1; i<sb.length(); i++) {
            final String s = new String(new char[] {sb.charAt(i-1)});
            lineLength++;
            if (lineLength > maxLineLength && splitChars.contains(s)) {
                sb.insert(i, newline);
                i += newline.length();
                lineLength = 0;
            }
        }
        
        return sb.toString();
    }
    
    public static List<MiriamEntry> getRDFAnnotations(SBase obj) {
        List<MiriamEntry> annotations = new ArrayList<MiriamEntry>();
        for (int i=0; i<obj.getNumCVTerms(); i++) {
            annotations.add(new MiriamEntry(obj.getCVTerm(i)));
        }
        return annotations;
    }
    
    public static String formulaToString(ASTNode tree, String newline, int maxLineLength) {
        String formula = libsbml.formulaToString(tree);
        formula = formula.replace(" ", "");
        formula = SBMLTools.insertNewlines(formula, " +-*/()", newline, maxLineLength);
        return formula;
    }
    
    public static ASTNode parseFormula(String formula) {
        ASTNode node = libsbml.parseFormula(formula);
        processParsedFormula(node);
        return node;
    }
    
    private static void processParsedFormula(ASTNode node) {
        if (node.getType() == libsbmlConstants.AST_NAME) {
            if (node.getName().equals("time")) {
                node.setType(libsbmlConstants.AST_NAME_TIME);
            }
        }
        for (int i=0; i<node.getNumChildren(); i++) {
            processParsedFormula(node.getChild(i));
        }
    }
    
    public static double getInitial(Species species) {
        if (species.isSetInitialAmount()) {
            return species.getInitialAmount();
        } else {
            return species.getInitialConcentration();
        }
    }
    
    public static List<Species> getReactionSpecies(SBMLDocument sbml, Reaction reaction,
            boolean includeModifiers) {
        LinkedHashSet<Species> speciesSet = new LinkedHashSet<Species>();
        final Model model = sbml.getModel();
        
        for (int i=0; i<reaction.getNumReactants(); i++) {
            String name = reaction.getReactant(i).getSpecies();
            Species species = model.getSpecies(name);
            if (species == null) continue;
            speciesSet.add(species);
        }

        for (int i=0; i<reaction.getNumProducts(); i++) {
            String name = reaction.getProduct(i).getSpecies();
            Species species = model.getSpecies(name);
            if (species == null) continue;
            speciesSet.add(species);
        }
        
        if (includeModifiers) {
            for (int i=0; i<reaction.getNumModifiers(); i++) {
                String name = reaction.getModifier(i).getSpecies();
                Species species = model.getSpecies(name);
                if (species == null) continue;
                speciesSet.add(species);
            }
        }
        
        return new ArrayList<Species>(speciesSet);
    }
    
    public static String getXMLContents(XMLNode node) {
        if (node == null) return null;
        StringBuffer sb = new StringBuffer();
        sb.append(node.getCharacters());
        for (int i=0; i<node.getNumChildren(); i++) {
            sb.append(node.getChild(i).getCharacters());
        }
        return sb.toString().trim();
    }
    
    public static final boolean setMetaID(SBase obj) {
        if (obj.getMetaId() != null && !obj.getMetaId().isEmpty()) {
            return false;
        }
        obj.setMetaId(obj.getId());
        return true;
    }
    
}
