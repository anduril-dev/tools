package org.anduril.javatools.pathway;

import org.apache.commons.collections15.Transformer;

public class SBMLVertexTransformer implements Transformer<SBMLVertex, String> {
    private String attribute;
    
    public SBMLVertexTransformer(String attribute) {
        this.attribute = attribute;
    }
    
    public String transform(SBMLVertex vertex) {
        return vertex.getAttribute(this.attribute);
    }
}