package org.anduril.javatools.pathway.anduril;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sbml.libsbml.Parameter;
import org.sbml.libsbml.Species;

import org.anduril.component.CommandFile;
//import org.anduril.component.DelegateSetup;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;
import org.anduril.javatools.pathway.SBML;
import org.anduril.javatools.pathway.SBMLTools;

public class SensitivityAnalysisComponent extends SkeletonComponent {
    private CommandFile cf;
    
    @Override
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        boolean ok = SBMLTools.initLibSBML();
        if (!ok) {
            cf.writeError("Could not load the libSBML library");
            return ErrorCode.ERROR;
        }
        this.cf = cf;
        
        String[] variables = cf.getParameter("variables").split("[ \t]*,[ \t]*");
        SBML sbml = new SBML(cf.getInput("model"));
        List<Double> timePoints = new ArrayList<Double>();
        List<Double> baseline = simulate(sbml, timePoints);
        double[][] jacobian = new double[baseline.size()][variables.length];
        
        for (int v=0; v<variables.length; v++) {
            String varID = variables[v];
            SBML cloned = new SBML(sbml.getDocument().cloneObject());
            Species species = cloned.getModel().getSpecies(varID);
            Parameter param = cloned.getModel().getParameter(varID);
            
            double origValue;
            double h;
            if (species != null) {
                if (species.isSetInitialAmount()) {
                    origValue = species.getInitialAmount();
                    h = getH(origValue);
                    species.setInitialAmount(origValue+h);
                } else if (species.isSetInitialConcentration()) {
                    origValue = species.getInitialConcentration();
                    h = getH(origValue);
                    species.setInitialConcentration(origValue+h);
                } else {
                    cf.writeError("Could not find initial value for species "+varID);
                    return ErrorCode.INVALID_INPUT;
                }
            } else if (param != null) {
                if (param.isSetValue()) {
                    origValue = param.getValue();
                    h = getH(origValue);
                    param.setValue(origValue+h);
                } else {
                    cf.writeError("Could not find initial value for parameter "+varID);
                    return ErrorCode.INVALID_INPUT;
                }
            } else {
                cf.writeError("Could not find model component "+varID);
                return ErrorCode.PARAMETER_ERROR;
            }

            System.out.println(String.format("Simulating with %s=%.10e",
                    varID, origValue+h));
            List<Double> result = simulate(cloned, null);
            if (result.size() != baseline.size()) {
                cf.writeError("Simulation results have different length with "+varID);
                return ErrorCode.ERROR;
            }
            
            for (int t=0; t<result.size(); t++) {
                jacobian[t][v] = (baseline.get(t) - result.get(t)) / h;
            }
        }
        
        String[] columns = new String[variables.length+1];
        columns[0] = "time";
        for (int i=0; i<variables.length; i++) columns[i+1] = variables[i];
        CSVWriter writer = new CSVWriter(columns, cf.getOutput("sensitivity"));
        for (int t=0; t<baseline.size(); t++) {
            writer.write(timePoints.get(t));
            for (int v=0; v<variables.length; v++) {
                writer.write(jacobian[t][v]);
            }
        }
        writer.close();
        
        return ErrorCode.OK;
    }
    
    private double getH(double x) {
     // sqrt(DOUBLE_EPSILON) * x
        return 1e-8 * x;
    }
    
    private List<Double> simulate(SBML model, List<Double> timePoints) throws IOException {
        return new ArrayList<Double>();
        // TODO revise former Delegate
        /*
        if (timePoints == null) timePoints = new ArrayList<Double>();
        
        DelegateSetup ds = cf.getDelegate("simulator");
        model.write(ds.getInput("model"));
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("startTime", cf.getParameter("startTime"));
        params.put("endTime", cf.getParameter("endTime"));
        ds.writeParameters(params);
        
        boolean ok = ds.launch();
        if (!ok) {
            throw new IOException("SBML simulation failed");
        }
        
        List<Double> result = new ArrayList<Double>();
        timePoints.clear();
        CSVParser parser = new CSVParser(ds.getOutput("timepoints"));
        int index = parser.getColumnIndex(cf.getParameter("target"));
        final int C_TIME = 1;
        
        while (parser.hasNext()) {
            String[] line = parser.next();
            
            if (line[C_TIME] == null) timePoints.add(null);
            else timePoints.add(Double.parseDouble(line[C_TIME]));
            
            if (line[index] == null) result.add(null);
            else result.add(Double.parseDouble(line[index]));
        }        
        
        return result;
        */
    }

    public static void main(String[] args) {
        new SensitivityAnalysisComponent().run(args);
    }
    
}
