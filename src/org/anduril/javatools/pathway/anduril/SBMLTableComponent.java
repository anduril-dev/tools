package org.anduril.javatools.pathway.anduril;

import org.sbml.libsbml.SBMLDocument;
import org.sbml.libsbml.libsbml;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.javatools.pathway.SBML;
import org.anduril.javatools.pathway.SBMLTable;
import org.anduril.javatools.pathway.SBMLTools;

public class SBMLTableComponent extends SkeletonComponent {
    public final String PORT_SPECIES = "species";
    public final String PORT_REACTION = "reactions";
    public final String PORT_PARAMETER = "parameters";
    public final String PORT_COMPARTMENT = "compartments";
    public final String PORT_RULE = "rules";
    public final String PORT_EVENT = "events";
    public final String PORT_ANNOTATION = "annotations";

    @Override
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        boolean ok = SBMLTools.initLibSBML();
        if (!ok) {
            cf.writeError("Could not load the libSBML library");
            return ErrorCode.ERROR;
        }
        
        SBMLDocument sbmlDoc = libsbml.readSBML(cf.getInput("model").getAbsolutePath());
        if (sbmlDoc == null) {
            cf.writeError("Could not load the SBML model");
            return ErrorCode.INPUT_IO_ERROR;
        }
        SBML sbml = new SBML(sbmlDoc);
        SBMLTable table = new SBMLTable(cf.getInput(PORT_SPECIES),
                cf.getInput(PORT_REACTION), cf.getInput(PORT_PARAMETER),
                cf.getInput(PORT_COMPARTMENT), cf.getInput(PORT_RULE),
                cf.getInput(PORT_EVENT), cf.getInput(PORT_ANNOTATION));
        table.readTables(sbml);

        table = new SBMLTable(cf.getOutput(PORT_SPECIES),
                cf.getOutput(PORT_REACTION), cf.getOutput(PORT_PARAMETER),
                cf.getOutput(PORT_COMPARTMENT), cf.getOutput(PORT_RULE),
                cf.getOutput(PORT_EVENT), cf.getOutput(PORT_ANNOTATION));
        table.writeTables(sbml);
        libsbml.writeSBML(sbml.getDocument(), cf.getOutput("model").getAbsolutePath());
        
        return ErrorCode.OK;
    }

    public static void main(String[] args) {
        new SBMLTableComponent().run(args);
    }
    
}
