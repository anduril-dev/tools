package org.anduril.javatools.pathway.anduril;

import java.io.File;

import org.sbml.libsbml.SBMLDocument;
import org.sbml.libsbml.libsbml;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;
import org.anduril.javatools.pathway.SBML;
import org.anduril.javatools.pathway.SBML2HTML;
import org.anduril.javatools.pathway.SBMLTools;

public class SBML2HTMLComponent extends SkeletonComponent {
    @Override
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        boolean ok = SBMLTools.initLibSBML();
        if (!ok) {
            cf.writeError("Could not load the libSBML library");
            return ErrorCode.ERROR;
        }
        
        SBMLDocument sbmlDoc = libsbml.readSBML(cf.getInput("model").getAbsolutePath());
        if (sbmlDoc == null) {
            cf.writeError("Could not load the SBML model");
            return ErrorCode.INPUT_IO_ERROR;
        }
        SBML sbml = new SBML(sbmlDoc);
        
        final String html = new SBML2HTML(sbml).generateHTML(true);
        File outDir = cf.getOutput("report");
        outDir.mkdirs();
        File outFile = new File(outDir, "index.html");
        Tools.writeString(outFile, html);
        
        return ErrorCode.OK;
    }

    public static void main(String[] args) {
        new SBML2HTMLComponent().run(args);
    }
}
