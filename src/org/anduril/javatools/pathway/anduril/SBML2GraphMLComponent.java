package org.anduril.javatools.pathway.anduril;

import java.util.Arrays;
import java.util.List;

import org.sbml.libsbml.SBMLDocument;
import org.sbml.libsbml.libsbml;

import edu.uci.ics.jung.graph.DirectedGraph;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.javatools.pathway.SBML;
import org.anduril.javatools.pathway.SBML2GraphML;
import org.anduril.javatools.pathway.SBMLEdge;
import org.anduril.javatools.pathway.SBMLTools;
import org.anduril.javatools.pathway.SBMLVertex;

public class SBML2GraphMLComponent extends SkeletonComponent {

    @Override
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        boolean ok = SBMLTools.initLibSBML();
        if (!ok) {
            cf.writeError("Could not load the libSBML library");
            return ErrorCode.ERROR;
        }
        
        SBMLDocument sbmlDoc = libsbml.readSBML(cf.getInput("model").getAbsolutePath());
        if (sbmlDoc == null) {
            cf.writeError("Could not load the SBML model");
            return ErrorCode.INPUT_IO_ERROR;
        }
        SBML sbml = new SBML(sbmlDoc);
        
        /* Create a sub-graph if necessary */
        final String inclParam = cf.getParameter("includeNodes").replaceAll(" ", "");
        List<String> nodes = Arrays.asList(inclParam.split(","));
        if (nodes.size() == 0) {
            cf.writeError("Empty includeNodes parameter");
            return ErrorCode.PARAMETER_ERROR;
        }
        DirectedGraph<SBMLVertex, SBMLEdge> graph;
        if (nodes.contains("*")) {
            graph = sbml.asJUNG();
        } else {
            graph = sbml.getSubGraph(nodes);
        }
        
        new SBML2GraphML().writeGraphML(graph, cf.getOutput("graph"));
        return ErrorCode.OK;
    }

    public static void main(String[] args) {
        new SBML2GraphMLComponent().run(args);
    }
    
}
