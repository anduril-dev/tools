package org.anduril.javatools.pathway;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.sbml.libsbml.ASTNode;
import org.sbml.libsbml.AlgebraicRule;
import org.sbml.libsbml.AssignmentRule;
import org.sbml.libsbml.Compartment;
import org.sbml.libsbml.Delay;
import org.sbml.libsbml.Event;
import org.sbml.libsbml.EventAssignment;
import org.sbml.libsbml.Model;
import org.sbml.libsbml.Parameter;
import org.sbml.libsbml.RateRule;
import org.sbml.libsbml.Reaction;
import org.sbml.libsbml.Rule;
import org.sbml.libsbml.SBase;
import org.sbml.libsbml.Species;
import org.sbml.libsbml.Trigger;
import org.sbml.libsbml.libsbml;

import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;

public class SBMLTable {
    public static final String[] SPECIES_COLUMNS = new String[] {
            "id", "name", "compartment",
            "initialAmount", "initialConcentration",
            "substanceUnits", "boundaryCondition",
            "notes"};

    public static final String[] REACTION_COLUMNS = new String[] {
            "id", "name", "reversible", "fast",
            "reactants", "products", "modifiers",
            "kineticLaw"};
    
    public static final String[] PARAMETER_COLUMNS = new String[] {
            "id", "name", "reaction", "value", "units", "constant"};

    public static final String[] COMPARTMENT_COLUMNS = new String[] {
        "id", "name", "size", "outside"};
    
    public static final String[] RULE_COLUMNS = new String[] {
            "type", "variable", "formula" };

    public static final String[] EVENT_COLUMNS = new String[] {
        "trigger", "delay"};

    public static final String[] ANNOTATION_COLUMNS = new String[] {
        "id", "namespace", "qualifier", "resourceHead", "resourceTail"};
    
    public static final String EVENT_COLUMN_VARIABLE = "variable"; 
    public static final String EVENT_COLUMN_FORMULA = "formula";
    
    public static final String CMD_KEEP = "_KEEP_";
    public static final String CMD_DELETE = "_DELETE_";
    
    public static final String RULE_ALGEBRAIC = "algebraic";
    public static final String RULE_ASSIGNMENT = "assignment";
    public static final String RULE_RATE = "rate";
        
    private File speciesFile;
    private File reactionFile;
    private File parameterFile;
    private File compartmentFile;
    private File ruleFile;
    private File eventFile;
    private File annotationFile;
    
    public SBMLTable(File speciesFile, File reactionFile, File parameterFile,
            File compartmentFile, File ruleFile, File eventFile,
            File annotationFile) {
        this.speciesFile = speciesFile;
        this.reactionFile = reactionFile;
        this.parameterFile = parameterFile;
        this.compartmentFile = compartmentFile;
        this.ruleFile = ruleFile;
        this.eventFile = eventFile;
        this.annotationFile = annotationFile;
    }
    
    public File getSpeciesFile() {
        return speciesFile;
    }
    
    public File getReactionFile() {
        return reactionFile;
    }
    
    public File getParameterFile() {
        return parameterFile;
    }
    
    public File getCompartmentFile() {
        return compartmentFile;
    }
    
    public File getRuleFile() {
        return ruleFile;
    }
    
    public File getEventFile() {
        return eventFile;
    }
    
    public File getAnnotationFile() {
        return annotationFile;
    }
    
    /* Writing methods */
    
    public void writeTables(SBML sbml) throws IOException {
        writeSpecies(sbml);
        writeReactions(sbml);
        writeParameters(sbml);
        writeCompartments(sbml);
        writeRules(sbml);
        writeEvents(sbml);
        writeAnnotations(sbml);
    }
    
    private static String format(String s, boolean emptyToNull) {
        if (s == null || s.isEmpty()) return null;
        else return s;
    }

    private static String format(String s) {
        return format(s, true);
    }
    
    private void writeSpecies(SBML sbml) throws IOException {
        CSVWriter writer = new CSVWriter(SPECIES_COLUMNS, speciesFile);
        for (int i=0; i<sbml.getModel().getNumSpecies(); i++) {
            Species species = sbml.getModel().getSpecies(i);
            writer.write(format(species.getId()));
            writer.write(format(species.getName()));
            writer.write(format(species.getCompartment()));
            if (species.isSetInitialAmount()) {
                writer.write(species.getInitialAmount());
            } else {
                writer.write(null);
            }
            if (species.isSetInitialConcentration()) {
                writer.write(species.getInitialConcentration());    
            } else {
                writer.write(null);
            }
            writer.write(format(species.getSubstanceUnits()));
            writer.write(species.getBoundaryCondition());
            writer.write(SBMLTools.getXMLContents(species.getNotes()));
        }
        writer.close();
    }
   
    
    private void writeReactions(SBML sbml) throws IOException {
        CSVWriter writer = new CSVWriter(REACTION_COLUMNS, reactionFile);
        for (int i=0; i<sbml.getModel().getNumReactions(); i++) {
            Reaction reaction = sbml.getModel().getReaction(i);
            writer.write(format(reaction.getId()));
            writer.write(format(reaction.getName()));
            writer.write(reaction.getReversible());
            writer.write(reaction.getFast());
            
            StringBuffer sb = new StringBuffer();
            for (int j=0; j<reaction.getNumReactants(); j++) {
                if (j>0) sb.append(",");
                sb.append(reaction.getReactant(j).getSpecies());
            }
            writer.write(format(sb.toString()));
            
            sb = new StringBuffer();
            for (int j=0; j<reaction.getNumProducts(); j++) {
                if (j>0) sb.append(",");
                sb.append(reaction.getProduct(j).getSpecies());
            }
            writer.write(format(sb.toString()));
            
            sb = new StringBuffer();
            for (int j=0; j<reaction.getNumModifiers(); j++) {
                if (j>0) sb.append(",");
                sb.append(reaction.getModifier(j).getSpecies());
            }
            writer.write(format(sb.toString()));          
            
            ASTNode math = reaction.getKineticLaw().getMath();
            if (math != null) writer.write(libsbml.formulaToString(math));
            else writer.write(null);
        }
        writer.close();
    }
    
    private void writeParameters(SBML sbml) throws IOException {
        CSVWriter writer = new CSVWriter(PARAMETER_COLUMNS, parameterFile);
        for (int i=0; i<sbml.getModel().getNumParameters(); i++) {
            Parameter param = sbml.getModel().getParameter(i);
            writer.write(format(param.getId()));
            writer.write(format(param.getName()));
            writer.write(null);
            if (param.isSetValue()) {
                writer.write(param.getValue());
            } else {
                writer.write(null);
            }
            writer.write(format(param.getUnits()));
            writer.write(param.getConstant());
        }
        /* TODO: reaction-local parameters */
        writer.close();
    }
    
    private void writeCompartments(SBML sbml) throws IOException {
        CSVWriter writer = new CSVWriter(COMPARTMENT_COLUMNS, compartmentFile);
        for (int i=0; i<sbml.getModel().getNumCompartments(); i++) {
            Compartment comp = sbml.getModel().getCompartment(i);
            writer.write(format(comp.getId()));
            writer.write(format(comp.getName()));
            writer.write(comp.getSize());
            writer.write(format(comp.getOutside()));
        }
        writer.close();
    }
    
    private void writeRules(SBML sbml) throws IOException {
        CSVWriter writer = new CSVWriter(RULE_COLUMNS, ruleFile);
        for (int i=0; i<sbml.getModel().getNumRules(); i++) {
            Rule rule = sbml.getModel().getRule(i);
            String type;
            if (rule.isAlgebraic()) type = "algebraic";
            else if (rule.isAssignment()) type = "assignment";
            else if (rule.isRate()) type = "rate";
            else type = "unknown";
            writer.write(type);
            
            writer.write(format(rule.getVariable()));
            
            ASTNode math = rule.getMath();
            if (math != null) writer.write(libsbml.formulaToString(math));
            else writer.write(null);
        }
        writer.close();
    }

    private void writeEvents(SBML sbml) throws IOException {
        int numAssignments = 0;
        for (int i=0; i<sbml.getModel().getNumEvents(); i++) {
            Event event = sbml.getModel().getEvent(i);
            int num = (int)event.getNumEventAssignments();
            if (num > numAssignments) numAssignments = num;
        }
        
        List<String> columns = new ArrayList<String>();
        for (String col: EVENT_COLUMNS) columns.add(col);
        for (int i=0; i<numAssignments; i++) {
            columns.add(EVENT_COLUMN_VARIABLE+(i+1));
            columns.add(EVENT_COLUMN_FORMULA+(i+1));
        }
        
        CSVWriter writer = new CSVWriter(columns.toArray(new String[] {}), eventFile);
        for (int i=0; i<sbml.getModel().getNumEvents(); i++) {
            Event event = sbml.getModel().getEvent(i);
            
            ASTNode math = event.getTrigger().getMath();
            if (math != null) writer.write(libsbml.formulaToString(math));
            else writer.write(null);
            
            if (event.getDelay() != null) {
                math = event.getDelay().getMath();
                if (math != null) writer.write(libsbml.formulaToString(math));
                else writer.write(null);
            } else {
                writer.write(null);
            }
            
            for (int a=0; a<numAssignments; a++) {
                EventAssignment ea = event.getEventAssignment(a);
                if (ea == null) {
                    writer.write(null);
                    writer.write(null);
                } else {
                    writer.write(ea.getVariable());
                    math = ea.getMath();
                    if (math != null) writer.write(libsbml.formulaToString(math));
                    else writer.write(null);
                }
            }
        }
        writer.close();
    }
    
    private void writeAnnotations(SBML sbml) throws IOException {
        CSVWriter writer = new CSVWriter(ANNOTATION_COLUMNS, annotationFile);
        final Model model = sbml.getModel();
        
        for (int i=0; i<model.getNumSpecies(); i++) {
            writeObjectAnnotations(model.getSpecies(i), writer);
        }
        
        for (int i=0; i<model.getNumReactions(); i++) {
            writeObjectAnnotations(model.getReaction(i), writer);
        }

        for (int i=0; i<model.getNumParameters(); i++) {
            writeObjectAnnotations(model.getParameter(i), writer);
        }
        
        for (int i=0; i<model.getNumCompartments(); i++) {
            writeObjectAnnotations(model.getCompartment(i), writer);
        }
        
        writer.close();
    }

    private void writeObjectAnnotations(SBase obj, CSVWriter writer) {
        if (obj == null) return;
        
        for (int term=0; term<obj.getNumCVTerms(); term++) {
            MiriamEntry miriam = new MiriamEntry(obj.getCVTerm(term));
            for (int resource=0; resource<miriam.getNumResources(); resource++) {
                writer.write(obj.getId());
                writer.write(miriam.getNamespaceURI());
                writer.write(miriam.getQualifier());
                writer.write(miriam.getResourceHead(resource));
                writer.write(miriam.getResourcePostfix(resource));
            }
        }
    }
    
    /* Read methods. */
    
    /**
     * Update the SBML model based on contents of
     * CSV tables.
     * @param sbml The model. It is updated in place.
     */
    public void readTables(SBML sbml) throws IOException {
        if (compartmentFile != null) readCompartments(sbml);
        if (speciesFile != null) readSpecies(sbml);
        if (parameterFile != null) readParameters(sbml);
        if (ruleFile != null) readRules(sbml);
        if (eventFile != null) readEvents(sbml);
        if (annotationFile != null) readAnnotations(sbml);
    }
    
    private static Double parseDouble(String doubleStr) {
        if (doubleStr == null || doubleStr.isEmpty()) return null;
        else return Double.parseDouble(doubleStr);
    }
    
    private void readSpecies(SBML sbml) throws IOException {
        CSVParser parser = new CSVParser(speciesFile);
        final int C_ID = parser.getColumnIndex("id", false);
        if (C_ID<0) {
            throw new IOException("Species table does not contain the id column");
        }
        final int C_NAME = parser.getColumnIndex("name", false);
        final int C_COMPARTMENT = parser.getColumnIndex("compartment", false);
        final int C_INIT_AMOUNT = parser.getColumnIndex("initialAmount", false);
        final int C_INIT_CONCENTRATION = parser.getColumnIndex("initialConcentration", false);
        
        while (parser.hasNext()) {
            String[] line = parser.next();
            String id = line[C_ID];
            String name = C_NAME<0 ? "" : line[C_NAME];
            if (name == null) name = "";
            String compartment = C_COMPARTMENT<0 ? null : line[C_COMPARTMENT];
            Double initAmount = C_INIT_AMOUNT<0 ?
                    null : parseDouble(line[C_INIT_AMOUNT]);
            Double initConcentration = C_INIT_CONCENTRATION<0 ?
                    null : parseDouble(line[C_INIT_CONCENTRATION]);
            
            boolean isNew = false;
            Species species = sbml.getModel().getSpecies(id);
            if (species == null) {
                species = new Species(id, name);
                isNew = true;
                
            }
            
            if (!name.equals(CMD_KEEP)) species.setName(name);
            if (name.equals(CMD_DELETE)) species.setName("");

            if (compartment != null && !compartment.equals(CMD_KEEP)) {
                if (compartment.equals(CMD_DELETE)) species.setCompartment("");
                else species.setCompartment(compartment);
            }
            
            if (initAmount != null) species.setInitialAmount(initAmount);
            if (initConcentration != null) species.setInitialConcentration(initConcentration);
            
            if (isNew) sbml.getModel().addSpecies(species);
        }
        parser.close();
    }
    
    private void readParameters(SBML sbml) throws IOException {
        CSVParser parser = new CSVParser(parameterFile);
        final int C_ID = parser.getColumnIndex("id", false);
        if (C_ID<0) {
            throw new IOException("Parameter table does not contain the id column");
        }
        final int C_NAME = parser.getColumnIndex("name", false);
        final int C_VALUE = parser.getColumnIndex("value", false);
        
        while (parser.hasNext()) {
            String[] line = parser.next();
            String id = line[C_ID];
            String name = C_NAME<0 ? "" : line[C_NAME];
            Double value = C_VALUE<0 ? null : Double.parseDouble(line[C_VALUE]);
            
            boolean isNew = false;
            Parameter param = sbml.getModel().getParameter(id);
            if (param == null) {
                param = new Parameter(id, name);
                isNew = true;
            }
            
            if (value != null) param.setValue(value);
            if (!name.equals(CMD_KEEP)) param.setName(name);
            if (name.equals(CMD_DELETE)) param.setName("");
            if (isNew) sbml.getModel().addParameter(param);
        }
        parser.close();
    }
    
    private void readCompartments(SBML sbml) throws IOException {
        CSVParser parser = new CSVParser(compartmentFile);
        final int C_ID = parser.getColumnIndex("id", true);
        final int C_NAME = parser.getColumnIndex("name", false);
        final int C_SIZE = parser.getColumnIndex("size", false);
        final int C_OUTSIDE = parser.getColumnIndex("outside", false);

        while (parser.hasNext()) {
            String[] line = parser.next();
            String id = line[C_ID];
            String name = C_NAME<0 ? "" : line[C_NAME];
            Double size = C_SIZE<0 ? null : Double.parseDouble(line[C_SIZE]);
            String outside = C_OUTSIDE<0 ? null : line[C_OUTSIDE];

            boolean isNew = false;
            Compartment comp = sbml.getModel().getCompartment(id);
            if (comp == null) {
                comp = new Compartment(id, name);
                isNew = true;
            }

            if (size != null) comp.setSize(size);
            if (!name.equals(CMD_KEEP)) comp.setName(name);
            if (name.equals(CMD_DELETE)) comp.setName("");
            if (outside != null && !outside.equals(CMD_KEEP)) {
                if (outside.equals(CMD_DELETE)) comp.unsetOutside();
                else comp.setOutside(outside);
            }
            
            if (isNew) sbml.getModel().addCompartment(comp);
        }
        parser.close();
    }
    
    private void readRules(SBML sbml) throws IOException {
        final Model model = sbml.getModel();
        CSVParser parser = new CSVParser(ruleFile);
        final int C_TYPE = parser.getColumnIndex("type", true);
        final int C_VARIABLE = parser.getColumnIndex("variable", true);
        final int C_FORMULA = parser.getColumnIndex("formula", true);

        while (parser.hasNext()) {
            String[] line = parser.next();
            String type = line[C_TYPE];
            String variable = line[C_VARIABLE];
            String formula = line[C_FORMULA];

            Rule rule = null;
            if (variable != null) {
                rule = model.getRule(variable);
            } else if (formula != null) {
                String normFormula = formula.replace(" ", "");
                for (int i=0; i<model.getNumRules(); i++) {
                    Rule thisRule = model.getRule(i);
                    ASTNode math = thisRule.getMath();
                    if (math != null) {
                        String thisFormula = libsbml.formulaToString(math).replace(" ", "");
                        if (normFormula.equals(thisFormula)) {
                            rule = thisRule;
                            break;
                        }
                    }

                }
            }

            if (rule == null) {
                if (type.equals(RULE_ALGEBRAIC)) rule = new AlgebraicRule(formula);
                else if (type.equals(RULE_ASSIGNMENT)) rule =
                    new AssignmentRule(variable, formula);
                else if (type.equals(RULE_RATE)) rule =
                    new RateRule(variable, formula);
                else throw new IOException("Invalid rule type: "+type);
                model.addRule(rule);
            } else {
                if (variable != null) rule.setVariable(variable);
                if (formula != null) rule.setFormula(formula);
            }

        }
        parser.close();
    }
    
    private void readEvents(SBML sbml) throws IOException {
        final Model model = sbml.getModel();
        CSVParser parser = new CSVParser(eventFile);
        final int C_TRIGGER = parser.getColumnIndex("trigger", true);
        final int C_DELAY = parser.getColumnIndex("delay", false);
        
        while (parser.hasNext()) {
            String[] line = parser.next();
            String trigger = line[C_TRIGGER];
            String delay = C_DELAY<0 ? null : line[C_DELAY];
            Event event = null;
            
            String normTrigger = trigger.replace(" ", "");
            for (int i=0; i<model.getNumEvents(); i++) {
                Event thisEvent = model.getEvent(i);
                String thisTrigger = null;
                ASTNode math = thisEvent.getTrigger().getMath();
                if (math != null) {
                    thisTrigger = libsbml.formulaToString(math).replace(" ", "");
                    if (normTrigger.equals(thisTrigger)) {
                        event = thisEvent;
                        break;
                    }
                }
            }
            
            boolean isNew = false;
            if (event == null) {
                event = new Event();
                event.setTrigger(new Trigger(SBMLTools.parseFormula(trigger)));
                if (event.getTrigger()==null || event.getTrigger().getMath()==null) {
                    throw new IllegalArgumentException("Invalid event trigger: "+trigger);
                }
                isNew = true;
            }
            
            if (delay != null) {
                event.setDelay(new Delay(SBMLTools.parseFormula(delay)));
            }
            
            /* Remove existing event assignments, then add the assignments
             * present in the CSV file. */
            while (event.getNumEventAssignments() > 0) {
                event.getListOfEventAssignments().remove(0);
            }
            
            for (int a=1; a<50; a++) {
                final int variableIndex = parser.getColumnIndex(EVENT_COLUMN_VARIABLE+a, false);
                final int formulaIndex = parser.getColumnIndex(EVENT_COLUMN_FORMULA+a, false);
                if (variableIndex >= 0 && formulaIndex >= 0) {
                    String variable = line[variableIndex];
                    String formula = line[formulaIndex];
                    event.addEventAssignment(new EventAssignment(
                            variable, SBMLTools.parseFormula(formula)));
                }
            }
            
            if (isNew) model.addEvent(event);
        }
        parser.close();
    }
    
    private void readAnnotations(SBML sbml) throws IOException {
        CSVParser parser = new CSVParser(annotationFile);
        final int C_ID = parser.getColumnIndex("id", true);
        final int C_NAMESPACE = parser.getColumnIndex("namespace", true);
        final int C_QUALIFIER = parser.getColumnIndex("qualifier", true);
        final int C_RESOURCE_HEAD = parser.getColumnIndex("resourceHead", true);
        final int C_RESOURCE_TAIL = parser.getColumnIndex("resourceTail", true);
        
        Set<String> seenIDs = new HashSet<String>();
        while (parser.hasNext()) {
            String[] line = parser.next();
            String id = line[C_ID];
            String namespace = line[C_NAMESPACE];
            String qualifier = line[C_QUALIFIER];
            String head = line[C_RESOURCE_HEAD]; 
            String tail = line[C_RESOURCE_TAIL];
            String resource = (head==null ? "" : head) + (tail==null ? "" : tail);
            
            SBase obj = sbml.getSBase(id);
            if (obj == null) {
                throw new IllegalArgumentException("SBML object not found: "+id);
            }
            if (!seenIDs.contains(id)) {
                obj.unsetCVTerms();
                seenIDs.add(id);
            }
            SBMLTools.setMetaID(obj);
            
            MiriamEntry miriam = new MiriamEntry(namespace, qualifier);
            miriam.getCVTerm().addResource(resource);
            
            boolean termFound = false;
            for (int i=0; i<obj.getNumCVTerms(); i++) {
                MiriamEntry thisMiriam = new MiriamEntry(obj.getCVTerm(i));
                if (miriam.typeEquals(thisMiriam)) {
                    termFound = true;
                    if (!thisMiriam.containsResource(resource)) {
                        thisMiriam.getCVTerm().addResource(resource);
                    }
                    break;
                }
            }
            if (!termFound) {
                obj.addCVTerm(miriam.getCVTerm());
            }
        }
        parser.close();
    }
}
