package org.anduril.javatools.pathway;

import java.util.List;

import org.sbml.libsbml.CVTerm;
import org.sbml.libsbml.libsbmlConstants;

public class MiriamEntry {
    public static final String MIRIAM_URI_BIOLOGICAL = "http://biomodels.net/biological-qualifiers/";
    public static final String MIRIAM_URI_MODEL = "http://biomodels.net/model-qualifiers/";
    public static final String MIRIAM_URI_UNKNOWN = "urn:unknown";
    
    private CVTerm term;
    
    public MiriamEntry(CVTerm term) {
        if (term == null) {
            throw new NullPointerException("term is null");
        }
        this.term = term;
    }
    
    public MiriamEntry(String namespace, String qualifier, List<String> resources) {
        this(new CVTerm());
        if (resources != null) { 
            for (String resource: resources) term.addResource(resource);
        }
        
        if (namespace.startsWith(MIRIAM_URI_BIOLOGICAL)) {
            term.setQualifierType(libsbmlConstants.BIOLOGICAL_QUALIFIER);
            int type;
            if (qualifier.equals("encodes")) type = libsbmlConstants.BQB_ENCODES;
            else if (qualifier.equals("hasPart")) type = libsbmlConstants.BQB_HAS_PART;
            else if (qualifier.equals("hasVersion")) type = libsbmlConstants.BQB_HAS_VERSION;
            else if (qualifier.equals("is")) type = libsbmlConstants.BQB_IS;
            else if (qualifier.equals("isDescribedBy")) type = libsbmlConstants.BQB_IS_DESCRIBED_BY;
            else if (qualifier.equals("isEncodedBy")) type = libsbmlConstants.BQB_IS_ENCODED_BY;
            else if (qualifier.equals("isHomologTo")) type = libsbmlConstants.BQB_IS_HOMOLOG_TO;
            else if (qualifier.equals("isPartOf")) type = libsbmlConstants.BQB_IS_PART_OF;
            else if (qualifier.equals("isVersionOf")) type = libsbmlConstants.BQB_IS_VERSION_OF;
            else if (qualifier.equals("occursIn")) type = libsbmlConstants.BQB_OCCURS_IN;
            else type = libsbmlConstants.BQB_UNKNOWN;
            term.setBiologicalQualifierType(type);
        } else if (namespace.startsWith(MIRIAM_URI_MODEL)) {
            term.setQualifierType(libsbmlConstants.MODEL_QUALIFIER);
            int type;
            if (qualifier.equals("is")) type = libsbmlConstants.BQM_IS;
            else if (qualifier.equals("isDescribedBy")) type = libsbmlConstants.BQM_IS_DESCRIBED_BY;
            else type = libsbmlConstants.BQM_UNKNOWN;
            term.setModelQualifierType(type);
        } else {
            term.setQualifierType(libsbmlConstants.UNKNOWN_QUALIFIER);
        }
    }
    
    public MiriamEntry(String namespace, String qualifier) {
        this(namespace, qualifier, null);
    }
    
    public CVTerm getCVTerm() {
        return this.term;
    }
    
    public String getQualifier() {
        switch (term.getQualifierType()) {
        case libsbmlConstants.BIOLOGICAL_QUALIFIER:
            switch (term.getBiologicalQualifierType()) {
            case libsbmlConstants.BQB_ENCODES:
                return "encodes";
            case libsbmlConstants.BQB_HAS_PART:
                return "hasPart";
            case libsbmlConstants.BQB_HAS_VERSION:
                return "hasVersion";
            case libsbmlConstants.BQB_IS:
                return "is";
            case libsbmlConstants.BQB_IS_DESCRIBED_BY:
                return "isDescribedBy";
            case libsbmlConstants.BQB_IS_ENCODED_BY:
                return "isEncodedBy";
            case libsbmlConstants.BQB_IS_HOMOLOG_TO:
                return "isHomologTo";
            case libsbmlConstants.BQB_IS_PART_OF:
                return "isPartOf";
            case libsbmlConstants.BQB_IS_VERSION_OF:
                return "isVersionOf";
            case libsbmlConstants.BQB_OCCURS_IN:
                return "occursIn";
            default:
                return "unknown";
            }
        case libsbmlConstants.MODEL_QUALIFIER:
            switch (term.getModelQualifierType()) {
            case libsbmlConstants.BQM_IS:
                return "is";
            case libsbmlConstants.BQM_IS_DESCRIBED_BY:
                return "isDescribedBy";
            default:
                return "unknown";
            }
        default:
            return "unknown";
        }
    }
    
    public String getNamespaceURI() {
        switch (term.getQualifierType()) {
        case libsbmlConstants.BIOLOGICAL_QUALIFIER:
            return MIRIAM_URI_BIOLOGICAL;
        case libsbmlConstants.MODEL_QUALIFIER:
            return MIRIAM_URI_MODEL;
        default:
            return MIRIAM_URI_UNKNOWN;
        }
    }
    
    public boolean typeEquals(MiriamEntry other) {
        final CVTerm t1 = this.getCVTerm();
        final CVTerm t2 = other.getCVTerm();
        return t1.getQualifierType() == t2.getQualifierType()
                && t1.getBiologicalQualifierType() == t2.getBiologicalQualifierType()
                && t1.getModelQualifierType() == t2.getModelQualifierType();
    }
    
    public boolean containsResource(String resource) {
        for (int i=0; i<term.getNumResources(); i++) {
            if (term.getResourceURI(i).equals(resource)) {
                return true;
            }
        }
        return false;
    }
    
    public long getNumResources() {
        return term.getNumResources();
    }
    
    public String getResource(long i) {
        return term.getResourceURI(i);
    }

    public String getResourcePrefix(long i) {
        String[] tokens = splitResource(term.getResourceURI(i));
        return tokens[0];
    }

    public String getResourceSeparator(long i) {
        String[] tokens = splitResource(term.getResourceURI(i));
        return tokens[1];
    }

    public String getResourceHead(long i) {
        String[] tokens = splitResource(term.getResourceURI(i));
        return tokens[0] + tokens[1];
    }
    
    public String getResourcePostfix(long i) {
        String[] tokens = splitResource(term.getResourceURI(i));
        return tokens[2];
    }
    
    public static String[] splitResource(String resource) {
        if (resource == null || resource.isEmpty()) {
            return new String[] {"", "", ""};
        }
        
        if (resource.toLowerCase().startsWith("urn:")) {
            /* Format: urn:xxx:yyy:ID */
            int pos = resource.lastIndexOf(':');
            if (pos < 0) {
                return new String[] {"", "", resource};
            }
            String prefix = resource.substring(0, pos);
            String postfix = resource.substring(pos+1, resource.length());
            return new String[] {prefix, ":", postfix};
        } else if (resource.toLowerCase().startsWith("http:")) {
            /* Format: http://xxx.yyy#ID */
            int pos = resource.indexOf('#');
            if (pos < 0) {
                return new String[] {"", "", resource};
            }
            String prefix = resource.substring(0, pos);
            String postfix = resource.substring(pos+1, resource.length());
            return new String[] {prefix, "#", postfix};
        } else {
            return new String[] {"", "", resource};
        }
    }

}
