package org.anduril.javatools.pathway;

import org.sbml.libsbml.Reaction;

public class SBMLReactionVertex implements SBMLVertex {
    private Reaction reaction;
    
    public SBMLReactionVertex(Reaction reaction) {
        this.reaction = reaction;
    }
    
    public String toString() {
        return this.reaction.getId();
    }
    
    public Reaction getReaction() {
        return this.reaction;
    }
    
    public String getId() {
        return this.reaction.getId();
    }
    
    public String getAttribute(String name) {
        if (name.equals("fontsize")) {
            return "48";
        } else if (name.equals("label")) {
            return " ";
//            return SBMLTools.formulaToString(this.reaction.getKineticLaw().getMath(), "\\n", 14);
//            return this.reaction.getName();
        } else if (name.equals("shape")) {
            return "box";
        } else {
            return null;
        }
    }
    
    public boolean isSpecies() {
        return false;
    }
    
    public void visit(SBMLVisitor visitor) {
        visitor.visitReaction(this);
    }
}
