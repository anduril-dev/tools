package org.anduril.javatools.pathway;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.sbml.libsbml.ASTNode;
import org.sbml.libsbml.Model;
import org.sbml.libsbml.Parameter;
import org.sbml.libsbml.Reaction;
import org.sbml.libsbml.SBMLDocument;
import org.sbml.libsbml.SBase;
import org.sbml.libsbml.Species;
import org.sbml.libsbml.SpeciesReference;
import org.sbml.libsbml.libsbml;
import org.sbml.libsbml.libsbmlConstants;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;

import edu.uci.ics.jung.algorithms.shortestpath.DijkstraDistance;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;

public class SBML {
    private SBMLDocument sbml;
    private DirectedGraph<SBMLVertex, SBMLEdge> jungGraph = null;
    private File sourceFile;
    
    public SBML(SBMLDocument sbml) {
        if (sbml == null) throw new NullPointerException("sbml is null");
        this.sbml = sbml;
    }
    
    public SBML(File sbmlFile) {
        this(libsbml.readSBML(sbmlFile.getAbsolutePath()));
        this.sourceFile = sbmlFile;
    }
    
    public SBMLDocument getDocument() {
        return this.sbml;
    }
    
    public Model getModel() {
        return this.sbml.getModel();
    }
    
    public String getLabel() {
        if (sbml.getName() != null && sbml.getName().length() > 0) {
            return sbml.getName();
        } else if (sbml.getId() != null && sbml.getId().length() > 0) {
            return sbml.getId();
        } else if (sourceFile != null) {
            return sourceFile.getName();
        } else {
            return "";
        }
    }
    
    public Iterable<Species> iterSpecies() {
        class SpeciesIterator implements Iterator<Species> {
            private Model model;
            private int pos;
            
            public SpeciesIterator(Model model) {
                this.model = model;
                this.pos = -1;
            }

            public boolean hasNext() {
                return this.pos+1 < this.model.getNumSpecies();
            }

            public Species next() {
                this.pos++;
                return model.getSpecies(this.pos);
            }

            public void remove() {}
        }
        
        class SpeciesIterable implements Iterable<Species> {
            private Model model;
            
            public SpeciesIterable(Model model) {
                this.model = model;
            }
            
            public Iterator<Species> iterator() {
                return new SpeciesIterator(model);
            }
        }
        
        return new SpeciesIterable(sbml.getModel());
    }
    
    public Map<Species,List<Reaction>> getSpeciesReactionMap() {
        Map<Species,List<Reaction>> map = new HashMap<Species, List<Reaction>>();
        final Model model = sbml.getModel();
        
        for (int i=0; i<model.getNumSpecies(); i++) {
            map.put(model.getSpecies(i), new ArrayList<Reaction>());
        }
        
        for (int i=0; i<model.getNumReactions(); i++) {
            Reaction reaction = model.getReaction(i);
            
            for (Species species: SBMLTools.getReactionSpecies(sbml, reaction, true)) {
                List<Reaction> reactions = map.get(species);
                if (!reactions.contains(reaction)) reactions.add(reaction);
            }
        }
        
        return map;
    }
    
    public Map<Parameter,List<Reaction>> getParameterReactionMap() {
        Map<Parameter,List<Reaction>> map = new HashMap<Parameter, List<Reaction>>();
        final Model model = sbml.getModel();
        
        for (int i=0; i<model.getNumParameters(); i++) {
            map.put(model.getParameter(i), new ArrayList<Reaction>());
        }
        
        for (int i=0; i<model.getNumReactions(); i++) {
            Reaction reaction = model.getReaction(i);
            if (reaction.getKineticLaw() != null) {
                List<String> ids = SBMLTools.getReferences(reaction.getKineticLaw().getMath(), null);
                for (String id: ids) {
                    Parameter param = model.getParameter(id);
                    if (param != null) {
                        List<Reaction> reactions = map.get(param);
                        if (!reactions.contains(reaction)) reactions.add(reaction);
                    }
                }
            }
        }
        
        return map;
    }
    
    public Map<Species,Integer> getSpeciesIndexMap() {
        Map<Species,Integer> map = new HashMap<Species, Integer>();
        for (int i=0; i<sbml.getModel().getNumSpecies(); i++) {
            map.put(sbml.getModel().getSpecies(i), i);
        }
        return map;
    }
    
    public DirectedGraph<SBMLVertex, SBMLEdge> asJUNG() {
        if (this.jungGraph != null) return this.jungGraph;
        
        this.jungGraph = new DirectedSparseGraph<SBMLVertex, SBMLEdge>();
        
        Map<String,SBMLVertex> vertexMap = new HashMap<String, SBMLVertex>();
        for (int i=0; i<sbml.getModel().getNumSpecies(); i++) {
            Species species = sbml.getModel().getSpecies(i);
            SBMLVertex vertex = new SBMLSpeciesVertex(species);
            jungGraph.addVertex(vertex);
            vertexMap.put(species.getId(), vertex);
        }
        
        for (int reactionCount=0; reactionCount<sbml.getModel().getNumReactions(); reactionCount++) {
            Reaction reaction = sbml.getModel().getReaction(reactionCount);
            SBMLVertex vertex = new SBMLReactionVertex(reaction);
            jungGraph.addVertex(vertex);
            
            for (int reactantCount=0; reactantCount<reaction.getNumReactants(); reactantCount++) {
                final String species = reaction.getReactant(reactantCount).getSpecies();
                if (!vertexMap.containsKey(species)) continue;
                final SBMLVertex from = vertexMap.get(species);
                jungGraph.addEdge(new SBMLEdge(from, vertex), from, vertex);
                /*
                if (reaction.getReversible()) {
                    jungGraph.addEdge(new SBMLEdge(vertex, from), vertex, from);
                }
                */
            }
            
            for (int productCount=0; productCount<reaction.getNumProducts(); productCount++) {
                final String species = reaction.getProduct(productCount).getSpecies();
                if (!vertexMap.containsKey(species)) continue;
                final SBMLVertex to = vertexMap.get(species);
                jungGraph.addEdge(new SBMLEdge(vertex, to), vertex, to);
                /*
                if (reaction.getReversible()) {
                    jungGraph.addEdge(new SBMLEdge(to, vertex), to, vertex);
                }
                */
            }
            
            for (int modifierCount=0; modifierCount<reaction.getNumModifiers(); modifierCount++) {
                final String species = reaction.getModifier(modifierCount).getSpecies();
                if (!vertexMap.containsKey(species)) continue;
                final SBMLVertex from = vertexMap.get(species);
                jungGraph.addEdge(new SBMLEdge(from, vertex, true), from, vertex);
            }
        }
        
        return this.jungGraph;
    }
    
    public DirectedGraph<SBMLVertex, SBMLEdge> getSubGraph(List<String> nodes) {
        DirectedGraph<SBMLVertex, SBMLEdge> graph = asJUNG();
        DirectedGraph<SBMLVertex, SBMLEdge> subGraph = new DirectedSparseGraph<SBMLVertex, SBMLEdge>();
        
        for (SBMLVertex v: graph.getVertices()) subGraph.addVertex(v);
        for (SBMLEdge e: graph.getEdges()) subGraph.addEdge(e, e.getFrom(), e.getTo());
        
        for (SBMLVertex v: new HashSet<SBMLVertex>(subGraph.getVertices())) {
            if (!nodes.contains(v.getId())) {
                for (SBMLVertex from: subGraph.getPredecessors(v)) {
                    for (SBMLVertex to: subGraph.getSuccessors(v)) {
                        if (!from.equals(to)) {
                            subGraph.addEdge(new SBMLEdge(from, to), from, to);
                        }
                    }
                }
                subGraph.removeVertex(v);
            }
        }
        
        DijkstraDistance<SBMLVertex, SBMLEdge> dist = new DijkstraDistance<SBMLVertex, SBMLEdge>(graph);

        for (SBMLVertex from: subGraph.getVertices()) {
            for (SBMLVertex to: subGraph.getSuccessors(from)) {
                Number d = dist.getDistance(from, to);
                if (d != null) {
//                    double doub = d.doubleValue();
                    SBMLEdge edge = subGraph.findEdge(from, to);
                    edge.setWeight(d.doubleValue()/2);
                }
            }
        }
        
        /*
        DijkstraDistance<SBMLVertex, SBMLEdge> dist = new DijkstraDistance<SBMLVertex, SBMLEdge>(graph);
        
        for (SBMLVertex vertex: graph.getVertices()) {
            if (vertex.isSpecies()) {
                SBMLSpeciesVertex sv = (SBMLSpeciesVertex)vertex;
                if (nodes.contains(sv.getId())) {
                    subGraph.addVertex(sv);
                }
            }
        }
        
        for (SBMLVertex v1: subGraph.getVertices()) {
            for (SBMLVertex v2: subGraph.getVertices()) {
                if (v1.equals(v2)) continue;
                Number d = dist.getDistance(v1, v2);
                if (d != null) {
//                    double doub = d.doubleValue();
                    subGraph.addEdge(new SBMLEdge(v1, v2), v1, v2);
                }
            }
        }
        */
        
        return subGraph;
    }
    
    private List<Species> getSourceSinkSpecies(boolean source) {
        DirectedGraph<SBMLVertex, SBMLEdge> graph = asJUNG();
        List<Species> speciesList = new ArrayList<Species>();
        
        for (SBMLVertex vertex: graph.getVertices()) {
            if (!vertex.isSpecies()) continue;
            int count;
            if (source) count = graph.getInEdges(vertex).size();
            else count = graph.getOutEdges(vertex).size();
            if (count == 0) {
                Species species = getModel().getSpecies(vertex.getId());
                if (species != null) speciesList.add(species);
            }
        }
        
        return speciesList;
    }
    
    public List<Species> getSourceSpecies() {
        return getSourceSinkSpecies(true);
    }
    
    public List<Species> getSinkSpecies() {
        return getSourceSinkSpecies(false);
    }
    
    public Map<Species, Integer> computeSizes(Map<Species, Integer> userMap) throws ArithmeticException {
        if (userMap == null) userMap = new HashMap<Species, Integer>();
        final List<Species> sources = getSourceSpecies();
        
        int rows = (int)(sbml.getModel().getNumReactions()+sources.size()+userMap.size());
        DoubleMatrix2D matrixA = new DenseDoubleMatrix2D(rows, (int)getModel().getNumSpecies());
        DoubleMatrix2D vectorB = new DenseDoubleMatrix2D(rows, 1);
        Map<Species,Integer> indexMap = getSpeciesIndexMap();
        
        for (int i=0; i<getModel().getNumReactions(); i++) {
            Reaction reaction = getModel().getReaction(i);
            
            for (int j=0; j<reaction.getNumReactants(); j++) {
                SpeciesReference ref = reaction.getReactant(j);
                int spNum = indexMap.get(getModel().getSpecies(ref.getSpecies()));
                double old = matrixA.get(i, spNum);
                matrixA.set(i, spNum, old-ref.getStoichiometry());
            }
            
            for (int j=0; j<reaction.getNumProducts(); j++) {
                SpeciesReference ref = reaction.getProduct(j);
                int spNum = indexMap.get(getModel().getSpecies(ref.getSpecies()));
                double old = matrixA.get(i, spNum);
                matrixA.set(i, spNum, old+ref.getStoichiometry());
            }
            
            vectorB.set(i, 0, 0);
        }
        
        int offset = (int)getModel().getNumReactions();
        for (int i=0; i<sources.size(); i++) {
            if (userMap.containsKey(sources.get(i))) continue;
            int spNum = indexMap.get(sources.get(i));
            matrixA.set(offset+i, spNum, 1);
            vectorB.set(offset+i, 0, 1);
        }

        offset = (int)getModel().getNumReactions()+sources.size();
        int count = 0;
        for (Map.Entry<Species, Integer> entry: userMap.entrySet()) {
            int spNum = indexMap.get(entry.getKey());
            matrixA.set(offset+count, spNum, 1);
            vectorB.set(offset+count, 0, entry.getValue());
            count++;
        }

        int rank = new Algebra().rank(matrixA);
        if (rank < getModel().getNumSpecies()) {
            String msg = String.format(
                    "Can not determine complex sizes: missing %d initial values (userMap)",
                    getModel().getNumSpecies()-rank);
            throw new ArithmeticException(msg);
        }
        
        DoubleMatrix2D vectorX = new Algebra().solve(matrixA, vectorB);
        Map<Species, Integer> map = new HashMap<Species, Integer>();
        for (int i=0; i<getModel().getNumSpecies(); i++) {
            double size = vectorX.get(i, 0);
            int rounded = (int)Math.round(size);
            double fract = Math.abs(rounded - size);
            if (size < 0 || fract > 0.1) {
                String msg = String.format(
                        "Can not determine complex sizes: size of %s is invalid: %f",
                        getModel().getSpecies(i).getId(), size);
                throw new ArithmeticException(msg);
            }
            map.put(getModel().getSpecies(i), rounded);
        }
        return map;
    }
    
    public SBase getSBase(String id) {
        SBase obj;
        
        obj = getModel().getSpecies(id);
        if (obj != null) return obj;

        obj = getModel().getReaction(id);
        if (obj != null) return obj;

        obj = getModel().getParameter(id);
        if (obj != null) return obj;
        
        obj = getModel().getCompartment(id);
        if (obj != null) return obj;
        
        return null;
    }
    
    public String formulaToHTML(ASTNode node) {
        StringBuffer sb = new StringBuffer();
        final int type = node.getType();
        final boolean isBinaryLogical = type==libsbmlConstants.AST_LOGICAL_AND
            || type==libsbmlConstants.AST_LOGICAL_OR
            || type==libsbmlConstants.AST_LOGICAL_XOR;
        
        if (type==libsbmlConstants.AST_LOGICAL_NOT || node.isUMinus()) {
            /* Unary prefix operator */
            if (node.isUMinus()) sb.append("-");
            else sb.append("not ");
            sb.append(formulaToHTML(node.getLeftChild()));
        } else if (node.isOperator() || isBinaryLogical) {
            /* Binary operator */
            sb.append(formulaToHTML(node.getLeftChild()));
            if (node.isOperator()) {
                sb.append(node.getCharacter());
            } else if (type==libsbmlConstants.AST_LOGICAL_AND) {
                sb.append(" and ");
            } else if (type==libsbmlConstants.AST_LOGICAL_OR) {
                sb.append(" or ");
            } else if (type==libsbmlConstants.AST_LOGICAL_AND) {
                sb.append(" xor ");
            } else {
                sb.append(" UNKNOWN-OPERATOR ");
            }
            sb.append(formulaToHTML(node.getRightChild()));
        } else if (node.isFunction()) {
            sb.append(node.getName()+"(");
            for (int i=0; i<node.getNumChildren(); i++) {
                if (i>0) sb.append(", ");
                sb.append(formulaToHTML(node.getChild(i)));
            }
            sb.append(")");
        } else if (node.isNumber()) {
            double value;
            if (node.isInteger()) value = node.getInteger();
            else if (node.isRational()) value = ((double)node.getNumerator()) / node.getDenominator();
            else value = node.getReal();
            sb.append(value);
        } else if (node.isName()) {
            final String name = node.getName();
            SBase obj = getSBase(name);
            if (obj == null) sb.append(name);
            else sb.append(SBML2HTML.formatLink(obj));
        } else {
            /* Unknown type */
            sb.append("UNKNOWN ");
            for (int i=0; i<node.getNumChildren(); i++) {
                sb.append(formulaToHTML(node.getChild(i)));
                sb.append(" ");
            }
        }
        
        return sb.toString();
    }
    
    public void write(File outFile) throws IOException {
        int status = libsbml.writeSBML(this.sbml, outFile.getAbsolutePath());
        if (status == 0) {
            throw new IOException("Could not write SBML to "+outFile.getAbsolutePath());
        }
    }
    
}
