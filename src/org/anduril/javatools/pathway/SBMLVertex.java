package org.anduril.javatools.pathway;

public interface SBMLVertex  {
    public static final String[] VERTEX_ATTRS = {"fontsize", "label", "shape"};    
    
    public String getId();
    public String getAttribute(String name);
    public boolean isSpecies();
    public void visit(SBMLVisitor visitor);
}
