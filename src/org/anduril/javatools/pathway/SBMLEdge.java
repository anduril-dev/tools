package org.anduril.javatools.pathway;

public class SBMLEdge {
    public static final String[] EDGE_ATTRS = {"label", "style"};
    
    private SBMLVertex from;
    private SBMLVertex to;
    private boolean modifier;
    private Double weight;
    
    public SBMLEdge(SBMLVertex from, SBMLVertex to, boolean modifier) {
        this.from = from;
        this.to = to;
        this.modifier = modifier;
        this.weight = null;
    }
    
    public SBMLEdge(SBMLVertex from, SBMLVertex to) {
        this(from, to, false);
    }
    
    public String toString() {
        return String.format("%s-%s", from.getId(), to.getId());
    }
    
    public SBMLVertex getFrom() {
        return this.from;
    }

    public SBMLVertex getTo() {
        return this.to;
    }
    
    public boolean isModifier() {
        return this.modifier;
    }
    
    public Double getWeight() {
        return this.weight;
    }
    
    public void setWeight(Double weight) {
        this.weight = weight;
    }
    
    public String getAttribute(String name) {
        if (name.equals("label")) {
            if (this.weight != null) {
                return String.format("%.0f", this.weight);
            } else {
                return "";
            }
        }
        if (name.equals("style")) {
            return this.modifier ? "dotted" : "solid";
        } else {
            return null;
        }
    }
}
