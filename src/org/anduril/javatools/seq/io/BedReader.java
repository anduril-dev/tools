package org.anduril.javatools.seq.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.anduril.asser.io.CSVParser;
import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.DNARegion;
import org.anduril.javatools.utils.AbstractIterator;

public class BedReader extends AbstractIterator<BedReader.BedRecord> {
    public static final float DUPLICATE_MAP_LOAD_FACTOR = 0.85f;
    
    private ChromosomeSet chrSet;
    private CSVParser parser;
    private String name;
    private DNARegion[] regions;
    private Pattern chrPattern;
    private int elongate;
    private boolean addChromosome;
    private int maxDuplicateReads;
    private HashMap<Long, Integer> duplicateCounts;
    private int duplicatesRemovedCount;
    
    public static class BedRecord extends DNARegion {
        public final String name;
        public final double score;
        
        public BedRecord(int chromosome, int start, int end,
                String name,  double score, int strand) {
            super(chromosome, start, end, strand);
            this.name = name;
            this.score = score;
        }
    }
    
    public BedReader(File file, ChromosomeSet chrSet, String chrPattern, DNARegion[] regions) throws IOException {
        this(new BufferedReader(new FileReader(file)),
                chrSet, chrPattern, regions);
        if (this.name == null) this.name = file.getName();
    }
    
    public BedReader(InputStream in, ChromosomeSet chrSet, String chrPattern, DNARegion[] regions) throws IOException {
        this(new BufferedReader(new InputStreamReader(in)),
                chrSet, chrPattern, regions);
    }

    private BedReader(BufferedReader reader, ChromosomeSet chrSet, String chrPattern, DNARegion[] regions) throws IOException {
        reader.mark(1024);
        String header = reader.readLine();
        int skip = header.startsWith("track") ? 1 : 0;
        for (String entry: header.split("[ \t]+")) {
            String[] tokens = entry.split("=", 2);
            if (tokens.length == 2 && tokens[0].trim().equals("name")) {
                this.name = tokens[1].trim();
            }
        }
        reader.reset();
        
        this.parser = new CSVParser(reader, CSVParser.DEFAULT_DELIMITER, skip,
                CSVParser.DEFAULT_MISSING_VALUE_SYMBOL, true, false);
        this.chrSet = chrSet;
        
        if (chrPattern != null) this.chrPattern = Pattern.compile(chrPattern);
        
        if (regions != null && regions.length == 0) regions = null;
        this.regions = regions;
        this.addChromosome = true;
        this.maxDuplicateReads = -1;
        this.duplicatesRemovedCount = 0;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getElongate() {
        return this.elongate;
    }
    
    public void setElongate(int basePairs) {
        this.elongate = basePairs;
    }
    
    public int getMaxDuplicateReads() {
        return this.maxDuplicateReads;
    }
    
    public void setMaxDuplicateReads(int maxRepeatReads) {
        this.maxDuplicateReads = maxRepeatReads;
        this.duplicateCounts = new HashMap<Long, Integer>(10000, DUPLICATE_MAP_LOAD_FACTOR);
    }
    
    public int getDuplicatesRemovedCount() {
        return this.duplicatesRemovedCount;
    }
    
    public boolean getAddChromosome() {
        return this.addChromosome;
    }
    
    public void setAddChromosome(boolean add) {
        this.addChromosome = add;
    }
    
    @Override
    protected BedRecord getNext() {
        while (this.parser.hasNext()) {
            String[] row = this.parser.next();
            if (row == null) return null;
            final int N = row.length;

            final String chromo = row[0];
            if (this.chrPattern != null && !this.chrPattern.matcher(chromo).matches()) {
                continue;
            }
            final int chromoID = this.chrSet.getChromosomeID(chromo, this.addChromosome, true);
            if (chromoID < 0) continue;
            
            int start = Integer.parseInt(row[1]);
            int end = Integer.parseInt(row[2]);

            final int strand;
            if (N < 6) strand = 0;
            else if (row[5].equals("+")) strand = 1;
            else if (row[5].equals("-")) strand = -1;
            else strand = 0;
            
            if (this.maxDuplicateReads > 0) {
                final Long regionID = Long.valueOf(getRegionID(start, end, strand, chromoID));
                Integer countObj = this.duplicateCounts.get(regionID);
                final int updatedCount;
                if (countObj == null) updatedCount = 1;
                else updatedCount = countObj.intValue() + 1;
                if (updatedCount > this.maxDuplicateReads) {
                    this.duplicatesRemovedCount++;
                    continue;
                }
                this.duplicateCounts.put(regionID, Integer.valueOf(updatedCount));
            }

            if (this.elongate != 0) {
                if (strand == 1) {
                    end += this.elongate;
                } else if (strand == -1) {
                    start -= this.elongate;
                    if (start < 0) start = 0;
                }
            }
            
            if (this.regions != null) {
                boolean found = false;
                for (DNARegion region: this.regions) {
                    if (region.contains(chromoID, start, end)) {
                        found = true;
                        break;
                    }
                }
                if (!found) continue;
            }

            final String name = (N < 4) ? null : row[3]; 
            final double score = (N < 5) ? 0 : Double.parseDouble(row[4]);

            BedRecord record = new BedRecord(chromoID, start, end, name, score, strand);
            return record;
        }
        
        this.duplicateCounts = null;
        return null;
    }
    
    private long getRegionID(int start, int end, int strand, int chromosome) {
        /*
         * chr: 15 bits = 49-63
         * strand: 1 bit = 48
         * start: 32 bits = 16-47
         * length: 16 bits = 0-15 (LSB)
         */
        if (strand < 0) strand = 0; 
        
        final long chromoL = (long)chromosome & 0x7fffL;
        final long strandL = (long)strand & 0x1L;
        final long startL = (long)start;
        final long lengthL = (long)(end - start) & 0xffffL;
        
        return chromoL << 49
            | strandL << 48
            | startL << 16
            | lengthL;
    }
    
    public void close() throws IOException {
        this.duplicateCounts = null;
        this.parser.close();
    }
}
