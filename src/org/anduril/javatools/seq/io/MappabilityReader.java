package org.anduril.javatools.seq.io;

import java.io.File;
import java.io.IOException;

import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.DNARegion;
import org.anduril.javatools.seq.FixedDenseBinner;
import org.anduril.javatools.seq.io.BedReader.BedRecord;
import org.anduril.javatools.utils.IOUtils;

public class MappabilityReader {
    private final ChromosomeSet chrSet;
    private final int binSize;
    private final int fragmentSize;
    
    public MappabilityReader(ChromosomeSet chrSet, int binSize, int fragmentSize) {
        this.chrSet = chrSet;
        this.binSize = binSize;
        this.fragmentSize = fragmentSize;
    }
    
    public FixedDenseBinner readMappability(File uniquePositionsBed) throws IOException {
        final FixedDenseBinner binner = new FixedDenseBinner(this.binSize, this.chrSet);
        
        BedReader bedReader = new BedReader(IOUtils.openGZIPFile(uniquePositionsBed),
                this.chrSet, null, null);
        
        bedReader.setAddChromosome(false);
        
        try {
            for (BedRecord region: bedReader) {
                final boolean forwardStrand = (region.strand == 1);
                for (int i=region.start; i<region.end; i++) {
                    if (forwardStrand) {
                        binner.addRegion(new DNARegion(region.chromosome,
                                i, i+this.fragmentSize));
                    } else {
                        binner.addRegion(new DNARegion(region.chromosome,
                                Math.max(0, i-this.fragmentSize), i));
                    }
                }
            }
        } finally {
            bedReader.close();
        }
        
        binner.multiply(1.0/binner.getMaximum());
        
        return binner;
    }
    
    public void writeWiggle(FixedDenseBinner mappabilityBinner) throws IOException {
        final String name = "mappability";
        System.out.println("track type=wiggle_0 visibility=full name="+name);
        for (int chrID=0; chrID<mappabilityBinner.getNumChromosomes(); chrID++) {
            double[] levels = mappabilityBinner.getLevels(chrID);
            if (levels == null) continue;

            int firstNonZero = -1;
            int lastNonZero = -1;
            for (int bin=0; bin<mappabilityBinner.getNumBins(chrID); bin++) {
                if (levels[bin] > 1e-7) {
                    if (firstNonZero < 0) firstNonZero = bin;
                    lastNonZero = bin;
                }
            }
            
            if (firstNonZero < 0) continue;
            
            System.out.println(String.format(
                    "fixedStep chrom=chr%s start=%d step=%d span=%d",
                    this.chrSet.getName(chrID, true),
                    mappabilityBinner.getStart(chrID, 0)+1,
                    this.binSize, this.binSize));
            for (int bin=firstNonZero; bin<lastNonZero; bin++) {
                System.out.println(String.format("%.3f", levels[bin]));
            }
        }
    }
}
