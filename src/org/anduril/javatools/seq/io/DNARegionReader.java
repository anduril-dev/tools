package org.anduril.javatools.seq.io;

import java.io.File;
import java.io.IOException;

import org.anduril.asser.io.CSVParser;
import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.NamedRegion;
import org.anduril.javatools.utils.AbstractIterator;

public class DNARegionReader extends AbstractIterator<NamedRegion> {
    private final CSVParser parser;
    private final ChromosomeSet chrSet;
    private final int chrIndex;
    private final int startIndex;
    private final int endIndex;
    private final int strandIndex;
    private final int idIndex;
    private final int nameIndex;
    private boolean addChromosomes;
    
    public DNARegionReader(File csv, ChromosomeSet chrSet,
            String chrColumn, String startColumn, String endColumn,
            String strandColumn, String idColumn, String nameColumn)
            throws IOException {
        this.parser = new CSVParser(csv);
        this.chrSet = chrSet;
        
        this.chrIndex = this.parser.getColumnIndex(chrColumn);
        this.startIndex = this.parser.getColumnIndex(startColumn);
        this.endIndex = this.parser.getColumnIndex(endColumn);
        this.strandIndex = this.parser.getColumnIndex(strandColumn);
        this.idIndex = this.parser.getColumnIndex(idColumn, false);
        this.nameIndex = this.parser.getColumnIndex(nameColumn, false);
        this.addChromosomes = true;
    }
    
    public boolean getAddChromosomes() {
        return this.addChromosomes;
    }
    
    public void setAddChromosome(boolean add) {
        this.addChromosomes = add;
    }

    @Override
    protected NamedRegion getNext() {
        while (this.parser.hasNext()) {
            String[] row = this.parser.next();
            if (row == null) return null;
            
            if (row[this.chrIndex] == null || row[this.startIndex] == null
                    || row[this.endIndex] == null) {
                continue;
            }

            final int chrID = this.chrSet.getChromosomeID(row[this.chrIndex],
                    this.addChromosomes, true);
            if (chrID < 0) continue;
            
            final int start = Integer.parseInt(row[this.startIndex]);
            final int end = Integer.parseInt(row[this.endIndex]);

            String strandStr = row[this.strandIndex];
            final int strand;
            if ("-1".equals(strandStr)) strand = -1;
            else if ("1".equals(strandStr)) strand = 1;
            else strand = 0;

            final String id = this.idIndex < 0 ? null : row[this.idIndex];
            final String name = this.nameIndex < 0 ? null : row[this.nameIndex];
            return new NamedRegion(chrID, start, end, strand, id, name);
        }
        
        return null;
    }

    public void close() {
        this.parser.close();
    }
}
