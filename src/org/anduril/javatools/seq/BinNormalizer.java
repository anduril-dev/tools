package org.anduril.javatools.seq;

import org.apache.commons.math.stat.StatUtils;

public class BinNormalizer {
    private final boolean[] referenceHasChr;
    private final double referenceLevel;
    
    public BinNormalizer(FixedDenseBinner reference) {
        this.referenceHasChr = new boolean[reference.getNumChromosomes()];
        for (int chr=0; chr<reference.getNumChromosomes(); chr++) {
            boolean present = reference.hasLevels(chr);
            this.referenceHasChr[chr] = present;
        }
        this.referenceLevel = computeMedian(reference);
    }
    
    public double normalize(FixedDenseBinner binner) {
        double binnerLevel = computeMedian(binner);
        if (binnerLevel == 0) return 1;
        double factor = this.referenceLevel / binnerLevel;
        binner.multiply(factor);
        return factor;
    }
    
    private double computeMedian(FixedDenseBinner binner) {
        int totalLength = 0;
        for (int chr=0; chr<binner.getNumChromosomes(); chr++) {
            if (referenceHasChr[chr] && binner.hasLevels(chr)) {
                totalLength += binner.getNumBins(chr);    
            }
        }
        
        double[] bins = new double[totalLength];
        
        int pos = 0;
        for (int chr=0; chr<binner.getNumChromosomes(); chr++) {
            if (referenceHasChr[chr] && binner.hasLevels(chr)) {
                System.arraycopy(binner.getLevels(chr), 0, bins, pos, binner.getNumBins(chr));
                pos += binner.getNumBins(chr);    
            }
        }
        
        return StatUtils.mean(bins);
    }
}
