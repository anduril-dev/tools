package org.anduril.javatools.seq;

public class NamedRegion extends DNARegion {
    public final String id;
    public final String name;
    
    public NamedRegion(int chromosome, int start, int end, int strand,
            String id, String name) {
        super(chromosome, start, end, strand);
        this.id = id;
        this.name = name;
    }
}
