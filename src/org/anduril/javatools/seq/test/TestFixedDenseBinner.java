package org.anduril.javatools.seq.test;

import org.junit.Assert;
import org.junit.Test;

import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.DNARegion;
import org.anduril.javatools.seq.FixedDenseBinner;

public class TestFixedDenseBinner {
    public static final int BIN_SIZE = 100;
    
    public FixedDenseBinner getBinner(int binSize) {
        ChromosomeSet chrSet = new ChromosomeSet();
        chrSet.addChromosome("chr0", 240);
        chrSet.addChromosome("chr1", 300);
        chrSet.addChromosome("chr2", 75);
        return new FixedDenseBinner(binSize, chrSet);
    }
    
    @Test
    public void testBinNumbers() {
        FixedDenseBinner binner = getBinner(BIN_SIZE);
        Assert.assertEquals(3, binner.getNumChromosomes());
        Assert.assertEquals(3, binner.getNumBins(0));
        Assert.assertEquals(4, binner.getNumBins(1));
        Assert.assertEquals(1, binner.getNumBins(2));
    }
    
    @Test
    public void testBoundaries() {
        FixedDenseBinner binner = getBinner(BIN_SIZE);
        int[][] CASES = new int[][] {
                // chromosome, bin, start, end, length
                {0, 0, 0, 100, 100},
                {0, 1, 100, 200, 100},
                {0, 2, 200, 240, 40},
                {1, 0, 0, 100, 100},
                {1, 1, 100, 200, 100},
                {1, 2, 200, 300, 100},
                {1, 3, 300, 300, 0},
                {2, 0, 0, 75, 75},
        };
        
        for (int[] tcase: CASES) {
            final int chr = tcase[0];
            final int bin = tcase[1];
            final int start = tcase[2];
            final int end = tcase[3];
            final int length = tcase[4];
            final String desc = String.format("Chr %d, bin %d: ", chr, bin);
            
            Assert.assertEquals(desc+"start", start, binner.getStart(chr, bin));
            Assert.assertEquals(desc+"end", end, binner.getEnd(chr, bin));
            Assert.assertEquals(desc+"size", length, binner.getSize(chr, bin));
        }
    }
    
    @Test
    public void testPositionSearch() {
        FixedDenseBinner binner = getBinner(BIN_SIZE);
        int[][] CASES = new int[][] {
                // chromosome, location, bin
                {0, 0, 0},
                {0, 99, 0},
                {0, -1, 0},
                {0, -200, 0},
                {0, 100, 1},
                {0, 150, 1},
                {0, 199, 1},
                {0, 200, 2},
                {0, 500, 2},
                {1, 299, 2},
                {1, 300, 3},
                {1, 500, 3},
                {2, 0, 0},
                {2, 50, 0},
                {2, 74, 0},
                {2, 75, 0},
                {2, 500, 0},
        };
        
        for (int[] tcase: CASES) {
            final int chr = tcase[0];
            final int position = tcase[1];
            final int bin = tcase[2];
            final String desc = String.format("chr%d, position %d", chr, position);
            Assert.assertEquals(desc, bin, binner.getBinIndex(chr, position));
        }
    }

    @Test
    public void testRegions() {
        int[][] CASES = new int[][] {
                // chromosome, start, end
                {0, 50, 50},   // TC1, zero length
                {0, 0, 240},   // TC2, whole chromosome
                {0, 20, 40},   // TC3, bin 0
                {0, 120, 140}, // TC4, bin 1
                {0, 210, 215}, // TC5, bin 2
                {0, 60, 130},  // TC6, bins 0..1
                {0, 90, 220},  // TC7, bins 0..2
                {0, 320, 350}, // TC8, pure overflow
                {0, 210, 350}, // TC9, bin 2+overflow
                {0, 0, 350},   // TC10, whole+overflow
                {0, 99, 100},  // TC11, boundary
                {0, 99, 101},  // TC12, boundary
                {0, 199, 200}, // TC13, boundary
                {0, 199, 201}, // TC14, boundary
                {1, 0, 300},   // TC15, whole chromosome
                {1, 0, 345},   // TC16, whole+overflow
                {1, 320, 345}, // TC17, pure overflow
                {1, 80, 260},  // TC18, bins 0..2
                {2, 0, 75},    // TC19, whole
                {2, 20, 35},   // TC20, partial
                {2, 120, 150}, // TC21, pure overflow
                {2, 60, 110},  // TC22, overflow
        };
        
        double[][] EXPECTED = new double[][] {
                {0, 0, 0},       // TC1
                {100, 100, 40},  // TC2
                {20, 0, 0},      // TC3
                {0, 20, 0},      // TC4
                {0, 0, 5},       // TC5
                {40, 30, 0},     // TC6
                {10, 100, 20},   // TC7
                {0, 0, 30},      // TC8
                {0, 0, 140},     // TC9
                {100, 100, 150}, // TC10
                {1, 0, 0},       // TC11
                {1, 1, 0},       // TC12
                {0, 1, 0},       // TC13
                {0, 1, 1},       // TC14
                {100, 100, 100, 0},  // TC15
                {100, 100, 100, 45}, // TC16
                {0, 0, 0, 25},       // TC17
                {20, 100, 60, 0},    // TC18
                {75},            // TC19
                {15},            // TC20
                {30},            // TC21
                {50},            // TC22
        };
        
        for (int tc=0; tc<CASES.length; tc++) {
            FixedDenseBinner binner = getBinner(BIN_SIZE);
            final int chr = CASES[tc][0];
            final int start = CASES[tc][1];
            final int end = CASES[tc][2];
            final String desc = String.format("TC%d, chr%d:%d-%d", tc+1, chr, start, end);
            binner.addRegion(new DNARegion(chr, start, end));
            Utils.assertArrayEquals(desc, EXPECTED[tc], binner.getLevels(chr));
        }
    }
}
