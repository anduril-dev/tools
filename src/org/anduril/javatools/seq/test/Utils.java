package org.anduril.javatools.seq.test;

import org.junit.Assert;

public class Utils {
    public static void assertArrayEquals(String message, double[] expected, double[] actual, double epsilon) {
        if (expected == null && actual == null) return;
        
        Assert.assertEquals(message+" Array lengths do not match",
                expected.length, actual.length);
        for (int i=0; i<expected.length; i++) {
            if (Math.abs(expected[i]-actual[i]) > epsilon) {
                String msg = String.format(
                        "%s Array difference at position %d: expected %.3f, got %.3f",
                        message, i, expected[i], actual[i]);
                Assert.fail(msg);
            }
        }
    }
    
    public static void assertArrayEquals(String message, double[] expected, double[] actual) {
        assertArrayEquals(message, expected, actual, 1e-6);
    }

    public static void assertArrayEquals(String message, double[][] expected, double[][] actual, double epsilon) {
        if (expected == null && actual == null) return;
        
        Assert.assertEquals(message+" Number of rows do not match",
                expected.length, actual.length);
        for (int row=0; row<expected.length; row++) {
            assertArrayEquals(message+" Row "+row, expected[row], actual[row], epsilon);
        }
    }
    
    public static void assertArrayEquals(String message, double[][] expected, double[][] actual) {
        assertArrayEquals(message, expected, actual, 1e-6);
    }
}
