package org.anduril.javatools.seq.test;

import org.junit.Assert;
import org.junit.Test;

import org.anduril.javatools.seq.ArrayTools;
import org.anduril.javatools.seq.pattern.model.Segment;
import org.anduril.javatools.seq.pattern.model.View;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunction;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunctionMean;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunctionMinMax;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunctionQuantile;

public class TestScoreFunction {
    public enum FunctionType { MAXIMUM, MINIMUM, MEAN, QUANTILE };
    
    private ScoreFunction makeSingleFunction(FunctionType type, double quantile) {
        switch(type) {
        case MAXIMUM:
            return new ScoreFunctionMinMax(false, null, 1);
        case MEAN:
            return new ScoreFunctionMean(null, 1);
        case MINIMUM:
            return new ScoreFunctionMinMax(true, null, 1);
        case QUANTILE:
            return new ScoreFunctionQuantile(quantile, null, 1);
        default:
            throw new IllegalArgumentException("Invalid function type: "+type);
        }
    }
    
    /**
     * Generate a score function of the specified type.
     * The function expression is as follows:
     * f( f(f(s1)), f(s3, s4), s1, s2 ). Here, f is
     * the function type and s1-s4 are segments.
     */
    private ScoreFunction getTestFunction(FunctionType type, double quantile) {
        View view = new View(0, "v1", null, "s1", null, View.NO_CHAIN_INDEX);
        Segment seg1 = new Segment(0, "s1", 'L', view, 0);
        Segment seg2 = new Segment(1, "s2", 'L', view, 1);
        Segment seg3 = new Segment(2, "s3", 'L', view, 2);
        Segment seg4 = new Segment(3, "s4", 'L', view, 3);
        
        ScoreFunction func = makeSingleFunction(type, quantile);
        ScoreFunction child1 = makeSingleFunction(type, quantile);
        ScoreFunction child1b = makeSingleFunction(type, quantile);
        ScoreFunction child2 = makeSingleFunction(type, quantile);
        func.addArgument(child1);
        func.addArgument(child2);
        func.addArgument(seg1);
        func.addArgument(seg2);
        child1.addArgument(child1b);
        child1b.addArgument(seg1);
        child2.addArgument(seg3);
        child2.addArgument(seg4);
        return func;
    }

    private void testGeneric(double[][] testCases, double[] expected, FunctionType type, double quantile) {
        ScoreFunction func = getTestFunction(type, quantile);
        for (int tc=0; tc<testCases.length; tc++) {
            double[] segmentScores = testCases[tc];
            double actual = func.evaluate(segmentScores);
            double exp = expected[tc];
            String msg = String.format("%s, TC%d: %s", type, tc+1,
                    ArrayTools.arrayToString(segmentScores));
            Assert.assertEquals(msg, exp, actual, 1e-5);
        }
    }
    
    @Test
    public void testMean() {
        final double[][] testCases = new double[][] {
            {0, 0, 0, 0, 100}, // TC1
            {1, 1, 1, 1, 100},
            {1, 2, 3, 4, 100},
            {0, 0, 4, 6, 100},
            {3, 0, 0, 0, 100},
            {0, 1, 0, 0, 100},
        };
        final double[] expected = new double[] {
                0, // TC1
                1,
                1.875,
                1.25,
                1.5,
                0.25,
        };
        testGeneric(testCases, expected, FunctionType.MEAN, -1);
    }

    @Test
    public void testQuantile() {
        final double[][] testCases = new double[][] {
            {0, 0, 0, 0, 100}, // TC1
            {1, 1, 1, 1, 100},
            {4, 2, 3, 1, 100},
            {0, 4, 4, 6, 100},
            {3, 0, 0, 0, 100},
            {0, 1, 0, 5, 100},
        };
        
        final double[] expected0 = new double[] {
                0, // TC1
                1,
                1,
                0,
                0,
                0,
        };
        testGeneric(testCases, expected0, FunctionType.QUANTILE, 0);
        
        final double[] expected50 = new double[] {
                0, // TC1
                1,
                3,
                2,
                1.5,
                0.5,
        };
        // median(s1, median(s3, s4), s1, s2)
        testGeneric(testCases, expected50, FunctionType.QUANTILE, 0.5);
        
        final double[] expected100 = new double[] {
                0, // TC1
                1,
                4,
                6,
                3,
                5,
        };
        testGeneric(testCases, expected100, FunctionType.QUANTILE, 1);
    }

    @Test
    public void testMin() {
        final double[][] testCases = new double[][] {
            {0, 0, 0, 0, -100}, // TC1
            {1, 1, 1, 1, -100},
            {1, 2, 0, 3, -100},
            {4, 5, 6, 2, -100},
            {10, 3, 10, 10, -100},
        };
        final double[] expected = new double[] {
                0, // TC1
                1,
                0,
                2,
                3,
        };
        testGeneric(testCases, expected, FunctionType.MINIMUM, -1);
    }

    @Test
    public void testMax() {
        final double[][] testCases = new double[][] {
            {0, 0, 0, 0, -100}, // TC1
            {1, 1, 1, 1, -100},
            {1, 2, 0, 3, -100},
            {4, 5, 6, 2, -100},
            {3, 10, 3, 3, -100},
        };
        final double[] expected = new double[] {
                0, // TC1
                1,
                3,
                6,
                10,
        };
        testGeneric(testCases, expected, FunctionType.MAXIMUM, -1);
    }

}
