package org.anduril.javatools.seq.test;

import junit.framework.Assert;

import org.junit.Test;

import org.anduril.javatools.seq.pattern.solver.WeightedRandomSelector;

public class TestWeightedRandomSelector {
    @Test
    public void testGeneratingSelected() {
        WeightedRandomSelector sel = new WeightedRandomSelector(5, null,
                new double[] {0, 0, 0, 0, 0}, null);
        sel.setWeight(1, 1);
        sel.addWeight(3, 1);
        final int rounds = 100;
        for (int i=0; i<rounds; i++) {
            final int element = sel.getRandomElement();
            if (element != 1 && element != 3) {
                String msg = String.format("Invalid element produced: %d", element);
                Assert.fail(msg);
            }
        }
    }
    
    @Test
    public void testGeneratingDistribution() {
        final int rounds = 1000;
        final int[] counts = new int[3];
        
        WeightedRandomSelector sel = new WeightedRandomSelector(3);
        for (int i=0; i<rounds; i++) {
            counts[sel.getRandomElement()]++;
        }
        
        /*
         * Probability of seeing less than 200 items of each
         * element is pbinom(200, 1000, 1/3) = 6.6e-21
         */
        
        for (int i=0; i<counts.length; i++) {
            if (counts[i] < 200) {
                String msg = String.format("Element %d: abnormally low count (%d)",
                        i, counts[i]);
                Assert.fail(msg);
            }
            
        }
    }
}
