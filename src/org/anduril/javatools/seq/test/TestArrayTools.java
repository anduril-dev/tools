package org.anduril.javatools.seq.test;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import org.anduril.javatools.seq.ArrayTools;

public class TestArrayTools {

    @Test
    public void testMedianFilterWindow5() {
        final int windowSize = 5;
        double[][] testCases = {
                {}, {}, // TC1
                {1}, {1}, // TC2
                {1, 0, 2}, {1, 1, 2}, // TC3
                {1, 0, 0, 2}, {1, 1, 1, 2}, // TC4
                {1, 0, 2, 0, 3}, {1, 1, 1, 2, 3}, // TC5
                {0, 2, 2, 5, 0, 4, 3}, // TC6: input
                {0, 2, 2, 2, 3, 3, 3}, // TC6: expected
        };
        
        for (int tc=0; tc<testCases.length/2; tc++) {
            double[] input = testCases[tc*2];
            double[] expected = testCases[tc*2+1];
            double[] actual = ArrayTools.medianFilter(input, windowSize);
            Utils.assertArrayEquals("TC "+(tc+1), expected, actual);
        }
    }
    
    @Test
    public void testReverse() {
        double[][] testCases = {
                {}, {}, // TC1
                {0}, {0},
                {0, 1}, {1, 0},
                {0, 1, 2}, {2, 1, 0},
                {2, 1, 2}, {2, 1, 2},
                {0, 1, 2, 3}, {3, 2, 1, 0},
                {0, 1, 2, 3, 4}, {4, 3, 2, 1, 0},
                {0, 1, 2, 3, 4, 5}, {5, 4, 3, 2, 1, 0},
        };
        
        for (int tc=0; tc<testCases.length/2; tc++) {
            final double[] input = testCases[tc*2];
            final double[] expected = testCases[tc*2+1];
            ArrayTools.reverse(input);
            Utils.assertArrayEquals("Array not properly reversed: TC"+(tc+1),
                    expected, input);
        }
    }
    
    @Test
    public void testRREF() {
        double[][][] testCases = {
                {}, // TC1: 0x0
                {{-2}}, // TC2: 1x1
                {{1, 0}, {0, 1}}, // TC3: 2x2, identity
                {{0, -2}, {5, 0}}, // TC4, 2x2, non-identity
                {{-2, 3}, {-4, 6}}, // TC5: 2x2, singular
                {{3, -6, 7}, {4, 5, 11}}, // TC6: 2x3, rank 2
                {{3, -6, 7}, {6, -12, 14}}, // TC7: 2x3, rank 1
                {{0, 0, 2}, {0, 3, -5}}, // TC8: 2x3, rank 2, leading 0-column
                {{-3, 6}, {-4, 11}, {0, 18}}, // TC9: 3x2, rank 2
                {{-3, 6}, {-6, 12}, {-9, 18}}, // TC10: 3x2, rank 1
                {{3, 0, 2}, {-4, 0, 7}, {2, 0, -1}}, // TC11: 3x3, rank 2, one 0-column
                {{0, 0, 2}, {0, 0, 7}, {0, 0, -1}}, // TC12: 3x3, rank 1, two 0-columns
                {{3, -5, 2}, {-4, 6, 7}, {2, -8, -1}}, // TC13: 3x3, rank 3
                {{1e-5, -2.5e-6, 5.9e-6}, {3.2e4, 1.7e-2, -1.7e4}}, // TC14: 2x3, rank 2, numeric
                {{1, -2, 4, 9, 11}, {0, 3, -7, -5, 4}}, // TC15: 2x5, rank 2
                {{0, -2, 4, 9, 11}, {0, 3, -7, -5, 4}}, // TC16: 2x5, rank 2, one 0-column
        };
        double[][][] expected = {
                {},
                {{1}},
                {{1, 0}, {0, 1}},
                {{1, 0}, {0, 1}},
                {{1, -1.5}, {0, 0}},
                {{1, 0, 101/39.0}, {0, 1, 5/39.0}},
                {{1, -2, 7/3.0}, {0, 0, 0}},
                {{0, 1, 0}, {0, 0, 1}},
                {{1, 0}, {0, 1}, {0, 0}},
                {{1, -2}, {0, 0}, {0, 0}},
                {{1, 0, 0}, {0, 0, 1}, {0, 0, 0}},
                {{0, 0, 1}, {0, 0, 0}, {0, 0, 0}},
                {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}},
                {{1, 0, -0.531248}, {0, 1, -4.48499}}, // TC14: Precise answer
                {{1, 0, -2/3.0, 17/3.0, 41/3.0}, {0, 1, -7/3.0, -5/3.0, 4/3.0}},
                {{0, 1, 0, -43/2.0, -93/2.0}, {0, 0, 1, -17/2.0, -41/2.0}},
        };
        
        for (int tc=0; tc<testCases.length; tc++) {
            final double[][] matrix = testCases[tc];
            final double[][] expMatrix = expected[tc];
            ArrayTools.rref(matrix);
            Utils.assertArrayEquals("TC "+(tc+1), expMatrix, matrix);
        }
    }
    
    @Test
    public void testShuffle1() {
        final double[] orig = {0, 0, 0, 1, 1, 1, 2, 2, 2};
        final int ROUNDS = 100;

        for (int rnd=0; rnd<ROUNDS; rnd++) {
            for (int segment=0; segment<3; segment++) {
                for (int len=0; len<=3; len++) {
                    double[] array = Arrays.copyOf(orig, orig.length);
                    ArrayTools.shuffle(array, segment*3, len);
                    String desc = String.format("Segment=%d, length=%d", segment, len);
                    Utils.assertArrayEquals(desc, orig, array);
                }
            }
        }
    }
    
    @Test
    public void testShuffle2() {
        final double[] orig = {0, 0, 1, 2, 0, 0};
        final int ROUNDS = 100;
        
        boolean changed = false;
        for (int rnd=0; rnd<ROUNDS; rnd++) {
            double[] array = Arrays.copyOf(orig, orig.length);
            ArrayTools.shuffle(array, 2, 2);
            if (array[2] == 2 && array[3] == 1) {
                changed = true;
                break;
            }
        }
        
        Assert.assertTrue("No elements were shuffled", changed);
    }
    
}
