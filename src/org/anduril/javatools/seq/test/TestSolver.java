package org.anduril.javatools.seq.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import org.anduril.javatools.seq.LocusTransformation;
import org.anduril.javatools.seq.pattern.model.LinearConstraint;
import org.anduril.javatools.seq.pattern.model.Model;
import org.anduril.javatools.seq.pattern.model.ModelInstance;
import org.anduril.javatools.seq.pattern.model.Pattern;
import org.anduril.javatools.seq.pattern.model.PatternInstance;
import org.anduril.javatools.seq.pattern.model.ScorerConfiguration;
import org.anduril.javatools.seq.pattern.model.Segment;
import org.anduril.javatools.seq.pattern.model.SegmentLength;
import org.anduril.javatools.seq.pattern.model.View;
import org.anduril.javatools.seq.pattern.model.LinearConstraint.Relation;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunction;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunctionMean;
import org.anduril.javatools.seq.pattern.solver.AbstractScorer;
import org.anduril.javatools.seq.pattern.solver.ArrayScorer;
import org.anduril.javatools.seq.pattern.solver.SASolver;
import org.anduril.javatools.seq.pattern.solver.SolverResult;

public class TestSolver {
    public static final int BIN_SIZE = 100;
    public static final double HIGH_VALUE = 100;
    public static final String SOURCE = "test";
    public static final String SCORER = "scorer";

    public static final TestCase[] TEST_CASES = new TestCase[] {
            new TestCase(0, 0, Segment.SEGMENT_LOW), // 0/0 low (TC1)
            new TestCase(0, 100, Segment.SEGMENT_LOW), // 100/100 low
            new TestCase(0, 100, Segment.SEGMENT_HIGH), // 0/100 high
            new TestCase(100, 50, Segment.SEGMENT_HIGH), // 50/50 high
            new TestCase(100, 100, Segment.SEGMENT_HIGH), // 90/100 high
            new TestCase(99, 1, Segment.SEGMENT_HIGH), // 0/1 high
            new TestCase(99, 2, Segment.SEGMENT_HIGH), // 1/2 high
            new TestCase(99, 3, Segment.SEGMENT_HIGH), // 2/3 high
            new TestCase(0, 300, Segment.SEGMENT_LOW), // 210/300 low
            new TestCase(0, 300, Segment.SEGMENT_HIGH), // 90/300 high
    };
    
    private static class TestCase {
        public final double offset;
        public final double length;
        public final char pattern;

        public TestCase(double offset, double length, char pattern) {
            this.offset = offset;
            this.length = length;
            this.pattern = pattern;
        }
        
        @Override
        public String toString() {
            return String.format("Offset: %.1f, length: %.1f, pattern: %c",
                    this.offset, this.length, this.pattern);
        }
    }
    
    private double[] getData() {
        double[] data = new double[300];
        for (int i=100; i<200; i++) data[i] = HIGH_VALUE;
        for (int i=150; i<160; i++) data[i] = 0;
        return data;
    }
    
    private ModelInstance getModelInstance() {
        ScorerConfiguration scorer = new ScorerConfiguration(0, SCORER, ArrayScorer.SCORER_TYPE);
        View view = new View(0, "v1", scorer, SOURCE, null, -1);
        Model model = new Model();
        model.addView(view);
        model.addScorer(scorer);
        Pattern pattern = new Pattern(null, null);
        pattern.addSegment(new Segment(0, "s1", Segment.SEGMENT_LOW, view, 0));
        pattern.addSegment(new Segment(1, "s2", Segment.SEGMENT_HIGH, view, 1));
        pattern.addSegment(new Segment(2, "s3", Segment.SEGMENT_LOW, view, 2));
        ScoreFunction func = new ScoreFunctionMean(null, 1);
        LinearConstraint lc = new LinearConstraint(Relation.EQ);
        lc.setRHS(new SegmentLength(0, 1, view));
        for (Segment segment: pattern.getSegments()) {
            segment.setLowBound(new SegmentLength(10*BIN_SIZE));
            func.addArgument(segment);
            lc.addSegment(segment, 1);
        }
        pattern.setScoreFunction(func);
        pattern.addConstraint(lc);
        
        model.addPattern(pattern);
        
        LocusTransformation start = new LocusTransformation(0, 1);
        ModelInstance modelInst = new ModelInstance(model, start, 300*BIN_SIZE);
        return modelInst;
    }

    private ArrayScorer getArrayScorer(ModelInstance mi) {
        Map<String, String> args = new HashMap<String, String>();
        ArrayScorer scorer = new ArrayScorer(0, mi, mi.getModel().getViews(), args, BIN_SIZE);
        scorer.setData(SOURCE, getData());
        return scorer;
    }

    private void testScorerGeneric(String label, AbstractScorer scorer, View view, double[] expected) {
        for (int tc=0; tc<expected.length; tc++) {
            TestCase testCase = TEST_CASES[tc];
            double actual = scorer.evaluateScore(view,
                    testCase.offset*BIN_SIZE, testCase.length*BIN_SIZE,
                    testCase.pattern);
            String msg = String.format("%s, TC%d: %s", label, tc+1, testCase);
            Assert.assertEquals(msg, expected[tc], actual, 1e-3);
        }
    }
    
    @Test
    public void testArrayThreshold() {
        ModelInstance mi = getModelInstance();
        View view = mi.getModel().getViews().get(0);
        ArrayScorer scorer = getArrayScorer(mi);
        
        scorer.setThreshold(HIGH_VALUE/2);
        double[] expected = new double[] {
                0,
                1, -1, 1, 0.8,
                -1, 0, 1/3.0,
                0.4, -0.4 };
        testScorerGeneric("Array/threshold, T=high/2", scorer, view, expected);
        
        scorer.setThreshold(HIGH_VALUE+1);
        expected = new double[] {
                0,
                1, -1, -1, -1,
                -1, -1, -1,
                1, -1 };
        testScorerGeneric("Array/threshold, T=high+1", scorer, view, expected);

        scorer.setThreshold(-1);
        expected = new double[] {
                0,
                -1, 1, 1, 1,
                1, 1, 1,
                -1, 1 };
        testScorerGeneric("Array/threshold, T=-1", scorer, view, expected);
    }
    
    @Test
    public void testSolverEvaluate() {
        ModelInstance mi = getModelInstance();
        ArrayScorer scorer = getArrayScorer(mi);
        scorer.setThreshold(HIGH_VALUE/2);
        
        Map<String, String> args = new HashMap<String, String>();
        args.put(Model.ARG_BIN_SIZE, "100");
        SASolver solver = new SASolver(mi, args);
        solver.setScorer(mi.getModel().getScorer(SCORER), scorer);
        PatternInstance pi = mi.getPatternInstances()[0];
        
        double[][] testCases = new double[][] {
                {100, 100, 100}, {(1+0.8+1)/3.0}, // TC1: 100/100, (90-10)/100, 100/100
                {10, 10, 10}, {(1-1+1)/3.0}, // TC2: 10/10, -10/10, 10/10
                {0, 0, 0}, {0}, // TC3
                {99, 1, 0}, {(1-1+0)/3.0}, // TC4
                {100, 1, 0}, {(1+1+0)/3.0}, // TC5
                {100, 50, 10}, {1}, // TC6: 100/100, 50/50, 10/10
                {80, 60, 40}, {(1+1/3.0-0.5)/3.0}, // TC7: 80/80, (40-20)/60, (10-30)/40
        };
        
        for (int tc=0; tc<testCases.length/2; tc++) {
            double[] solution = testCases[tc*2];
            for (int i=0; i<solution.length; i++) {
                solution[i] *= BIN_SIZE;
            }
            double score = solver.evaluate(pi, solution);
            String msg = String.format("TC%d", tc+1);
            Assert.assertEquals(msg, testCases[tc*2+1][0], score, 1e-5);
        }
    }
    
    @Test
    public void testSolverOptimize() {
        ModelInstance mi = getModelInstance();
        ArrayScorer scorer = getArrayScorer(mi);
        PatternInstance pi = mi.getPatternInstances()[0];

        final double optimal = 0.933333333333;
        final int maxRounds = 100;
        
        for (int restarts: new int[] {0, 3}) {
            for (String cooling: new String[] {"exponential", "adaptive"}) {
                Map<String, String> args = new HashMap<String, String>();
                args.put(Model.ARG_BIN_SIZE, ""+BIN_SIZE);
                args.put(SASolver.ARG_COOLING, cooling);
                args.put(SASolver.ARG_RESTARTS, ""+restarts);
                args.put(SASolver.ARG_ROUNDS, "1000");
                SASolver solver = new SASolver(mi, args);
                solver.setScorer(mi.getModel().getScorer(SCORER), scorer);

                boolean foundOptimal = false;
                for (int i=0; i<maxRounds; i++) {
                    SolverResult result = solver.optimize(pi);
                    if (Math.abs(result.score - optimal) < 0.01) {
                        foundOptimal = true;
                        break;
                    }
                }
                String tag = String.format("%s, %d restarts", cooling, restarts);
                Assert.assertTrue(tag+": could not find optimal solution", foundOptimal);
            }
        }
    }
}
