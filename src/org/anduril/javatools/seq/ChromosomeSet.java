package org.anduril.javatools.seq;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections.primitives.ArrayIntList;

import org.anduril.asser.io.CSVParser;

public final class ChromosomeSet {
    private final ArrayIntList lengths;
    private final ArrayList<String> names;
    private final Map<String, Integer> nameRevMap;
    private final Map<String, Integer> normalizedNameRevMap;
    private int nextID;
    
    public ChromosomeSet(int initialCapacity) {
        this.lengths = new ArrayIntList(initialCapacity);
        this.names = new ArrayList<String>(initialCapacity);
        this.nameRevMap = new ConcurrentHashMap<String, Integer>(initialCapacity);
        this.normalizedNameRevMap = new ConcurrentHashMap<String, Integer>(initialCapacity);
        this.nextID = 0;
    }
    
    public ChromosomeSet() {
        this(32);
    }
    
    public synchronized int addChromosome(String name, int size) {
        if (this.nameRevMap.containsKey(name)) {
            throw new IllegalArgumentException("Chromosome already exists: "+name);
        }
        if (name == null) {
            throw new NullPointerException("Chromosome name must not be null");
        }
        
        final int id = this.nextID;
        this.nextID++;
        this.lengths.add(size);
        this.names.add(name);
        this.normalizedNameRevMap.put(normalizeChromosome(name), id);
        this.nameRevMap.put(name, id);
        return id;
    }
    
    public boolean containsChromosome(String name) {
        return this.nameRevMap.containsKey(name);
    }
    
    public int getNumChromosomes() {
        return this.nextID;
    }
    
    public int getChromosomeID(String name, boolean add, boolean fuzzy) {
        Integer idObj = this.nameRevMap.get(name);
        if (idObj != null) return idObj.intValue();
        else {
            if (fuzzy) {
                int chrID = fuzzySearchChromosomeID(name);
                if (chrID >= 0) return chrID;
            }
            if (add) return addChromosome(name, 0);
            else return -1;
        }
    }
    
    public int getChromosomeID(String name) {
        return getChromosomeID(name, true, true);
    }
    
    private int fuzzySearchChromosomeID(String name) {
        Integer idObj = this.normalizedNameRevMap.get(normalizeChromosome(name));
        return idObj == null ? -1 : idObj.intValue();
    }
    
    public String normalizeChromosome(String name) {
        name = name.replaceFirst("^chr", "");
        name = name.replaceFirst("[.]fa(sta)?$", "");
        return name;
    }
    
    public String getName(int chromosomeID, boolean normalize) {
        if (chromosomeID < 0 || chromosomeID == DNARegion.SEQ_NONE) return null;
        try {
            final String name = this.names.get(chromosomeID);
            return normalize ? normalizeChromosome(name) : name;
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Chromosome does not exist: "+chromosomeID);
        }
    }
    
    public String getName(int chromosomeID) {
        return getName(chromosomeID, false);
    }

    public int getLength(int chromosomeID) {
        try {
            return this.lengths.get(chromosomeID);
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Chromosome does not exist: "+chromosomeID);
        }
    }
    
    public synchronized void setLength(int chromosomeID, int length) {
        if (chromosomeID >= this.nextID) {
            throw new IllegalArgumentException("Chromosome does not exist: "+chromosomeID);
        }
        this.lengths.set(chromosomeID, length);
    }
    
    public synchronized int readSizesFromCSV(File csvFile) throws IOException {
        CSVParser parser = new CSVParser(csvFile);
        try {
            int count = 0;
            for (String[] row: parser) {
                String name = row[0];
                int length = Integer.parseInt(row[1]);
                count++;
                setLength(getChromosomeID(name), length);
            }
            return count;
        } finally {
            parser.close();
        }
    }
}
