package org.anduril.javatools.seq.region;

import java.util.Iterator;

import org.anduril.javatools.utils.AbstractIterator;

public class ScoreFilter implements RegionReader {
    private final RegionReader reader;
    private final int scoreAnnotationID;
    private final int mergeGap;
    private final double minScore;
    private final double maxScore;
    
    private class ScoreIterator extends AbstractIterator<DNARegion> {
        private final Iterator<DNARegion> iter;
        private MutableDNARegion mergedRegion;
        private MutableDNARegion mergeExport;
        
        public ScoreIterator() {
            this.iter = reader.iterator();
            if (mergeGap >= 0) {
                this.mergedRegion = new MutableDNARegion(reader.getAnnotationRegistry());
                this.mergedRegion.setSequenceID(DNARegion.SEQ_NONE);
                this.mergeExport = new MutableDNARegion(reader.getAnnotationRegistry());
            } else {
                this.mergedRegion = null;
                this.mergeExport = null;
            }
        }
        
        private boolean mergeIsActive() {
            return this.mergedRegion != null
                && this.mergedRegion.getSequenceID() != DNARegion.SEQ_NONE;
        }
        
        @Override
        protected DNARegion getNext() {
            while (this.iter.hasNext()) {
                final DNARegion reg = this.iter.next();
                if (reg == null) return null;
                final double score = reg.getDoubleAnnotation(ScoreFilter.this.scoreAnnotationID);
                if (score < ScoreFilter.this.minScore || score > ScoreFilter.this.maxScore) {
                    continue;
                }
                
                if (mergeGap >= 0) {
                    final double mergeScore = this.mergedRegion.getDoubleAnnotation(scoreAnnotationID);
                    if (mergeIsActive() && score == mergeScore && this.mergedRegion.getDistanceGap(reg) <= mergeGap) {
                        this.mergedRegion.setRightPosition(reg.getRightPosition(false), false);
                        continue; // Continue merging
                    } else {
                        final boolean emitPrevious = mergeIsActive();
                        if (emitPrevious) {
                            this.mergeExport.replace(this.mergedRegion);
                            this.mergeExport.copyAnnotations(this.mergedRegion);
                        }
                        this.mergedRegion.replace(reg); // Begin new merge
                        this.mergedRegion.setDoubleAnnotation(scoreAnnotationID, score);
                        
                        if (emitPrevious) {
                            return this.mergeExport;
                        }
                        else continue;
                    }
                }
                
                return reg;
            }

            if (mergeIsActive()) {
                this.mergeExport.replace(this.mergedRegion);
                this.mergeExport.copyAnnotations(this.mergedRegion);
                this.mergedRegion.setSequenceID(DNARegion.SEQ_NONE);
                return this.mergeExport;
            }
            
            return null;
        }
        
        @Override
        public void remove() {
            this.iter.remove();
        }
    }
    
    public ScoreFilter(RegionReader reader, int mergeGap, double minScore, double maxScore) {
        this.reader = reader;
        this.scoreAnnotationID = reader.getAnnotationRegistry().getAnnotationID(AnnotationRegistry.ANNOTATION_SCORE);
        this.mergeGap = mergeGap;
        this.minScore = minScore;
        this.maxScore = maxScore;
        
        if (this.scoreAnnotationID < 0) {
            throw new IllegalArgumentException("Region source has no score annotation: "+reader);
        }
    }
    
    public ScoreFilter(RegionReader reader, int mergeGap) {
        this(reader, mergeGap, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    }
    
    @Override
    public Iterator<DNARegion> iterator() {
        return new ScoreIterator();
    }

    @Override
    public AnnotationRegistry getAnnotationRegistry() {
        return this.reader.getAnnotationRegistry();
    }

    @Override
    public String getRegionSetName() {
        return this.reader.getRegionSetName();
    }

    @Override
    public void close() {
        this.reader.close();
    }

}
