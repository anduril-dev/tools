package org.anduril.javatools.seq.region;

import java.util.Iterator;

public interface RegionWriter {
    public boolean add(DNARegion region);
    public void addAll(Iterator<DNARegion> iterator);
    public void close();
}
