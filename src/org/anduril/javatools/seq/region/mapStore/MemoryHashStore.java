package org.anduril.javatools.seq.region.mapStore;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.anduril.javatools.seq.region.AnnotationRegistry;
import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.RegionStore;

public class MemoryHashStore<T extends Map<DNARegion,DNARegion>> implements RegionStore {
    private final Map<DNARegion,DNARegion> store;
    private final AnnotationRegistry registry;
    private final String regionSetName;
    
    public MemoryHashStore(T storage, AnnotationRegistry registry, String regionSetName) {
        this.store = storage;
        this.registry = registry;
        this.regionSetName = regionSetName;
    }

    @Override
    public String toString() {
        return this.store.toString();
    }
    
    @Override
    public Iterator<DNARegion> iterator() {
        return this.store.keySet().iterator();
    }
    
    @Override
    public AnnotationRegistry getAnnotationRegistry() {
        return this.registry;
    }
    
    @Override
    public String getRegionSetName() {
        return this.regionSetName;
    }
    
    @Override
    public boolean add(DNARegion region) {
        final DNARegion nativeRegion = region.getImmutable(this.registry);
        return this.store.put(nativeRegion, nativeRegion) == null;
    }

    @Override
    public void addAll(Iterator<DNARegion> iterator) {
        while (iterator.hasNext()) {
            final DNARegion nativeRegion = iterator.next().getImmutable(this.registry);
            this.store.put(nativeRegion, nativeRegion);
        }
    }
    
    @Override
    public DNARegion get(DNARegion region) {
        return this.store.get(region);
    }
    
    @Override
    public boolean contains(DNARegion region) {
        return this.store.containsKey(region);
    }
    
    @Override
    public boolean containsLocation(DNARegion region) {
        throw new UnsupportedOperationException("Location query is not supported for hash stores");
    }
    
    @Override
    public Iterator<DNARegion> getRange(DNARegion region) {
        throw new UnsupportedOperationException("Range query is not supported for hash stores");
    }
    
    @Override
    public Iterator<DNARegion> getOverlapping(DNARegion query) {
        throw new UnsupportedOperationException("Overlap query is not supported for hash stores");
    }
    
    @Override
    public boolean remove(DNARegion region) {
        return this.store.remove(region) != null;
    }
    
    @Override
    public void removeAll(Iterator<DNARegion> iter) {
        while (iter.hasNext()) {
            this.store.remove(iter.next());
        }
    }

    @Override
    public void close() {
        this.store.clear();
    }

    public static MemoryHashStore<HashMap<DNARegion,DNARegion>> makeHashSet(AnnotationRegistry registry, String regionSetName) {
        return new MemoryHashStore<HashMap<DNARegion,DNARegion>>(new HashMap<DNARegion,DNARegion>(), registry, regionSetName);
    }
    
    public static MemoryHashStore<TreeMap<DNARegion,DNARegion>> makeTreeSet(AnnotationRegistry registry, String regionSetName) {
        return new MemoryHashStore<TreeMap<DNARegion,DNARegion>>(new TreeMap<DNARegion,DNARegion>(), registry, regionSetName);
    }
    
}
