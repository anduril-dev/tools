package org.anduril.javatools.seq.region;

public enum AnnotationType { DOUBLE, LONG, STRING }