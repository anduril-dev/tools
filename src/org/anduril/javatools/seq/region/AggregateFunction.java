package org.anduril.javatools.seq.region;

/**
 * Aggregate function combines scores from several regions
 * into one value. Aggregate values are computed incrementally:
 * first value is <code>initial()</code>, second is
 * <code>y1 = update(initial(), x1)</code>, third is
 * <code>y2 = update(y1, x2)</code>, etc. 
 */
public interface AggregateFunction {
    
    /**
     * Return the initial value.
     */
    public double initial();
    
    /**
     * Return an updated aggragate value.
     * @param previous Previous aggregate value, or initial
     *  if current element is first.
     * @param newElement Value of the new element.
     */
    public double update(double previous, double newElement);
    
    /* Standard aggregate functions */

    public static final Assign ASSIGN = new Assign();
    public static final Count COUNT = new Count();
    public static final First FIRST = new First();
    public static final LogSum LOGSUM = new LogSum();
    public static final Min MIN = new Min();
    public static final Max MAX = new Max();
    public static final Product PRODUCT = new Product();
    public static final Sum SUM = new Sum();
    
    public static final class Assign implements AggregateFunction {
        @Override
        public double initial() { return 0; }

        @Override
        public double update(double previous, double newElement) {
            return newElement;
        }
    }
    
    public static final class Count implements AggregateFunction {
        @Override
        public double initial() { return 0; }

        @Override
        public double update(double previous, double newElement) {
            return previous + 1;
        }
    }

    public static final class First implements AggregateFunction {
        private static final double INIT_VALUE = -Double.MIN_VALUE;
        
        @Override
        public double initial() { return INIT_VALUE; }

        @Override
        public double update(double previous, double newElement) {
            if (previous == INIT_VALUE) return newElement;
            else return previous;
        }
    }
    
    /**
     * Sum of negative natural logarithms, useful for values in range
     * (0, 1] such as p-values. Final value is -(log(x1) + ... + log(xN)).
     */
    public static final class LogSum implements AggregateFunction {
        /** Value used for log(0). */
        public static final double INF_VALUE = 1000;
        
        @Override
        public double initial() { return 0; }

        @Override
        public double update(double previous, double newElement) {
            if (newElement == 0) return previous + INF_VALUE;
            else return previous - Math.log(newElement);
        }
    }
    
    public static final class Mean implements AggregateFunction {
        private final double totalElements;
        
        public Mean(int totalElements) {
            if (totalElements < 1) {
                throw new IllegalArgumentException("totalElements must be >= 1");
            }
            this.totalElements = totalElements;
        }
        
        @Override
        public double initial() { return 0; }

        @Override
        public double update(double previous, double newElement) {
            return previous + (newElement / this.totalElements);
        }
    }
    
    public static final class Min implements AggregateFunction {
        @Override
        public double initial() { return 1e100; }

        @Override
        public double update(double previous, double newElement) {
            return Math.min(previous, newElement);
        }
    }
    
    public static final class Max implements AggregateFunction {
        @Override
        public double initial() { return -1e100; }

        @Override
        public double update(double previous, double newElement) {
            return Math.max(previous, newElement);
        }
    }

    public static final class Product implements AggregateFunction {
        @Override
        public double initial() { return 1; }

        @Override
        public double update(double previous, double newElement) {
            return previous * newElement;
        }
    }
    
    public static final class Sum implements AggregateFunction {
        @Override
        public double initial() { return 0; }

        @Override
        public double update(double previous, double newElement) {
            return previous + newElement;
        }
    }
}
