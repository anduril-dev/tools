package org.anduril.javatools.seq.region.unittest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

//import org.junit.Ignore;
import org.junit.Test;

import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.ImmutableDNARegion;
import org.anduril.javatools.seq.region.MutableDNARegion;
import org.anduril.javatools.seq.region.RegionStore;
import org.anduril.javatools.seq.region.Strand;

public abstract class RegionStoreTest <T extends RegionStore> {
    private enum OperationOrder { ASCENDING, DESCENDING, RANDOM };
    
    public static final int[] NUM_DUPLICATE_LOCATIONS = {1, 9};
    
    private static final int N = 10;
    private static final int STEP = 100;
    private static final int LENGTH = 300;
    private static final long KEY_HASH = 0x100;
    
    /**
     * Return a sorted list of test regions. Total number of
     * regions is 24+(N*2), where N=numRunningRegions.
     * Regions are as follows:
     * <pre>
     * # 16 regions
     * for seq in {0, 1}:
     *   for pos in {0, 1}:
     *     for len in {0, 1}:
     *       for strand in {-1, 1}:
     *         <seq, pos, len, strand>
     *
     * # 8 regions
     * for seq in {0, MAX_SEQUENCE}:
     *   for strand in {0, 1}:
     *     <seq, 0, MAX_LEN, strand>
     *     <seq, MAX_POS, 0, strand>
     *
     * # N*2 regions
     * for i in 1:N:
     *   <1, i*K, L, 1>
     *   <1, i*K, L, -1>
     * </pre>
     * 
     * Regions (sequence=1):
     * [0, 300), [100, 400), [200, 500), [300, 600), [400, 700), [500, 800),
     * [600, 900), [700, 1000), [800, 1100), [900, 1200), [1000, 1300),
     * [1100, 1400), [1200, 1500), [1300, 1600), [1400, 1700), ...
     */
    private List<DNARegion> getTestRegions(int numRunningRegions, int runningRegionStep, int runningLegionLength, int numDuplicateLocations) {
        final List<DNARegion> regions = new ArrayList<DNARegion>();
        DNARegion reg;

        for (int dupLoc=0; dupLoc<numDuplicateLocations; dupLoc++) {
            for (int seq: new int[] {0, DNARegionTest.MAX_SEQUENCE}) {
                for (int pos=0; pos<2; pos++) {
                    for (int len=0; len<2; len++) {
                        for (Strand strand: DNARegionTest.STRANDS) {
                            regions.add(new ImmutableDNARegion(null, seq, pos, len, strand, KEY_HASH+dupLoc));
                        }
                    }
                }
            }

            for (int seq: new int[] {0, DNARegionTest.MAX_SEQUENCE}) {
                for (Strand strand: DNARegionTest.STRANDS) {
                    reg = new ImmutableDNARegion(null, seq, DNARegionTest.MAX_POS, 0, strand, KEY_HASH+dupLoc);
                    Assert.assertEquals(DNARegionTest.MAX_POS, reg.getLeftPosition());
                    regions.add(reg);

                    reg = new ImmutableDNARegion(null, seq, 0, DNARegionTest.MAX_LENGTH, strand, KEY_HASH+dupLoc);
                    Assert.assertEquals(DNARegionTest.MAX_LENGTH, reg.getLength());
                    regions.add(reg);
                }
            }

            for (int i=0; i<numRunningRegions; i++) {
                regions.add(new ImmutableDNARegion(null, 1, i*runningRegionStep, runningLegionLength, Strand.FORWARD, KEY_HASH+dupLoc));
                regions.add(new ImmutableDNARegion(null, 1, i*runningRegionStep, runningLegionLength, Strand.REVERSE, KEY_HASH+dupLoc));
            }
        }
        
        return regions;
    }
    
    protected abstract T[] buildRegionStores();
    protected void testInsertExtra(T store, DNARegion region, String baseMessage) {}
    protected void testDeleteIteratorExtra(T store, DNARegion region, String baseMessage) {}
    protected void testDeleteRandomExtra(T store, DNARegion region, String baseMessage) {}
    protected void testInsertDeleteRepetition(T store, String baseMessage) {}
    
    @Test //@Ignore
    public void testInsert() {
        for (int numDupLocs: NUM_DUPLICATE_LOCATIONS) {
            final List<DNARegion> regions = getTestRegions(N, STEP, LENGTH, numDupLocs);
            for (OperationOrder opOrder: OperationOrder.values()) {
                for (T store: buildRegionStores()) {
                    switch (opOrder) {
                    case ASCENDING:
                        Collections.sort(regions);
                        break;
                    case DESCENDING:
                        Collections.reverse(regions);
                        break;
                    case RANDOM:
                        Collections.shuffle(regions);
                        break;
                    default:
                        Assert.fail();    
                    }

                    for (final DNARegion region: regions) {
                        String baseMessage = String.format(
                                "%s, dups = %d, op-order = %s, region = <%s>",
                                store.getRegionSetName(), numDupLocs, opOrder, region);
                        Assert.assertFalse(baseMessage, store.contains(region));
                        Assert.assertTrue(baseMessage, store.add(region));
                        Assert.assertTrue(baseMessage, store.contains(region));
                        Assert.assertFalse(baseMessage, store.add(region));
                        Assert.assertEquals(baseMessage, region, store.get(region));
                        testInsertExtra(store, region, baseMessage);
                    }

                    String baseMessage = String.format(
                            "%s, dups = %d, op-order = %s",
                            store.getRegionSetName(), numDupLocs, opOrder);
                    int count = 0;
                    for (DNARegion reg: store) {
                        Assert.assertTrue(baseMessage, regions.contains(reg));
                        count++;
                    }
                    Assert.assertEquals(baseMessage, regions.size(), count);
                    
                    store.close();
                }
            }
        }
    }
    
    @Test //@Ignore
    public void testDeleteIterator() {
        for (int numDups: NUM_DUPLICATE_LOCATIONS) {
            final List<DNARegion> regions = getTestRegions(N, STEP, LENGTH, numDups);
            for (T store: buildRegionStores()) {
                for (final DNARegion region: regions) {
                    Assert.assertTrue(store.add(region));
                }

                Iterator<DNARegion> iter = store.iterator();
                while (iter.hasNext()) {
                    final DNARegion region = iter.next();
                    String baseMessage = String.format(
                            "%s, dups = %d, region = <%s>",
                            store.getRegionSetName(), numDups, region);
                    Assert.assertTrue(baseMessage, store.contains(region));
                    iter.remove();
                    Assert.assertFalse(baseMessage, store.contains(region));

                    testDeleteIteratorExtra(store, region, baseMessage);
                }
                
                store.close();
            }
        }
    }
    
    
    @Test //@Ignore
    public void testDeleteRandom() {
        for (int numDups: NUM_DUPLICATE_LOCATIONS) {
            final List<DNARegion> regions = getTestRegions(N, STEP, LENGTH, numDups);
            for (OperationOrder opOrder: OperationOrder.values()) {
                for (T store: buildRegionStores()) {
                    for (final DNARegion region: regions) {
                        Assert.assertTrue(store.add(region));
                    }

                    switch (opOrder) {
                    case ASCENDING:
                        Collections.sort(regions);
                        break;
                    case DESCENDING:
                        Collections.reverse(regions);
                        break;
                    case RANDOM:
                        Collections.shuffle(regions);
                        break;
                    default:
                        Assert.fail();    
                    }

                    for (DNARegion region: regions) {
                        String baseMessage = String.format(
                                "%s, dups = %d, order = %s, region = <%s>",
                                store.getRegionSetName(), numDups, opOrder, region);
                        Assert.assertTrue(baseMessage, store.contains(region));
                        Assert.assertTrue(store.remove(region));
                        Assert.assertFalse(baseMessage, store.contains(region));
                        Assert.assertFalse(store.remove(region));
                        
                        testDeleteRandomExtra(store, region, baseMessage);
                    }
                }
            }
        }
    }

    private List<DNARegion> iteratorToList(Iterator<DNARegion> source) {
        List<DNARegion> target = new ArrayList<DNARegion>();
        while (source.hasNext()) {
            target.add(source.next());
        }
        return target;
    }
    
    @Test //@Ignore
    public void testRangeQuery() {
        final int SEQUENCE = 1;

        // [query, N]; expected result size = N*numDups*(2 if Strand.ANY)
        final Object[][] testCases = new Object[][] {
                {new MutableDNARegion(null, SEQUENCE, 499, 0, Strand.ANY, KEY_HASH), 0},
                {new MutableDNARegion(null, SEQUENCE, 499, 1, Strand.ANY, KEY_HASH), 0},
                {new MutableDNARegion(null, SEQUENCE, 500, 0, Strand.ANY, KEY_HASH), 0},
                {new MutableDNARegion(null, SEQUENCE, 501, 0, Strand.ANY, KEY_HASH), 0},
                {new MutableDNARegion(null, SEQUENCE, 501, 99, Strand.ANY, KEY_HASH), 0},
                {new MutableDNARegion(null, SEQUENCE, 500, 1, Strand.ANY, KEY_HASH), 1},
                {new MutableDNARegion(null, SEQUENCE, 500, 300, Strand.ANY,KEY_HASH), 3},
                {new MutableDNARegion(null, SEQUENCE, 500, 301, Strand.ANY, KEY_HASH), 4}
        };

        for (int numDups: NUM_DUPLICATE_LOCATIONS) {
            final List<DNARegion> regions = getTestRegions(N, STEP, LENGTH, numDups);
            for (T store: buildRegionStores()) {
                for (DNARegion region: regions) {
                    Assert.assertTrue(store.add(region));
                }

                for (Object[] testCase: testCases) {
                    for (Strand strand: Strand.values()) {
                        final MutableDNARegion query = (MutableDNARegion)testCase[0];
                        query.setStrand(strand);
                        
                        final Iterator<DNARegion> iter;
                        try {
                            iter = store.getRange(query);
                        } catch (UnsupportedOperationException e) {
                            return; // Store does not support range queries
                        }
                        final List<DNARegion> result = iteratorToList(iter);
                        
                        int expected = ((Integer)(testCase[1])).intValue() * numDups * (strand == Strand.ANY ? 2 : 1);
                        String msg = String.format("%s, dups = %d, query = <%s>",
                                store.getRegionSetName(), numDups, query);
                        Assert.assertEquals(msg, expected, result.size());

                        for (DNARegion reg: result) {
                            Assert.assertTrue(msg, reg.getLeftPosition() >= query.getLeftPosition());
                            Assert.assertTrue(msg, reg.getLeftPosition() < query.getRightPosition(false));
                            if (strand != Strand.ANY) {
                                Assert.assertEquals(msg, strand, reg.getStrand());
                            }
                        }
                    }
                }
            }
        }
    }

    @Test //@Ignore
    public void testOverlapQuery() {
        final int SEQUENCE = 1;

        // [query, N]; expected result size = N*numDups*(2 if Strand.ANY)
        final Object[][] testCases = new Object[][] {
                {new MutableDNARegion(null, SEQUENCE, 499, 0, Strand.ANY, KEY_HASH), 3},
                {new MutableDNARegion(null, SEQUENCE, 499, 1, Strand.ANY, KEY_HASH), 3},
                {new MutableDNARegion(null, SEQUENCE, 499, 2, Strand.ANY, KEY_HASH), 4},
                {new MutableDNARegion(null, SEQUENCE, 500, 0, Strand.ANY, KEY_HASH), 2},
                {new MutableDNARegion(null, SEQUENCE, 500, 1, Strand.ANY, KEY_HASH), 3},
                {new MutableDNARegion(null, SEQUENCE, 399, 102, Strand.ANY, KEY_HASH), 5},
                {new MutableDNARegion(null, SEQUENCE, 400, 101, Strand.ANY, KEY_HASH), 4},
                {new MutableDNARegion(null, SEQUENCE, 400, 100, Strand.ANY, KEY_HASH), 3},
        };

        for (int numDups: NUM_DUPLICATE_LOCATIONS) {
            final List<DNARegion> regions = getTestRegions(N, STEP, LENGTH, numDups);
            for (T store: buildRegionStores()) {
                for (DNARegion region: regions) {
                    Assert.assertTrue(store.add(region));
                }

                for (Object[] testCase: testCases) {
                    for (Strand strand: Strand.values()) {
                        MutableDNARegion query = (MutableDNARegion)testCase[0];
                        query.setStrand(strand);
                        
                        final Iterator<DNARegion> iter;
                        try {
                            iter = store.getOverlapping(query);
                        } catch (UnsupportedOperationException e) {
                            return; // Store does not support range queries
                        }
                        final List<DNARegion> result = iteratorToList(iter);
                        
                        int expected = ((Integer)(testCase[1])).intValue() * numDups * (strand == Strand.ANY ? 2 : 1);
                        String msg = String.format("%s, dups = %d, query = <%s>",
                                store.getRegionSetName(), numDups, query);
                        Assert.assertEquals(msg, expected, result.size());

                        for (DNARegion reg: result) {
                            Assert.assertTrue(msg, query.overlaps(reg));
                            Assert.assertTrue(msg, reg.overlaps(query));
                            if (strand != Strand.ANY) {
                                Assert.assertEquals(msg, strand, reg.getStrand());
                            }
                        }
                    }
                }
            }
        }
    }

    @Test //@Ignore
    public void testInsertDeleteRepetition() {
        final List<DNARegion> regions = getTestRegions(N, STEP, LENGTH, 5);
        
        final int repetitions = 10;

        for (T store: buildRegionStores()) {
            String msg = store.getRegionSetName();
            
            for (int round=0; round<repetitions; round++) {
                for (DNARegion reg: regions) {
                    Assert.assertFalse(msg, store.contains(reg));
                    Assert.assertTrue(msg, store.add(reg));
                    Assert.assertTrue(msg, store.contains(reg));
                }
                
                testInsertDeleteRepetition(store, msg);
                
                Iterator<DNARegion> iter = store.iterator();
                while (iter.hasNext()) {
                    DNARegion reg = iter.next();
                    Assert.assertTrue(msg, store.contains(reg));
                    iter.remove();
                    Assert.assertFalse(msg, store.contains(reg));
                }
                
                Assert.assertFalse(store.iterator().hasNext());
            }
        }
    }
}
