package org.anduril.javatools.seq.region.unittest;

import org.anduril.javatools.seq.region.mapStore.MemoryHashStore;

public class HashStoreTest extends RegionStoreTest {

    @Override
    protected MemoryHashStore[] buildRegionStores() {
        return new MemoryHashStore[] {
                MemoryHashStore.makeHashSet(null, "HashSet")
        };
    }
}
