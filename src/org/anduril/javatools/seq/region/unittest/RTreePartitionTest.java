package org.anduril.javatools.seq.region.unittest;

import org.anduril.javatools.seq.region.rtree.RTreeDensity;

public class RTreePartitionTest extends RegionPartitionTest<RTreeDensity> {
    public static final int[] TREE_ORDERS = {2, 3, 4, 5, 32};
    
    @Override
    protected RTreeDensity[] buildRegionPartitions() {
        RTreeDensity[] trees = new RTreeDensity[TREE_ORDERS.length];
        for (int i=0; i<TREE_ORDERS.length; i++) {
            final int order = TREE_ORDERS[i];
            final String name = String.format("Order = %d", order);
            trees[i] = new RTreeDensity(order, name);
        }
        return trees;
    }
    
    @Override
    protected void testPostOperationExtra(RTreeDensity partition, String baseMessage) {
        partition.getRoot().checkTree(baseMessage);
    }
}
