package org.anduril.javatools.seq.region.unittest;

import org.junit.Test;

import junit.framework.Assert;
import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.ImmutableDNARegion;
import org.anduril.javatools.seq.region.Strand;

public class DNARegionTest {
    public static final int MAX_SEQUENCE = 0x3f;
    public static final int MAX_LENGTH = 0x7fffffff; 
    public static final long MAX_POS = 0xffffffffffL; 
    public static final Strand[] STRANDS = new Strand[] {Strand.FORWARD, Strand.REVERSE};

    @Test
    public void getTestRegions() {
        DNARegion reg;
        
        for (int seq: new int[] {0, MAX_SEQUENCE}) {
            for (int leftPos=0; leftPos<2; leftPos++) {
                for (int len=0; len<2; len++) {
                    for (Strand strand: STRANDS) {
                        reg = new ImmutableDNARegion(null, seq, leftPos, len, strand, 0x100);
                        Assert.assertEquals(seq, reg.getSequenceID());
                        Assert.assertEquals(leftPos, reg.getLeftPosition());
                        Assert.assertEquals(leftPos+len, reg.getRightPosition(false));
                        Assert.assertEquals(len, reg.getLength());
                        Assert.assertEquals(strand, reg.getStrand());
                        Assert.assertEquals(0x100, reg.getKeyHash());
                        
                        if (strand == Strand.FORWARD) {
                            Assert.assertEquals(leftPos, reg.getFirstPosition());
                            Assert.assertEquals(leftPos+len, reg.getLastPosition());
                        } else {
                            Assert.assertEquals(leftPos+len, reg.getFirstPosition());
                            Assert.assertEquals(leftPos, reg.getLastPosition());
                        }
                    }
                }
            }
        }
        
        for (int seq: new int[] {0, MAX_SEQUENCE}) {
            for (Strand strand: STRANDS) {
                reg = new ImmutableDNARegion(null, seq, MAX_POS, 0, strand, 0);
                Assert.assertEquals(seq, reg.getSequenceID());
                Assert.assertEquals(0, reg.getLength());
                Assert.assertEquals(MAX_POS, reg.getLeftPosition());
                Assert.assertEquals(MAX_POS, reg.getRightPosition(false));
                Assert.assertEquals(MAX_POS, reg.getFirstPosition());
                Assert.assertEquals(MAX_POS, reg.getLastPosition());
                Assert.assertEquals(strand, reg.getStrand());
                
                reg = new ImmutableDNARegion(null, seq, 0, MAX_LENGTH, strand, 0);
                Assert.assertEquals(seq, reg.getSequenceID());
                Assert.assertEquals(0, reg.getLeftPosition());
                Assert.assertEquals(MAX_LENGTH, reg.getRightPosition(false));
                Assert.assertEquals(MAX_LENGTH, reg.getLength());
                Assert.assertEquals(strand, reg.getStrand());
                if (strand == Strand.FORWARD) {
                    Assert.assertEquals(0, reg.getFirstPosition());
                    Assert.assertEquals(MAX_LENGTH, reg.getLastPosition());
                } else { 
                    Assert.assertEquals(MAX_LENGTH, reg.getFirstPosition());
                    Assert.assertEquals(0, reg.getLastPosition());
                }
            }
        }
    }
}
