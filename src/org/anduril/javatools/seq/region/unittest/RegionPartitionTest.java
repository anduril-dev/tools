package org.anduril.javatools.seq.region.unittest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import junit.framework.Assert;

import org.junit.Test;

import org.anduril.javatools.seq.region.AggregateFunction;
import org.anduril.javatools.seq.region.AnnotationRegistry;
import org.anduril.javatools.seq.region.AnnotationType;
import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.ImmutableDNARegion;
import org.anduril.javatools.seq.region.RegionPartition;
import org.anduril.javatools.seq.region.Strand;

public abstract class RegionPartitionTest <T extends RegionPartition> {
    public static final int SEQ = 1;
    
    private static class Operation {
        private static final AnnotationRegistry registry = new AnnotationRegistry();
        static {
            registry.registerAnnotation(AnnotationRegistry.ANNOTATION_SCORE, AnnotationType.DOUBLE);
        }
        
        public final int sequenceID;
        public final long left;
        public final long length;
        public final Strand strand;
        public final AggregateFunction func;
        public final double value;
        public final double[] expectedScores;
        public final List<DNARegion> expectedSegments;
        
        public Operation(int sequenceID, long left, long length, Strand strand, AggregateFunction func, double value, double[] expectedScores) {
            this.sequenceID = sequenceID;
            this.left = left;
            this.length = length;
            this.strand = strand;
            this.func = func;
            this.value = value;
            this.expectedScores = expectedScores;
            this.expectedSegments = new ArrayList<DNARegion>();
        }
        
        @Override
        public String toString() {
            return String.format("seq=%d, left=%d, length=%d, strand=%s, func=%s, value=%.2f",
                    this.sequenceID, this.left, this.length, this.strand, this.func, this.value);
        }
        
        public void addSegment(long length, double score, int counter) {
            final int n = this.expectedSegments.size();
            long left = n == 0 ? 0 : this.expectedSegments.get(n-1).getRightPosition(false);
            DNARegion reg = new ImmutableDNARegion(registry, this.sequenceID, left, length, this.strand, counter);
            reg.setDoubleAnnotation(0, score);
            this.expectedSegments.add(reg);
        }
    }
    
    protected abstract T[] buildRegionPartitions();
    protected void testPostOperationExtra(T partition, String baseMessage) {}
    
    private void runTest(T partition, Operation[] operations) {
        for (Operation op: operations) {
            DNARegion span = new ImmutableDNARegion(partition.getAnnotationRegistry(), op.sequenceID, op.left, op.length, op.strand, 0);
            partition.modifyScore(span, op.func, op.value);
            
            String baseMessage = String.format("%s, op=<%s>", partition.getRegionSetName(), op);
            
            if (op.expectedScores != null) {
                final int numPositions = op.expectedScores.length;
                for (int pos=0; pos<numPositions; pos++) {
                    String msg = String.format("%s: pos=%d", baseMessage, pos);
                    Assert.assertEquals(msg, op.expectedScores[pos], partition.getScore(op.sequenceID, pos, op.strand));
                }
                Assert.assertEquals(baseMessage, 0.0, partition.getScore(op.sequenceID, numPositions, op.strand));
                Assert.assertEquals(baseMessage, 0.0, partition.getScore(op.sequenceID, numPositions+1, op.strand));
            }
            
            if (!op.expectedSegments.isEmpty()) {
                int i = 0;
                for (DNARegion reg: partition) {
                    if (reg.getSequenceID() != op.sequenceID || reg.getStrand() != op.strand) continue;
                    
                    if (i >= op.expectedSegments.size()) {
                        Assert.fail(baseMessage+": too many segments: "+(i+1));
                    }
                    DNARegion expected = op.expectedSegments.get(i);
                    String msg = String.format("%s: segment %d", baseMessage, i);
                    i++;
                    
                    Assert.assertEquals(msg, expected, reg);
                    Assert.assertEquals(msg, expected.getDoubleAnnotation(0), reg.getDoubleAnnotation(0));
                }
                
                Assert.assertEquals(baseMessage+": invalid number of segments",
                        op.expectedSegments.size(), i);
            }
            
            testPostOperationExtra(partition, partition.getRegionSetName());
        }
    }
    
    private Operation[] getOperations(Strand strand) {
        Operation[] ops = new Operation[6];
        final AggregateFunction assign = AggregateFunction.ASSIGN;
        final AggregateFunction product = AggregateFunction.PRODUCT;
        final AggregateFunction sum = AggregateFunction.SUM;
        
        ops[0] = new Operation(SEQ, 2, 2, strand, sum, 3, new double[] {0, 0, 3, 3});
        ops[0].addSegment(2, 0.0, 1);
        ops[0].addSegment(2, 3.0, 1);
        
        ops[1] = new Operation(SEQ, 5, 1, strand, sum, 2, new double[] {0, 0, 3, 3, 0, 2});
        ops[1].addSegment(2, 0.0, 1);
        ops[1].addSegment(2, 3.0, 1);
        ops[1].addSegment(1, 0.0, 1);
        ops[1].addSegment(1, 2.0, 1);
        
        ops[2] = new Operation(SEQ, 0, 1, strand, assign, 4, new double[] {4, 0, 3, 3, 0, 2});
        ops[2].addSegment(1, 4.0, 2);
        ops[2].addSegment(1, 0.0, 2);
        ops[2].addSegment(2, 3.0, 1);
        ops[2].addSegment(1, 0.0, 1);
        ops[2].addSegment(1, 2.0, 1);
        
        // 2x + 1 => x+1
        ops[3] = new Operation(SEQ, 1, 4, strand, sum, 1, new double[] {4, 1, 4, 4, 1, 2});
        ops[3].addSegment(1, 4.0, 2);
        ops[3].addSegment(1, 1.0, 3);
        ops[3].addSegment(2, 4.0, 2);
        ops[3].addSegment(1, 1.0, 2);
        ops[3].addSegment(1, 2.0, 1);
        
        ops[4] = new Operation(SEQ, 0, 2, strand, product, 0, new double[] {0, 0, 4, 4, 1, 2});
        ops[4].addSegment(1, 0.0, 3); // TODO: merge
        ops[4].addSegment(1, 0.0, 4);
        ops[4].addSegment(2, 4.0, 2);
        ops[4].addSegment(1, 1.0, 2);
        ops[4].addSegment(1, 2.0, 1);

        ops[5] = new Operation(SEQ, 3, 10, strand, assign, 0, new double[] {0, 0, 4, 0, 0, 0});
        ops[5].addSegment(1, 0.0, 3); // TODO: merge
        ops[5].addSegment(1, 0.0, 4);
        ops[5].addSegment(1, 4.0, 3);
        ops[5].addSegment(1, 0.0, 3); // TODO: merge
        ops[5].addSegment(1, 0.0, 3);
        ops[5].addSegment(1, 0.0, 2);
        
        return ops;
    }

    @Test //@Ignore
    public void testOneSequence() {
        for (Strand strand: Strand.forwardReverse()) {
            Operation[] mainOps = getOperations(strand);
            for (T part: buildRegionPartitions()) {
                runTest(part, mainOps);
            }
        }
    }
    
    @Test //@Ignore
    public void testManySequence() {
        final int seqL = 0;
        final int seqR = 2;

        Operation[] mainOpsForward = getOperations(Strand.FORWARD);
        Operation[] mainOpsReverse = getOperations(Strand.REVERSE);
        Operation left = new Operation(seqL, 1, 2, Strand.FORWARD, AggregateFunction.SUM, 1.5, new double[] {0, 1.5, 1.5});
        Operation right = new Operation(seqR, 1, 2, Strand.FORWARD, AggregateFunction.SUM, 2.5, new double[] {0, 2.5, 2.5});
        
        for (boolean forwardFirst: new boolean[] {true, false}) {
            for (T part: buildRegionPartitions()) {
                runTest(part, new Operation[] {left, right});
                if (forwardFirst) {
                    runTest(part, mainOpsForward);
                    runTest(part, mainOpsReverse);
                } else {
                    runTest(part, mainOpsReverse);
                    runTest(part, mainOpsForward);
                }
            }
        }
    }
    
    @Test //@Ignore
    public void testSequential() {
        Operation[] ops = new Operation[4];
        final AggregateFunction sum = AggregateFunction.SUM;

        ops[0] = new Operation(SEQ, 0, 2, Strand.FORWARD, sum, 1, new double[] {1, 1});
        ops[0].addSegment(2, 1.0, 1);

        ops[1] = new Operation(SEQ, 2, 2, Strand.FORWARD, sum, 2, new double[] {1, 1, 2, 2});
        ops[1].addSegment(2, 1.0, 1);
        ops[1].addSegment(2, 2.0, 1);

        ops[2] = new Operation(SEQ, 4, 1, Strand.FORWARD, sum, 3, new double[] {1, 1, 2, 2, 3});
        ops[2].addSegment(2, 1.0, 1);
        ops[2].addSegment(2, 2.0, 1);
        ops[2].addSegment(1, 3.0, 1);

        ops[3] = new Operation(SEQ, 6, 1, Strand.FORWARD, sum, 4, new double[] {1, 1, 2, 2, 3, 0, 4});
        ops[3].addSegment(2, 1.0, 1);
        ops[3].addSegment(2, 2.0, 1);
        ops[3].addSegment(1, 3.0, 1);
        ops[3].addSegment(1, 0.0, 1);
        ops[3].addSegment(1, 4.0, 1);
        
        for (T part: buildRegionPartitions()) {
            runTest(part, ops);
        }
    }
    
    @Test
    public void testAnyStrand() {
        final Operation[] ops = new Operation[6];
        final AggregateFunction sum = AggregateFunction.SUM;
        
        // This should be executed with order=2 to control for
        // regression related to Strand.ANY leading to duplicate
        // keys in tree.
        ops[0] = new Operation(SEQ, 0, 5, Strand.REVERSE, sum, 1, null);
        ops[1] = new Operation(SEQ, 40, 5, Strand.FORWARD, sum, 1, null);
        ops[2] = new Operation(SEQ, 20, 5, Strand.FORWARD, sum, 1, null);
        ops[3] = new Operation(SEQ, 50, 5, Strand.ANY, sum, 1, null);
        ops[4] = new Operation(SEQ, 30, 5, Strand.FORWARD, sum, 1, null);
        ops[5] = new Operation(SEQ, 10, 5, Strand.FORWARD, sum, 1, null);

        for (T part: buildRegionPartitions()) {
            runTest(part, ops);
        }
    }
    
    @Test //@Ignore
    public void testRandom() {
        final int rounds = 1000;
        final Operation[] ops = new Operation[1];
        
        Random rnd = new Random();
        for (T part: buildRegionPartitions()) {
            for (int i=0; i<rounds; i++) {
                final int seq = rnd.nextInt(3);
                final long left = rnd.nextInt(1000);
                final long length = rnd.nextInt(20);
                final Strand strand = Strand.values()[rnd.nextInt(Strand.values().length)];
                final double addOffset = rnd.nextDouble() * 2 - 1;

                ops[0] = new Operation(seq, left, length, strand, AggregateFunction.SUM, addOffset, null);
                runTest(part, ops);
            }
        }
    }
}
