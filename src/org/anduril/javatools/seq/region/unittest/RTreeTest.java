package org.anduril.javatools.seq.region.unittest;

import org.junit.Assert;

import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.rtree.RTree;

public class RTreeTest extends RegionStoreTest<RTree> {
    public static final int[] TREE_ORDERS = {4, 5, 32};
    
    @Override
    protected RTree[] buildRegionStores() {
        RTree[] trees = new RTree[TREE_ORDERS.length];
        for (int i=0; i<TREE_ORDERS.length; i++) {
            final int order = TREE_ORDERS[i];
            final String name = String.format("Order = %d", order);
            trees[i] = new RTree(order, false, true, 0, name);
        }
        return trees;
    }
    
    @Override
    protected void testInsertExtra(RTree tree, DNARegion region, String baseMessage) {
        tree.getRoot().checkTree(baseMessage);
        final DNARegion target = tree.get(region);
        Assert.assertEquals(baseMessage, region, target);
    }
    
    @Override
    protected void testDeleteIteratorExtra(RTree tree, DNARegion region, String baseMessage) {
        tree.getRoot().checkTree(baseMessage);
    }
    
    @Override
    protected void testDeleteRandomExtra(RTree tree, DNARegion region, String baseMessage) {
        tree.getRoot().checkTree(baseMessage);
    }
    
    @Override
    protected void testInsertDeleteRepetition(RTree tree, String baseMessage) {
        tree.getRoot().checkTree(baseMessage);
    }
}
