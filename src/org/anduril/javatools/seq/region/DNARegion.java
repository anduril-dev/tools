package org.anduril.javatools.seq.region;

public abstract class DNARegion implements Comparable<DNARegion> {
    public static final int SEQ_NONE = 127;
    
    private final AnnotationRegistry registry;
    protected int sequenceID;
    protected long left;
    protected long length;
    protected Strand strand;
    protected long keyHash;
    protected final double[] doubleAnnotations;
    protected final long[] longAnnotations;
    protected final String[] stringAnnotations;
    
    public DNARegion(AnnotationRegistry annRegistry,
            int sequenceID, long leftPosition, long length, Strand strand, long keyHash) {
        this.registry = annRegistry == null ? AnnotationRegistry.EMPTY_REGISTRY : annRegistry;
        this.sequenceID = sequenceID;
        this.left = leftPosition;
        this.length = length;
        this.strand = strand;
        this.keyHash = keyHash;
        if (annRegistry == null) {
            this.doubleAnnotations = null;
            this.longAnnotations = null;
            this.stringAnnotations = null;
        } else {
            final int numDouble = annRegistry.getNumAnnotations(AnnotationType.DOUBLE);
            this.doubleAnnotations = numDouble > 0 ? new double[numDouble] : null;

            final int numLong = annRegistry.getNumAnnotations(AnnotationType.LONG);
            this.longAnnotations = numLong > 0 ? new long[numLong] : null;
            
            final int numString = annRegistry.getNumAnnotations(AnnotationType.STRING);
            this.stringAnnotations = numString > 0 ? new String[numString] : null;
        }
    }

    @Override
    public int compareTo(final DNARegion other) {
        final int seqCompare = this.sequenceID - other.sequenceID;
        if (seqCompare != 0) return seqCompare;
  
        final int leftCompare = Long.signum(this.left - other.left);
        if (leftCompare != 0) return leftCompare;
        
        final int lengthCompare = Long.signum(this.length - other.length);
        if (lengthCompare != 0) return lengthCompare;

        final int strandCompare = this.strand.compareTo(other.strand);
        if (strandCompare != 0) return strandCompare;
        
        return Long.signum(this.keyHash - other.keyHash);
    }
    
    @Override
    public boolean equals(Object other) {
        if ((other instanceof DNARegion)) {
            return compareTo((DNARegion)other) == 0;    
        } else {
            return false;
        }
    }
    
    public boolean matches(DNARegion other, boolean exactStrandMatch, boolean compareKeyHash) {
        if (this.sequenceID != other.sequenceID) return false;
        if (this.left != other.left) return false;
        if (this.length != other.length) return false;
        if (compareKeyHash && this.keyHash != other.keyHash) return false;
        
        if (exactStrandMatch) {
            return this.strand == other.strand;
        } else {
            if (this.strand == Strand.ANY || other.strand == Strand.ANY) {
                return true;
            } else {
                return this.strand == other.strand;
            }
        }
    }
    
    @Override
    public int hashCode() {
        return this.sequenceID ^ (int)this.left ^ (int)this.length ^ this.strand.twoBit ^ (int)this.keyHash;
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(String.format("seq=%d, span=[%d, %d), len=%d, strand=%s, hash=%x",
                this.sequenceID,
                this.getLeftPosition(), this.getRightPosition(false),
                this.length,
                this.strand,
                this.keyHash));

        for (String name: this.registry.getAnnotations(null)) {
            final AnnotationType type = this.registry.getType(name);
            final int annotationID = this.registry.getAnnotationID(name);
            
            final String annStr;
            switch (type) {
            case DOUBLE:
                annStr = String.format("%.2g", this.doubleAnnotations[annotationID]);
                break;
            case LONG:
                annStr = String.format("%d", this.longAnnotations[annotationID]);
                break;
            case STRING:
                annStr = String.format("%s", this.stringAnnotations[annotationID]);
                break;
            default:
                throw new RuntimeException("Unknown annotation type: "+type);     
            }
            
            sb.append(String.format(", %s=%s", name, annStr));
        }
        
        return sb.toString();
    }

    public abstract ImmutableDNARegion getImmutable();
    
    public ImmutableDNARegion getImmutable(AnnotationRegistry targetRegistry) {
        if (targetRegistry == this.registry) {
            return getImmutable();
        } else {
            return mapAnnotations(targetRegistry);
        }
    }
    
    public void copyAnnotations(DNARegion from) {
        if (this.getAnnotationRegistry() != from.getAnnotationRegistry()) {
            throw new IllegalArgumentException("Annotation registries must be equal");
        }
        if (this.doubleAnnotations != null) {
            System.arraycopy(from.doubleAnnotations, 0, this.doubleAnnotations, 0, from.doubleAnnotations.length);
        }
        if (this.longAnnotations != null) {
            System.arraycopy(from.longAnnotations, 0, this.longAnnotations, 0, from.longAnnotations.length);
        }
        if (this.stringAnnotations != null) {
            System.arraycopy(from.stringAnnotations, 0, this.stringAnnotations, 0, from.stringAnnotations.length);
        }
    }
    
    public ImmutableDNARegion mapAnnotations(AnnotationRegistry targetRegistry) {
        ImmutableDNARegion region = new ImmutableDNARegion(targetRegistry,
                this.sequenceID, this.left, this.length, this.strand, this.keyHash);
        
        if (targetRegistry != null) {
            for (String ann: this.registry.getAnnotations()) {
                if (targetRegistry.contains(ann)) {
                    final AnnotationType type = this.registry.getType(ann);
                    if (targetRegistry.getType(ann) != type) {
                        throw new IllegalArgumentException(String.format(
                                "Inconsistent annotation types for %s: %s vs %s",
                                ann, type, targetRegistry.getType(ann)));
                    }

                    final int fromID = this.registry.getAnnotationID(ann);
                    final int toID = targetRegistry.getAnnotationID(ann);

                    switch (type) {
                    case DOUBLE:
                        region.setDoubleAnnotation(toID, getDoubleAnnotation(fromID));
                        break;
                    case LONG:
                        region.setLongAnnotation(toID, getLongAnnotation(fromID));
                        break;
                    case STRING:
                        region.setStringAnnotation(toID, getStringAnnotation(fromID));
                        break;
                    default:
                        throw new RuntimeException("Unknown annotation type: "+type);
                    }
                }
            }
        }
        
        return region;
    }
    
    public final int getSequenceID() {
        return sequenceID;
    }

    public final long getLeftPosition() {
        return this.left;
    }
    
    public final long getRightPosition(boolean inclusive) {
        if (inclusive) {
            if (this.length == 0) {
                throw new IllegalArgumentException("Region length is 0: can not form inclusive right position");
            }
            return this.left + this.length - 1;
        } else {
            return this.left + this.length;    
        }
    }
    
    public final long getFirstPosition() {
        return this.strand != Strand.REVERSE ? this.left : this.left + this.length;
    }

    public final long getLastPosition() {
        return this.strand != Strand.REVERSE ? this.left + this.length : this.left;
    }
    
    public final long getLength() {
        return this.length;
    }

    public final boolean isEmpty() {
        return this.length == 0;
    }
    
    public final Strand getStrand() {
        return strand;
    }

    public final long getKeyHash() {
        return this.keyHash;
    }
    
    /**
     * Return max(left1-right2, left2-right1) which determines
     * whether regions overlap and the gap between non-overlapping
     * regions. If returned determinant D is < 0, regions overlap.
     * If D == 0, region are adjacent (touching). If D > 0, there
     * is a gap of D positions between regions. If
     * D == Long.MAX_VALUE, regions are in different sequences or
     * strands.
     */
    public long getGapDeterminant(DNARegion other) {
        if (this.sequenceID != other.sequenceID) return Long.MAX_VALUE;
        if (!this.strand.isCompatible(other.strand)) return Long.MAX_VALUE;
        
        final long left1 = getLeftPosition();
        final long right1 = getRightPosition(false);
        final long left2 = other.getLeftPosition();
        final long right2 = other.getRightPosition(false);

        final long diff1 = left1 - right2;
        final long diff2 = left2 - right1;

        return Math.max(diff1, diff2);
    }

    public final boolean overlaps(DNARegion other) {
        return getGapDeterminant(other) < 0;
    }
    
    public final boolean touches(DNARegion other) {
        return getGapDeterminant(other) == 0;
    }
    
    public long getDistanceGap(DNARegion other) {
        return Math.max(0, getGapDeterminant(other));
    }
    
    public long getDistanceStart(DNARegion other) {
        if (this.sequenceID != other.sequenceID) return Long.MAX_VALUE;
        if (!this.strand.isCompatible(other.strand)) return Long.MAX_VALUE;
        return Math.abs(this.getFirstPosition() - other.getFirstPosition());
    }
    
    public long getOverlap(DNARegion other) {
        if (this.sequenceID != other.sequenceID) return 0;
        if (!this.strand.isCompatible(other.strand)) return 0;

        final long left1 = getLeftPosition();
        final long right1 = getRightPosition(false);
        final long left2 = other.getLeftPosition();
        final long right2 = other.getRightPosition(false);

        return Math.min(right1, right2) - Math.max(left1, left2);
    }
    
    public final AnnotationRegistry getAnnotationRegistry() {
        return this.registry;
    }

    public final double getDoubleAnnotation(int annotationID) {
        return this.doubleAnnotations[annotationID];
    }
    
    public void setDoubleAnnotation(int annotationID, double value) {
        this.doubleAnnotations[annotationID] = value;
    }
    
    public final long getLongAnnotation(int annotationID) {
        return this.longAnnotations[annotationID];
    }
    
    public void setLongAnnotation(int annotationID, long value) {
        this.longAnnotations[annotationID] = value;
    }
    
    public final String getStringAnnotation(int annotationID) {
        return this.stringAnnotations[annotationID];
    }
    
    public void setStringAnnotation(int annotationID, String value) {
        this.stringAnnotations[annotationID] = value;
    }
    
    public static boolean overlappingCoordinates(long left1, long right1, long left2, long right2) {
        return (right1 == left2 && left1 == right2) || (right1 > left2 && left1 < right2);
    }
    
    public static long makeKeyHash(String key) {
        return key == null ? 0 : key.hashCode();
    }

}
