package org.anduril.javatools.seq.region;

import java.util.Iterator;

import org.anduril.javatools.utils.AbstractIterator;

public abstract class RegionFilter implements RegionReader {
    private final RegionReader reader;
    
    private class FilterIterator extends AbstractIterator<DNARegion> {
        private final Iterator<DNARegion> iter;
        
        public FilterIterator(Iterator<DNARegion> iter) {
            this.iter = iter;
        }
        
        @Override
        protected DNARegion getNext() {
            while (this.iter.hasNext()) {
                final DNARegion region = this.iter.next();
                if (RegionFilter.this.accept(region)) return transform(region);
            }
            return null;
        }
        
        @Override
        public void remove() {
            this.iter.remove();
        }
    }
    
    public RegionFilter(RegionReader reader) {
        this.reader = reader;
    }

    @Override
    public Iterator<DNARegion> iterator() {
        return new FilterIterator(this.reader.iterator());
    }

    @Override
    public AnnotationRegistry getAnnotationRegistry() {
        return this.reader.getAnnotationRegistry();
    }

    @Override
    public String getRegionSetName() {
        return this.reader.getRegionSetName();
    }

    @Override
    public void close() {
        this.reader.close();
    }

    public abstract boolean accept(DNARegion region);
    
    public DNARegion transform(DNARegion region) {
        return region;
    }
}
