package org.anduril.javatools.seq.region;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class AnnotationRegistry {
    public static final int ANNOTATION_NONE = -1;
    
    public static final String ANNOTATION_ID = "ID";
    public static final String ANNOTATION_SCORE = "Score";
    
    public static final AnnotationRegistry EMPTY_REGISTRY = new AnnotationRegistry();
    
    private final HashMap<String, AnnotationType> annotationTypes;
    private final HashMap<String, Integer> annotationIDs;
    private final ArrayList<String> annotationOrder;
    private final int[] freeIDMap; /* AnnotationType.ordinal -> next free index */
    
    public AnnotationRegistry() {
        this.annotationTypes = new HashMap<String, AnnotationType>();
        this.annotationIDs = new HashMap<String, Integer>();
        this.annotationOrder = new ArrayList<String>();
        this.freeIDMap = new int[AnnotationType.values().length];
    }
    
    public AnnotationRegistry(AnnotationRegistry template) {
        this.annotationTypes = new HashMap<String, AnnotationType>(template.annotationTypes);
        this.annotationIDs = new HashMap<String, Integer>(template.annotationIDs);
        this.annotationOrder = new ArrayList<String>(template.annotationOrder);
        this.freeIDMap = Arrays.copyOf(template.freeIDMap, template.freeIDMap.length);
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (String ann: this.annotationOrder) {
            sb.append(String.format("%s: type %s, ID %d",
                    ann, this.annotationTypes.get(ann), this.annotationIDs.get(ann)));
            sb.append('\n');
        }
        return sb.toString();
    }
    
    public int registerAnnotation(String name, AnnotationType type) {
        if (this.annotationTypes.containsKey(name)) {
            throw new IllegalArgumentException("Annotation already exists: "+name);
        }
        
        final int newID = this.freeIDMap[type.ordinal()];
        this.freeIDMap[type.ordinal()]++;
        this.annotationTypes.put(name, type);
        this.annotationIDs.put(name, newID);
        this.annotationOrder.add(name);
        return newID;
    }
    
    public void remoteAnnotation(String name) {
        if (!this.annotationOrder.remove(name)) {
            throw new IllegalArgumentException("Annotation not found: "+name);
        }
        this.annotationTypes.remove(name);
        this.annotationIDs.remove(name);
    }
    
    public boolean contains(String annotationName) {
        return this.annotationTypes.containsKey(annotationName);
    }
    
    public int getAnnotationID(String name) {
        final Integer idObj = this.annotationIDs.get(name);
        if (idObj == null)  {
            return ANNOTATION_NONE;
        }
        return idObj.intValue();
    }
    
    public AnnotationType getType(String name) {
        return this.annotationTypes.get(name);
    }
    
    public List<String> getAnnotations() {
        return this.annotationOrder;
    }
    
    public List<String> getAnnotations(AnnotationType type) {
        if (type == null) return this.annotationOrder;
        
        final List<String> annotations = new ArrayList<String>();
        for (String name: this.annotationOrder) {
            AnnotationType curType = this.annotationTypes.get(name);
            if (type == null || curType == type) {
                annotations.add(name);
            }
        }
        return annotations;
    }
    
    public int getNumAnnotations(AnnotationType type) {
        return this.annotationIDs.size();
    }
    
    public void addAll(AnnotationRegistry other) {
        if (other == null) return;
        
        for (String ann: other.getAnnotations(null)) {
            if (this.contains(ann)) {
                if (this.getType(ann) != other.getType(ann)) {
                    throw new IllegalArgumentException(String.format(
                            "Inconsistent annotation types for %s: %s vs %s",
                            ann, this.getType(ann), other.getType(ann)));
                }
            } else {
                this.registerAnnotation(ann, other.getType(ann));
            }
        }
    }
}
