package org.anduril.javatools.seq.region.rtree;

public abstract class TreeNode implements Comparable<TreeNode> {
    protected static final String INDENT = "  ";
    
    private final RTree owner;
    private IndexNode parent;
    private long minLeftKey;
    private long maxRightKey;
    private int numElements;
    
    public TreeNode(RTree owner) {
        this.owner = owner;
        this.parent = null;
        this.minLeftKey = Long.MAX_VALUE;
        this.maxRightKey = Long.MIN_VALUE;
        this.numElements = 0;
    }
    
    @Override
    public int compareTo(TreeNode other) {
        if (other == null) return -1;
        return Long.signum(this.minLeftKey - other.minLeftKey);
    }
    
    public abstract void format(int indentDepth, StringBuffer output);
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        format(0, sb);
        return sb.toString();
    }
    
    public final RTree getOwner() {
        return this.owner;
    }
    
    public final IndexNode getParent() {
        return this.parent;
    }
    
    protected void setParent(IndexNode parent) {
        this.parent = parent;
    }
    
    public abstract void checkTree(String baseMessage);
    
    public final boolean isRoot() {
        return this.parent == null;
    }
    
    public final boolean isEmpty() {
        return this.numElements == 0;
    }
    
    public abstract boolean isLeaf();
    
    protected final void assertThat(boolean condition, String message) {
        if (!condition) {
            StringBuffer msg = new StringBuffer(message);
            msg.append('\n');
            msg.append(this);
            if (getLeftSibling() != null) {
                msg.append("LEFT: ");
                msg.append(getLeftSibling());
            }
            if (getRightSibling() != null) {
                msg.append("RIGHT: ");
                msg.append(getRightSibling());
            }
            throw new RuntimeException(msg.toString());
        }
    }
    
    public final TreeNode getRoot() {
        TreeNode node = this;
        while (node.getParent() != null) {
            node = node.getParent();
        }
        return node;
    }
    
    public abstract LeafNode findLeafNode(long key);
    
    public final int getNumElements() {
        return this.numElements;
    }
    
    public final void setNumElements(int numElements) {
        assert numElements >= 0;
        assert numElements <= this.owner.getOrder();
        this.numElements = numElements;
    }
    
    public final long getMinLeftKey() {
        return this.minLeftKey;
    }
    
    public final long getMaxRightKey() {
        return this.maxRightKey;
    }
    
    public final void resetBounds() {
        this.minLeftKey = Long.MAX_VALUE;
        this.maxRightKey = Long.MIN_VALUE;
    }
    
    public final void updateMinLeftKey(long leftKey) {
        this.minLeftKey = Math.min(this.minLeftKey, leftKey);
    }
    
    public final void updateMaxRightKey(long rightKey) {
        this.maxRightKey = Math.max(this.maxRightKey, rightKey);
    }

    public abstract TreeNode getLeftSibling();
    
    public abstract void setLeftSibling(TreeNode sibling);
    
    public abstract TreeNode getRightSibling();
    
    public abstract void setRightSibling(TreeNode sibling);
    
    public abstract boolean removeNode();
}
