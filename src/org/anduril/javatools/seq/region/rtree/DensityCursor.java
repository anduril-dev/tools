package org.anduril.javatools.seq.region.rtree;

import java.util.ArrayList;

import org.apache.commons.collections.primitives.ArrayDoubleList;
import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.ArrayLongList;

import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.MutableDNARegion;
import org.anduril.javatools.seq.region.Strand;

public final class DensityCursor {
    private final RTree rtree;
    private final MutableDNARegion exportRegion;
    private long startKey;
    private long endKey;
    private LeafNode leaf;
    private int leafPos;
    private boolean eor;
    
    private final ArrayIntList addQueueSequence;
    private final ArrayLongList addQueueLeft;
    private final ArrayLongList addQueueLength;
    private final ArrayList<Strand> addQueueStrand;
    private final ArrayLongList addQueueKeyHash;
    private final ArrayDoubleList addQueueScore;
    private final ArrayLongList removeQueue;
    
    public DensityCursor(RTree rtree) {
        this.rtree = rtree;
        this.exportRegion = new MutableDNARegion(rtree.getAnnotationRegistry());
        setRange(0, Long.MAX_VALUE);
        
        this.addQueueSequence = new ArrayIntList(16);
        this.addQueueLeft = new ArrayLongList(16);
        this.addQueueLength = new ArrayLongList(16);
        this.addQueueStrand = new ArrayList<Strand>();
        this.addQueueKeyHash = new ArrayLongList(16);
        this.addQueueScore = new ArrayDoubleList(16);
        this.removeQueue = new ArrayLongList(16);
    }
    
    public boolean endOfRange() {
        return this.eor;
    }
    
    public long setRange(long startKey, long endKey) {
        this.startKey = startKey;
        this.endKey = endKey;
        
        this.leaf = this.rtree.getRoot().findLeafNode(this.startKey+1);
        this.leafPos = this.leaf.getKeyIndex(this.startKey, true);
        
        if (this.leaf.isEmpty()) {
            this.eor = true;
            return 0;
        }
        
        if (this.leafPos >= this.leaf.getNumElements()) {
            this.leafPos--;
        }
        
        /* Check whether previous region intersects with range */
        if (this.leafPos > 0) {
            if (this.leaf.overlapsRegion(this.leafPos-1, startKey, endKey)) {
                this.leafPos--;
            }
        } else if (this.leaf.getLeftSibling() != null) {
            final LeafNode left = this.leaf.getLeftSibling();
            if (left.overlapsRegion(left.getNumElements()-1, startKey, endKey)) {
                this.leaf = left;
                this.leafPos = left.getNumElements()-1;
            }
        }
        
        if (this.leaf.overlapsRegion(this.leafPos, startKey, endKey)) {
            this.eor = false;
            return 0;
        } else {
            this.eor = true;
            final int sequenceID = KeyHandler.extractSequenceID(startKey);
            final Strand strand = KeyHandler.extractStrand(startKey);
            long leftKey = this.leaf.getKey(this.leafPos);
            if (KeyHandler.extractSequenceID(leftKey) == sequenceID && KeyHandler.extractStrand(leftKey) == strand) {
                return leftKey;
            } else {
                if (this.leafPos > 0) {
                    this.leafPos--;
                } else if (this.leaf.getLeftSibling() != null) {
                    this.leaf = this.leaf.getLeftSibling();
                    this.leafPos = this.leaf.getNumElements() - 1;
                } else {
                    return 0;
                }
                leftKey = this.leaf.getKey(this.leafPos);
                if (KeyHandler.extractSequenceID(leftKey) == sequenceID && KeyHandler.extractStrand(leftKey) == strand) {
                    return leftKey;
                } else {
                    return 0;
                }
            }
        }
    }
    
    public long setRange(DNARegion region) {
        return setRange(KeyHandler.makeKey(region, 0), KeyHandler.makeRightKey(region, RTree.MAX_TAG));
    }
    
    public void resetRange() {
        setRange(this.startKey, this.endKey);
    }
    
    public void forward() {
        this.leafPos++;
        if (this.leafPos >= this.leaf.getNumElements()) {
            final LeafNode right = this.leaf.getRightSibling();
            if (right == null) {
                this.eor = true;
                return;
            }
            this.leaf = right;
            this.leafPos = 0;
        }
        
        if (this.leaf.getKey(this.leafPos) >= this.endKey) {
            this.eor = true;
        }
    }
    
    public DNARegion getRegion() {
        this.leaf.exportRegion(this.leafPos, this.exportRegion);
        return this.exportRegion;
    }
    
    public double getScore() {
        return this.leaf.getScore(this.leafPos);
    }
    
    public void setScore(double score) {
        this.leaf.setScore(this.leafPos, score);
    }
    
    public long getKeyHash() {
        return this.leaf.getKeyHash(this.leafPos);
    }
    
    public void incKeyHash() {
        final int pos = this.leafPos;
        long old = this.leaf.getKeyHash(pos);
        this.leaf.setKeyHash(pos, old+1);
    }
    
    public void add(int sequenceID, long left, long length, Strand strand, long keyHash, double score) {
        if (sequenceID != DNARegion.SEQ_NONE && length > 0) {
            this.addQueueSequence.add(sequenceID);
            this.addQueueLeft.add(left);
            this.addQueueLength.add(length);
            this.addQueueStrand.add(strand == Strand.ANY ? Strand.FORWARD : strand);
            this.addQueueKeyHash.add(keyHash);
            this.addQueueScore.add(score);
        }
    }
    
    public void add(DNARegion region, long keyHash, double score) {
        add(region.getSequenceID(), region.getLeftPosition(), region.getLength(), region.getStrand(), keyHash, score);
    }
    
    public void remove() {
        final long key = this.leaf.getKey(this.leafPos);
        this.removeQueue.add(key);
    }
    
    public void commit() {
        final int removeSize = this.removeQueue.size();
        for (int i=0; i<removeSize; i++) {
            if (!this.rtree.remove(this.removeQueue.get(i))) {
                throw new RuntimeException(String.format(
                        "Could not remove region with key=%x", this.removeQueue.get(i)));
            }
        }
        this.removeQueue.clear();
        
        final int addSize = this.addQueueSequence.size();
        for (int i=0; i<addSize; i++) {
            final int sequenceID = this.addQueueSequence.get(i);
            final long left = this.addQueueLeft.get(i);
            final long length = this.addQueueLength.get(i);
            final Strand strand = this.addQueueStrand.get(i);
            final long keyHash = this.addQueueKeyHash.get(i);
            final double score = this.addQueueScore.get(i);
            this.exportRegion.replace(sequenceID, left, length, strand, keyHash);
            this.exportRegion.setDoubleAnnotation(this.rtree.getScoreAnnotationID(), score);
            if (!this.rtree.add(this.exportRegion)) {
                throw new RuntimeException("Could not add region: "+this.exportRegion);
            }
        }
        
        this.addQueueSequence.clear();
        this.addQueueLeft.clear();
        this.addQueueLength.clear();
        this.addQueueStrand.clear();
        this.addQueueKeyHash.clear();
        this.addQueueScore.clear();
    }
}
