package org.anduril.javatools.seq.region.rtree;

import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.Strand;

public final class KeyHandler {
    /* tag: 0-15, start: 16-55, strand: 56, sequence: 57-63 */
    public static final int SEQUENCE_BITS = 7;
    public static final int STRAND_BITS = 1;
    public static final int POS_BITS = 40;
    public static final int TAG_BITS = 16;
    
    private static final int SEQUENCE_SHIFT = STRAND_BITS + TAG_BITS + POS_BITS;
    private static final int STRAND_SHIFT = TAG_BITS + POS_BITS;
    private static final int POS_SHIFT = TAG_BITS;
    
    private static final long SEQUENCE_MASK = (1L << SEQUENCE_BITS) - 1L;
    private static final long STRAND_MASK = (1L << STRAND_BITS) - 1L;
    private static final long POS_MASK = (1L << POS_BITS) - 1L;
    private static final long TAG_MASK = (1L << TAG_BITS) - 1L;
    
    public static long makeKey(int sequenceID, long position, Strand strand, int tag) {
        final long strandBit = STRAND_BITS == 1 ? strand.oneBit : strand.twoBit;
        return
            ((long)sequenceID & SEQUENCE_MASK) << SEQUENCE_SHIFT |
            strandBit << STRAND_SHIFT |
            ((long)position & POS_MASK) << POS_SHIFT |
            ((long)tag & TAG_MASK);
    }
    
    public static long makeKey(DNARegion region, int tag) {
        return KeyHandler.makeKey(region.getSequenceID(), region.getLeftPosition(), region.getStrand(), tag);
    }

    public static long makeRightKey(DNARegion region, int tag) {
        if (region.isEmpty()) {
            return KeyHandler.makeKey(region.getSequenceID(), region.getLeftPosition(), region.getStrand(), tag);
        } else {
            return KeyHandler.makeKey(region.getSequenceID(), region.getRightPosition(true), region.getStrand(), tag);    
        }
    }
    
    public static long makeKey(long existingKey, long newPosition, int newTag) {
        final long bits = SEQUENCE_BITS + STRAND_BITS;
        final long mask = ((1L << bits)-1) << (64-bits); /* Mask for sequence and strand */
        return
            ((long)existingKey & mask) |
            ((long)newPosition & POS_MASK) << POS_SHIFT |
            ((long)newTag & TAG_MASK);
    }
    
    public static long makeKey(long existingKey, int newTag) {
        return
            ((long)existingKey & (~TAG_MASK)) |
            ((long)newTag & TAG_MASK);
    }

    public static int extractSequenceID(long key) {
        return (int)((key >> SEQUENCE_SHIFT) & SEQUENCE_MASK);
    }

    public static Strand extractStrand(long key) {
        int strandBit = (int)((key >> STRAND_SHIFT) & STRAND_MASK);
        return STRAND_BITS == 1 ? Strand.fromOneBit(strandBit) : Strand.fromTwoBit(strandBit);
    }
    
    public static long extractPosition(long key) {
        return (key >> POS_SHIFT) & POS_MASK;
    }
    
    public static int extractTag(long key) {
        return (int)(key & TAG_MASK);
    }
    
    public static boolean duplicateRegions(long key1, long key2) {
        return (key1 | TAG_MASK) == (key2 | TAG_MASK);
    }
}
