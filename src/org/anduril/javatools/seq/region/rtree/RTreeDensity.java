package org.anduril.javatools.seq.region.rtree;

import java.util.Iterator;

import org.anduril.javatools.seq.region.AggregateFunction;
import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.RegionPartition;
import org.anduril.javatools.seq.region.Strand;

public class RTreeDensity extends RTree implements RegionPartition {
    private final DensityCursor cursor;

    public RTreeDensity(int order, String regionSetName) {
        super(order, true, true, 0, regionSetName);
        this.cursor = new DensityCursor(this);
    }

    public RTreeDensity(String regionSetName) {
        this(DEFAULT_ORDER, regionSetName);
    }

    @Override
    public Iterator<DNARegion> iterator() {
        return new RTreeIterator(this);
        // return new PartitionIterator(this);
    }

    @Override
    public Iterator<DNARegion> getOverlapping(DNARegion query) {
        return new RTreeIterator(this, KeyHandler.makeKey(query, 0),
                KeyHandler.makeRightKey(query, MAX_TAG));
    }

    @Override
    public double getScore(int sequenceID, long position, Strand strand) {
        long key = KeyHandler.makeKey(sequenceID, position, strand, 0);
        Iterator<DNARegion> iter = new OverlapIterator(this, key,
                KeyHandler.makeKey(key, MAX_TAG));
        if (iter.hasNext()) {
            DNARegion reg = iter.next();
            return reg.getDoubleAnnotation(getScoreAnnotationID());
        } else {
            return 0;
        }
    }

    @Override
    public void modifyScore(final DNARegion span, final AggregateFunction func, final double value) {
        final DensityCursor cur = this.cursor;
        
        final long leftKey = cur.setRange(span);
        if (cur.endOfRange()) {
            final double initial = func.initial();
            final double updated = func.update(initial, value);
            
            DNARegion left = leftKey == 0 ? null : get(leftKey);
            final Strand spanStrand = span.getStrand() == Strand.ANY ? Strand.FORWARD : span.getStrand();
            if (left == null || left.getSequenceID() != span.getSequenceID() || left.getStrand() != spanStrand) {
                cur.add(span.getSequenceID(), 0L, span.getLeftPosition(), span.getStrand(), 1, initial);
                cur.add(span, 1, updated);
            } else {
                final long lastEnd = left.getRightPosition(false);
                cur.add(span.getSequenceID(), lastEnd, span.getLeftPosition()-lastEnd, span.getStrand(), 1, initial);
                cur.add(span, 1, updated);
            }
            cur.commit();
            return;
        }
        
        final int sequenceID = span.getSequenceID();
        
        while (!cur.endOfRange()) {
            final DNARegion reg = cur.getRegion();
            
            final double oldScore = reg.getDoubleAnnotation(getScoreAnnotationID());
            final double newScore = func.update(oldScore, value);
            
            if (oldScore != newScore) {
                if (span.getLeftPosition() > reg.getLeftPosition()) {
                    final long splitPos = span.getLeftPosition() - reg.getLeftPosition();
                    final long keyHash = cur.getKeyHash();

                    cur.remove();

                    /* Insert left half (old score) */
                    cur.add(sequenceID, reg.getLeftPosition(), splitPos, reg.getStrand(), keyHash+1, oldScore);

                    /* Insert right half (new score) */
                    cur.add(sequenceID, reg.getLeftPosition()+splitPos, reg.getLength()-splitPos, reg.getStrand(), keyHash+1, newScore);
                } else if (reg.getRightPosition(false) > span.getRightPosition(false)) {
                    final long splitPos = reg.getRightPosition(false) - span.getRightPosition(false);
                    final long keyHash = cur.getKeyHash();

                    cur.remove();

                    /* Insert left half (new score) */
                    cur.add(sequenceID, reg.getLeftPosition(), splitPos, reg.getStrand(), keyHash+1, newScore);

                    /* Insert right half (old score) */
                    cur.add(sequenceID, reg.getLeftPosition()+splitPos, reg.getLength()-splitPos, reg.getStrand(), keyHash+1, oldScore);
                } else {
                    cur.setScore(newScore);
                    cur.incKeyHash();
                }
            }
            
            cur.forward();
        }
        
        cur.commit();
    }
}
