package org.anduril.javatools.seq.region.rtree;

import java.util.ArrayDeque;
import java.util.Iterator;

import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.MutableDNARegion;
import org.anduril.javatools.utils.AbstractIterator;

class OverlapIterator extends AbstractIterator<DNARegion> implements Iterator<DNARegion> {
    private final MutableDNARegion region;
    private final long startKey;
    private final long endKey;
    private final ArrayDeque<LeafNode> leafQueue;
    private LeafNode curLeaf;
    private int nextLeafPos;
    
    public OverlapIterator(RTree rtree, long startKey, long endKey) {
        this.region = new MutableDNARegion(rtree.getAnnotationRegistry());
        this.startKey = startKey;
        this.endKey = endKey;
        
        this.leafQueue = new ArrayDeque<LeafNode>();
        if (rtree.getRoot() instanceof LeafNode) {
            this.leafQueue.add((LeafNode)rtree.getRoot());
        } else {
            traverseIndexNodes((IndexNode)rtree.getRoot());
        }
        
        this.curLeaf = null;
        this.nextLeafPos = 0;
    }
    
    private void traverseIndexNodes(IndexNode node) {
        if (!DNARegion.overlappingCoordinates(this.startKey, this.endKey, node.getMinLeftKey(), node.getMaxRightKey())) {
            return;
        }
        
        final int N = node.getNumElements();
        for (int i=0; i<N; i++) {
            final TreeNode child = node.getChild(i);
            if (child instanceof LeafNode) {
                if (DNARegion.overlappingCoordinates(this.startKey, this.endKey, child.getMinLeftKey(), child.getMaxRightKey())) {
                    this.leafQueue.add((LeafNode)child);
                }
            } else {
                traverseIndexNodes((IndexNode)child);
            }
        }
    }
    
    @Override
    protected DNARegion getNext() {
        while (true) {
            if (this.curLeaf == null || this.nextLeafPos >= this.curLeaf.getNumElements()) {
                if (this.leafQueue.isEmpty()) break;
                this.curLeaf = this.leafQueue.pop();
                this.nextLeafPos = 0;
            }

            final int N = this.curLeaf.getNumElements();
            while (this.nextLeafPos < N) {
                final boolean overlap = this.curLeaf.overlapsRegion(this.nextLeafPos, this.startKey, this.endKey);
                if (overlap) {
                    this.curLeaf.exportRegion(this.nextLeafPos, this.region);
                    this.nextLeafPos++;
                    return this.region;
                } else {
                    this.nextLeafPos++;
                }
            }
        }
        
        return null;
    }
}