package org.anduril.javatools.seq.region.rtree;

import java.util.Arrays;

public final class IndexNode extends TreeNode {
    private final TreeNode[] children;
    
    public IndexNode(RTree owner) {
        super(owner);
        final int order = owner.getOrder();
        this.children = new TreeNode[order];
    }

    @Override
    public void format(int indentDepth, StringBuffer output) {
        for (int i=0; i<indentDepth; i++) output.append(TreeNode.INDENT);
        output.append(String.format("Index, hash=%x, k=%d, minLeft=%x, maxRight=%x\n",
                hashCode(), this.getNumElements(), getMinLeftKey(), getMaxRightKey()));
        for (int elem=0; elem<getNumElements(); elem++) {
            TreeNode child = this.children[elem];
            child.format(indentDepth+1, output);
        }
    }
    
    @Override
    public void checkTree(String baseMessage) {
        final String localMessage = String.format("%s (%x)", baseMessage, hashCode());
        
        assertThat(this.getNumElements() <= this.children.length, localMessage);
        if (!isRoot()) {
            assertThat(this.getNumElements() > 0, localMessage);
            assertThat(this.getNumElements() >= getOwner().getMinFill(), localMessage);
        } else {
            assertThat(this.getNumElements() >= 0, localMessage);    
        }
        if (getNumElements() > 1) {
            assertThat(getMinLeftKey() < getMaxRightKey(), localMessage+": minLeft >= maxRight");
        }
        
        long prevChildMinLeft = Long.MIN_VALUE;
        long childMaxRight = Long.MIN_VALUE;
        boolean firstLeafSeen = false;
        boolean lastLeafSeen = false;
        for (int i=0; i<getNumElements(); i++) {
            TreeNode child = this.children[i];
            assertThat(child.getMinLeftKey() > prevChildMinLeft, localMessage+": Invalid child minLeftKey order");
            assertThat(child.getParent() == this, localMessage+": Invalid parent");
            if (i == 0) {
                assertThat(child.getMinLeftKey() == getMinLeftKey(), localMessage+": Invalid child minLeft");
            }
            childMaxRight = Math.max(childMaxRight, child.getMaxRightKey());
            prevChildMinLeft = child.getMinLeftKey();
            
            child.checkTree(baseMessage);
            
            if (child instanceof LeafNode) {
                LeafNode leaf = (LeafNode)child;
                if (leaf.getLeftSibling() == null) {
                    assertThat(!firstLeafSeen, localMessage+": Several left-most leaf nodes");
                    firstLeafSeen = true;
                }
                if (leaf.getRightSibling() == null) {
                    assertThat(!lastLeafSeen, localMessage+": Several right-most leaf nodes");
                    lastLeafSeen = true;
                }
            }
        }
        assertThat(childMaxRight == getMaxRightKey(), localMessage+": Invalid maxRight");
    }
    
    @Override
    public LeafNode findLeafNode(long key) {
        for (int i=1; i<this.getNumElements(); i++) {
            final TreeNode child = this.children[i];
            if (child.getMinLeftKey() > key) {
                return this.children[i-1].findLeafNode(key);
            }
        }
        
        if (isEmpty()) return null;
        else return this.children[getNumElements()-1].findLeafNode(key);
    }
    
    @Override
    public final boolean isLeaf() {
        return false;
    }
    
    public TreeNode getChild(int index) {
        if (index >= getNumElements()) {
            throw new IndexOutOfBoundsException("Index out of bounds: "+index);
        }
        return this.children[index];
    }
    
    private void copyChildrenSelf(int sourcePos, int destPos, int length) {
        System.arraycopy(this.children, sourcePos, this.children, destPos, length);
    }
    
    private void moveChildren(int sourcePos, IndexNode destination, int destPos, int length) {
        for (int i=0; i<length; i++) {
            destination.children[destPos+i] = this.children[sourcePos+i];
            destination.children[destPos+i].setParent(destination);
        }
    }
    
    private void insertChild(TreeNode child) {
        int pos = Arrays.binarySearch(this.children, 0, getNumElements(), child);
        if (pos < 0) pos = -pos - 1;
        copyChildrenSelf(pos, pos+1, getNumElements()-pos);
        this.children[pos] = child;
    }
    
    private void scanBounds() {
        resetBounds();
        for (int i=0; i<getNumElements(); i++) {
            updateMinLeftKey(this.children[i].getMinLeftKey());
            updateMaxRightKey(this.children[i].getMaxRightKey());
        }
    }
    
    private void sort() {
        Arrays.sort(this.children, 0, getNumElements());
    }
    
    public void updateBounds(TreeNode child) {
        scanBounds();
        if (!isRoot()) getParent().updateBounds(this);
    }
    
    public void addChild(TreeNode child) {
        final int numChildren = getNumElements();
        
        if (numChildren < this.children.length) {
            updateMinLeftKey(child.getMinLeftKey());
            updateMaxRightKey(child.getMaxRightKey());
            insertChild(child);
            child.setParent(this);
            setNumElements(numChildren+1);
            if (!isRoot()) getParent().updateBounds(this);
        } else {
            IndexNode rightSibling = new IndexNode(getOwner());
            final int leftSize = numChildren/2;
            final int rightSize = numChildren - leftSize;
            moveChildren(leftSize, rightSibling, 0, rightSize);
            setNumElements(leftSize);
            rightSibling.setNumElements(rightSize);
            scanBounds();
            rightSibling.scanBounds();
            setRightSibling(rightSibling);
            rightSibling.setLeftSibling(this);

            if (child.getMinLeftKey() >= rightSibling.getMinLeftKey()) {
                rightSibling.addChild(child);
            } else {
                this.addChild(child);
            }
            
            if (isRoot()) {
                IndexNode newRoot = new IndexNode(getOwner());
                newRoot.addChild(this);
            }
            getParent().addChild(rightSibling);
        }
    }
    
    private int getChildIndex(TreeNode child) {
        return Arrays.binarySearch(this.children, 0, getNumElements(), child);
    }
    
    public boolean removeChild(TreeNode child) {
        sort(); // TODO: this sort should be optimized
        final int pos = getChildIndex(child);
        if (pos < 0) return false;
        
        if (!isEmpty()) {
            setNumElements(getNumElements()-1);
            copyChildrenSelf(pos+1, pos, getNumElements()-pos);
            scanBounds();
            sort();
            if (!isRoot() && !isEmpty()) getParent().updateBounds(this);
        }

        if (isEmpty() && !isRoot()) {
            boolean ok = getParent().removeChild(this);
            if (!ok) {
                String msg = String.format("%x: Could not remove empty index from parent", hashCode());
                throw new RuntimeException(msg);
            }
        }
        
        return true;
    }
    
    @Override
    public boolean removeNode() {
        if (!isEmpty() || !isRoot()) return false;
        return getParent().removeChild(this);
    }

    @Override
    public TreeNode getLeftSibling() {
        return null;
    }
    
    @Override
    public void setLeftSibling(TreeNode sibling) { }
    
    @Override
    public TreeNode getRightSibling() {
        return null;
    }
    
    @Override
    public void setRightSibling(TreeNode sibling) { }

}
