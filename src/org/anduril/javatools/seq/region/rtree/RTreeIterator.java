package org.anduril.javatools.seq.region.rtree;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.MutableDNARegion;
import org.anduril.javatools.utils.AbstractIterator;

class RTreeIterator extends AbstractIterator<DNARegion> implements Iterator<DNARegion> {
    private final RTree rtree;
    private final MutableDNARegion region;
    private final long endKey;
    private LeafNode curLeaf;
    private int nextLeafPos;
    private int validVersion;
    
    public RTreeIterator(RTree rtree, long startKey, long endKey) {
        this.rtree = rtree;
        this.region = new MutableDNARegion(rtree.getAnnotationRegistry());
        this.endKey = endKey;
        this.validVersion = rtree.version;
        
        if (startKey <= 0) {
            this.curLeaf = rtree.getFirstLeaf();
            this.nextLeafPos = 0;
        } else {
            this.curLeaf = rtree.getRoot().findLeafNode(startKey);
            this.nextLeafPos = this.curLeaf.getKeyIndex(startKey, true);
        }
    }
    
    public RTreeIterator(RTree rtree) {
        this(rtree, 0, Long.MAX_VALUE);
    }
    
    @Override
    protected DNARegion getNext() {
        if (this.curLeaf == null) return null;
        
        if (rtree.version != this.validVersion) {
            throw new ConcurrentModificationException("Tree was modified during iteration");
        }

        while (this.nextLeafPos >= this.curLeaf.getNumElements()) {
            this.curLeaf = this.curLeaf.getRightSibling();
            if (this.curLeaf == null) return null;
            this.nextLeafPos = 0;
        }
        
        final long key = this.curLeaf.getKey(this.nextLeafPos);
        if (key > this.endKey) {
            this.curLeaf = null;
            return null;
        }
        
        this.curLeaf.exportRegion(this.nextLeafPos, this.region);
        this.nextLeafPos++;
        return this.region;
    }
    
    @Override
    public void remove() {
        if (this.curLeaf == null || this.nextLeafPos == 0) {
            throw new IllegalStateException("Iterator is not active");
        }
        
        final long key = this.curLeaf.getKey(this.nextLeafPos - 1);
        boolean ok = rtree.remove(key);
        if (!ok) {
            throw new RuntimeException(String.format("Could not remove key: %x", key));
        }
        this.nextLeafPos--;
        
        this.validVersion = rtree.version;
    }
}