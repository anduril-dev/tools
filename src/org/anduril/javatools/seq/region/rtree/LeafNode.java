package org.anduril.javatools.seq.region.rtree;

import java.util.Arrays;

import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.MutableDNARegion;

public final class LeafNode extends TreeNode {
    private final long[] keys;
    private final int[] lengths;
    private final double[] score;
    private final long[] keyHash;
    private LeafNode leftSibling;
    private LeafNode rightSibling;
    
    public LeafNode(RTree owner) {
        super(owner);
        final int order = owner.getOrder();
        this.keys = new long[order];
        this.lengths = new int[order];
        this.score = owner.getHasScore() ? new double[order] : null;
        this.keyHash = new long[order]; //owner.getIsPartition() ? null : new long[order];
    }
    
    @Override
    public void format(int indentDepth, StringBuffer output) {
        for (int i=0; i<indentDepth; i++) output.append(TreeNode.INDENT);
        
        String left = this.leftSibling == null ? null : String.format("%x", this.leftSibling.hashCode());
        String right = this.rightSibling == null ? null : String.format("%x", this.rightSibling.hashCode());
        output.append(String.format("Leaf, hash=%x, k=%d, minLeft=%x, maxRight=%x, left=%s, right=%s\n",
                hashCode(), this.getNumElements(), getMinLeftKey(), getMaxRightKey(), left, right));
        
        MutableDNARegion target = new MutableDNARegion(getOwner().getAnnotationRegistry());
        for (int elem=0; elem<this.getNumElements(); elem++) {
            for (int i=0; i<indentDepth+1; i++) output.append(TreeNode.INDENT);
            exportRegion(elem, target);
            output.append(String.format("0x%x: ", this.keys[elem]));
            output.append(target);
            output.append('\n');
        }
    }

    @Override
    public void checkTree(String baseMessage) {
        final String localMessage = String.format("%s (%x)", baseMessage, hashCode());
        
        assertThat(this.getNumElements() <= this.keys.length, localMessage);
        if (!isRoot()) {
            assertThat(this.getNumElements() > 0, localMessage);
            assertThat(this.getNumElements() >= getOwner().getMinFill(), localMessage);
        } else {
            assertThat(this.getNumElements() >= 0, localMessage);    
        }
        
        if (getNumElements() > 1) {
            assertThat(getMinLeftKey() < getMaxRightKey(), localMessage+": minLeft >= maxRight");
        }
        
        if (getLeftSibling() != null) {
            assertThat(getLeftSibling().getMinLeftKey() < getMinLeftKey(), localMessage);
            assertThat(!getLeftSibling().isEmpty(), localMessage+": left sibling is empty");
            assertThat(getLeftSibling().getRightSibling() == this, localMessage);
        }

        if (getRightSibling() != null) {
            assertThat(getRightSibling().getMinLeftKey() > getMinLeftKey(), localMessage);
            assertThat(!getRightSibling().isEmpty(), localMessage+": right sibling is empty");
            assertThat(getRightSibling().getLeftSibling() == this, localMessage);
        }
        
        long prevKey = Long.MIN_VALUE;
        long maxRight = Long.MIN_VALUE;
        final int numElements = getNumElements();
        for (int i=0; i<numElements; i++) {
            assertThat(this.keys[i] > prevKey, localMessage+": Invalid key order");
            if (i == 0) {
                assertThat(this.keys[i] == getMinLeftKey(), localMessage+": Invalid minLeft");
            } else if (getOwner().getIsPartition()) {
                assertThat(!overlapsRegion(i-1, this.keys[i], getRightKey(i)), localMessage+": Overlapping regions");
            }
            maxRight = Math.max(maxRight, getRightKey(i));
            prevKey = this.keys[i];
        }
        assertThat(maxRight == getMaxRightKey(), localMessage+": Invalid maxRight");
    }
    
    @Override
    public LeafNode findLeafNode(long key) {
        return this;
    }
    
    @Override
    public final boolean isLeaf() {
        return true;
    }

    @Override
    public LeafNode getLeftSibling() {
        return this.leftSibling;
    }
    
    @Override
    public void setLeftSibling(TreeNode sibling) {
        if (sibling == this) {
            throw new IllegalArgumentException("sibling == this");
        }
        this.leftSibling = (LeafNode)sibling;
    }
    
    @Override
    public LeafNode getRightSibling() {
        return this.rightSibling;
    }
    
    @Override
    public void setRightSibling(TreeNode sibling) {
        if (sibling == this) {
            throw new IllegalArgumentException("sibling == this");
        }
        this.rightSibling = (LeafNode)sibling;
    }
    
    public boolean contains(long key) {
        final int pos = Arrays.binarySearch( this.keys, 0, getNumElements(), key);
        return pos >= 0;
    }
    
    public long getKey(int index) {
        if (index >= getNumElements()) {
            throw new IndexOutOfBoundsException("Index out of bounds: "+index);
        }
        return this.keys[index];
    }
    
    public void exportRegion(int index, MutableDNARegion targetRegion) {
        if (index >= getNumElements()) {
            throw new IndexOutOfBoundsException("Index out of bounds: "+index);
        }

        final long key = this.keys[index];
        targetRegion.setSequenceID(KeyHandler.extractSequenceID(key));
        targetRegion.setLeftPosition(KeyHandler.extractPosition(key));
        targetRegion.setStrand(KeyHandler.extractStrand(key));
        targetRegion.setLength(this.lengths[index]);
        if (this.keyHash != null) {
            targetRegion.setKeyHash(this.keyHash[index]);
        }
        if (this.score != null) {
            targetRegion.setDoubleAnnotation(getOwner().getScoreAnnotationID(), this.score[index]);
        }
    }
    
    public boolean overlapsRegion(int index, long leftKey, long rightKey) {
        if (index >= getNumElements()) {
            throw new IndexOutOfBoundsException("Index out of bounds: "+index);
        }
        
        if (KeyHandler.extractSequenceID(leftKey) == DNARegion.SEQ_NONE) return false;
        
        final long myLeftKey = this.keys[index];
        if (KeyHandler.extractSequenceID(myLeftKey) == DNARegion.SEQ_NONE) return false;
        final long myRightKey = getRightKey(index);
        return DNARegion.overlappingCoordinates(leftKey, rightKey, myLeftKey, myRightKey);
    }
    
    public double getScore(int index) {
        if (index >= getNumElements()) {
            throw new IndexOutOfBoundsException("Index out of bounds: "+index);
        }
        return this.score[index];
    }
    
    public void setScore(int index, double score) {
        if (index >= getNumElements()) {
            throw new IndexOutOfBoundsException("Index out of bounds: "+index);
        }
        this.score[index] = score;
    }
    
    public long getKeyHash(int index) {
        if (index >= getNumElements()) {
            throw new IndexOutOfBoundsException("Index out of bounds: "+index);
        }
        return this.keyHash[index];
    }
    
    public void setKeyHash(int index, long keyHash) {
        if (index >= getNumElements()) {
            throw new IndexOutOfBoundsException("Index out of bounds: "+index);
        }
        this.keyHash[index] = keyHash;
    }
    
    private void copyRegions(int sourcePos, LeafNode destination, int destPos, int length) {
        System.arraycopy(this.keys, sourcePos, destination.keys, destPos, length);
        System.arraycopy(this.lengths, sourcePos, destination.lengths, destPos, length);
        if (this.score != null) {
            System.arraycopy(this.score, sourcePos, destination.score, destPos, length);
        }
        if (this.keyHash != null) {
            System.arraycopy(this.keyHash, sourcePos, destination.keyHash, destPos, length);
        }
    }
    
    private long getRightKey(int index) {
        final long key = this.keys[index];
        final int length = this.lengths[index];
        if (length == 0) {
            return KeyHandler.makeKey(key, KeyHandler.extractTag(key));
        } else {
            return KeyHandler.makeKey(
                    key,
                    KeyHandler.extractPosition(key) + this.lengths[index] - 1,
                    KeyHandler.extractTag(key));
        }
    }
    
    private void scanBounds() {
        resetBounds();
        
        if (isEmpty()) {
            updateMinLeftKey(0);
            updateMaxRightKey(1);
        } else {
            final int N = getNumElements();
            for (int i=0; i<N; i++) {
                updateMinLeftKey(this.keys[i]);
                updateMaxRightKey(getRightKey(i));
            }
        }
    }
    
    private void insertRegion(long key, int length, double score, long keyHash) {
        int pos = Arrays.binarySearch(this.keys, 0, getNumElements(), key);
        if (pos < 0) pos = -pos - 1;
        copyRegions(pos, this, pos+1, getNumElements()-pos);
        this.keys[pos] = key;
        this.lengths[pos] = length;
        if (this.score != null) this.score[pos] = score;
        if (this.keyHash != null) this.keyHash[pos] = keyHash;
    }
    
    public long getInsertKey(DNARegion region) {
        long firstKey = KeyHandler.makeKey(region, RTree.FIRST_TAG);
        long lastKey = getPreviousKey(KeyHandler.makeKey(region, RTree.MAX_TAG));
        if (!KeyHandler.duplicateRegions(firstKey, lastKey)) {
            return firstKey;
        } else {
            int prevTag = KeyHandler.extractTag(lastKey);
            if (prevTag >= RTree.MAX_TAG) {
                throw new RuntimeException("Can not add region: too many duplicate regions: "+region);
            }
            return KeyHandler.makeKey(lastKey, prevTag+1);
        }
    }
    
    public int getKeyIndex(long key, boolean alwaysPositive) {
        int pos = Arrays.binarySearch(this.keys, 0, getNumElements(), key);
        if (alwaysPositive && pos < 0) {
            pos = -pos - 1;
        }
        return pos;
    }
    
    private long getPreviousKey(long queryKey) {
        int pos = Arrays.binarySearch(this.keys, 0, getNumElements(), queryKey);
        if (pos < 0) pos = -pos - 1;
        
        if (pos == 0) return 0;
        else return this.keys[pos-1];
    }
    
    public long addRegion(DNARegion region, long key) {
        final int numElements = getNumElements();
        
        if (contains(key)) {
            String msg = String.format("Can not add key %x: duplicate key; region = %s", key, region);
            throw new RuntimeException(msg);
        }
        
        if (numElements < this.keys.length) {
            final double score;
            if (this.score == null || region.getAnnotationRegistry() != getOwner().getAnnotationRegistry()) {
                score = 0;
            } else {
                score = region.getDoubleAnnotation(getOwner().getScoreAnnotationID());
            }
            insertRegion(key, (int)region.getLength(), score, region.getKeyHash());
            updateMinLeftKey(key);
            updateMaxRightKey(KeyHandler.makeRightKey(region, KeyHandler.extractTag(key)));
            setNumElements(numElements + 1);
            if (!isRoot()) getParent().updateBounds(this);
        } else {
            LeafNode rightSibling = new LeafNode(getOwner());
            final int leftSize = numElements/2;
            final int rightSize = numElements - leftSize;
            copyRegions(leftSize, rightSibling, 0, rightSize);
            setNumElements(leftSize);
            rightSibling.setNumElements(rightSize);
            scanBounds();
            rightSibling.scanBounds();
            
            if (getRightSibling() != null) {
                getRightSibling().setLeftSibling(rightSibling);
            }
            rightSibling.setLeftSibling(this);
            rightSibling.setRightSibling(getRightSibling());
            setRightSibling(rightSibling);
            
            if (isRoot()) {
                IndexNode newRoot = new IndexNode(getOwner());
                newRoot.addChild(this);
            }
            getParent().addChild(rightSibling);
            
            if (key > rightSibling.getMinLeftKey()) {
                rightSibling.addRegion(region, key);
            } else {
                this.addRegion(region, key);
            }
        }
        
        return key;
    }
    
    public boolean removeRegion(long key) {
        final int pos = Arrays.binarySearch(this.keys, 0, getNumElements(), key);
        if (pos < 0) return false;
        setNumElements(getNumElements()-1);
        copyRegions(pos+1, this, pos, getNumElements()-pos);
        
        if (isEmpty()) {
            removeNode();
        } else {
            scanBounds();
            if (!isRoot()) getParent().updateBounds(this);
        }
        
        return true;
    }

    @Override
    public boolean removeNode() {
        if (!isEmpty() || isRoot()) {
            return false;
        }
        
        if (getLeftSibling() != null) {
            getLeftSibling().setRightSibling(getRightSibling());
        }
        if (getRightSibling() != null) {
            getRightSibling().setLeftSibling(getLeftSibling());
        }
        
        return getParent().removeChild(this);
    }
    
    public boolean isFull() {
        return this.getNumElements() == this.keys.length;
    }
}
