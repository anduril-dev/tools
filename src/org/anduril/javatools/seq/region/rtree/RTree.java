package org.anduril.javatools.seq.region.rtree;

import java.util.Iterator;

import org.apache.commons.collections15.iterators.IteratorChain;

import org.anduril.javatools.seq.region.AnnotationRegistry;
import org.anduril.javatools.seq.region.AnnotationType;
import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.MutableDNARegion;
import org.anduril.javatools.seq.region.RegionStore;
import org.anduril.javatools.seq.region.Strand;

public class RTree implements RegionStore {
    public static final int DEFAULT_ORDER = 64;
    public static final String ANN_LOCATION_KEY = "LocationKey";
    public static final int FIRST_TAG = 0x1;
    public static final int MAX_TAG = (1 << (KeyHandler.TAG_BITS-1)) - 1;
    public static final boolean DEBUG = false;
    
    private final int order;
    private final boolean isPartition;
    private final boolean hasScore;
    private final int minFill;
    private final String regionSetName;
    private final AnnotationRegistry registry;
    private final int scoreAnnotationID;
    private final MutableDNARegion exportRegion;
    private LeafNode firstLeaf;
    private TreeNode root;
    protected int version;
    
    public RTree(int order, boolean isPartition, boolean hasScore, double minFillFactor, String regionSetName) {
        this.order = order;
        this.isPartition = isPartition;
        this.hasScore = hasScore;
        this.minFill = (int)Math.floor(order*minFillFactor);
        this.regionSetName = regionSetName;
        this.firstLeaf = new LeafNode(this);
        this.root = this.firstLeaf;
        this.version = 0;
        
        this.registry = new AnnotationRegistry();
        this.scoreAnnotationID = hasScore ?
                this.registry.registerAnnotation(AnnotationRegistry.ANNOTATION_SCORE, AnnotationType.DOUBLE) : -1;
        this.exportRegion = new MutableDNARegion(this.registry);
    }
    
    public RTree(int order) {
        this(order, false, true, 0, null);
    }
    
    public RTree() {
        this(DEFAULT_ORDER);
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        this.root.format(0, sb);
        return sb.toString();
    }
    
    public final int getOrder() {
        return this.order;
    }
    
    public final TreeNode getRoot() {
        return this.root;
    }
    
    public final LeafNode getFirstLeaf() {
        return this.firstLeaf;
    }
    
    public final boolean getIsPartition() {
        return this.isPartition;
    }
    
    public final boolean getHasScore() {
        return this.hasScore;
    }
    
    public final int getMinFill() {
        return this.minFill;
    }
    
    @Override
    public final AnnotationRegistry getAnnotationRegistry() {
        return this.registry;
    }
    
    public final int getScoreAnnotationID() {
        return this.scoreAnnotationID;
    }
    
    public long addGetKey(DNARegion region) {
        if (contains(region)) return 0;
        
        final long tailKey = KeyHandler.makeKey(region, MAX_TAG);
        final LeafNode leaf = this.root.findLeafNode(tailKey);
        final long key = leaf.getInsertKey(region);
        assert key > 0;
        if (RTree.DEBUG) System.out.println(String.format("Add %x (%s) to <<%s>>", key, region, leaf));
        leaf.addRegion(region, key);
        this.root = this.root.getRoot();
        this.version++;
        return key;
    }
    
    @Override
    public boolean add(DNARegion region) {
        return addGetKey(region) != 0;
    }
    
    @Override
    public void addAll(Iterator<DNARegion> iter) {
        while (iter.hasNext()) add(iter.next());
    }
    
    @Override
    public void close() {
        this.root = null;
    }
    
    @Override
    public String getRegionSetName() {
        return this.regionSetName;
    }
    
    public DNARegion get(long key) {
        final LeafNode leaf = this.root.findLeafNode(key);
        if (leaf == null) return null;
        
        final int index = leaf.getKeyIndex(key, false);
        if (index < 0) return null;
        
        leaf.exportRegion(index, this.exportRegion);
        return this.exportRegion;
    }
    
    @Override
    public DNARegion get(DNARegion query) {
        final long startKey = KeyHandler.makeKey(query, 0);
        final long endKey = KeyHandler.makeKey(query, MAX_TAG);
        final Iterator<DNARegion> iter = getRange(startKey, endKey);
        while (iter.hasNext()) {
            final DNARegion region = iter.next();
            if (query.matches(region, false, true)) {
                return region;
            }
        }
        return null;
    }
    
    @Override
    public boolean contains(DNARegion region) {
        return get(region) != null;
    }
    
    @Override
    public boolean containsLocation(DNARegion region) {
        final long startKey = KeyHandler.makeKey(region, 0);
        final long endKey = KeyHandler.makeKey(region, MAX_TAG);
        final Iterator<DNARegion> iter = getRange(startKey, endKey);
        return iter.hasNext();
    }
    
    public boolean contains(long key) {
        LeafNode leaf = this.root.findLeafNode(key);
        if (leaf == null) return false;
        return leaf.contains(key);
    }
    
    @Override
    public boolean remove(DNARegion region) {
        final long startKey = KeyHandler.makeKey(region, 0);
        final long endKey = KeyHandler.makeKey(region, MAX_TAG);
        final Iterator<DNARegion> iter = getRange(startKey, endKey);
        while (iter.hasNext()) {
            final DNARegion reg = iter.next();
            if (reg.matches(region, false, true)) {
                iter.remove();
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void removeAll(Iterator<DNARegion> iter) {
        while (iter.hasNext()) remove(iter.next());
    }
    
    public boolean remove(long key) {
        final LeafNode leaf = this.root.findLeafNode(key);
        if (leaf == null) return false;
        final boolean wasRemoved = leaf.removeRegion(key);
        if (wasRemoved) this.version++;
        if (this.root.isEmpty() && !this.root.isLeaf()) {
            this.firstLeaf = new LeafNode(this);
            this.root = this.firstLeaf;
        }
        return wasRemoved;
    }
    
    @Override
    public Iterator<DNARegion> iterator() {
        return new RTreeIterator(this);
    }
    
    public Iterator<DNARegion> getRange(long startKey, long endKey) {
        return new RTreeIterator(this, startKey, endKey);
    }

    private Iterator<DNARegion> getRange(DNARegion region, Strand strand) {
        if (region.getStrand() == Strand.ANY && strand == null) {
            IteratorChain<DNARegion> iterChain = new IteratorChain<DNARegion>();
            iterChain.addIterator(getRange(region, Strand.FORWARD));
            iterChain.addIterator(getRange(region, Strand.REVERSE));
            return iterChain;
        } else {
            return getRange(
                    KeyHandler.makeKey(region, 0),
                    KeyHandler.makeRightKey(region, region.isEmpty() ? 0 : MAX_TAG));
        }
    }
    
    @Override
    public Iterator<DNARegion> getRange(DNARegion region) {
        return getRange(region, null);
    }
    
    public Iterator<DNARegion> getOverlapping(long startKey, long endKey) {
        return new OverlapIterator(this, startKey, endKey);
    }

    private Iterator<DNARegion> getOverlapping(DNARegion region, Strand strand) {
        if (region.getStrand() == Strand.ANY && strand == null) {
            IteratorChain<DNARegion> iterChain = new IteratorChain<DNARegion>();
            iterChain.addIterator(getOverlapping(region, Strand.FORWARD));
            iterChain.addIterator(getOverlapping(region, Strand.REVERSE));
            return iterChain;
        } else {
            return getOverlapping(
                    KeyHandler.makeKey(region, 0),
                    KeyHandler.makeRightKey(region, region.isEmpty() ? 0 : MAX_TAG));
        }
    }
    
    @Override
    public Iterator<DNARegion> getOverlapping(DNARegion region) {
        return getOverlapping(region, null);
    }
}
