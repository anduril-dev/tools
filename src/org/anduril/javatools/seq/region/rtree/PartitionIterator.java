package org.anduril.javatools.seq.region.rtree;

import java.util.ConcurrentModificationException;

import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.MutableDNARegion;
import org.anduril.javatools.utils.AbstractIterator;

public class PartitionIterator extends AbstractIterator<DNARegion> {
    private final RTree rtree;
    private final MutableDNARegion region;
    private final long endKey;
    private LeafNode curLeaf;
    private int curLeafPos;
    private long nextEmitKey;
    private boolean lastEmittedGap;
    private int validVersion;

    public PartitionIterator(RTree rtree, long startKey, long endKey) {
        this.rtree = rtree;
        this.region = new MutableDNARegion(rtree.getAnnotationRegistry());
        this.endKey = endKey;
        this.validVersion = rtree.version;
    
        if (startKey <= 0) {
            this.curLeaf = rtree.getFirstLeaf();
            this.curLeafPos = 0;
            this.nextEmitKey = KeyHandler.makeKey(this.curLeaf.getKey(0), 0, RTree.FIRST_TAG);
        } else {
            this.curLeaf = rtree.getRoot().findLeafNode(startKey);
            this.curLeafPos = this.curLeaf.getKeyIndex(startKey, true);
            this.nextEmitKey = startKey;
        }
    }
    
    public PartitionIterator(RTree rtree) {
        this(rtree, 0, Long.MAX_VALUE);
    }

    @Override
    protected DNARegion getNext() {
        if (this.curLeaf == null) return null;
        if (this.nextEmitKey > this.endKey) return null;
        
        if (rtree.version != this.validVersion) {
            throw new ConcurrentModificationException("Tree was modified during iteration");
        }
        
        while (this.curLeafPos >= this.curLeaf.getNumElements()) {
            this.curLeaf = this.curLeaf.getRightSibling();
            if (this.curLeaf == null) return null;
            this.curLeafPos = 0;
        }
        
        final long curKey = this.curLeaf.getKey(this.curLeafPos);
        
        if (this.nextEmitKey < curKey) {
            final long emitPos = KeyHandler.extractPosition(this.nextEmitKey);
            final long gap = KeyHandler.extractPosition(curKey) - emitPos;
            this.region.replace(
                    KeyHandler.extractSequenceID(this.nextEmitKey),
                    emitPos,
                    gap,
                    KeyHandler.extractStrand(this.nextEmitKey),
                    0);
            this.region.setDoubleAnnotation(this.rtree.getScoreAnnotationID(), 0);
            this.nextEmitKey = curKey;
            this.lastEmittedGap = true;
            return this.region;
        } else {
            this.curLeaf.exportRegion(this.curLeafPos, this.region);
            this.curLeafPos++;
            this.nextEmitKey = KeyHandler.makeKey(curKey, this.region.getRightPosition(false), RTree.FIRST_TAG);
            this.lastEmittedGap = false;
            return this.region;
        }
    }
    
    @Override
    public void remove() {
        if (this.curLeaf == null || this.curLeafPos == 0) {
            throw new IllegalStateException("Iterator is not active");
        }
        
        if (this.lastEmittedGap) return;
        
        final long key = this.curLeaf.getKey(this.curLeafPos - 1);
        boolean ok = rtree.remove(key);
        if (!ok) {
            throw new RuntimeException(String.format("Could not remove key: %x", key));
        }
        this.curLeafPos--;
        
        this.validVersion = rtree.version;
    }    
}
