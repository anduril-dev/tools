package org.anduril.javatools.seq.region;

import java.util.Iterator;

public interface RegionStore extends RegionReader, RegionWriter {
    public DNARegion get(DNARegion region);
    public boolean contains(DNARegion region);
    public boolean containsLocation(DNARegion region);
    public Iterator<DNARegion> getRange(DNARegion region);
    public Iterator<DNARegion> getOverlapping(DNARegion query);
    public boolean remove(DNARegion region);
    public void removeAll(Iterator<DNARegion> iter);
}
