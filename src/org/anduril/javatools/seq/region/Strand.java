package org.anduril.javatools.seq.region;

public enum Strand {
    FORWARD(1, 1),
    REVERSE(0, 2),
    ANY(1, 3);
    
    public final int oneBit;
    public final int twoBit;
    
    private Strand(int oneBit, int twoBit) {
        this.oneBit = oneBit;
        this.twoBit = twoBit;
    }
    
    public String getStrandString(String forwardStrand, String reverseStrand, String anyString) {
        switch (this) {
        case FORWARD:
            return forwardStrand;
        case REVERSE:
            return reverseStrand;
        case ANY:
            return anyString;
        default:
            throw new RuntimeException("Can not convert strand to string: "+this);
        }
    }
    
    public Strand flip() {
        switch (this) {
        case FORWARD:
            return REVERSE;
        case REVERSE:
            return FORWARD;
        case ANY:
            return ANY;
        default:
            throw new RuntimeException("Can not flip strand: "+this);
        }
    }
    
    public boolean isCompatible(Strand other) {
        if (this == ANY || other == ANY) return true;
        else return this == other;
    }
    
    public static Strand fromOneBit(final int oneBit) {
        if (oneBit == 0) return REVERSE;
        else return FORWARD;
    }
    
    public static Strand fromTwoBit(final int twoBit) {
        if (twoBit == 1) return FORWARD;
        else if (twoBit == 2) return REVERSE;
        else return ANY;
    }
    
    public static Strand[] forwardReverse() {
        return new Strand[] { FORWARD, REVERSE };
    }
}
