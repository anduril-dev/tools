package org.anduril.javatools.seq.region;

import java.util.Iterator;

public interface RegionPartition extends RegionReader {
    public double getScore(int sequenceID, long position, Strand strand);
    public void modifyScore(DNARegion span, AggregateFunction func, double value);
    public Iterator<DNARegion> getOverlapping(DNARegion query);
}
