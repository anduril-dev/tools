package org.anduril.javatools.seq.region;

public class AttributeTransformer extends RegionFilter {
    private final boolean flipStrand;
    private final Strand fixedStrand;
    private final int shift;
    private final int expandStart;
    private final int expandEnd;
    
    public AttributeTransformer(RegionReader reader,
            boolean flipStrand, Strand fixedStrand,
            int shift,
            int expandStart, int expandEnd) {
        super(reader);
        this.flipStrand = flipStrand;
        this.fixedStrand = fixedStrand == null ? Strand.ANY : fixedStrand;
        this.shift = shift;
        this.expandStart = expandStart;
        this.expandEnd = expandEnd;
    }
    
    @Override
    public boolean accept(DNARegion region) {
        return true;
    }

    @Override
    public DNARegion transform(DNARegion region) {
        final boolean doFlip = this.flipStrand || (this.fixedStrand != Strand.ANY && region.getStrand() != this.fixedStrand);
        return region.getImmutable().transform(doFlip, this.shift,
                this.expandStart, this.expandEnd);
    }
}