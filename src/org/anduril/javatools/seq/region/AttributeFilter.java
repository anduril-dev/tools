package org.anduril.javatools.seq.region;

import java.util.regex.Pattern;

import org.anduril.javatools.seq.ChromosomeSet;

public class AttributeFilter extends RegionFilter {
    private final long minLength;
    private final long maxLength;
    private final Strand strand;
    private final Pattern chrPattern;
    private final ChromosomeSet chrSet;
    
    public AttributeFilter(RegionReader reader,
            long minLength, long maxLength,
            Strand strand,
            String chrPattern, ChromosomeSet chrSet) {
        super(reader);
        
        if (minLength < 0) {
            throw new IllegalArgumentException("minLength is negative: "+minLength);
        }
        if (minLength > maxLength) {
            throw new IllegalArgumentException(String.format(
                    "minLength (%d) > maxLength (%d)", minLength, maxLength));
        }
        this.minLength = minLength;
        this.maxLength = maxLength;
        
        this.strand = strand == null ? Strand.ANY : strand;
        
        if (chrPattern == null) {
            this.chrPattern = null;
            this.chrSet = null;
        } else {
            if (chrSet == null) {
                throw new IllegalArgumentException("When chrPattern is given, chrSet must also be given");
            }
            this.chrPattern = Pattern.compile(chrPattern);
            this.chrSet = chrSet;
        }
    }
    
    public static AttributeFilter getLengthFilter(RegionReader reader, long minLength, long maxLength) {
        return new AttributeFilter(reader, minLength, maxLength, null, null, null);
    }

    public static AttributeFilter getSequenceFilter(RegionReader reader, String chrPattern, ChromosomeSet chrSet) {
        return new AttributeFilter(reader, 0, Integer.MAX_VALUE, null, chrPattern, chrSet);
    }

    public static AttributeFilter getStrandFilter(RegionReader reader, Strand strand) {
        return new AttributeFilter(reader, 0, Integer.MAX_VALUE, strand, null, null);
    }
    
    @Override
    public boolean accept(DNARegion region) {
        final long length = region.getLength();
        if (length < this.minLength || length > this.maxLength) return false;
        
        if (this.strand != Strand.ANY && region.getStrand() != this.strand) {
            return false;
        }
        
        if (this.chrPattern == null) {
            return true;
        } else {
            final String chrName = this.chrSet.getName(region.getSequenceID());
            return chrName != null && this.chrPattern.matcher(chrName).matches();
        }
    }
}
