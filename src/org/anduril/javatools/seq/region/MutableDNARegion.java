package org.anduril.javatools.seq.region;

public class MutableDNARegion extends DNARegion {
    public static final int SEQ_NONE = -1;
    
    public MutableDNARegion(AnnotationRegistry annRegistry,
            int sequenceID, long leftPosition, long length, Strand strand, long keyHash) {
        super(annRegistry, sequenceID, leftPosition, length, strand, keyHash);
    }

    public MutableDNARegion(AnnotationRegistry annRegistry) {
        this(annRegistry, SEQ_NONE, 0, 0, Strand.ANY, 0);
    }

    @Override
    public ImmutableDNARegion getImmutable() {
        return new ImmutableDNARegion(this);
    }
    
    public void setKeyHash(long keyHash) {
        this.keyHash = keyHash;
    }
    
    public void setKeyHash(String key) {
        this.keyHash = (key == null ? 0 : key.hashCode());
    }
    
    public void setSequenceID(int sequenceID) {
        this.sequenceID = sequenceID;
    }

    public void setLeftPosition(long leftPosition) {
        this.left = leftPosition;
    }
    
    public void setLength(long length) {
        this.length = length;
    }
    
    public void setRightPosition(long rightPosition, boolean inclusive) {
        this.length = rightPosition - this.left;
        if (inclusive) this.length--;
        if (this.length < 0) {
            throw new IllegalArgumentException("Invalid rightPosition: length becomes negative: "+rightPosition);
        }
    }

    public void replace(int sequenceID, long leftPosition, long length, Strand strand, long keyHash) {
        this.sequenceID = sequenceID;
        this.left = leftPosition;
        this.length = length;
        this.strand = strand;
        this.keyHash = keyHash;
    }
    
    public void replaceSpan(int sequenceID, long leftPosition, long rightPosition, Strand strand, long keyHash, boolean rightInclusive) {
        long length = rightPosition - leftPosition;
        if (rightInclusive) length++;
        replace(sequenceID, leftPosition, length, strand, keyHash);
    }
    
    public void replace(DNARegion region) {
        this.sequenceID = region.getSequenceID();
        this.left = region.getLeftPosition();
        this.length = region.getLength();
        this.strand = region.getStrand();
        this.keyHash = region.getKeyHash();
    }
    
    public void setStrand(Strand strand) {
        this.strand = strand;
    }
}
