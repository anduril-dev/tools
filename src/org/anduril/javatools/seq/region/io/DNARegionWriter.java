package org.anduril.javatools.seq.region.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;

import org.anduril.asser.io.CSVWriter;
import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.region.AnnotationRegistry;
import org.anduril.javatools.seq.region.AnnotationType;
import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.RegionWriter;

public class DNARegionWriter implements RegionWriter {
    private static final int FIXED_COLUMNS = 5;
    
    private final ChromosomeSet sequenceSet;
    private final AnnotationRegistry registry;
    private final CSVWriter writer;
    private final int idAnnotation;
    
    public DNARegionWriter(OutputStream outStream, ChromosomeSet sequenceSet, AnnotationRegistry registry) {
        this.sequenceSet = sequenceSet;
        this.registry = registry;
        
        int extraAnnotation;
        if (registry != null) {
            this.idAnnotation = registry.getAnnotationID(AnnotationRegistry.ANNOTATION_ID);
            extraAnnotation = registry.getNumAnnotations(null);
            if (this.idAnnotation >= 0) extraAnnotation--;
        } else {
            this.idAnnotation = -1;
            extraAnnotation = 0;
        }
        
        String[] columns = new String[FIXED_COLUMNS+extraAnnotation];
        columns[0] = "ID";
        columns[1] = "Chromosome";
        columns[2] = "Start";
        columns[3] = "Length";
        columns[4] = "Strand";

        if (registry != null) {
            int index = FIXED_COLUMNS;
            for (String extraAnn: registry.getAnnotations(null)) {
                if (extraAnn.equals(AnnotationRegistry.ANNOTATION_ID)) continue;
                columns[index] = extraAnn;
                index++;
            }
        }
        
        this.writer = new CSVWriter(columns, outStream);
    }
    
    public DNARegionWriter(File outFile, ChromosomeSet sequenceSet, AnnotationRegistry registry) throws IOException {
        this(new FileOutputStream(outFile), sequenceSet, registry);
    }
    
    @Override
    public boolean add(DNARegion region) {
        if (this.idAnnotation < 0) {
            this.writer.write(this.writer.getLineNumber()-1);
        } else {
            this.writer.write(region.getStringAnnotation(this.idAnnotation));
        }
        
        this.writer.write(this.sequenceSet.getName(region.getSequenceID()), false);
        this.writer.write(region.getLeftPosition());
        this.writer.write(region.getLength());
        this.writer.write(region.getStrand().getStrandString("1", "-1", null));

        if (this.registry != null) {
            for (String extraAnn: this.registry.getAnnotations(null)) {
                if (extraAnn.equals(AnnotationRegistry.ANNOTATION_ID)) continue;
                final AnnotationType type = this.registry.getType(extraAnn);
                final int annotationID = this.registry.getAnnotationID(extraAnn);
                switch (type) {
                case DOUBLE:
                    this.writer.write(region.getDoubleAnnotation(annotationID));
                    break;
                case LONG:
                    this.writer.write(region.getLongAnnotation(annotationID));
                    break;
                case STRING:
                    this.writer.write(region.getStringAnnotation(annotationID));
                    break;
                default:
                    throw new RuntimeException("Unknown annotation type: "+type);
                }
            }
        }
        
        return true;
    }

    @Override
    public void addAll(Iterator<DNARegion> iterator) {
        while (iterator.hasNext()) {
            add(iterator.next());
        }
    }

    @Override
    public void close() {
        this.writer.close();
    }

}