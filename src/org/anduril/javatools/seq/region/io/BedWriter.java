package org.anduril.javatools.seq.region.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Iterator;

import org.anduril.asser.io.CSVWriter;
import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.region.AnnotationRegistry;
import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.RegionWriter;

public class BedWriter implements RegionWriter {
    private final ChromosomeSet sequenceSet;
    private final CSVWriter writer;
    private final int idAnnotation;
    private final int scoreAnnotation;
    
    public BedWriter(OutputStream outStream, ChromosomeSet sequenceSet, AnnotationRegistry registry, String trackName) throws IOException {
        if (trackName != null) {
            String header = String.format("track name=%s\n", trackName);
            outStream.write(header.getBytes(Charset.forName("UTF-8")));
        }
        
        if (registry != null) {
            this.idAnnotation = registry.getAnnotationID(AnnotationRegistry.ANNOTATION_ID);
            this.scoreAnnotation = registry.getAnnotationID(AnnotationRegistry.ANNOTATION_SCORE);
        } else {
            this.idAnnotation = -1;
            this.scoreAnnotation = -1;
        }
        
        this.sequenceSet = sequenceSet;
        this.writer = new CSVWriter(6, outStream, "\t");
    }
    
    public BedWriter(File bedFile, ChromosomeSet sequenceSet, AnnotationRegistry registry, String trackName) throws IOException {
        this(new FileOutputStream(bedFile), sequenceSet, registry, trackName);
    }
    
    public BedWriter(File bedFile, ChromosomeSet sequenceSet, AnnotationRegistry registry) throws IOException {
        this(bedFile, sequenceSet, registry, null);
    }
    
    @Override
    public boolean add(DNARegion region) {
        this.writer.write(this.sequenceSet.getName(region.getSequenceID()), false);
        this.writer.write(region.getLeftPosition());
        this.writer.write(region.getRightPosition(false));
        
        if (this.idAnnotation >= 0) {
            this.writer.write(region.getStringAnnotation(this.idAnnotation), false);
        } else {
            this.writer.write("", false);
        }
        
        if (this.scoreAnnotation >= 0) {
            String score = String.valueOf(region.getDoubleAnnotation(this.scoreAnnotation));
            if (score.endsWith(".0")) score = score.substring(0, score.length()-2);
            this.writer.write(score, false);
        } else {
            this.writer.write(0);
        }
        
        this.writer.write(region.getStrand().getStrandString("+", "-", null), false);
        
        return true;
    }

    @Override
    public void addAll(Iterator<DNARegion> iter) {
        while (iter.hasNext()) {
            add(iter.next());
        }
    }

    public void close() {
        this.writer.close();
    }
}
