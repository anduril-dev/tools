package org.anduril.javatools.seq.region.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import org.apache.commons.collections.primitives.ArrayIntList;

import org.anduril.asser.io.CSVParser;
import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.region.AnnotationRegistry;
import org.anduril.javatools.seq.region.AnnotationType;
import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.MutableDNARegion;
import org.anduril.javatools.seq.region.RegionReader;
import org.anduril.javatools.seq.region.Strand;
import org.anduril.javatools.utils.AbstractIterator;
import org.anduril.javatools.utils.IOUtils;

public class DNARegionReader extends AbstractIterator<DNARegion> implements RegionReader {
    private final ChromosomeSet sequenceSet;
    private final CSVParser parser;
    private final MutableDNARegion region;
    private final AnnotationRegistry registry;
    private String name;
    
    private final int idColumn;
    private final int chrColumn;
    private final int startColumn;
    private final int lengthColumn;
    private final int strandColumn;
    private final int scoreColumn;
    
    private final int idAnnotation;
    private final int scoreAnnotation;
    
    private final int[] extraColumns;
    private final int[] extraAnnotation;
    
    public DNARegionReader(BufferedReader reader, ChromosomeSet sequenceSet) throws IOException {
        this.sequenceSet = sequenceSet;
        this.parser = new CSVParser(reader, CSVParser.DEFAULT_DELIMITER, 0, CSVParser.DEFAULT_MISSING_VALUE_SYMBOL, false);

        this.idColumn = this.parser.getColumnIndex("ID", false);
        this.chrColumn = this.parser.getColumnIndex("Chromosome");
        this.startColumn = this.parser.getColumnIndex("Start");
        this.lengthColumn = this.parser.getColumnIndex("Length");
        this.strandColumn = this.parser.getColumnIndex("Strand", false);
        this.scoreColumn = this.parser.getColumnIndex("Score", false);

        this.registry = new AnnotationRegistry();
        
        if (this.idColumn >= 0) {
            this.idAnnotation = this.registry.registerAnnotation(AnnotationRegistry.ANNOTATION_ID, AnnotationType.STRING);
        } else {
            this.idAnnotation = -1;
        }
        
        if (this.scoreColumn >= 0) {
            this.scoreAnnotation = this.registry.registerAnnotation(AnnotationRegistry.ANNOTATION_SCORE, AnnotationType.DOUBLE);
        } else {
            this.scoreAnnotation = -1;
        }
        
        ArrayIntList extraColumnsList = new ArrayIntList();
        ArrayIntList extraAnnotationList = new ArrayIntList();
        for (int index=0; index<this.parser.getColumnCount(); index++) {
            final boolean knownColumn =
                index == this.idColumn ||
                index == this.chrColumn ||
                index == this.startColumn ||
                index == this.lengthColumn ||
                index == this.strandColumn ||
                index == this.scoreColumn;
            if (knownColumn) continue;

            final String colName = this.parser.getColumnNames()[index];
            extraColumnsList.add(index);
            extraAnnotationList.add(this.registry.registerAnnotation(colName, AnnotationType.STRING));
        }
        this.extraColumns = extraColumnsList.toArray();
        this.extraAnnotation = extraAnnotationList.toArray();
        
        this.region = new MutableDNARegion(this.registry);
    }
    
    public DNARegionReader(File regionFile, ChromosomeSet sequenceSet) throws IOException {
        this(IOUtils.openGZIPFileReader(regionFile), sequenceSet);
        if (this.name == null) this.name = regionFile.getName();
    }
    
    @Override
    protected DNARegion getNext() {
        while (this.parser.hasNext()) {
            final String[] row = this.parser.next();
            if (row == null) break;
            
            final String chromo = row[this.chrColumn];
            final int chromoID;
            if (chromo == null) chromoID = DNARegion.SEQ_NONE;
            else chromoID = this.sequenceSet.getChromosomeID(chromo, true, true);
            
            final long left = Long.parseLong(row[this.startColumn]);
            final long length = Long.parseLong(row[this.lengthColumn]);
            
            final Strand strand;
            if (this.strandColumn >= 0) {
                String strandValue = row[this.strandColumn];
                if (strandValue == null) strand = Strand.ANY;
                else if (strandValue.equals("1")) strand = Strand.FORWARD;
                else if (strandValue.equals("-1")) strand = Strand.REVERSE;
                else {
                    throw new RuntimeException("Invalid strand: "+strandValue);
                }
            } else {
                strand = Strand.ANY;
            }
            
            final long keyHash;
            if (this.idColumn >= 0) {
                this.region.setStringAnnotation(this.idAnnotation, row[this.idColumn]);
                keyHash = DNARegion.makeKeyHash(row[this.idColumn]);
            } else {
                keyHash = 0;
            }
            
            this.region.replace(chromoID, left, length, strand, keyHash);
            
            if (this.scoreColumn >= 0) {
                final double score = row[this.scoreColumn] == null ?
                        0 : Double.parseDouble(row[this.scoreColumn]);
                this.region.setDoubleAnnotation(this.scoreAnnotation, score);
            }
            
            for (int i=0; i<this.extraColumns.length; i++) {
                final int columnIndex = this.extraColumns[i];
                final int annotationID = this.extraAnnotation[i];
                this.region.setStringAnnotation(annotationID, row[columnIndex]);
            }
            
            return this.region;
        }
        
        close();
        return null;
    }

    @Override
    public AnnotationRegistry getAnnotationRegistry() {
        return this.registry;
    }

    @Override
    public String getRegionSetName() {
        return this.name;
    }
    
    @Override
    public void close() {
        this.parser.close();
    }
}
