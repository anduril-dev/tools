package org.anduril.javatools.seq.region.io;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.region.AnnotationRegistry;
import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.RegionReader;
import org.anduril.javatools.utils.IteratorException;

public class FileRegionReader implements RegionReader {
    public static final String FORMAT_AUTO = "auto";
    public static final String FORMAT_BED = "bed";
    public static final String FORMAT_CSV = "csv";
    
    private final File regionFile;
    private final ChromosomeSet sequenceSet;
    private final String format;
    private final AnnotationRegistry registry;
    private final String name;
    private boolean closed;
    
    public FileRegionReader(File regionFile, ChromosomeSet sequenceSet, String format) throws IOException {
        this.regionFile = regionFile;
        this.sequenceSet = sequenceSet;
        this.format = format;
        this.closed = false;
        
        final RegionReader initialReader = getReader(this.format);
        this.registry = initialReader.getAnnotationRegistry();
        this.name = initialReader.getRegionSetName();
        initialReader.close();
    }
    
    public FileRegionReader(File regionFile, ChromosomeSet sequenceSet) throws IOException {
        this(regionFile, sequenceSet, FORMAT_AUTO);
    }

    @Override
    public String toString() {
        return String.format("FileRegionReader: %s", this.regionFile);
    }
    
    private RegionReader getReader(String format) throws IOException {
        if (format.equals(FORMAT_BED)) {
            return new BedReader(this.regionFile, this.sequenceSet);
        } else if (format.equals(FORMAT_CSV)) {
            return new DNARegionReader(this.regionFile, this.sequenceSet);
        } else if (format.equals(FORMAT_AUTO)) {
            final String name = this.regionFile.getName().toLowerCase();
            if (name.endsWith(".bed") || name.endsWith(".bed.gz")) {
                return getReader(FORMAT_BED);
            }
            else {
                return getReader(FORMAT_CSV);
            }
        } else {
            throw new IllegalArgumentException("Unknown format: "+this.format);
        }
    }
    
    @Override
    public Iterator<DNARegion> iterator() {
        if (this.closed) {
            throw new RuntimeException("Can not get iterator: reader is closed");
        }
        try {
            return getReader(this.format).iterator();
        } catch (IOException e) {
            throw new IteratorException("I/O error reading file", e);
        }
    }

    @Override
    public AnnotationRegistry getAnnotationRegistry() {
        return this.registry;
    }

    @Override
    public String getRegionSetName() {
        return this.name;
    }

    @Override
    public void close() {
        this.closed = true;
    }
}
