package org.anduril.javatools.seq.region.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import org.anduril.asser.io.CSVParser;
import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.region.AnnotationRegistry;
import org.anduril.javatools.seq.region.AnnotationType;
import org.anduril.javatools.seq.region.DNARegion;
import org.anduril.javatools.seq.region.MutableDNARegion;
import org.anduril.javatools.seq.region.RegionReader;
import org.anduril.javatools.seq.region.Strand;
import org.anduril.javatools.utils.AbstractIterator;
import org.anduril.javatools.utils.IOUtils;

public class BedReader extends AbstractIterator<DNARegion> implements RegionReader {
    private final ChromosomeSet sequenceSet;
    private final CSVParser parser;
    private final MutableDNARegion region;
    private final AnnotationRegistry registry;
    private final int idAnnotation;
    private final int scoreAnnotation;
    private String name;
    
    public BedReader(BufferedReader reader, ChromosomeSet sequenceSet) throws IOException {
        reader.mark(1024);
        String header = reader.readLine();
        int skip = header.startsWith("track") ? 1 : 0;
        for (String entry: header.split("[ \t]+")) {
            String[] tokens = entry.split("=", 2);
            if (tokens.length == 2 && tokens[0].trim().equals("name")) {
                this.name = tokens[1].trim();
            }
        }
        reader.reset();
        
        this.sequenceSet = sequenceSet;
        this.parser = new CSVParser(reader, CSVParser.DEFAULT_DELIMITER, skip,
                CSVParser.DEFAULT_MISSING_VALUE_SYMBOL, true, false);
        this.registry = new AnnotationRegistry();
        
        final int numColumns = this.parser.getColumnCount();
        if (numColumns < 3) {
            throw new IOException("Too few columns in BED file");
        }
        
        if (numColumns >= 4) {
            this.idAnnotation = this.registry.registerAnnotation(AnnotationRegistry.ANNOTATION_ID, AnnotationType.STRING);
        } else {
            this.idAnnotation = -1;
        }
        
        if (numColumns >= 5) {
            this.scoreAnnotation = this.registry.registerAnnotation(AnnotationRegistry.ANNOTATION_SCORE, AnnotationType.DOUBLE);
        } else {
            this.scoreAnnotation = -1;
        }
        
        this.region = new MutableDNARegion(this.registry);
    }
    
    public BedReader(File bedFile, ChromosomeSet sequenceSet) throws IOException {
        this(IOUtils.openGZIPFileReader(bedFile), sequenceSet);
        if (this.name == null) this.name = bedFile.getName();
    }
    
    @Override
    protected DNARegion getNext() {
        while (this.parser.hasNext()) {
            final String[] row = this.parser.next();
            if (row == null) break;
            final int N = row.length;

            final String chromo = row[0];
            final int chromoID = this.sequenceSet.getChromosomeID(chromo, true, true);
            if (chromoID < 0) continue;

            final Strand strand;
            if (N >= 6) {
                if (row[5].equals("+")) strand = Strand.FORWARD;
                else if (row[5].equals("-")) strand = Strand.REVERSE;
                else {
                    throw new RuntimeException("Invalid strand: "+row[5]);
                }
            } else {
                strand = Strand.ANY;
            }

            final long keyHash;
            if (N >= 4) {
                this.region.setStringAnnotation(this.idAnnotation, row[3]);
                keyHash = DNARegion.makeKeyHash(row[3]);
            } else {
                keyHash = 0;
            }
            
            final long left = Long.parseLong(row[1]);
            final long right = Long.parseLong(row[2]);
            this.region.replaceSpan(chromoID, left, right, strand, keyHash, false);

            if (N >= 5) {
                this.region.setDoubleAnnotation(this.scoreAnnotation, Double.parseDouble(row[4]));
            }

            return this.region;
        }
        
        this.parser.close();
        return null;
    }
    
    @Override
    public AnnotationRegistry getAnnotationRegistry() {
        return this.registry;
    }

    @Override
    public String getRegionSetName() {
        return this.name;
    }
    
    @Override
    public void close() {
        this.parser.close();
    }
}
