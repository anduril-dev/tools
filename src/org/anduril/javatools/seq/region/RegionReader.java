package org.anduril.javatools.seq.region;

import java.util.Iterator;

public interface RegionReader extends Iterable<DNARegion> {
    public Iterator<DNARegion> iterator();
    public AnnotationRegistry getAnnotationRegistry();
    public String getRegionSetName();
    public void close();
}
