package org.anduril.javatools.seq.region;

public class ImmutableDNARegion extends DNARegion {
    public ImmutableDNARegion(AnnotationRegistry annRegistry,
            int sequenceID, long leftPosition, long length, Strand strand, long keyHash) {
        super(annRegistry, sequenceID, leftPosition, length, strand, keyHash);
    }
    
    public ImmutableDNARegion(DNARegion template) {
        super(template.getAnnotationRegistry(), template.getSequenceID(), template.getLeftPosition(),
                template.getLength(), template.getStrand(), template.getKeyHash());
        copyAnnotations(template);
    }
    
    @Override
    public ImmutableDNARegion getImmutable() {
        return this;
    }
    
    public ImmutableDNARegion transform(boolean flipStrand, int shift, int expandStart, int expandEnd) {
        final Strand strand = flipStrand ? getStrand().flip() : getStrand();
        
        final long expandLeft;
        final long expandRight;
        if (strand == Strand.REVERSE) {
            expandLeft = Math.min(getLeftPosition(), expandEnd + shift);
            expandRight = expandStart - shift;
        } else {
            expandLeft = Math.min(getLeftPosition(), expandStart - shift);
            expandRight = expandEnd + shift;
        }
        
        final long left = getLeftPosition() - expandLeft;
        final long length = Math.max(0, getLength() + expandLeft + expandRight);
        
        ImmutableDNARegion newRegion = new ImmutableDNARegion(
                getAnnotationRegistry(),
                getSequenceID(), left, length, strand, getKeyHash());
        newRegion.copyAnnotations(this);
        return newRegion;
    }
}
