package org.anduril.javatools.seq;

import java.util.Arrays;

import org.apache.commons.math.stat.StatUtils;

import org.anduril.javatools.seq.pattern.solver.AbstractSolver;

public class ArrayTools {
    public static String arrayToString(double[] array, int digits, String separator) {
        final String format = "%."+digits+"f";
        StringBuffer sb = new StringBuffer();
        for (int i=0; i<array.length; i++) {
            if (i > 0) sb.append(separator);
            sb.append(String.format(format, array[i]));
        }
        return sb.toString();
    }
    
    public static String arrayToString(double[] array, int digits) {
        return arrayToString(array, digits, ",");
    }
    
    public static String arrayToString(double[] array) {
        return arrayToString(array, 2, ",");
    }

    public static double[] computeFraction(double[] nominator, double[] denominator) {
        if (nominator == null || denominator == null) return null;
        
        if (nominator.length != denominator.length) {
            throw new IllegalArgumentException("Arrays must have same length");
        }
        
        final int N = nominator.length;
        double[] fractions = new double[N];
        for (int seg=0; seg<N; seg++) {
            final double nominVal = nominator[seg];
            final double denominVal = denominator[seg];
            if (denominVal != 0) {
                fractions[seg] = nominVal / denominVal; 
            }
        }
        return fractions;
    }
    
    public static double[] flatten(final double[][] arrays) {
        int totalLength = 0;
        for (double[] array: arrays) {
            if (array != null) totalLength += array.length;
        }
        
        final double[] result = new double[totalLength];
        int pos = 0;
        for (double[] array: arrays) {
            if (array != null) {
                System.arraycopy(array, 0, result, pos, array.length);
                pos += array.length;
            }
        }

        return result;
    }
    
    public static double[] medianFilter(double[] array, int windowSize) {
        if (array.length < 2 || windowSize < 2) {
            return Arrays.copyOf(array, array.length);
        }
        
        final int middle = windowSize / 2;
        final int last = array.length - 1;
        final double[] buffer = new double[windowSize];
        final double[] result = new double[array.length];
        
        for (int i=0; i<array.length; i++) {
            for (int w=0; w<windowSize; w++) {
                int pos = i-middle+w;
                if (pos < 0) pos = 0;
                else if (pos > last) pos = last;
                buffer[w] = array[pos];
            }
            Arrays.sort(buffer);
            result[i] = buffer[middle];
        }
        
        return result;
    }
    
    public static double[][] preprocess(double[][] levels, int medianWindow, double backgroundPercentile) {
        final double[][] result = new double[levels.length][];
        for (int sample=0; sample<levels.length; sample++) {
            result[sample] = ArrayTools.medianFilter(levels[sample], medianWindow);
        }
        if (backgroundPercentile > 0) {
            ArrayTools.subtractBackground(result, backgroundPercentile);
        }
        return result;
    }

    public static double[] preprocess(double[] levels, int medianWindow, double backgroundPercentile) {
        final double[] result = ArrayTools.medianFilter(levels, medianWindow);
        if (backgroundPercentile > 0) {
            ArrayTools.subtractBackground(result, backgroundPercentile);
        }
        return result;
    }
    
    public static void subtractBackground(double[] array, double percentile) {
        if (percentile < 0 || percentile > 1) {
            throw new IllegalArgumentException("Percentile is out of bounds");
        }
        final double bg = StatUtils.percentile(array, percentile*100);
        for (int i=0; i<array.length; i++) {
            array[i] -= bg;
            if (array[i] < 0) array[i] = 0;
        }
    }

    public static void subtractBackground(double[][] arrays, double percentile) {
        if (percentile < 0 || percentile > 1) {
            throw new IllegalArgumentException("Percentile is out of bounds");
        }

        int totalLength = 0;
        for (double[] array: arrays) totalLength += array.length;
        double[] combined = new double[totalLength];
        
        int pos = 0;
        for (double[] array: arrays) {
            System.arraycopy(array, 0, combined, pos, array.length);
            pos += array.length;
        }

        final double bg = StatUtils.percentile(combined, percentile*100);
        for (double[] array: arrays) {
            for (int i=0; i<array.length; i++) {
                array[i] -= bg;
                if (array[i] < 0) array[i] = 0;
            }
        }
    }
    
    public static void reverse(double[] array) {
        final int N = array.length;
        for(int i=0; i<N/2; i++) {
            double tmp = array[i];
            array[i] = array[N-1-i];
            array[N-1-i] = tmp;
        }
    }

    /**
     * Transform a matrix into reduced row echelon
     * form using Gauss-Jordan elimination with partial
     * pivoting.
     * 
     * @param matrix The matrix.
     * @param beginColumn Column where to begin the elimination.
     * @param numColumns Number of columns for which to apply
     *  elimination. 
     */
    public static void rref(double[][] matrix, int beginColumn, int numColumns) {
        if (matrix.length == 0) return;
        
        final double EPSILON = 1e-10;
        final int numRows = matrix.length;
        final int numCols = matrix[0].length;
        
        int targetRow = 0;
        for (int i=0; i<numColumns; i++) {
            if (targetRow >= numRows) break;
            
            /* Select the row with the largest absolute value as the pivot */
            int pivotRow = targetRow;
            double pivotValue = Math.abs(matrix[pivotRow][beginColumn+targetRow]);
            for (int row=targetRow; row<numRows; row++) {
                final double value = Math.abs(matrix[row][beginColumn+i]);
                if (value > pivotValue) {
                    pivotRow = row;
                    pivotValue = value;
                }
            }
            
            if (pivotValue < EPSILON) continue; /* All zeroes */
            
            if (pivotRow != targetRow) {
                /* Swap rows targetRow and pivotRow so that pivot value
                 * is on targetRow */ 
                double[] tmp = matrix[targetRow];
                matrix[targetRow] = matrix[pivotRow];
                matrix[pivotRow] = tmp;
            }
            pivotRow = -1;
            pivotValue = matrix[targetRow][beginColumn+i];
            
            if (Math.abs(pivotValue - 1) > EPSILON) {
                /* Normalize current row so that pivot is 1 */
                for (int col=0; col<numCols; col++) {
                    matrix[targetRow][col] /= pivotValue;
                }
            }
            
            /* Subtract a multiple of current row from each
             * other row. */
            for (int row=0; row<numRows; row++) {
                if (row == targetRow) continue;
                final double factor = matrix[row][beginColumn+i];
                for (int col=0; col<numCols; col++) {
                    matrix[row][col] -= matrix[targetRow][col] * factor;
                }
            }
            
            targetRow++;
        }
    }
    
    public static void rref(double[][] matrix) {
        if (matrix.length == 0) return;
        rref(matrix, 0, matrix[0].length);
    }
    
    public static void shuffle(final double[] array, final int start, final int length) {
        if (start < 0 || start >= array.length) {
            throw new IllegalArgumentException("start is out of range: "+start);
        }
        if (length < 0 || start+length > array.length) {
            throw new IllegalArgumentException("length is out of range: "+length);
        }
        
        for (int i=length; i>1; i--) { 
            final int pos1 = start+AbstractSolver.random.nextInt(i);
            final int pos2 = start+i-1;
            final double tmp = array[pos1];
            array[pos1] = array[pos2];
            array[pos2] = tmp;
        }
    }
    
    public static void shuffle(final Object[] array) {
        shuffle(array, 0, array.length);
    }

    public static void shuffle(final Object[] array, final int start, final int length) {
        if (start < 0 || start >= array.length) {
            throw new IllegalArgumentException("start is out of range: "+start);
        }
        if (length < 0 || start+length > array.length) {
            throw new IllegalArgumentException("length is out of range: "+length);
        }
        
        for (int i=length; i>1; i--) { 
            final int pos1 = start+AbstractSolver.random.nextInt(i);
            final int pos2 = start+i-1;
            final Object tmp = array[pos1];
            array[pos1] = array[pos2];
            array[pos2] = tmp;
        }
    }
    
    public static void shuffle(final double[] array) {
        shuffle(array, 0, array.length);
    }
    
    public static double[] toDoubleArray(int[] array) {
        if (array == null) return null;
        double[] result = new double[array.length];
        for (int i=0; i<array.length; i++) result[i] = array[i];
        return result;
    }
}
