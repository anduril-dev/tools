package org.anduril.javatools.seq.cli;

import java.io.File;
import java.io.IOException;

import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.FixedDenseBinner;
import org.anduril.javatools.seq.io.MappabilityReader;

public class Mappability2Wiggle {

    public static void main(String[] args) throws IOException {
        if (args.length != 4) {
            System.err.println("Usage: Bed2Wiggle bin-size fragment-size chromosome-set input.bed > output.wig");
            System.exit(1);
        }

        final int binSize = Integer.parseInt(args[0]);
        final int fragmentSize = Integer.parseInt(args[1]);
        final File chrFile = new File(args[2]);
        final File inputBed = new File(args[3]);
        
        final ChromosomeSet chrSet = new ChromosomeSet();
        chrSet.readSizesFromCSV(chrFile);
        
        MappabilityReader reader = new MappabilityReader(chrSet, binSize, fragmentSize);
        FixedDenseBinner binner = reader.readMappability(inputBed);
        reader.writeWiggle(binner);
    }

}
