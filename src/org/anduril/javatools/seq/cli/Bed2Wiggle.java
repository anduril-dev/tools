package org.anduril.javatools.seq.cli;

import java.io.File;

import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.DNARegion;
import org.anduril.javatools.seq.FixedDenseBinner;
import org.anduril.javatools.seq.io.BedReader;

public class Bed2Wiggle {
    public static void main(String[] args) throws Exception {
        if (args.length != 3) {
            System.err.println("Usage: Bed2Wiggle bin-size elongate chromosome-set < input.bed > output.wig");
            System.exit(1);
        }
        
        final int binSize = Integer.parseInt(args[0]);
        final int elongate = Integer.parseInt(args[1]);
        final ChromosomeSet chrSet = new ChromosomeSet();
        final File chrFile = new File(args[2]);
        chrSet.readSizesFromCSV(chrFile);
        
        BedReader reader = new BedReader(System.in, chrSet, null, null);
        reader.setElongate(elongate);
        reader.setAddChromosome(false);
        reader.setMaxDuplicateReads(3);
        final FixedDenseBinner binner = new FixedDenseBinner(binSize, chrSet);
        try {
            for (DNARegion region: reader) {
                binner.addRegion(region);
            }
        } finally {
            reader.close();    
        }
        
        binner.normalize(1e9);
        
        final String name = reader.getName() == null ? "noname" : reader.getName();
        System.out.println("track type=wiggle_0 visibility=full name="+name);
        
        for (int chromID=0; chromID<chrSet.getNumChromosomes(); chromID++) {
            final double[] levels = binner.getLevels(chromID);
            if (levels == null) continue;
            
            int firstNonZero = -1;
            int lastNonZero = -1;
            for (int bin=0; bin<binner.getNumBins(chromID); bin++) {
                if (levels[bin] > 1e-7) {
                    if (firstNonZero < 0) firstNonZero = bin;
                    lastNonZero = bin;
                }
            }
            
            if (firstNonZero < 0) continue;
            
            System.out.println(String.format(
                    "fixedStep chrom=chr%s start=%d step=%d span=%d",
                    chrSet.getName(chromID, true),
                    binner.getStart(chromID, firstNonZero) + 1,
                    binSize, binSize));
                    
            for (int bin=firstNonZero; bin<=lastNonZero; bin++) {
                System.out.println(String.format("%.1f", levels[bin]));
            }
        }
    }
}
