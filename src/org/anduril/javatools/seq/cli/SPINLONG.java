package org.anduril.javatools.seq.cli;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.core.engine.CommandFileWriter;
import org.anduril.javatools.seq.pattern.anduril.SegmenterComponent;

public class SPINLONG extends SegmenterComponent {
    public static final String VERSION = "1.0";
    
    public static final String OPT_CONTROL = "c";
    public static final String OPT_DATA_DIR = "data";
    public static final String OPT_GENOME = "genome";
    public static final String OPT_OUT_PLOTS = "out-plot";
    public static final String OPT_OUT_SCORES = "out-score";
    public static final String OPT_PATTERNS = "p";
    public static final String OPT_REGIONS = "r";
    public static final String OPT_SCORE_THRESHOLD = "score";
    public static final String OPT_VERSION = "version";
    
    public static final String DEFAULT_GENOME = "hs37.csv";
    public static final String DEFAULT_OUT_SCORES = "scores.csv";
    public static final String DEFAULT_OUT_PLOTS = "spinlong-plots";
    public static final String DEFAULT_SCORE_THRESHOLD = "0.5";
    
    @SuppressWarnings("static-access")
    private static Options buildOptions() {
        Options opt = new Options();

        opt.addOption(OptionBuilder
                .hasArg()
                .withArgName("CONTROL.bed")
                .withDescription("Control track in BED format")
                .create(OPT_CONTROL));
        opt.addOption(OptionBuilder
                .hasArg()
                .withArgName("DIR")
                .withDescription("Directory containing data files referred to in pattern XML file (default: .)")
                .withLongOpt(OPT_DATA_DIR)
                .create());
        opt.addOption(OptionBuilder
                .hasArg()
                .withArgName("GENOME.csv")
                .withDescription(String.format("Tabulated file of chromosome lengths (default: %s", DEFAULT_GENOME))
                .withLongOpt(OPT_GENOME)
                .create());
        opt.addOption(OptionBuilder
                .hasArg()
                .withArgName("DIR")
                .withDescription(String.format("Output directory for plots (default: %s)", DEFAULT_OUT_PLOTS))
                .withLongOpt(OPT_OUT_PLOTS)
                .create());
        opt.addOption(OptionBuilder
                .hasArg()
                .withArgName("FILE.csv")
                .withDescription(String.format("Output file for scores (default: %s)", DEFAULT_OUT_SCORES))
                .withLongOpt(OPT_OUT_SCORES)
                .create());
        opt.addOption(OptionBuilder
                .isRequired()
                .hasArg()
                .withArgName("PATTERNS.xml")
                .withDescription("XML file containing set of patterns")
                .create(OPT_PATTERNS));
        opt.addOption(OptionBuilder
                .isRequired()
                .hasArg()
                .withArgName("REGIONS.csv")
                .withDescription("Tab-delimited file containing regions of focus. Must contain header row "+
                		"with columns: GeneID GeneName Chromosome Start End Strand")
                .create(OPT_REGIONS));
        opt.addOption(OptionBuilder
                .hasArg()
                .withArgName("SCORE")
                .withDescription(String.format("Score threshold (default: %s)", DEFAULT_SCORE_THRESHOLD))
                .withLongOpt(OPT_SCORE_THRESHOLD)
                .create());
        opt.addOption(OptionBuilder
                .withDescription("Show SPINLONG version and exit")
                .withLongOpt(OPT_VERSION)
                .create());
        
        return opt;
    }
    
    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("--"+OPT_VERSION)) {
            System.out.println("SPINLONG "+VERSION);
            System.exit(0);
        }
        
        SPINLONG comp = new SPINLONG();
        
        Options options = buildOptions();
        CommandLineParser parser = new PosixParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("SPINLONG "+VERSION);
            System.err.println("ERROR: "+e.getMessage());
            new HelpFormatter().printHelp("spinlong -p PATTERNS.xml -r REGIONS.csv [OPTIONS]", options);
            System.exit(1);
            return;
        }
        
        CommandFile cf = new CommandFile();
        
        cf.setInput("bedFiles", new File(cmd.getOptionValue(OPT_DATA_DIR, ".")));
        cf.setInput("patterns", new File(cmd.getOptionValue(OPT_PATTERNS)));
        cf.setInput("regions", new File(cmd.getOptionValue(OPT_REGIONS)));
        if (cmd.hasOption(OPT_CONTROL)) {
            cf.setInput("control", new File(cmd.getOptionValue(OPT_CONTROL)));
        }
        cf.setInput("chromosomes", new File(cmd.getOptionValue(OPT_GENOME, DEFAULT_GENOME)));
        
        if (!cf.getInput("bedFiles").exists()) {
            System.err.println("Data directory does not exist: "+cf.getInput("bedFiles"));
            System.exit(1);
        }
        
        cf.setOutput("scores", new File(cmd.getOptionValue(OPT_OUT_SCORES, DEFAULT_OUT_SCORES)));
        cf.setOutput("plots", new File(cmd.getOptionValue(OPT_OUT_PLOTS, DEFAULT_OUT_PLOTS)));
        cf.setOutput("patternsDump", new File(cmd.getOptionValue(OPT_OUT_PLOTS, DEFAULT_OUT_PLOTS)));
        cf.setOutput(CommandFileWriter.OUTPUT_ERRORS, new File("_errors"));
        cf.setOutput(CommandFileWriter.OUTPUT_LOG, new File("_log"));
        
        cf.setParameter("plotSegments", "true");
        cf.setParameter("plotOptimizer", "false");
        cf.setParameter("scoreThreshold", cmd.getOptionValue(OPT_SCORE_THRESHOLD, DEFAULT_SCORE_THRESHOLD));
        cf.setParameter("chromosomePreset", "hs37");
        cf.setParameter("chromosomePattern", "");
        cf.setParameter("fragmentSize", "200");
        cf.setParameter("maxDuplicateReads", "0");
        cf.setParameter("threads", "4");
        cf.setParameter("yLog", "false");
        cf.setParameter("minRegionLength", "1000");
        cf.setParameter("seed", "-1");
        
        try {
            ErrorCode status = comp.runImpl(cf);
            System.exit(status.getCode());
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
