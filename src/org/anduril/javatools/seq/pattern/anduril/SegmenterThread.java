package org.anduril.javatools.seq.pattern.anduril;

import java.io.File;
import java.util.List;

import org.anduril.asser.io.CSVWriter;
import org.anduril.javatools.seq.ArrayTools;
import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.FixedDenseBinner;
import org.anduril.javatools.seq.LocusTransformation;
import org.anduril.javatools.seq.NamedRegion;
import org.anduril.javatools.seq.pattern.model.Model;
import org.anduril.javatools.seq.pattern.model.ModelInstance;
import org.anduril.javatools.seq.pattern.model.Pattern;
import org.anduril.javatools.seq.pattern.model.PatternInstance;
import org.anduril.javatools.seq.pattern.model.Segment;
import org.anduril.javatools.seq.pattern.solver.AbstractSolver;
import org.anduril.javatools.seq.pattern.solver.SolverResult;

public class SegmenterThread implements Runnable {
    private final SegmenterComponent component;
    private final Model model;
    private final NamedRegion gene;
    private final List<FixedDenseBinner> binners;
    private final List<String> labels;
    private final ChromosomeSet chrSet;
    private final CSVWriter resultWriter;
    private final File pngDirectory;
    private final double scoreThreshold;
    private final boolean yLogScale;
    private final boolean plotSegments;
    private final boolean plotOptimizer;
    
    public SegmenterThread(SegmenterComponent component, Model model, NamedRegion gene,
            List<FixedDenseBinner> binners,
            List<String> labels, ChromosomeSet chrSet, CSVWriter resultWriter, File pngDirectory,
            double scoreThreshold, boolean yLogScale, boolean plotSegments, boolean plotOptimizer) {
        this.component = component;
        this.model = model;
        this.gene = gene;
        this.binners = binners;
        this.labels = labels;
        this.chrSet = chrSet;
        this.resultWriter = resultWriter;
        this.pngDirectory = pngDirectory;
        this.scoreThreshold = scoreThreshold;
        this.yLogScale = yLogScale;
        this.plotSegments = plotSegments;
        this.plotOptimizer = plotOptimizer;
    }
    
    @Override
    public void run() {
        try {
            runImpl();
        } catch (Throwable e) {
            this.component.addError(String.format(
                    "Gene %s (%s) failed: %s",
                    this.gene.name, this.gene.id, e.getMessage()));
            e.printStackTrace();
        }
    }
    
    private void runImpl() throws Exception {
        final FixedDenseBinner firstBinner = this.binners.get(0);
        final int chrID = this.gene.chromosome;
        final int strand = this.gene.strand;
        final int binSize = firstBinner.getBinSize();
        
        final String normID = this.gene.name.replaceAll("[^0-9a-zA-Z]", "_");
        
        int startBin = firstBinner.getBinIndex(chrID, this.gene.start);
        int endBin = firstBinner.getBinIndex(chrID, this.gene.end);
        final int numBins = endBin - startBin;
        
        final int offset = (strand > 0 ? startBin : endBin) * binSize;
        LocusTransformation start = new LocusTransformation(offset, strand);
        ModelInstance modelInst = new ModelInstance(model, start, numBins*binSize);
        
        AbstractSolver solver = modelInst.getSolver();
        
        for (int i=0; i<this.labels.size(); i++) {
            solver.setData(this.labels.get(i), this.binners.get(i).getLevels(chrID));
        }
        
        for (PatternInstance pi: modelInst.getPatternInstances()) {
            SolverResult result = solver.optimize(pi);
            if (result == null || result.score < this.scoreThreshold) continue;
            
            final Pattern pattern = pi.getPattern();
            final String patternID = pattern.getName() != null ? pattern.getName() : pattern.getSegmentString();
            final String normPattern = patternID.replaceAll("[^0-9a-zA-Z]", "_");

            final String segmentPlotOutput;
            if (this.plotSegments) {
                File png = new File(pngDirectory,
                        String.format("%s-%s.png", normID, normPattern));
                String title = String.format("%s (%s): score=%.2f",
                        this.gene.name, pattern.getName(), result.score);
                solver.visualize(result, png, title, this.yLogScale);
                segmentPlotOutput = png.getName();
            } else {
                segmentPlotOutput = null;
            }

            final String optimizerPlotOutput;
            if (this.plotOptimizer) {
                File png = new File(pngDirectory,
                        String.format("log-%s-%s.png", normID, normPattern));
                String title = String.format("%s (%s): score=%.2f",
                        this.gene.name, pattern.getSegmentString(), result.score);
                solver.visualizeLog(png, title);
                optimizerPlotOutput = png.getName();
            } else {
                optimizerPlotOutput = null;
            }

            double[][] meanSD = result.getSegmentMeanSD(solver);
            
            StringBuffer segmentNames = new StringBuffer();
            for (Segment seg: pi.getPattern().getSegments()) {
                if (segmentNames.length() > 0) segmentNames.append(',');
                segmentNames.append(seg.getName());
            }
            
            synchronized (resultWriter) {
                resultWriter.write(this.gene.id);
                resultWriter.write(this.gene.name);
                resultWriter.write(this.chrSet.getName(chrID));
                resultWriter.write(startBin*binSize);
                resultWriter.write(endBin*binSize);
                
                resultWriter.write(patternID);
                resultWriter.write(result.score);
                
                resultWriter.write(segmentNames.toString());
                resultWriter.write(ArrayTools.arrayToString(result.solution, 0));
                resultWriter.write(ArrayTools.arrayToString(result.getSegmentScores(solver), 3));
                resultWriter.write(ArrayTools.arrayToString(meanSD[0], 2));
                resultWriter.write(ArrayTools.arrayToString(meanSD[1], 2));
                
                resultWriter.write(segmentPlotOutput);
                resultWriter.write(optimizerPlotOutput);
                resultWriter.flush();
            };
        }
    }
}