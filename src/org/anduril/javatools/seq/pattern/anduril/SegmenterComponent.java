package org.anduril.javatools.seq.pattern.anduril;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;
import org.anduril.asser.io.CSVWriter;
import org.anduril.javatools.seq.ChromosomeSet;
import org.anduril.javatools.seq.DNARegion;
import org.anduril.javatools.seq.FixedDenseBinner;
import org.anduril.javatools.seq.NamedRegion;
import org.anduril.javatools.seq.io.BedReader;
import org.anduril.javatools.seq.io.DNARegionReader;
import org.anduril.javatools.seq.io.MappabilityReader;
import org.anduril.javatools.seq.pattern.model.Model;
import org.anduril.javatools.seq.pattern.model.ModelParser;
import org.anduril.javatools.seq.pattern.solver.AbstractSolver;
import org.anduril.javatools.utils.IOUtils;

public class SegmenterComponent extends SkeletonComponent {
    public static final String[] COLUMNS = new String[] {
        "ID", "Name", "Chromosome", "Start", "End",
        "Pattern", "Score",
        "Segments", "SegmentLength", "SegmentScore", "SegmentMean", "SegmentSD",
        "MatchPlot", "OptimizerPlot"
    };
    
    private List<String> errors;
    
    public SegmenterComponent() {
        this.errors = Collections.synchronizedList(new ArrayList<String>());
    }
    
    public void addError(String msg) {
        this.errors.add(msg);
    }
    
    public List<String> getErrors() {
        return this.errors;
    }
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        final int seed = cf.getIntParameter("seed");
        if (seed >= 0) {
            AbstractSolver.random.setSeed(seed);
            try {
                /* Try to set the seed on global RNG used by java.lang.Math.
                 * This accesses a private non-API field and has been tested
                 * only on Oracle JDK. */
                Math.random();
                Field globalRNG = Math.class.getDeclaredField("randomNumberGenerator");
                globalRNG.setAccessible(true);
                globalRNG.set(null, AbstractSolver.random);
            } catch (Exception e) {
                System.out.println("Warning: could not set seed of random number generator: "+e);
            }
        }
        
        final File chrFile;
        if (cf.inputDefined("chromosomes")) {
            chrFile = cf.getInput("chromosomes");
        } else {
            String preset = cf.getParameter("chromosomePreset");
            chrFile = new File(String.format("%s.csv", preset));
            if (!chrFile.exists()) {
                cf.writeError("Invalid chromosome preset: "+preset);
                return ErrorCode.PARAMETER_ERROR;
            }
        }
        ChromosomeSet chrSet = new ChromosomeSet();
        chrSet.readSizesFromCSV(chrFile);

        Model model = ModelParser.parseModel(cf.getInput("patterns"));
        if (model.getPatterns().isEmpty()) {
            cf.writeError("XML file does not define any patterns");
            return ErrorCode.INVALID_INPUT;
        }
        
        File htmlDir = cf.getOutput("patternsDump");
        htmlDir.mkdirs();
        File htmlIndex = new File(htmlDir, "index.html");
        Tools.writeString(htmlIndex, model.toHTML());
        
        final int binSize  = model.getBinSize();
        final int fragmentSize = cf.getIntParameter("fragmentSize");
        final int maxDuplicateReads = cf.getIntParameter("maxDuplicateReads");
        
        String chrPattern = cf.getParameter("chromosomePattern");
        if (chrPattern.isEmpty()) chrPattern = null;
        
        FixedDenseBinner mappabilityBinner;
        if (cf.inputDefined("mappability")) {
            System.out.println("Reading mappability track");
            MappabilityReader mapReader = new MappabilityReader(chrSet, binSize, fragmentSize);
            mappabilityBinner = mapReader.readMappability(cf.getInput("mappability"));
        } else {
            mappabilityBinner = null;
        }
        
        FixedDenseBinner controlBinner;
        if (cf.inputDefined("control")) {
            controlBinner = readTrack(cf.getInput("control"),
                    binSize, chrSet, chrPattern,
                    fragmentSize, maxDuplicateReads, null);
        } else {
            controlBinner = null;
        }
        
        List<String> labels = new ArrayList<String>();
        List<FixedDenseBinner> binners = getBinners(cf, chrSet, model, labels, binSize,
                fragmentSize, mappabilityBinner, controlBinner);
        final int numSamples = binners.size();
        
        // Release memory
        mappabilityBinner = null;
        controlBinner = null;
        
        CSVWriter resultWriter = new CSVWriter(COLUMNS, cf.getOutput("scores"));
        final File pngDirectory = cf.getOutput("plots");
        pngDirectory.mkdirs();

        final int maxThreads = cf.getIntParameter("threads");
        final double scoreThreshold = cf.getDoubleParameter("scoreThreshold");
        final boolean yLogScale = cf.getBooleanParameter("yLog");
        final int minRegionLength = cf.getIntParameter("minRegionLength");
        final boolean plotSegments = cf.getBooleanParameter("plotSegments");
        final boolean plotOptimizer = cf.getBooleanParameter("plotOptimizer");
        
        DNARegionReader geneReader = new DNARegionReader(cf.getInput("regions"),
                chrSet,
                "Chromosome", "Start", "End", "Strand",
                "GeneID", "GeneName");
        geneReader.setAddChromosome(false);
        
        ExecutorService executor = new ThreadPoolExecutor(1, maxThreads,
                7, TimeUnit.DAYS, new ArrayBlockingQueue<Runnable>(2*maxThreads));
        
        int counter = 0;
        geneLoop:
        for (NamedRegion gene: geneReader) {
            if (!this.errors.isEmpty()) break;
            
            if (gene.length() / binSize < 3 || gene.length() < minRegionLength) {
                continue geneLoop;
            }
            
            /* Skip chromosomes that are not present on some sample */
            for (int sample=0; sample<numSamples; sample++) {
                if (!binners.get(sample).hasLevels(gene.chromosome)) {
                    continue geneLoop;
                }
            }
            
            counter++;
            if ((counter % 1000) == 0) {
                System.out.println(String.format("Processed %d gene regions", counter));
            }
            
            SegmenterThread seg = new SegmenterThread(this, model, gene, binners,
                    labels, chrSet, resultWriter, pngDirectory, scoreThreshold,
                    yLogScale, plotSegments, plotOptimizer);
            while(true) {
                try {
                    executor.submit(seg);
                    break;
                } catch(RejectedExecutionException e) {
                    try {
                        // When queue is full, sleep and try again
                        Thread.sleep(500);
                    } catch (InterruptedException e2) {}
                }
            }
        }
        
        executor.shutdown();
        try {
            executor.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {}
        
        resultWriter.close();
        geneReader.close();
        
        for (String error: getErrors()) {
            cf.writeError(error);
        }
        
        return this.errors.isEmpty() ? ErrorCode.OK : ErrorCode.ERROR;
    }
    
    private FixedDenseBinner readTrack(File bedFile, int binSize, ChromosomeSet chrSet, String chrPattern,
            int fragmentSize, int maxDuplicateReads, Model model) throws IOException {
        final BedReader reader = new BedReader(
                IOUtils.openGZIPFile(bedFile),
                chrSet, chrPattern, null);
        try {
            if (reader.getName() == null) {
                reader.setName(bedFile.getName());
            }
            if (model != null && !model.hasViewBySource(reader.getName())) {
                System.out.println("Skipping "+bedFile.getName());
                return null;
            }
            
            final FixedDenseBinner binner = new FixedDenseBinner(binSize, chrSet);
            binner.setName(reader.getName());
            
            System.out.println("Reading "+bedFile.getName());
            reader.setAddChromosome(false);
            reader.setMaxDuplicateReads(maxDuplicateReads);
            if (fragmentSize > 0) reader.setElongate(fragmentSize);
            int count = 0;
            for (DNARegion reg: reader) {
                binner.addRegion(reg);
                count++;
            }
            
            double normFactor = binner.normalize(1e9);
            System.out.println(String.format(
                    "%s: processed %d reads, filtered %d duplicates, normalization factor %.3f",
                    bedFile.getName(), count, reader.getDuplicatesRemovedCount(), normFactor));
            
            return binner;
        } finally {
            reader.close();
        }
    }
    
    private List<FixedDenseBinner> getBinners(CommandFile cf, ChromosomeSet chrSet, Model model,
            List<String> labels, int binSize, int fragmentSize, FixedDenseBinner mappability,
            FixedDenseBinner controlTrack) throws IOException {
        List<FixedDenseBinner> binners = new ArrayList<FixedDenseBinner>();
        
        String chrPattern = cf.getParameter("chromosomePattern");
        if (chrPattern.isEmpty()) chrPattern = null;
        final int maxDuplicateReads = cf.getIntParameter("maxDuplicateReads");
        
        for (File file: cf.getInput("bedFiles").listFiles()) {
            if (file.isDirectory() || file.getName().startsWith(".")) continue;
            
            FixedDenseBinner binner = readTrack(file, binSize, chrSet, chrPattern, fragmentSize, maxDuplicateReads, model);
            if (binner == null) continue;
            
            binners.add(binner);
            labels.add(binner.getName());
            
            if (controlTrack != null) {
                binner.add(controlTrack, -1);
            }
            if (mappability != null) {
                binner.multiply(mappability, true);
            }
        }
        
        return binners;
    }
    
    public static void main(String[] args) {
        new SegmenterComponent().run(args);
    }
}