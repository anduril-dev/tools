package org.anduril.javatools.seq.pattern;

import java.io.File;
import java.io.IOException;

import org.apache.commons.math.random.RandomDataImpl;

import org.anduril.asser.io.CSVWriter;
import org.anduril.javatools.seq.LocusTransformation;
import org.anduril.javatools.seq.pattern.model.Model;
import org.anduril.javatools.seq.pattern.model.ModelInstance;
import org.anduril.javatools.seq.pattern.model.ModelParser;
import org.anduril.javatools.seq.pattern.model.Pattern;
import org.anduril.javatools.seq.pattern.model.PatternInstance;
import org.anduril.javatools.seq.pattern.model.Segment;
import org.anduril.javatools.seq.pattern.model.SegmentLength;
import org.anduril.javatools.seq.pattern.solver.AbstractSolver;
import org.anduril.javatools.seq.pattern.solver.SolverResult;

public class SolverEvaluator {
    public static final double DEFAULT_SD = 40;
    public static final String[] COLUMNS = new String[] {
        "Case", "Method", "SD", "Score", "LengthDistance"};

    public static final int[] SEGMENT_LENGTHS = {140, 100, 60};
    public static final int TOTAL_LENGTH = 300;
    
    private static RandomDataImpl random = new RandomDataImpl();

    private Model model;
    private CSVWriter writer;
    private int rounds;
    private double sd;
    
    private double scoreSum;
    private int numScores;
    
    public SolverEvaluator(Model model, CSVWriter writer, int rounds) {
        this.model = model;
        this.writer = writer;
        this.rounds = rounds;
        this.sd = DEFAULT_SD;
    }

    public SolverEvaluator(Model model, File csvFile, int rounds) throws IOException {
        this(model, new CSVWriter(COLUMNS, csvFile), rounds);
    }
    
    public void close() {
        this.writer.close();
    }

    public CSVWriter getWriter() {
        return this.writer;
    }
    
    public double getSD() {
        return this.sd;
    }
    
    public void setSD(double sd) {
        this.sd = sd;
    }

    public static double makeClippedGaussian(double mean, double sd) {
        double value =  SolverEvaluator.random.nextGaussian(mean, sd);
        if (value < 0) value = 0;
        return value;
    }
    
    public void evaluate() {
        final int lowMean = 100;
        final double[] highMeans = new double[] {110, 125, 150}; 
        final int binSize = this.model.getBinSize();
        
        for (Pattern pattern: model.getPatterns()) {
            for (Segment segment: pattern.getSegments()) {
                segment.setLowBound(new SegmentLength(10*binSize));
            }
        }
        
        LocusTransformation start = new LocusTransformation(0, 1);
        ModelInstance inst = new ModelInstance(this.model, start, TOTAL_LENGTH*binSize);
        
        evaluateRound(inst, lowMean, lowMean, false, false, false);
        for (double highMean: highMeans) {
            evaluateRound(inst, lowMean, highMean, false, true, false);
            evaluateRound(inst, lowMean, highMean, true, false, true);
        }
        
        System.err.println(String.format("Mean score: %.2f", this.scoreSum/this.numScores));
    }
    
    private void evaluateRound(ModelInstance inst,
            double lowMean, double highMean,
            boolean high1, boolean high2, boolean high3) {
        final int binSize = this.model.getBinSize();
        
        final char HIGH = 'H', LOW = 'L';
        final String tag = String.format("%s%s%s-%.0f-SD%.0f",
                high1 ? HIGH : LOW,
                high2 ? HIGH : LOW,
                high3 ? HIGH : LOW,
                highMean, this.sd);                
        
        double[] levels = new double[TOTAL_LENGTH];
        for (int round=0; round<this.rounds; round++) {
            if ((round % 100) == 0) System.err.println(tag+" "+round);
            
            int pos = 0;
            for (int i=0; i<SEGMENT_LENGTHS[0]; i++) {
                levels[pos] = makeClippedGaussian(high1 ? highMean : lowMean, this.sd);
                pos++;
            }
            for (int i=0; i<SEGMENT_LENGTHS[1]; i++) {
                levels[pos] = makeClippedGaussian(high2 ? highMean : lowMean, this.sd);
                pos++;
            }
            for (int i=0; i<SEGMENT_LENGTHS[2]; i++) {
                levels[pos] = makeClippedGaussian(high3 ? highMean : lowMean, this.sd);
                pos++;
            }
            
            AbstractSolver solver = inst.getSolver();
            solver.setData("random", levels);
            for (PatternInstance pi: inst.getPatternInstances()) {
                double score;
                double lengthDistance;
                try {
                    SolverResult result = solver.optimize(pi);
                    score = result.score;
                    this.scoreSum += score;
                    this.numScores++;
                    double lengthDistanceSum = 0;
                    for (int seg=0; seg<result.solution.length; seg++) {
                        final double diff = result.solution[seg] - SEGMENT_LENGTHS[seg]*binSize;
                        lengthDistanceSum += Math.pow(diff, 2);
                    }
                    lengthDistance = Math.sqrt(lengthDistanceSum);
                } catch (RuntimeException e) {
                    score = Double.NaN;
                    lengthDistance = Double.NaN;
                }
                
                this.writer.write(tag);
                this.writer.write(pi.getPattern().getName());
                this.writer.write(this.sd);
                this.writer.write(score);
                this.writer.write(lengthDistance/binSize);
                this.writer.flush();
            }
        }
    }
    
    public static void main(String[] args) throws Exception {
        if (args.length != 3) {
            System.err.println("Usage: SolverEvaluator NUM-ROUNDS IN-MODEL.xml OUTPUT.csv");
            System.exit(1);
        }
        
        final int rounds = Integer.parseInt(args[0]);
        File modelFile = new File(args[1]);
        File outFile = new File(args[2]);
        Model model = new ModelParser(modelFile).parse();
        System.out.println(model);
        new SolverEvaluator(model, outFile, rounds).evaluate();
    }
}
