package org.anduril.javatools.seq.pattern.solver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jfree.chart.plot.XYPlot;

import org.anduril.javatools.seq.pattern.model.ModelInstance;
import org.anduril.javatools.seq.pattern.model.ScorerConfiguration;
import org.anduril.javatools.seq.pattern.model.View;

public abstract class AbstractScorer extends BaseConfigurable {
    public static final boolean VERBOSE = false;    
    
    private final int id;
    private final ModelInstance inst;
    private final List<View> views;
    private final int binSize;
    
    public AbstractScorer(int id, ModelInstance inst, List<View> views, Map<String, String> args, int binSize) {
        super(args);
        this.id = id;
        this.inst = inst;
        this.views = views;
        this.binSize = binSize;
    }
    
    public static AbstractScorer getInstance(ScorerConfiguration scorerConf, ModelInstance mi, int binSize) {
        final String type = scorerConf.getType();
        final AbstractScorer scorer;
        if (type.equals(ArrayScorer.SCORER_TYPE)) {
            scorer = new ArrayScorer(scorerConf.getID(), mi, scorerConf.getViews(),
                    scorerConf.getArgs(), binSize);
        } else if (type.equals(HMMScorer.SCORER_TYPE)) {
            scorer = new HMMScorer(scorerConf.getID(), mi, scorerConf.getViews(),
                    scorerConf.getArgs(), binSize);
        } else {
            throw new IllegalArgumentException("Unknown scorer type: "+type); 
        }

        return scorer;
    }
    
    public final int getID() {
        return this.id;
    }
    
    public ModelInstance getModelInstance() {
        return this.inst;
    }
    
    public List<View> getViews(boolean includeInactive) {
        if (includeInactive) {
            return this.views;
        } else {
            List<View> activeViews = new ArrayList<View>();
            for (int i=0; i<this.views.size(); i++) {
                View view = this.views.get(i);
                if (isViewActive(view, i)) activeViews.add(view);
            }
            return activeViews;
        }
    }
    
    public List<View> getViews() {
        return getViews(true);
    }

    public int getMaxViewID() {
        int max = 0;
        for (View view: this.views) {
            final int viewID = view.getID();
            if (viewID > max) max = viewID;
        }
        return max;
    }
    
    public final int getBinSize() {
        return this.binSize;
    }
    
    public abstract int setData(String source, double[] data);
    
    
    /**
     * Return the mean and standard deviation of the segment
     * as a length-2 array {mean, sd}.
     */
    public abstract double[] getSegmentMeanSD(View view, double offset, double length);
    
    public abstract void initializeData();
    
    public abstract void initializeAuxState();
    public abstract double changeAuxState(double phase, int preferredDirection);
    public abstract void revertAuxState(boolean best);
    public abstract void saveBestAuxState();

    public abstract double evaluateScore(View view, double offset, double length, char pattern);
    
    public abstract XYPlot makeSamplePlot(SolverResult result, AbstractSolver solver, View view, boolean yLogScale);
    
    public abstract double[] getScoreStatistics();
    
    protected abstract boolean isViewActive(View view, int index);
}
