package org.anduril.javatools.seq.pattern.solver;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.ObservationReal;
import be.ac.ulg.montefiore.run.jahmm.Opdf;
import be.ac.ulg.montefiore.run.jahmm.OpdfFactory;
import be.ac.ulg.montefiore.run.jahmm.learn.BaumWelchScaledLearner;
import be.ac.ulg.montefiore.run.jahmm.learn.KMeansLearner;

import org.anduril.javatools.seq.pattern.model.ModelInstance;
import org.anduril.javatools.seq.pattern.model.Segment;
import org.anduril.javatools.seq.pattern.model.View;

public class HMMScorer extends ArrayScorer {
    public static final int K_CLUSTER_ROUNDS = 30;
    public static final int BAUM_WELCH_ROUNDS = 30;
    public static final double PROB_CHANGE_STATES = 0.1;
    public static final double MIN_LOG_PROB = -20;
    
    public static final String SCORER_TYPE = "HMM";
    public static final String ARG_NUM_STATES = "numStates";
    
    public static final String METHOD_THRESHOLD = "threshold";
    public static final String METHOD_PROBABILITY = "probability";
    
    public enum ScoreMethod { THRESHOLD, PROBABILITY };
    
    private Hmm<ObservationReal> hmm;
    private int[][] optimalPath;
    private final int numStates;
    private int[] stateMeanRanks;
    private int peakState;
    
    private ScoreMethod scoreMethod;
    
    /// Runtime structures ///
    private final char[] statePatterns;
    private int activeNumLowStates;
    private int storedNumLowStates;
    private int bestNumLowStates;
    
    /** The cache table emitLogProbs[state][view][bin] gives the
     * probability of each bin in each state. */
    private double[][][] emitLogProbs;
    
    private double[][] transLogProbs;
    
    public HMMScorer(int id, ModelInstance inst, List<View> views, Map<String, String> args, int scale) {
        super(id, inst, views, args, scale);
        
        this.numStates = getIntArgument(ARG_NUM_STATES);
        this.statePatterns = new char[numStates];        
    }
    
    private void initModel() {
        /* HMM already initialized */
        if (this.hmm != null) return;
        
        final OpdfFactory<PdfGaussian> factory = new PdfGaussian();
        Hmm<ObservationReal> initHmm = new Hmm<ObservationReal>(this.numStates, factory);

        double sum = 0, squareSum = 0;
        double min = 1e9, max = -1e9;
        int numBins = 0;
        List<List<ObservationReal>> obs = new ArrayList<List<ObservationReal>>();
        for (View view: getViews(false)) {
            List<ObservationReal> sampleObs = new ArrayList<ObservationReal>();
            obs.add(sampleObs);
            for (double value: getData(view))  {
                sampleObs.add(new ObservationReal(value));
                sum += value;
                squareSum += value * value;
                if (value < min) min = value;
                if (value > max) max = value;
                numBins++;
            }
        }
        /* Var(X) = E(X^2) - E(X)^2 */
        double var = (squareSum / numBins) - Math.pow(sum / numBins, 2);

        final double P_STAY = 0.9;
        for (int s=0; s<this.numStates; s++) {
            initHmm.setOpdf(s, new PdfGaussian(s == 0 ? min : max, var));
            initHmm.setPi(s, 1.0/this.numStates);
            for (int t=0; t<this.numStates; t++) {
                double prob = (s == t) ? P_STAY : (1.0-P_STAY) / (this.numStates-1.0);
                initHmm.setAij(s, t, prob);
            }
        }
        
        KMeansLearner<ObservationReal> kmeans = new KMeansLearner<ObservationReal>(this.numStates, factory, obs);
        for (int i=0; i<K_CLUSTER_ROUNDS; i++) { initHmm = kmeans.iterate(); }

        BaumWelchScaledLearner bw = new BaumWelchScaledLearner();
        bw.setNbIterations(BAUM_WELCH_ROUNDS);
        this.hmm = bw.learn(initHmm, obs);
        
        double p = this.hmm.getPi(0);
        if (Double.isNaN(p) || Double.isInfinite(p)) {
            this.hmm = initHmm;
        }

        p = this.hmm.getPi(0);
        if (Double.isNaN(p) || Double.isInfinite(p)) {
            throw new RuntimeException("Could not obtain HMM");
        }
        
        this.stateMeanRanks = new int[this.numStates];
        double[] means = new double[this.numStates];
        for (int state=0; state<this.numStates; state++) {
            means[state] = ((PdfGaussian)this.hmm.getOpdf(state)).mean();
        }
        Arrays.sort(means);
        for (int state=0; state<this.numStates; state++) {
            final double mean = ((PdfGaussian)this.hmm.getOpdf(state)).mean();
            int pos = Arrays.binarySearch(means, mean);
            if (pos < 0) pos = 1 - pos;
            this.stateMeanRanks[state] = pos;
            if (pos >= numStates-1) this.peakState = state;
        }
        
        final int numViews = getMaxViewID()+1;
        
        this.optimalPath = new int[numViews][];
        for (int i=0; i<obs.size(); i++) {
            int[] viewPath = this.hmm.mostLikelyStateSequence(obs.get(i));
            final int viewID = getViews(false).get(i).getID();
            this.optimalPath[viewID] = viewPath;
        }

        this.emitLogProbs = new double[this.numStates][numViews][];
        for (int state=0; state<this.numStates; state++) {
            final Opdf<ObservationReal> pdf = this.hmm.getOpdf(state);
            for (View view: getViews(false)) {
                final double[] bins = getData(view);
                final int viewID = view.getID();
                this.emitLogProbs[state][viewID] = new double[bins.length];
                for (int bin=0; bin<bins.length; bin++) {
                    double logProb = Math.log(pdf.probability(
                            new ObservationReal(bins[bin])));
                    if (logProb < MIN_LOG_PROB) logProb = MIN_LOG_PROB;
                    this.emitLogProbs[state][viewID][bin] = logProb;
                    
                }
            }
        }
        
        this.transLogProbs = new double[this.numStates][this.numStates];
        for (int from=0; from<this.numStates; from++) {
            for (int to=0; to<this.numStates; to++) {
                double logProb = Math.log(this.hmm.getAij(from, to));
                if (logProb < MIN_LOG_PROB) logProb = MIN_LOG_PROB;
                this.transLogProbs[from][to] = logProb;
            }
        }
    }
    
    @Override
    public double[] getScoreStatistics() {
        if (this.scoreMethod == ScoreMethod.THRESHOLD) {
            return new double[] {0, 1};
        } else if (this.scoreMethod == ScoreMethod.PROBABILITY) {
            return new double[] {-1, 1};
        } else {
            throw new RuntimeException("Score method not set or unknown");
        }
    }
    
    @Override
    public int setData(String source, double[] data) {
        this.hmm = null;
        return super.setData(source, data);
    }
    
    @Override
    public double evaluateScore(View view, double offset, double length, char pattern) {
        if (pattern == Segment.SEGMENT_IGNORE) return 0;
        if (this.scoreMethod == ScoreMethod.THRESHOLD) {
            return evaluateThreshold(view, offset, length, pattern);
        } else if (this.scoreMethod == ScoreMethod.PROBABILITY) {
            return evaluateProbability(view, offset, length, pattern);
        } else {
            throw new RuntimeException("Score method not set or unknown");
        }
    }

    private double evaluateThreshold(View view, double offset, double length, char pattern) {
        final int numBins = (int)(length / getBinSize());
        if (numBins == 0) return 0;
        final int startBin = (int)(offset / getBinSize());
        final int viewID = view.getID();
        
        int score = 0;
        for (int i=0; i<numBins; i++) {
            final int bin = startBin + i;
            final int state = this.optimalPath[viewID][bin];
            
            final boolean correct;
            if (pattern == Segment.SEGMENT_PEAK) {
                correct = (state == this.peakState);
            } else {
                final boolean stateHigh = this.statePatterns[state] == Segment.SEGMENT_HIGH;
                final boolean patHigh = (pattern == Segment.SEGMENT_HIGH);
                correct = (stateHigh == patHigh); 
            }
            if (correct) score++;
        }
        
        return score / (double)numBins;
    }
    
    private double evaluateProbability(View view, double offset, double length, char pattern) {
        final int numBins = (int)(length / getBinSize());
        if (numBins == 0) return 0;
        final int startBin = (int)(offset / getBinSize());
        final int viewID = view.getID();
        
        double logProbs = 0;
        for (int i=0; i<numBins; i++) {
            final int bin = startBin + i;
            
            double bestEmitLogP = MIN_LOG_PROB;
            double bestEmitCompLogP = MIN_LOG_PROB;
            for (int state=0; state<this.numStates; state++) {
                double logP = this.emitLogProbs[state][viewID][bin];
                
                /* TODO: peak is currently a special case */
                if (pattern == Segment.SEGMENT_PEAK) {
                    if (state == this.peakState) {
                        if (logP > bestEmitLogP) bestEmitLogP = logP;
                    } else {
                        if (logP > bestEmitCompLogP) bestEmitCompLogP = logP;
                    }
                } else if (this.statePatterns[state] == pattern) {
                    if (logP > bestEmitLogP) bestEmitLogP = logP;
                } else {
                    if (logP > bestEmitCompLogP) bestEmitCompLogP = logP;
                }
            }
            
            logProbs += bestEmitLogP - bestEmitCompLogP;
        }
        
        return logProbs / ((double)numBins * -MIN_LOG_PROB);
    }
    
    @Override
    public void initializeAuxState() {
        final String method = getArgument(ARG_METHOD);
        if (method.equals(METHOD_THRESHOLD)) {
            this.scoreMethod = ScoreMethod.THRESHOLD;
        } else if (method.equals(METHOD_PROBABILITY)) {
            this.scoreMethod = ScoreMethod.PROBABILITY;
        }
        else {
            throw new IllegalArgumentException("Invalid HMM scorer method: "+method);
        }
        
        initModel();
        this.activeNumLowStates = 1;
        setStatePatterns(this.activeNumLowStates);
    }
    
    private void setStatePatterns(int numLowStates) {
        if (numLowStates <= 0 || numLowStates >= this.numStates) {
            String msg = String.format("numStates is out of bounds: %d (numStates=%d)",
                    numLowStates, this.numStates);
            throw new IllegalArgumentException(msg);
        }
        
        for (int state=0; state<this.numStates; state++) {
            if (this.stateMeanRanks[state] < numLowStates) {
                this.statePatterns[state] = Segment.SEGMENT_LOW;
            } else {
                this.statePatterns[state] = Segment.SEGMENT_HIGH;
            }
        }
        if (VERBOSE) System.out.println(String.format(
                "Set state patterns to %s (ranks: %s)",
                Arrays.toString(this.statePatterns),
                Arrays.toString(this.stateMeanRanks)));
    }
    
    @Override
    public double changeAuxState(double phase, int preferredDirection) {
        final boolean canIncrease = this.activeNumLowStates < this.numStates - 1;
        final boolean canDecrease = this.activeNumLowStates > 1;
        if (!canIncrease && !canDecrease) return 0;
        if (preferredDirection > 0 && !canIncrease) return 0;
        if (preferredDirection < 0 && !canDecrease) return 0;
        
        this.storedNumLowStates = this.activeNumLowStates;

        final boolean doIncrease;
        if (preferredDirection > 0) doIncrease = true;
        else if (preferredDirection < 0) doIncrease = false;
        else if (canIncrease && canDecrease) doIncrease = AbstractSolver.random.nextBoolean();
        else doIncrease = canIncrease;
        
        if (doIncrease) this.activeNumLowStates++;
        else this.activeNumLowStates--;
        
        setStatePatterns(this.activeNumLowStates);
        if (VERBOSE) System.out.println(String.format("Set numLowStates from %d to %d",
                this.storedNumLowStates, this.activeNumLowStates));
        return this.activeNumLowStates - this.storedNumLowStates;
    }

    @Override
    public void revertAuxState(boolean best) {
        this.activeNumLowStates = best ? this.bestNumLowStates : this.storedNumLowStates;
        if (VERBOSE) System.out.println(String.format("Revert numLowStates to %d",
                this.activeNumLowStates));        
        setStatePatterns(this.activeNumLowStates);
    }

    @Override
    public void saveBestAuxState() {
        this.bestNumLowStates = this.activeNumLowStates;
    }
    
    @Override
    protected boolean isBinClassificationCorrect(SolverResult result, View view, Segment segment, int bin, double value) {
        final char pattern = segment.getPattern();
        final int viewID = view.getID();
        if (pattern == Segment.SEGMENT_PEAK) {
            return this.optimalPath[viewID][bin] == this.peakState;
        } else {
            final int state = this.optimalPath[viewID][bin];
            final char statePat = this.statePatterns[state];
            return statePat == pattern;
        }
    }
    
    @Override
    protected void customizePlot(XYPlot plot, SolverResult result) {
        setStatePatterns(this.bestNumLowStates);
        final Color markerColor = new Color(0, 196, 0);
        for (int s=0; s<this.numStates; s++) {
            PdfGaussian pdf = (PdfGaussian)this.hmm.getOpdf(s);
            ValueMarker marker = new ValueMarker(pdf.mean());
            marker.setPaint(markerColor);
            String label = ""+this.statePatterns[s];
            marker.setLabel(label);
            plot.addRangeMarker(marker);
        }
    }
    
}
