package org.anduril.javatools.seq.pattern.solver;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.primitives.ArrayDoubleList;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYBarDataset;
import org.jfree.ui.TextAnchor;

import org.anduril.javatools.seq.ArrayTools;
import org.anduril.javatools.seq.LocusTransformation;
import org.anduril.javatools.seq.pattern.model.ModelInstance;
import org.anduril.javatools.seq.pattern.model.Pattern;
import org.anduril.javatools.seq.pattern.model.Segment;
import org.anduril.javatools.seq.pattern.model.View;

public class ArrayScorer extends AbstractScorer {
    public enum AggregateFunction { MIN, MAX, MEAN, SUM };
    public enum ScoreMethod { THRESHOLD, LOGRATIO };
    
    public static final String SCORER_TYPE = "threshold";
    public static final int MAX_THRESHOLDS = 100;
    
    // LR = min(ln(x/y) * LOGRATIO_SCALE, MAX_LOGRATIO)
    public static final double MAX_LOGRATIO = 5;
    public static final double LOGRATIO_SCALE = 1.0 / Math.log(2); 
    
    public static final String ARG_METHOD = "method";
    public static final String ARG_MEDIAN_WINDOW = "medianWindow";
    public static final String ARG_BG_PERCENTILE = "bgPercentile";
    public static final String ARG_AGGREGATE = "aggregate";
    public static final String ARG_SEPARATION = "separation";
    
    public static final String METHOD_THRESHOLD = "threshold";
    public static final String METHOD_LOGRATIO = "logratio";

    private final double[][] levels;
    private final double[][] originalLevels;
    
    private final int medianWindow;
    private final double bgPercentile;
    private final double separation;
    private final double separationSqrt;
    private final AggregateFunction aggregate;
    private ScoreMethod scoreMethod;
    
    private double[] thresholds;
    private int activeThreshold;
    private int oldThreshold;
    private int bestThreshold;
    
    public ArrayScorer(int id, ModelInstance inst, List<View> views, Map<String, String> args, int scale) {
        super(id, inst, views, args, scale);
        
        this.levels = new double[getMaxViewID()+1][];
        this.originalLevels = new double[getMaxViewID()+1][];
        
        this.medianWindow = getIntArgument(ARG_MEDIAN_WINDOW);
        this.bgPercentile = getDoubleArgument(ARG_BG_PERCENTILE);
        this.separation = getDoubleArgument(ARG_SEPARATION);
        this.separationSqrt = Math.sqrt(this.separation);
        
        String aggr = getArgument(ARG_AGGREGATE);
        if (aggr.equals("none") || aggr.isEmpty()) this.aggregate = null;
        else if (aggr.equals("min")) this.aggregate = AggregateFunction.MIN;
        else if (aggr.equals("max")) this.aggregate = AggregateFunction.MAX;
        else if (aggr.equals("mean")) this.aggregate = AggregateFunction.MEAN;
        else if (aggr.equals("sum")) this.aggregate = AggregateFunction.SUM;
        else {
            throw new IllegalArgumentException("Invalid aggregate function: "+aggr);
        }
    }
    
    @Override
    public int setData(String source, double[] data) {
        int numFound = 0;
        for (View view: getViews()) {
            if (view.hasSource(source)) {
                numFound++;
                final LocusTransformation trans = getModelInstance().getViewStart(view);
                final int viewLength = getModelInstance().getViewLength(view);
                final int numBins = (int)Math.ceil(viewLength / getBinSize());
                int startBin = (int)Math.round(trans.getOffset() / getBinSize());
                int endBin = startBin+numBins*trans.getScale();
                if (startBin > endBin) {
                    int tmp = startBin;
                    startBin = endBin;
                    endBin = tmp;
                }
                
                if (endBin > data.length) {
                    throw new IllegalArgumentException(String.format(
                            "Can not set data for view %s (source %s): array too short (%d < %d)",
                            view.getName(), view.getSource(),
                            data.length, endBin));
                }
                
                double[] levels = Arrays.copyOfRange(data, startBin, endBin);
                if (trans.getScale() < 0) {
                    ArrayTools.reverse(levels);
                }
                this.levels[view.getID()] = levels; 
                this.originalLevels[view.getID()] = Arrays.copyOfRange(levels, 0, levels.length);
            }
        }
        
        return numFound;
    }

    @Override
    public double[] getSegmentMeanSD(View view, double offset, double length) {
        final int n = (int)(length / getBinSize());
        if (n == 0) return new double[] {0, 0};
        final int startBin = (int)(offset / getBinSize());
        final double[] bins = getData(view, true);
        
        if (bins == null) {
            String msg = String.format("Data for view %s (source %s) is not defined",
                    view.getName(), view.getSource());
            throw new RuntimeException(msg);
        }
        if (bins.length < startBin+n) {
            String msg = String.format(
                    "Can not compute read counts for view %s (offset %.1f, length %.1f): "+
                    "data too short (%d bins, bin size %d)",
                    view.getName(), offset, length, bins.length, getBinSize());
            throw new RuntimeException(msg);
        }
        
        double sum = 0;
        double squareSum = 0;
        for (int i=0; i<n; i++) {
            final double value = bins[startBin + i];
            sum += value;
            squareSum += value*value;
        }
        
        final double mean = sum / n;
        
        final double sd;
        if (n == 1) {
            // Maximum likelihood estimate
            sd = 0;
        } else {
            // Unbiased estimate
            final double v1 = squareSum / (n-1);
            final double v2 = sum*sum / (n*n - n);
            sd = Math.sqrt(v1 - v2);
        }
        
        return new double[] {mean, sd};
    }
    
    @Override
    public void initializeData() {
        checkSources();
        
        if (this.aggregate == null) {
            for (View view: getViews()) {
                double[] data = getData(view);
                data = ArrayTools.preprocess(data, this.medianWindow, this.bgPercentile);
                this.levels[view.getID()] = data;
            }
        } else {
            int dataLength = 0;
            for (View view: getViews()) {
                dataLength = Math.max(dataLength, getData(view).length);
            }

            double[] aggrData = new double[dataLength];

            for (View view: getViews()) {
                double[] data = getData(view);
                if (data.length != dataLength) {
                    String msg = String.format("View %s: can not compute aggregate function: all views musts have same length",
                            view.getName());
                    throw new RuntimeException(msg);
                }

                final double numViews = getViews().size();
                for (int i=0; i<dataLength; i++) {
                    switch(this.aggregate) {
                    case MIN:
                        aggrData[i] = Math.min(aggrData[i], data[i]);
                        break;
                    case MAX:
                        aggrData[i] = Math.max(aggrData[i], data[i]);
                        break;
                    case MEAN:
                        aggrData[i] += data[i] / numViews;
                        break;
                    case SUM:
                        aggrData[i] += data[i];
                        break;
                    default:
                        throw new RuntimeException("Invalid aggregate function: "+this.aggregate);
                    }
                }
            }
            
            aggrData = ArrayTools.preprocess(aggrData, this.medianWindow, this.bgPercentile);
            for (View view: getViews()) {
                this.levels[view.getID()] = aggrData;
            }
        }
    }
    
    public double[] getData(View view, boolean original) {
        final int viewID = view.getID();
        return original ? this.originalLevels[viewID] : this.levels[viewID];
    }
    
    public double[] getData(View view) {
        return getData(view, false);
    }
    
    public int getNumBins(View view) {
        return this.levels[view.getID()].length;
    }
    
    public double getSeparation() {
        return this.separation;
    }
    
    @Override
    public double evaluateScore(View view, double offset, double length, char pattern) {
        if (pattern == Segment.SEGMENT_IGNORE) return 0;
        final int numBins = (int)(length / getBinSize());
        if (numBins == 0) return 0;
        final int startBin = (int)(offset / getBinSize());
        final double[] bins = getData(view);
        
        if (bins == null) {
            String msg = String.format("Data for view %s (source %s) is not defined",
                    view.getName(), view.getSource());
            throw new RuntimeException(msg);
        }
        if (bins.length < startBin+numBins) {
            String msg = String.format(
                    "Can not evaluate segment for view %s (offset %.1f, length %.1f): "+
                    "data too short (%d bins, bin size %d)",
                    view.getName(), offset, length, bins.length, getBinSize());
            throw new RuntimeException(msg);
        }

        final boolean patHigh = pattern == Segment.SEGMENT_HIGH
            || pattern == Segment.SEGMENT_PEAK;
        
        final double threshold = getThreshold(this.activeThreshold);
        
        if (this.scoreMethod == ScoreMethod.THRESHOLD) {
            final double actualThreshold = patHigh ? threshold * this.separationSqrt : threshold / this.separationSqrt;
            return evaluateThreshold(bins, startBin, numBins, patHigh, actualThreshold);
        } else if (this.scoreMethod == ScoreMethod.LOGRATIO) {
            return evaluateLogratio(bins, startBin, numBins, patHigh, threshold);
        } else {
            throw new RuntimeException("Score method not set or unknown");
        }
    }
    
    private double evaluateThreshold(final double[] bins, final int startBin, final int numBins,
            final boolean patternHigh, final double actualThreshold) {
        int score = 0;
        for (int i=0; i<numBins; i++) {
            final int bin = startBin + i;
            final boolean correct = patternHigh ? bins[bin] > actualThreshold : bins[bin] <= actualThreshold;
            if (correct) score++;
        }
        
        return score / (double)numBins;
    }

    private double evaluateLogratio(final double[] bins, final int startBin, final int numBins,
            final boolean patternHigh, final double threshold) {
        if (threshold == 0) return 0;
        
        double sum = 0;
        for (int i=0; i<numBins; i++) {
            final int bin = startBin + i;
            sum += bins[bin];
        }
        final double mean = sum / numBins;
        
        double ratio;
        if (patternHigh) {
            ratio = mean / threshold;
        } else {
            if (mean == 0) return MAX_LOGRATIO;
            ratio = threshold / mean;
        }
        
//        if (ratio < this.separation) ratio = 1;
        final double lr = Math.log(ratio) * LOGRATIO_SCALE;
        return Math.max(Math.min(lr, MAX_LOGRATIO), -MAX_LOGRATIO);
    }
    
    @Override
    public double changeAuxState(double phase, int preferredDirection) {
        final double MAX_THRESHOLD_STEP = 0.2;
        
        if (this.thresholds == null) return 0;
        
        final boolean canIncrease = this.activeThreshold < this.thresholds.length-1;
        final boolean canDecrease = this.activeThreshold > 0;
        if (!canIncrease && !canDecrease) return 0;
        if (preferredDirection > 0 && !canIncrease) return 0;
        if (preferredDirection < 0 && !canDecrease) return 0;
        
        this.oldThreshold = this.activeThreshold;

        /* Step follows exponential curve */
        final double maxStep = AbstractSolver.random.nextDouble() * MAX_THRESHOLD_STEP
            * this.thresholds.length;
        int step = (int)Math.round(maxStep * Math.exp((phase-1) * Math.log(maxStep)));
        if (step < 1) step = 1;

        final boolean doIncrease;
        if (preferredDirection > 0) doIncrease = true;
        else if (preferredDirection < 0) doIncrease = false;
        else if (canIncrease && canDecrease) doIncrease = AbstractSolver.random.nextBoolean();
        else doIncrease = canIncrease;
        
        if (doIncrease) this.activeThreshold += step;
        else this.activeThreshold -= step;
        
        if (this.activeThreshold < 0) {
            this.activeThreshold = 0;
        } else if (this.activeThreshold >= this.thresholds.length) {
            this.activeThreshold = this.thresholds.length-1;
        }
        return this.activeThreshold - this.oldThreshold;
    }
    
    protected void checkSources() {
        for (View view: getViews()) {
            if (getData(view) == null) {
                String msg = String.format("View %s: Data source %s not found",
                        view.getName(), view.getSource());
                throw new RuntimeException(msg);
            }
        }
    }
    
    private final double getThreshold(int pos) {
        return this.thresholds[pos];
    }

    public void setThreshold(double threshold) {
        for (int i=0; i<this.thresholds.length; i++) {
            if (this.thresholds[i] > threshold) {
                this.activeThreshold = i;
                break;
            }
        }
    }
    
    @Override
    public void initializeAuxState() {
        String method = getArgument(ARG_METHOD);
        if (method.equals(METHOD_THRESHOLD)) {
            this.scoreMethod = ScoreMethod.THRESHOLD;
        } else if (method.equals(METHOD_LOGRATIO)) {
            this.scoreMethod = ScoreMethod.LOGRATIO;
        } else {
            throw new IllegalArgumentException("Invalid array scorer method: "+method);
        }
        
        double[] data = ArrayTools.flatten(this.levels);
        if (data.length == 0) return;
        
        Arrays.sort(data);
        final double maxValue = data[data.length-1];
        final int maxThresholds = Math.min(MAX_THRESHOLDS, data.length);
        final double range = maxValue - data[0];
        final double minDifference = range / maxThresholds;
        ArrayDoubleList thresholds = new ArrayDoubleList();
        double previous = -1e15;
        for (int i=0; i<maxThresholds; i++) {
            final double p = i / (double)maxThresholds;
            final int pos = (int)Math.round(data.length * p);
            final double value = data[pos];
            if (value > previous + minDifference) {
                thresholds.add(value);
                previous = value;
            }
        }
        
        if (thresholds.get(thresholds.size()-1) != maxValue) {
            thresholds.add(maxValue);
        }
        
        this.thresholds = thresholds.toArray();
        this.activeThreshold = thresholds.size() / 2;
    }

    @Override
    public void revertAuxState(boolean best) {
        this.activeThreshold = best ? this.bestThreshold : this.oldThreshold;
    }

    @Override
    public void saveBestAuxState() {
        this.bestThreshold = activeThreshold;
    }
    
    @Override
    public double[] getScoreStatistics() {
        if (this.scoreMethod == ScoreMethod.THRESHOLD) {
            return new double[] {0, 1};
        } else if (this.scoreMethod == ScoreMethod.LOGRATIO) {
            return new double[] {-MAX_LOGRATIO, MAX_LOGRATIO};
        } else {
            throw new RuntimeException("Score method not set or unknown");
        }
    }
    
    @Override
    public XYPlot makeSamplePlot(SolverResult result, AbstractSolver solver, View view, boolean yLogScale) {
        final List<Segment> segments = result.pi.getPattern().getSegments(view); 
        if (segments.isEmpty()) return null;
        
        final Color markerColor = new Color(0, 196, 0);
        DefaultXYDataset dataset = new DefaultXYDataset();
        XYBarRenderer renderer = new XYBarRenderer(0);
        NumberAxis xAxis = new NumberAxis();
        NumberAxis yAxis;
        if (yLogScale) {
            LogarithmicAxis logAxis = new LogarithmicAxis(null);
            yAxis = logAxis;
            logAxis.setAllowNegativesFlag(true);
        } else {
            yAxis = new NumberAxis();
        }
        
        XYPlot plot = new XYPlot(new XYBarDataset(dataset, getBinSize()),
                xAxis, yAxis, renderer);
        plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
        renderer.setPlot(plot);
        
        plot.setBackgroundPaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.GRAY);
        plot.setDomainGridlinesVisible(false);

        renderer.setShadowVisible(false);
        renderer.setBarPainter(new StandardXYBarPainter());
        renderer.setMargin(0);
        
        final int numBins = getNumBins(view);
        final boolean plotOriginalData = this.aggregate == null;
        final double[] bins = getData(view, plotOriginalData);
        final double[] processedBins = plotOriginalData ? getData(view, false) : bins;
        
        int curSegment = segments.get(0).getID();
        int curSegmentPos = 0;
        final int lastSegment = segments.get(segments.size()-1).getID();
        final Pattern pattern = result.pi.getPattern();
        final double[] segmentScores = result.getSegmentScores(solver);

        for (int bin=0; bin<numBins; bin++) {
            while (curSegmentPos >= result.solution[curSegment]-1e-8 && curSegment <= lastSegment) {
                curSegment++;
                curSegmentPos = 0;
            }
            
            final char pat = result.pi.getPattern().getSegments().get(curSegment).getPattern();
            
            if (curSegmentPos == 0 && bin > 0) {
                ValueMarker marker = new ValueMarker(bin*getBinSize());
                marker.setPaint(markerColor);
                plot.addDomainMarker(marker);
            }
            if (curSegmentPos == 0 && pat != Segment.SEGMENT_IGNORE) {
                final double segmentStart = bin*getBinSize();
                final double segmentEnd = segmentStart + result.solution[curSegment];
                final double middle = (segmentStart + segmentEnd) / 2.0;
                final double score = segmentScores[curSegment];
                XYTextAnnotation ann = new XYTextAnnotation(
                        String.format("%c: %.2f", pat, score),
                        middle, 0);
                ann.setTextAnchor(TextAnchor.TOP_CENTER);
                plot.addAnnotation(ann);
            }
            curSegmentPos += getBinSize();
            
            final double value = bins[bin];
            final double processedValue = processedBins[bin];
            
            final Color color;
            final boolean correct = isBinClassificationCorrect(result, view, pattern.getSegment(curSegment), bin, processedValue);
            if (correct) {
                switch (pat) {
                case Segment.SEGMENT_LOW:
                    color = Color.BLUE;
                    break;
                case Segment.SEGMENT_HIGH:
                    color = Color.BLACK;
                    break;
                case Segment.SEGMENT_PEAK:
                    color = Color.DARK_GRAY;
                    break;
                case Segment.SEGMENT_IGNORE:
                    color = Color.LIGHT_GRAY;
                    break;
                default:
                    color = Color.LIGHT_GRAY;
                }
            } else {
                color = (pat == Segment.SEGMENT_IGNORE) ? Color.LIGHT_GRAY : Color.RED;
            }
            
            final Integer binID = new Integer(bin);
            dataset.addSeries(binID,
                    new double[][] {{bin*getBinSize()}, {value}});
            renderer.setSeriesPaint(binID, color);
        }

        yAxis.setAutoRangeIncludesZero(true);
        yAxis.configure();
        
        customizePlot(plot, result);
        
        return plot;
    }

    protected boolean isBinClassificationCorrect(SolverResult result, View view, Segment segment, int bin, double value) {
        final char pat = segment.getPattern();
        boolean patHigh = pat  == Segment.SEGMENT_HIGH
            || pat == Segment.SEGMENT_PEAK;
        
        final double threshold = getThreshold(this.bestThreshold);
        final double actualThreshold = patHigh ? threshold * this.separationSqrt : threshold / this.separationSqrt;
        
        return patHigh ? value > actualThreshold : value <= actualThreshold;
    }
    
    protected void customizePlot(XYPlot plot, SolverResult result) {
        final Color markerColor = new Color(0, 196, 0);
        final double threshold = getThreshold(this.bestThreshold);
        ValueMarker marker = new ValueMarker(threshold);
        marker.setPaint(markerColor);
        plot.addRangeMarker(marker);
    }
    
    @Override
    protected boolean isViewActive(View view, int index) {
        if (this.aggregate == null) return true;
        else return index == 0;
    }
}
