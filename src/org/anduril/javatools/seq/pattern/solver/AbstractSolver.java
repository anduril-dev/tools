package org.anduril.javatools.seq.pattern.solver;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.ui.TextAnchor;

import org.anduril.javatools.seq.pattern.model.Model;
import org.anduril.javatools.seq.pattern.model.ModelInstance;
import org.anduril.javatools.seq.pattern.model.PatternInstance;
import org.anduril.javatools.seq.pattern.model.ScorerConfiguration;
import org.anduril.javatools.seq.pattern.model.Segment;
import org.anduril.javatools.seq.pattern.model.View;

public abstract class AbstractSolver extends BaseConfigurable {
    public static final Random random = new Random();
    
    private final ModelInstance modelInst;
    private final AbstractScorer[] scorers;
    
    public AbstractSolver(ModelInstance modelInst, Map<String, String> args) {
        super(args);
        
        this.modelInst = modelInst;
        int binSize = getIntArgument("binSize");
        
        final int numScorers = modelInst.getModel().getScorers().size();
        this.scorers = new AbstractScorer[numScorers];
        for (ScorerConfiguration scorer: modelInst.getModel().getScorers()) {
            this.scorers[scorer.getID()] = AbstractScorer.getInstance(scorer, modelInst, binSize);
        }
    }
    
    public ModelInstance getModelInstance() {
        return this.modelInst;
    }
    
    public AbstractScorer[] getScorers() {
        return this.scorers;
    }
    
    public AbstractScorer getScorer(View view) {
        if (view.getScorer() == null) {
            String msg = String.format("Scorer for view %s is not set", view.getName());
            throw new IllegalArgumentException(msg);
        }
        return this.scorers[view.getScorer().getID()];
    }

    public AbstractScorer getScorer(Segment segment) {
        return this.scorers[segment.getView().getScorer().getID()];
    }
    
    public AbstractScorer getScorer(int scorerID) {
        return this.scorers[scorerID];
    }
    
    public void setScorer(ScorerConfiguration scorerConf, AbstractScorer scorer) {
        this.scorers[scorerConf.getID()] = scorer;
    }
    
    public void setData(String source, double[] data) {
        boolean found = false;
        for (AbstractScorer scorer: this.scorers) {
            int numFound = scorer.setData(source, data);
            if (numFound > 0) found = true;
        }
        if (!found) {
            throw new IllegalArgumentException("Data source not present in model: "+source);
        }
    }
    
    public int getScorerMaxScale() {
        int maxScale = 1;
        for (AbstractScorer scorer: this.scorers) {
            maxScale = Math.max(maxScale, scorer.getBinSize());
        }
        return maxScale;
    }
    
    private double getScoreLimitGeneric(PatternInstance pi, boolean min) {
        if (pi.getPattern().getScoreFunction() == null) {
            String msg = String.format("No score function has been defined for pattern %s",
                    pi.getPattern().getName());
            throw new IllegalArgumentException(msg);
        }
        
        final int index = min ? 0 : 1;
        double[] segmentScores = new double[pi.getPattern().getSegments().size()];
        for (Segment segment: pi.getPattern().getSegments()) {
            AbstractScorer scorer = getScorer(segment);
            segmentScores[segment.getID()] = scorer.getScoreStatistics()[index];
        }
        return pi.getPattern().getScoreFunction().evaluate(segmentScores);
    }
    
    public double getScoreRange(PatternInstance pi) {
        final double low = getScoreLimitGeneric(pi, true);
        final double high = getScoreLimitGeneric(pi, false);
        return high - low;
    }
 
    public double evaluate(PatternInstance pi, double[] solution) {
        if (pi.getPattern().getScoreFunction() == null) {
            String msg = String.format("No score function has been defined for pattern %s",
                    pi.getPattern().getName());
            throw new IllegalArgumentException(msg);
        }
        
        final int numSegments = pi.getNumSegments();
        double[] segmentScores = new double[numSegments];
        double[] viewOffsets = new double[modelInst.getModel().getViews().size()];
        for (Segment segment: pi.getPattern().getSegments()) {
            final int segID = segment.getID();
            final View view = segment.getView();
            AbstractScorer scorer = getScorer(view);
            segmentScores[segID] = scorer.evaluateScore(view,
                    viewOffsets[view.getID()], solution[segID], segment.getPattern());
            viewOffsets[view.getID()] += solution[segID];
        }
        
        return pi.getPattern().getScoreFunction().evaluate(segmentScores);
    }
    
    public double[] evaluateSegmentScores(PatternInstance pi, double[] solution) {
        final int numSegments = pi.getNumSegments();
        double[] segmentScores = new double[numSegments];
        if (solution.length != numSegments) {
            throw new IllegalArgumentException("Solution is of wrong length for pattern "+pi.getPattern().getName());
        }
        
        double[] viewOffsets = new double[modelInst.getModel().getViews().size()];
        for (Segment segment: pi.getPattern().getSegments()) {
            final int segID = segment.getID();
            final View view = segment.getView();
            AbstractScorer scorer = getScorer(view);
            segmentScores[segID] = scorer.evaluateScore(view,
                    viewOffsets[view.getID()], solution[segID], segment.getPattern());
            viewOffsets[view.getID()] += solution[segID];
        }

        return segmentScores;
    }
    
    public double[][] getSegmentMeanSD(PatternInstance pi, double[] solution) {
        final int numSegments = pi.getNumSegments();
        if (solution.length != numSegments) {
            throw new IllegalArgumentException("Solution is of wrong length for pattern "+pi.getPattern().getName());
        }

        final double[] segmentMeans = new double[numSegments];
        final double[] segmentSD = new double[numSegments];
        
        final double[] viewOffsets = new double[modelInst.getModel().getViews().size()];
        for (Segment segment: pi.getPattern().getSegments()) {
            final int segID = segment.getID();
            final View view = segment.getView();
            AbstractScorer scorer = getScorer(view);
            
            final double[] meanSD = scorer.getSegmentMeanSD(view,
                    viewOffsets[view.getID()], solution[segID]);
            
            segmentMeans[segID] = meanSD[0];
            segmentSD[segID] = meanSD[1];
            viewOffsets[view.getID()] += solution[segID];
        }

        return new double[][] {segmentMeans, segmentSD};
    }
    
    public void visualize(SolverResult result, File output,
            int width, int height, String title, boolean yLogScale) throws IOException {
        NumberAxis xAxis = new NumberAxis();
        
        CombinedDomainXYPlot combinedPlot = new CombinedDomainXYPlot(xAxis);
        combinedPlot.setOrientation(PlotOrientation.VERTICAL);
        
//        double lowBound = 1e9, highBound = -1e9;
        
        Model model = result.pi.getModelInstance().getModel();
        List<View> usedViews = new ArrayList<View>();
        
        final int numScorers = model.getScorers().size();
        double[] lowBounds = new double[model.getScorers().size()];
        double[] highBounds = new double[model.getScorers().size()];
        for (int i=0; i<numScorers; i++) {
            lowBounds[i] = 1e9;
            highBounds[i] = 0;
        }
        
        for (View view: model.getViews()) {
            AbstractScorer scorer = getScorer(view);
            XYPlot subPlot = scorer.makeSamplePlot(result, this, view, yLogScale);
            if (subPlot == null) continue;
            combinedPlot.add(subPlot);
            usedViews.add(view);
            
            final double low = subPlot.getRangeAxis().getLowerBound();
            final double high = subPlot.getRangeAxis().getUpperBound();
            lowBounds[scorer.getID()] = Math.min(lowBounds[scorer.getID()], low);
            highBounds[scorer.getID()] = Math.max(highBounds[scorer.getID()], high);
        }

        final Color markerColor = new Color(180, 180, 180);
        
        for (int i=0; i<combinedPlot.getSubplots().size(); i++) {
            // Find bounds for the common Y axis
            XYPlot subPlot = (XYPlot)combinedPlot.getSubplots().get(i);
            ValueAxis yAxis = subPlot.getRangeAxis();
            yAxis.setAutoRange(false);
            
            final View view = usedViews.get(i);
            final String label = view.getName();
            final int xMax = getModelInstance().getViewLength(view);
            final double lowBound = lowBounds[view.getScorer().getID()];
            final double highBound = highBounds[view.getScorer().getID()];
            
            XYTextAnnotation ann = new XYTextAnnotation(label,
                    xMax, highBound);
            ann.setFont(new Font("SansSerif", java.awt.Font.BOLD, 14));
            ann.setTextAnchor(TextAnchor.TOP_RIGHT);
            subPlot.addAnnotation(ann);
            
            yAxis.setLowerBound(lowBound);
            yAxis.setUpperBound(highBound);
            
            // AbstractScorer.makeSamplePlot may place text annotations whose
            // y value is preliminary set to 0, so we place them on top of plot
            for (Object annObj: subPlot.getAnnotations()) {
                if (annObj instanceof XYTextAnnotation) {
                    XYTextAnnotation annotation = (XYTextAnnotation)annObj;
                    if (annotation.getY() == 0) {
                        annotation.setY(highBound*0.95);
                    }
                }
            }
  
            final int leftOffset = result.pi.getModelInstance().getViewLeftOffset(view);
            final int rightOffset = result.pi.getModelInstance().getViewRightOffset(view);
            if (leftOffset != 0 || rightOffset != 0) {
                ValueMarker marker = new ValueMarker(leftOffset);
                marker.setPaint(markerColor);
                subPlot.addDomainMarker(marker);

                marker = new ValueMarker(leftOffset+result.pi.getModelInstance().getRegionLength());
                marker.setPaint(markerColor);
                subPlot.addDomainMarker(marker);
            }
        }
        
        JFreeChart chart = new JFreeChart(title, null, combinedPlot, false);
        
        chart.setBackgroundPaint(Color.WHITE);
        chart.setAntiAlias(false);
        chart.setTextAntiAlias(true);
        
        output.getParentFile().mkdirs();
        ChartUtilities.saveChartAsPNG(output, chart, width, height);
    }

    public void visualize(SolverResult result, File output, String title, boolean yLogScale) throws IOException {
        visualize(result, output, 800, 600, title, yLogScale);
    }
    
    public void visualize(SolverResult result, File output, String title) throws IOException {
        visualize(result, output, title, false);
    }
    
    public abstract SolverResult optimize(PatternInstance pi);
    
    public abstract void visualizeLog(File output, int width, int height,
            String title) throws IOException;
    
    public void visualizeLog(File output, String title) throws IOException {
        visualizeLog(output, 800, 600, title);
    }
    
}
