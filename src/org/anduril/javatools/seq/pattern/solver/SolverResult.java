package org.anduril.javatools.seq.pattern.solver;

import org.anduril.javatools.seq.ArrayTools;
import org.anduril.javatools.seq.pattern.model.PatternInstance;

public class SolverResult implements Comparable<SolverResult> {
    public final PatternInstance pi;
    public double[] solution;
    public double score;
    
    public SolverResult(PatternInstance pi, double[] solution, double score) {
        if (solution.length != pi.getNumSegments()) {
            throw new IllegalArgumentException("Solution must have as many items as there are segments");
        }
        this.pi = pi;
        this.solution = solution;
        this.score = score;
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("Score %.3f, lengths ", this.score));
        sb.append(ArrayTools.arrayToString(this.solution));
        return sb.toString();
    }

    @Override
    public int compareTo(SolverResult other) {
        if (this.score < other.score) return 1;
        else if (this.score > other.score) return -1;
        else return 0;
    }

    public double[] getSegmentScores(AbstractSolver solver) {
        return solver.evaluateSegmentScores(this.pi, this.solution);
    }
    
    public double[][] getSegmentMeanSD(AbstractSolver solver) {
        return solver.getSegmentMeanSD(this.pi, this.solution);
    }
}
