package org.anduril.javatools.seq.pattern.solver;

import java.util.HashMap;
import java.util.Map;

public class BaseConfigurable {
    private final Map<String, String> args;
    
    public BaseConfigurable(Map<String, String> args) {
        if (args == null) args = new HashMap<String, String>();
        this.args = args;
    }
    
    public final Map<String, String> getArgs() {
        return this.args;
    }
    
    public final String getArgument(String name) {
        return this.args.get(name);
    }
    
    public final int getIntArgument(String name) {
        String value = getArgument(name);
        if (value == null) {
            throw new IllegalArgumentException("No value for argument: "+name);
        }
        return Integer.parseInt(value);
    }

    public final double getDoubleArgument(String name) {
        String value = getArgument(name);
        if (value == null) {
            throw new IllegalArgumentException("No value for argument: "+name);
        }
        return Double.parseDouble(value);
    }
    
    public final boolean hasArgument(String name) {
        return this.args.containsKey(name);
    }
    
    public void setArgument(String name, String value) {
        this.args.put(name, value);
    }
    
    public void setArguments(Map<String, String> args) {
        this.args.putAll(args);
    }
    
    public void setDefaultArgument(String name, String value) {
        if (!this.args.containsKey(name)) {
            this.args.put(name, value);
        }
    }
}
