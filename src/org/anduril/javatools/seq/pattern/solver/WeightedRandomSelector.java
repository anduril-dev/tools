package org.anduril.javatools.seq.pattern.solver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WeightedRandomSelector {
    public static final double DEFAULT_MIN_WEIGHT = 1;
    public static final double DEFAULT_MAX_WEIGHT = 1000;
    
    private final double[] cumulative;
    private final double[] minWeights;
    private final double[] maxWeights;
    private final ArrayList<WeightedRandomSelector> children;
    
    public WeightedRandomSelector(int numElements, double[] initWeights, double[] minWeights, double[] maxWeights) {
        if (numElements < 0) {
            throw new IllegalArgumentException("numElements must be non-negative");
        }
        
        this.children = new ArrayList<WeightedRandomSelector>(numElements);
        for (int i=0; i<numElements; i++) this.children.add(null);

        if (minWeights == null) {
            minWeights = new double[numElements];
            Arrays.fill(minWeights, DEFAULT_MIN_WEIGHT);
        }
        if (maxWeights == null) {
            maxWeights = new double[numElements];
            Arrays.fill(maxWeights, DEFAULT_MAX_WEIGHT);
        }
        
        if (minWeights.length != numElements) {
            throw new IllegalArgumentException("Length of minWeights must equal numElements");
        }
        if (maxWeights.length != numElements) {
            throw new IllegalArgumentException("Length of maxWeights must equal numElements");
        }
        if (initWeights != null && initWeights.length != numElements) {
            throw new IllegalArgumentException("Length of initWeights must equal numElements");
        }
        
        this.minWeights = minWeights;
        this.maxWeights = maxWeights;
        
        this.cumulative = new double[numElements];
        int sum = 0;
        boolean initOK = false || numElements == 0;
        for (int i=0; i<this.cumulative.length; i++) {
            final double min = this.minWeights[i];
            final double max = this.maxWeights[i];
            if (min > max) {
                throw new IllegalArgumentException("Invalid min/max weights for element "+i);
            }
            
            double init = initWeights == null ? min : initWeights[i];
            if (init < min) init = min;
            else if (init > max) init = max;
            else initOK = true;
            sum += init;
            this.cumulative[i] = sum;
        }
        
        if (!initOK) {
            throw new IllegalArgumentException("Initial weights are out of min/max range for each element");
        }
    }
    
    public WeightedRandomSelector(int numElements, double[] initMinWeight, double[] maxWeights) {
        this(numElements, initMinWeight, initMinWeight, maxWeights);
    }
    
    public WeightedRandomSelector(int numElements) {
        this(numElements, null, null, null);
    }
    
    public final int getNumElements() {
        return this.cumulative.length;
    }
    
    public final double getWeight(int element) {
        if (element == 0) return this.cumulative[element];
        else return this.cumulative[element] - this.cumulative[element-1];
    }

    public final double[] getWeights() {
        final double[] weights = new double[getNumElements()];
        double prev = 0;
        for (int i=0; i<this.cumulative.length; i++) {
            weights[i] = this.cumulative[i] - prev;
            prev = this.cumulative[i];
        }
        return weights;
    }
    
    public final double[] getCumulativeWeights() {
        return this.cumulative;
    }
    
    public final int getRandomElement() {
        final double sum;
        try {
            sum = this.cumulative[this.cumulative.length-1];
        } catch(ArrayIndexOutOfBoundsException e) {
            throw new RuntimeException("Can not select random element: number of elements is 0");
        }
        
        if (sum < 1e-8) {
            throw new RuntimeException("All weights are 0: can not select random element");
        }
        final double rnd = AbstractSolver.random.nextDouble() * sum;
        int index = Arrays.binarySearch(this.cumulative, rnd);
        if (index < 0) index = -index - 1;
        return index;
    }
    
    public final void setWeight(int element, double weight) {
        if (weight < this.minWeights[element]) weight = this.minWeights[element];
        else if (weight > this.maxWeights[element]) weight = this.maxWeights[element];
        
        final double oldWeight = getWeight(element);
        final double delta = weight - oldWeight;
        if (delta == 0) return;
        for (int i=element; i<this.cumulative.length; i++) {
            this.cumulative[i] += delta;
        }
    }
    
    public final void addWeight(int element, double delta) {
        setWeight(element, getWeight(element)+delta);
    }
    
    public final void multiplyWeight(int element, double factor) {
        setWeight(element, getWeight(element)*factor);
    }
    
    public final void resetWeight(int element) {
        setWeight(element, this.minWeights[element]);
    }
    
    public final void resetWeights(boolean recursive) {
        int sum = 0;
        for (int i=0; i<this.cumulative.length; i++) {
            sum += this.minWeights[i];
            this.cumulative[i] = sum;
            if (recursive) {
                WeightedRandomSelector child = getChild(i);
                if (child != null) child.resetWeights(recursive);
            }
        }
    }

    public final List<WeightedRandomSelector> getChildren() {
        return this.children;
    }

    public final WeightedRandomSelector getChild(int element) {
        return this.children.get(element);
    }

    public void setChild(int element, WeightedRandomSelector child) {
        this.children.set(element, child);
    }
    
    public final int getRandomPath(int[] result) {
        WeightedRandomSelector sel = this;
        int level = 0;
        while (sel != null) {
            final int element = sel.getRandomElement();
            result[level] = element;
            sel = sel.getChild(element);
            level++;
        }
        return level;
    }
}
