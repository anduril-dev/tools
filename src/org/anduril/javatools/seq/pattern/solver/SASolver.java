package org.anduril.javatools.seq.pattern.solver;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import org.anduril.javatools.seq.ArrayTools;
import org.anduril.javatools.seq.pattern.model.ModelInstance;
import org.anduril.javatools.seq.pattern.model.NoSolutionException;
import org.anduril.javatools.seq.pattern.model.PatternInstance;

public class SASolver extends AbstractSolver {
    public static final double RESTART_THRESHOLD = 0.25;
    
    public static final String ARG_COOLING = "cooling";
    public static final String ARG_RESTARTS = "restarts";
    public static final String ARG_ROUNDS = "rounds";
    public static final String ARG_ROUNDS_PER_BOUNDARY = "roundsPerBoundary";
    public static final String ARG_START_TEMPERATURE = "startTemperature";
    public static final String ARG_END_TEMPERATURE = "endTemperature";
    
    public static final boolean DEBUG = false;
    public static final boolean VERBOSE = false;
    
    public enum CoolingSchedule {
        EXPONENTIAL, LINEAR, ADAPTIVE
    }
    
    private CoolingSchedule schedule;
    private double startTemperature;
    private double endTemperature;
    private boolean hasExplicitTemperature;
    
    private double[] scoreLog;
    private double[] temperatureLog;
    private double[] lengthStepLog;
    
    public SASolver(ModelInstance modelInst, Map<String, String> args) {
        super(modelInst, args);
        setDefaultArgument(ARG_ROUNDS, "2000");
        setDefaultArgument(ARG_ROUNDS_PER_BOUNDARY, "0");
        setDefaultArgument(ARG_RESTARTS, "3");
        setDefaultArgument(ARG_COOLING, "exponential");

        final String cooling = getArgument(ARG_COOLING);
        if (cooling.equals("exponential")) {
            this.schedule = CoolingSchedule.EXPONENTIAL;
        } else if (cooling.equals("linear")) {
            this.schedule = CoolingSchedule.LINEAR;
        } else if (cooling.equals("adaptive")) {
            this.schedule = CoolingSchedule.ADAPTIVE;
        } else {
            throw new IllegalArgumentException("Invalid cooling schedule: "+cooling);
        }

        if (hasArgument(ARG_START_TEMPERATURE)) {
            this.startTemperature = getDoubleArgument(ARG_START_TEMPERATURE);
            this.endTemperature = getDoubleArgument(ARG_END_TEMPERATURE);
            this.hasExplicitTemperature = true;
        } else {
            this.hasExplicitTemperature = false;
        }
    }
    
    public double getStartTemperature() {
        return this.startTemperature;
    }
    
    public void setStartTemperature(double temp) {
        this.startTemperature = temp;
        this.hasExplicitTemperature = true;
    }

    public double getEndTemperature() {
        return this.endTemperature;
    }
    
    public void setEndTemperature(double temp) {
        this.endTemperature = temp;
        this.hasExplicitTemperature = true;
    }

    public SolverResult optimize(PatternInstance pi) {
        double[] solution;
        try {
            solution = pi.getInitialSolution();
        } catch (NoSolutionException e) {
            return null;
        }
        pi.validateSolution(solution);
        
        final double[][] elementaryOps = pi.getElementaryOperations();
  
        for (AbstractScorer scorer: getScorers()) {
            scorer.initializeData();
            scorer.initializeAuxState();
            scorer.saveBestAuxState();
        }
        
        final int maxIterations;
        final int roundsPerBoundary = getIntArgument(ARG_ROUNDS_PER_BOUNDARY);
        if (roundsPerBoundary > 0) {
            maxIterations = elementaryOps.length * roundsPerBoundary;
        } else {
            maxIterations = getIntArgument(ARG_ROUNDS);
        }
        
        final int restarts = getIntArgument(ARG_RESTARTS);
        
        final int CHOICE_VECTOR = 0;
        final int CHOICE_AUX = 1;
        
        if (!this.hasExplicitTemperature) {
            double range = getScoreRange(pi);
            this.startTemperature = range;
            this.endTemperature = range / 2000.0;
        }
        
        /* Percentage of operation range at each iteration */
        final double startLengthStep = 0.5;
        final double endLengthStep = 0.005;
        
        this.scoreLog = new double[maxIterations];
        this.temperatureLog = new double[maxIterations];
        this.lengthStepLog = new double[maxIterations];
        
        final int numElemOps = elementaryOps.length;
        final int numAuxOps = getScorers().length;
        WeightedRandomSelector elemWeights = new WeightedRandomSelector(numElemOps);

        boolean[] scorersActive = pi.getModel().getActiveScorers(pi.getPattern());
        final double[] minAuxWeights = new double[numAuxOps];
        for (AbstractScorer scorer: getScorers()) {
            boolean active = scorersActive[scorer.getID()];
            minAuxWeights[scorer.getID()] = active ? 1 : 0; 
        }
        WeightedRandomSelector auxWeights = new WeightedRandomSelector(numAuxOps, minAuxWeights, null);
        
        final double[] minChoiceWeights = new double[2];
        minChoiceWeights[CHOICE_VECTOR] = numElemOps > 0 ? 4 : 0;
        minChoiceWeights[CHOICE_AUX] = 1;
        WeightedRandomSelector choiceWeights = new WeightedRandomSelector(2,
                minChoiceWeights, null);
        choiceWeights.setChild(CHOICE_VECTOR, elemWeights);
        choiceWeights.setChild(CHOICE_AUX, auxWeights);

        final int[][] preferredDirection = new int[2][];
        preferredDirection[CHOICE_VECTOR] = new int[numElemOps];
        preferredDirection[CHOICE_AUX] = new int[numAuxOps];
        
        double score = evaluate(pi, solution);
        
        double minScore = 1e9;
        double bestScore = score;
        double[] bestSolution = solution;
        
        /* Factor is such that T(N) = factor^N * T_start = T_end */
        final double tempFactor = Math.pow(
                this.endTemperature/this.startTemperature,
                1.0/(double)maxIterations);
        double temp = this.startTemperature;
        
        final double lengthStepFactor = Math.pow(
                endLengthStep/startLengthStep,
                1.0/(double)maxIterations);
        double lengthStep = startLengthStep;
        final int restartPeriod = (int)Math.ceil((double)maxIterations / (restarts+1));
        
        final double minScale = getScorerMaxScale();
        int upMoves = 0;
        
        for (int round=0; round<maxIterations; round++) {
            /* Runs from 1 to 0 as i goes from 0 to max */
            final double phase = ((maxIterations - round) / (double)maxIterations);
            
            final double curLengthStep = lengthStep;
            lengthStep *= lengthStepFactor;
            
            if (restarts > 0 && round > 0 && (round % restartPeriod) == 0) {
                final double scoreDiff = bestScore - score;
                final double scoreRange = bestScore - minScore;
                if (scoreDiff > RESTART_THRESHOLD * scoreRange) {
                    solution = bestSolution;
                    score = bestScore;
                    for (AbstractScorer scorer: getScorers()) {
                        scorer.revertAuxState(true);
                    }
                    choiceWeights.resetWeights(true);
                    for (int[] pref: preferredDirection) Arrays.fill(pref, 0);
                    if (VERBOSE) System.out.println(String.format("Round %d: restart", round));
                }
            }
            
            double[] newSolution = Arrays.copyOf(solution, solution.length);
            
            int choice = -1;
            int operationIndex = -1;
            double opScale = 0;
            
            final int[] randomPath = new int[2];
            for (int attempt=0; attempt<20; attempt++) {
                choiceWeights.getRandomPath(randomPath);
                choice = randomPath[0];
                operationIndex = randomPath[1];
                if (choice == CHOICE_AUX) {
                    opScale = getScorer(operationIndex).changeAuxState(phase,
                            preferredDirection[choice][operationIndex]);
                } else {
                    double[] operation = elementaryOps[operationIndex];
                    opScale = applyOperation(pi, newSolution, operation, curLengthStep,
                            minScale, preferredDirection[choice][operationIndex]);
                }
                
                if (Math.abs(opScale) > 1e-5) {
                    break;
                } else {
                    choiceWeights.getChild(choice).resetWeight(operationIndex);
                    preferredDirection[choice][operationIndex] = 0;
                }
            }
            
            final double newScore = evaluate(pi, newSolution);
            if (newScore > bestScore) {
                bestScore = newScore;
                bestSolution = newSolution;
                for (AbstractScorer scorer: getScorers()) {
                    scorer.saveBestAuxState();
                }
            }
            
            if (newScore > score) {
                upMoves /= 2;
                preferredDirection[choice][operationIndex] = (int)Math.signum(opScale);
                choiceWeights.addWeight(choice, 1);
                choiceWeights.getChild(choice).addWeight(operationIndex, 1);
            } else {
                upMoves++;
                preferredDirection[choice][operationIndex] = 0;
                choiceWeights.multiplyWeight(choice, 0.5);
                choiceWeights.getChild(choice).multiplyWeight(operationIndex, 0.5);
            }
            
            if (newScore < minScore) minScore = newScore;
            
            double prob = Math.exp((newScore-score) / temp);
            
            switch (this.schedule) {
            case ADAPTIVE:
                final int ROUNDS_MAX_TEMP = 20;
                final double adaptiveFactor =
                    (this.startTemperature - this.endTemperature)
                    / ROUNDS_MAX_TEMP;
                temp = this.endTemperature + adaptiveFactor * upMoves;
                break;
            case EXPONENTIAL:
                temp *= tempFactor;
                break;
            case LINEAR:
                final double factor = (this.endTemperature - this.startTemperature) / maxIterations;
                temp = this.startTemperature + factor * round;
                break;
            default:
                throw new IllegalArgumentException("Invalid cooling schedule: "+this.schedule);
            }
            
            if (newScore >= score || prob >= 1 || AbstractSolver.random.nextFloat() < prob) {
                /* Move to neighbor state */
                score = newScore;
                solution = newSolution;
                if (VERBOSE) System.out.println(String.format(
                        "Round %d: move to [%s], score %.3f",
                        round, ArrayTools.arrayToString(solution), score));
            } else {
                /* Revert to previous state */
                if (choice == CHOICE_AUX) {
                    getScorer(operationIndex).revertAuxState(false);
                }
            }
            
            this.scoreLog[round] = score;
            this.temperatureLog[round] = temp;
            this.lengthStepLog[round] = opScale;
            
            if (DEBUG) {
                pi.validateSolution(newSolution);
                if (Math.abs(newScore) > 1e8) {
                    throw new RuntimeException("Score out of bounds: "+newScore);
                }
            }
        }

        for (AbstractScorer scorer: getScorers()) {
            scorer.revertAuxState(true);
        }
        
        pi.validateSolution(bestSolution);
        if (Math.abs(bestScore) > 1e8) {
            throw new RuntimeException("Score out of bounds: "+bestScore);
        }
        
        final double bestScore2 = evaluate(pi, bestSolution);
        if (Math.abs(bestScore - bestScore2) > 1e-6) {
            throw new RuntimeException(String.format(
                    "Internal error: inconsistent best scores: %f vs %f", bestScore, bestScore2));
        }
        
        return new SolverResult(pi,
                Arrays.copyOf(bestSolution, pi.getNumSegments()),
                bestScore);
    }

    private double applyOperation(PatternInstance pi, double[] solution, double[] operation,
            double relativeScale, double minScale, int preferredDirection) {
        final double[] limits = pi.getOperationRange(solution, operation);

        double scale;
        final boolean canIncrease = limits[1] > minScale;
        final boolean canDecrease = limits[0] < -minScale;
        scale = Math.abs(limits[1] - limits[0]) * relativeScale;

        if (preferredDirection > 0 && !canIncrease) return 0;
        if (preferredDirection < 0 && !canDecrease) return 0;
        
        if (!canIncrease && !canDecrease) {
            return 0;
        } else if (canIncrease && canDecrease) {
            if (preferredDirection > 0) {
                /* Prefer positive => keep */
            } else if (preferredDirection < 0) {
                scale = -scale;
            } else if (AbstractSolver.random.nextBoolean()) {
                /* No preference => random */
                scale = -scale;
            }
        } else if (canDecrease) {
            scale = -scale;
        }

        if (Math.abs(scale) < minScale) scale = minScale * Math.signum(scale);

        if (scale < limits[0]) scale = limits[0];
        if (scale > limits[1]) scale = limits[1];

        if (VERBOSE) System.out.println(String.format("Scale: %.2f, operation: %s",
                scale, ArrayTools.arrayToString(operation, 2)));

        for (int i=0; i<operation.length; i++) {
            solution[i] += operation[i] * scale;
        }
        
        return scale;
    }

    public void visualizeLog(File output, int width, int height,
            String title) throws IOException {
        final DefaultCategoryDataset scoreDataset = new DefaultCategoryDataset();
        final DefaultCategoryDataset tempDataset = new DefaultCategoryDataset();
        final DefaultCategoryDataset lengthDataset = new DefaultCategoryDataset();
        final Integer key = new Integer(1);
        
        for (int i=0; i<this.scoreLog.length; i++) {
            final Integer id = new Integer(i);
            scoreDataset.addValue(this.scoreLog[i], key, id);
            tempDataset.addValue(this.temperatureLog[i], key, id);
            lengthDataset.addValue(this.lengthStepLog[i], key, id);
        }
        
        final JFreeChart chart = ChartFactory.createLineChart(title,
                null, null,
                scoreDataset,
                PlotOrientation.VERTICAL,
                false, false, false);
        chart.setBackgroundPaint(Color.WHITE);
        chart.setAntiAlias(true);
        chart.setTextAntiAlias(true);

        chart.getCategoryPlot().getDomainAxis().setLabel(String.format("Optimizer round (total: %d)", this.scoreLog.length));
        chart.getCategoryPlot().getDomainAxis().setMinorTickMarksVisible(false);
        chart.getCategoryPlot().getDomainAxis().setTickMarksVisible(false);
        
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.getRangeAxis(0).setLabel("Score");
        
        plot.setRangeAxis(1, new NumberAxis());
        plot.setRangeAxisLocation(1, AxisLocation.BOTTOM_OR_RIGHT);
        plot.getRangeAxis(1).setLabel("Temperature");
        plot.setDataset(1, tempDataset);
        plot.mapDatasetToRangeAxis(1, 1);
        final LineAndShapeRenderer renderer = new LineAndShapeRenderer(false, true);
        renderer.setSeriesShape(0, new Rectangle2D.Double(0, 0, 1, 1));
        plot.setRenderer(1, renderer);
        
        plot.setRangeAxis(2, new NumberAxis());
        plot.setRangeAxisLocation(2, AxisLocation.BOTTOM_OR_RIGHT);
        plot.setDataset(2, lengthDataset);
        plot.getRangeAxis(2).setLabel("OperationScale");
        plot.mapDatasetToRangeAxis(2, 2);
        final LineAndShapeRenderer renderer2 = new LineAndShapeRenderer(false, true);
        renderer2.setSeriesShape(0, new Rectangle2D.Double(0, 0, 1, 1));
        plot.setRenderer(2, renderer2);
        
        Color[] colors = new Color[] { Color.BLACK, Color.RED, Color.BLUE };
        for (int i=0; i<colors.length; i++) {
            Color color = colors[i];
            plot.getRenderer(i).setBasePaint(color);
            plot.getRenderer(i).setSeriesPaint(0, color);
            plot.getRangeAxis(i).setLabelPaint(color);
        }
        
        plot.setBackgroundPaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.GRAY);
        plot.setDomainGridlinesVisible(false);
        
        output.getParentFile().mkdirs();
        ChartUtilities.saveChartAsPNG(output, chart, width, height);
    }

}
