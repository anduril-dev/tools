package org.anduril.javatools.seq.pattern.solver;

import be.ac.ulg.montefiore.run.distributions.RandomDistribution;

/**
 * Gaussian distribution for Jahmm package that tolerates
 * non-positive variances. Such variances are obtained during
 * Baum-Welch optimization.
 * Most code copied from be.ac.ulg.montefiore.run.distributions.GaussianDistribution
 */
public class MarkovGaussian
implements RandomDistribution {
    private static final long serialVersionUID = -647472714285822445L;
    
    public static final double MIN_PROB = Math.exp(HMMScorer.MIN_LOG_PROB * 2);
    
    private final double mean;
    private final double deviation;
    private final double variance;
    
    public MarkovGaussian() {
        this(0.0, 1.0);
    }
    
    public MarkovGaussian(double mean, double variance)
    {
        this.mean = mean;
        this.variance = variance;
        this.deviation = Math.sqrt(variance);
    }
    
    public double mean()
    {
        return mean;
    }
    
    
    /**
     * Returns this distribution's variance.
     *
     * @return This distribution's variance.
     */
    public double variance()
    {
        return variance;
    }
    
    public double generate()
    {
        return AbstractSolver.random.nextGaussian() * deviation + mean;
    }
    
    public double probability(double n)
    {
        if (variance <= 0) return MIN_PROB;
        
        final double expArg = -.5 * (n - mean) * (n - mean) / variance;
        double p = Math.pow(2. * Math.PI * variance, -.5) * Math.exp(expArg);
        return p;
    }
}