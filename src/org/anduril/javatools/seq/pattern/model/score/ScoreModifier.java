package org.anduril.javatools.seq.pattern.model.score;

public interface ScoreModifier {
    public double transform(double value);
    public boolean hasEffect();
}
