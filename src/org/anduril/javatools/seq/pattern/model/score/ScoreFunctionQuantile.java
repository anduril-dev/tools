package org.anduril.javatools.seq.pattern.model.score;

import org.apache.commons.math.stat.descriptive.rank.Percentile;

import org.anduril.javatools.seq.pattern.model.HTMLFormatter;
import org.anduril.javatools.seq.pattern.model.Segment;

public class ScoreFunctionQuantile extends ScoreFunction {
    private final Percentile percentile;
    private double[] scoreBuffer;
    
    public ScoreFunctionQuantile(double quantile, ScoreModifier modifier, double weight) {
        super("quantile", modifier, weight);
        if (quantile < 0 || quantile > 1) {
            String msg = String.format("Invalid quantile %.3f: must be between 0 and 1", quantile);
            throw new IllegalArgumentException(msg);
        }
        double q = quantile == 0 ? 0.01 : quantile*100;
        this.percentile = new Percentile(q);
    }
    
    @Override
    public String toString() {
        return format(String.format("quantile<%.2f>", this.percentile.getQuantile()/100.0), true); 
    }

    @Override
    protected double evaluateRaw(double[] segmentScores) {
        if (this.scoreBuffer == null) {
            this.scoreBuffer = new double[getNumArgs()];
        } else {
            if (this.scoreBuffer.length != getNumArgs()) {
                throw new IllegalArgumentException("Number of arguments has changed since last evaluation");
            }
        }
        
        int index = 0;
        for (ScoreFunction func: getFunctionArgs()) {
            this.scoreBuffer[index] = func.evaluate(segmentScores);
            index++;
        }
        
        for (Segment seg: getSegmentArgs()) {
            this.scoreBuffer[index] = segmentScores[seg.getID()];
            index++;
        }
        
        return this.percentile.evaluate(this.scoreBuffer);
    }

    protected String formatHTMLFunction(HTMLFormatter formatter, int indent, String postfix) {
        return super.formatHTMLFunction(formatter, indent, String.format(", q=%.2f", this.percentile.getQuantile()/100.0));
    }
    
}
