package org.anduril.javatools.seq.pattern.model.score;

import java.util.ArrayList;

import org.anduril.javatools.seq.pattern.model.HTMLFormatter;
import org.anduril.javatools.seq.pattern.model.Segment;

public abstract class ScoreFunction {
    private final String funcName;
    private final ArrayList<ScoreFunction> funcArgs;
    private final ArrayList<Segment> segmentArgs;
    private final ScoreModifier modifier;
    private final double weight;
    
    public ScoreFunction(String funcName, ScoreModifier modifier, double weight) {
        this.funcName = funcName;
        this.funcArgs = new ArrayList<ScoreFunction>();
        this.segmentArgs = new ArrayList<Segment>();
        this.modifier = (modifier != null && modifier.hasEffect()) ? modifier : null;
        this.weight = weight;
    }
    
    public void addArgument(ScoreFunction arg) {
        this.funcArgs.add(arg);
    }
    
    public void addArgument(Segment segment) {
        this.segmentArgs.add(segment);
    }
    
    public final ArrayList<ScoreFunction> getFunctionArgs() {
        return this.funcArgs;
    }
    
    public final ArrayList<Segment> getSegmentArgs() {
        return this.segmentArgs;
    }
    
    public final int getNumArgs() {
        return this.funcArgs.size() + this.segmentArgs.size();
    }
    
    public String getFunctionName() {
        return this.funcName;
    }
    
    public final ScoreModifier getModifier() {
        return this.modifier;
    }
    
    public final double getWeight() {
        return this.weight;
    }
    
    public final double evaluate(double[] segmentScores) {
        double score = evaluateRaw(segmentScores);
        if (this.modifier != null) score = this.modifier.transform(score);
        return score;
    }
    
    protected abstract double evaluateRaw(double[] segmentScores);
    
    protected String format(String funcName, boolean weights) {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("%s(", funcName));
        boolean first = true;
        
        for (ScoreFunction func: getFunctionArgs()) {
            if (!first) sb.append(", ");
            if (weights && func.getWeight() != 1) {
                sb.append(String.format("%.1f * ", func.getWeight()));
            }
            sb.append(func);
            first = false;
        }
        
        for (Segment seg: getSegmentArgs()) {
            if (!first) sb.append(", ");
            if (weights && seg.getWeight() != 1) {
                sb.append(String.format("%.1f * ", seg.getWeight()));
            }
            sb.append(seg.getName());
            first = false;
        }
        
        sb.append(')');
        return sb.toString();
    }
    
    public String formatHTML(HTMLFormatter formatter) {
        return formatHTML(formatter, 0);
    }
    
    public String formatHTML(HTMLFormatter formatter, int indent) {
        if (indent == 0) {
            return String.format(
                "<span style=\"white-space: pre\">%s</span>",
                formatHTMLFunction(formatter, indent, null));
        } else {
            return formatHTMLFunction(formatter, indent, null);
        }
    }
    
    protected boolean useWeights() {
        return false;
    }
    
    protected String formatHTMLFunction(HTMLFormatter formatter, int indent, String postfix) {
        final String indentStr = "  ";
        final boolean weights = useWeights();
        
        StringBuffer sb = new StringBuffer();
        for (int i=0; i<indent; i++) sb.append(indentStr);
        sb.append(String.format("%s(", funcName));
        boolean first = true;
        
        for (ScoreFunction func: getFunctionArgs()) {
            if (!first) sb.append(", ");
            sb.append("\n");
            for (int i=0; i<indent+1; i++) sb.append(indentStr);
            if (weights && func.getWeight() != 1) {
                sb.append(String.format("%.1f*", func.getWeight()));
            }
            sb.append(func.formatHTML(formatter, indent+1));
            first = false;
        }
        
        boolean firstSegment = true;
        for (Segment seg: getSegmentArgs()) {
            if (!first) {
                sb.append(", ");
            }
            if (firstSegment && !first) { 
                sb.append("\n");
                for (int i=0; i<indent+2; i++) sb.append(indentStr);
            }
            if (weights && seg.getWeight() != 1) {
                sb.append(String.format("%.1f * ", seg.getWeight()));
            }
            sb.append(formatter.formatSegment(seg));
            first = false;
            firstSegment = false;
        }
        
        if (postfix != null) sb.append(postfix);
        
        sb.append(')');
        return sb.toString();
    }
}
