package org.anduril.javatools.seq.pattern.model.score;

public class ScaleModifier implements ScoreModifier {
    private final double offset;
    private final double scale;
    private final double power;
    private final boolean hasEffect;
    
    public ScaleModifier(double offset, double scale, double power) {
        this.offset = offset;
        this.scale = scale;
        this.power = power;
        this.hasEffect = (offset != 0) || (scale != 1) || (power != 1);
    }
    
    public final boolean hasEffect() {
        return this.hasEffect;
    }
    
    public final double getOffset() {
        return this.offset;
    }
    
    public final double getScale() {
        return this.scale;
    }

    public final double getPower() {
        return this.power;
    }

    public final double transform(double value) {
        if (!this.hasEffect) return value;
        
        value = (value + this.offset) * this.scale;
        if (this.power != 1) value = Math.pow(value, this.power);
        return value;
    }
}