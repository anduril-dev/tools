package org.anduril.javatools.seq.pattern.model.score;

import org.anduril.javatools.seq.pattern.model.Segment;

public class ScoreFunctionMinMax extends ScoreFunction {

    final boolean isMin;
    
    public ScoreFunctionMinMax(boolean minimum, ScoreModifier modifier, double weight) {
        super(minimum ? "min" : "max", modifier, weight);
        this.isMin = minimum;
    }
    
    @Override
    public String toString() {
        return format(this.isMin ? "min" : "max", false); 
    }
    
    @Override
    protected double evaluateRaw(double[] segmentScores) {
        final double preset = this.isMin ? 1e15 : -1e15;
        double result = preset;
        
        for (ScoreFunction func: getFunctionArgs()) {
            final double funcScore = func.evaluate(segmentScores);
            result = this.isMin ? Math.min(result, funcScore) : Math.max(result, funcScore);
        }
        
        for (Segment seg: getSegmentArgs()) {
            final double segmentScore = segmentScores[seg.getID()];
            result = this.isMin ? Math.min(result, segmentScore) : Math.max(result, segmentScore);
        }
        
        return result == preset ? 0 : result;
    }
}
