package org.anduril.javatools.seq.pattern.model.score;

import org.anduril.javatools.seq.pattern.model.Segment;

public class ScoreFunctionMean extends ScoreFunction {
    
    public ScoreFunctionMean(ScoreModifier modifier, double weight) {
        super("mean", modifier, weight);
    }
    
    @Override
    public String toString() {
        return format("mean", true); 
    }
    
    @Override
    protected double evaluateRaw(double[] segmentScores) {
        double scoreSum = 0;
        double weightSum = 0;
        
        for (ScoreFunction func: getFunctionArgs()) {
            final double weight = func.getWeight();
            scoreSum += func.evaluate(segmentScores) * weight;
            weightSum += weight;
        }
        
        for (Segment seg: getSegmentArgs()) {
            final double weight = seg.getWeight();
            scoreSum += segmentScores[seg.getID()] * weight;
            weightSum += weight;
        }

        return (weightSum == 0) ? 0 : scoreSum / weightSum;
    }
    
    @Override
    protected boolean useWeights() {
        return true;
    }

}
