package org.anduril.javatools.seq.pattern.model;

import java.util.ArrayList;

public class SegmentScore {
    private final ArrayList<Segment> segments;
    
    public SegmentScore() {
        this.segments = new ArrayList<Segment>();
    }
    
    public void addSegment(Segment segment) {
        this.segments.add(segment);
    }
    
    public final ArrayList<Segment> getSegments() {
        return this.segments;
    }
}
