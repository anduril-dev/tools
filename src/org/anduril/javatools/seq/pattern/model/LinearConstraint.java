package org.anduril.javatools.seq.pattern.model;

import java.util.ArrayList;
import java.util.List;

public class LinearConstraint {
    public enum Relation {
        EQ("eq", "=", true),
//        LT("lt", "<", false),
        LEQ("leq", "<=", false),
//        GT("gt", ">", false),
        GEQ("geq", ">=", false);
        
        public final String id;
        public final String printable;
        public final boolean isEquality;
        
        private Relation(String id, String printable, boolean isEquality) {
            this.id = id;
            this.printable = printable;
            this.isEquality = isEquality;
        }
        
        public static Relation findByID(String id) {
            for (Relation rel: Relation.values()) {
                if (rel.id.equals(id)) return rel;
            }
            return null;
        }
    };
    
    private Relation relation;
    private List<Segment> segments;
    private List<Double> coeffs;
    private SegmentLength rhs;
    
    public LinearConstraint(Relation relation) {
        this.relation = relation;
        this.segments = new ArrayList<Segment>();
        this.coeffs = new ArrayList<Double>();
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("LC: ");
        
        for (int i=0; i<this.segments.size(); i++) {
            if (i > 0) sb.append(" + ");
            final double coeff = getCoeff(i);
            if (coeff != 1.0) {
                sb.append(String.format("%.2f * ", coeff));
            }
            sb.append(this.segments.get(i).getName());
        }
        
        sb.append(" "+this.relation.printable+" ");
        sb.append(this.rhs);
        
        return sb.toString();
    }

    public Relation getRelation() {
        return this.relation;
    }

    public List<Segment> getSegments() {
        return this.segments;
    }

    public double getCoeff(int index) {
        return this.coeffs.get(index);
    }

    public SegmentLength getRHS() {
        return this.rhs;
    }

    public void setRHS(SegmentLength rhs) {
        this.rhs = rhs;
    }
    
    public void addSegment(Segment segment, double coeff) {
        if (coeff != 0) {
            this.segments.add(segment);
            this.coeffs.add(coeff);
        }
    }
}
