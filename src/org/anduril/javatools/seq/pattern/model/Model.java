package org.anduril.javatools.seq.pattern.model;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.anduril.core.utils.HTMLTools;
import org.anduril.javatools.seq.pattern.solver.BaseConfigurable;

public class Model {
    public static final String ARG_BIN_SIZE = "binSize";
    public static final String ARG_MAX_THREADS = "maxThreads";
    
    public static final String DEFAULT_SOLVER = "SA";
    
    private final List<View> views;
    private final List<Pattern> patterns;
    private final Map<String,ScorerConfiguration> scorers;
    
    private String solverType;
    private BaseConfigurable solverConf;
    
    private int maxChainLength;
    
    public Model() {
        this.views = new ArrayList<View>();
        this.patterns = new ArrayList<Pattern>();
        this.scorers = new HashMap<String, ScorerConfiguration>();
        
        this.solverType = DEFAULT_SOLVER;
        this.solverConf = new BaseConfigurable(null);
        this.solverConf.setDefaultArgument(ARG_BIN_SIZE, "300");
        this.solverConf.setDefaultArgument(ARG_MAX_THREADS, "4");
        
        this.maxChainLength = 0;
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        
        for (View view: getViews()) {
            sb.append(view);
            sb.append('\n');
        }
        for (Pattern pattern: getPatterns()) {
            sb.append(pattern);
            sb.append('\n');
        }
        
        return sb.toString();
    }
    
    public List<View> getViews() {
        return this.views;
    }

    public View getViewByName(String name) {
        for (View view: this.views) {
            if (view.getName().equals(name)) return view;
        }
        return null;
    }
    
    public List<View> getViewsByChain(String chain) {
        List<View> views = new ArrayList<View>();
        if (chain == null || chain.isEmpty()) return views;
        for (View view: this.views) {
            if (chain.equals(view.getChain())) {
                views.add(view);
            }
        }
        return views;
    }
    
    public View getViewByChain(String chain, int index) {
        if (chain == null || chain.isEmpty()) return null;
        for (View view: this.views) {
            if (chain.equals(view.getChain()) && index == view.getChainIndex()) {
                return view;
            }
        }
        return null;
    }
    
    public boolean hasViewBySource(String source) {
        for (View view: this.views) {
            if (view.hasSource(source)) return true;
        }
        return false;
    }
    
    public void addView(View view) {
        this.views.add(view);
        this.maxChainLength = Math.max(1, Math.max(this.maxChainLength, view.getChainIndex()));
    }
    
    public int getMaxChainLength() {
        return this.maxChainLength;
    }
    
    public List<Pattern> getPatterns() {
        return this.patterns;
    }
    
    public void addPattern(Pattern pattern) {
        this.patterns.add(pattern);
    }
    
    public List<Segment> getSegmentsByRegexp(Pattern pattern, String regexp, boolean includeIgnored) {
        final java.util.regex.Pattern compiled = java.util.regex.Pattern.compile(regexp); 
        List<Segment> segments = new ArrayList<Segment>();
        for (Segment segment: pattern.getSegments()) {
            if (compiled.matcher(segment.getName()).matches()) {
                if (includeIgnored || segment.getPattern() != Segment.SEGMENT_IGNORE) {
                    segments.add(segment);
                }
            }
        }
        return segments;
    }
    
    public void addScorer(ScorerConfiguration scorer) {
        this.scorers.put(scorer.getName(), scorer);
    }
    
    public ScorerConfiguration getScorer(String name) {
        return this.scorers.get(name);
    }
    
    public Collection<ScorerConfiguration> getScorers() {
        return this.scorers.values();
    }
    
    public String getSolverType() {
        return this.solverType;
    }
    
    public BaseConfigurable getSolver() {
        return this.solverConf;
    }
    
    public void setSolver(String type, Map<String, String> args) {
        this.solverType = type;
        this.solverConf.setArguments(args);
    }

    public int getBinSize() {
        return this.solverConf.getIntArgument(ARG_BIN_SIZE);
    }
    
    public int getMaxThreads() {
        return this.solverConf.getIntArgument(ARG_MAX_THREADS);
    }
    
    public Set<View> findUnusedViews() {
        Set<View> unused = new HashSet<View>(this.views);
        for (Pattern pattern: this.patterns) {
            for (Segment segment: pattern.getSegments()) {
                unused.remove(segment.getView());
            }
        }
        return unused;
    }
    
    public boolean[] getActiveScorers(Pattern pattern) {
        boolean[] active = new boolean[this.scorers.size()];
        for (Segment segment: pattern.getSegments()) {
            active[segment.getView().getScorer().getID()] = true;
        }
        return active;
    }
    
    public String toHTML() throws IOException {
        final String templateName = "/model-template.html";
        URL template = Model.class.getResource(templateName);
        if (template == null) {
            throw new IOException("HTML template not found: "+templateName);
        }
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("title", "Model");
        model.put("model", this);
        model.put("formatter", new HTMLFormatter());
        return HTMLTools.renderVelocity(template, model);
    }
}
