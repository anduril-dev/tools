package org.anduril.javatools.seq.pattern.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.anduril.javatools.seq.pattern.model.LinearConstraint.Relation;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunction;

public class Pattern {
    private static int counter = 1;
    
    private final String name;
    private final List<String> tags;
    private List<Segment> segments;
    private Map<View, List<Segment>> viewSegments;
    private List<LinearConstraint> linearConstraints;
    private ScoreFunction scoreFunction;
    
    public Pattern(String name, List<String> tags) {
        if (name == null) name = String.format("pattern%d", Pattern.counter);
        Pattern.counter++;
        this.name = name;
        
        if (tags == null) tags = new ArrayList<String>();
        this.tags = tags;
        
        this.segments = new ArrayList<Segment>();
        this.viewSegments = new HashMap<View, List<Segment>>();
        this.linearConstraints = new ArrayList<LinearConstraint>();
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("Pattern (%s, tags=%s):", this.name, this.tags));
        for (Segment seg: this.segments) {
            sb.append("\n  "+seg);
        }
        for (LinearConstraint lc: this.linearConstraints) {
            sb.append("\n  "+lc);
        }
        if (this.scoreFunction != null) {
            sb.append("\n  score = "+this.scoreFunction);
        }
        return sb.toString();
    }

    public final String getName() {
        return this.name;
    }
    
    public final List<String> getTags() {
        return this.tags;
    }
    
    public List<Segment> getSegments() {
        return segments;
    }
    
    public List<Segment> getSegments(View view) {
        List<Segment> segments = this.viewSegments.get(view);
        return segments == null ? new ArrayList<Segment>() : segments;
    }
    
    public Segment getSegment(int segmentID) {
        return this.segments.get(segmentID);
    }
    
    public String getSegmentString() {
        StringBuffer sb = new StringBuffer(this.segments.size()
                +this.viewSegments.keySet().size());
        View prevView = null;
        for (Segment segment: this.segments) {
            if (prevView != null && !prevView.equals(segment.getView())) {
                sb.append('-');
            }
            sb.append(segment.getPattern());
            prevView = segment.getView();
        }
        return sb.toString();
    }

    public void addSegment(Segment segment) {
        this.segments.add(segment);
        List<Segment> viewSegs = this.viewSegments.get(segment.getView());
        if (viewSegs == null) {
            viewSegs = new ArrayList<Segment>();
            this.viewSegments.put(segment.getView(), viewSegs);
        }
        if (segment.getIndex() != viewSegs.size()) {
            String msg = String.format("Index of segment %s is invalid: %d",
                    segment.getName(), segment.getID());
            throw new IllegalArgumentException(msg);
        }
        viewSegs.add(segment);
    }
    
    public List<LinearConstraint> getLinearConstraints() {
        return linearConstraints;
    }

    public void addConstraint(LinearConstraint constraint) {
        this.linearConstraints.add(constraint);
    }
    
    public ScoreFunction getScoreFunction() {
        return scoreFunction;
    }
    
    public void setScoreFunction(ScoreFunction function) {
        this.scoreFunction = function;
    }
    
    public void addDefaultConstraints() {
        for (Map.Entry<View, List<Segment>> entry: this.viewSegments.entrySet()) {
            LinearConstraint lc = new LinearConstraint(Relation.EQ);
            for (Segment seg: entry.getValue()) {
                lc.addSegment(seg, 1);
            }
            lc.setRHS(new SegmentLength(0, 1, entry.getKey()));
            addConstraint(lc);
        }
    }
}
