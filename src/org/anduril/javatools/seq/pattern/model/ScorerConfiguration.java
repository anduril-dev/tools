package org.anduril.javatools.seq.pattern.model;

import java.util.ArrayList;
import java.util.List;

import org.anduril.javatools.seq.pattern.solver.ArrayScorer;
import org.anduril.javatools.seq.pattern.solver.BaseConfigurable;
import org.anduril.javatools.seq.pattern.solver.HMMScorer;

public class ScorerConfiguration extends BaseConfigurable {
    private final String type;
    private final int id;
    private final String name;
    private final List<View> views;
    
    public ScorerConfiguration(int id, String name, String type) {
        super(null);
        this.type = type;
        this.id = id;
        this.name = name;
        this.views = new ArrayList<View>();
        setDefaultArguments();
    }
    
    private void setDefaultArguments() {
        final String type = getType();
        boolean validType = false;
        
        if (type.equals(ArrayScorer.SCORER_TYPE) || type.equals(HMMScorer.SCORER_TYPE)) {
            validType = true;
            setDefaultArgument(ArrayScorer.ARG_METHOD, ArrayScorer.METHOD_THRESHOLD);
            setDefaultArgument(ArrayScorer.ARG_MEDIAN_WINDOW, "9");
            setDefaultArgument(ArrayScorer.ARG_BG_PERCENTILE, "0.1");
            setDefaultArgument(ArrayScorer.ARG_AGGREGATE, "none");
            setDefaultArgument(ArrayScorer.ARG_SEPARATION, "1");
        }
        
        if (type.equals(HMMScorer.SCORER_TYPE)) {
            validType = true;
            setDefaultArgument(ArrayScorer.ARG_METHOD, HMMScorer.METHOD_THRESHOLD);
            setDefaultArgument(HMMScorer.ARG_NUM_STATES, "4");
        }
        
        if (!validType) {
            throw new IllegalArgumentException("Unknown scorer type: "+type);
        }
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final int getID() {
        return this.id;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public List<View> getViews() {
        return this.views;
    }
    
    public void addView(View view) {
        this.views.add(view);
    }
    
}
