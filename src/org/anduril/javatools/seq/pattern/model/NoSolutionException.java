package org.anduril.javatools.seq.pattern.model;

public class NoSolutionException extends Exception {
    private static final long serialVersionUID = 4640350993502973650L;

    public NoSolutionException(String msg) {
        super(msg);
    }
}
