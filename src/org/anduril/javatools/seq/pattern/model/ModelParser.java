package org.anduril.javatools.seq.pattern.model;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import net.sf.saxon.s9api.DOMDestination;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XsltTransformer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import org.anduril.core.utils.XMLTools;
import org.anduril.javatools.seq.pattern.model.LinearConstraint.Relation;
import org.anduril.javatools.seq.pattern.model.score.ScaleModifier;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunction;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunctionMean;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunctionMinMax;
import org.anduril.javatools.seq.pattern.model.score.ScoreFunctionQuantile;

public class ModelParser implements ErrorHandler {
    public static final String DEFAULT_INITIAL_TEMPLATE = "model";
    
    public static final String E_MODEL = "model";
    public static final String E_LEFT_SHIFT = "left-shift";
    public static final String E_LENGTH = "length";
    public static final String E_PATTERN = "pattern";
    public static final String E_RIGHT_SHIFT = "right-shift";
    public static final String E_SAMPLE = "sample";
    public static final String E_SAMPLES = "samples";
    public static final String E_SEGMENT = "segment";
    public static final String E_LOW_BOUND = "low-bound";
    public static final String E_HIGH_BOUND = "high-bound";
    public static final String E_SEGMENT_REF = "segment-ref";
    public static final String E_LINEAR_CONSTRAINT = "linear-constraint";
    public static final String E_SCORE = "score";
    public static final String E_SCORER = "scorer";
    public static final String E_ARGUMENT = "argument";
    public static final String E_SOLVER = "solver";
    
    private Element root;
    private Model model;
    
    public ModelParser(File file) throws ParserConfigurationException,
            SAXException, IOException {
        DocumentBuilder builder = XMLTools.getDocumentBuilder(getSchemaLocation(), this);
        Document doc = builder.parse(file);
        this.root = doc.getDocumentElement();
    }
    
    public ModelParser(XsltTransformer transformer) throws ParserConfigurationException, SAXException, SaxonApiException, IOException {
        DocumentBuilder builder = XMLTools.getDocumentBuilder(null, this);
        Document doc = builder.newDocument();
        transformer.setDestination(new DOMDestination(doc));
        transformer.transform();
        
        SchemaFactory sf = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
        Schema schema = sf.newSchema(getSchemaURL());
        Validator validator = schema.newValidator();
        validator.validate(new DOMSource(doc));
        
        this.root = doc.getDocumentElement();
    }
    
    public static Model parseModel(File file, String initialXSLTTemplate) throws IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser formatSniffer;
        factory.setNamespaceAware(true);
        try {
            factory.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
            formatSniffer = factory.newSAXParser();
        } catch (ParserConfigurationException pce) {
            throw new IOException(pce);
        } catch (SAXException se) {
            throw new IOException(se);
        }
        XMLHandler handler = new XMLHandler();
        try {
            formatSniffer.parse(file, handler);
        } catch (SAXException se) {
            // Exception is normal here
        }
        
        ModelParser parser;
        try {
            if (handler.isXSLT()) {
                XsltTransformer transformer = new Processor(false).newXsltCompiler().compile(new StreamSource(file)).load();
                if (handler.isStandaloneXSLT()) {
                    transformer.setInitialTemplate(new QName(initialXSLTTemplate));
                } else {
                    transformer.setSource(new StreamSource(file));                
                }
                parser = new ModelParser(transformer);
            } else {
                parser = new ModelParser(file);
            }
        } catch (ParserConfigurationException pce) {
            throw new IOException(pce);
        } catch (SaxonApiException sae) {
            throw new IOException(sae);
        } catch (SAXException se) {
            throw new IOException(se);
        }
        
        return parser.parse();
    }
    
    public static Model parseModel(File file) throws IOException {
        return parseModel(file, DEFAULT_INITIAL_TEMPLATE);
    }
    
    private static class XMLHandler extends DefaultHandler {
        private static final int T_UNKNOWN = 0;
        private static final int T_MODEL = 1;
        private static final int T_XSLT_STANDALONE = 2;
        private static final int T_XSLT_SIMPLIFIED = 3;
        
        private int type = T_UNKNOWN;
        
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (this.type == T_UNKNOWN) {
                if (localName.equals(E_MODEL)) {
                    String xsl = attributes.getValue("xmlns:xsl");
                    if (xsl != null && xsl.equals("http://www.w3.org/1999/XSL/Transform")) {
                        this.type = T_XSLT_SIMPLIFIED;
                    } else {
                        this.type = T_MODEL;
                    }
                } else if (localName.equals("transform") || localName.equals("stylesheet")) {
                    this.type = T_XSLT_STANDALONE;
                }
                throw new SAXException("Planned abort of parsing after first element");
            }
        }
        
        public boolean isXSLT() {
            if (this.type == T_UNKNOWN) {
                throw new RuntimeException("Parser has not been executed or did not find out type of document");
            }
            return this.type != T_MODEL;
        }
        
        public boolean isStandaloneXSLT() {
            if (this.type == T_UNKNOWN) {
                throw new RuntimeException("Parser has not been executed or did not find out type of document");
            }
            return this.type == T_XSLT_STANDALONE;
        }
    }
    
    public Model parse() {
        this.model = new Model();
        
        parseSolver();
        parseScorers();
        parseSamples();
        
        for (Element el: XMLTools.getElementsByTagName(this.root, E_PATTERN)) {
            parsePattern(el);
        }
        
        return this.model;
    }
    
    private static String getSchemaLocation() {
        URL url = ModelParser.class.getResource("/model.xsd");
        if (url == null) {
            System.err.println("Warning: schema of model.xml not found");
            return null;
        }
        return url.toString();
    }

    private static URL getSchemaURL() {
        URL url = ModelParser.class.getResource("/model.xsd");
        if (url == null) {
            System.err.println("Warning: schema of model.xml not found");
            return null;
        }
        return url;
    }

    private void parseSolver() {
        Element solverElem = XMLTools.getChildByName(this.root, E_SOLVER);
        if (solverElem == null) return;
        
        String type = solverElem.getAttribute("type");
        Map<String, String> args = new HashMap<String, String>();

        for (Element argElem: XMLTools.getElementsByTagName(solverElem, E_ARGUMENT)) {
            args.put(argElem.getAttribute("name"), argElem.getAttribute("value"));
        }

        this.model.setSolver(type, args);
    }
    
    private void parseScorers() {
        int id = 0;
        for (Element scorerElem: XMLTools.getElementsByTagName(this.root, E_SCORER)) {
            String name = scorerElem.getAttribute("id");
            String type = scorerElem.getAttribute("type");
            ScorerConfiguration scorer = new ScorerConfiguration(id, name, type);
            this.model.addScorer(scorer);
            id++;
            for (Element argElem: XMLTools.getElementsByTagName(scorerElem, E_ARGUMENT)) {
                scorer.setArgument(argElem.getAttribute("name"),
                        argElem.getAttribute("value"));
            }
        }
    }
    
    private void parseSamples() {
        Element samplesElem = XMLTools.getChildByName(this.root, E_SAMPLES);
        if (samplesElem == null) return;
        ScorerConfiguration defaultScorer = null;
        String scorerName = samplesElem.getAttribute("default-scorer");
        if (scorerName.isEmpty()) {
            if (this.model.getScorers().size() == 1) {
                defaultScorer = this.model.getScorers().iterator().next();
            }
        } else {
            defaultScorer = this.model.getScorer(scorerName);
            if (defaultScorer == null) {
                throw new IllegalArgumentException("Invalid scorer: "+scorerName);
            }
        }
        
        for (Element el: XMLTools.getElementsByTagName(samplesElem, E_SAMPLE)) {
            this.model.addView(parseSample(el, defaultScorer));
        }
    }
    
    private View parseSample(Element el, ScorerConfiguration defaultScorer) {
        int id = this.model.getViews().size();
        String name = el.getAttribute("id");
        String source = el.getAttribute("source");
        
        final ScorerConfiguration scorer;
        String scorerName = el.getAttribute("scorer");
        if (scorerName.isEmpty()) scorer = defaultScorer;
        else {
            scorer = this.model.getScorer(scorerName);
            if (scorer == null) {
                throw new IllegalArgumentException("Invalid scorer: "+scorerName);
            }
        }
        if (scorer == null) {
            throw new IllegalArgumentException("Scorer not set for sample "+name);
        }
        
        String chain = el.getAttribute("chain");
        final int chainIndex;
        if (chain.isEmpty()) {
            chain = null;
            chainIndex = View.NO_CHAIN_INDEX;
        } else {
            chainIndex = this.model.getViewsByChain(chain).size() + 1;    
        }
        
        View view = new View(id, name, scorer, source, chain, chainIndex);
        
        Element shift = XMLTools.getChildByName(el, E_LEFT_SHIFT);
        if (shift != null) {
            view.setLeftOffset(parseLength(XMLTools.getChildByName(shift, E_LENGTH), view));
        }

        shift = XMLTools.getChildByName(el, E_RIGHT_SHIFT);
        if (shift != null) {
            view.setRightOffset(parseLength(XMLTools.getChildByName(shift, E_LENGTH), view));
        }
        
        scorer.addView(view);
        
        return view;
    }
    
    private void parsePattern(Element patternElement) {
        int addedPatterns = 0;
        
        String name = patternElement.getAttribute("name");
        if (name.isEmpty()) name = null;
        
        List<String> tags = new ArrayList<String>();
        for (String tag: patternElement.getAttribute("tags").split(",")) {
            tag = tag.trim();
            if (tag.isEmpty()) continue;
            tags.add(tag);
        }
        
        outerLoop:
        for (int i=0; i<this.model.getMaxChainLength(); i++) {
            Pattern pattern = new Pattern(name, tags);
            boolean hasRelativeSample = false;
            for (Element segmentElement:
                    XMLTools.getChildrenByName(patternElement, E_SEGMENT)) {
                Segment segment = parseSegment(segmentElement, pattern, i+1);
                if (segment == null) continue outerLoop;
                pattern.addSegment(segment);
                if (segmentElement.hasAttribute("relative-index")) {
                    hasRelativeSample = true;
                }
            }

            pattern.addDefaultConstraints();
            this.model.addPattern(pattern);
            addedPatterns++;
            
            for (Element lcElement:
                    XMLTools.getElementsByTagName(patternElement, E_LINEAR_CONSTRAINT)) {
                pattern.addConstraint(parseLinearConstraint(pattern, lcElement));
            }

            Element sfElement = XMLTools.getChildByName(patternElement, E_SCORE);
            if (sfElement != null) {
                pattern.setScoreFunction(parseScoreFunction(pattern, XMLTools.getChildren(sfElement).get(0)));
            }
            
            /* Not an iterated pattern => stop at i=0 */
            if (!hasRelativeSample) break;           
        }
        
        if (addedPatterns == 0) {
            throw new IllegalArgumentException("Relative sample indices do not define any pattern");
        }
    }
    
    private Segment parseSegment(Element el, Pattern pattern, int chainPos) {
        int id = pattern.getSegments().size();
        String name = el.getAttribute("id");
        char pat = el.getAttribute("pattern").charAt(0);
        
        final View view;
        if (el.hasAttribute("sample")) {
            String viewName = el.getAttribute("sample");
            view = this.model.getViewByName(viewName);
        } else if (el.hasAttribute("sample-chain")) {
            final int index;
            if (el.hasAttribute("relative-index")) {
                index = chainPos + Integer.parseInt(el.getAttribute("relative-index"));
                if (index < 1 || index > this.model.getMaxChainLength()) {
                    return null;
                }
            } else if (el.hasAttribute("absolute-index")) {
                index = Integer.parseInt(el.getAttribute("absolute-index"));
            } else {
                String msg = String.format("Pattern %s: View chain index not defined for segment %s",
                        pattern.getName(), name);
                throw new IllegalArgumentException(msg);
            }
            view = this.model.getViewByChain(el.getAttribute("sample-chain"), index);
        } else {
            String msg = String.format("Pattern %s: View not defined for segment %s",
                    pattern.getName(), name);
            throw new IllegalArgumentException(msg);
        }

        if (view == null) {
            String msg = String.format("Pattern %s: Invalid sample for segment %s",
                    pattern.getName(), name);
            throw new IllegalArgumentException(msg);
        }
        
        int viewIndex = pattern.getSegments(view).size();
        Segment segment = new Segment(id, name, pat, view, viewIndex);

        if (el.hasAttribute("weight")) {
            segment.setWeight(Double.parseDouble(el.getAttribute("weight")));
        }
        if (el.getAttribute("nullable").equals("true")) {
            segment.setNullable(true);
        }
        
        Element bound = XMLTools.getChildByName(el, E_LOW_BOUND);
        if (bound != null) {
            segment.setLowBound(parseLength(XMLTools.getChildByName(bound, E_LENGTH), view));
        }
        
        bound = XMLTools.getChildByName(el, E_HIGH_BOUND);
        if (bound != null) {
            segment.setHighBound(parseLength(XMLTools.getChildByName(bound, E_LENGTH), view));
        }
        
        return segment;
    }
    
    private LinearConstraint parseLinearConstraint(Pattern pattern, Element el) {
        final String relStr = el.getAttribute("relation");
        Relation rel = Relation.findByID(relStr);
        if (rel == null) throw new IllegalArgumentException("Invalid relation: "+relStr);
        LinearConstraint constraint = new LinearConstraint(rel);
        
        List<Double> coeffs = new ArrayList<Double>();
        List<Segment> segments = parseSegmentRefs(pattern,
                XMLTools.getChildrenByName(el, E_SEGMENT_REF), coeffs, true);
        View uniqueView = null;
        boolean hasUniqueView = true;
        for (int i=0; i<segments.size(); i++) {
            final Segment segment = segments.get(i);
            constraint.addSegment(segment, coeffs.get(i));
            if (uniqueView == null) uniqueView = segment.getView();
            else if (!segment.getView().equals(uniqueView)) hasUniqueView = false;
        }
        if (!hasUniqueView) uniqueView = null;
        constraint.setRHS(parseLength(XMLTools.getChildByName(el, E_LENGTH), uniqueView));        
        
        return constraint;
    }
    
    private double parseDouble(String str, double defaultValue) {
        if (str == null || str.isEmpty()) return defaultValue;
        else return Double.parseDouble(str);
    }
    
    private ScoreFunction parseScoreFunction(Pattern pattern, Element el) {
        if (el == null) return null;
        
        final String name = el.getTagName();
        final ScoreFunction func;
        
        final double arg = parseDouble(el.getAttribute("arg"), Double.NaN);
        final double weight = parseDouble(el.getAttribute("weight"), 1);
        
        final double offset = parseDouble(el.getAttribute("offset"), 0);
        final double scale = parseDouble(el.getAttribute("scale"), 1);
        final double power = parseDouble(el.getAttribute("power"), 1);
        ScaleModifier scaleMod = new ScaleModifier(offset, scale, power);

        if (name.equals("maximum")) {
            func = new ScoreFunctionMinMax(false, scaleMod, weight);
        } else if (name.equals("minimum")) {
            func = new ScoreFunctionMinMax(true, scaleMod, weight);
        } else if (name.equals("mean")) {
            func = new ScoreFunctionMean(scaleMod, weight);
        } else if (name.equals("quantile")) {
            if (Double.isNaN(arg)) {
                throw new IllegalArgumentException("The percentile must be provided for quantile using the 'arg' attribute");
            }
            func = new ScoreFunctionQuantile(arg, scaleMod, weight);
        } else {
            throw new IllegalArgumentException("Invalid function: "+name);
        }
        
        List<Element> refChildren = new ArrayList<Element>();
        for (Element child: XMLTools.getChildren(el)) {
            if (child.getTagName().equals(E_SEGMENT_REF)) {
                refChildren.add(child);
            } else {
                func.addArgument(parseScoreFunction(pattern, child));
            }
        }
        
        List<Segment> segments = parseSegmentRefs(pattern, refChildren, null, false);
        for (int i=0; i<segments.size(); i++) {
            func.addArgument(segments.get(i));
        }
        
        return func;
    }
    
    private SegmentLength parseLength(Element el, View defaultView) {
        final int absolute;
        if (el.hasAttribute("absolute")) {
            absolute = Integer.parseInt(el.getAttribute("absolute"));
        } else {
            absolute = 0;
        }
        
        final double relative;
        if (el.hasAttribute("relative")) {
            relative = Double.parseDouble(el.getAttribute("relative"));
        } else {
            relative = 0;
        }
        
        final View view;
        if (el.hasAttribute("relative-sample")) {
            view = this.model.getViewByName(el.getAttribute("relative-sample"));
        } else {
            view = defaultView;
        }
        
        return new SegmentLength(absolute, relative, view);
    }
    
    private List<Segment> parseSegmentRefs(Pattern pattern,
            List<Element> refElements, List<Double> outCoeffs, boolean includeIgnored) {
        List<Segment> segments = new ArrayList<Segment>();
        if (outCoeffs != null) outCoeffs.clear();
        
        for (Element el: refElements) {
            String ref = el.getAttribute("ref");
            List<Segment> thisSegments = this.model.getSegmentsByRegexp(pattern, ref, includeIgnored);
            if (thisSegments.isEmpty()) {
                throw new IllegalArgumentException("Regular expression matches no segments: "+ref);
            }
            segments.addAll(thisSegments);
            if (outCoeffs != null) {
                final Double coeff;
                if (el.hasAttribute("coeff")) {
                    coeff = Double.parseDouble(el.getAttribute("coeff"));
                } else {
                    coeff = new Double(1);
                }
                for (int i=0; i<thisSegments.size(); i++) {
                    outCoeffs.add(coeff);
                }
            }
        }
        return segments;
    }
    
    @Override
    public void error(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        throw e;
    }
}
