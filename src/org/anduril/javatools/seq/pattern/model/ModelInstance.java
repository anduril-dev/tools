package org.anduril.javatools.seq.pattern.model;

import java.util.Map;

import org.anduril.javatools.seq.LocusTransformation;
import org.anduril.javatools.seq.pattern.solver.AbstractSolver;
import org.anduril.javatools.seq.pattern.solver.SASolver;

public class ModelInstance {
    private Model model;
    
    private LocusTransformation[] startPositions;
    private int[] viewLengths;
    private PatternInstance[] patternInstances;
    private final int regionLength;
    
    public ModelInstance(Model model, LocusTransformation regionStart, int regionLength) {
        final int numViews = model.getViews().size();
        final int numPatterns = model.getPatterns().size();
        
        this.model = model;
        this.regionLength = regionLength;
        
        this.startPositions = new LocusTransformation[numViews];
        this.viewLengths = new int[numViews];
        
        final int scale = regionStart.getScale();
        if (Math.abs(scale) != 1) {
            throw new IllegalArgumentException("Invalid scale: must be -1 or 1: is "+scale);
        }
        
        for (View view: this.model.getViews()) {
            int leftOffset = view.getLeftOffset().evaluate(regionLength);
            int rightOffset = view.getRightOffset().evaluate(regionLength);
            int start = regionStart.getOffset() - leftOffset * scale;
            if (start < 0) start = 0;
            int end = regionStart.getOffset() + (regionLength + rightOffset) * scale;
            if (end < 0) end = 0;
            if (end != start && (end < start != scale < 0)) {
                throw new IllegalArgumentException(String.format(
                        "View (%s): end (%d) and start (%d) positions are invalid",
                        view.getName(), end, start));
            }
            this.startPositions[view.getID()] = new LocusTransformation(start, scale);
            this.viewLengths[view.getID()] = Math.abs(end - start);
        }
        
        this.patternInstances = new PatternInstance[numPatterns];
        for (int pat=0; pat<this.model.getPatterns().size(); pat++) {
            Pattern pattern = this.model.getPatterns().get(pat);
            PatternInstance inst = new PatternInstance(pattern, this);
            this.patternInstances[pat] = inst;
        }
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (View view: this.model.getViews()) {
            int start = this.startPositions[view.getID()].getOffset();
            int length = this.viewLengths[view.getID()];
            int end = start + length * this.startPositions[view.getID()].getScale();
            sb.append(String.format("View %s: %d to %d, length %d\n",
                    view.getName(), start, end, length));
        }
        for (PatternInstance pi: this.patternInstances) {
            sb.append(pi+"\n");
        }
        return sb.toString();
    }
    
    public Model getModel() {
        return this.model;
    }
    
    public LocusTransformation getViewStart(View view) {
        if (view == null) return null;
        else return this.startPositions[view.getID()];
    }
    
    public int getViewLength(View view) {
        if (view == null) return 0;
        else return this.viewLengths[view.getID()];
    }
    
    public int getViewLeftOffset(View view) {
        return view.getLeftOffset().evaluate(this.regionLength);
    }

    public int getViewRightOffset(View view) {
        return view.getRightOffset().evaluate(this.regionLength);
    }

    public int getRegionLength() {
        return this.regionLength;
    }
    
    public PatternInstance[] getPatternInstances() {
        return this.patternInstances;
    }
    
    public AbstractSolver getSolver() {
        final String type = this.model.getSolverType();
        Map<String, String> args = this.model.getSolver().getArgs();
        final AbstractSolver solver;
        if (type.equals("SA")) {
            solver = new SASolver(this, args);
        } else {
            throw new IllegalArgumentException("Invalid solver type: "+type);
        }
        return solver;
    }
    
}
