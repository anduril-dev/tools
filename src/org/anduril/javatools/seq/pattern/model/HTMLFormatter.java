package org.anduril.javatools.seq.pattern.model;

import java.util.ArrayList;
import java.util.List;

import org.anduril.javatools.seq.LocusTransformation;

public class HTMLFormatter {
    public List<View> getViewsByPattern(Pattern pattern) {
        List<View> views = new ArrayList<View>();
        
        for (Segment segment: pattern.getSegments()) {
            View view = segment.getView();
            if (!views.contains(view)) views.add(view);
        }
        
        return views;
    }
    
    public String formatSegment(Segment segment) {
        final String bgColor = getSegmentBGColor(segment);
        String style = String.format("background-color: %s", bgColor);
        String content = segment.getName();
        return String.format("<span style=\"%s\">%s</span>",
                style, content);
    }
    
    public String formatLength(SegmentLength length, View defaultView) {
        final boolean hasAbsolute = (length.getAbsolute() != 0);
        final boolean hasRelative = (length.getRelative() != 0);

        String relativeFormat;
        if (hasRelative) {
            relativeFormat = String.format("%.0f%%" ,length.getRelative()*100);
            if (length.getRelativeView() != defaultView) {
                relativeFormat += String.format("(%s)", length.getRelativeView().getName());
            }
        } else {
            relativeFormat = null;
        }

        String absoluteFormat;
        if (hasAbsolute) {
            final int abs = length.getAbsolute();
            if (abs < 1000) absoluteFormat = String.format("%db", abs);
            else if (abs < 100000) absoluteFormat = String.format("%.1fkb", abs/1000.0);
            else absoluteFormat = String.format("%.1fMb", abs/1000000.0);
        } else {
            absoluteFormat = null;
        }
        
        if (hasAbsolute && hasRelative) {
            return String.format("%s+%s", relativeFormat, absoluteFormat);
        } else if (hasAbsolute) {
            return absoluteFormat;
        } else if (hasRelative) {
            return relativeFormat;
        } else {
            return "0";
        }
    }
    
    public String formatChain(View view, String noChainString) {
        if (view.getChain() == null) {
            return noChainString;
        } else {
            return String.format("%s: %d", view.getChain(), view.getChainIndex());
        }
    }
    
    private String getSegmentBGColor(Segment segment) {
        switch (segment.getPattern()) {
        case Segment.SEGMENT_PEAK:
        case Segment.SEGMENT_HIGH:
            return "#ffbbbb";
        case Segment.SEGMENT_LOW:
            return "#bbbbff";
        case Segment.SEGMENT_IGNORE:
            return "#bbbbbb";
        default:
            return "#ffffff";
        }
    }
    
    public String formatPattern(Pattern pattern, View view) {
        StringBuffer sb = new StringBuffer();
        
        for (Segment segment: pattern.getSegments(view)) {
            final String bgColor = getSegmentBGColor(segment);
            String style = String.format("background-color: %s", bgColor);
            String content = String.format("%s (%s) &ge;%s &le;%s",
                    segment.getName(), segment.getPattern(),
                    formatLength(segment.getLowBound(), view),
                    formatLength(segment.getHighBound(), view));
            sb.append(String.format("<span style=\"%s\">%s</span> ",
                    style, content));
        }
        
        return sb.toString();
    }
    
    public String formatLinearConstraintLHS(LinearConstraint lc) {
        StringBuffer sb = new StringBuffer();
        
        for (int i=0; i<lc.getSegments().size(); i++) {
            Segment segment = lc.getSegments().get(i);
            double coeff = lc.getCoeff(i);
            
            final boolean coeffPlusOne = coeff == 1;
            final boolean coeffMinusOne = coeff == -1;
            
            if (i > 0) sb.append(coeffMinusOne ? " - " : " + ");
            if (!(coeffPlusOne || coeffMinusOne)) sb.append(String.format("%.2f*", coeff));
            sb.append(formatSegment(segment));
        }
        
        return sb.toString();
    }
    
    public String formatLinearConstraintRelation(LinearConstraint lc) {
        switch (lc.getRelation()) {
        case EQ:
            return "=";
        case LEQ:
            return "&le";
        case GEQ:
            return "&ge";
        default:
            throw new IllegalArgumentException("Unknown relation in linear constraint: "+lc.getRelation());
        }
    }
    
    public String formatDouble(double value) {
        String s = String.format("%f", value);
        s = s.replaceAll("0+$", "");
        s = s.replaceFirst("[.]$", "");
        return s;
    }
    
    public String formatConstraintMatrix(Model model, Pattern pattern, int regionLength) {
        ModelInstance mi = new ModelInstance(model, new LocusTransformation(0, 1), regionLength);
        PatternInstance pi = new PatternInstance(pattern, mi);
        StringBuffer sb = new StringBuffer();
        
        sb.append("<table>\n");
        sb.append("<tr>\n");
        for (Segment seg: pattern.getSegments()) {
            sb.append(String.format("<th><small>%s</small></th>", formatSegment(seg)));
        }
        for (int i=0; i<pi.getNumSlackVariables(); i++) {
            sb.append(String.format("<th>S<sub>%d</sub></th>", i+1));
        }
        sb.append("<th>RHS</th>\n");
        sb.append("</tr>\n");
        
        for (int constraint=0; constraint<pi.getNumConstraints(); constraint++) {
            sb.append("<tr>\n");
            double[] coeffs = pi.getCoeffs(constraint);
            for (double coeff: coeffs) {
                sb.append(String.format("<td>%s</td>\n", formatDouble(coeff)));
            }
            sb.append(String.format("<td>%s</td>\n", formatDouble(pi.getRHS(constraint))));
            sb.append("</tr>\n");
        }
        
        sb.append("</table>\n");
        return sb.toString();
    }
    
    public String formatElementaryOperations(Model model, Pattern pattern, int regionLength) {
        ModelInstance mi = new ModelInstance(model, new LocusTransformation(0, 1), regionLength);
        PatternInstance pi = new PatternInstance(pattern, mi);
        StringBuffer sb = new StringBuffer();

        sb.append("<table>\n");
        sb.append("<tr>\n");
        for (Segment seg: pattern.getSegments()) {
            sb.append(String.format("<th><small>%s</small></th>", formatSegment(seg)));
        }
        for (int i=0; i<pi.getNumSlackVariables(); i++) {
            sb.append(String.format("<th>S<sub>%d</sub></th>", i+1));
        }
        
        for (double[] op: pi.getElementaryOperations()) {
            sb.append("<tr>\n");
            for (double coeff: op) {
                sb.append(String.format("<td>%s</td>\n", formatDouble(coeff)));
            }
            sb.append("</tr>\n");
        }
        
        sb.append("</table>\n");
        return sb.toString();

    }
    
    public String formatInitialSolution(Model model, Pattern pattern, int regionLength) {
        ModelInstance mi = new ModelInstance(model, new LocusTransformation(0, 1), regionLength);
        PatternInstance pi = new PatternInstance(pattern, mi);
        StringBuffer sb = new StringBuffer();

        double[] solution;
        try {
            solution = pi.getInitialSolution();
        } catch (NoSolutionException e) {
            sb.append("No initial solution found");
            return sb.toString();
        }
        
        sb.append("<table>\n");
        sb.append("<tr>\n");
        for (Segment seg: pattern.getSegments()) {
            sb.append(String.format("<th><small>%s</small></th>", formatSegment(seg)));
        }
        for (int i=0; i<pi.getNumSlackVariables(); i++) {
            sb.append(String.format("<th>S<sub>%d</sub></th>", i+1));
        }
        
        sb.append("<tr>\n");
        for (double element: solution) {
            sb.append(String.format("<td>%s</td>\n", formatDouble(element)));
        }
        sb.append("</tr>\n");
        
        sb.append("</table>\n");
        return sb.toString();
    }
    
 }
