package org.anduril.javatools.seq.pattern.model;

public class SegmentLength {
    private int absolute;
    private double relative;
    private View relativeView;

    public SegmentLength(int absolute, double relative, View relativeView) {
        this.absolute = absolute;
        if (relative != 0 && relativeView == null) {
            throw new IllegalArgumentException("View must be given when relative != 0");
        }
        this.relative = relative;
        this.relativeView = relativeView;
    }
    
    public SegmentLength(int absolute) {
        this(absolute, 0, null);
    }
    
    @Override
    public String toString() {
        final boolean hasAbsolute = (absolute != 0);
        final boolean hasRelative = (relative != 0);
        if (hasAbsolute && hasRelative) {
            return String.format("<absolute: %d + relative %.2f(%s)>",
                    this.absolute, this.relative, this.relativeView.getName());
        } else if (hasRelative) {
            return String.format("<relative %.2f(%s)>",
                    this.relative, this.relativeView.getName());
        } else {
            return String.format("<absolute %d>",
                    this.absolute);
        }
    }
    
    public int getAbsolute() {
        return this.absolute;
    }
    
    public void setAbsolute(int absolute) {
        this.absolute = absolute;
    }
    
    public double getRelative() {
        return this.relative;
    }
    
    public void setRelative(double relative) {
        this.relative = relative;
    }
    
    public View getRelativeView() {
        return this.relativeView;
    }
    
    public int evaluate(int relativeLength) {
        return (int)(this.relative * relativeLength) + this.absolute;
    }
}
