package org.anduril.javatools.seq.pattern.model;

public class View {
    public static final int NO_CHAIN_INDEX = -1;
    
    private final int id;
    private String name;
    private final String source;
    private final String chain;
    private final int chainIndex;
    private ScorerConfiguration scorer;
    private SegmentLength leftOffset;
    private SegmentLength rightOffset;

    public View(int id, String name, ScorerConfiguration scorer, String source,
            String chain, int chainIndex) {
        if (chain == null && chainIndex != -1) {
            throw new IllegalArgumentException("Chain is null but chainIndex is defined");
        } else if (chain != null && chainIndex < 1) {
            throw new IllegalArgumentException("Chain is defined but chainIndex is "+chainIndex);
        }
        
        this.id = id;
        this.name = name;
        this.scorer = scorer;
        this.source = source;
        this.chain = chain;
        this.chainIndex = chainIndex;
        this.leftOffset = new SegmentLength(0);
        this.rightOffset = new SegmentLength(0);
    }
    
    @Override
    public String toString() {
        String s = String.format("View %s (%s)", this.id, this.name);
        if (this.chain != null) {
            s += String.format(", chain=<%s, %d>", this.chain, this.chainIndex);
        }
        if (this.leftOffset != null) {
            s += ", left="+this.leftOffset;
        }
        if (this.rightOffset != null) {
            s += ", right="+this.rightOffset;
        }
        s += String.format(", source=%s", this.source);
        return s;
    }
    
    public final int getID() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }
    
    public final ScorerConfiguration getScorer() {
        return this.scorer;
    }
    
    public final String getSource() {
        return this.source;
    }
    
    public final boolean hasSource(String source) {
        return this.source.equals(source);
    }
    
    public final String getChain() {
        return this.chain;
    }
    
    public final int getChainIndex() {
        return this.chainIndex;
    }
    
    public SegmentLength getLeftOffset() {
        return this.leftOffset;
    }
    
    public void setLeftOffset(SegmentLength leftOffset) {
        this.leftOffset = leftOffset;
    }
    
    public SegmentLength getRightOffset() {
        return this.rightOffset;
    }
    
    public void setRightOffset(SegmentLength rightOffset) {
        this.rightOffset = rightOffset;
    }
}
