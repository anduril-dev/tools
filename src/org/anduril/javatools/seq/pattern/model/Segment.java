package org.anduril.javatools.seq.pattern.model;

public class Segment {
    public static final char SEGMENT_LOW = 'L';
    public static final char SEGMENT_HIGH = 'H';
    public static final char SEGMENT_PEAK = 'P';
    public static final char SEGMENT_IGNORE = 'N';
    
    private final int id;
    private String name;
    private final char pattern;
    private final View view;
    private final int index;
    private double weight;
    private boolean nullable;
    private SegmentLength lowBound;
    private SegmentLength highBound;
    
    public Segment(int id, String name, char pattern, View view, int segmentIndex) {
        this.id = id;
        this.name = name;
        this.pattern = pattern;
        this.view = view;
        this.index = segmentIndex;
        this.weight = 1;
        this.nullable = false;
        this.lowBound = new SegmentLength(1);
        this.highBound = new SegmentLength(0, 1, view);
    }
    
    @Override
    public String toString() {
        String s = String.format(
                "Segment %d (%s): %c, view=<%s, %d>",
                this.id, this.name, this.pattern,
                this.view.getName(), this.index);
        if (this.weight != 1) s += String.format(", weight=%.2f", this.weight);
        if (this.nullable) s += ", nullable";
        if (this.lowBound != null) s += ", low=" + this.lowBound;
        if (this.highBound != null) s += ", high=" + this.highBound;
        return s;
    }
    
    public final int getID() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public final View getView() {
        return this.view;
    }

    public final int getIndex() {
        return this.index;
    }
    
    public final char getPattern() {
        return this.pattern;
    }
    
    public double getWeight() {
        return this.weight;
    }
    
    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    public boolean getNullable() {
        return this.nullable;
    }
    
    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public SegmentLength getLowBound() {
        return this.lowBound;
    }

    public void setLowBound(SegmentLength lowBound) {
        this.lowBound = lowBound;
    }

    public SegmentLength getHighBound() {
        return this.highBound;
    }

    public void setHighBound(SegmentLength highBound) {
        this.highBound = highBound;
    }
}
