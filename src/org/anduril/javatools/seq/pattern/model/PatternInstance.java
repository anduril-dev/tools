package org.anduril.javatools.seq.pattern.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.math.linear.Array2DRowRealMatrix;
import org.apache.commons.math.linear.ArrayRealVector;
import org.apache.commons.math.linear.RealMatrix;
import org.apache.commons.math.linear.RealVector;
import org.apache.commons.math.linear.SingularValueDecomposition;
import org.apache.commons.math.linear.SingularValueDecompositionImpl;
import org.apache.commons.math.optimization.GoalType;
import org.apache.commons.math.optimization.OptimizationException;
import org.apache.commons.math.optimization.RealPointValuePair;
import org.apache.commons.math.optimization.linear.LinearObjectiveFunction;
import org.apache.commons.math.optimization.linear.NoFeasibleSolutionException;
import org.apache.commons.math.optimization.linear.Relationship;
import org.apache.commons.math.optimization.linear.SimplexSolver;

import org.anduril.javatools.seq.ArrayTools;

public class PatternInstance {
    /**
     * Tableau matrix stores linear constraints. Format:
     * <pre>
     *        | slack | |  real     | rhs
     * (ineq) 1 0 ... 0 A11 ... A1N R1
     * (ineq) 0 1 ... 0 A21 ... A1N R2
     * (ineq) 0 0 ... 1 AMN ... AMN RM
     * (eq)   0 0 ... 0 B11 ... B1N R(M+1)
     * (eq)   0 0 ... 0 BK1 ... BKN R(M+K)
     * 
     * Simplified:
     * I A1 | R1
     * 0 A2 | R2
     * </pre>
     */
    private Array2DRowRealMatrix tableau;
    
    private final Pattern pattern;
    private final ModelInstance modelInst;
    private int numSlackVariables;
    private final int[] lowBounds;
    private final int[] highBounds;
    
    public PatternInstance(Pattern pattern, ModelInstance modelInst) {
        final int numSegments = pattern.getSegments().size();
        this.pattern = pattern;
        this.modelInst = modelInst;
        this.lowBounds = new int[numSegments];
        this.highBounds = new int[numSegments];
        
        populateTableau(modelInst);
//        simplifyTableau();
        normalizeTableau();
    }
    
    @Override
    public String toString() {
        return String.format("Low: %s, high: %s",
                Arrays.toString(this.lowBounds),
                Arrays.toString(this.highBounds));
    }
    
    private void populateTableau(ModelInstance modelInst) {
        final int numSegments = pattern.getSegments().size();
        
        this.numSlackVariables = 2*numSegments; /* Lower and upper bounds */
        int numEqualityConstraints = 0;
        for (LinearConstraint lc: pattern.getLinearConstraints()) {
            if (lc.getRelation().isEquality) numEqualityConstraints++;
            else this.numSlackVariables++;
        }
        
        this.tableau = new Array2DRowRealMatrix(
                this.numSlackVariables+numEqualityConstraints,
                this.numSlackVariables+numSegments+1);
        final int rhsColumn = this.tableau.getColumnDimension() - 1;
        
        int slack = 0;
        for (Segment segment: pattern.getSegments()) {
            final int segmentColumn = this.numSlackVariables+segment.getID();
            
            final int lowLength = modelInst.getViewLength(segment.getLowBound().getRelativeView());
            final int lowBound = segment.getLowBound().evaluate(lowLength);
            this.lowBounds[segment.getID()] = lowBound;
            /* Low bound x >= B: -x + slack = -B */
            this.tableau.setEntry(slack, slack, 1);
            this.tableau.setEntry(slack, segmentColumn, -1);
            this.tableau.setEntry(slack, rhsColumn, -lowBound);
            slack++;
            
            final int highLength = modelInst.getViewLength(segment.getHighBound().getRelativeView());
            final int highBound = segment.getHighBound().evaluate(highLength);
            this.highBounds[segment.getID()] = highBound;
            /* High bound x <= B: x + slack = B */
            this.tableau.setEntry(slack, slack, 1);
            this.tableau.setEntry(slack, segmentColumn, 1);
            this.tableau.setEntry(slack, rhsColumn, highBound);
            slack++;
        }
        
        int eqConstraint = 0;
        for (int lcIndex=0; lcIndex<pattern.getLinearConstraints().size(); lcIndex++) {
            LinearConstraint lc = pattern.getLinearConstraints().get(lcIndex);
            final int row;
            final LinearConstraint.Relation relation = lc.getRelation();
            if (relation.isEquality) {
                row = this.numSlackVariables + eqConstraint;
                eqConstraint++;
            } else {
                row = slack;
                slack++;
                boolean lessThan = lc.getRelation() == LinearConstraint.Relation.LEQ;
                this.tableau.setEntry(row, row, lessThan ? 1 : -1);
            }
            
            int viewLength = modelInst.getViewLength(lc.getRHS().getRelativeView());
            final int rhs = lc.getRHS().evaluate(viewLength);
            this.tableau.setEntry(row, rhsColumn, rhs);
            for (int s=0; s<lc.getSegments().size(); s++) {
                Segment segment = lc.getSegments().get(s);
                this.tableau.setEntry(row,
                        this.numSlackVariables+segment.getID(), lc.getCoeff(s));
            }
        }
    }
    
    private void simplifyTableau() {
        /*
         * Row manipulations:
         * Slack, no real: drop
         * Slack, one real: keep ((convert to bound, drop))
         * Slack, two+ reals: keep
         * No slack, one real: keep ((convert to bounds, drop))
         * No slack, two+ reals: keep
         * No slack, no real, zero RHS: drop
         * No slack, no real, non-zero RHS: inconsistent system
         */
        
        final int rhsColumn = this.tableau.getColumnDimension() - 1;
        ArrayTools.rref(this.tableau.getDataRef());
        
        ArrayIntList includeRows = new ArrayIntList();
        ArrayIntList includeColumns = new ArrayIntList();
        for (int row=0; row<this.tableau.getRowDimension(); row++) {
            boolean hasReal = false;
            boolean nonzeroRHS = false;
            int rowSlack = -1;
            for (int col=0; col<this.tableau.getColumnDimension(); col++) {
                if (Math.abs(this.tableau.getEntry(row, col)) > 1e-6) {
                    if (col < this.numSlackVariables) {
                        rowSlack = col;
                    }
                    else if (col < rhsColumn) {
                        hasReal = true;
                    }
                    else nonzeroRHS = true;
                }
            }
            
            final boolean hasSlack = rowSlack >= 0;
            if (hasSlack) {
                if (hasReal) {
                    includeRows.add(row);
                    includeColumns.add(rowSlack);
                }
            } else {
                if (hasReal) {
                    includeRows.add(row);
                }
                else if (nonzeroRHS) {
                    throw new IllegalArgumentException("Linear constraints can not be satisfied");
                }
            }
        }
        
        /* Keep non-slack variables */
        for (int i=this.numSlackVariables; i<this.tableau.getColumnDimension(); i++) {
            includeColumns.add(i);
        }
        
        final int removeRows = this.tableau.getRowDimension() - includeRows.size();
        final int removeCols = this.tableau.getColumnDimension() - includeColumns.size();
        if (removeRows > 0 || removeCols > 0) {
            System.err.println(String.format(
                    "Removing %d redundant constraints and %d slack variables",
                    removeRows, removeCols));
            if (includeRows.isEmpty() || includeColumns.isEmpty()) {
                this.tableau = new Array2DRowRealMatrix();
            } else {
                this.tableau = (Array2DRowRealMatrix)this.tableau.getSubMatrix(includeRows.toArray(), includeColumns.toArray());
            }
            this.numSlackVariables -= removeCols;
        }
    }
    
    private void normalizeTableau() {
        final double EPSILON = 1e-5;
        for (int row=0; row<this.tableau.getRowDimension(); row++) {
            for (int col=0; col<this.tableau.getColumnDimension(); col++) {
                if (Math.abs(this.tableau.getEntry(row, col)) < EPSILON) {
                    this.tableau.setEntry(row, col, 0);
                }
            }
        }
    }

    public String formatTableau() {
        final String numberFormat = "%10.1f";
        final String headerFormat = "%10s";
        StringBuffer sb = new StringBuffer();
        
        for (int col=0; col<this.tableau.getColumnDimension(); col++) {
            final String content;
            if (col < this.numSlackVariables) {
                content = String.format("slack%d", col);
            } else if (col < this.tableau.getColumnDimension()-1) {
                final int segID = col-this.numSlackVariables;
                content = this.pattern.getSegments().get(segID).getName();
            } else {
                content = "RHS";
            }
            sb.append(String.format(headerFormat, content));
        }
        if (this.tableau.getColumnDimension() > 0) sb.append('\n');
        
        for (double[] row: this.tableau.getData()) {
            for (double item: row) {
                sb.append(String.format(numberFormat, item));
            }
            sb.append('\n');
        }
        
        if (sb.length() == 0) sb.append("<empty>");
        
        return sb.toString();
    }

    public final Pattern getPattern() {
        return this.pattern;
    }
    
    public final ModelInstance getModelInstance() {
        return this.modelInst;
    }
    
    public final Model getModel() {
        return this.modelInst.getModel();
    }
    
    public final int getNumSegments() {
        return this.pattern.getSegments().size();
    }
    
    public int getNumSlackVariables() {
        return this.numSlackVariables;
    }
    
    public final int getNumVariables() {
        return this.pattern.getSegments().size() + this.numSlackVariables;
    }
    
    public final int getNumConstraints() {
        return this.tableau.getRowDimension();
    }
    
    public final boolean isEqualityConstraint(int constraint) {
        return constraint >= this.numSlackVariables;
    }
    
    public final int getConstraintBySlack(int slackVariable) {
        if (slackVariable >= this.numSlackVariables) {
            throw new IllegalArgumentException("Invalid slack variable: "+slackVariable);
        }
        return slackVariable;
    }
    
    public final int getSlackVariable(int constraint) {
        if (constraint >= this.numSlackVariables) return -1;
        else return constraint;
    }
    
    public final double getSegmentCoeff(int constraint, Segment segment) {
        final int column = this.numSlackVariables + segment.getID();
        return this.tableau.getEntry(constraint, column);
    }
    
    public final double getSlackCoeff(int constraint, int slackVariable) {
        if (slackVariable >= this.numSlackVariables) {
            throw new IllegalArgumentException("Invalid slack variable: "+slackVariable);
        }
        return this.tableau.getEntry(constraint, slackVariable);
    }
    
    public final double[] getSegmentCoeffs(int constraint) {
        final int numSegments = getNumSegments();
        final double[] row = this.tableau.getRow(constraint);
        final double[] result = new double[numSegments];
        System.arraycopy(row, this.numSlackVariables, result, 0, numSegments);
        return result;
    }
    
    public final double[] getCoeffs(int constraint) {
        final double[] coeffs = new double[getNumVariables()];
        final double[] row = this.tableau.getRow(constraint);
        final int numSegments = getNumSegments();
        final int numSlack = getNumSlackVariables();
        System.arraycopy(row, numSlack, coeffs, 0, numSegments);
        System.arraycopy(row, 0, coeffs, numSegments, numSlack);
        return coeffs;
    }
    
    public final double getRHS(int constraint) {
        final int column = this.tableau.getColumnDimension() - 1;
        return this.tableau.getEntry(constraint, column);
    }

    public List<Segment> getConstraintSegments(int constraint) {
        List<Segment> segments = new ArrayList<Segment>();
        for (Segment segment: this.pattern.getSegments()) {
            if (Math.abs(getSegmentCoeff(constraint, segment)) > 1e-5) {
                segments.add(segment);
            }
        }
        return segments;
    }
    
    public final int getLowBound(Segment segment) {
        return this.lowBounds[segment.getID()];
    }

    public final int getLowBound(int variable) {
        if (variable >= getNumSegments()) return 0;
        else return this.lowBounds[variable];
    }
    
    public final int getHighBound(Segment segment) {
        return this.highBounds[segment.getID()];
    }
    
    public final int getHighBound(int variable) {
        if (variable >= getNumSegments()) {
            /* TODO: this should be cached */
            int max = 0;
            for (int bound: this.highBounds) {
                max = Math.max(max, bound);
            }
            return max;
        }
        else return this.highBounds[variable];
    }

    /**
     * Find the maximum magnitude for an elementary operation
     * that can be applied to current solution. The magnitude
     * consists of lower and upper limits for parameter alpha
     * so that solution+alpha*operation is non-negative.
     * 
     * @param solution Solution vector containing current variable
     *  values, including slack variables.
     * @param operation Elementary operation vector.
     * @return Two-component array containing the lower and
     *  upper bound for alpha.
     */
    public final double[] getOperationRange(double[] solution, double[] operation) {
        final double big = 1e15;
        double low = -big;
        double high = big;
        
        /*
         * When c is a coefficient in operation and x is the
         * corresponding value in solution,
         * - c = 0 => alpha in (-inf, inf)
         * - c > 0 => alpha in [-x/c, inf)
         * - c < 0 => alpha in (-inf, -x/c]
         */
        
        for (int i=0; i<solution.length; i++) {
            final double coeff = operation[i];
            if (Math.abs(coeff) < 1e-5) continue;
            
            final double limit = solution[i] / -coeff;
            if (coeff > 0) {
                if (limit > low) low = limit; 
            } else {
                if (limit < high) high = limit;
            }
        }
        
        return new double[] {low, high};
    }
    
    public final double[] getOperationMaxRange(double[] operation) {
        /* TODO: lower bound is buggy */
        final double big = 1e15;
        double low = -big;
        double high = big;

        for (int i=0; i<operation.length; i++) {
            final double coeff = operation[i];
            if (Math.abs(coeff) < 1e-5) continue;
            
            if (coeff > 0) {
                final double limit = getLowBound(i) / -coeff;
                if (limit > low) low = limit; 
            } else {
                final double limit = getHighBound(i) / -coeff;
                if (limit < high) high = limit;
            }
        }
        
        return new double[] {low, high};
    }
    
    private RealPointValuePair solveSimplex() throws OptimizationException {
        List<org.apache.commons.math.optimization.linear.LinearConstraint> lcs =
            new ArrayList<org.apache.commons.math.optimization.linear.LinearConstraint>();
        for (int constraint=0; constraint<getNumConstraints(); constraint++) {
            double rhs = getRHS(constraint);
            Relationship rel;
            if (isEqualityConstraint(constraint)) rel = Relationship.EQ;
            else rel = Relationship.LEQ;
            double[] coeffs = Arrays.copyOf(getSegmentCoeffs(constraint), getNumSegments());
            
            org.apache.commons.math.optimization.linear.LinearConstraint lc = 
                new org.apache.commons.math.optimization.linear.LinearConstraint(
                        coeffs, rel, rhs);
            lcs.add(lc);
        }
        
        double[] coeffs = new double[getNumSegments()];
        for (int i=0; i<coeffs.length; i++) coeffs[i] = 1;
        
        /* Simplex solver sometimes crashes when RHS of objective is 0 */
        LinearObjectiveFunction objective = new LinearObjectiveFunction(coeffs, 1);
        SimplexSolver solver = new SimplexSolver();
        return solver.optimize(objective, lcs, GoalType.MINIMIZE, true);
    }
    
    public double[] getInitialSolution() throws NoSolutionException {
        RealPointValuePair rawSolution;
        try {
            rawSolution = solveSimplex();
        } catch (NoFeasibleSolutionException e) {
            throw new NoSolutionException(e.getMessage());
        } catch (OptimizationException e) {
            throw new IllegalArgumentException(
                "Could not find initial segment length assignment: "+e.getMessage());
        }
        
        double[] solution = new double[getNumVariables()];
        for (int i=0; i<getNumSegments(); i++) {
            solution[i] = rawSolution.getPointRef()[i];
        }
        
        for (int slack=0; slack<getNumSlackVariables(); slack++) {
            int constraint = getConstraintBySlack(slack);
            double value = getRHS(constraint);
            for (Segment segment: getPattern().getSegments()) {
                value -= solution[segment.getID()] * getSegmentCoeff(constraint, segment);
            }
            value /= getSlackCoeff(constraint, slack);
            if (value < 0) {
                throw new NoSolutionException(String.format("Slack variable %d is negative: %.2f", slack, value));
            }
            solution[getNumSegments()+slack] = value;
        }
        
        return solution;
    }
    
    public double[][] getElementaryOperations() {
        RealMatrix preMatrix = this.tableau.getSubMatrix(
                0, this.tableau.getRowDimension()-1,
                0, this.tableau.getColumnDimension()-2);
        /* Make a square matrix for SVD */
        final int extraRows = Math.max(0, getNumVariables()-getNumConstraints());
        RealMatrix matrix = new Array2DRowRealMatrix(getNumConstraints()+extraRows,
                getNumVariables());
        matrix.setSubMatrix(preMatrix.getData(), 0, 0);
        
        SingularValueDecomposition svd = new SingularValueDecompositionImpl(matrix);
        /* Compute rank by hand, using a custom epsilon */
        int rank = 0;
        for (double singular: svd.getSingularValues()) {
            if (Math.abs(singular) > 1e-5) rank++;
        }
        final int nullity = matrix.getColumnDimension() - rank;
        
        double[][] operations = new double[nullity][];
        for (int i=0; i<nullity; i++) {
            double[] base = svd.getV().getColumn(rank+i);
            double[] op = new double[getNumVariables()];
            System.arraycopy(base, getNumSlackVariables(), op, 0, getNumSegments());
            System.arraycopy(base, 0, op, getNumSegments(), getNumSlackVariables());
            operations[i] = op;
        }
        ArrayTools.rref(operations);
        for (double[] op: operations) {
            for (int i=0; i<op.length; i++) {
                if (Math.abs(op[i]) < 1e-8) op[i] = 0;
                if (Double.isNaN(op[i])) {
                    throw new RuntimeException(
                            "Could not find elementary operations (NaN produced)");
                }
            }
        }
        return operations;
    }
    
    public void validateSolution(double[] solution) {
        final double EPSILON = 1e-3;
        
        final int firstSlack = getNumSegments();
        for (int i=firstSlack; i<firstSlack+getNumSlackVariables(); i++) {
            if (solution[i] < -EPSILON) {
                String msg = String.format("Invalid solution: slack variable %d is negative: %.2f",
                        i-firstSlack, solution[i]);
                throw new IllegalArgumentException(msg);
            }
        }
        
        for (int row=0; row<this.tableau.getRowDimension(); row++) {
            RealVector v = new ArrayRealVector(getCoeffs(row));
            double dot = v.dotProduct(solution);
            double rhs = getRHS(row);
            double diff = Math.abs(dot - rhs);
            if (diff > EPSILON) {
                String msg = String.format("Solution violates constraint %d (difference: %.5f): %s",
                        row, diff, ArrayTools.arrayToString(solution));
                throw new IllegalArgumentException(msg);
            }
        }
    }
}
