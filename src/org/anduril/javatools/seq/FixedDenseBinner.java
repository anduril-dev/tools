package org.anduril.javatools.seq;

import java.util.Arrays;

public final class FixedDenseBinner {
    private final int binSize;
    private final double[][] levels;
    private final int[] numBins;
    private final int[] tailLengths;
    private String name;
    
    public FixedDenseBinner(int binSize, ChromosomeSet chrSet) {
        if (binSize < 1) {
            throw new IllegalArgumentException("binSize must be at least 1");
        }
        
        this.binSize = binSize;
        
        final int N = chrSet.getNumChromosomes();
        this.levels = new double[N][];
        
        this.numBins = new int[N];
        for (int i=0; i<N; i++) {
            /* There are chrLen/this.binSize regular
             * fixed size bins and one variable size
             * tail bin, which may be empty. */
            final int chrLen = chrSet.getLength(i);
            this.numBins[i] = (chrLen / this.binSize) + 1;
        }
        
        this.tailLengths = new int[N];
        for (int i=0; i<N; i++) {
            final int chrLen = chrSet.getLength(i);
            this.tailLengths[i] = chrLen % this.binSize;
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getNumChromosomes() {
        return this.levels.length;
    }
    
    public int getBinSize() {
        return this.binSize;
    }
    
    public int getNumBins(int chromosomeID) {
        try {
            return this.numBins[chromosomeID];
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Chromosome does not exist: "+chromosomeID);
        }
    }
    
    public int getBinIndex(int chromosomeID, int position) {
        if (position < 0) return 0;
        int index = position / this.binSize;
        final int tailBin = getNumBins(chromosomeID) - 1;
        return (index <= tailBin) ? index : tailBin;
    }
    
    public int getStart(int chromosomeID, int binIndex) {
        if (binIndex < getNumBins(chromosomeID)) {
            return binIndex*this.binSize;
        } else {
            throw new IllegalArgumentException(
                    String.format("No bin %d for chromosome %d",
                            binIndex, chromosomeID));
        }
    }

    public int getEnd(int chromosomeID, int binIndex) {
        final int tailBin = getNumBins(chromosomeID) - 1;
        if (binIndex < tailBin) {
            return (binIndex+1)*this.binSize;
        } else if (binIndex == tailBin) {
            final int start = binIndex*this.binSize;
            return start+this.tailLengths[chromosomeID];
        } else {
            throw new IllegalArgumentException(
                    String.format("No bin %d for chromosome %d",
                            binIndex, chromosomeID));
        }
    }

    public int getSize(int chromosomeID, int binIndex) {
        final int tailBin = getNumBins(chromosomeID) - 1;
        if (binIndex < tailBin) {
            return this.binSize;
        } else if (binIndex == tailBin) {
            return this.tailLengths[chromosomeID];
        } else {
            throw new IllegalArgumentException(
                    String.format("No bin %d for chromosome %d",
                            binIndex, chromosomeID));
        }
    }
    
    public double getLevel(int chromosomeID, int binIndex) {
        final double[] chrLevels;
        try {
            chrLevels = this.levels[chromosomeID];
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Chromosome does not exist: "+chromosomeID);
        }
        if (chrLevels == null) return 0;
        
        try {
            return chrLevels[binIndex];
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException(
                    String.format("No bin %d for chromosome %d",
                            binIndex, chromosomeID));
        }
    }
    
    public double[] getLevels(int chromosomeID) {
        double[] chrLevels;
        try {
            chrLevels = this.levels[chromosomeID];
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Chromosome does not exist: "+chromosomeID);
        }

        if (chrLevels == null) {
            chrLevels = new double[getNumBins(chromosomeID)];
            this.levels[chromosomeID] = chrLevels;
        }
        return chrLevels;
    }
    
    public boolean hasLevels(int chromosomeID) {
        try {
            return this.levels[chromosomeID] != null;
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Chromosome does not exist: "+chromosomeID);
        }
    }
    
    public void addRegion(final DNARegion region) {
        final int chromosomeID = region.chromosome;
        
        final int tailBin = getNumBins(chromosomeID) - 1;
        final double[] levels = getLevels(chromosomeID);
        
        int binIndex = getBinIndex(chromosomeID, region.start);
        int binStart = getStart(chromosomeID, binIndex);
        int binEnd = getEnd(chromosomeID, binIndex);

        if (binIndex == tailBin && region.end > binEnd) {
            /* Region overflows the tail bin, so increase
             * the length of the tail bin. */
            this.tailLengths[chromosomeID] = region.end - region.start;
            binEnd = region.end;
        }
        
        while (region.end > binStart) {
            int overlap = binEnd - binStart;
            if (region.start > binStart) overlap -= (region.start - binStart);
            if (region.end < binEnd) overlap -= (binEnd - region.end);
            levels[binIndex] += overlap;
            
            binIndex++;
            binStart = binEnd;
            if (binIndex == tailBin) {
                binEnd = getEnd(chromosomeID, binIndex);
                if (region.end > binEnd) {
                    /* Region overflows the tail bin */
                    this.tailLengths[chromosomeID] = region.end - region.start;
                    binEnd = region.end;
                }
            } else {
                binEnd += this.binSize;
            }
        }
    }
    
    public void setZero() {
        for (double[] levels: this.levels) {
            if (levels == null) continue;
            Arrays.fill(levels, 0.0);
        }
    }
    
    public void multiply(double factor) {
        for (double[] chrLevels: this.levels) {
            if (chrLevels == null) continue;
            for (int i=0; i<chrLevels.length; i++) {
                chrLevels[i] *= factor;
            }
        }
    }
    
    public double normalize(double outputSum) {
        double sum = 0;
        for (double[] chrLevels: this.levels) {
            if (chrLevels == null) continue;
            for (double value: chrLevels) {
                sum += value;
            }
        }
        if (sum != 0) {
            double factor = outputSum / sum;
            multiply(factor);
            return factor;
        } else {
            return 1.0;
        }
    }
    
    public void add(FixedDenseBinner other, final double otherFactor) {
        if (this.getBinSize() != other.getBinSize()) {
            throw new IllegalArgumentException("Binners must have equal bin size");
        }
        if (otherFactor == 0) return;
        
        for (int chrID=0; chrID<getNumChromosomes(); chrID++) {
            if (!hasLevels(chrID) || !other.hasLevels(chrID)) continue;
            final double[] thisLevels = getLevels(chrID); 
            final double[] otherLevels = other.getLevels(chrID);
            for (int i=0; i<thisLevels.length; i++) {
                thisLevels[i] = Math.max(0, thisLevels[i] + otherLevels[i]*otherFactor);
            }
        }
    }
    
    public void multiply(FixedDenseBinner other, final boolean divide) {
        if (this.getBinSize() != other.getBinSize()) {
            throw new IllegalArgumentException("Binners must have equal bin size");
        }
        
        for (int chrID=0; chrID<getNumChromosomes(); chrID++) {
            if (!hasLevels(chrID) || !other.hasLevels(chrID)) continue;
            final double[] thisLevels = getLevels(chrID); 
            final double[] otherLevels = other.getLevels(chrID);
            for (int i=0; i<thisLevels.length; i++) {
                if (divide) {
                    if (otherLevels[i] != 0) {
                        thisLevels[i] /= otherLevels[i];
                    }
                } else {
                    thisLevels[i] *= otherLevels[i];
                }
            }
        }
    }
    
    public double getSum(int chromosomeID, int startBin, int endBin) {
        if (!hasLevels(chromosomeID)) return 0;
        
        final double[] levels = getLevels(chromosomeID);
        double sum = 0;
        for (int bin=startBin; bin<endBin; bin++) {
            sum += levels[bin];
        }
        return sum;
    }
    
    public double getMaximum() {
        double max = 0;
        for (double[] levels: this.levels) {
            if (levels == null) continue;
            for (double level: levels) {
                max = Math.max(max, level);
            }
        }
        return max;
    }
}
