package org.anduril.javatools.seq;

public class DNARegion implements Comparable<DNARegion> {
    public static final int SEQ_NONE = 127;
    
    public final short chromosome;
    public final int start;
    public final int end;
    public final byte strand;
    
    public DNARegion(int chromosome, int start, int end, int strand) {
        if (start > end) {
            throw new IllegalArgumentException("start must be <= end");
        }
        if (strand < -1 || strand > 1) {
            throw new IllegalArgumentException("strand must be 1 (pos), -1 (neg) or 0 (unknown)");
        }
        
        this.chromosome = (short)chromosome;
        this.start = start;
        this.end = end;
        this.strand = (byte)strand;
    }
    
    public DNARegion(int chromosome, int start, int end) {
        this(chromosome, start, end, 0);
    }
    
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof DNARegion)) return false;
        DNARegion oth = (DNARegion)other;
        
        return (this.start == oth.start)
            && (this.end == oth.end)
            && (this.chromosome == oth.chromosome)
            && (this.strand == oth.strand);
    }
    
    @Override
    public int hashCode() {
        return this.chromosome ^ this.start ^ this.end ^ this.strand;
    }
    
    @Override
    public int compareTo(DNARegion other) {
        if (this.chromosome < other.chromosome) return -1;
        else if (this.chromosome > other.chromosome) return 1;
        
        if (this.start < other.start) return -1;
        else if (this.start > other.start) return 1;
        
        if (this.end < other.end) return -1;
        else if (this.end > other.end) return 1;
        
        return 0;
    }
    
    @Override
    public String toString() {
        return String.format("%d:%d-%d", this.chromosome, this.start, this.end);
    }
    
    public boolean contains(int chromosome, int location) {
        if (this.chromosome != chromosome) return false;
        return location >= this.start && location < this.end;
    }

    public boolean contains(int chromosome, int start, int end) {
        if (this.chromosome != chromosome) return false;
        return start >= this.start && end <= this.end;
    }
 
    public int length() {
        return this.end - this.start;
    }
}
