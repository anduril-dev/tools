package org.anduril.javatools.seq;

/**
 * Transform genomic coordinates from one coordinate
 * system to another. Locus x is transformed using
 * y = offset + scale*x. Here, y may refer to a global
 * genomic location and x to local gene-level position,
 * for example; scale is negative for the reverse strand.
 * Transformations can be combined (arbitrarily many times)
 * so that T1(T2(x)) first transforms x using T2 and then
 * using T1.
 */
public class LocusTransformation {
    private final int offset;
    private final int scale;
    
    public LocusTransformation(int offset, int scale) {
        if (scale == 0) {
            throw new IllegalArgumentException("Scale must not be 0");
        }
        this.offset = offset;
        this.scale = scale;
    }
    
    @Override
    public String toString() {
        return String.format("<offset %d, scale %d>", this.offset, this.scale);
    }
    
    public final int getOffset() {
        return this.offset;
    }
    
    public final int getScale() {
        return this.scale;
    }
    
    public final int transform(int locus) {
        return this.offset + this.scale*locus;
    }
    
    public final int reverseTransform(int locus) {
        return (locus - this.offset) / this.scale;
    }

    /**
     * Create a composite transformation T1(T2(x)),
     * where T1 is the current transformation and T2 is
     * the parameter.
     * @param other The inner-most transformation (T2).
     */
    public LocusTransformation composite(LocusTransformation other) {
        /*
         * The formulas can be derived using 2x2 matrices.
         * Transformation matrix for scale factor S and
         * offset L is M = [[S, 0], [L, 1]], where [x, y]
         * is a column vector. Coordinates are defined as
         * column vectors [x, 1]. The composite transformation
         * is M1 * M2 = [[S1*S2, 0], [S1*L2+L1, 1]].
         */
        int newScale = this.scale*other.scale;
        int newOffset = this.scale*other.offset + this.offset;
        return new LocusTransformation(newScale, newOffset);
    }
}
