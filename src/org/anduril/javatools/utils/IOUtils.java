package org.anduril.javatools.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

public class IOUtils {
    public static InputStream openGZIPFile(File file) throws FileNotFoundException {
        InputStream fis = null;
        try {
            fis = new FileInputStream(file);
            return new GZIPInputStream(fis);
        } catch (IOException e) {
            try {
                if (fis != null) fis.close();
            } catch (IOException e2) {}
            return new FileInputStream(file);
        }
    }

    public static BufferedReader openGZIPFileReader(File file) throws FileNotFoundException {
        InputStream fis = null;
        try {
            fis = new FileInputStream(file);
            return new BufferedReader(new InputStreamReader(new GZIPInputStream(fis)));
        } catch (IOException e) {
            try {
                if (fis != null) fis.close();
            } catch (IOException e2) {}
            return new BufferedReader(new FileReader(file));
        }
    }
    
}
