package org.anduril.javatools.utils;

import java.util.Iterator;
import java.util.List;

/**
 * Base class for making iterators conveniently.
 * This class implements {@link java.util.Iterator} and {@link java.lang.Iterarable}
 * methods. Subclasses must provide functionality to
 * produce the next item, which is cached by this base
 * class and returned in {@link AbstractIterator#next()}
 * when needed. Subclasses should override
 * {@link AbstractIterator#iterator()} if they need
 * to instantiate new iterators; the default implementation
 * returns the current instance.
 */
public abstract class AbstractIterator<T> implements Iterator<T>, Iterable<T> {
    private T nextItem;

    @Override
    public final boolean hasNext() {
        if (this.nextItem == null) {
            this.nextItem = getNext();
        }
        return this.nextItem != null;
    }

    @Override
    public final T next() {
        if (this.nextItem == null) {
            this.nextItem = getNext();
        }
        final T item = this.nextItem;
        this.nextItem = null;
        return item;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Remove is not supported");
    }

    @Override
    public Iterator<T> iterator() {
        return this;
    }
    
    public void populateList(List<T> list) {
        for (T obj: this) list.add(obj);
    }
    
    /**
     * Produce the next item, or return null if there are
     * no more items.
     */
    protected abstract T getNext();
}
