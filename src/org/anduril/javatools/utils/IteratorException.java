package org.anduril.javatools.utils;

public class IteratorException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public IteratorException(String message, Throwable cause) {
        super(message, cause);
    }
}
