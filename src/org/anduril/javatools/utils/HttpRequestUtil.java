package org.anduril.javatools.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;

public class HttpRequestUtil {
	public static InputStream sendPostRequest(String address,
			Map<String, String> params) throws IOException,
			MalformedURLException {

		try {
			// Construct data
			StringBuffer data = new StringBuffer();
			for (String key : params.keySet()) {
				data.append(URLEncoder.encode(key, "UTF-8"));
				data.append("=");
				data.append(URLEncoder.encode(params.get(key), "UTF-8"));
				data.append("&");

			}
			data.deleteCharAt(data.length() - 1);

			// Send data
			URL url = new URL(address);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(conn
					.getOutputStream());
			wr.write(data.toString());
			wr.flush();

			// Get the response
			return conn.getInputStream();

		} catch (UnsupportedEncodingException e) {
			System.err.println("Encoding UTF-8 not supported. ");
			e.printStackTrace();
		}

		return null;
	}

	public static InputStream sendHttpRequest(String address)
			throws IOException, MalformedURLException {


			URL url = new URL(address);
			URLConnection conn = url.openConnection();

			return conn.getInputStream();
	}

	public static InputStream sendGetRequest(String address,
			Map<String, String> params) throws IOException,
			MalformedURLException {

		try {
			// Construct data
			StringBuffer data = new StringBuffer();
			data.append(address);
			data.append("?");

			for (String key : params.keySet()) {
				data.append(URLEncoder.encode(key, "UTF-8"));
				data.append("=");
				data.append(URLEncoder.encode(params.get(key), "UTF-8"));
				data.append("&");

			}
			data.deleteCharAt(data.length() - 1);

			// Send data
			URL url = new URL(data.toString());
			URLConnection conn = url.openConnection();

			// Get the response
			return conn.getInputStream();

		} catch (UnsupportedEncodingException e) {
			System.err.println("Encoding UTF-8 not supported. ");
			e.printStackTrace();
		}

		return null;
	}

}
