package org.anduril.javatools.utils;

import java.util.Collection;
import java.util.LinkedList;

import org.apache.commons.collections.primitives.ArrayDoubleList;
import org.apache.commons.collections.primitives.ArrayIntList;

public class TextUtils {
    public static int[] parseIntList(String intList, String separator) {
        if (intList == null) return null;
        
        ArrayIntList list = new ArrayIntList();
        for (String token: intList.split(separator)) {
            token = token.trim();
            if (token.isEmpty()) continue;
            list.add(Integer.parseInt(token));
        }
        return list.toArray();
    }
    
    public static double[] parseDoubleList(String doubleList) {
        return parseDoubleList(doubleList, ",");
    }

    public static double[] parseDoubleList(String doubleList, String separator) {
        if (doubleList == null) return null;
        
        ArrayDoubleList list = new ArrayDoubleList();
        for (String token: doubleList.split(separator)) {
            token = token.trim();
            if (token.isEmpty()) continue;
            list.add(Double.parseDouble(token));
        }
        return list.toArray();
    }
    
    public static int[] parseIntList(String intList) {
        return parseIntList(intList, ",");
    }

    public	static	String[]	filterStringCollection(Collection<String>	collection,String	regexp,boolean	negate)
    {
    	LinkedList<String>	filteredList=new	LinkedList<String>();
    	
    	for(String	item:collection)
    	{
    		if((negate)^(item.matches(regexp)))
    			filteredList.add(item);
    	}
    	
    	return	filteredList.toArray(new	String[filteredList.size()]);
    }
    
    public	static	String[]	filterStringCollection(Collection<String>	collection,String	regexp)
    {
    	return	filterStringCollection(collection,regexp,false);
    }
    
    public	static boolean	collectionContains(Collection<String>	collection,String	regexp)
    {
    	for(String	item:collection)
    	{
    		if(item.matches(regexp))
    			return	true;
    	}
    	return	false;
    }
}
