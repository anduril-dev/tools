package org.anduril.javatools.weka;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.AbstractLoader;
import weka.core.converters.ArffLoader;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 * Class for reading files for Weka-format
 * 
 * @author sirkku.karinen@helsinki.fi
 * 
 * */
public class WekaDataUtils {

	/**
	 * @param filename csv file name
	 * */
	public static Instances readCSV(File filename) throws IOException , Exception{
		return readData(filename, null, "CSV");
	}
	
	/**
	 * @param filename csv file name
	 * @param classAttribute column name for the class-variable
	 * */
	public static Instances readCSV(File filename, String classAttribute) throws IOException , Exception{
		return readData(filename, classAttribute, "CSV");
	}
	/**
	 * @param filename arff (weka format) file name
	 * @param classAttribute column name for the class-variable
	 * */
	public static Instances readArff(File filename) throws IOException , Exception{
		return readData(filename, null, "arff");
	}

	/**
	 * @param filename arff (weka format) file name
	 * @param classAttribute column name for the class-variable
	 * */
	public static Instances readArff(File filename, String classAttribute) throws IOException , Exception{
		return readData(filename, classAttribute, "arff");
	}

	/**
	 * @param filename data file name
	 * @param classAttrName column name for the class-variable (null if no class variable)
	 * @param fileType arff/csv
	 * */
	public static Instances readData(File filename,
			String classAttrName,
			String fileType) throws IOException , Exception{

		AbstractLoader inputLoader;

		if (fileType.equalsIgnoreCase("CSV"))
			inputLoader = new CSVLoader();
		else if(fileType.equalsIgnoreCase("arff"))
			inputLoader = new ArffLoader();
		else
			throw new IllegalArgumentException("Invalid file type: "+fileType);

		inputLoader.setSource(filename);
		Instances data = inputLoader.getDataSet();
		
		if(classAttrName != null){
			Attribute attr = data.attribute(classAttrName);

			if (attr == null)
				throw new IllegalArgumentException("Invalid class column name: "+classAttrName);
			data.setClass(data.attribute(classAttrName));
		}

		return data;
	}
	/**
	 * Method for removing columns from Weka-dataset
	 * 
	 * @param data dataset
	 * @param removeCols list of columns to be removed
	 * */
	public static Instances removeColumns(Instances data, String [] removeCols) throws Exception{

		// Convert given attribute names into a Weka Range String
		StringBuffer rangeString = new StringBuffer();
		
		for (String s : removeCols) {
			if(!s.equals("")){
				if (data.attribute(s.trim()) != null) {
					if (rangeString.length() > 0)
						rangeString.append(",");
					rangeString.append(String.valueOf(data.attribute(s.trim()).index() + 1));
				} else {
					throw new IllegalArgumentException("Invalid attribute to remove: "+s);
				}
			}
		}

		// Initialize remove filter
		Remove removeFilter = new Remove();
		removeFilter.setAttributeIndices(rangeString.toString());
		// Do removing
		removeFilter.setInputFormat(data);
		
		return Filter.useFilter(data, removeFilter);
	}
    /**
     * Reads the property line from given CSV file.
     * @author Viljami Aittomaki
     * @param csvFile CSV <code>File</code> to read
     * @return property line from CSV as a <code>String</code> or
     *         <code>null</code> if line is not present
     * @throws Exception in case of an error reading the CSV file
     */
    public static String readPropertyLine(File csvFile) throws Exception {
        // Open file
        BufferedReader reader = new BufferedReader(new FileReader(csvFile));

        // Find first non-whitespace line
        String ln;
        do {
            ln = reader.readLine();
        } while (ln.matches("\\s*"));

        // First non-empty line should contain properties so we can close reader
        reader.close();

        // If property line present, return it, otherwise NULL
        if (ln.trim().startsWith("#")) {
            return ln;
        } else {
            return null;
        }
    }

    /**
     * Converts a pipeline CSV file to a CSV file suitable for Weka.
     * 
     * @author Viljami Aittomaki
     * @param pipelineCSV Pipeline CSV <code>File</code> to convert
     * @param wekaCSV <code>File</code> where Weka CSV will be written
     * @return property line from CSV as a <code>String</code> or
     *         <code>null</code> if there is no property line
     * @throws Exception in case of IO errors
     */
    public static String andurilToWeka(File pipelineCSV, File wekaCSV)
            throws Exception {
        // Open files
        BufferedReader reader = new BufferedReader(new FileReader(pipelineCSV));
        BufferedWriter writer = new BufferedWriter(new FileWriter(wekaCSV));

        // Find first non-whitespace line and skip property line
        String ln;
        do {
            ln = reader.readLine();
        } while (ln.matches("\\s*"));

        // Catch property line if present
        String propertyLine = null;
        if (ln.trim().startsWith("#")) {
            propertyLine = ln;
            ln = reader.readLine();
        }

        // Replace NA's with ?, write to output, read all remaining lines
        Pattern p = Pattern.compile("(?<=^|\t)\"?NA\"?(?=\t|$)");
        Matcher m;
        while (ln != null) {
            m = p.matcher(ln);
            writer.write(m.replaceAll("?"));
            writer.newLine();
            ln = reader.readLine();
        }

        // Close streams
        reader.close();
        writer.close();

        return propertyLine;
    }

    /**
     * Converts a CSV file in Weka format to a proper pipeline CSV. To be exact,
     * it replaces question marks (?) with NA's and allows writing of a property
     * line to the beginning of the CSV file.
     * 
     * @author Viljami Aittomaki
     * @param wekaCSV CSV <code>File</code> in Weka format to convert
     * @param pipelineCSV <code>File</code> where pipeline CSV will be written
     * @param properties property line <code>String</code> to write into
     *        pipeline CSV. Use <code>null</code> for no property line.
     * @throws Exception in case of IO errors
     */
    public static void wekaToAnduril(File wekaCSV, File pipelineCSV,
            String properties) throws Exception {
        // Open files
        BufferedReader reader = new BufferedReader(new FileReader(wekaCSV));
        BufferedWriter writer = new BufferedWriter(new FileWriter(pipelineCSV));

        // Write property line if given
        if (properties != null) {
            writer.write(properties);
            writer.newLine();
        }

        // Read all Weka CSV lines, replace ?'s with NA's, write to output
        Pattern p = Pattern.compile("(?<=^|\t)\"?\\?\"?(?=\t|$)");
        Matcher m;
        String ln = reader.readLine();
        while (ln != null) {
            m = p.matcher(ln);
            writer.write(m.replaceAll("NA"));
            writer.newLine();
            ln = reader.readLine();
        }

        // Close streams
        reader.close();
        writer.close();
    }

    /**
     * Converts a CSV file in Weka format to a proper pipeline CSV. Calls
     * {@link #wekaToPipeline(File, File, String)} with <code>null</code> as
     * the property string.
     * 
     * @author Viljami Aittomaki
     * @param wekaCSV <code>File</code> in Weka format to convert
     * @param pipelineCSV <code>File</code> where pipeline CSV will be written
     * @throws Exception in case of IO errors
     */
    public static void wekaToAnduril(File wekaCSV, File pipelineCSV)
            throws Exception {
        WekaDataUtils.wekaToAnduril(wekaCSV, pipelineCSV, null);
    }
	
}
