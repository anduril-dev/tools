package org.anduril.javatools.petrinet;

import java.util.Collection;
import java.util.Collections;

public abstract class PNNode extends PNObject implements Comparable<PNNode> {
    public static final NodeType DEFAULT_TYPE = NodeType.DISCRETE;
    
    private final int index;
    private NodeType type;
    
    public PNNode(PetriNet net, String id, int index) {
        super(net, id);
        this.index = index;
        this.type = DEFAULT_TYPE;
    }
    
    @Override
    public int compareTo(PNNode other) {
        return this.getIndex() - other.getIndex();
    }
    
    public int getIndex() {
        return this.index;
    }
    
    public NodeType getType() {
        return this.type;
    }
    
    public void setType(NodeType type) {
        this.type = type;
    }
    
    public void addPreArc(Arc arc) {
        if (arc.getTo() != this) {
            throw new IllegalArgumentException(String.format(
                    "Can not add pre-arc %s to %s: invalid target (%s)",
                    arc.getID(), getID(), arc.getTo().getID()));
        }
        getPetriNet().getGraph().addEdge(arc, arc.getFrom(), arc.getTo());
    }
    
    public void addPostArc(Arc arc) {
        if (arc.getFrom() != this) {
            throw new IllegalArgumentException(String.format(
                    "Can not add post-arc %s to %s: invalid source (%s)",
                    arc.getID(), getID(), arc.getFrom().getID()));
        }
        getPetriNet().getGraph().addEdge(arc, arc.getFrom(), arc.getTo());
    }
    
    public Collection<Arc> getPreArcs() {
        Collection<Arc> edges = getPetriNet().getGraph().getInEdges(this);
        if (edges == null) return Collections.emptyList();
        else return edges;
    }
    
    public Collection<Arc> getPostArcs() {
        Collection<Arc> edges = getPetriNet().getGraph().getOutEdges(this);
        if (edges == null) return Collections.emptyList();
        else return edges;
    }
    
}
