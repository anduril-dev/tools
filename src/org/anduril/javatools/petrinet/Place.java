package org.anduril.javatools.petrinet;

public abstract class Place extends PNNode {
    public static final double DEFAULT_CAPACITY = 2e9;
    
    private double initialMarking;
    private double capacity;
    
    public Place(PetriNet net, String id, int index) {
        super(net, id, index);
        this.initialMarking = 0;
        this.capacity = DEFAULT_CAPACITY;
    }
    
    @Override
    public String toString() {
        return String.format("Place %s (%d, %s): %s, init %.1f, capacity %.2g",
                getID(), getIndex(), getName(), getType(), getInitialMarking(), this.capacity);
    }
    
    public double getInitialMarking() {
        return this.initialMarking;
    }
    
    public void setInitialMarking(double marking) {
        this.initialMarking = marking;
    }
    
    public double getCapacity() {
        return this.capacity;
    }
    
    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }
    
    public boolean hasFiniteCapacity() {
        return this.capacity < DEFAULT_CAPACITY;
    }
}
