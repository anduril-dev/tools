package org.anduril.javatools.petrinet;

public class Arc extends PNObject {
    public static final double DEFAULT_WEIGHT = 1.0;
    
    private final PNNode from;
    private final PNNode to;
    private double weight;
    
    public Arc(PetriNet net, String id, PNNode from, PNNode to) {
        super(net, id);
        this.from = from;
        this.to = to;
        this.weight = DEFAULT_WEIGHT;
        from.addPostArc(this);
        to.addPreArc(this);
    }

    @Override
    public String toString() {
        return String.format("Arc %s (%s): %s -> %s, weight %.1f",
                getID(), getName(), this.from.getID(), this.to.getID(), this.weight);
    }

    public PNNode getFrom() {
        return this.from;
    }
    
    public PNNode getTo() {
        return this.to;
    }
    
    public double getWeight() {
        return this.weight;
    }
    
    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    public boolean hasMixedTyped() {
        if (this.from == null || this.to == null) return false;
        if (this.from.getType() == null || this.to.getType() == null) return false;
        return this.from.getType() != this.to.getType();
    }
}
