package org.anduril.javatools.petrinet.test;

import org.junit.Before;

import org.anduril.javatools.petrinet.Arc;
import org.anduril.javatools.petrinet.DPlace;
import org.anduril.javatools.petrinet.DTransition;
import org.anduril.javatools.petrinet.EventGenerator;
import org.anduril.javatools.petrinet.GeneratorType;
import org.anduril.javatools.petrinet.PetriNet;
import org.anduril.javatools.petrinet.Place;
import org.anduril.javatools.petrinet.Transition;
import org.anduril.javatools.petrinet.codegen.CodeGenerator;
import org.anduril.javatools.petrinet.codegen.Compiler;
import org.anduril.javatools.petrinet.parser.PetriNetFinalizer;
import org.anduril.javatools.petrinet.runtime.Simulator;
import org.anduril.javatools.petrinet.runtime.WriterEventSource;
import org.anduril.javatools.seq.test.Utils;

/**
 * Systematically test Petri nets that are below a certain
 * size. Test cases are encoded using number of places (P),
 * number of transitions (T) and number of arcs (A).
 * Variants within a P/T/A group are encoded with the help
 * of a matrix. In the following example with P=2, T=2 and
 * A=4, the matrix indicates whether there is an arc from
 * place to transition (P), transition to place (T), or
 * both (PT).
 * <pre>
 *    t1 t2
 * p1 PT P  
 * p2    P 
 * </pre>
 * This matrix is abbreviated PT_P_n_P by reading row-wise
 * (n marks empty spot).
 */
public abstract class GeneratedModelTest {
    protected PetriNet net;
    private int numIntervals;
    
    public GeneratedModelTest(int numIntervals) {
        this.numIntervals = numIntervals;
    }
    
    public int getNumIntervals() {
        return this.numIntervals;
    }
    
    public void setNumIntervals(int numIntervals) {
        this.numIntervals = numIntervals;
    }
    
    @Before
    public void setUp() {
        this.net = new PetriNet();
    }

    public void run(double[][] expectedMarking) throws Exception {
        new PetriNetFinalizer(this.net).runFinalizer();
        
        final int numPlaces = this.net.getPlaces().size();
        String message = String.format("P=%d T=%d A=%d",
                numPlaces, this.net.getTransitions().size(), this.net.getArcs().size());
        if (numPlaces > 0) {
            message += " m=";
            int index = 0;
            for (Place place: this.net.getSortedPlaces()) {
                message += String.format("%s%.1f", index > 0 ? "," : "", place.getInitialMarking());
                index++;
            }
        }
        
        final String packageName = "pkg";
        final String className = "GeneratedSimulator";
        String code = new CodeGenerator(net).generate(packageName, className);
        Simulator sim = new Compiler().compile(code, packageName, className, null);
        sim.initialize(this.net);
        WriterEventSource reporter = new WriterEventSource(0, this.numIntervals-1, this.numIntervals, sim);
        sim.simulate(reporter);
        Utils.assertArrayEquals(message, expectedMarking, reporter.getMarkingSums(), 1e-9);
    }

    public void addPlace(String placeID, double initialMarking) {
        DPlace place = new DPlace(this.net, placeID, this.net.getPlaces().size());
        place.setInitialMarking(initialMarking);
        this.net.addPlace(place);
    }
    
    public void addTransition(String transitionID) {
        DTransition trans = new DTransition(this.net, transitionID, this.net.getTransitions().size());
        this.net.addTransition(trans);
    }
    
    public void addArc(String fromID, String toID) {
        String arcID = String.format("%s -> %s", fromID, toID);
        Arc arc = new Arc(this.net, arcID, this.net.getNode(fromID), this.net.getNode(toID));
        this.net.addArc(arc);
    }
    
    public void setMarking(String placeID, double marking) {
        this.net.getPlace(placeID).setInitialMarking(marking);
    }
    
    public void setGeneratorType(GeneratorType type) {
        this.net.getGenerators().clear();
        EventGenerator gen = new EventGenerator(this.net, "gen-"+type,
                0, this.net.getTransitions().size());
        gen.setGeneratorType(type);
        this.net.addGenerator(gen);
        for (Transition trans: this.net.getSortedTransitions()) {
            gen.addTransition(trans.getIndex(), (DTransition)trans);
        }
    }

}
