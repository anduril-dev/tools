package org.anduril.javatools.petrinet.test;

import org.junit.Test;

public class SmallNetTest extends GeneratedModelTest {
    public static final int NUM_INTERVALS = 5;
    
    public SmallNetTest() {
        super(NUM_INTERVALS);
    }
    
    @Test
    public void test_P0_T0() throws Exception {
        run(new double[5][0]);
    }

    @Test
    public void test_P0_T1() throws Exception {
        addTransition("t1");
        run(new double[5][0]);
    }
    
    @Test
    public void test_P1_T0() throws Exception {
        addPlace("p1", 5);
        run(new double[][] { {5}, {5}, {5}, {5}, {5} });
    }

    @Test
    public void test_P1_T1_A0() throws Exception {
        addPlace("p1", 5);
        addTransition("t1");
        run(new double[][] { {5}, {5}, {5}, {5}, {5} });
    }

    @Test
    public void test_P1_T1_A1_V1() throws Exception {
        addPlace("p1", 5);
        addTransition("t1");
        addArc("p1", "t1");
        run(new double[][] { {5}, {4}, {3}, {2}, {1} });
        
        setMarking("p1", 3);
        run(new double[][] { {3}, {2}, {1}, {0}, {0} });        
    }

    @Test
    public void test_P1_T1_A1_V2() throws Exception {
        addPlace("p1", 5);
        addTransition("t1");
        addArc("t1", "p1");
        run(new double[][] { {5}, {6}, {7}, {8}, {9} });
    }

    @Test
    public void test_P1_T1_A2() throws Exception {
        addPlace("p1", 5);
        addTransition("t1");
        addArc("p1", "t1");
        addArc("t1", "p1");
        run(new double[][] { {5}, {5}, {5}, {5}, {5} });
    }

}
