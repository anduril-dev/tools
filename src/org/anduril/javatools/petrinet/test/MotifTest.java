package org.anduril.javatools.petrinet.test;

import org.junit.Test;

import org.anduril.javatools.petrinet.GeneratorType;

public class MotifTest extends GeneratedModelTest {
    public static final int NUM_INTERVALS = 10;
    
    public MotifTest() {
        super(NUM_INTERVALS);
    }

    private void makeLoop(int numPlaces) {
        for (int i=0; i<numPlaces; i++) {
            final String placeID = String.format("p%d", i+1);
            final String transID = String.format("t%d", i+1);
            final String prevTransID = String.format("t%d", i);
            addPlace(placeID, i == 0 ? 1 : 0);
            if (i > 0) {
                addArc(prevTransID, placeID);
            }
            if (i < numPlaces - 1) {
                addTransition(transID);
                addArc(placeID, transID);
            } else {
                addArc(prevTransID, "p1");
            }
        }
    }
    
    @Test
    public void testLoop_P3() throws Exception {
        makeLoop(3);
        
        setGeneratorType(GeneratorType.ALL);
        run(new double[][] {
                {1, 0, 0}, {1, 0, 1}, {1, 0, 2}, {1, 0, 3}, {1, 0, 4},
                {1, 0, 5}, {1, 0, 6}, {1, 0, 7}, {1, 0, 8}, {1, 0, 9},
                });
        
        setGeneratorType(GeneratorType.INCREMENTAL);
        run(new double[][] {
                {1, 0, 0}, {0, 1, 0}, {1, 0, 1}, {0, 1, 1},
                {1, 0, 2}, {0, 1, 2}, {1, 0, 3}, {0, 1, 3},
                {1, 0, 4}, {0, 1, 4} });
    }
    
    @Test
    public void testLoop_P4() throws Exception {
        makeLoop(4);
        
        setGeneratorType(GeneratorType.ALL);
        run(new double[][] {
                {1, 0, 0, 0}, {1, 0, 0, 1}, {1, 0, 0, 2}, {1, 0, 0, 3}, {1, 0, 0, 4},
                {1, 0, 0, 5}, {1, 0, 0, 6}, {1, 0, 0, 7}, {1, 0, 0, 8}, {1, 0, 0, 9},
                });
        
        setGeneratorType(GeneratorType.INCREMENTAL);
        run(new double[][] {
                {1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0},
                {1, 0, 0, 1}, {0, 1, 0, 1}, {0, 0, 1, 1},
                {1, 0, 0, 2}, {0, 1, 0, 2}, {0, 0, 1, 2},
                {1, 0, 0, 3} });
    }

    @Test
    public void testLoop_P5() throws Exception {
        makeLoop(5);
        
        setGeneratorType(GeneratorType.ALL);
        run(new double[][] {
                {1, 0, 0, 0, 0}, {1, 0, 0, 0, 1}, {1, 0, 0, 0, 2}, {1, 0, 0, 0, 3}, {1, 0, 0, 0, 4},
                {1, 0, 0, 0, 5}, {1, 0, 0, 0, 6}, {1, 0, 0, 0, 7}, {1, 0, 0, 0, 8}, {1, 0, 0, 0, 9},
                });
        
        setGeneratorType(GeneratorType.INCREMENTAL);
        run(new double[][] {
                {1, 0, 0, 0, 0}, {0, 1, 0, 0, 0}, {0, 0, 1, 0, 0}, {0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1}, {0, 1, 0, 0, 1}, {0, 0, 1, 0, 1}, {0, 0, 0, 1, 1},
                {1, 0, 0, 0, 2}, {0, 1, 0, 0, 2} });
    }

    private void makeBranch(int numBranches) {
        addPlace("p0", 8);
        addTransition("t0");
        addArc("p0", "t0");
        
        for (int i=0; i<numBranches; i++) {
            String place1 = String.format("pre%d", i+1);
            String trans = String.format("t%d", i+1);
            String place2 = String.format("p%d", i+1);
            addPlace(place1, 0);
            addTransition(trans);
            addPlace(place2, 0);
            addArc("t0", place1);
            addArc(place1, trans);
            addArc(trans, place2);
        }
    }
    
    @Test
    public void testBranch_B2() throws Exception {
        makeBranch(2);
        
        setGeneratorType(GeneratorType.ALL);
        run(new double[][] {
                {8, 0, 0, 0, 0}, {7, 0, 1, 0, 1}, {6, 0, 2, 0, 2}, {5, 0, 3, 0, 3}, {4, 0, 4, 0, 4},
                {3, 0, 5, 0, 5}, {2, 0, 6, 0, 6}, {1, 0, 7, 0, 7}, {0, 0, 8, 0, 8}, {0, 0, 8, 0, 8} });
        
        setGeneratorType(GeneratorType.INCREMENTAL);
        run(new double[][] {
                {8, 0, 0, 0, 0}, {7, 1, 0, 1, 0}, {7, 0, 1, 1, 0}, {7, 0, 1, 0, 1},
                {6, 1, 1, 1, 1}, {6, 0, 2, 1, 1}, {6, 0, 2, 0, 2},
                {5, 1, 2, 1, 2}, {5, 0, 3, 1, 2}, {5, 0, 3, 0, 3} });
    }
    
    @Test
    public void testBranch_B3() throws Exception {
        makeBranch(3);
        
        setGeneratorType(GeneratorType.ALL);
        run(new double[][] {
                {8, 0,0, 0,0, 0,0}, {7, 0,1, 0,1, 0,1}, {6, 0,2, 0,2, 0,2},
                {5, 0,3, 0,3, 0,3}, {4, 0,4, 0,4, 0,4}, {3, 0,5, 0,5, 0,5},
                {2, 0,6, 0,6, 0,6}, {1, 0,7, 0,7, 0,7}, {0, 0,8, 0,8, 0,8},
                {0, 0,8, 0,8, 0,8} });
        
        setGeneratorType(GeneratorType.INCREMENTAL);
        run(new double[][] {
                {8, 0,0, 0,0, 0,0},
                {7, 1,0, 1,0, 1,0}, {7, 0,1, 1,0, 1,0},
                {7, 0,1, 0,1, 1,0}, {7, 0,1, 0,1, 0,1},
                {6, 1,1, 1,1, 1,1}, {6, 0,2, 1,1, 1,1},
                {6, 0,2, 0,2, 1,1}, {6, 0,2, 0,2, 0,2},
                {5, 1,2, 1,2, 1,2}
        });
    }
    
    private void makeJoin(int numTransitions, int initialMarking) {
        String place1 = "p1";
        String place2 = "p2";
        addPlace(place1, initialMarking);
        addPlace(place2, 0);
        
        for (int i=0; i<numTransitions; i++) {
            String trans = String.format("t%d", i+1);
            addTransition(trans);
            addArc(place1, trans);
            addArc(trans, place2);
        }
    }
    
    @Test
    public void testJoin_T3() throws Exception {
        makeJoin(3, 22);
        
        setGeneratorType(GeneratorType.ALL);
        run(new double[][] {
                {22, 0}, {19, 3}, {16, 6}, {13, 9}, {10, 12},
                {7, 15}, {4, 18}, {1, 21}, {0, 22}, {0, 22}
        });
        
        setGeneratorType(GeneratorType.INCREMENTAL);
        setMarking("p1", 8);
        run(new double[][] {
                {8, 0}, {7, 1}, {6, 2}, {5, 3}, {4, 4},
                {3, 5}, {2, 6}, {1, 7}, {0, 8}, {0, 8}
        });
    }

    @Test
    public void testJoin_T4() throws Exception {
        makeJoin(4, 31);
        
        setGeneratorType(GeneratorType.ALL);
        run(new double[][] {
                {31, 0}, {27, 4}, {23, 8}, {19, 12}, {15, 16},
                {11, 20}, {7, 24}, {3, 28}, {0, 31}, {0, 31}
        });
        
        setGeneratorType(GeneratorType.INCREMENTAL);
        setMarking("p1", 8);
        run(new double[][] {
                {8, 0}, {7, 1}, {6, 2}, {5, 3}, {4, 4},
                {3, 5}, {2, 6}, {1, 7}, {0, 8}, {0, 8}
        });
    }

    @Test
    public void testJoin_T7() throws Exception {
        makeJoin(7, 8);
        setGeneratorType(GeneratorType.INCREMENTAL);
        run(new double[][] {
                {8, 0}, {7, 1}, {6, 2}, {5, 3}, {4, 4},
                {3, 5}, {2, 6}, {1, 7}, {0, 8}, {0, 8}
        });
    }

}
