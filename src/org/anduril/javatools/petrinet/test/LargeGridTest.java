package org.anduril.javatools.petrinet.test;

import java.io.File;

import org.junit.Test;

import org.anduril.javatools.petrinet.Arc;
import org.anduril.javatools.petrinet.DPlace;
import org.anduril.javatools.petrinet.DTransition;
import org.anduril.javatools.petrinet.EventGenerator;
import org.anduril.javatools.petrinet.GeneratorType;
import org.anduril.javatools.petrinet.PetriNet;
import org.anduril.javatools.petrinet.Transition;
import org.anduril.javatools.petrinet.codegen.CodeGenerator;
import org.anduril.javatools.petrinet.codegen.Compiler;
import org.anduril.javatools.petrinet.parser.PetriNetFinalizer;
import org.anduril.javatools.petrinet.runtime.Simulator;
import org.anduril.javatools.petrinet.runtime.WriterEventSource;

public class LargeGridTest {
    @Test
    public void test() throws Exception {
        // too large: 10, 150
        final int chains = 3; // 7
        final int chainLength = 10; // 100
        final double initMarking = 20;
        
        PetriNet net = new PetriNet();
        int placeIndex = 0;
        int transIndex = 0;
        EventGenerator gen = new EventGenerator(net, "gen1", 0, chainLength);
        gen.setInterval(1.0/chainLength);
        gen.setGeneratorType(GeneratorType.PERMUTATION);
        net.addGenerator(gen);
        
        for (int chain=0; chain<chains; chain++) {
            for (int pos=0; pos<chainLength; pos++) {
                DPlace place = new DPlace(net, String.format("P(%d,%d)", chain, pos), placeIndex++);
                net.addPlace(place);
                
                if (pos > 0) {
                    Transition prevTrans = net.getTransition(String.format("T(%d,%d)", chain, pos-1));
                    Arc arc = new Arc(net, String.format("%s-%s", prevTrans.getID(), place.getID()), prevTrans, place);
                    net.addArc(arc);
                } else {
                    place.setInitialMarking(chain == 0 ? initMarking : initMarking*2);
                }

                final boolean lastOfChain = pos == chainLength-1;
                if (!lastOfChain) {
                    DTransition trans = new DTransition(net, String.format("T(%d,%d)", chain, pos), transIndex++);
                    net.addTransition(trans);
                    gen.addTransition(pos, trans);
                    Arc arc = new Arc(net, String.format("%s-%s", place.getID(), trans.getID()), place, trans);
                    net.addArc(arc);
                }
                
                if (chain > 0) {
                    if (pos > 0) {
                        Transition transUp = net.getTransition(String.format("T(%d,%d)", 0, pos-1));
                        Arc arc = new Arc(net, String.format("%s-%s", transUp.getID(), place.getID()), transUp, place);
                        net.addArc(arc);
                    }
                    if (!lastOfChain) {
                        Transition transUp = net.getTransition(String.format("T(%d,%d)", 0, pos));
                        Arc arc = new Arc(net, String.format("%s-%s", place.getID(), transUp.getID()), place, transUp);
                        net.addArc(arc);
                    }
                }
            }
        }
        
        new PetriNetFinalizer(net).runFinalizer();
        System.out.println(String.format("%d places, %d transitions",
                net.getPlaces().size(), net.getTransitions().size()));
//        System.out.println(net);
        simulate(net, initMarking, 1);
    }
    
    private void simulate(PetriNet net, double endTime, double interval) throws Exception {
        final String packageName = "pkg";
        final String className = "GeneratedSimulator";
        long totalStart = System.nanoTime();
        String code = new CodeGenerator(net).generate(packageName, className);
//        System.out.println(code);
        Simulator sim = new Compiler().compile(code, packageName, className, null);
        sim.initialize(net);
        System.out.println(String.format("Code generation + compiling: %.3f s",
                (System.nanoTime()-totalStart)/1e9));

        final int iterations = 10000;
        String[] places = new String[] {"P(0,0)", "P(0,1)", "P(1,0)"};
        WriterEventSource reporter = new WriterEventSource(0, endTime, (int)endTime+1, sim, places, true);
        long simStart = System.nanoTime();
        long firings = 0;
        for (int i=0; i<iterations; i++) {
            firings += sim.simulate(reporter);
        }
        double time = (System.nanoTime()-simStart)/1e9;
        System.out.println(String.format("Simulation: %.3f s (%d firings, %.0f ns/firing)",
                time, firings, time*1e9/firings));
        System.out.println(String.format("Total: %.3f s",
                (System.nanoTime()-totalStart)/1e9));
        reporter.writeCSV(new File("/home/kovaska/sim-output.csv"), new File("/home/kovaska/sim-output-stddev.csv"), iterations);
    }
}
