package org.anduril.javatools.petrinet.test;

import org.junit.Before;
import org.junit.Test;

public class SmallNetTestP2T1 extends GeneratedModelTest {
    public static final int NUM_INTERVALS = 5;
    
    public SmallNetTestP2T1() {
        super(NUM_INTERVALS);
    }
    
    @Before
    public void make_P2_T1() {
        addPlace("p1", 5);
        addPlace("p2", 5);
        addTransition("t1");
    }

    /**
     * <pre>
     *    t1
     * p1   
     * p2     
     * </pre>
     */
    @Test
    public void test_P2_T1_A0_n_n() throws Exception {
        run(new double[][] { {5, 5}, {5, 5}, {5, 5}, {5, 5}, {5, 5} });
    }

    /**
     * <pre>
     *    t1
     * p1 P 
     * p2     
     * </pre>
     */
    @Test
    public void test_P2_T1_A1_P_n() throws Exception {
        addArc("p1", "t1");
        run(new double[][] { {5, 5}, {4, 5}, {3, 5}, {2, 5}, {1, 5} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 5}, {2, 5}, {1, 5}, {0, 5}, {0, 5} });
    }

    /**
     * <pre>
     *    t1
     * p1   
     * p2 P     
     * </pre>
     */
    @Test
    public void test_P2_T1_A1_n_P() throws Exception {
        addArc("p2", "t1");
        run(new double[][] { {5, 5}, {5, 4}, {5, 3}, {5, 2}, {5, 1} });
    }

    /**
     * <pre>
     *    t1
     * p1 T 
     * p2     
     * </pre>
     */
    @Test
    public void test_P2_T1_A1_T_n() throws Exception {
        addArc("t1", "p1");
        run(new double[][] { {5, 5}, {6, 5}, {7, 5}, {8, 5}, {9, 5} });
    }

    /**
     * <pre>
     *    t1
     * p1   
     * p2 T   
     * </pre>
     */
    @Test
    public void test_P2_T1_A1_n_T() throws Exception {
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {5, 6}, {5, 7}, {5, 8}, {5, 9} });
    }

    /**
     * <pre>
     *    t1
     * p1 P 
     * p2 P   
     * </pre>
     */
    @Test
    public void test_P2_T1_A2_P_P() throws Exception {
        addArc("p1", "t1");
        addArc("p2", "t1");
        run(new double[][] { {5, 5}, {4, 4}, {3, 3}, {2, 2}, {1, 1} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 5}, {2, 4}, {1, 3}, {0, 2}, {0, 2} });

        setMarking("p1", 5);
        setMarking("p2", 3);
        run(new double[][] { {5, 3}, {4, 2}, {3, 1}, {2, 0}, {2, 0} });
    }

    /**
     * <pre>
     *    t1
     * p1 P 
     * p2 T   
     * </pre>
     */
    @Test
    public void test_P2_T1_A2_P_T() throws Exception {
        addArc("p1", "t1");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {4, 6}, {3, 7}, {2, 8}, {1, 9} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 5}, {2, 6}, {1, 7}, {0, 8}, {0, 8} });
    }
    
    /**
     * <pre>
     *    t1
     * p1 T 
     * p2 T   
     * </pre>
     */
    @Test
    public void test_P2_T1_A2_T_T() throws Exception {
        addArc("t1", "p1");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {6, 6}, {7, 7}, {8, 8}, {9, 9} });
        
        setMarking("p1", 0);
        run(new double[][] { {0, 5}, {1, 6}, {2, 7}, {3, 8}, {4, 9} });
    }
    
    /**
     * <pre>
     *    t1
     * p1 PT 
     * p2     
     * </pre>
     */
    @Test
    public void test_P2_T1_A2_PT_n() throws Exception {
        addArc("p1", "t1");
        addArc("t1", "p1");
        run(new double[][] { {5, 5}, {5, 5}, {5, 5}, {5, 5}, {5, 5} });
    }
    
    /**
     * <pre>
     *    t1
     * p1   
     * p2 PT    
     * </pre>
     */
    @Test
    public void test_P2_T1_A2_n_PT() throws Exception {
        addArc("p2", "t1");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {5, 5}, {5, 5}, {5, 5}, {5, 5} });
    }

    /**
     * <pre>
     *    t1
     * p1 PT
     * p2 P   
     * </pre>
     */
    @Test
    public void test_P2_T1_A3_PT_P() throws Exception {
        addArc("p1", "t1");
        addArc("p2", "t1");
        addArc("t1", "p1");
        run(new double[][] { {5, 5}, {5, 4}, {5, 3}, {5, 2}, {5, 1} });
        
        setMarking("p2", 3);
        run(new double[][] { {5, 3}, {5, 2}, {5, 1}, {5, 0}, {5, 0} });
        
        setMarking("p1", 0);
        setMarking("p2", 5);
        run(new double[][] { {0, 5}, {0, 5}, {0, 5}, {0, 5}, {0, 5} });
    }

    /**
     * <pre>
     *    t1
     * p1 PT
     * p2 T    
     * </pre>
     */
    @Test
    public void test_P2_T1_A3_PT_T() throws Exception {
        addArc("p1", "t1");
        addArc("t1", "p1");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {5, 6}, {5, 7}, {5, 8}, {5, 9} });
        
        setMarking("p1", 0);
        run(new double[][] { {0, 5}, {0, 5}, {0, 5}, {0, 5}, {0, 5} });        
    }
    
    /**
     * <pre>
     *    t1
     * p1 T 
     * p2 PT  
     * </pre>
     */
    @Test
    public void test_P2_T1_A3_T_PT() throws Exception {
        addArc("p2", "t1");
        addArc("t1", "p1");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {6, 5}, {7, 5}, {8, 5}, {9, 5} });
        
        setMarking("p2", 0);
        run(new double[][] { {5, 0}, {5, 0}, {5, 0}, {5, 0}, {5, 0} });        
    }

    /**
     * <pre>
     *    t1
     * p1 PT 
     * p2 PT  
     * </pre>
     */
    @Test
    public void test_P2_T1_PT_PT() throws Exception {
        addArc("p1", "t1");
        addArc("p2", "t1");
        addArc("t1", "p1");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {5, 5}, {5, 5}, {5, 5}, {5, 5} });
        
        setMarking("p1", 0);
        run(new double[][] { {0, 5}, {0, 5}, {0, 5}, {0, 5}, {0, 5} });        
    }

}
