package org.anduril.javatools.petrinet.test;

import org.junit.Before;
import org.junit.Test;

public class SmallNetTestP2T2 extends GeneratedModelTest {
    public static final int NUM_INTERVALS = 5;
    
    public SmallNetTestP2T2() {
        super(NUM_INTERVALS);
    }
    
    @Before
    public void make_P2_T2() {
        addPlace("p1", 5);
        addPlace("p2", 5);
        addTransition("t1");
        addTransition("t2");
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 P     
     * p2    P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A2_P_n_n_P() throws Exception {
        addArc("p1", "t1");
        addArc("p2", "t2");
        run(new double[][] { {5, 5}, {4, 4}, {3, 3}, {2, 2}, {1, 1} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 5}, {2, 4}, {1, 3}, {0, 2}, {0, 1} });
    }
    /**
     * <pre>
     *    t1 t2
     * p1 T     
     * p2    T 
     * </pre>
     */
    @Test
    public void test_P2_T2_A2_T_n_n_T() throws Exception {
        addArc("t1", "p1");
        addArc("t2", "p2");
        run(new double[][] { {5, 5}, {6, 6}, {7, 7}, {8, 8}, {9, 9} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 5}, {4, 6}, {5, 7}, {6, 8}, {7, 9} });
    }    

    /**
     * <pre>
     *    t1 t2
     * p1 P  P  
     * p2 P    
     * </pre>
     */
    @Test
    public void test_P2_T2_A3_P_P_P_n() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("p2", "t1");
        run(new double[][] { {5, 5}, {3, 4}, {1, 3}, {0, 2}, {0, 2} });
        
        setMarking("p2", 1);
        run(new double[][] { {5, 1}, {3, 0}, {2, 0}, {1, 0}, {0, 0} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 P  P  
     * p2    P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A3_P_P_n_P() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("p2", "t2");
        run(new double[][] { {5, 5}, {3, 4}, {1, 3}, {0, 3}, {0, 3} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 PT    
     * p2    P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A3_PT_n_n_P() throws Exception {
        addArc("p1", "t1");
        addArc("p2", "t2");
        addArc("t1", "p1");
        run(new double[][] { {5, 5}, {5, 4}, {5, 3}, {5, 2}, {5, 1} });
        
        setMarking("p2", 3);
        run(new double[][] { {5, 3}, {5, 2}, {5, 1}, {5, 0}, {5, 0} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 P     
     * p2 T  P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A3_P_n_T_P() throws Exception {
        addArc("p1", "t1");
        addArc("p2", "t2");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {4, 5}, {3, 5}, {2, 5}, {1, 5} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 5}, {2, 5}, {1, 5}, {0, 5}, {0, 4} });
        
        setMarking("p1", 1);
        run(new double[][] { {1, 5}, {0, 5}, {0, 4}, {0, 3}, {0, 2} });
        
        setMarking("p2", 2);
        run(new double[][] { {1, 2}, {0, 2}, {0, 1}, {0, 0}, {0, 0} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 T  P  
     * p2    T 
     * </pre>
     */
    @Test
    public void test_P2_T2_A3_T_P_n_T() throws Exception {
        addArc("p1", "t2");
        addArc("t1", "p1");
        addArc("t2", "p2");
        run(new double[][] { {5, 5}, {5, 6}, {5, 7}, {5, 8}, {5, 9} });
        
        setMarking("p1", 0);
        run(new double[][] { {0, 5}, {0, 6}, {0, 7}, {0, 8}, {0, 9} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 P  P  
     * p2 T    
     * </pre>
     */
    @Test
    public void test_P2_T2_A3_P_P_T_n() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {3, 6}, {1, 7}, {0, 8}, {0, 8} });
        
        setMarking("p2", 0);
        run(new double[][] { {5, 0}, {3, 1}, {1, 2}, {0, 3}, {0, 3} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 P  P  
     * p2    T 
     * </pre>
     */
    @Test
    public void test_P2_T2_A3_P_P_n_T() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("t2", "p2");
        run(new double[][] { {5, 5}, {3, 6}, {1, 7}, {0, 7}, {0, 7} });
        
        setMarking("p2", 0);
        run(new double[][] { {5, 0}, {3, 1}, {1, 2}, {0, 2}, {0, 2} });        
    }

    /**
     * <pre>
     *    t1 t2
     * p1 T  T  
     * p2 T    
     * </pre>
     */
    @Test
    public void test_P2_T2_A3_T_T_T_n() throws Exception {
        addArc("t1", "p1");
        addArc("t1", "p2");
        addArc("t2", "p1");
        run(new double[][] { {5, 5}, {7, 6}, {9, 7}, {11, 8}, {13, 9} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 P  P  
     * p2 P  P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_P_P_P_P() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("p2", "t1");
        addArc("p2", "t2");
        run(new double[][] { {5, 5}, {3, 3}, {1, 1}, {0, 0}, {0, 0} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 5}, {1, 3}, {0, 2}, {0, 2}, {0, 2} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 P  P  
     * p2 P  T 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_P_P_P_T() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("p2", "t1");
        addArc("t2", "p2");
        run(new double[][] { {5, 5}, {3, 5}, {1, 5}, {0, 4}, {0, 4} });
        
        setMarking("p2", 0);
        run(new double[][] { {5, 0}, {3, 0}, {1, 0}, {0, 1}, {0, 1} });
        
        setMarking("p1", 6);
        run(new double[][] { {6, 0}, {4, 0}, {2, 0}, {0, 0}, {0, 0} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 P  P  
     * p2 T  P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_P_P_T_P() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("p2", "t1");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {3, 5}, {1, 5}, {0, 5}, {0, 5} });
        
        setMarking("p1", 3);
        setMarking("p2", 0);
        run(new double[][] { {3, 0}, {2, 0}, {1, 0}, {0, 0}, {0, 0} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 P  P  
     * p2 T  T 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_P_P_T_T() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("t1", "p2");
        addArc("t2", "p2");
        run(new double[][] { {5, 5}, {3, 7}, {1, 9}, {0, 10}, {0, 10} });
        
        setMarking("p1", 6);
        setMarking("p2", 0);
        run(new double[][] { {6, 0}, {4, 2}, {2, 4}, {0, 6}, {0, 6} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 P  T  
     * p2 T  P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_P_T_T_P() throws Exception {
        addArc("p1", "t1");
        addArc("p2", "t2");
        addArc("t1", "p2");
        addArc("t2", "p1");
        run(new double[][] { {5, 5}, {5, 5}, {5, 5}, {5, 5}, {5, 5} });
        
        setMarking("p1", 0);
        run(new double[][] { {0, 5}, {0, 5}, {0, 5}, {0, 5}, {0, 5} });
        
        setMarking("p1", 5);
        setMarking("p2", 0);
        run(new double[][] { {5, 0}, {5, 0}, {5, 0}, {5, 0}, {5, 0} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 P  T  
     * p2 T  T 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_P_T_T_T() throws Exception {
        addArc("p1", "t1");
        addArc("t1", "p2");
        addArc("t2", "p1");
        addArc("t2", "p2");
        run(new double[][] { {5, 5}, {5, 7}, {5, 9}, {5, 11}, {5, 13} });
        
        setMarking("p1", 0);
        setMarking("p2", 0);
        run(new double[][] { {0, 0}, {0, 2}, {0, 4}, {0, 6}, {0, 8} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 T  T  
     * p2 T  P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_T_T_T_P() throws Exception {
        addArc("p2", "t2");
        addArc("t1", "p1");
        addArc("t1", "p2");
        addArc("t2", "p1");
        run(new double[][] { {5, 5}, {7, 5}, {9, 5}, {11, 5}, {13, 5} });
        
        setMarking("p1", 0);
        setMarking("p2", 0);
        run(new double[][] { {0, 0}, {2, 0}, {4, 0}, {6, 0}, {8, 0} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 T  T  
     * p2 T  T 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_T_T_T_T() throws Exception {
        addArc("t1", "p1");
        addArc("t1", "p2");
        addArc("t2", "p1");
        addArc("t2", "p2");
        run(new double[][] { {5, 5}, {7, 7}, {9, 9}, {11, 11}, {13, 13} });
        
        setMarking("p1", 0);
        run(new double[][] { {0, 5}, {2, 7}, {4, 9}, {6, 11}, {8, 13} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 PT P  
     * p2    P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_PT_P_n_P() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("p2", "t2");
        addArc("t1", "p1");
        run(new double[][] { {5, 5}, {4, 4}, {3, 3}, {2, 2}, {1, 1} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 5}, {2, 4}, {1, 3}, {0, 2}, {0, 2} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 P     
     * p2 PT P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_P_n_PT_P() throws Exception {
        addArc("p1", "t1");
        addArc("p2", "t1");
        addArc("p2", "t2");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {4, 4}, {3, 3}, {2, 2}, {1, 1} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 5}, {2, 4}, {1, 3}, {0, 2}, {0, 1} });
        
        setMarking("p1", 5);
        setMarking("p2", 3);
        run(new double[][] { {5, 3}, {4, 2}, {3, 1}, {2, 0}, {2, 0} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 P  P  
     * p2    PT
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_P_P_n_PT() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("p2", "t2");
        addArc("t2", "p2");
        run(new double[][] { {5, 5}, {3, 5}, {1, 5}, {0, 5}, {0, 5} });
        
        setMarking("p2", 0);
        run(new double[][] { {5, 0}, {4, 0}, {3, 0}, {2, 0}, {1, 0} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 0}, {2, 0}, {1, 0}, {0, 0}, {0, 0} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 P     
     * p2 P  PT
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_P_n_P_PT() throws Exception {
        addArc("p1", "t1");
        addArc("p2", "t1");
        addArc("p2", "t2");
        addArc("t2", "p2");
        run(new double[][] { {5, 5}, {4, 4}, {3, 3}, {2, 2}, {1, 1} });
        
        setMarking("p1", 3);
        run(new double[][] { {3, 5}, {2, 4}, {1, 3}, {0, 2}, {0, 2} });
        
        setMarking("p1", 5);
        setMarking("p2", 3);
        run(new double[][] { {5, 3}, {4, 2}, {3, 1}, {2, 0}, {2, 0} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 PT    
     * p2 T  P 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_PT_n_T_P() throws Exception {
        addArc("p1", "t1");
        addArc("p2", "t2");
        addArc("t1", "p1");
        addArc("t1", "p2");
        run(new double[][] { {5, 5}, {5, 5}, {5, 5}, {5, 5}, {5, 5} });
        
        setMarking("p1", 0);
        run(new double[][] { {0, 5}, {0, 4}, {0, 3}, {0, 2}, {0, 1} });
        
        setMarking("p2", 3);
        run(new double[][] { {0, 3}, {0, 2}, {0, 1}, {0, 0}, {0, 0} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 PT    
     * p2 T  T 
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_PT_n_T_T() throws Exception {
        addArc("p1", "t1");
        addArc("t1", "p1");
        addArc("t1", "p2");
        addArc("t2", "p2");
        run(new double[][] { {5, 5}, {5, 7}, {5, 9}, {5, 11}, {5, 13} });
        
        setMarking("p1", 0);
        run(new double[][] { {0, 5}, {0, 6}, {0, 7}, {0, 8}, {0, 9} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 PT T  
     * p2 T    
     * </pre>
     */
    @Test
    public void test_P2_T2_A4_PT_T_T_n() throws Exception {
        addArc("p1", "t1");
        addArc("t1", "p1");
        addArc("t1", "p2");
        addArc("t2", "p1");
        run(new double[][] { {5, 5}, {6, 6}, {7, 7}, {8, 8}, {9, 9} });
        
        setMarking("p1", 0);
        run(new double[][] { {0, 5}, {1, 6}, {2, 7}, {3, 8}, {4, 9} });
        
        setMarking("p2", 0);
        run(new double[][] { {0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4} });
    }

}
