package org.anduril.javatools.petrinet.test;

import org.junit.Before;
import org.junit.Test;

public class SmallNetTestP1T2 extends GeneratedModelTest {
    public static final int NUM_INTERVALS = 5;
    
    public SmallNetTestP1T2() {
        super(NUM_INTERVALS);
    }
    
    @Before
    public void make_P1_T2() {
        addPlace("p1", 5);
        addTransition("t1");
        addTransition("t2");
    }

    /**
     * <pre>
     *    t1 t2
     * p1      
     * </pre>
     */
    @Test
    public void test_P1_T2_A0_n_n() throws Exception {
        run(new double[][] { {5}, {5}, {5}, {5}, {5} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 P    
     * </pre>
     */
    @Test
    public void test_P1_T2_A1_P_n() throws Exception {
        addArc("p1", "t1");
        run(new double[][] { {5}, {4}, {3}, {2}, {1} });
        
        setMarking("p1", 3);
        run(new double[][] { {3}, {2}, {1}, {0}, {0} });        
    }

    /**
     * <pre>
     *    t1 t2
     * p1 T    
     * </pre>
     */
    @Test
    public void test_P1_T2_A1_T_n() throws Exception {
        addArc("t1", "p1");
        run(new double[][] { {5}, {6}, {7}, {8}, {9} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 P  P 
     * </pre>
     */
    @Test
    public void test_P1_T2_A2_P_P() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        run(new double[][] { {5}, {3}, {1}, {0}, {0} });
        
        setMarking("p1", 6);
        run(new double[][] { {6}, {4}, {2}, {0}, {0} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 P  T 
     * </pre>
     */
    @Test
    public void test_P1_T2_A2_P_T() throws Exception {
        addArc("p1", "t1");
        addArc("t2", "p1");
        run(new double[][] { {5}, {5}, {5}, {5}, {5} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 T  P 
     * </pre>
     */
    @Test
    public void test_P1_T2_A2_T_P() throws Exception {
        addArc("p1", "t2");
        addArc("t1", "p1");
        run(new double[][] { {5}, {5}, {5}, {5}, {5} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 T  T 
     * </pre>
     */
    @Test
    public void test_P1_T2_A2_T_T() throws Exception {
        addArc("t1", "p1");
        addArc("t2", "p1");
        run(new double[][] { {5}, {7}, {9}, {11}, {13} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 PT   
     * </pre>
     */
    @Test
    public void test_P1_T2_A2_PT_n() throws Exception {
        addArc("p1", "t1");
        addArc("t1", "p1");
        run(new double[][] { {5}, {5}, {5}, {5}, {5} });
    }
    
    /**
     * <pre>
     *    t1 t2
     * p1 PT P 
     * </pre>
     */
    @Test
    public void test_P1_T2_A3_PT_P() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("t1", "p1");
        run(new double[][] { {5}, {4}, {3}, {2}, {1} });
        
        setMarking("p1", 3);
        run(new double[][] { {3}, {2}, {1}, {0}, {0} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 P  PT
     * </pre>
     */
    @Test
    public void test_P1_T2_A3_P_PT() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("t2", "p1");
        run(new double[][] { {5}, {4}, {3}, {2}, {1} });
        
        setMarking("p1", 3);
        run(new double[][] { {3}, {2}, {1}, {0}, {0} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 PT T 
     * </pre>
     */
    @Test
    public void test_P1_T2_A3_PT_T() throws Exception {
        addArc("p1", "t1");
        addArc("t1", "p1");
        addArc("t2", "p1");
        run(new double[][] { {5}, {6}, {7}, {8}, {9} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 T  PT
     * </pre>
     */
    @Test
    public void test_P1_T2_A3_T_PT() throws Exception {
        addArc("p1", "t2");
        addArc("t1", "p1");
        addArc("t2", "p1");
        run(new double[][] { {5}, {6}, {7}, {8}, {9} });
    }

    /**
     * <pre>
     *    t1 t2
     * p1 PT PT
     * </pre>
     */
    @Test
    public void test_P1_T2_A4_PT_PT() throws Exception {
        addArc("p1", "t1");
        addArc("p1", "t2");
        addArc("t1", "p1");
        addArc("t2", "p1");
        run(new double[][] { {5}, {5}, {5}, {5}, {5} });
        
        setMarking("p1", 0);
        run(new double[][] { {0}, {0}, {0}, {0}, {0} });
    }

}
