package org.anduril.javatools.petrinet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;

public class PetriNet {
    private Map<String, Place> places;
    private Map<String, EventGenerator> generators;
    private Map<String, Transition> transitions;
    private Map<String, Arc> arcs;
    private Graph<PNNode, Arc> graph;
    
    public PetriNet() {
        this.places = new TreeMap<String, Place>();
        this.generators = new TreeMap<String, EventGenerator>();
        this.transitions = new TreeMap<String, Transition>();
        this.arcs = new TreeMap<String, Arc>();
        this.graph = new DirectedSparseGraph<PNNode, Arc>();
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        
        for (Place place: this.places.values()) {
            sb.append(place.toString());
            sb.append('\n');
        }

        for (EventGenerator gen: this.generators.values()) {
            sb.append(gen.toString());
            sb.append('\n');
        }
        
        for (Transition trans: this.transitions.values()) {
            sb.append(trans.toString());
            sb.append('\n');
        }

        for (Arc arc: this.arcs.values()) {
            sb.append(arc.toString());
            sb.append('\n');
        }
        
        return sb.toString();
    }
    
    public Graph<PNNode, Arc> getGraph() {
        return this.graph;
    }
    
    public void addPlace(Place place) {
        this.places.put(place.getID(), place);
        this.graph.addVertex(place);
    }
    
    public Place getPlace(String placeID) {
        return this.places.get(placeID);
    }

    public Map<String, Place> getPlaces() {
        return this.places;
    }
    
    public List<Place> getSortedPlaces() {
        List<Place> places = new ArrayList<Place>(this.places.values());
        Collections.sort(places);
        return places;
    }
    
    public void addGenerator(EventGenerator gen) {
        this.generators.put(gen.getID(), gen);
    }
    
    public EventGenerator getGenerator(String genID) {
        return this.generators.get(genID);
    }
    
    public Map<String, EventGenerator> getGenerators() {
        return this.generators;
    }
    
    public void addTransition(Transition transition) {
        this.transitions.put(transition.getID(), transition);
        this.graph.addVertex(transition);
    }
    
    public Map<String, Transition> getTransitions() {
        return this.transitions;
    }
    
    public Transition getTransition(String transID) {
        return this.transitions.get(transID);
    }
    
    public List<Transition> getSortedTransitions() {
        List<Transition> transitions = new ArrayList<Transition>(this.transitions.values());
        Collections.sort(transitions);
        return transitions;
    }
    
    public Map<String, Arc> getArcs() {
        return this.arcs;
    }
    
    public void addArc(Arc arc) {
        this.arcs.put(arc.getID(), arc);
    }
    
    public PNNode getNode(String nodeID) {
        Place place = this.places.get(nodeID);
        if (place != null) return place;
        else return this.transitions.get(nodeID);
    }
    
    /**
     * Return the number of continuous/discrete places.
     * @return Array of length 2, with elements #(continuous places),
     *  #(discrete places).
     */
    public int[] getPlaceCounts() {
        int cPlaces = 0;
        int dPlaces = 0;
        
        for (Place place: this.places.values()) {
            if (place.getType() == NodeType.CONTINUOUS) cPlaces++;
            else if (place.getType() == NodeType.DISCRETE) dPlaces++;
        }
        
        return new int[] {cPlaces, dPlaces};
    }
    
    /**
     * Return the number of continuous/discrete places.
     * @return Array of length 2, with elements
     *  #(continuous transitions), #(discrete transitions).
     */
    public int[] getTransitionCounts() {
        int cTransitions = 0;
        int dTransitions = 0;
        
        for (Transition trans: this.transitions.values()) {
            if (trans.getType() == NodeType.CONTINUOUS) cTransitions++;
            else if (trans.getType() == NodeType.DISCRETE) dTransitions++;
        }
        
        return new int[] {cTransitions, dTransitions};
    }
    
}
