package org.anduril.javatools.petrinet.runtime;

import java.util.Collection;

public interface EventSource {
    public Event createInitial();
    public void activateEvent(Event event);
    public int[] getTransitions(Event event);
    public boolean postEvent(Event prevEvent, Collection<Event> queue);
}
