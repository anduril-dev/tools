package org.anduril.javatools.petrinet.runtime;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math.random.MersenneTwister;
import org.apache.commons.math.random.RandomData;
import org.apache.commons.math.random.RandomDataImpl;
import org.apache.commons.math.random.RandomGenerator;

import org.anduril.javatools.petrinet.PetriNet;

public abstract class Simulator {
    private final RandomGenerator rng;
    private final RandomData randomData;
    
    public Simulator() {
        this.rng = new MersenneTwister();
        this.randomData = new RandomDataImpl(this.rng);
    }
    
    public final double min(double... values) {
        double minValue = 0;
        for (int i=0; i<values.length; i++) {
            minValue = Math.min(minValue, values[i]);
        }
        return minValue;
    }

    public final double max(double... values) {
        double maxValue = 0;
        for (int i=0; i<values.length; i++) {
            maxValue = Math.max(maxValue, values[i]);
        }
        return maxValue;
    }
    
    public final int randomBinomial(int n, double p) {
        // Algorithm based on review (Section 2) in Kachitvichyanukul and Schmeiser:
        // Binomial random variate generation. Communications of the ACM, 1988(31)2, 216-222.
        // Running time: O(n*min(p, 1-p)).
        
        final boolean invert = p > 0.5;
        if (invert) p = 1 - p;
        
        final double q = 1 - p;
        final double s = p / q;
        final double a = (n+1) * s;
        double r = Math.pow(q, n);
        double u = this.rng.nextDouble();
        int x = 0;
        
        while (u > r) {
            u -= r;
            x++;
            r = ((a/x) - s) * r;
        }
        
        return invert ? n-x : x;
    }
    
    public final int randomUniformInt(int low, int high) {
        return this.randomData.nextInt(low, high);
    }
    
    public abstract String[] getPlaceNames();
    public abstract double[] getMarking();
    public abstract double[] getCapacity();
    
    public abstract void initialize(PetriNet net);
    public abstract int simulate(EventSource reporter);
    
    public Map<String, Integer> getPlaceIndices() {
        Map<String, Integer> map = new HashMap<String, Integer>();
        final String[] placeNames = getPlaceNames();
        for (int i=0; i<placeNames.length; i++) {
            map.put(placeNames[i], i);
        }
        return map;
    }
}
