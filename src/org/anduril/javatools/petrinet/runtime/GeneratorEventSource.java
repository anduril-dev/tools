package org.anduril.javatools.petrinet.runtime;

import java.util.Collection;
import java.util.Random;
import java.util.TreeSet;

import org.anduril.javatools.petrinet.GeneratorType;
import org.anduril.javatools.seq.ArrayTools;

public final class GeneratorEventSource implements EventSource {
    private final GeneratorType type;
    private final double start;
    private final double interval;
    private final int numSignals;
    private final int[][] transitions;
    private final Random random;
    
    /**
     * Initialize.
     * @param type Generator type.
     * @param start Start time.
     * @param interval Interval between events.
     * @param numSignals Number of signals.
     * @param transitions Indicates which transitions are enabled by each.
     *  specific signal. transitions[i] gives the sorted array of transition
     *  indices for signal i. 
     */
    public GeneratorEventSource(GeneratorType type, double start, double interval, int numSignals, int[][] transitions) {
        this.type = type;
        this.start = start;
        this.interval = interval;
        this.numSignals = numSignals;
        this.transitions = transitions;
        this.random = new Random();
        
        if (type == GeneratorType.ALL) {
            this.transitions[0] = getAllTransitions(transitions);
            for (int i=1; i<this.transitions.length; i++) {
                this.transitions[i] = null;
            }
        }
    }
    
    private int[] getAllTransitions(int[][] transitions) {
        TreeSet<Integer> allTransitions = new TreeSet<Integer>();
        for (int[] ts: transitions) {
            for (int t: ts) {
                allTransitions.add(t);
            }
        }
        
        final int[] transIndices = new int[allTransitions.size()];
        int index = 0;
        for (int t: allTransitions) {
            transIndices[index++] = t;
        }
        return transIndices;
    }

    @Override
    public Event createInitial() {
        return new Event(this.start, this, 0);
    }

    @Override
    public void activateEvent(Event event) {
        if (this.type == GeneratorType.PERMUTATION && event.getID() == 0) {
            ArrayTools.shuffle(this.transitions);
        }
    }
    
    @Override
    public int[] getTransitions(Event event) {
        switch (this.type) {
        case ALL:
            return this.transitions[0];
        case RANDOM_ONE:
            return this.transitions[this.random.nextInt(this.numSignals)];
        case PERMUTATION:
        case INCREMENTAL:
            return this.transitions[event.getID()];
        default:
            throw new IllegalArgumentException("Invalid generator type: "+this.type);
        }
    }
    
    @Override
    public boolean postEvent(Event prevEvent, Collection<Event> queue) {
        final double newTime = prevEvent.getTime()+this.interval;
        final int nextEventID = (prevEvent.getID() + 1) % this.numSignals;
        queue.add(new Event(newTime, this, nextEventID));
        return true;
    }
}
