package org.anduril.javatools.petrinet.runtime;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.anduril.asser.io.CSVWriter;

public final class WriterEventSource implements EventSource {
    public static final int EVENT_PRIORITY = -999; 
    
    private final double start;
    private final int numTimepoints;
    private final double interval;
    private final Simulator sim;
    private final int[] placeIndices;
    private final boolean stddev;
  
    private final double[][] markingSums;
    private final double[][] markingSquareSums;
    
    private final int[] empty = new int[] {};
    
    public WriterEventSource(double start, double end, int numTimepoints, Simulator sim, String[] places, boolean stddev) {
        this.start = start;
        this.numTimepoints = numTimepoints;
        this.interval = (end - start) / (numTimepoints - 1);
        this.sim = sim;
        this.stddev = stddev;
        
        if (places == null) {
            this.placeIndices = new int[sim.getPlaceNames().length];
            for (int i=0; i<this.placeIndices.length; i++) {
                this.placeIndices[i] = i;
            }
        } else {
            this.placeIndices = new int[places.length];
            Map<String, Integer> placeMap = sim.getPlaceIndices();
            for (int i=0; i<places.length; i++) {
                Integer index = placeMap.get(places[i]);
                if (index == null) {
                    throw new IllegalArgumentException(
                            String.format("Place not found in Petri net: %s", places[i]));
                }
                this.placeIndices[i] = index.intValue();
            }
        }
        
        this.markingSums = new double[numTimepoints][this.placeIndices.length];
        if (stddev) {
            this.markingSquareSums = new double[numTimepoints][this.placeIndices.length];
        } else {
            this.markingSquareSums = null;
        }
    }
    
    public WriterEventSource(double start, double end, int numTimepoints, Simulator sim) {
        this(start, end, numTimepoints, sim, null, false);
    }
    
    @Override
    public Event createInitial() {
        return new Event(this.start, this, 0, EVENT_PRIORITY);
    }

    @Override
    public void activateEvent(Event event) {
        final int pos = event.getID();
        final double[] marking = this.sim.getMarking();
        for (int i=0; i<this.placeIndices.length; i++) {
            final int placeID = this.placeIndices[i];
            final double mark = marking[placeID];
            this.markingSums[pos][i] += mark;
            if (this.stddev) {
                this.markingSquareSums[pos][i] += mark * mark;
            }
        }
    }

    @Override
    public int[] getTransitions(Event event) {
        return this.empty;
    }

    @Override
    public boolean postEvent(Event prevEvent, Collection<Event> queue) {
        final int newID = prevEvent.getID() + 1;
        final double newTime = this.start + this.interval*newID;
        if (prevEvent.getID() < this.numTimepoints-1) {
            queue.add(new Event(newTime, this, newID, EVENT_PRIORITY));
            return true;
        } else {
            return false;
        }
    }
    
    public double[][] getMarkingSums() {
        return this.markingSums;
    }

    public double[][] getMarkingSquareSums() {
        return this.markingSquareSums;
    }
    
    public void writeCSV(File meanOutput, File stddevOutput, int numIterations) throws IOException {
        if (stddevOutput != null && !this.stddev) {
            throw new IllegalArgumentException("Stddev's were not recorded, so stddevOutput must be null");
        }
        
        String[] columns = new String[this.placeIndices.length+1];
        final String[] placeNames = this.sim.getPlaceNames();
        columns[0] = "Time";
        for (int i=0; i<this.placeIndices.length; i++) {
            columns[i+1] = placeNames[this.placeIndices[i]];
        }
        
        CSVWriter meanWriter = new CSVWriter(columns, meanOutput);
        CSVWriter stddevWriter = stddevOutput == null ? null : new CSVWriter(columns, stddevOutput);

        try {
            for (int tp=0; tp<this.numTimepoints; tp++) {
                final double time = this.start + tp*this.interval;
                meanWriter.write(time);
                if (stddevWriter != null) stddevWriter.write(time);
                for (int i=0; i<this.placeIndices.length; i++) {
                    final double mean = this.markingSums[tp][i] / numIterations;
                    meanWriter.write(mean);
                    if (stddevWriter != null) {
                        final double squareMean = this.markingSquareSums[tp][i] / numIterations;
                        final double variance = squareMean - (mean*mean);
                        stddevWriter.write(Math.sqrt(variance));
                    }
                }
            }
        } finally {
            meanWriter.close();
            if (stddevWriter != null) stddevWriter.close();
        }
    }
    
    public void writeCSV(File meanOutput, int numIterations) throws IOException {
        writeCSV(meanOutput, null, numIterations);
    }
}
