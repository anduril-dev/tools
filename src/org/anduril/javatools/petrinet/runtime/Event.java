package org.anduril.javatools.petrinet.runtime;

public final class Event implements Comparable<Event> {
    private final double time;
    private final EventSource source;
    private final int eventID;
    private final int priority;
    
    public Event(double time, EventSource source, int eventID, int priority) {
        this.time = time;
        this.source = source;
        this.eventID = eventID;
        this.priority = priority;
    }
    
    public Event(double time, EventSource source, int eventID) {
        this(time, source, eventID, 0);
    }
    
    @Override
    public final int compareTo(Event other) {
        final int timeComp = Double.compare(this.time, other.time);
        if (timeComp != 0) return timeComp;
        else return this.priority - other.priority;
    }
    
    public final double getTime() {
        return this.time;
    }
    
    public final EventSource getSource() {
        return this.source;
    }
    
    public final int getID() {
        return this.eventID;
    }
    
    public final int getPriority() {
        return this.priority;
    }
}
