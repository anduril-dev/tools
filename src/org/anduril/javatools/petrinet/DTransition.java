package org.anduril.javatools.petrinet;

import java.util.ArrayList;
import java.util.List;

public class DTransition extends Transition {
    private String condition;
    private List<Signal> signals;
    
    public DTransition(PetriNet net, String id, int index) {
        super(net, id, index);
        this.signals = new ArrayList<Signal>();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("Transition %s (%d, %s): %s, rate %s, signals {",
                getID(), getIndex(), getName(), getType(), getRate()));
        for (int i=0; i<this.signals.size(); i++) {
            if (i > 0) sb.append(' ');
            sb.append(this.signals.get(i));
        }
        sb.append('}');
        return sb.toString();
    }
    
    public String getCondition() {
        return this.condition;
    }
    
    public void setCondition(String condition) {
        this.condition = condition;
    }

    public void addSignal(EventGenerator generator, int signalID) {
        this.signals.add(new Signal(generator, signalID));
    }
    
    public List<Signal> getSignals() {
        return this.signals;
    }
}
