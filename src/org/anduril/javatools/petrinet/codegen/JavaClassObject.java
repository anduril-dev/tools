package org.anduril.javatools.petrinet.codegen;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import javax.tools.SimpleJavaFileObject;

public class JavaClassObject extends SimpleJavaFileObject {
    private final ByteArrayOutputStream os;
    
    protected JavaClassObject(URI uri) {
        super(uri, Kind.CLASS);
        this.os = new ByteArrayOutputStream();
    }

    public byte[] getBytes() {
        return this.os.toByteArray();
    }

    @Override
    public OutputStream openOutputStream() throws IOException {
        return this.os;
    }

}
