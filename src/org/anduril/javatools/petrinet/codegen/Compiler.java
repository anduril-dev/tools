package org.anduril.javatools.petrinet.codegen;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

public class Compiler {
    public <T> T compile(String source, String packageName, String relativeName, List<String> errors) {
        JavaCompiler javac = ToolProvider.getSystemJavaCompiler();
        JavaSourceObject sourceObj;
        JavaClassObject classObj;
        try {
            sourceObj = new JavaSourceObject(makeURI(packageName, relativeName), source);
            classObj = new JavaClassObject(makeURI(packageName, relativeName));
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        
        JavaFileManagerImpl manager = new JavaFileManagerImpl(javac.getStandardFileManager(null, null, null));
        manager.putInputObject(packageName, relativeName, sourceObj);
        manager.putOutputObject(packageName, relativeName, classObj);
        
        List<JavaFileObject> sources = new ArrayList<JavaFileObject>();
        sources.add(sourceObj);
        
        CompilerListener listener = new CompilerListener();
        CompilationTask task = javac.getTask(null, manager, listener, null, null, sources);
        final boolean ok = task.call();
        if (errors != null) errors.addAll(listener.getErrors());
        if (ok) {
            try {
                ClassLoader cl = manager.getClassLoader(StandardLocation.CLASS_PATH);
                Class<? extends T> simulatorClass;
                final String fullName = String.format("%s.%s", packageName, relativeName);
                simulatorClass = (Class<? extends T>) cl.loadClass(fullName);
                
                Constructor<? extends T> cons;
                try {
                    cons = simulatorClass.getConstructor();
                } catch(NoSuchMethodException e) {
                    throw new RuntimeException(e);
                }

                try {
                    return cons.newInstance();
                } catch(IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch(InstantiationException e) {
                    throw new RuntimeException(e);
                } catch(InvocationTargetException e) {
                    throw new RuntimeException(e);
                }

            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        } else {
            StringBuffer sb = new StringBuffer();
            sb.append("Compilation failed");
            if (!listener.getErrors().isEmpty()) {
                sb.append(": ");
                sb.append(listener.getErrors().get(0));
            }
            throw new IllegalArgumentException(sb.toString());
        }
    }
    
    public static URI makeURI(String packageName, String relativeName) throws URISyntaxException {
        return new URI(String.format("string://%s/%s.java", packageName, relativeName));
    }
}
