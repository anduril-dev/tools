package org.anduril.javatools.petrinet.codegen;

import java.net.URI;

import javax.tools.SimpleJavaFileObject;

public class JavaSourceObject extends SimpleJavaFileObject {
    private final String source;
    
    public JavaSourceObject(URI uri, String source) {
        super(uri, Kind.SOURCE);
        this.source = source;
    }
    
    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) {
        return this.source;
    }
}
