package org.anduril.javatools.petrinet.codegen;

import org.anduril.javatools.petrinet.Arc;
import org.anduril.javatools.petrinet.EventGenerator;
import org.anduril.javatools.petrinet.PetriNet;
import org.anduril.javatools.petrinet.Place;
import org.anduril.javatools.petrinet.Transition;
import org.anduril.javatools.petrinet.parser.expr.ASTNode;
import org.anduril.javatools.petrinet.parser.expr.ExpressionException;

public class VelocityTool {
    public double getCapacity(Place place) {
        return place.getCapacity();
    }
    
    public String generateGeneratorConstructor(EventGenerator gen) {
        return gen.generateConstructorCode();
    }
    
    public String generateArcsEnabledExpression(Transition trans) {
        StringBuffer sb = new StringBuffer();
        
        for (Arc arc: trans.getPreArcs()) {
            if (sb.length() > 0) sb.append(" && ");
            sb.append(String.format("m[%d] >= %s", arc.getFrom().getIndex(), arc.getWeight()));
        }
        
        for (Arc arc: trans.getPostArcs()) {
            if (sb.length() > 0) sb.append(" && ");
            final int nodeID = arc.getTo().getIndex();
            sb.append(String.format("m[%d] <= c[%d]-%s", nodeID, nodeID, arc.getWeight()));
        }
        
        return (sb.length() == 0) ? "false" : sb.toString();
    }
    
    public String generateRateExpression(PetriNet net, Transition trans) throws ExpressionException {
        ASTNode rateAST = trans.getRateAST();
        StringBuffer buffer = new StringBuffer();
        rateAST.generateCode(net, buffer);
        return buffer.toString();
    }
    
    public int batchStart(PetriNet net, int batch, int batchSize) {
        if (batch < 0) throw new IllegalArgumentException("batch must be >= 0");
        return batch*batchSize;
    }

    public int batchEnd(PetriNet net, int batch, int batchSize) {
        if (batch < 0) throw new IllegalArgumentException("batch must be >= 0");
        return Math.min((batch+1) * batchSize, net.getTransitions().size());
    }
    
}
