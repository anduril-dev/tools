package org.anduril.javatools.petrinet.codegen;

import org.nfunk.jep.ASTConstant;
import org.nfunk.jep.ASTFunNode;
import org.nfunk.jep.ASTVarNode;
import org.nfunk.jep.ParseException;

import org.anduril.javatools.petrinet.PetriNet;
import org.anduril.javatools.petrinet.Place;
import org.anduril.javatools.petrinet.parser.BaseParserVisitor;

public class ExpressionCodeGen extends BaseParserVisitor<Object> {
    public static final String[] SUPPORTED_UNARY_OPERATORS = {"+", "-", "!"};
    public static final String[] SUPPORTED_BINARY_OPERATORS = {"+", "-", "*", "/", "&&", "||", ">", ">=", "<", "<=", "==", "!="};
    
    private final PetriNet net;
    private StringBuffer expr;
    
    public ExpressionCodeGen(PetriNet net) {
        this.net = net;
        this.expr = new StringBuffer();
    }
    
    public String getExpr() {
        return this.expr.toString().trim();
    }

    @Override
    public Object visit(ASTFunNode node, Object arg) throws ParseException {
        if (node.isOperator()) {
            String op = node.getName();
            int numChildren = node.jjtGetNumChildren();
            if (numChildren == 1) {
                this.expr.append(op);
                this.expr.append(" (");
                node.jjtGetChild(0).jjtAccept(this, null);
                this.expr.append(") ");
            } else if (numChildren == 2) {
                this.expr.append(" (");
                node.jjtGetChild(0).jjtAccept(this, null);
                this.expr.append(") ");
                this.expr.append(op);
                this.expr.append(" (");
                node.jjtGetChild(1).jjtAccept(this, null);
                this.expr.append(") ");
            }
        } else {
            this.expr.append(mapFunction(node.getName()));
            this.expr.append('(');
            for (int i=0; i<node.jjtGetNumChildren(); i++) {
                if (i > 0) this.expr.append(", ");
                node.jjtGetChild(i).jjtAccept(this, null);
            }
            this.expr.append(')');
        }
        
        return null;
    }
    
    @Override
    public Object visit(ASTVarNode node, Object arg) throws ParseException {
        String name = node.getName();
        Place place = this.net.getPlace(name);
        if (place == null) {
            throw new ParseException("Invalid variable reference: "+name);
        }
        this.expr.append(String.format("m[%d]", place.getIndex()));
        
        visitChildren(node);
        return null;
    }

    @Override
    public Object visit(ASTConstant node, Object arg) throws ParseException {
        String value = node.getValue().toString();
        if (value.endsWith(".0")) value = value.substring(0, value.length()-2);
        this.expr.append(value);
        visitChildren(node);
        return null;
    }

    private String mapFunction(String exprFunction) throws ParseException {
        if (exprFunction.equals("event")) return "event";
        else if (exprFunction.equals("min")) return "Math.min";
        else if (exprFunction.equals("max")) return "Math.max";
        else {
            throw new ParseException("Unknown function: "+exprFunction);
        }
    }
}
