package org.anduril.javatools.petrinet.codegen;

import java.io.StringWriter;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import org.anduril.javatools.petrinet.PetriNet;

public class CodeGenerator {
    public static final String TEMPLATE_SIMULATOR = "simulator.java";
    
    private PetriNet net;
    
    public CodeGenerator(PetriNet net) {
        this.net = net;
        
        int[] counts = net.getPlaceCounts();
        if (counts[0] > 0) {
            throw new IllegalArgumentException("Only discrete nets are supported");
        }
        counts = net.getTransitionCounts();
        if (counts[0] > 0) {
            throw new IllegalArgumentException("Only discrete nets are supported");
        }
    }
    
    public String generate(String packageName, String className, int transitionBatchSize) throws Exception {
        final int numTransitions = this.net.getTransitions().size();
        final int numBatches;
        if (numTransitions == 0) {
            numBatches = 1;
        } else {
            if (transitionBatchSize < 1 || transitionBatchSize > numTransitions) {
                throw new IllegalArgumentException("transitionBatchSize out of range: "+transitionBatchSize);
            }
            numBatches = (int)Math.ceil(numTransitions/(double)transitionBatchSize);    
        }
        
        Velocity.setProperty("runtime.references.strict", "true");
        Velocity.setProperty("resource.loader", "class");
        Velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader"); 
        Velocity.init();

        VelocityContext context = new VelocityContext();
        context.put("package", packageName);
        context.put("class", className);
        context.put("net", this.net);
        context.put("tool", new VelocityTool());
        
        context.put("batches", numBatches);
        context.put("batchSize", transitionBatchSize);
        
        Template template = Velocity.getTemplate(TEMPLATE_SIMULATOR);
        
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }
    
    public String generate(String packageName, String className) throws Exception {
        return generate(packageName, className, estimateBatchSize());
    }
    
    public int estimateBatchSize() {
        final int t = this.net.getTransitions().size();
        if (t == 0) return 1;
        else if (t < 50) return t;
        else if (t < 70) return (t/2)+1;
        else return 25;
    }
}
