package org.anduril.javatools.petrinet.codegen;

import java.io.IOException;
import java.security.SecureClassLoader;
import java.util.HashMap;
import java.util.Map;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;

public class JavaFileManagerImpl extends ForwardingJavaFileManager<JavaFileManager> {

    private final Map<String, JavaSourceObject> inputObjects; 
    private final Map<String, JavaClassObject> outputObjects;
    
    protected JavaFileManagerImpl(JavaFileManager manager) {
        super(manager);
        this.inputObjects = new HashMap<String, JavaSourceObject>();
        this.outputObjects = new HashMap<String, JavaClassObject>();
    }

    @Override
    public FileObject getFileForInput(Location location, String packageName,
          String relativeName) throws IOException {
        String fullName = String.format("%s.%s", packageName, relativeName);
        FileObject obj = this.inputObjects.get(fullName);
        if (obj != null) return obj;
        return super.getFileForInput(location, packageName, relativeName);
    }
    
    @Override
    public JavaFileObject getJavaFileForInput(JavaFileManager.Location location, String className,
            JavaFileObject.Kind kind) throws IOException {
        JavaFileObject obj = this.inputObjects.get(className);
        if (obj != null) return obj;
        return super.getJavaFileForInput(location, className, kind);
    }
    
    @Override
    public FileObject getFileForOutput(JavaFileManager.Location location, String packageName, String relativeName,
            FileObject sibling) throws IOException {
        String fullName = String.format("%s.%s", packageName, relativeName);
        FileObject obj = this.outputObjects.get(fullName);
        if (obj != null) return obj;
        return super.getFileForOutput(location, packageName, relativeName, sibling);
    }

    @Override
    public JavaFileObject getJavaFileForOutput(JavaFileManager.Location location,
            String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
        JavaFileObject obj = this.outputObjects.get(className);
        if (obj != null) return obj;
        return super.getJavaFileForOutput(location, className, kind, sibling);
    }
    
    public void putInputObject(String packageName, String relativeName, JavaSourceObject file) {
        String fullName = String.format("%s.%s", packageName, relativeName);
        this.inputObjects.put(fullName, file);
    }

    public void putOutputObject(String packageName, String relativeName, JavaClassObject file) {
        String fullName = String.format("%s.%s", packageName, relativeName);
        this.outputObjects.put(fullName, file);
    }
    
    private class MyClassLoader extends SecureClassLoader {
        @Override
        protected Class<?> findClass(String name) throws ClassNotFoundException {
            JavaClassObject obj = outputObjects.get(name);
            if (obj == null) {
                return super.findClass(name);
            } else {
                byte[] bytes = obj.getBytes();
                return super.defineClass(name, bytes, 0, bytes.length);
            }
        }
    }
    
    @Override
    public ClassLoader getClassLoader(Location location) {
        return new MyClassLoader();
    }
}
