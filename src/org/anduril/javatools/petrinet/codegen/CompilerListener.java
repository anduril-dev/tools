package org.anduril.javatools.petrinet.codegen;

import java.util.ArrayList;
import java.util.List;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaFileObject;

public class CompilerListener implements DiagnosticListener<JavaFileObject> {

    private List<String> errors;
    
    public CompilerListener() {
        this.errors = new ArrayList<String>();
    }
    
    public List<String> getErrors() {
        return this.errors;
    }
    
    @Override
    public void report(Diagnostic<? extends JavaFileObject> message) {
        String msg = message.getMessage(null);
        
        final String prefix1 = "string://";
        if (msg.startsWith(prefix1)) msg = msg.substring(prefix1.length());
        this.errors.add(msg);
    }

}
