package org.anduril.javatools.petrinet.codegen;

import java.util.Stack;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommandI;

public class DummyFunction implements PostfixMathCommandI {
    private final int numArguments;
    
    public DummyFunction(int numArguments) {
        this.numArguments = numArguments;
    }
    
    @Override
    public boolean checkNumberOfParameters(int numArgs) {
        if (this.numArguments < 0) return true;
        else return (this.numArguments == numArgs);
    }

    @Override
    public int getNumberOfParameters() {
        return this.numArguments;
    }

    @Override
    public void run(Stack stack) throws ParseException {
        throw new RuntimeException("Dummy function is not intended to be executed");
    }

    @Override
    public void setCurNumberOfParameters(int numArgs) { }
    
}