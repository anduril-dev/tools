package org.anduril.javatools.petrinet;

public class Signal {
    private EventGenerator generator;
    private int signalID;
    
    public Signal(EventGenerator generator, int signalID) {
        this.generator = generator;
        this.signalID = signalID;
    }
    
    @Override
    public String toString() {
        return String.format("<%s, %d>", this.generator.getID(), this.signalID);
    }
    
    public EventGenerator getEventGenerator() {
        return this.generator;
    }
    
    public int getSignalID() {
        return this.signalID;
    }
}
