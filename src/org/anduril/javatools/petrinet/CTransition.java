package org.anduril.javatools.petrinet;

public class CTransition extends Transition {
    public CTransition(PetriNet net, String id, int index) {
        super(net, id, index);
    }

    @Override
    public String toString() {
        return String.format("Transition %s (%d, %s): %s, rate %s",
                getID(), getIndex(), getName(), getType(), getRate());
    }

}
