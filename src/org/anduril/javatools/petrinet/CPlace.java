package org.anduril.javatools.petrinet;

public class CPlace extends Place {
    public CPlace(PetriNet net, String id, int index) {
        super(net, id, index);
    }
    
    @Override
    public String toString() {
        return String.format("Place %s (%d, %s): %s, init %.1f",
                getID(), getIndex(), getName(), getType(), getInitialMarking());
    }
}
