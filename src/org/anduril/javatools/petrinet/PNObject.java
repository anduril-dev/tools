package org.anduril.javatools.petrinet;

public abstract class PNObject {
    private final PetriNet net;
    private final String id;
    private String name;

    public PNObject(PetriNet net, String id) {
        if (id == null) {
            throw new IllegalArgumentException("id is null");
        }
        this.net = net;
        this.id = id;
    }
    
    public PetriNet getPetriNet() {
        return this.net;
    }
    
    public String getID() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
}
