package org.anduril.javatools.petrinet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.uci.ics.jung.algorithms.filters.KNeighborhoodFilter;
import edu.uci.ics.jung.algorithms.filters.KNeighborhoodFilter.EdgeType;
import edu.uci.ics.jung.graph.Graph;
import org.anduril.javatools.petrinet.parser.expr.ASTNode;

public abstract class Transition extends PNNode {
    private String rate;
    private ASTNode rateAST;
    
    public Transition(PetriNet net, String id, int index) {
        super(net, id, index);
    }
    
    public String getRate() {
        return this.rate;
    }
    
    public void setRate(String rate) {
        this.rate = rate;
    }
    
    public ASTNode getRateAST() {
        return this.rateAST;
    }
    
    public void setRateAST(ASTNode ast) {
        this.rateAST = ast;
    }
  
    public List<Transition> getDependants() {
        KNeighborhoodFilter<PNNode, Arc> filter = new KNeighborhoodFilter<PNNode, Arc>(this, 2, EdgeType.IN_OUT);
        Graph<PNNode, Arc> neighbors =  filter.transform(getPetriNet().getGraph());
        
        List<Transition> transitions = new ArrayList<Transition>();
        for (PNNode vertex: neighbors.getVertices()) {
            if ((vertex instanceof Transition) && vertex != this) {
                transitions.add((Transition)vertex);
            }
        }
        
        Collections.sort(transitions);
        return transitions;
    }
}
