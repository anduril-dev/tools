package org.anduril.javatools.petrinet;

public enum NodeType {
    CONTINUOUS("continuous"),
    DISCRETE("discrete"),
    GENERATOR("generator");
    
    private String value;
    
    private NodeType(String value) {
        this.value = value;
    }
    
    public static NodeType fromValue(String value) {
        for (NodeType type: NodeType.values()) {
            if (type.value.equals(value)) return type;
        }
        return null;
    }
}
