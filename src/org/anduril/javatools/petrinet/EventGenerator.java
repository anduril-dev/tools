package org.anduril.javatools.petrinet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.anduril.javatools.petrinet.runtime.EventSource;
import org.anduril.javatools.petrinet.runtime.GeneratorEventSource;

public class EventGenerator extends PNNode {
    public static final double DEFAULT_INTERVAL = 1.0;
    public static final GeneratorType DEFAULT_TYPE = GeneratorType.ALL;
    
    private GeneratorType genType;
    private int numSignals;
    private double start;
    private double interval;
    private List<List<DTransition>> transitions; 
    
    public EventGenerator(PetriNet net, String id, int index, int numSignals) {
        super(net, id, index);
        this.numSignals = numSignals;
        this.genType = DEFAULT_TYPE;
        this.start = 0;
        this.interval = DEFAULT_INTERVAL;
        
        this.transitions = new ArrayList<List<DTransition>>(numSignals);
        for (int i=0; i<numSignals; i++) {
            this.transitions.add(null); //new ArrayList<DTransition>());
        }
    }
    
    @Override
    public String toString() {
        return String.format("Generator %s (%d, %s): gentype %s, signals %d, start %.1f, interval %.1f",
                getID(), getIndex(), getName(), this.genType, this.numSignals, this.start, this.interval);
    }
    
    public EventGenerator trim() {
        List<List<DTransition>> trimmedTransitions = new ArrayList<List<DTransition>>();
        for (List<DTransition> transList: this.transitions) {
            if (transList != null && !transList.isEmpty()) {
                trimmedTransitions.add(transList);
            }
        }
        
        EventGenerator trimmed = new EventGenerator(getPetriNet(), getID(), getIndex(),
                trimmedTransitions.size());
        trimmed.setGeneratorType(getGeneratorType());
        trimmed.setInterval(getInterval());
        trimmed.setName(getName());
        trimmed.setStart(getStart());
        trimmed.setType(getType());
        trimmed.transitions = trimmedTransitions;
        return trimmed;
    }
    
    public GeneratorType getGeneratorType() {
        return this.genType;
    }
    
    public void setGeneratorType(GeneratorType type) {
        this.genType = type;
    }
    
    public int getNumSignals() {
        return this.numSignals;
    }
    
    public double getStart() {
        return this.start;
    }
    
    public void setStart(double start) {
        this.start = start;
    }
    
    public double getInterval() {
        return this.interval;
    }
    
    public void setInterval(double interval) {
        this.interval = interval;
    }
    
    public void addTransition(int signalID, DTransition trans) {
        List<DTransition> transList = this.transitions.get(signalID);
        if (transList == null) {
            transList = new ArrayList<DTransition>();
            this.transitions.set(signalID, transList);
        }
        transList.add(trans);
    }
    
    public List<DTransition> getTransitions(int signalID) {
        List<DTransition> transList = this.transitions.get(signalID);
        if (transList == null) return Collections.emptyList();
        return transList;
    }
    
    public String generateConstructorCode() {
        StringBuffer sb = new StringBuffer();
        
        sb.append(String.format("new GeneratorEventSource(GeneratorType.%s, %f, %f, %d, new int[][] {",
                getGeneratorType(), getStart(), getInterval(), getNumSignals()));
        
        for (int signalID=0; signalID<getNumSignals(); signalID++) {
            List<DTransition> transitions = getTransitions(signalID);
            if (signalID > 0) sb.append(", ");
            sb.append('{');
            for (int tnum=0; tnum<transitions.size(); tnum++) {
                if (tnum > 0) sb.append(", ");
                sb.append(transitions.get(tnum).getIndex());
            }
            sb.append('}');
        }
        
        sb.append("})");
        
        return sb.toString();
    }
    
    public EventSource makeEventSource() {
        int[][] transitionMatrix = new int[this.numSignals][];
        for (int signalID=0; signalID<getNumSignals(); signalID++) {
            List<DTransition> transitions = getTransitions(signalID);
            int[] transArray = new int[transitions.size()];
            for (int i=0; i<transitions.size(); i++) {
                transArray[i] = transitions.get(i).getIndex();
            }
            transitionMatrix[signalID] = transArray;
        }
        return new GeneratorEventSource(getGeneratorType(), getStart(), getInterval(), getNumSignals(), transitionMatrix);
    }
}
