package org.anduril.javatools.petrinet;

public enum GeneratorType {
    ALL("all"),
    INCREMENTAL("incremental"),
//    RANDOM_K("random_k"),
    RANDOM_ONE("random_one"),
    PERMUTATION("permutation");
    
    private String value;
    
    private GeneratorType(String value) {
        this.value = value;
    }
    
    public static GeneratorType fromValue(String value) {
        for (GeneratorType type: GeneratorType.values()) {
            if (type.value.equals(value)) return type;
        }
        return null;
    }
};