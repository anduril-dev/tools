package org.anduril.javatools.petrinet.parser;

import java.util.ArrayList;
import java.util.List;

import org.nfunk.jep.ASTConstant;
import org.nfunk.jep.ASTFunNode;
import org.nfunk.jep.ASTVarNode;
import org.nfunk.jep.ParseException;

import org.anduril.javatools.petrinet.Transition;
import org.anduril.javatools.petrinet.parser.expr.ASTNode;
import org.anduril.javatools.petrinet.parser.expr.BinomialFunction;
import org.anduril.javatools.petrinet.parser.expr.ConstantNode;
import org.anduril.javatools.petrinet.parser.expr.FunctionNode;
import org.anduril.javatools.petrinet.parser.expr.InfixOperatorNode;
import org.anduril.javatools.petrinet.parser.expr.LimitFunction;
import org.anduril.javatools.petrinet.parser.expr.MaxFunction;
import org.anduril.javatools.petrinet.parser.expr.MinFunction;
import org.anduril.javatools.petrinet.parser.expr.PlaceReference;

public class ASTBuilderVisitor extends BaseParserVisitor<ASTNode> {

    private final Transition transition;
    
    public ASTBuilderVisitor(Transition transition) {
        this.transition = transition;
    }
    
    @Override
    public ASTNode visit(ASTFunNode node, Object arg) throws ParseException {
        List<ASTNode> args = new ArrayList<ASTNode>();
        for (int i=0; i<node.jjtGetNumChildren(); i++) {
            args.add((ASTNode)node.jjtGetChild(i).jjtAccept(this, null));
        }
        
        final FunctionNode func;
        if (node.isOperator()) {
            String op = node.getName();
            func = new InfixOperatorNode(op, args);
        } else {
            String funcName = node.getName();
            
            if (funcName.equals(BinomialFunction.FUNCTION_NAME)) {
                func = new BinomialFunction(args);
            } else if (funcName.equals(LimitFunction.FUNCTION_NAME)) {
                func = new LimitFunction(this.transition, args);
            } else if (funcName.equals(MaxFunction.FUNCTION_NAME)) {
                func = new MaxFunction(args);
            } else if (funcName.equals(MinFunction.FUNCTION_NAME)) {
                func = new MinFunction(args);
            } else {
                throw new ParseException("Invalid function: "+funcName);
            }
        }
        
        return func;
    }

    @Override
    public ASTNode visit(ASTVarNode node, Object arg1) throws ParseException {
        return new PlaceReference(node.getName());
    }

    @Override
    public ASTNode visit(ASTConstant node, Object arg1) throws ParseException {
        return new ConstantNode(node.getValue());
    }

}
