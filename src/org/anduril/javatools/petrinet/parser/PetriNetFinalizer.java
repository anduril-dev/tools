package org.anduril.javatools.petrinet.parser;

import org.nfunk.jep.JEP;
import org.nfunk.jep.Node;
import org.nfunk.jep.ParseException;

import org.anduril.javatools.petrinet.DTransition;
import org.anduril.javatools.petrinet.EventGenerator;
import org.anduril.javatools.petrinet.PetriNet;
import org.anduril.javatools.petrinet.Place;
import org.anduril.javatools.petrinet.Transition;
import org.anduril.javatools.petrinet.codegen.DummyFunction;
import org.anduril.javatools.petrinet.parser.expr.ASTNode;

public class PetriNetFinalizer {
    private PetriNet net;
    
    public PetriNetFinalizer(PetriNet net) {
        this.net = net;
    }
    
    public void runFinalizer() {
        parseExpressions();
        parseSignals();
        fillSignals();
    }
    
    private void parseExpressions() {
        JEP jep = new JEP();
        for (Place place: net.getPlaces().values()) {
            jep.addVariable(place.getID(), 0);
        }
        
        jep.addFunction("binomial", new DummyFunction(2));
        jep.addFunction("limit", new DummyFunction(-1));
        jep.addFunction("min", new DummyFunction(-1));
        jep.addFunction("max", new DummyFunction(-1));
        jep.addFunction("space", new DummyFunction(1));
        
        for (Transition trans: this.net.getTransitions().values()) {
            ASTBuilderVisitor builder = new ASTBuilderVisitor(trans);
            Node node;
            if (trans.getRate() == null || trans.getRate().isEmpty()) {
                trans.setRate("1");
            }
            try {
                node = jep.parse(trans.getRate());
                ASTNode ast = (ASTNode)node.jjtAccept(builder, null);
                trans.setRateAST(ast);
            } catch (ParseException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
    
    private void parseSignals() {
        JEP jep = new JEP();
        jep.addFunction("event", new DummyFunction(2));
        for (EventGenerator gen: this.net.getGenerators().values()) {
            jep.addVariable(gen.getID(), 0);
        }
        
        for (Transition trans: this.net.getTransitions().values()) {
            if (!(trans instanceof DTransition)) continue;
            DTransition dtrans = (DTransition)trans;
            String condition = dtrans.getCondition();
            if (condition == null || condition.isEmpty()) continue;
            
            try {
                Node node = jep.parse(condition);
                EventParserVisitor visitor = new EventParserVisitor(this.net, dtrans);
                node.jjtAccept(visitor, null);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
    
    private void setTransitionSignals(EventGenerator gen) {
        for (int signalID=0; signalID<gen.getNumSignals(); signalID++) {
            for (DTransition trans: gen.getTransitions(signalID)) {
                trans.addSignal(gen, signalID);
            }
        }
    }
    
    private void fillSignals() {
        for (EventGenerator gen: this.net.getGenerators().values()) {
            setTransitionSignals(gen);
        }
        
        EventGenerator defaultGen = new EventGenerator(this.net, "default-generator",
                this.net.getGenerators().size(), this.net.getTransitions().size());
        defaultGen.setName("Default generator");
        
        for (Transition trans: this.net.getTransitions().values()) {
            if (!(trans instanceof DTransition)) continue;
            DTransition dtrans = (DTransition)trans;
            
            if (dtrans.getSignals().isEmpty()) {
                defaultGen.addTransition(dtrans.getIndex(), dtrans);
            }
        }
        
        defaultGen = defaultGen.trim();
        if (defaultGen.getNumSignals() > 0) {
            setTransitionSignals(defaultGen);
            this.net.addGenerator(defaultGen);
        }
    }
}
