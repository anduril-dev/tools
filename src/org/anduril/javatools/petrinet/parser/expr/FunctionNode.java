package org.anduril.javatools.petrinet.parser.expr;

import java.util.List;

import org.anduril.javatools.petrinet.PetriNet;

public abstract class FunctionNode extends ASTNode {
    private String functionName;
    private List<ASTNode> args;
    
    public FunctionNode(String functionName, List<ASTNode> args) {
        this.functionName = functionName;
        this.args = args;
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.functionName);
        sb.append('(');
        for (int i=0; i<getArgs().size(); i++) {
            if (i > 0) sb.append(", ");
            sb.append(getArgs().get(i));
        }
        sb.append(')');
        return sb.toString();
    }
    
    public String getFunctionName() {
        return this.functionName;
    }
    
    public List<ASTNode> getArgs() {
        return this.args;
    }
    
    public ASTNode getArg(int position) {
        return this.args.get(position);
    }
    
    /**
     * Generate code for recursively calling a function with two
     * arguments, for all arguments of the current function.
     * @param funcName Specifies the Java function to be called,
     *  e.g., "Math.max".
     * @param noArgsValue If args are empty, this value is generated.
     * @param net The Petri net.
     * @param buffer Target buffer.
     */
    public void generateRecursiveCallBinary(String funcName, String noArgsValue, PetriNet net, StringBuffer buffer)
    throws ExpressionException {
        final int n = getArgs().size();

        if (n == 0) {
            buffer.append(noArgsValue);
            return;
        } else if (n == 1) {
            getArgs().get(0).generateCode(net, buffer);
            return;
        }

        for (int i=0; i<n-1; i++) {
            buffer.append(funcName);
            buffer.append("(");
        }
        for (int i=0; i<n; i++) {
            if (i == 1) buffer.append(", ");
            else if (i > 1) buffer.append("), ");
            getArgs().get(i).generateCode(net, buffer);
            if (i == n-1) buffer.append(')');
        }
    }
    
}
