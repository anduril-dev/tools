package org.anduril.javatools.petrinet.parser.expr;

import java.util.List;

import org.anduril.javatools.petrinet.PetriNet;

public class InfixOperatorNode extends FunctionNode {

    public InfixOperatorNode(String operator, List<ASTNode> args) {
        super(operator, args);
    }
    
    @Override
    public void generateCode(PetriNet net, StringBuffer buffer)
            throws ExpressionException {
        final int n = getArgs().size();
        
        if (n == 1) {
            buffer.append(getFunctionName());
            buffer.append('(');
            getArgs().get(0).generateCode(net, buffer);
            buffer.append(')');
        } else if (n > 1) {
            for (int i=0; i<n; i++) {
                if (i > 0) {
                    buffer.append(' ');
                    buffer.append(getFunctionName());
                    buffer.append(' ');
                }
                buffer.append('(');
                getArgs().get(i).generateCode(net, buffer);
                buffer.append(')');
            }
        }
    }
}
