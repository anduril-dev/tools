package org.anduril.javatools.petrinet.parser.expr;

import org.anduril.javatools.petrinet.PetriNet;
import org.anduril.javatools.petrinet.Place;

public class SpaceFunction extends ASTNode {
    public static final String FUNCTION_NAME = "space";
    
    private String placeID;
    
    public SpaceFunction(String placeID) {
        this.placeID = placeID;
    }

    @Override
    public String toString() {
        return String.format("space(%s)", this.placeID);
    }
    
    public String getID() {
        return this.placeID;
    }
    
    @Override
    public void generateCode(PetriNet net, StringBuffer buffer)
            throws ExpressionException {
        Place place = net.getPlace(this.placeID);
        if (place == null) {
            throw new ExpressionException("Invalid place reference: "+this.placeID);
        }
        buffer.append(String.format("c[%d]-m[%d]", place.getIndex(), place.getIndex()));
    }
}
