package org.anduril.javatools.petrinet.parser.expr;

import java.util.List;

import org.anduril.javatools.petrinet.PetriNet;

public class BinomialFunction extends FunctionNode {
    public static final String FUNCTION_NAME = "binomial";
    
    public BinomialFunction(List<ASTNode> args) {
        super(FUNCTION_NAME, args);
    }
    
    @Override
    public void generateCode(PetriNet net, StringBuffer buffer) throws ExpressionException {
        buffer.append("randomBinomial(");
        
        buffer.append("(int)");
        buffer.append('(');
        getArgs().get(0).generateCode(net, buffer);
        buffer.append(')');
        
        buffer.append(", ");
        
        buffer.append('(');
        getArgs().get(1).generateCode(net, buffer);
        buffer.append(')');
        
        buffer.append(')');
    }

}
