package org.anduril.javatools.petrinet.parser.expr;

import org.anduril.javatools.petrinet.PetriNet;

public abstract class ASTNode {
    public abstract void generateCode(PetriNet net, StringBuffer buffer) throws ExpressionException;
}
