package org.anduril.javatools.petrinet.parser.expr;

import org.anduril.javatools.petrinet.PetriNet;

public class ConstantNode extends ASTNode {

    private Object value;
    
    public ConstantNode(Object value) {
        this.value = value;
    }
    
    @Override
    public String toString() {
        return String.format("Constant: %s", this.value);
    }
    
    @Override
    public void generateCode(PetriNet net, StringBuffer buffer)
            throws ExpressionException {
        buffer.append(this.value);
    }

}
