package org.anduril.javatools.petrinet.parser.expr;

import java.util.List;

import org.anduril.javatools.petrinet.PetriNet;

public class MinFunction extends FunctionNode {
    public static final String FUNCTION_NAME = "min";
    
    public MinFunction(List<ASTNode> args) {
        super(FUNCTION_NAME, args);
    }
    
    @Override
    public void generateCode(PetriNet net, StringBuffer buffer) throws ExpressionException {
        final int n = getArgs().size();

        if (n == 0) {
            buffer.append('0');
            return;
        } else if (n == 1) {
            getArg(0).generateCode(net, buffer);
            return;
        } else if (n == 2) {
            buffer.append("Math.min(");
            getArg(0).generateCode(net, buffer);
            buffer.append(", ");
            getArg(1).generateCode(net, buffer);
            buffer.append(')');
        } else {
            buffer.append("min(");
            for (int i=0; i<n; i++) {
                if (i > 0) buffer.append(", ");
                getArg(i).generateCode(net, buffer);
            }
            buffer.append(')');
        }
    }
}
