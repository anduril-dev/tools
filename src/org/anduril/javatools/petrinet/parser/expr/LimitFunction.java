package org.anduril.javatools.petrinet.parser.expr;

import java.util.ArrayList;
import java.util.List;

import org.anduril.javatools.petrinet.Arc;
import org.anduril.javatools.petrinet.PetriNet;
import org.anduril.javatools.petrinet.Place;
import org.anduril.javatools.petrinet.Transition;

public class LimitFunction extends FunctionNode {
    public static final String FUNCTION_NAME = "limit";
    
    private Transition transition;
    
    public LimitFunction(Transition transition, List<ASTNode> args) {
        super(FUNCTION_NAME, args);
        this.transition = transition;
    }
    
    public Transition getTransition() {
        return this.transition;
    }
    
    @Override
    public void generateCode(PetriNet net, StringBuffer buffer)
            throws ExpressionException {
        // Generate tree for max(0, min(...)).
        
        List<ASTNode> minArgs = new ArrayList<ASTNode>(getArgs());
        
        for (Arc arc: this.transition.getPreArcs()) {
            Place from = (Place)arc.getFrom();
            minArgs.add(new PlaceReference(from.getID()));
        }

        for (Arc arc: this.transition.getPostArcs()) {
            Place to = (Place)arc.getTo();
            if (to.hasFiniteCapacity()) {
                minArgs.add(new SpaceFunction(to.getID()));
            }
        }
        
        List<ASTNode> maxArgs = new ArrayList<ASTNode>();
        maxArgs.add(new ConstantNode(0));
        maxArgs.add(new MinFunction(minArgs));
        MaxFunction maxFunc = new MaxFunction(maxArgs);
        maxFunc.generateCode(net, buffer);
    }

}
