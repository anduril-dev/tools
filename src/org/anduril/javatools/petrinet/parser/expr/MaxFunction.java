package org.anduril.javatools.petrinet.parser.expr;

import java.util.List;

import org.anduril.javatools.petrinet.PetriNet;

public class MaxFunction extends FunctionNode {
    public static final String FUNCTION_NAME = "max";
    
    public MaxFunction(List<ASTNode> args) {
        super(FUNCTION_NAME, args);
    }
    
    @Override
    public void generateCode(PetriNet net, StringBuffer buffer) throws ExpressionException {
        generateRecursiveCallBinary("Math.max", "0", net, buffer);
    }

}
