package org.anduril.javatools.petrinet.parser.expr;

public class ExpressionException extends Exception {
    private static final long serialVersionUID = -6695296282607541594L;

    public ExpressionException(String message) {
        super(message);
    }
}
