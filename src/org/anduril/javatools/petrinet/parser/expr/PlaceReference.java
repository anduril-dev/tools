package org.anduril.javatools.petrinet.parser.expr;

import org.anduril.javatools.petrinet.PetriNet;
import org.anduril.javatools.petrinet.Place;

public class PlaceReference extends ASTNode {
    private String id;
    
    public PlaceReference(String id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        return String.format("PlaceReference: %s", this.id);
    }
    
    public String getID() {
        return this.id;
    }
    
    @Override
    public void generateCode(PetriNet net, StringBuffer buffer) throws ExpressionException {
        Place place = net.getPlace(this.id);
        if (place == null) {
            throw new ExpressionException("Invalid place reference: "+this.id);
        }
        buffer.append(String.format("m[%d]", place.getIndex()));
    }
}
