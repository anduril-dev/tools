package org.anduril.javatools.petrinet.parser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import org.anduril.javatools.petrinet.Arc;
import org.anduril.javatools.petrinet.CPlace;
import org.anduril.javatools.petrinet.CTransition;
import org.anduril.javatools.petrinet.DPlace;
import org.anduril.javatools.petrinet.DTransition;
import org.anduril.javatools.petrinet.EventGenerator;
import org.anduril.javatools.petrinet.GeneratorType;
import org.anduril.javatools.petrinet.NodeType;
import org.anduril.javatools.petrinet.PNNode;
import org.anduril.javatools.petrinet.PetriNet;
import org.anduril.javatools.petrinet.Place;
import org.anduril.javatools.petrinet.Transition;
import org.anduril.javatools.petrinet.codegen.CodeGenerator;
import org.anduril.javatools.petrinet.codegen.Compiler;
import org.anduril.javatools.petrinet.runtime.Simulator;
import org.anduril.javatools.petrinet.runtime.WriterEventSource;

public class PNMLParser extends DefaultHandler {
    public static final String A_ID = "id";
    public static final String A_SOURCE = "source";
    public static final String A_TARGET = "target";
    
    public static final String E_ARC = "arc";
    public static final String E_CAPACITY = "capacity";
    public static final String E_CONDITION = "condition";
    public static final String E_DEFAULT_TYPE = "default-type";
    public static final String E_INITIAL_MARKING = "hlinitialmarking";
    public static final String E_INITIAL_MARKING2 = "initialMarking";
    public static final String E_INTERVAL = "interval";
    public static final String E_NAME = "name";
    public static final String E_PLACE = "place";
    public static final String E_RATE = "rate";
    public static final String E_SIGNALS = "signals";
    public static final String E_START = "start";
    public static final String E_SUBTYPE = "subtype";
    public static final String E_TEXT = "text";
    public static final String E_TEXT2 = "value";
    public static final String E_TRANSITION = "transition";
    public static final String E_TYPE = "type";
    public static final String E_WEIGHT = "hlinscription";
    public static final String E_WEIGHT2 = "inscription";
    
    private File file;
    private StringBuffer content;
    private String labelContent;
    
    private PetriNet net;
    private NodeType defaultType;
    private Map<String, String> attrMap;
    
    private int placeIndex;
    private int transitionIndex;
    private int generatorIndex;
    
    public PNMLParser(File file) {
        this.file = file;
        this.content = new StringBuffer();
        this.net = new PetriNet();
        this.defaultType = NodeType.DISCRETE;
        this.attrMap = new HashMap<String, String>();
        this.placeIndex = 0;
        this.transitionIndex = 0;
        this.generatorIndex = 0;
    }
    
    public void parse() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser parser = spf.newSAXParser();
        parser.parse(this.file, this);
    }
    
    public PetriNet getPetriNet() {
        return this.net;
    }
    
    private String getLabelContent() {
        if (this.labelContent != null) return this.labelContent;
        else return this.content.toString();
    }
    
    private double convertDouble(String str, String errorMessage) throws SAXException {
        double value;
        try {
            value = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            throw new SAXException(errorMessage+str);
        }
        return value;
    }

    private int convertInt(String str, String errorMessage) throws SAXException {
        int value;
        try {
            value = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            throw new SAXException(errorMessage+str);
        }
        return value;
    }
    
    @Override
    public void characters(char[] ch, int start, int length) {
        this.content.append(ch, start, length);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals(E_ARC)) {
            this.attrMap.clear();
            this.attrMap.put(A_ID, attributes.getValue(A_ID));
            this.attrMap.put(A_SOURCE, attributes.getValue(A_SOURCE));
            this.attrMap.put(A_TARGET, attributes.getValue(A_TARGET));
        } else if (qName.equals(E_PLACE)) {
            this.attrMap.clear();
            this.attrMap.put(A_ID, attributes.getValue(A_ID));
        } else if (qName.equals(E_TRANSITION)) {
            this.attrMap.clear();
            this.attrMap.put(A_ID, attributes.getValue(A_ID));
        }
        
        this.content.setLength(0);
        this.labelContent = null;
    }
    
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals(E_ARC)) {
            this.net.addArc(createArc(this.attrMap));
        } else if (qName.equals(E_PLACE)) {
            if ("generator".equals(this.attrMap.get(E_TYPE))) {
                this.net.addGenerator(createGenerator(this.attrMap));
            } else {
                this.net.addPlace(createPlace(this.attrMap));
            }
        } else if (qName.equals(E_TRANSITION)) {
            this.net.addTransition(createTransition(this.attrMap));
        } else if (qName.equals(E_CAPACITY)) {
            this.attrMap.put(E_CAPACITY, getLabelContent());
        } else if (qName.equals(E_CONDITION)) {
            this.attrMap.put(E_CONDITION, getLabelContent());
        } else if (qName.equals(E_INITIAL_MARKING) || qName.equals(E_INITIAL_MARKING2)) {
            this.attrMap.put(E_INITIAL_MARKING, getLabelContent());
        } else if (qName.equals(E_INTERVAL)) {
            this.attrMap.put(E_INTERVAL, getLabelContent());
        } else if (qName.equals(E_NAME)) {
            this.attrMap.put(E_NAME, getLabelContent());
        } else if (qName.equals(E_RATE)) {
            this.attrMap.put(E_RATE, getLabelContent());
        } else if (qName.equals(E_SIGNALS)) {
            this.attrMap.put(E_SIGNALS, getLabelContent());
        } else if (qName.equals(E_START)) {
            this.attrMap.put(E_START, getLabelContent());
        } else if (qName.equals(E_SUBTYPE)) {
            this.attrMap.put(E_SUBTYPE, getLabelContent());
        } else if (qName.equals(E_TYPE)) {
            this.attrMap.put(E_TYPE, getLabelContent());
        } else if (qName.equals(E_WEIGHT) || qName.equals(E_WEIGHT2)) {
            this.attrMap.put(E_WEIGHT, getLabelContent());
        } else if (qName.equals(E_TEXT) || qName.equals(E_TEXT2)) {
            this.labelContent = this.content.toString();
        }
    }
    
    private Arc createArc(Map<String, String> attrs) throws SAXException {
        String id = (String)attrs.get(A_ID);
        
        String sourceID = (String)attrs.get(A_SOURCE);
        PNNode source = this.net.getNode(sourceID);
        if (source == null) {
            throw new SAXException(String.format("Unknown source node for arc %s: %s", id, sourceID));
        }
        
        String targetID = attrs.get(A_TARGET);
        PNNode target = this.net.getNode(targetID);
        if (target == null) {
            throw new SAXException(String.format("Unknown target node for arc %s: %s", id, targetID));
        }
        
        Arc arc = new Arc(this.net, id, source, target);
        
        if (attrs.containsKey(E_NAME)) {
            arc.setName(attrs.get(E_NAME));
        }
        if (attrs.containsKey(E_WEIGHT)) {
            arc.setWeight(convertDouble(attrs.get(E_WEIGHT), "Invalid weight: "));
        }
        
        return arc;
    }
    
    private NodeType getNodeType(String value) throws SAXException {
        if (value == null) return null;
        NodeType type;
        if (value.equals("continuous")) type = NodeType.CONTINUOUS;
        else if (value.equals("discrete")) type = NodeType.DISCRETE;
        else if (value.equals("generator")) type = NodeType.GENERATOR;
        else throw new SAXException("Invalid node type: "+value);
        return type;
    }
    
    private void setNodeAttributes(PNNode node, Map<String, String> attrs) throws SAXException {
        if (attrs.containsKey(E_NAME)) {
            node.setName(attrs.get(E_NAME));
        }
        if (attrs.containsKey(E_TYPE)) {
            NodeType type = NodeType.fromValue(attrs.get(E_TYPE));
            node.setType(type);
        }
    }

    private EventGenerator createGenerator(Map<String, String> attrs) throws SAXException {
        if (!attrs.containsKey(E_SIGNALS)) {
            throw new SAXException("Invalid generator: <signals> must be present");
        }
        int numSignals = convertInt(attrs.get(E_SIGNALS), "Invalid signals: ");
        EventGenerator gen = new EventGenerator(this.net, attrs.get(A_ID), this.generatorIndex, numSignals);
        this.generatorIndex++;
        
        setNodeAttributes(gen, attrs);
        if (attrs.containsKey(E_INTERVAL)) {
            gen.setInterval(convertDouble(attrs.get(E_INTERVAL), "Invalid interval: "));
        }
        if (attrs.containsKey(E_START)) {
            gen.setStart(convertDouble(attrs.get(E_START), "Invalid generator start: "));
        }
        if (attrs.containsKey(E_SUBTYPE)) {
            GeneratorType type = GeneratorType.fromValue(attrs.get(E_SUBTYPE));
            if (type == null) {
                throw new SAXException("Invalid generator type: "+attrs.get(E_SUBTYPE));
            }
            gen.setGeneratorType(type);
        }
        
        return gen;
    }
    
    private Place createPlace(Map<String, String> attrs) throws SAXException {
        String id = attrs.get(A_ID);
        NodeType type = getNodeType(attrs.get(E_TYPE));
        if (type == null) type = this.defaultType;
        
        final Place place;
        switch (type) {
        case CONTINUOUS:
            place = new CPlace(this.net, id, this.placeIndex);
            break;
        case DISCRETE:
            DPlace dplace = new DPlace(this.net, id, this.placeIndex);
            place = dplace;
            if (attrs.containsKey(E_CAPACITY)) {
                dplace.setCapacity(convertDouble(attrs.get(E_CAPACITY), "Invalid capacity: "));
            }
            break;
        default:
            throw new SAXException("Invalid place type: "+type);
        }
        
        this.placeIndex++;
        
        setNodeAttributes(place, attrs);
        if (attrs.containsKey(E_INITIAL_MARKING)) {
            place.setInitialMarking(convertDouble(attrs.get(E_INITIAL_MARKING), "Invalid initial marking: "));
        }
        
        return place;
    }

    private Transition createTransition(Map<String, String> attrs) throws SAXException {
        String id = attrs.get(A_ID);
        NodeType type = getNodeType(attrs.get(E_TYPE));
        if (type == null) type = this.defaultType;
        
        final Transition trans;
        switch (type) {
        case CONTINUOUS:
            trans = new CTransition(this.net, id, this.transitionIndex);
            break;
        case DISCRETE:
            DTransition dtrans = new DTransition(this.net, id, this.transitionIndex);
            trans = dtrans;
            if (attrs.containsKey(E_CONDITION)) {
                dtrans.setCondition(attrs.get(E_CONDITION));
            }
            break;
        default:
            throw new SAXException("Invalid transition type: "+type);
        }
        
        this.transitionIndex++;
        
        setNodeAttributes(trans, attrs);
        if (attrs.containsKey(E_RATE)) {
            trans.setRate(attrs.get(E_RATE));
        }
        
        return trans;
    }
    
    public static void main(String[] args) throws Exception {
        for (String arg: args) {
            PNMLParser parser = new PNMLParser(new File(arg));
            parser.parse();
            PetriNet pn = parser.getPetriNet();
            new PetriNetFinalizer(pn).runFinalizer();
            System.out.println(pn);
            final String packageName = "org.anduril.javatools.petrinet.runtime";
            final String className = "TestSimGenerated";
            String code = new CodeGenerator(pn).generate(packageName, className);
            System.out.println(code);
            
            List<String> errors = new ArrayList<String>();
            Simulator sim;
            try {
                sim = new Compiler().compile(code, packageName, className, errors);
            } catch (IllegalArgumentException e) {
                for (String err: errors) System.err.println(err);
                throw e;
            }
            sim.initialize(pn);
            
            int n = 10;
            FileOutputStream os = new FileOutputStream(new File("/home/kovaska/sim-output.csv"));
            WriterEventSource reporter = new WriterEventSource(0, n, 10, sim);
            long start = System.nanoTime();
            try {
                sim.simulate(reporter);
            } finally {
                os.close();
            }
            double duration = (System.nanoTime() - start) / 1e9;
            System.out.println(String.format("%.2f s (%.1f ns/round)", duration, duration*1e9/n));
        }
    }
}
