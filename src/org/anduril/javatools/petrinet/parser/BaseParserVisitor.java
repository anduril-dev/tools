package org.anduril.javatools.petrinet.parser;

import org.nfunk.jep.ASTStart;
import org.nfunk.jep.Node;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.ParserVisitor;
import org.nfunk.jep.SimpleNode;

public abstract class BaseParserVisitor<T> implements ParserVisitor {
    
    protected void visitChildren(Node node) throws ParseException {
        for (int i=0; i<node.jjtGetNumChildren(); i++) {
            node.jjtGetChild(i).jjtAccept(this, null);
        }
    }
    
    @Override
    public T visit(SimpleNode node, Object arg) throws ParseException {
        visitChildren(node);
        return null;
    }

    @Override
    public T visit(ASTStart node, Object arg) throws ParseException {
        visitChildren(node);
        return null;
    }
}
