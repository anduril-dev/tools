package org.anduril.javatools.petrinet.parser;

import org.nfunk.jep.ASTConstant;
import org.nfunk.jep.ASTFunNode;
import org.nfunk.jep.ASTVarNode;
import org.nfunk.jep.ParseException;

import org.anduril.javatools.petrinet.DTransition;
import org.anduril.javatools.petrinet.EventGenerator;
import org.anduril.javatools.petrinet.PetriNet;

public class EventParserVisitor extends BaseParserVisitor<Object> {
    
    private final PetriNet net;
    private DTransition transition;
    
    public EventParserVisitor(PetriNet net, DTransition transition) {
        this.net = net;
        this.transition = transition;
    }
    
    @Override
    public Object visit(ASTFunNode node, Object arg) throws ParseException {
        final String name = node.getName();
        
        if (node.isOperator()) {
            if (!name.equals("||")) {
                throw new ParseException(String.format(
                        "Invalid operator %s: only || (or) is supported",
                        name));
            }
            visitChildren(node);
        } else {
            if (!name.equals("event")) {
                throw new ParseException(String.format(
                        "Invalid function %s: only event(generator, int) function is supported",
                        name));
            }
            if (node.jjtGetNumChildren() != 2) {
                throw new ParseException(String.format(
                        "Invalid function %s: expected 2 arguments",
                        name));
            }
            
            EventGenerator gen = (EventGenerator)node.jjtGetChild(0).jjtAccept(this, null);
            int signalID = (Integer)node.jjtGetChild(1).jjtAccept(this, null) - 1;
            if (signalID < 0) {
                throw new ParseException(String.format(
                        "Invalid condition (generator %s): signal ID must be >= 1",
                        gen.getID()));
            }
            if (signalID >= gen.getNumSignals()) {
                throw new ParseException(String.format(
                        "Invalid condition: generator %s has only %d signals",
                        gen.getID(), gen.getNumSignals()));
            }
            gen.addTransition(signalID, this.transition);
        }
        
        return null;
    }

    @Override
    public Object visit(ASTVarNode node, Object arg) throws ParseException {
        EventGenerator gen = this.net.getGenerator(node.getName());
        return gen;
    }

    @Override
    public Object visit(ASTConstant node, Object arg) throws ParseException {
        double d = (Double)node.getValue();
        return (int)d;
    }
}
