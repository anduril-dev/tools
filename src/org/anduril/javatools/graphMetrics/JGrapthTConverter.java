package org.anduril.javatools.graphMetrics;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import org.jgrapht.Graphs;

import java.util.HashSet;
import java.util.Set;

/**
 * Convert JGraphT graphs to JUNG graphs.
 * JGraphT edge type is generic; vertex type is String.
 */
public class JGrapthTConverter<E> {

    private org.jgrapht.Graph<String, E> jgraph;

    public JGrapthTConverter(org.jgrapht.Graph<String, E> jgraph) {
        this.jgraph = jgraph;
    }

    /**
     * Convert a JGraphT graph to a JUNG graph.
     * Return the JUNG graph instance.
     */
    public edu.uci.ics.jung.graph.Graph<String,String> convert() {
        Graph<String, String> jungGraph = new UndirectedSparseGraph<String,String>();

        /* Add vertices, populate ID map */
        for (String vertex: jgraph.vertexSet()) {
            jungGraph.addVertex(vertex);
        }

        /* Add edges */
        // remember edges has been added, otherwise will cause edu.uci.ics.jung.exceptions.ConstraintViolationException:
        // Predicate org.apache.commons.collections.functors.NotPredicate rejected E1
        Set<String> edgeSet = new HashSet<String>();
        for (String vertexName: jgraph.vertexSet()) {
            for (String neighborName : Graphs.neighborListOf(jgraph, vertexName)) {
                if (!edgeSet.contains(neighborName + "-" + vertexName)) {
                    final String edge = vertexName + "-" + neighborName;
                    edgeSet.add(edge);
                    jungGraph.addEdge(edge, vertexName, neighborName);
                }
            }
        }
        return jungGraph;
    }
}
