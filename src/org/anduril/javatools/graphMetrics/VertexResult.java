package org.anduril.javatools.graphMetrics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
Container for vertex metrics results. VertexResult contains a metric value
for each vertex. The values can be obtained as a map format and as a
sorted list.
*/
public class VertexResult {
    
    /**
    Pair that contains vertex ID and a double metric result
    value.
    */
    public static class Pair implements Comparable<Pair> {
        private String key;
        private double value;
        
        /**
        Init.
        
        @param key Vertex ID object.
        @param value Numeric value of some metric for the vertex.
        */
        public Pair(String key, double value) {
            this.key = key;
            this.value = value;
        }
        
        public String getKey() {
            return key;
        }
        
        public double getValue() {
            return value;
        }
        
        public int compareTo(Pair other) {
            Double t = new Double(getValue());
            Double o = new Double(other.getValue());
            return t.compareTo(o);
        }
    }

    private Map<String, Double> map;
    
    public VertexResult(Map<String, Double> map) {
        this.map = map;
    }
    
    public Map<String, Double> getMap() {
        return map;
    }
    
    /**
    Return results as a list. The list is ordered by value.
    
    @param ascending If true, smallest values appear first. Otherwise,
      largest values appear first.
    */
    public List<Pair> getList(boolean ascending) {
        return mapToList(map, ascending);
    }

    /**
    Return results as a list. The list is ordered by value in
    ascending order.
    */
    public List<Pair> getList() {
        return getList(true);
    }
    
    /**
    Return the metric value associated to given vertex.
    
    @throws IllegalArgumentException If vertexKey is not found.
    */
    public double getVertexValue(String vertexKey) {
        Double value = map.get(vertexKey);
        if (value == null) {
            throw new IllegalArgumentException("Vertex with ID "+vertexKey+" not found");
        }
        return value;
    }
    
    /**
    Convert a map of (key, value) to a sorted list of Pair instances.
    The pairs are ordered by value.
    
    @param map Metrics result map.
    @param ascending If true, smallest values appear first. Otherwise,
      largest values appear first.
    */
    private List<Pair> mapToList(Map<String, Double> map, boolean ascending) {
        List<Pair> list = new ArrayList<Pair>();
        for (Map.Entry<String, Double> entry: map.entrySet()) {
            list.add(new Pair(entry.getKey(), entry.getValue()));
        }
        Collections.sort(list);
        if (!ascending) Collections.reverse(list);
        return list;
    }
    
}
