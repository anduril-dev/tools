/*
Based on the original Handler from the JUNG project.
Modified to support "data" elements.
Not used at the moment.
*/
package org.anduril.javatools.graphMetrics;

/*
import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.io.GraphMLFileHandler;

public class MyGraphMLFileHandler extends GraphMLFileHandler {
    private UserDataContainer currentContainer;
    private Object currentDataKey;
    private StringBuilder value = new StringBuilder();
    
    protected Map getAttributeMap(Attributes attrs) {
        Map map = new HashMap();
        if (attrs != null) {
            for (int i = 0; i < attrs.getLength(); i++) {
                map.put(attrs.getQName(i), attrs.getValue(i));
            }
        }
        return map;
    }
    
    @Override
    public StringLabeller getLabeller() {
        return super.getLabeller();
    }
    
    @Override
    protected Edge createEdge(Map attributeMap) {
        Edge e = super.createEdge(attributeMap);
        this.currentContainer = e;
        return e;
    }

    @Override
    protected void createGraph(Map attributeMap) {
        super.createGraph(attributeMap);
        this.currentContainer = getGraph();
    }

    @Override
    protected ArchetypeVertex createVertex(Map attributeMap) {
        ArchetypeVertex vertex = super.createVertex(attributeMap);
        this.currentContainer = vertex;
        return vertex;
    }

    @Override
    public void startElement(
            String namespaceURI,
            String lName, // local name
            String qName, // qualified name
            Attributes attrs) throws SAXException {

        Map attributeMap = getAttributeMap(attrs);

        if (qName.toLowerCase().equals("graph")) {
            createGraph(attributeMap);
        } else if (qName.toLowerCase().equals("node")) {
            createVertex(attributeMap);
        } else if (qName.toLowerCase().equals("edge")) {
            createEdge(attributeMap);
        } else if (qName.toLowerCase().equals("data")) {
            this.currentDataKey = attributeMap.get("key");
         }
    }

    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        if (qName.toLowerCase().equals("data")) {
            this.currentContainer.setUserDatum(this.currentDataKey, value.toString().trim(), UserData.SHARED);
            this.currentDataKey = null;
            value.setLength(0);
        }
    }

    @Override
    public void characters(final char ch[], final int start, final int length) throws SAXException {
        for (int i = start; i < start + length; i++) {
            value.append(ch[i]);
        }
    }
    

}
*/