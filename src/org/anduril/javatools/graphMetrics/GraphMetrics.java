package org.anduril.javatools.graphMetrics;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections15.BidiMap;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import hep.aida.bin.DynamicBin1D; /* part of Colt */

import edu.uci.ics.jung.algorithms.importance.BetweennessCentrality;
import edu.uci.ics.jung.algorithms.importance.AbstractRanker;
import edu.uci.ics.jung.algorithms.metrics.Metrics;
import edu.uci.ics.jung.algorithms.shortestpath.Distance; 
import edu.uci.ics.jung.algorithms.shortestpath.DistanceStatistics;
import edu.uci.ics.jung.algorithms.shortestpath.UnweightedShortestPath;
import edu.uci.ics.jung.algorithms.util.Indexer;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.util.EdgeType;

import org.anduril.javatools.graphMetrics.VertexResult;

/**
Calculate various graph metrics. Currently tested only with undirected,
unweighted graphs. Many metrics are not defined for disconnected graphs.
*/
public class GraphMetrics {
    
    private Graph<String,String> graph;
    private Distance<String> distance;
    private BidiMap<String,Integer> indexer;
    
    /** degrees[k] = m <=> graph has m nodes with degree k */
    private int[] degrees;
    
    /**
    Initialize data structures that are used by metrics methods.
    
    @param graph JUNG graph instance.
    @throws NullPointerException if graph is null.
    */
    public GraphMetrics(Graph<String,String> graph) {
        if (graph == null) throw new NullPointerException("graph must not be null");
        
        this.graph = graph;
        this.distance = new UnweightedShortestPath<String,String>(graph);
        this.degrees = calcDegreeDistribution();
        this.indexer = Indexer.create(graph.getVertices());
    }

    /**
    Return the adjacency matrix for the graph. Vertex order is arbitrary
    but consistent (non-changing) between calls.
    */	
    public double[][] adjacencyMatrix() {
        final int VERTICES = graph.getVertexCount();
        double[][] result = new double[VERTICES][VERTICES];

        // TODO: could use JUNG's GraphMatrixOperations
        for (String vertex: graph.getVertices()) {
            final int row = indexer.get(vertex);
            for (String neighbor: graph.getNeighbors(vertex)) {
                final int col = indexer.get(neighbor);
                result[row][col] = 1;
            }
        }
        
        return result;
    }
    
    /**
    Return true if the graph is connected.
    */
    public boolean isConnected() {
        return DistanceStatistics.diameter(graph, distance, false) != Double.POSITIVE_INFINITY;
    }
    
    /**
    Return true if all edges of the graph are directed,
    false if there are any undirected edges.
    Note that it is possible that both isDirected and
    isUndirected are false.
    */
    public boolean isDirected() {
        return graph.getEdgeCount(EdgeType.UNDIRECTED) == 0;
    }

    /**
    Return true if all edges of the graph are undirected,
    false if there are any directed edges.
    Note that it is possible that both isDirected and
    isUndirected are false.
    */
    public boolean isUndirected() {
        return graph.getEdgeCount(EdgeType.DIRECTED) == 0;
    }
    
    /**
    Return the average degree of vertices.
    */
    public double avgDegree() {
        double sum = 0;
        int total = 0;
        for (int degree=0; degree<degrees.length; degree++) {
            sum += degree * degrees[degree];
            total += degrees[degree];
        }
        if (total == 0) return 0;
        return sum / (double)total;
    }

    /**
    Return the charasteristic path length of the graph. The path length is defined
    as S/(V*(V-1)), where S is the sum of all shortest path lengths between vertices
    and V is the number of vertices. If the graph is not connected,
    return POSITIVE_INFINITY.
    */
    public double charasteristicPathLength() {
        double sum = 0;
        for (String vertex: graph.getVertices()) {
            for (String w: graph.getVertices()) {
                Number dst = distance.getDistance(vertex, w);
                if (dst == null) return Double.POSITIVE_INFINITY; /* Early exit */
                sum += dst.doubleValue();
            }
        }
        
        final double N = graph.getVertexCount();
        return sum / (N*(N-1));
    }
    
    /**
    Return the clustering coefficient for each vertex. If
    the degree of a vertex is 0 or 1, the coefficient is 0.
    */
    public VertexResult clusteringCoefficients() {
        Map<String, Double> cmap = Metrics.clusteringCoefficients(graph);
        return new VertexResult(new TreeMap<String, Double>(cmap));
    }
    
    /**
    Return the average clustering coefficient. This is the mean
    of values returned by #clusteringCoefficients.
    */
    public double avgClusteringCoefficient() {
        Map<String, Double> map = clusteringCoefficients().getMap();
        if (map.isEmpty()) return 0;
        
        double sum = 0.0;
        for (Map.Entry<String, Double> entry: map.entrySet()) {
            sum += entry.getValue();
        }
        return sum / (double)map.size();
    }
    
    /**
    Return vertex degree distribution. In the result array, value x at
    index k gives the frequency or proportion of vertices with degree k.
    
    @param normalized If true, the values are normalized to range 0..1.
      Normalized (proportional) values are defined as f/V, where f is
      the number of vertices with a certain degree and V is the number
      of vertices.
    */
    public double[] degreeDistribution(boolean normalized) {
        final double N = graph.getVertexCount();
        double[] result = new double[degrees.length];
        if (N == 0) return result;
        
        for (int i=0; i<degrees.length; i++) {
            result[i] = normalized ? (double)degrees[i]/N : (double)degrees[i];
        }
        return result;
    }
    
    /**
    Return normalized vertex degree distribution.
    */
    public double[] degreeDistribution() {
        return degreeDistribution(true);
    }
    
    /**
    Return the diameter of the graph. Diameter is the maximum of shortest
    paths between vertices.
    */
    public double diameter() {
        return DistanceStatistics.diameter(graph, distance, false);
    }
    
    /**
    Return shortest path distribution. In the result array, value x at
    index k gives the frequency or proportion of shortest paths with length k.
    If graph is not connected, return null.
    
    @param normalized If true, values are divided by V*(V-1), where
      V is the number of vertices.
    */
    public double[] pathLengthDistribution(boolean normalized) {
        Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
        
        for (String vertex: graph.getVertices()) {
            for (String w: graph.getVertices()) {
                if (vertex.equals(w)) continue;
                
                Number number = distance.getDistance(vertex, w);
                if (number == null) return null; /* Early exit */
                int dst = number.intValue(); /* XXX: weighted graphs? */
            
                int count = 0;
                if (map.containsKey(dst)) count = map.get(dst);
                count++;
                map.put(dst, count);
            }
        }
        
        final double N = graph.getVertexCount();
        return mapToDistribution(map, normalized, 1/(N*(N-1)));
    }
    
    /* centrality */

    /**
    Return the betweenness centrality of each vertex.
    
    @param normalized If true, betweenness values are normalized by
      dividing by (V-1)(V-2), where V is the number of vertices.
      Normalized values are between 0..1 and values close to 1 are
      more central.
      If false, the values are unnormalized.
    */
    public VertexResult betweennessCentrality(boolean normalized) {
        Map<String, Double> result = calcCentrality(new BetweennessCentrality<String,String>(graph));
        
        if (!(graph instanceof UndirectedGraph) && isUndirected()) {
            /* JUNG (as of 2.0) divides betweenness values by 2 for undirected
             * graphs, but only if they are instances of UndirectedGraph.
             * Here we handle other undirected graphs (like SparseGraph). */ 
            multiplyMap(result, 0.5);
        }
        
        final double VERTICES = graph.getVertexCount();
        if (normalized && VERTICES > 2) {
            double factor = 1.0 / ( (VERTICES-1)*(VERTICES-2) );
            multiplyMap(result, factor);
        }
        
        return new VertexResult(result);
    }
    
    /**
    Return the closeness centrality of each vertex. The closeness centrality
    of vertex v is the sum of shortest paths from v to all other vertices.
    
    @param normalized If true, the score of each vertex is (V-1)/S, where S
      is the unnormalized sum. Normalized values are between 0..1
      and values close to 1 are more central.
      If false, the score is S.
    */
    public VertexResult closenessCentrality(boolean normalized) {
        Map<String, Double> result = new TreeMap<String, Double>();
        for (String vertex: graph.getVertices()) {
            double sum = 0;
            for (String w: graph.getVertices()) {
                Number dst = distance.getDistance(vertex, w);
                if (dst == null) {
                    sum = Double.POSITIVE_INFINITY;
                    break;
                }
                else {
                    sum += dst.doubleValue();
                }
            }
            result.put(vertex, sum);
        }
        
        final int VERTICES = graph.getVertexCount();
        if (normalized && VERTICES > 2) {
            for (Map.Entry<String, Double> entry: result.entrySet()) {
                double normValue = (VERTICES-1) / entry.getValue();
                result.put(entry.getKey(), normValue);
            }
        }
        
        return new VertexResult(result);
    }

    /**
    Return the degree centrality for each vertex.
    
    @param normalized If true, degrees are divided by V-1, where V is the
      number of vertices. Normalized values are between 0..1
      and values close to 1 are more central.
    */
    public VertexResult degreeCentrality(boolean normalized) {
        Map<String,Double> map = new TreeMap<String, Double>();
        
        for (String vertex: graph.getVertices()) {
            map.put(vertex, new Double(graph.degree(vertex)));
        }
            
        final double VERTICES = graph.getVertexCount();
        if (normalized && VERTICES>1) {
            multiplyMap(map, 1.0/(VERTICES-1));
        }
        
        return new VertexResult(map);
    }
    
    /**
    Return eigenvector centrality for each vertex. The values are normalized to
    range 0..1 and values close to 1 are more central.
    */
    public VertexResult eigenvectorCentrality() {
        final int VERTICES = graph.getVertexCount();
        final int MAX_ITERATIONS = 100;
        final double EPSILON = 1e-5; /* Eigenvector convergence condition */
        final double EPSILON_SQUARED = EPSILON*EPSILON;
        
        double[][] adjArray = adjacencyMatrix();
        SparseDoubleMatrix2D matrix = new SparseDoubleMatrix2D(adjArray);
        DoubleMatrix1D eigenvector = new DenseDoubleMatrix1D(VERTICES);
        DoubleMatrix1D temp = new DenseDoubleMatrix1D(VERTICES);
        DoubleMatrix1D previous = null;
        
        /*
        Algorithm: power iteration.
        Repeatedly compute e(k+1) = A*e(k), where e(k) is the eigenvector
        at time k and A is the adjacency matrix. e(0) = [1 ... 1].
        Loop MAX_ITERATIONS times or until eigenvectors converge.
        Eigenvectors converge when |e(k+1)-e(k)| < EPSILON.
        Based on Ulrik Brandes, Visual Ranking of Link Structures, 2003, 
        http://www.emis.de/journals/JGAA/accepted/2003/BrandesCornelsen2003.7.2.pdf
        */

        eigenvector.assign(1.0);

        for (int i=0; i<MAX_ITERATIONS; i++) {
            matrix.zMult(eigenvector, temp); /* temp = matrix*eigenvector */
            eigenvector.swap(temp);

            /* Scale eigenvector so that largest value is 1 */ 
            double max = 0;
            for (int j=0; j<VERTICES; j++) {
                if (eigenvector.get(j) > max) max = eigenvector.get(j);
            }
            if (max > 0) {
                final double scale = 1.0 / max;
                for (int j=0; j<VERTICES; j++) {
                    final double prev = eigenvector.get(j);
                    eigenvector.set(j, prev*scale);
                }
            }
            
            if (previous != null) {
                double diff = 0; /* Compute square of |e(k)-e(k-1)| */
                for (int j=0; j<VERTICES; j++) {
                    double delta = eigenvector.get(j) - previous.get(j);
                    diff += delta*delta;
                }
                if (diff < EPSILON_SQUARED) break; /* converged */
            }
            
            previous = eigenvector.copy();
        }
        
        Map<String, Double> result = new TreeMap<String, Double>();
        for (int i=0; i<VERTICES; i++) {
            String vertex = indexer.getKey(i); // TODO: OK?
            result.put(vertex, eigenvector.get(i));
        }

        return new VertexResult(result);
    }
    
    /* correlation */
    
    /**
    Return correlation coefficient between two metric results.
    */
    public static double resultCorrelation(VertexResult r1, VertexResult r2) {
        if (r1 == null) throw new NullPointerException("r1 must not be null");
        if (r2 == null) throw new NullPointerException("r2 must not be null");
        
        DynamicBin1D bin1 = new DynamicBin1D();
        DynamicBin1D bin2 = new DynamicBin1D();
        
        for (String key: r1.getMap().keySet()) {
            bin1.add(r1.getVertexValue(key));
            bin2.add(r2.getVertexValue(key));
        }
        
        return bin1.correlation(bin2);
    }
    
    /**
    Return a new VertexResult instance that has values standardized so that
    mean=0 and stddev=1.
    */
    public static VertexResult standardizeResult(VertexResult result) {
        DynamicBin1D bin = new DynamicBin1D();
        for (String key: result.getMap().keySet()) {
            bin.add(result.getVertexValue(key));
        }
        
        final double mean = bin.mean();
        final double stddev = bin.standardDeviation();
        
        Map<String, Double> newMap = new TreeMap<String, Double>(result.getMap());
        if (stddev != 0) {
            for (Map.Entry<String, Double> entry: newMap.entrySet()) {
                double std = (entry.getValue() - mean) / stddev;
                newMap.put(entry.getKey(), std);
            }
        }
        
        return new VertexResult(newMap);
    }
    
    /* private */
    
    private Map<String, Double> calcCentrality(AbstractRanker<String,String> ranker) {
        ranker.setRemoveRankScoresOnFinalize(false);
        ranker.setNormalizeRankings(false);
        ranker.evaluate();
        
        Map<String, Double> result = new TreeMap<String, Double>();
        for (String vertex: graph.getVertices()) {
            double value = ranker.getVertexRankScore(vertex);
            result.put(vertex, value);
        }
        
        return result;
    }

    private int[] calcDegreeDistribution() {
        Map<Integer, Integer> degreeMap = new TreeMap<Integer, Integer>();
        int maxDegree = 0;
        
        for (String vertex: graph.getVertices()) {
            int degree = graph.degree(vertex);
            if (degree > maxDegree) maxDegree = degree;
            
            int count = 0;
            if (degreeMap.containsKey(degree)) count = degreeMap.get(degree);
            count++;
            degreeMap.put(degree, count);
        }
        
        int[] result = new int[maxDegree+1];
        for (Map.Entry<Integer, Integer> entry: degreeMap.entrySet()) {
            assert(result[entry.getKey()] == 0);
            result[entry.getKey()] = entry.getValue();
        }
        
        return result;
    }
    
    private double[] mapToDistribution(Map<Integer, Integer> map, boolean normalized, double normFactor) {
        final int maxValue = Collections.max(map.keySet());
        
        double[] result = new double[maxValue+1];
        for (Map.Entry<Integer, Integer> entry: map.entrySet()) {
            assert(result[entry.getKey()] == 0);
            double value = normalized ? entry.getValue()*normFactor : entry.getValue();
            result[entry.getKey()] = value;
        }
        return result;
    }
    
    /**
    Multiply each value of the map with factor.
    */
    private void multiplyMap(Map<String, Double> map, double factor) {
        for (Map.Entry<String, Double> entry: map.entrySet()) {
            double normValue = entry.getValue() * factor;
            map.put(entry.getKey(), normValue);
        }
    }
    
}
