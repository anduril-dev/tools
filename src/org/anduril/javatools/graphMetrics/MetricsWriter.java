package org.anduril.javatools.graphMetrics;

import org.anduril.javatools.graphMetrics.VertexResult;

/**
String serializer for GraphMetrics result objects.
*/
public class MetricsWriter {
    
    public static String asString(double d) {
        return (new Double(d)).toString();
    }
    
    /**
    Serialize a list of (vertex, value) pairs.
    Output format: there is one pair on each line. Lines are separated by \n
    newlines.
    <pre>
    LINE ::= vertex-name metric-value NEWLINE
    </pre>
    */
    public static String asString(VertexResult result) {
        StringBuffer sb = new StringBuffer();
        for (VertexResult.Pair pair: result.getList()) {
            sb.append(pair.getKey() + " " + pair.getValue() + "\n");
        }
        return sb.toString();
    }

    /**
    Serialize degree distribution. Output format: there is one degree on each
    line. Lines are separated by \n newlines. There is a line for each degree
    0..M, where M is the maximum degree.
    <pre>
    LINE ::= degree frequency-or-proportion NEWLINE
    </pre>
    */
    public static String distributionAsString(double[] degrees) {
        StringBuffer sb = new StringBuffer();
        for (int degree=0; degree<degrees.length; degree++) {
            sb.append(degree + " " + degrees[degree] + "\n");
        }
        return sb.toString();
    }
    
}
