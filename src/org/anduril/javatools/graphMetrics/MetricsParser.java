package org.anduril.javatools.graphMetrics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.anduril.javatools.graphMetrics.VertexResult;

/**
Parser for string serialization formats outputted by MetricsWriter.
*/
public class MetricsParser {

    private static final String newlinePattern = "\n|\r\n|\r";

    /**
    Parse a string outputted by MetricsWriter.distributionAsString.
    Return an array similar to the array returned by
    GraphMetrics.degreeDistribution.
    
    @throws IllegalArgumentException on parse error.
    */
    public static double[] parseDistribution(String s) {
        List<Double> list = new ArrayList<Double>();

        for (String line: s.split(newlinePattern)) {
            line = line.trim();
            if (line.length() == 0) continue;
            
            String[] tokens = line.split("\\s+", 2);
            if (tokens.length != 2) {
                throw new IllegalArgumentException("Tokenizing parse error on line "+line);
            }
            
            int degree;
            try {
                degree = Integer.parseInt(tokens[0]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Degree parse error on line "+line);
            }
            
            if (degree != list.size()) {
                throw new IllegalArgumentException("Invalid degree on line "+line);
            }
            
            double value;
            try {
                value = Double.parseDouble(tokens[1]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Value parse error on line "+line);
            }
            
            list.add(value);
        }
            
        double[] result = new double[list.size()];
        for (int i=0; i<list.size(); i++) result[i] = list.get(i);
        return result;
    }
    
    public static double parseDouble(String s) {
        return Double.parseDouble(s.trim());
    }
    
    /**
    Parse a string outputted by MetricsWriter.asString(VertexResult).
    
    @throws IllegalArgumentException on parse error.
    */
    public static VertexResult parseVertexResult(String s) {
        Map<String, Double> map = new TreeMap<String, Double>();
        
        for (String line: s.split(newlinePattern)) {
            line = line.trim();
            if (line.length() == 0) continue;
            
            String[] tokens = line.split("\\s+", 2);
            if (tokens.length != 2) {
                throw new IllegalArgumentException("Tokenizing parse error on line "+line);
            }
            
            double value;
            try {
                value = Double.parseDouble(tokens[1]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Value parse error on line "+line);
            }
                
            map.put(tokens[0], value);
        }
        
        return new VertexResult(map);
    }
    
}
