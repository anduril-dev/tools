package org.anduril.javatools.graphMetrics;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;

public class ConvertDirected2Undirected {
	public	static	<TVertex, TEdge> Graph<TVertex,TEdge>	convert(Graph<TVertex,TEdge>	directed)
	{
		if(directed==null)
			return	null;
		Graph<TVertex, TEdge>	underlying=new	UndirectedSparseGraph<TVertex, TEdge>();
		for(TVertex	vertex:directed.getVertices())
		{
			underlying.addVertex(vertex);
		}
		for(TEdge	edge:directed.getEdges())
		{
			underlying.addEdge(edge, directed.getSource(edge), directed.getDest(edge));
		}
		return	underlying;
	}
}
