package org.anduril.javatools.graphMetrics;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.Transformer;
import org.xml.sax.SAXException;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.Hypergraph;
import edu.uci.ics.jung.graph.SparseGraph;
import edu.uci.ics.jung.io.GraphMLMetadata;
import edu.uci.ics.jung.io.GraphMLReader;
import edu.uci.ics.jung.io.GraphMLWriter;
import org.anduril.javatools.pathway.CSBLGraphMLWriter;

/**
Tools for reading and writing GraphML files.
*/
public class GraphML {
    static class EdgeFactory implements Factory<String> {
        private static int counter = 1;
        public String create() {
            return "e"+counter++;
        }
    }
    
    private class VertexTransformer implements Transformer<String, String> {
        private String attribute;
        
        public VertexTransformer(String attribute) {
            this.attribute = attribute;
        }
        
        public String transform(String vertex) {
            return getVertexMetadata(vertex,this.attribute);
        }
    }
    
    private class EdgeTransformer implements Transformer<String, String> {
        private String attribute;
        
        public EdgeTransformer(String attribute) {
            this.attribute = attribute;
        }
        
        public String transform(String edge) {
            return  getEdgeMetadata(edge,this.attribute);
        }
    }
    
    private class GraphTransformer implements Transformer<Hypergraph<String, String>, String> {
        private String attribute;
        
        public GraphTransformer(String attribute) {
            this.attribute = attribute;
        }
        
        public String transform(Hypergraph<String, String>	graph) {
            return getGraphMetadata(this.attribute);
        }
    }
    
    private File file;
    private Graph<String,String> graph;
    private GraphMLReader<Graph<String,String>, String, String> reader;
    
    public GraphML(File file) {
        this.file = file;
    }

    public void loadGraph() throws
            ParserConfigurationException, SAXException, IOException {
        reader = new GraphMLReader<Graph<String,String>, String, String>(null, new EdgeFactory());
        graph = new SparseGraph<String,String>(); 
        reader.load(file.getAbsolutePath(), graph);
    }
    
    public	void	saveGraph(Graph<String,String>	graph,File	target) throws IOException
    {
    	GraphMLWriter<String, String> graphWriter = new CSBLGraphMLWriter<String, String>();
    	
    	for (String attr: reader.getVertexMetadata().keySet()) {
            graphWriter.addVertexData(attr, null, null,
                new VertexTransformer(attr));
        }
        for (String attr: reader.getEdgeMetadata().keySet()) {
            graphWriter.addEdgeData(attr, null, null,
                new EdgeTransformer(attr));
        }
          
        for (String attr: reader.getGraphMetadata().keySet()) {
        	graphWriter.addGraphData(attr, null, null, 
        			new	GraphTransformer(attr));
        }
        
        
        Writer fileWriter = new FileWriter(target);
        graphWriter.save(graph, fileWriter);
    }
    
    public Graph<String,String> getGraph() {
        return this.graph;
    }

    public Set<String> getGraphMetadataNames() {
        return reader.getGraphMetadata().keySet();
    }
    
    public String getGraphMetadata(String metadataName) {
        if (reader == null) return null;
        GraphMLMetadata<Graph<String,String>> metadata = reader.getGraphMetadata().get(metadataName);
        if (metadata == null) return null;
        return metadata.transformer.transform(this.graph);
    }
    
    public Set<String> getVertexMetadataNames() {
        return reader.getVertexMetadata().keySet();
    }
    
    public String getVertexMetadata(String vertex, String metadataName) {
        if (reader == null) return null;
        GraphMLMetadata<String> metadata = reader.getVertexMetadata().get(metadataName);
        if (metadata == null) return null;
        return metadata.transformer.transform(vertex);
    }
    
    public Set<String> getEdgeMetadataNames() {
        return reader.getEdgeMetadata().keySet();
    }
    
    public String getEdgeMetadata(String edge, String metadataName) {
        if (reader == null) return null;
        GraphMLMetadata<String> metadata = reader.getEdgeMetadata().get(metadataName);
        if (metadata == null) return null;
        return metadata.transformer.transform(edge);
    }
}
