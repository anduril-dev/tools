package org.anduril.javatools.graphMetrics;

import java.util.LinkedList;

import edu.uci.ics.jung.algorithms.shortestpath.UnweightedShortestPath;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.graph.util.Pair;

public class SplitGraph {
	static	private	<TVertex, TEdge> LinkedList<Graph<TVertex,TEdge>>	split(Graph<TVertex,TEdge>	graph,
			UndirectedSparseGraph<TVertex,String>	relation)
	{
		LinkedList<Graph<TVertex,TEdge>>	graphs=new	LinkedList<Graph<TVertex,TEdge>>();
		LinkedList<TVertex>	unexploredVerticies=new	LinkedList<TVertex>(graph.getVertices());
		while(!unexploredVerticies.isEmpty()){
			try {
				Graph<TVertex,TEdge>	subGraph=(Graph<TVertex,TEdge>) graph.getClass().newInstance();
				TVertex	seed=unexploredVerticies.iterator().next();
				subGraph.addVertex(seed);
				for(TVertex	vertex:unexploredVerticies){
					if(relation.isNeighbor(seed, vertex))
						subGraph.addVertex(vertex);
				}
				for(TEdge	edge:graph.getEdges())
				{
					Pair<TVertex>	pair=graph.getEndpoints(edge);
					if(subGraph.containsVertex(pair.getFirst())&&
							subGraph.containsVertex(pair.getSecond()))
					{
						subGraph.addEdge(edge, pair.getFirst(), pair.getSecond(),graph.getEdgeType(edge));
					}
						
					
				}
				graphs.add(subGraph);
				unexploredVerticies.removeAll(subGraph.getVertices());
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return graphs;
	}
	
	static	public	<TVertex, TEdge> LinkedList<Graph<TVertex,TEdge>>	conComponents(Graph<TVertex,TEdge>	graph)
	{
		UndirectedSparseGraph<TVertex,String>	relation=new	UndirectedSparseGraph<TVertex,String>();
		for(TVertex	v:graph.getVertices())
			relation.addVertex(v);
		for(TVertex	v1:graph.getVertices())
			for(TVertex	v2:graph.getVertices())
				if(graph.isNeighbor(v1, v2))
					relation.addEdge(v1+" - "+v2, v1, v2);
		for(TVertex	v1:relation.getVertices())
			for(TVertex	v2:relation.getVertices())
				for(TVertex	v3:relation.getVertices())
				{
					if(relation.isNeighbor(v1, v2)&&relation.isNeighbor(v2, v3)&&
							!relation.isNeighbor(v1, v3))
						relation.addEdge(v1+" - "+v3, v1, v3);
				}
		return	split(graph,relation);
	}
	
	static	public	<TVertex, TEdge> LinkedList<Graph<TVertex,TEdge>>	strongConComponents(Graph<TVertex,TEdge>	graph)
	{
		UnweightedShortestPath<TVertex,TEdge>  pathes = new UnweightedShortestPath<TVertex,TEdge>(graph);// Search for paths between all graph nodes
		UndirectedSparseGraph<TVertex,String>	relation=new	UndirectedSparseGraph<TVertex,String>();
		for(TVertex	v:graph.getVertices())
			relation.addVertex(v);
		for(TVertex	v1:graph.getVertices())
			for(TVertex	v2:graph.getVertices())
				if((pathes.getDistance(v1, v2)!=null)&&(pathes.getDistance(v2, v1)!=null))
					relation.addEdge(v1+" - "+v2, v1, v2);
		return	split(graph,relation);
	}
}
