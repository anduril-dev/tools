package org.anduril.javatools.graphMetrics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import edu.uci.ics.jung.graph.Graph;

import org.anduril.javatools.graphMetrics.GraphML;
import org.anduril.javatools.graphMetrics.GraphMetrics;

/**
Print statistics on undirected, unweighted graphs loaded from
a text file.
*/
public class ReportPrinter {
    
    /**
    Print given number of top values from the vertex result list.
    */
    public static void printTop(Formatter fmt, int top, VertexResult result, boolean ascending) {
        VertexResult standardized = GraphMetrics.standardizeResult(result);
        
        int i = 0;
        for (VertexResult.Pair pair: result.getList(ascending)) {
            fmt.format("%-15s %.3f (std: %.2f)%n",
                pair.getKey(), pair.getValue(), standardized.getVertexValue(pair.getKey()));
            i++;
            if (i >= top) break;
        }
    }
    
    public static void printDistribution(Formatter fmt, double[] distribution, double[] normDistribution) {
        for (int value=0; value<distribution.length; value++) {
            if (distribution[value] == 0) continue;
            fmt.format("%2d: %2.0f (%.3f)%n", value,
                distribution[value], normDistribution[value]);
        }
    }
    
    /**
    Print text-formatted report.
    */
    public static void printReport(Graph<String,String> graph) {
        GraphMetrics gm = new GraphMetrics(graph);
        Formatter fmt = new Formatter(System.out);
        boolean isConnected = gm.isConnected();

        fmt.format("Vertices: %d%n", graph.getVertexCount());
        fmt.format("Edges: %d%n", graph.getEdgeCount());
        fmt.format("Diameter: %.2f%n", gm.diameter());
        fmt.format("Char path length: %.2f%n", gm.charasteristicPathLength());
        fmt.format("Avg degree: %.2f%n", gm.avgDegree());
        fmt.format("Avg clustering coeff: %.2f%n", gm.avgClusteringCoefficient());
        fmt.format("Is connected: %s%n", isConnected);

        fmt.format("%nDegree distribution\n");
        printDistribution(fmt, gm.degreeDistribution(false),
            gm.degreeDistribution(true));

        if (isConnected) {
            fmt.format("%nPath length distribution\n");
            printDistribution(fmt, gm.pathLengthDistribution(false),
                gm.pathLengthDistribution(true));
        }
        
        final boolean NORMALIZED = false;
        final int TOP = 20;
        VertexResult degreeResult = gm.degreeCentrality(NORMALIZED);
        VertexResult closenessResult = gm.closenessCentrality(NORMALIZED);
        VertexResult betweennessResult = gm.betweennessCentrality(NORMALIZED);
        VertexResult eigenvectorResult = gm.eigenvectorCentrality();
        
        fmt.format("%nDegree centrality%n");
        printTop(fmt, TOP, degreeResult, false);

        fmt.format("%nCloseness centrality%n");
        printTop(fmt, TOP, closenessResult, false);

        fmt.format("%nBetweenness centrality%n");
        printTop(fmt, TOP, betweennessResult, false);

        fmt.format("%nEigenvector centrality%n");
        printTop(fmt, TOP, eigenvectorResult, false);
        
        fmt.format("%nCentrality correlation coefficients%n");
        fmt.format("Degree      - closeness:   %.2f%n",
            GraphMetrics.resultCorrelation(degreeResult, closenessResult));
        fmt.format("Degree      - betweenness: %.2f%n",
            GraphMetrics.resultCorrelation(degreeResult, betweennessResult));
        fmt.format("Degree      - eigenvector: %.2f%n",
            GraphMetrics.resultCorrelation(degreeResult, eigenvectorResult));
        fmt.format("Closeness   - betweenness: %.2f%n",
            GraphMetrics.resultCorrelation(closenessResult, betweennessResult));
        fmt.format("Closeness   - eigenvector: %.2f%n",
            GraphMetrics.resultCorrelation(closenessResult, eigenvectorResult));
        fmt.format("Betweenness - eigenvector: %.2f%n",
            GraphMetrics.resultCorrelation(betweennessResult, eigenvectorResult));
    }
    
    /**
    Print metrics on each node in tab separated file format. 
    */
    public static void printCSV(Graph<String,String> graph) {
        GraphMetrics gm = new GraphMetrics(graph);
        Formatter fmt = new Formatter(Locale.ENGLISH);
        
        String[] columns = {
            "id", "degree", "clustering_coeff", "degree_centrality",
            "closeness_centrality", "betweenness_centrality", "eigenvector_centrality"
        };
        
        /* Print header */
        for (int i=0; i<columns.length; i++) {
            if (i!=0) fmt.format("\t");
            fmt.format("\"%s\"", columns[i]);
        }
        fmt.format("%n");
        
        VertexResult clusterCoeffs = gm.clusteringCoefficients();
        VertexResult degreeResult = gm.degreeCentrality(true);
        VertexResult closenessResult = gm.closenessCentrality(true);
        VertexResult betweennessResult = gm.betweennessCentrality(true);
        VertexResult eigenvectorResult = gm.eigenvectorCentrality();
        
        for (String vertex: graph.getVertices()) {
            fmt.format("\"%s\"\t", vertex.toString());
            fmt.format("%d\t", graph.degree(vertex));
            fmt.format("%.5f\t", clusterCoeffs.getVertexValue(vertex));
            fmt.format("%.5f\t", degreeResult.getVertexValue(vertex));
            fmt.format("%.5f\t", closenessResult.getVertexValue(vertex));
            fmt.format("%.5f\t", betweennessResult.getVertexValue(vertex));
            fmt.format("%.5f", eigenvectorResult.getVertexValue(vertex));
            fmt.format("%n");
        }
        
        System.out.println(fmt.toString());
    }

    public static void main(String[] args)
        throws FileNotFoundException, IOException, ParserConfigurationException, SAXException
    {
        if (args.length == 0) {
            System.out.println("Usage: ReportPrinter [--csv] <graph-filename.xml>");
            System.out.println(" --csv: print tab separated file (otherwise, print text file");
            System.out.println("Input file format: GraphML.");
            return;
        }
        
        boolean csv = false;
        if (args[0].equals("--csv")) {
            csv = true;
            args = Arrays.copyOfRange(args, 1, args.length);
        }
        
        for (String filename: args) {
            GraphML reader = new GraphML(new File(filename));
            reader.loadGraph();
            Graph<String,String> graph = reader.getGraph();
            if (csv) printCSV(graph);
            else printReport(graph);
        }
    }
    
}
