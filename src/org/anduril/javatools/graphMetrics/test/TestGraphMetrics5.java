package org.anduril.javatools.graphMetrics.test;

import org.anduril.javatools.graphMetrics.test.TestGraphMetricsBase;

/*
Adjacency matrix
0 1 1 1
1 0 1 1 
1 1 0 1
1 1 1 0

v  degree
1  3
2  3
3  3
4  3

Shortest paths
  1 2 3 4  sum
  ------------
1 0 1 1 1  3
2 1 0 1 1  3
3 1 1 0 1  3
4 1 1 1 0  3

Clustering coeff
CC = 2*edges/degree(degree-1)
v  degree   edges   CC
1  3        3       1
2  3        3       1
3  3        3       1
4  3        3       1

Betweenness
1: 3-1-4 (not 3-2-4) => 0.5
2,3,4: same
*/

public class TestGraphMetrics5 extends TestGraphMetricsBase {
    
    private static final int[][] edges = new int[][] {
        {1, 2},
        {1, 3},
        {1, 4},
        {2, 3},
        {2, 4},
        {3, 4}
    };
    
    private static final Double[] degreeDistribution = {
        0.0,
        0.0,
        0.0,
        1.0
    };
    
    private static Double[] closeness = { 3.0, 3.0, 3.0, 3.0 };
    
    private static Double[] clusteringCoeffs = { 1.0, 1.0, 1.0, 1.0 };
    
    private static Double[] betweenness = { 0.0, 0.0, 0.0, 0.0 };
    
    private static Double[] degreeCentrality = { 3.0, 3.0, 3.0, 3.0 };
    
    private static Double[] eigenvector = { 1.0, 1.0, 1.0, 1.0 };

    public TestGraphMetrics5() {
        super(4, edges, 1.0, 1.0, 3.0, degreeDistribution,
            clusteringCoeffs, closeness, betweenness, degreeCentrality, eigenvector);
    }

}
