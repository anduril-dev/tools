package org.anduril.javatools.graphMetrics.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import org.anduril.javatools.graphMetrics.MetricsParser;
import org.anduril.javatools.graphMetrics.MetricsWriter;
import org.anduril.javatools.graphMetrics.VertexResult;
import org.anduril.javatools.graphMetrics.test.TestGraphMetricsBase;

public class TestMetricsParser {
    
    private void genericTestDistribution(double[] distribution) {
        String s = MetricsWriter.distributionAsString(distribution);
        double[] parsedArray = MetricsParser.parseDistribution(s);
        TestGraphMetricsBase.assertDoubleArray(distribution, parsedArray);
    }

    @Test public void testDistribution1() {
        genericTestDistribution(new double[] { });
    }
    
    @Test public void testDistribution2() {
        genericTestDistribution(new double[] { 0.0 });
    }

    @Test public void testDistribution3() {
        genericTestDistribution(
            new double[] { 0.0, 0.1, 1.5, Double.POSITIVE_INFINITY, 1e10 }
        );
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testDistributionInvalid1() {
        String s = "0 1.0\n1 xxx";
        MetricsParser.parseDistribution(s);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDistributionInvalid2() {
        String s = "0";
        MetricsParser.parseDistribution(s);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDistributionInvalid3() {
        String s = "0 0.5\n2 0.5";
        MetricsParser.parseDistribution(s);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDistributionInvalid4() {
        String s = "V1 0.5";
        MetricsParser.parseDistribution(s);
    }
    
    @Test public void testVertexResult() {
        Map<String, Double> map = new HashMap<String, Double>();
        map.put("1", 1.0);
        map.put("2", -557.441);
        map.put("abc", 1e-9);
        map.put("AAAAA", 999999999999.9);
        map.put("zzz-zzz", Double.POSITIVE_INFINITY);
        
        VertexResult vresult = new VertexResult(map);
        String s = MetricsWriter.asString(vresult);
        
        Map<String, Double> parsedMap = MetricsParser.parseVertexResult(s).getMap();
        Assert.assertTrue(map.equals(parsedMap));
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testVertexResultInvalid1() {
        String s = "V1 1.0\nV2 xxx";
        MetricsParser.parseVertexResult(s);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testVertexResultInvalid2() {
        String s = "V1";
        MetricsParser.parseVertexResult(s);
    }
    
}

