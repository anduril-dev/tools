package org.anduril.javatools.graphMetrics.test;

import org.anduril.javatools.graphMetrics.test.TestGraphMetricsBase;

public class TestGraphMetrics3 extends TestGraphMetricsBase {
    
    private static final int[][] edges = new int[][] {	};
    
    private static final Double[] degreeDistribution = { 1.0 };
    
    private static Double[] clusteringCoeffs = {
        0.0, 0.0, 0.0
    };
    
    public TestGraphMetrics3() {
        super(3, edges, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY,
            0.0, degreeDistribution, clusteringCoeffs, null, null, null, null);
    }
    
}
