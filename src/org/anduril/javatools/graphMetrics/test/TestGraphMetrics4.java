package org.anduril.javatools.graphMetrics.test;

import org.anduril.javatools.graphMetrics.test.TestGraphMetricsBase;

/*
Adjacency matrix
0 0 1 1
0 0 1 1 
1 1 0 0
1 1 0 0

v  degree
1  2
2  2
3  2
4  2

  1 2 3 4  sum
  ------------
1 0 2 1 1  4
2 2 0 1 1  4
3 1 1 0 2  4
4 1 1 2 0  4

Clustering coeff
CC = 2*edges/degree(degree-1)
v  degree   edges   CC
1  2
2  2
3  2
4  2

Betweenness
1: 3-1-4 (not 3-2-4) => 0.5
2,3,4: same
*/

public class TestGraphMetrics4 extends TestGraphMetricsBase {
    
    private static final int[][] edges = new int[][] {
        {1, 3},
        {1, 4},
        {2, 3},
        {2, 4}
    };
    
    private static final Double[] degreeDistribution = {
        0.0,
        0.0,
        1.0
    };
    
    private static Double[] closeness = { 4.0, 4.0, 4.0, 4.0 };
    
    private static Double[] clusteringCoeffs = { 0.0, 0.0, 0.0, 0.0 };
    
    private static Double[] betweenness = { 0.5, 0.5, 0.5, 0.5 };
    
    private static Double[] eigenvector = { 1.0, 1.0, 1.0, 1.0 };

    public TestGraphMetrics4() {
        super(4, edges, 2.0, 4.0/3, 2.0, degreeDistribution,
            clusteringCoeffs, closeness, betweenness, null, eigenvector);
    }

}
