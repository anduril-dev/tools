package org.anduril.javatools.graphMetrics.test;

import org.anduril.javatools.graphMetrics.test.TestGraphMetricsBase;

/*
From
http://www.sscnet.ucla.edu/soc/faculty/mcfarland/soc112/cent-ans.htm

n  degree
1  1
2  1
3  3
4  2
5  3
6  2
7  2

Adjacency matrix
0 0 1 0 0 0 0
0 0 1 0 0 0 0
1 1 0 1 0 0 0
0 0 1 0 1 0 0
0 0 0 1 0 1 1
0 0 0 0 1 0 1
0 0 0 0 1 1 0

avg degree = 14/7 = 2

Shortest paths
  1 2 3 4 5 6 7  sum
--------------------
1 0 2 1 2 3 4 4  16
2 2 0 1 2 3 4 4  16
3 1 1 0 1 2 3 3  11
4 2 2 1 0 1 2 2  10
5 3 3 2 1 0 1 1  11
6 4 4 3 2 1 0 1  15
7 4 4 3 2 1 1 0  15

Char. path length = 94/(7*6) = 2.2380952380952381

Clustering coeff
CC = 2*edges/degree(degree-1)
v  degree   edges   CC
1  1        -       0
2  1        -       0
3  3        0       0
4  2        0       0
5  3        1       0.33333333333333333
6  2        1       1
7  2        1       1

Betweenness
3: 1-2 1-4 1-5 1-6 1-7 2-4 2-5 2-6 2-7 = 9
4: 1-5 1-6 1-7 2-5 2-6 2-7 3-5 3-6 3-7 = 9
5: 1-6 1-7 2-6 2-7 3-6 3-7 4-6 4-7 = 8
*/

public class TestGraphMetrics2 extends TestGraphMetricsBase {
    
    private static final int[][] edges = new int[][] {
        {1, 3},
        {2, 3},
        {3, 4},
        {4, 5},
        {5, 6},
        {5, 7},
        {6, 7}
    };
    
    private static final Double[] degreeDistribution = {
        0.0,
        2/7.0,
        3/7.0,
        2/7.0
    };
    
    private static Double[] clusteringCoeffs = {
        0.0,
        0.0,
        0.0,
        0.0,
        0.333333333333333333,
        1.0,
        1.0
    };
    
    private static Double[] betweenness = {
        0.0,
        0.0,
        9.0,
        9.0,
        8.0,
        0.0,
        0.0
    };
    
    private static Double[] closeness = { 16.0, 16.0, 11.0, 10.0, 11.0, 15.0, 15.0 };
    
    /* Computed using Python / numpy.linalg.
    Corresponding eigenvalue is 2.25327161. */
    private static Double[] eigenvector = {
        0.21364919, 0.21364919, 0.48140966, 0.65744833, 1.0, 0.79791164, 0.79791164
    };

    public TestGraphMetrics2() {
        super(7, edges, 4.0, 2.2380952380952381, 2.0,
            degreeDistribution, clusteringCoeffs, closeness, betweenness, null, eigenvector);
    }
    
}
