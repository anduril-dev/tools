package org.anduril.javatools.graphMetrics.test;

import org.anduril.javatools.graphMetrics.test.TestGraphMetricsBase;

/*
Adjacency matrix

  1 2 3 4 5 6 7 8 9
-------------------
1 0 1 1 1 0 0 0 0 0
2 1 0 0 1 0 0 0 0 0
3 1 0 0 1 0 0 0 0 0
4 1 1 1 0 1 1 1 1 0
5 0 0 0 1 0 0 1 0 0
6 0 0 0 1 0 0 1 0 0
7 0 0 0 1 1 1 0 1 0
8 0 0 0 1 0 0 1 0 1
9 0 0 0 0 0 0 0 1 0

Edges: 3+1+1+4+1+1+1+1= 13

v  Degree
1  3
2  2
3  2
4  7
5  2
6  2
7  4
8  3
9  1

Degree distribution
d  n
0  0
1  1   
2  4
3  2
4  1
5  0
6  0
7  1

avg degree: 26/9 = 2.88888888888

Shortest paths & closeness

  1 2 3 4 5 6 7 8 9  sum
------------------------
1 0 1 1 1 2 2 2 2 3  14  
2 1 0 2 1 2 2 2 2 3  15
3 1 2 0 1 2 2 2 2 3  15
4 1 1 1 0 1 1 1 1 2  9
5 2 2 2 1 0 2 1 2 3  15
6 2 2 2 1 2 0 1 2 3  15
7 2 2 2 1 1 1 0 1 2  12
8 2 2 2 1 2 2 1 0 1  13
9 3 3 3 2 3 3 2 1 0  20

diameter: 3
char path length: 128/(9*8) = 1.777777777777777777 

Clustering coeffs

CC = 2*edges/degree(degree-1)
v  degree   edges   CC
1  3        2       0.6666666666
2  2        1       1
3  2        1       1
4  7        5       0.23809523809523808
5  2        1       1
6  2        1       1
7  4        3       0.5
8  3        1       0.33333333333333333
9  1        0       0

Betweenness
1: 2-3 (not 2-4-3) => 0.5
2: 0
3: 0
4: 1-5 1-6 1-7 1-8 1-9 => 5
   2-3 (not 2-1-3) 2-5 2-6 2-7 2-8 2-9 => 5.5
   3-5 3-6 3-7 3-8 3-9 => 5
   5-6 (not 5-7-6) 5-8 (not 5-7-8) 5-9 (not 5-7-8-9) => 1.5
   6-8 (not 6-7-8) 6-9 (not 6-7-8-9) => 1
   total: 18
5: 0
6: 0
7: 5-8 (not 5-4-8) 5-9 (not 5-4-8-9) 6-5 (not 6-4-5) 6-8 (not 6-4-8) 6-9 (not 6-4-8-9) => 2.5
8: 1-9 2-9 3-9 4-9 5-9 6-9 7-9 => 7
*/

public class TestGraphMetrics1 extends TestGraphMetricsBase {
    
    private static final int[][] edges = new int[][] {
        {1, 2},
        {1, 3},
        {1, 4},
        {2, 4},
        {3, 4},
        {4, 5},
        {4, 6},
        {4, 7},
        {4, 8},
        {5, 7},
        {6, 7},
        {7, 8},
        {8, 9}
    };
    
    private static final Double[] degreeDistribution = {
        0.0,
        1/9.0,
        4/9.0,
        2/9.0,
        1/9.0,
        0/9.0,
        0/9.0,
        1/9.0
    };
    
    private static Double[] clusteringCoeffs = {
        0.6666666666,
        1.0,
        1.0,
        0.23809523809523808,
        1.0,
        1.0,
        0.5,
        0.33333333333333333,
        0.0
    };
    
    private static Double[] closeness = { 14.0, 15.0, 15.0, 9.0, 15.0, 15.0, 12.0, 13.0, 20.0 };
    
    private static Double[] betweenness = {
        0.5,
        0.0,
        0.0,
        18.0,
        0.0,
        0.0,
        2.5,
        7.0,
        0.0
    };
    
    private static Double[] degreeCentrality = {
        3.0,
        2.0,
        2.0,
        7.0,
        2.0,
        2.0,
        4.0,
        3.0,
        1.0
    };

    /* Computed using Python / numpy.linalg. */
    private static Double[] eigenvalues = {
        0.523044152581,
        0.428829631841,
        0.428829631841,
        1.0,
        0.477712318321,
        0.477712318321,
        0.696657364629,
        0.518844506086,
        0.14608630889		
    };

    public TestGraphMetrics1() {
        super(9, edges, 3.0, 1.777777777777777, 2.88888888888,
            degreeDistribution, clusteringCoeffs, closeness, betweenness,
            degreeCentrality, eigenvalues);
    }
    
}
