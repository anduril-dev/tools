package org.anduril.javatools.graphMetrics.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseGraph;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.anduril.javatools.graphMetrics.GraphMetrics;

public abstract class TestGraphMetricsBase {
    
    protected Graph<String,String> graph;
    protected GraphMetrics metrics;
    
    private int numVertices;
    private int[][] edges;
    
    /* Expected values for metrics follow. */
    
    private Double diameter;
    private Double charPathLength;
    
    private Double avgDegree;
    private Double[] degreeDistribution;
    
    private Double[] clusteringCoeffs;
    private Double[] closeness;
    private Double[] betweenness;
    private Double[] eigenvector;
    private Double[] degreeCentrality;
    
    public TestGraphMetricsBase(int numVertices, int[][] edges,
        Double diameter, Double charPathLength,
        Double avgDegree, Double[] degreeDistribution,
        Double[] clusteringCoeffs,
        Double[] closeness,
        Double[] betweenness,
        Double[] degreeCentrality,
        Double[] eigenvector)
    {
        if (numVertices < 1) throw new IllegalArgumentException("numVertices < 1");
        this.numVertices = numVertices;
        this.edges = edges;
        this.diameter = diameter;
        this.charPathLength = charPathLength;
        this.avgDegree = avgDegree;
        this.degreeDistribution = degreeDistribution;
        this.clusteringCoeffs = clusteringCoeffs;
        this.closeness = closeness;
        this.betweenness = betweenness;
        this.degreeCentrality = degreeCentrality;
        this.eigenvector = eigenvector;
    }

    @Before public void setUp() {
        this.graph = new SparseGraph<String, String>();
        List<String> vertices = makeAddVertices(numVertices);
        addEdges(vertices, edges);
        this.metrics = new GraphMetrics(graph);
    }
    
    public static void assertDouble(Double expected, double actual) {
        if (expected == null) return;
        if (expected == actual) return;
        
        final double epsilon = 1e-5;
        Assert.assertTrue("Expected "+expected+", got "+actual,
            Math.abs(expected-actual) < epsilon);
    }
    
    public static void assertDoubleArray(double[] expected, double[] actual) {
        if (expected == null) return;
        
        if (expected.length != actual.length) {
            Assert.fail("Arrays are of different size");
        }
        for (int i=0; i<expected.length; i++) {
            assertDouble(expected[i], actual[i]);
        }
    }

    public static void assertDoubleArray(Double[] expected, double[] actual) {
        if (expected == null) return;
        
        if (expected.length != actual.length) {
            Assert.fail("Arrays are of different size");
        }
        for (int i=0; i<expected.length; i++) {
            if (expected[i] != null) assertDouble(expected[i], actual[i]);
        }
    }
    
    @Test public void testDiameter() {
        double actual = metrics.diameter();
        
        if (metrics.isConnected()) {
            Assert.assertTrue("Graph is connected but diameter is INF", actual != Double.POSITIVE_INFINITY);
            Assert.assertTrue("Diameter is >= VERTICES: " + actual, actual < graph.getVertexCount());
            assertDouble(diameter, actual);
        }
        else {
            Assert.assertTrue("Graph is disconnected but diameter is finite: "+actual,
                actual == Double.POSITIVE_INFINITY);
        }
    }
    
    @Test public void testCharasteristicPathLength() {
        double actual = metrics.charasteristicPathLength();
        
        if (metrics.isConnected()) {
            Assert.assertTrue("Graph is connected but CPL is INF", actual != Double.POSITIVE_INFINITY);
            Assert.assertTrue("CPL is >= VERTICES: " + actual, actual < graph.getVertexCount());
            assertDouble(charPathLength, actual);
        }
        else {
            Assert.assertTrue("Graph is disconnected but CPL is finite", actual == Double.POSITIVE_INFINITY);
        }
    }

    @Test public void testAvgDegree() {
        double actual = metrics.avgDegree();
        Assert.assertTrue("actual >= VERTICES", actual < graph.getVertexCount());
        assertDouble(avgDegree, actual);
    }
    
    @Test public void testDegreeDistribution() {
        double[] actual = metrics.degreeDistribution();
        assertValuesBetween(actual, 0, 1);
        
        double sum = 0;
        for (double x: actual) sum += x;
        assertDouble(1.0, sum);
        
        assertDoubleArray(degreeDistribution, actual);
    }
    
    @Test public void testPathLengthDistribution() {
        double[] actual = metrics.pathLengthDistribution(true);
        if (actual == null) {
            if (metrics.isConnected()) {
                Assert.fail("pathLengthDistribution returned null but graph is connected");
            }
        }
        else {
            assertValuesBetween(actual, 0, 1);
            double sum = 0;
            for (double x: actual) sum += x;
            assertDouble(1.0, sum);
        }
    }

    @Test public void testClusteringCoeff() {
        Map<String, Double> map = metrics.clusteringCoefficients().getMap();
        for (Map.Entry<String, Double> entry: map.entrySet()) {
            int node = Integer.valueOf(entry.getKey());
            double coeff = entry.getValue();
            Assert.assertTrue(coeff >= 0);
            Assert.assertTrue(coeff <= 1);
            if (clusteringCoeffs != null) {
                assertDouble(clusteringCoeffs[node-1], coeff);
            }
        }
    }
    
    @Test public void testClosenessUnnormalized() {
        Map<String, Double> map = metrics.closenessCentrality(false).getMap();
        if (closeness != null) {
            for (Map.Entry<String, Double> entry: map.entrySet()) {
                int node = Integer.valueOf(entry.getKey());
                assertDouble(closeness[node-1], entry.getValue());
            }
        }
    }
    
    @Test public void testClosenessNormalized() {
        Map<String, Double> map = metrics.closenessCentrality(true).getMap();
        assertValuesBetween(map, 0, 1);
    }

    @Test public void testBetweennessUnnormalized() {
        Map<String, Double> map = metrics.betweennessCentrality(false).getMap();
        if (betweenness != null) {
            for (Map.Entry<String, Double> entry: map.entrySet()) {
                int node = Integer.valueOf(entry.getKey());
                assertDouble(betweenness[node-1], entry.getValue());
            }
        }
    }

    @Test public void testBetweennessNormalized() {
        Map<String, Double> map = metrics.betweennessCentrality(true).getMap();
        assertValuesBetween(map, 0, 1);
        
        final double N = map.size();
        if (N < 3) return;
        
        final double factor = 1.0 / ( (N-1)*(N-2) );
        for (Map.Entry<String, Double> entry: map.entrySet()) {
            int node = Integer.valueOf(entry.getKey());
            if (betweenness != null && betweenness[node-1] != null) {
                assertDouble(betweenness[node-1]*factor, entry.getValue());
            }
        }
    }
    
    @Test public void testEigenvector() {
        Map<String, Double> map = metrics.eigenvectorCentrality().getMap();
        assertValuesBetween(map, 0, 1);
        
        if (eigenvector != null) {
            for (Map.Entry<String, Double> entry: map.entrySet()) {
                int node = Integer.valueOf(entry.getKey());
                assertDouble(eigenvector[node-1], entry.getValue());
            }
        }
    }

    @Test public void testDegreeCentralityUnnormalized() {
        Map<String, Double> map = metrics.degreeCentrality(false).getMap();
        
        if (degreeCentrality != null) {
            for (Map.Entry<String, Double> entry: map.entrySet()) {
                int node = Integer.valueOf(entry.getKey());
                assertDouble(degreeCentrality[node-1], entry.getValue());
            }
        }
    }

    @Test public void testDegreeCentralityNormalized() {
        Map<String, Double> map = metrics.degreeCentrality(true).getMap();
        assertValuesBetween(map, 0, 1);
        
        final int N = map.size();
        if (N < 2) return;
        
        final double factor = 1.0 / (N-1.0);
        for (Map.Entry<String, Double> entry: map.entrySet()) {
            int node = Integer.valueOf(entry.getKey());
            if (degreeCentrality != null) {
                assertDouble(degreeCentrality[node-1]*factor, entry.getValue());
            }
        }
    }
    
    /* private */
    
    private List<String> makeAddVertices(int n) {
        List<String> list = new ArrayList<String>();
        for (int i=1; i<n+1; i++) {
            String vertex = ""+i;
            graph.addVertex(vertex);
            list.add(vertex);
        }
        return list;
    }
    
    private void addEdges(List<String> vertices, int[][] edges) {
        for (int i=0; i<edges.length; i++) {
            int from = edges[i][0] - 1;
            int to = edges[i][1] - 1;
            if (from == to) {
                throw new IllegalArgumentException("Tried to add self-edge");
            }
            graph.addEdge(from+"-"+to, vertices.get(from), vertices.get(to));
        }
    }
    
    private void assertValuesBetween(double[] array, double min, double max) {
        for (double value: array) {
            if (value == Double.POSITIVE_INFINITY) continue;
            Assert.assertTrue("Expected value between "+min+" and "+max+", got "+value,
                min <= value && value <= max);
        }
    }
    
    private void assertValuesBetween(Map<String, Double> map, double min, double max) {
        for (Double value: map.values()) {
            if (value == null || value == Double.POSITIVE_INFINITY) continue;
            Assert.assertTrue("Expected value between "+min+" and "+max+", got "+value,
                min <= value && value <= max);
        }
    }
}
