package org.anduril.javatools.graphMetrics.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import org.anduril.javatools.graphMetrics.test.TestGraphMetricsBase;

/**
Generate a number of random binomial graphs and run sanity checks on them.
*/
@RunWith(Parameterized.class)
public class TestGraphMetricsRandom extends TestGraphMetricsBase {
    
    private static final int NUM_GRAPHS = 300;
    
    /**
    Generate random graphs that are used as parameters for the constructor.
    */
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        /* Array of { numVertices, edges[][] } */
        ArrayList<Object[]> params = new ArrayList<Object[]>();
        Random rand = new Random();

        for (int g=0; g<NUM_GRAPHS; g++) {
            final int numVertices = 2 + rand.nextInt(30);
            final double EDGE_PROB = 0.01 + rand.nextDouble() % 0.3;
            
            ArrayList<Integer[]> edges = new ArrayList<Integer[]>();

            for (int v1=1; v1<numVertices+1; v1++) {
                for (int v2=v1; v2<numVertices+1; v2++) {
                    if (v1 != v2 && rand.nextFloat() < EDGE_PROB) {
                        edges.add(new Integer[] { v1, v2 } );
                    }
                }
            }

            int[][] edgeArray = new int[edges.size()][2];
            for (int i=0; i<edges.size(); i++) {
                edgeArray[i][0] = edges.get(i)[0];
                edgeArray[i][1] = edges.get(i)[1];
            }
            params.add(new Object[] { numVertices, edgeArray } );
        }
        
        return params;
    }
    
    public TestGraphMetricsRandom(int numVertices, int[][] edges) {
        super(numVertices, edges, null, null, null, null,
            null, null, null, null, null);
    }
}
