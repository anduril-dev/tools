package org.anduril.javatools.graphMetrics.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.runner.JUnitCore;

public class Test {

    public static void main(String[] args) throws Exception {
        List<String> classes = new ArrayList<String>();
        classes.add(TestGraphMetrics1.class.getCanonicalName());
        classes.add(TestGraphMetrics2.class.getCanonicalName());
        classes.add(TestGraphMetrics3.class.getCanonicalName());
        classes.add(TestGraphMetrics4.class.getCanonicalName());
        classes.add(TestGraphMetrics5.class.getCanonicalName());
        classes.add(TestGraphMetricsRandom.class.getCanonicalName());
        classes.add(TestMetricsParser.class.getCanonicalName());
        JUnitCore.main(classes.toArray(new String[] {}));
    }
}
