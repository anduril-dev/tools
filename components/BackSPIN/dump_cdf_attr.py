#!/usr/bin/env python

# This script is useful for debugging 
# It will dump the marginals from the CEF files

import backspinpy

import numpy as np
import pandas as pd

if __name__ == '__main__':
	import sys 

	try:
		_, filename, which = sys.argv
	except ValueError:
		sys.stderr.write('Usage: {} filename {{rows | cols}}'.format(sys.argv[0]) + '\n')
		sys.exit(1)

	input_cef = backspinpy.CEF_obj()
 	input_cef.readCEF(filename)

	if   which == 'rows':
		names = input_cef.row_attr_names
		values = input_cef.row_attr_values
	elif which == 'cols':
		names = input_cef.col_attr_names
		values = input_cef.col_attr_values
	else:
		sys.stderr.write('warning: should specify ''rows'' or ''cols''' + '\n')
		sys.exit(1)

	data = pd.DataFrame(values, index = names).T
	data.to_csv(sys.stdout, sep = '\t', index = False)
