
# Anduril launcher for BackSPIN modified from the original bin/backspin script
# from the git commit 2fbcf5d5d5. The original copyright is produced verbatim
# below.

# Copyright (c) 2015, Amit Zeisel, Gioele La Manno and Sten Linnarsson
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# This .py file can be used as a library or a command-line version of BackSPIN, 
# This version of BackSPIN was implemented by Gioele La Manno.
# The BackSPIN biclustering algorithm was developed by Amit Zeisel and is described
# in Zeisel et al. Cell types in the mouse cortex and hippocampus revealed by 
# single-cell RNA-seq Science 2015 (PMID: 25700174, doi: 10.1126/science.aaa1934). 

from __future__ import division
from numpy import *
import getopt
import sys
import os
from backspinpy import SPIN, backSPIN, fit_CV, feature_selection
from backspinpy import CEF_obj

from anduril.args import * 
import numpy as np, pandas as pd

if True:
    print("")

    # NB. argument parsing thrown away... we import the Anduril options instead

    # seed the prng
    random.seed(seed)
    # normalize spin axis flag
    normal_spin_axis = {0: 0, 1: 1, "genes": 0, "cells": 1, "both": "both"}[normal_spin_axis]

    if True:
        if verbose:
            print ('Loading file.')
        input_csv = pd.read_csv(input_in, sep = '\t', header = 0, index_col = 0)
        data = np.array(input_csv.values)

        if feature_fit:
            if verbose:
                print ("Performing feature selection")
            ix_features = feature_selection(data, feature_genes, verbose=verbose)
            if verbose:
                print ("Selected %i genes" % len(ix_features))
            data = data[ix_features, :]
            input_csv = input_csv.ix[ix_features, :]

    if preprocess:
        data = log2(data+1)
        data = data - data.mean(1)[:,newaxis]
    if data.shape[0] <= 3 and data.shape[1] <= 3:
        print ('Input file is not correctly formatted.\n')
        sys.exit(1)

    def make_fmt(values):
        if any(type(i) == float for i in values):
            fmt ='%.6g'
        else:
            fmt = '%i'

    if normal_spin == False:

        print ('backSPIN started\n----------------\n')
        print ('numLevels: %i\nfirst_run_iters: %i\nfirst_run_step: %.3f\nruns_iters: %i\nruns_step: %.3f\nsplit_limit_g: %i\nsplit_limit_c: %i\nstop_const: %.3f\nlow_thrs: %.3f\n' % (numLevels, first_run_iters, first_run_step, runs_iters,\
            runs_step, split_limit_g, split_limit_c, stop_const, low_thrs))


        results = backSPIN(data, numLevels, first_run_iters, first_run_step, runs_iters, runs_step,\
            split_limit_g, split_limit_c, stop_const, low_thrs, verbose)

        sys.stdout.flush()
        print ('\nWriting output.\n')

        def make_clusters(levels, row_names):
            col_names = ['Level_%i_group' % level for level, _ in enumerate(levels.T)]
            result = pd.DataFrame(levels, columns = col_names, index = row_names)
            return result

        row_clusters = make_clusters(results.genes_gr_level, input_csv.index[results.genes_order])
        row_clusters.to_csv(output_rowClusts, sep = '\t', float_format = '%i')

        col_clusters = make_clusters(results.cells_gr_level, input_csv.columns[results.cells_order])
        col_clusters.to_csv(output_colClusts, sep = '\t', float_format = '%i')

        row_perm, col_perm = results.genes_order, results.cells_order

    else:

        print ('normal SPIN started\n----------------\n')

        results = SPIN(data, widlist=runs_step, iters=runs_iters, axis=normal_spin_axis, verbose=verbose)

        print ('\nWriting output.\n')

        if normal_spin_axis == 'both':
            row_perm, col_perm = results[0], results[1]

        if normal_spin_axis == 0:
            row_perm, col_perm = results, None

        if normal_spin_axis == 1:
            row_perm, col_perm = None, results

    # copy permuted input data to output
    output_csv = input_csv
    if not row_perm is None:
        output_csv = output_csv.ix[row_perm, :]
    if not col_perm is None:
        output_csv = output_csv.ix[:, col_perm]

    output_csv.to_csv(output_permutedInput, sep = '\t', float_fmt = make_fmt(output_csv))
