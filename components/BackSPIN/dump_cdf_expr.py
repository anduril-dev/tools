#!/usr/bin/env python

# This script is useful for debugging 
# It will dump the main values from the CEF files

import backspinpy

import numpy as np
import pandas as pd

if __name__ == '__main__':
	import sys 

	try:
		_, filename, row_attr, col_attr = sys.argv
	except ValueError:
		sys.stderr.write('Usage: {} filename row_attr col_attr'.format(sys.argv[0]) + '\n')
		sys.exit(1)

	input_cef = backspinpy.CEF_obj()
 	input_cef.readCEF(filename)

	data = pd.DataFrame(np.array(input_cef.matrix),
		index   = input_cef.row_attr_values[input_cef.row_attr_names.index(row_attr)],
		columns = input_cef.col_attr_values[input_cef.col_attr_names.index(col_attr)]
			)
	data.to_csv(sys.stdout, sep = '\t')
