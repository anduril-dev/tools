#!/bin/bash

set -e

. $ANDURIL_HOME/lang/bash/generic.sh || {
	echo "\$ANDURIL_HOME not set?" >&2
	exit 1
}

path=$(dirname "$0")
backspin_path="$path/BackSPIN"

iscmd git python || {
	echo "requires: git python" >&2
	exit 1
}

shell() {
	echo "$@" >&2
	"$@"
}

# install/update BackSPIN
if [ ! -e "$backspin_path" ]; then
	# clone backspin
	shell mkdir -p "$backspin_path"
	shell git clone 'https://github.com/linnarsson-lab/BackSPIN' "$backspin_path"
	# build 
	(cd "$backspin_path" && shell python setup.py build)
	# link to here so we can import it
	shell ln -s "$backspin_path/build/lib."*"/backspinpy" "$path/backspinpy"
else
	# pull & update 
	(cd "$backspin_path" && shell git pull && shell python setup.py build)
fi

# install test case
shell python "$path/dump_cdf_expr.py" "$backspin_path/oligos.cef" Gene CellID >"$path/testcases/case-example/input/in.csv"
