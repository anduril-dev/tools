#!/bin/bash

if [[ $1 == "" ]]; then
    dpkg -s libboost-python-dev libboost-dev python-dev libboost-regex-dev > /dev/null
    if [[ $? != 0 ]] ; then
       read -p "A required package not found! Install? (y/n)" -n 1 -r
       echo    # (optional) move to a new line
       if [[ $REPLY =~ ^[Yy]$ ]] ; then
          echo "sudo apt-get install libboost-python-dev libboost-dev python-dev libboost-regex-dev"
          sudo apt-get install libboost-python-dev libboost-dev python-dev libboost-regex-dev
       fi
    fi
fi

set -e
set -x

g++ $(pkg-config --cflags python) -fPIC -O3 -c -o partial.o partial.cpp
g++ -shared -Wl,-soname,"partial.so" partial.o -lboost_python -lboost_regex -fPIC -o partial.so

rm partial.o

echo "Running test snippet."

PYTHONPATH=.:$PYTHONPATH python <( echo '

import partial

print partial.match("12", "123")
print partial.match("AB", "")
print partial.match("12")
print partial.match("A1234-1234-1234-1234", ".*")

')

