#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import sys
sys.path.append(".")

import glob2csv

if __name__=="__main__":
    options = anduril.Struct(
            pattern = param_pattern,
            slashless = param_slashless,
            col_filename = param_filenameColumn,
            root = input_in,
            csv = output_out,
            tracks = []
    )
    if not glob2csv.main(options):
        sys.exit(1)
    sys.exit(0)

