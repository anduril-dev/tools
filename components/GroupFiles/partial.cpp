
#include <boost/regex.hpp>

int is_possible_card_number(const std::string& input, std::string pat=std::string(""))
{
   static boost::regex e(".*");
   if (pat.length() != 0) {
	e = boost::regex(pat);
   }
   boost::match_results<std::string::const_iterator> what;
   if(0 == boost::regex_match(input, what, e, boost::match_default | boost::match_partial))
   {
	return -1;
   }

   if(what[0].matched)
   {
      return 0;
   }
   return 1;
}

int is_possible_card_number_def(const std::string& input) {
   return is_possible_card_number(input, std::string(""));
}

// can be exposed to Python by writing a Boost.Python wrapper:
#include <boost/python.hpp>
BOOST_PYTHON_MODULE(partial)
{
    using namespace boost::python;
    def("match", is_possible_card_number);
    def("match", is_possible_card_number_def);
}

