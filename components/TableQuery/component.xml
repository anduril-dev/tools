<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>TableQuery</name>
    <version>1.3</version>
    <doc>
    Executes an SQL query on CSV tables and creates a result table. TableQuery
    uses <a target="_blank" href="http://hsqldb.org/">HSQLDB 2.3.1</a> (default),
    <a target="_blank" href="http://www.h2database.com">H2 1.3.173</a> or <a
    target="_blank" href="http://www.sqlite.org/">SQLite 3.7.2</a> for executing
    the query. Consequently, the syntax of the query is defined by the selected
    SQL engine. The input script may include multiple SQL statements delimited
    with '--&lt;statement break&gt;--' strings and the results of the last
    statement are returned.
    
    Each defined input table is inserted into a temporary in-memory (default) or
    on-disk database. The name of the SQL table corresponds to the name of the
    input file, e.g. table1 or the key in the array. If you have a column named
    MyColumn in table1, you can refer to it using table1."MyColumn" in the SQL
    query. The result SQL relation is written to the output table. You can
    define names of output columns using SELECT ... AS "MyName".
    
    Note: currently, you should surround all column names with
    quotes (e.g. table1."MyColumn").
    
    HSQLDB can use static Java methods as stored procedures.
    Some stored procedures are provided in the class Functions
    supplied with the component. For example,
    "Functions.date"(2009, 12, 31) in the query creates an
    SQL date instance from three numeric values (date, month, day).
    See test case 7 for an example.
    </doc>
    <author email="kristian.ovaska@helsinki.fi">Kristian Ovaska</author>
    <author email="Marko.Laakso@Helsinki.FI">Marko Laakso</author>
    <category>Convert</category>
    <category>Filter</category>
    <launcher type="java">
        <argument name="class" value="TableQuery" />
        <argument name="source" value="TableQuery.java" />
        <argument name="source" value="Functions.java" />
    </launcher>
    <requires type="jar" URL="http://hsqldb.org/">hsqldb.jar</requires>
    <requires type="jar" URL="http://www.h2database.com">h2-1.3.173.jar</requires>
    <requires type="jar" URL="https://bitbucket.org/xerial/sqlite-jdbc">sqlite-jdbc-3.7.2.jar</requires>
    <inputs>
        <input name="table1" type="CSV" optional="true">
            <doc>CSV table 1. The table is referred to as 'table1' in
            the SQL query.</doc>
        </input>
        <input name="table2" type="CSV" optional="true">
            <doc>CSV table 2. The table is referred to as 'table2' in
            the SQL query.</doc>
        </input>
        <input name="table3" type="CSV" optional="true">
            <doc>CSV table 3. The table is referred to as 'table3' in
            the SQL query.</doc>
        </input>
        <input name="table4" type="CSV" optional="true">
            <doc>CSV table 4. The table is referred to as 'table4' in
            the SQL query.</doc>
        </input>
        <input name="table5" type="CSV" optional="true">
            <doc>CSV table 5. The table is referred to as 'table5' in
            the SQL query.</doc>
        </input>
        <input name="table6" type="CSV" optional="true">
            <doc>CSV table 6. The table is referred to as 'table6' in
            the SQL query.</doc>
        </input>
        <input name="table7" type="CSV" optional="true">
            <doc>CSV table 7. The table is referred to as 'table7' in
            the SQL query.</doc>
        </input>
        <input name="table8" type="CSV" optional="true">
            <doc>CSV table 8. The table is referred to as 'table8' in
            the SQL query.</doc>
        </input>
        <input name="table9" type="CSV" optional="true">
            <doc>CSV table 9. The table is referred to as 'table9' in
            the SQL query.</doc>
        </input>
        <input name="table10" type="CSV" optional="true">
            <doc>CSV table 10. The table is referred to as 'table10' in
            the SQL query.</doc>
        </input>
        <input name="table11" type="CSV" optional="true">
            <doc>CSV table 11. The table is referred to as 'table11' in
            the SQL query.</doc>
        </input>
        <input name="table12" type="CSV" optional="true">
            <doc>CSV table 12. The table is referred to as 'table12' in
            the SQL query.</doc>
        </input>
        <input name="table13" type="CSV" optional="true">
            <doc>CSV table 13. The table is referred to as 'table13' in
            the SQL query.</doc>
        </input>
        <input name="table14" type="CSV" optional="true">
            <doc>CSV table 14. The table is referred to as 'table14' in
            the SQL query.</doc>
        </input>
        <input name="table15" type="CSV" optional="true">
            <doc>CSV table 15. The table is referred to as 'table15' in
            the SQL query.</doc>
        </input>
        <input name="tables" type="CSV" array="true" optional="true">
            <doc>List of relations each referred by its key.</doc>
        </input>
        <input name="queryFile" type="SQL" optional="true">
            <doc>SQL query. Either this file or the query parameter
            must be provided, but not both.</doc>
        </input>
        <input name="columnTypes" type="CSV" optional="true">
            <doc>Contains SQL types for individual columns. If the file
            is not provided, the type is inferred from the contents of
            the columns. This can be used to force the use of VARCHAR
            for values that are also valid numerics. The file contains
            the columns Table (refers to one of table1 to table15), Column
            (refers to a column name in the table), Type (contains an
            SQL type). A row with Table='result', Column=X and Type='STRING'
            forces the use string values for result column X.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="table" type="CSV">
            <doc>Result table that contains the rows from the query.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="query" type="string" default="">
            <doc>SQL query. Either this parameter or the query input
            must be provided, but not both.</doc>
        </parameter>
        <parameter name="numIndices" type="string" default="">
            <doc>Comma-separated list of index counts for input tables.
            All indices are single-column indices, running from column
            1 to N, where N is retrieved from this parameter. If empty,
            the default number (1) is used. For example, ",2,,2" sets
            two indices for table2 and table4 and one index for the
            rest.</doc>
        </parameter>
        <parameter name="engine" type="string" default="hsqldb">
            <doc>Database engine. Legal values: hsqldb, h2, sqlite.</doc> 
        </parameter>
        <parameter name="memoryDB" type="boolean" default="true">
            <doc>If true, the temporary database is stored in memory
            for fast access. If false, it is stored on disk to allow
            processing large data sets.</doc>
        </parameter>
    </parameters>
</component>
