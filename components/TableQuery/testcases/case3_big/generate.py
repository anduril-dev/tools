import sys
import random

sys.stdout.write('"K1"\t"K2"\t"A1"\t"A2"\n')

ROWS = 500000

for i in range(1, ROWS+1):
    s = '"a%d"\t"b%d"\t%d\t"x%d"\n' % (i, i, random.randint(1, ROWS), random.randint(1, ROWS))
    sys.stdout.write(s)
