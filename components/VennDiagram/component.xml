<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>VennDiagram</name>
    <version>1.1.2</version>
    <doc>
    Draws Venn diagrams showing the intersections of the given sets of  
    identifiers. The required R-package can be downloaded 
    <a target="_blank" href="https://r-forge.r-project.org/projects/vennerable/">here</a>.
    You can draw up to four Venn diagrams with 2 to 9 sets 
    in each. The diagrams are displayed as subfigures in one figure.

    You can refer to the diagrams created by the component in the LaTex
    document. The figure containing all the diagrams is labeled 
    'fig:instanceName' and the subfigures for individual diagrams are labeled 
    'fig:instanceName-i', where instanceName is the name of the variable used
    for the component in the network definition file and i is 1,2,3 or 4 
    referring to the specific Venn diagram.

    If there are NA or duplicated values in the sets they are removed.
    
    An example output:    
    <br/><br/>
    <img src="VennDiagramExample.png"/>    
    </doc>    
    <author email="erkka.valo@helsinki.fi">Erkka Valo</author>
    <author email="kristian.ovaska@helsinki.fi">Kristian Ovaska</author>
    <category>Plot</category>
    <launcher type="R">
        <argument name="file" value="VennDiagram.r" />
    </launcher>
    <requires URL="http://www.r-project.org/" type="manual">R</requires>
    <requires type="R-package" URL="https://r-forge.r-project.org/projects/vennerable/">Vennerable</requires>
    <requires name="installer" optional="false">
        <resource type="bash">install.sh</resource>
    </requires>
    <inputs>
        <input name="in" type="SetList">
            <doc>Sets containing identifiers.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="report" type="Latex">
            <doc>Latex subsection containing the Venn diagrams.</doc>
        </output>
        <output name="out" type="SetList">
            <doc>Sets visualized in the Venn diagrams.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="sets1" type="string">
            <doc>A comma-separated list of minimum of 2 and maximum of
            9 sets identified by the set IDs for 
            which the Venn diagram is drawn.</doc>
        </parameter>
        <parameter name="sets2" type="string" default="">
            <doc>Sets for the second Venn diagram.</doc>
        </parameter>
        <parameter name="sets3" type="string" default="">
            <doc>Sets for the third Venn diagram.</doc>
        </parameter>
        <parameter name="sets4" type="string" default="">
            <doc>Sets for the fourth Venn diagram.</doc>
        </parameter>
        <parameter name="names1" type="string" default="">
            <doc>Names of the sets to be used in the first Venn diagram. 
             Names are given in a comma-separated list and mapped to
             sets in <code>sets1</code> positionally. If non are given,
             the set ID is used as set name.</doc>
        </parameter>
        <parameter name="names2" type="string" default="">
            <doc>Names of the sets for the second Venn diagram.</doc>
        </parameter>
        <parameter name="names3" type="string" default="">
            <doc>Names of the sets for the third Venn diagram.</doc>
        </parameter>
        <parameter name="names4" type="string" default="">
            <doc>Names of the sets for the forth Venn diagram.</doc>
        </parameter>
        <parameter name="sectionTitle" type="string" default="">
            <doc>Title for a new latex section. If non is given, a new section is not created.</doc>
        </parameter>
        <parameter name="sectionType" type="string" default="section">
            <doc>Type of LaTeX section: usually one of section, subsection or subsubsection.
            No section statement is written if sectionTitle is empty.</doc>
        </parameter>
        <parameter name="cexSetName" type="float" default="0.5">
            <doc>A numerical value giving the amount by which the set names
                 in the plot should be scaled relative to the default values.</doc>
        </parameter>
         <parameter name="cexSetSize" type="float" default="0.5">
            <doc>A numerical value giving the amount by which the sizes of the
                 sets in the plot should be scaled relative to the default 
                 values.</doc>
        </parameter>
        <parameter name="colorAlgorithm" type="string" default="">
            <doc>The algorithm used to fill in the colors to Venn diagram.
                 Possible values are "signature", "binary" and "sequential".
                 The default empty string omits the coloring.</doc>
        </parameter>
        <parameter name="types" type="string" default="circles">
            <doc>A comma separated list of types for the Venn diagrams. The
                 first is used with the diagram of <code>sets1</code> and so 
                 on. If there are less types than diagrams to be drawn, the
                 values are recycled. The type defines  the shapes of the objects
                 used to represent the sets in the Venn diagrams. Possible 
                 types are "circles", "squares", "triangles", "ellipses", 
                 "AWFE", "ChowRuskey" and "battle". Note that only a subset 
                 of shapes is supported for a diagram with particular number
                 of sets.</doc>
        </parameter>
        <parameter name="doWeights" type="boolean" default="true">
            <doc>If true, the size of the sets in the figure are weighted
                 by the cardinality of the sets. However, this is not 
                 always possible depending on the number of the sets in
                 the diagram and the <code>type</code> parameter. If false,
                 all the sets are represented with the same sized objects.</doc>
        </parameter>
        <parameter name="increasingLineWidth" type="boolean" default="false">
            <doc>If true, each set is given a different linewidth, with the 
                 last to be plotted given the thinnest width, to help in 
                 visualising nonsimple Venn diagrams.</doc>
        </parameter>
    </parameters>
</component>
