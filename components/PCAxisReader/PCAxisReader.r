library('componentSkeleton')

execute <- function(cf) {
  library('pxR')

  # Read input.
  data <- read.px(get.input(cf, 'in'))

  param.codes  <- get.parameter(cf, 'codes', type='boolean')
  param.fail   <- get.parameter(cf, 'fail',  type='boolean')
  param.direct <- get.parameter(cf, 'mode',  type='boolean')
  param.direct <- if (param.direct) { 'long' } else { 'wide' }

  # Write output.
  CSV.write(get.output(cf, 'out'),
            as.data.frame(data,
                          use.codes          = param.codes,
                          warnings.as.errors = param.fail,
                          direction          = param.direct))
  return(0)
}
main(execute)
