#!/bin/bash

# Get Anduril functions for bash
. "$ANDURIL_HOME/lang/bash/generic.sh" || {
	echo "\$ANDURIL_HOME may not be set!" >&2
	exit 1
}

# Get our path
path="$(dirname "$0")"

# Stop if already installed
jars='GlassComponents Sandbox VorteX'
installed=y
for jar in $jars; do
	[ -e "$path/../../lib/java/xshift-$jar.jar" ] || installed=n
done
[ "$installed" = y ] && exit 0

# Settings
URL='https://github.com/nolanlab/vortex/releases/download/Oct4-2016/X-shift.Standalone.Oct-4-2016.zip'

# Check for external commands
for cmd in 'wget' 'unzip'; do
	iscmd "$cmd" || exit 1
done

# Download X-shift 
zipfile="$(mktemp)"
echo "Downloading \"$(basename "$URL")\".. " >&2
wget "$URL" -qO "$zipfile" || {
	echo 'download failed!' >&2
	exit 1
}

# Extract relevant files from the .zip
trap "rm -f $zipfile" 0 1 9 15
set -e
for jar in $jars; do
	echo "Extracting \"$jar.jar\".. " >&2
	unzip -p "$zipfile" "lib/$jar.jar" >"$path/../../lib/java/xshift-$jar.jar"
done

# Unlink downloaded file
rm "$zipfile"
