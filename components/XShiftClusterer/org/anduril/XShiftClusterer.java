
package org.anduril.xshift;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;

// Miscellaneous classes from the X-shift jars
import clustering.AngularDistance;
import clustering.Cluster;
import clustering.ClusterMember;
import clustering.ClusterSet;
import clustering.Datapoint;
import clustering.Dataset;
import clustering.DistanceMeasure;
import clustering.EuclideanDistance;
import java.sql.SQLException;
import util.LinePlusExponent;
import vortex.clustering.XShiftClustering;

/**
 * An X-shift clustering component for Anduril.
 * @author Antti Hakkinen
 * @version 1.0
 */
public class XShiftClusterer extends SkeletonComponent {

	// Reads data from a CSV into X-shift's internal representation
	private static Dataset readDatasetFromCSV(CSVParser reader, String[] colnames, int[] colindices) {
		// Samples will be gathered here
		ArrayList<Datapoint> samples = new ArrayList<Datapoint>();

		// Read data
		while (reader.hasNext()) {
			// Get fields
			String[] fields = reader.next();
			// Convert 
			double[] values = new double[colindices.length];
			for (int i = 0; i < colindices.length; ++i)
				values[i] = Double.parseDouble(fields[colindices[i]]);

			// Push
			samples.add(new Datapoint("dataset1", values, values, samples.size()));
		}

		Datapoint[] points = new Datapoint[samples.size()];
		return new Dataset("", samples.toArray(points), colnames, colnames);
	}

	// Reads all columns of a CSV with a header
	public static Dataset readDatasetFromHeaderedCSV(CSVParser reader) {
		// Get column names
		String[] colnames = reader.getColumnNames();

		// Get indices {1,...,n}
		int[] colindices = new int[colnames.length];
		for (int i = 0; i < colnames.length; ++i)
			colindices[i] = i;

		// Read data
		return readDatasetFromCSV(reader, colnames, colindices);
	}

	// Reads select columns of a CSV with a header
	public static Dataset readDatasetFromHeaderedCSV(CSVParser reader, String[] colnames) {
		// Map names to indices
		int[] colindices = new int[colnames.length];
		for (int i = 0; i < colnames.length; ++i)
			colindices[i] = reader.getColumnIndex(colnames[i]);

		// Read data
		return readDatasetFromCSV(reader, colnames, colindices);
	}

	// Splits a single line of CSV
	private static String[] splitCSVLine(String line, String delim) throws IOException {
		return new CSVParser(new StringReader(line), delim, 0, CSVParser.DEFAULT_MISSING_VALUE_SYMBOL, false).getColumnNames();
	}

	// Generates a sequence of integers from "from" to "to", incrementing by "by"
	private static Integer[] integerSequence(int from, int to, int by) {
		// Create output array
		int count = (to - from) / by + 1;
		if (count < 0)
			count = 0;
		Integer values[] = new Integer[count];

		// Populate array
		if (count > 0)
		{
			values[0] = from;
			for (int i = 1; i < count; ++i)
				values[i] = values[i - 1] + by;
		}

		return values;
	}

	// Select optimal set of clusters 
	public static int selectClusters(ClusterSet[] clusters) {
		// Get parameter vs number of clusters
		double[] numClusters = new double[clusters.length];
		double[] params = new double[clusters.length];

		for (int i = 0; i < clusters.length; ++i) {
			numClusters[i] = clusters[i].getNumberOfClusters();
			params[i] = clusters[i].getMainClusteringParameterValue();
		}

		// Find elbow point
		double bestParam = LinePlusExponent.findElbowPointLinePlusExp(numClusters, params);

		// Get index of the cluster with the best parameter value
		for (int i = 0; i < clusters.length; ++i)
			if (params[i] == bestParam) 
				return i;

		return -1;
	}

	// Get labels for a cluster set
	public static int[] getLabels(Cluster[] clusters, int samples) {
		// Create a label array
		int[] labels = new int[samples];

		// Eat SQL exceptions--we shouldn't get them
		try {
			// Loop clusters
			for (int i = 0; i < clusters.length; ++i) {
				// Loop members
				ClusterMember[] members = clusters[i].getClusterMembers();
				for (int j = 0; j < members.length; ++j)
					// Record label
					labels[members[j].getDatapoint().getID()] = i;
			}
		}
		catch (SQLException error) {
			return null;
		}

		return labels;
	}

	// Anduril run callback
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// Get parameters
		String columns = cf.getParameter("columns");
		String distance = cf.getParameter("distance");
		int kFrom = cf.getIntParameter("kFrom");
		int kTo = cf.getIntParameter("kTo");
		int kStep = cf.getIntParameter("kStep");
		int n = cf.getIntParameter("n");

		// Open input file
		CSVParser inCSV = new CSVParser(cf.getInput("in"), "\t", 0);

		// Read data
		Dataset input;
		try {
			input = !columns.isEmpty() ? 
				readDatasetFromHeaderedCSV(inCSV, splitCSVLine(columns, ",")) :
					readDatasetFromHeaderedCSV(inCSV);
		} 
		catch (IOException error)
		{
			// I/O error
			cf.writeError(error.getMessage());
			return ErrorCode.INPUT_IO_ERROR;
		}
		catch (Exception error)
		{
			// Missing/non-numeric fields etc.
			cf.writeError(error.getMessage());
			return ErrorCode.INVALID_INPUT;
		}

		// Get distance measure
		DistanceMeasure distInstance;
		if (distance.compareTo("euclidean") == 0)
			distInstance = new EuclideanDistance();
		else if (distance.compareTo("angular") == 0)
			distInstance = new AngularDistance();
		else
		{
			// Invalid distance
			cf.writeError("the parameter 'distance' must be \"euclidean\" or \"angular\"");
			return ErrorCode.PARAMETER_ERROR;
		}

		// Create a list of K values
		Integer[] kvals = integerSequence(kFrom, kTo, kStep);
		if (kvals.length < 5)
		{
			// Error out
			cf.writeError("the parameters 'kFrom', 'kTo', and 'kStep' must generate at least 5 values");
			return ErrorCode.PARAMETER_ERROR;
		}

		// Run clustering
		XShiftClustering xs = new XShiftClustering(distInstance);

		// Disable saving in internal database-- we don't have it
		xs.setSave(false);

		// Apply parameters
		xs.setK(kvals);
		xs.setUseVMF(Boolean.FALSE);
		xs.setnSize(n);

		// Cluster & select best
		ClusterSet[] css = xs.doBatchClustering(input, "comment1");
		int csidx = selectClusters(css);

		// Get labels
		int[] labels = getLabels(css[csidx].getClusters(), input.size());

		// Write output 
		try {
			// Open file
			CSVWriter outCSV = new CSVWriter(new String[]{ "label" }, cf.getOutput("out"), "\t", false);
			
			// Write labels
			for (int i = 0; i < labels.length; ++i)
				outCSV.write(labels[i] + 1); // NOTE: adjust for 1-based indices

			// Close
			outCSV.close();
		}
		catch (IOException error) {
			// I/O error
			cf.writeError(error.getMessage());
			return ErrorCode.OUTPUT_IO_ERROR;
		}

		return ErrorCode.OK;
	} // runImpl

	public static void main(String[] argv) {
		new XShiftClusterer().run(argv);
	}

} // XShiftClusterer
