import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.nfunk.jep.JEP;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommandI;

public class Functions {
    /**
     * Keeps track of the current wildcard expansion for "*"
     * in iterated transformations. Must be set to null after
     * iteration stops.
     */
    public static String wildcard = null;
    
    public static void addFunctions(JEP jep, Map<String, Set<String>> sets) {
        addFunction(jep, new AllNamesFunction(sets));
        addFunction(jep, new MatchFunction(sets));
        addFunction(jep, new ReplaceFunction(sets));
        addFunction(jep, new SetFunction());
        addFunction(jep, new SetMatchFunction(sets));
        addFunction(jep, new ExpandFunction(sets));
        addFunction(jep, new UnionFunction(sets));
        addFunction(jep, new IntersectionFunction(sets));
        addFunction(jep, new DiffFunction(sets));
        addFunction(jep, new FrequencyFunction(FrequencyFunction.NAME, sets));
        addFunction(jep, new MinFrequencyFunction(sets));
        addFunction(jep, new NamesFunction(sets));
    }
    
    private static void addFunction(JEP jep, BaseFunction function) {
        jep.addFunction(function.getName(), function);
    }
    
    public static final String replaceWildcard(String value) {
        if (Functions.wildcard == null) return value;
        else return value.replace("*", Functions.wildcard);
    }
    
    public static class SetCollection extends ArrayList<Set<String>> {
        private static final long serialVersionUID = -2133649657137672586L;
    }
    
    public static abstract class BaseFunction implements PostfixMathCommandI {
        private final String name;
        private final int numArguments;
        private int curNumArguments;
        private final Map<String, Set<String>> sets;
        
        public BaseFunction(String name, int numArguments, Map<String, Set<String>> sets) {
            this.name = name;
            this.numArguments = numArguments;
            this.curNumArguments = numArguments;
            this.sets = sets;
        }
        
        @Override
        public boolean checkNumberOfParameters(int numArgs) {
            if (this.numArguments < 0) return true;
            else return (this.numArguments == numArgs);
        }

        @Override
        public int getNumberOfParameters() {
            return this.numArguments;
        }

        @Override
        public abstract void run(Stack stack) throws ParseException;
        
        @Override
        public void setCurNumberOfParameters(int numArgs) {
            this.curNumArguments = numArgs;
        }

        public final int getCurNumArguments(int stackSize) {
            return this.curNumArguments < 0 ? stackSize : curNumArguments;
        }
        
        public final String getName() {
            return this.name;
        }
        
        public final Map<String, Set<String>> getSets() {
            return this.sets;
        }
        
        @SuppressWarnings("unchecked")
        public final Set<String> resolveSet(Object obj) throws ParseException {
            if (obj instanceof Set) {
                return (Set<String>)obj;
            } else if (obj instanceof String) { 
                String name = replaceWildcard((String)obj);
                Set<String> set = getSets().get(name);
                if (set == null) {
                    String msg = String.format("%s: Set %s is not found",
                            getName(), name);
                    throw new ParseException(msg);
                }
                return set;
            } else {
                String msg = String.format("%s: Argument os not a set: %s", getName(), obj);
                throw new ParseException(msg);
            }
        }
    }

    public static class AllNamesFunction extends BaseFunction {
        public static final String NAME = "allnames";
        
        public AllNamesFunction(Map<String, Set<String>> sets) {
            super(NAME, 0, sets);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void run(Stack stack) throws ParseException {
            stack.push(new TreeSet<String>(getSets().keySet()));
        }
    }

    public static class MatchFunction extends BaseFunction {
        public static final String NAME = "match";
        
        public MatchFunction(Map<String, Set<String>> sets) {
            super(NAME, 2, sets);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void run(Stack stack) throws ParseException {
            if (getCurNumArguments(stack.size()) != 2) {
                throw new ParseException("Number of arguments must be 2");
            }
            
            Object arg2 = stack.pop();
            Object arg1 = stack.pop();
            
            if (!(arg1 instanceof String)) {
                String msg = String.format("%s: Argument 1 must be a string; is %s", getName(), arg1);
                throw new ParseException(msg);
            }
            
            Pattern pattern = Pattern.compile((String)arg1);
            Set<String> set = resolveSet(arg2);
            Set<String> result = new TreeSet<String>();
            for (String value: set) {
                if (pattern.matcher(value).matches()) result.add(value);
            }
            stack.push(result);
        }
    }
    
    public static class ReplaceFunction extends BaseFunction {
        public static final String NAME = "replace";
        
        public ReplaceFunction(Map<String, Set<String>> sets) {
            super(NAME, 3, sets);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void run(Stack stack) throws ParseException {
            if (getCurNumArguments(stack.size()) != 3) {
                throw new ParseException("Number of arguments must be 3");
            }
            
            Object arg3 = stack.pop();
            Object arg2 = stack.pop();
            Object arg1 = stack.pop();
            
            if (!(arg1 instanceof String)) {
                String msg = String.format("%s: Argument 1 must be a string; is %s", getName(), arg1);
                throw new ParseException(msg);
            }
            if (!(arg2 instanceof String)) {
                String msg = String.format("%s: Argument 2 must be a string; is %s", getName(), arg2);
                throw new ParseException(msg);
            }
            
            String pat = (String)arg1;
            /* ".*" should be "^.*$", otherwise replacing is done twice (Sun JVM 1.6) */
            if (pat.equals(".*") || pat.equals("(.*)")) pat = "^"+pat+"$";
            
            final Pattern pattern = Pattern.compile(pat);
            final String replace = (String)arg2;
            final Set<String> set = resolveSet(arg3);
            Set<String> result = new TreeSet<String>();
            
            for (String value: set) {
                String newValue = pattern.matcher(value).replaceAll(replace);
                result.add(newValue);
            }
            stack.push(result);
        }
    }
    
    public static class SetFunction extends BaseFunction {
        public static final String NAME = "set";
        
        public SetFunction() {
            super(NAME, -1, null);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void run(Stack stack) throws ParseException {
            int numArgs = getCurNumArguments(stack.size());

            Set<String> result = new TreeSet<String>();
            for (int i=0; i<numArgs; i++) {
                result.add(stack.pop().toString());
            }
            stack.push(result);
        }
    }
    
    public static class SetMatchFunction extends BaseFunction {
        public static final String NAME = "setmatch";
        
        public SetMatchFunction(Map<String, Set<String>> sets) {
            super(NAME, 1, sets);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void run(Stack stack) throws ParseException {
            if (getCurNumArguments(stack.size()) != 1) {
                throw new ParseException("Number of arguments must be 1");
            }
            
            final Object arg = stack.pop();
            
            if (arg instanceof String) {
                Pattern pattern = Pattern.compile((String)arg);
                SetCollection sets = new SetCollection();
                boolean found = false;
                for (Map.Entry<String, Set<String>> entry: getSets().entrySet()) {
                    if (pattern.matcher(entry.getKey()).matches()) {
                        sets.add(entry.getValue());
                        found = true;
                    }
                }
                if (!found) {
                    String msg = String.format("Regular expression '%s' does not match any set ID",
                            pattern.pattern());
                    throw new ParseException(msg);
                }
                stack.push(sets);
            } else {
                String msg = String.format("%s: Invalid argument: %s", getName(), arg);
                throw new ParseException(msg);
            }
        }
    }
    
    public static class ExpandFunction extends BaseFunction {
        public static final String NAME = "expand";
        public ExpandFunction(Map<String, Set<String>> sets) {
            super(NAME, 1, sets);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void run(Stack stack) throws ParseException {
            int numArgs = getCurNumArguments(stack.size());
            SetCollection coll = new SetCollection();
            
            for (int i=0; i<numArgs; i++) {
                Object obj = stack.pop();
                Set<String> arg = resolveSet(obj);
                for (String name: arg) {
                    Set<String> set = getSets().get(name);
                    if (set == null) {
                        throw new ParseException("Set not found: "+name);    
                    }
                    coll.add(set);
                }
            }
            
            stack.push(coll);
        }
    }
    
    public static abstract class SetOperationFunction extends BaseFunction {
        public SetOperationFunction(String name, int numArguments, Map<String, Set<String>> sets) {
            super(name, numArguments, sets);
        }
        
        @SuppressWarnings("unchecked")
        @Override
        public void run(Stack stack) throws ParseException {
            int numArgs = getCurNumArguments(stack.size());
            
            List<Set<String>> args = new ArrayList<Set<String>>();
            List<Double> numericArgs = new ArrayList<Double>();
            
            for (int i=0; i<numArgs; i++) {
                Object obj = stack.pop();
                if (obj instanceof Set || obj instanceof String) {
                    args.add(resolveSet(obj));
                } else if (obj instanceof SetCollection) {
                    SetCollection coll = (SetCollection)obj;
                    Collections.reverse(coll);
                    args.addAll(coll);
                } else if (obj instanceof Double) {
                    numericArgs.add((Double)obj);
                } else {
                    String msg = String.format("%s: Invalid argument: %s", getName(), obj);
                    throw new ParseException(msg);
                }
            }
            
            Collections.reverse(args);
            Collections.reverse(numericArgs);
            
            Set<String> result = new TreeSet<String>();
            evaluate(args, numericArgs, result);
            stack.push(result);
        }
        
        protected abstract void evaluate(List<Set<String>> args, List<Double> numericArgs,
                Set<String> result) throws ParseException;
    }
    
    public static class UnionFunction extends SetOperationFunction {
        public static final String NAME = "union";
        public UnionFunction(Map<String, Set<String>> sets) {
            super(NAME, -1, sets);
        }
        
        protected void evaluate(List<Set<String>> args, List<Double> numericArgs, Set<String> result) {
            for (Set<String> set: args) {
                result.addAll(set);
            }
        }
    }

    public static class IntersectionFunction extends SetOperationFunction {
        public static final String NAME = "intersection";
        public IntersectionFunction(Map<String, Set<String>> sets) {
            super(NAME, -1, sets);
        }
        
        protected void evaluate(List<Set<String>> args, List<Double> numericArgs, Set<String> result) {
            for (int i=0; i<args.size(); i++) {
                final Set<String> set = args.get(i);
                if (i == 0) result.addAll(set);
                else result.retainAll(set);
            }
        }
    }

    public static class DiffFunction extends SetOperationFunction {
        public static final String NAME = "diff";
        public DiffFunction(Map<String, Set<String>> sets) {
            super(NAME, -1, sets);
        }
        
        protected void evaluate(List<Set<String>> args, List<Double> numericArgs, Set<String> result) {
            for (int i=0; i<args.size(); i++) {
                final Set<String> set = args.get(i);
                if (i == 0) result.addAll(set);
                else result.removeAll(set);
            }
        }
    }

    public static class FrequencyFunction extends SetOperationFunction {
        public static final String NAME = "freq";
        public FrequencyFunction(String name, Map<String, Set<String>> sets) {
            super(name, -1, sets);
        }
        
        protected void evaluate(List<Set<String>> args, List<Double> numericArgs, Set<String> result) throws ParseException {
            if (numericArgs.size() != 2) {
                throw new ParseException("Expected two numeric arguments");
            }
            
            final int minFreq = numericArgs.get(0).intValue();
            int maxFreq = numericArgs.get(1).intValue();
            if (maxFreq < 0) maxFreq = Integer.MAX_VALUE;
            
            if (minFreq > maxFreq) {
                String msg = String.format("Min freq (%d) larger than max freq (%d)",
                        minFreq, maxFreq);
                throw new ParseException(msg);
            }
            
            if (minFreq > args.size()) {
                String msg = String.format("Warning: min freq (%d) larger than number of input sets (%d)",
                        minFreq, args.size());
                System.err.println(msg);
            }
            
            Map<String, Integer> freqs = new HashMap<String, Integer>();
            for (Set<String> set: args) {
                for (String value: set) {
                    final Integer freqObj = freqs.get(value);
                    final int freq = freqObj == null ? 0 : freqObj.intValue();
                    freqs.put(value, freq+1);
                }
            }
            
            for (Map.Entry<String, Integer> entry: freqs.entrySet()) {
                final int freq = entry.getValue().intValue();
                if (freq >= minFreq && freq <= maxFreq) result.add(entry.getKey());
            }
        }
    }
    
    public static class MinFrequencyFunction extends FrequencyFunction {
        public static final String NAME = "minfreq";
        public MinFrequencyFunction(Map<String, Set<String>> sets) {
            super(NAME, sets);
        }
        
        protected void evaluate(List<Set<String>> args, List<Double> numericArgs, Set<String> result) throws ParseException {
            if (numericArgs.size() != 1) {
                throw new ParseException("Expected one numeric arguments");
            }
            numericArgs.add(new Double(Integer.MAX_VALUE));
            super.evaluate(args, numericArgs, result);
        }
    }
    
    public static class NamesFunction extends SetOperationFunction {
        public static final String NAME = "names";
        public NamesFunction(Map<String, Set<String>> sets) {
            super(NAME, -1, sets);
        }
        
        protected void evaluate(List<Set<String>> args, List<Double> numericArgs, Set<String> result) throws ParseException {
            if (numericArgs.size() != 0) {
                throw new ParseException("No numeric arguments are allowed");
            }
            
            for (Set<String> arg: args) {
                boolean found = false;
                for (Map.Entry<String, Set<String>> entry: getSets().entrySet()) {
                    final Set<String> set = entry.getValue();
                    if (set == arg) {
                        result.add(entry.getKey());
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    throw new ParseException("Could not find a set name for "+arg);
                }
            }
        }
    }
    
}
