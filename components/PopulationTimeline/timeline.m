try
    addpath(genpath('wanderlust'));
    cf=readcommandfile(commandfile);
    %% Read input data skipping first row (header)
    file_in=getinput(cf,'in');
    data_in=dlmread(file_in,'\t',1,0);

    %% Read parameters
    params.branch=getparameter(cf,'branch','boolean');
    params.band_sample=getparameter(cf,'band_sample','boolean');
    params.voting_scheme=getparameter(cf,'voting_scheme','string');
    params.snn=getparameter(cf,'snn','float');
    params.k=getparameter(cf,'k','float');
    params.l=getparameter(cf,'l','float');
    params.num_landmarks=getparameter(cf,'num_landmarks','float');
    params.num_graphs=getparameter(cf,'num_graphs','float');
    seed=getparameter(cf,'seed','float');
    if (seed >= 0) params.s=seed; end
    params.metric=getparameter(cf,'metric','string');
    % TODO: split string list and parse to int
    partial_order=getparameter(cf,'partial_order','string');
    if (~strcmp(partial_order, '')) params.partial_order=partial_order; end
    params.verbose = 1;
    params.chunk_size = 1000;
    %Add parameter if needed in the future
    %params.flock_landmarks = 2;

    tempdir = gettempdir(cf);
    pc = parcluster;
    %getmetadata(cf, 'instanceName')
    pc.JobStorageLocation = tempdir;

    %% Set number of workers for parallelisation using parfor
    var='metadata_cpu';
    if ( isfield(cf,var) )
        cores=floor(str2num(getfield(cf,var))/2);
        myPool = parpool(pc, cores);
    else
        % Default pool uses 1 worker
        myPool = parpool(1);
    end

    %% Run wanderlust
    result=wanderlust(data_in,params);
    delete(myPool);

    %% Save output of the wanderlust call
    filename = getoutput(cf,'out');
    %% write header
    fid = fopen(filename, 'w');
    fprintf(fid, 'Trajectory\n');
    fclose(fid)
    %% write data
    dlmwrite(filename, mean(result.T, 1)', '-append', 'precision', '%.6f', 'delimiter', '\t');

    %% Save output of the wanderlust call
    % data_out.numdata = num2cell(mean(result.T, 1)');
    % data_out.data = cellfun(@(x) sprintf('%g',x), data_out.numdata,'UniformOutput',false);
    % data_out.columnheads = {'Trajectory'};
    % writefilecsv(getoutput(cf,'out'), data_out);

catch myError
    try
        whos
        writeerrorstack(cf, myError);
        delete(gcp('nocreate'));
    catch ErrorAgain
        exit
    end
end
exit;  
