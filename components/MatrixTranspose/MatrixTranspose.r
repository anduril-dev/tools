library(componentSkeleton)

execute <- function(cf) {
    matr <- Matrix.read(get.input(cf, 'in'))
    row.name <- get.parameter(cf, 'rowName')
    CSV.write(get.output(cf, 'out'), t(matr), first.cell=row.name)
    return(0)
}

main(execute)
