#!/bin/bash
set -e

destdir='../../lib/qubic'
mkdir -p "$destdir"
R --slave <<EOF
install.packages(c('biclust', 'isa2', 'MASS'), lib = '$destdir')
source("http://bioconductor.org/biocLite.R")
biocLite("QUBIC", lib = '$destdir')
biocLite(c('fabia',
           'GEOquery',
           'GEOmetadb',
           'GOstats',
           'GO.db',
           'multtest',
           'pcaMethods'), lib = '$destdir')
EOF
