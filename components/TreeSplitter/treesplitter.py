import os
from anduril import *
from anduril.args import *
import networkx as nx
import sys

import rootedTree


if pThreshold==1.0: #there is no threshold if effect
    pThreshold=None

t=rootedTree.RootedTree.init_graphML(tree)

if "," in data_label:
    data_labels=data_label.split(",")
else:
    data_labels=[data_label]


if annotationColumnTypes not in ["strict","guess"]:
    try:
        annotationColumnTypes=eval(annotationColumnTypes)
    except Exception:
        raise Exception("Invalid annotation column type.")
    assert isinstance(annotationColumnTypes,type), "Invalid annotation column type."


if nodeAnnotations!=None:
    annotations=TableReader(nodeAnnotations,column_types=annotationColumnTypes)
    data=annotations.get_dict()

    #first check that all the leafs are in the annotations file
    if len(set(data_labels).intersection( set(annotations.fieldnames)))>0: #if annotation table is used
        for node in t.iter_leaves():
            if t.metadata[node][node_label] not in data:
                if allowMissingLeafs:
                    missing_vals=dict(map(lambda column:(column,None),annotations.fieldnames[1:]))
                    data[t.metadata[node][node_label]]=missing_vals
                else:
                    raise Exception("Leaf "+t.metadata[node][node_label]+" not in nodeAnnotations file. See component parameter allowMissingLeafs.")

    #then add the annotations from the table to the leafs of the tree
    for data_label in data_labels:
        if data_label!=annotations.fieldnames[0]:
            if data_label in annotations.fieldnames:
                for node in t.iter_leaves():
                    t.add_metadata(node,data_label,data[t.metadata[node][node_label]][data_label])
            else:
                print "Warning: data_label '"+data_label +"' is not in the nodeAnnotations file."
        else:
            print "Warning: data_label '"+data_label+"' is the same as node id column header of the nodeAnnotations file."

splits=[]
for data_label in data_labels:
    for node in t.iter_leaves():
        assert data_label in t.metadata[node], "data_label '"+data_label +"' cannot be found from the tree for node '"+node+"'."

    if splitMethod=="single":
        maxmi_value,maxmi_node,maxmi_subtree_enriched,maxmi_other_enriched=t.find_split(data_label,useMissingData=useMissingData,localMI=localMI)
        pval= t.find_split_get_mi_p_val_estimate(data_label,randomizations,maxmi_value,useMissingData=useMissingData,localMI=localMI)
        splits.append( (maxmi_value,maxmi_node,maxmi_subtree_enriched,maxmi_other_enriched,pval,data_label))

        print "Found split with MI value",
        print maxmi_value,
        if randomizations>0:
            print "with estimated p-value",
            print pval,
        print "and split node '"+str(maxmi_node)+ "'."
    elif splitMethod=="iterative":
        splits.extend(t.find_splits_iterative(data_label,MIThreshold,p_threshold=pThreshold,localMI=localMI))

splitTable=TableWriter(output_splits,fieldnames=["ID","Members","EnrichedValues","MI","Pval","DataLabel"])

#map from node name to node label
namemap=lambda nlist:map(lambda n:t.metadata[n][node_label],nlist)

net=t.get_as_net()
for i,split in enumerate(splits):
    mi,node,ste,oe,pval,data_label=split
    split_name="split"+str(i+1)
    net.node[node][data_label+"_mi"]=mi
    net.node[node][data_label+"_enriched"]=",".join(map(str,ste))
    net.node[node]["split_name"]=split_name

    subtree_nodes=namemap(t.iter_leaves(node))
    other_nodes=namemap(set(t.iter_leaves())-set(t.iter_leaves(node)))
    splitTable.writerow(dict(zip(splitTable.fieldnames,[split_name+"_subtree",",".join(sorted(subtree_nodes)),",".join(map(str,ste)),mi,pval,data_label])))
    splitTable.writerow(dict(zip(splitTable.fieldnames,[split_name+"_other",",".join(sorted(other_nodes)),",".join(map(str,oe)),mi,pval,data_label])))

nx.write_graphml(net,newTree)






