try
    cf=readcommandfile(commandfile);
    for t=1:5
        eval(['table' num2str(t) 'file=getinput(cf,''table' num2str(t) ''');']);
        try
            eval(['table' num2str(t) '=readcsvcell(table' num2str(t) 'file);']);
            eval(['[h,w]=size(table' num2str(t) '.data);']);
            writelog(cf, ['table' num2str(t) ' contains ' num2str(w) ' columns and ' num2str(h) ' rows']);
        catch noFile
            eval(['table' num2str(t) '=nan;']);
        end
    end
    
    instance.name=getmetadata(cf, 'instanceName');
    for t=1:9
        eval(['var' num2str(t) '=getinput(cf,''var' num2str(t) ''');']);
    end
    for t=1:10
        eval(['param' num2str(t) '=getparameter(cf,''param' num2str(t) ''',''guess'');']);
    end
    srcfile.full=getinput(cf, 'script');
    [srcfile.path srcfile.name srcfile.ext]=fileparts(srcfile.full);
    srcfile.current=pwd();
    
    % Prepare the document
    document.dir=getoutput(cf, 'document');
    mkdir(document.dir);
    document.out=sprintf('%s produced no \\LaTeX{} output.', instance.name);
    
    % Prepare the optional outputs
    optOut1='';
    optOut2='';
    optOut3='';
    
    cd(srcfile.path)

    if ( strcmp(srcfile.ext, '.m') )
        source_code = srcfile.name
    else
	source_code = fileread(srcfile.name)
    end
    eval(source_code)

    cd(srcfile.current)
    
    if ~exist('table','var') 
        writeerror(cf, 'No output (table) defined during the evaluation.');
        exit
    end
    if ~isstruct(table)
        writeerror(cf, 'Output (table) is not a struct!');
        exit
    end 
    if ~isfield(table,'data')
        writeerror(cf, 'Output (table.data) contains nothing');
        exit
    end
    % generate table heads if needed
    if ~isfield(table,'columnheads')
        w=size(table.data,2);
        writelog(cf, 'Struct table does not contain field "columnheads". Generating...');

        table.columnheads=cellfun(@(x) sprintf('V%d',x), num2cell(1:w),'UniformOutput',false);
    end
    % convert table to string, if needed
    if isnumeric(table.data)
        table.data=num2csvcell(table.data);
    end
    writefilecsv(getoutput(cf,'table'),table);
    if exist('optOut1','var')
        if isnumeric(optOut1)
            optOut1=mat2str(optOut1);
        end
        fid=fopen(getoutput(cf,'optOut1'),'wt');
        fwrite(fid,optOut1); fclose(fid);
    end
    if exist('optOut2','var')
        if isnumeric(optOut2)
            optOut2=mat2str(optOut2);
        end
        fid=fopen(getoutput(cf,'optOut2'),'wt');
        fwrite(fid,optOut2); fclose(fid);
    end
    if exist('optOut3','var')
        if isnumeric(optOut3)
            optOut3=mat2str(optOut3);
        end
        fid=fopen(getoutput(cf,'optOut3'),'wt');
        fwrite(fid,optOut3); fclose(fid);
    end
    if isfield(document,'out')
        fid=fopen([document.dir filesep 'document.tex'],'at');
        fprintf(fid,'%s',document.out); fclose(fid);
    end
    
catch myError
    try
        whos
        writeerrorstack(cf, myError);
    catch ErrorAgain
        exit
    end
end
exit;  
