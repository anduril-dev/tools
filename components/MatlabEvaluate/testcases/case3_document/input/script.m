% Print parameter values to the output document
table.data={'nothing'};
table.columnheads={'X.nothing.'};

document.out=[sprintf('Current parameter values are: %s, %s, %s, %s, and %s.\n',...
                        param1, param2, param3, param4, param5),...
                  sprintf('Instance name: %s.\n', instance.name)];
