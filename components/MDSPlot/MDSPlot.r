library(componentSkeleton)
library(cluster)
library(scatterplot3d)


execute <- function(cf) {
    # Read input and get parameters
    expr          <- Matrix.read(get.input(cf, 'expr'))
    group.file    <- get.input(cf,'groups')
    plot.names    <- get.parameter(cf, 'plotNames')
    plot.legend   <- get.parameter(cf, 'plotLegend', type='boolean')
    dimensions    <- get.parameter(cf, 'dimensions', type='int')
    cex           <- get.parameter(cf, 'cex', 'float')
    pch           <- get.parameter(cf, 'pch', 'int')
    colorFunction <- get.parameter(cf, 'colorFunction')
    section.title <- get.parameter(cf, 'sectionTitle')
    section.type  <- get.parameter(cf, 'sectionType')
    width.cm      <- get.parameter(cf, 'width', 'float')
    if(!(dimensions %in% c(2,3))) {
        write.error(cf, "Incorrect value for parameter 'dimensions'. It must equal 2 or 3.")
        return(PARAMETER_ERROR)
    }
    if(!(section.type %in% c('', LATEX.SECTIONS))) {
        write.error(cf, sprintf("Unknown LaTeX section type '%s'", section.type))
        return(PARAMETER_ERROR)
    }
    D <- as.character(dimensions)

    # Get groups and samples to plot
    if(group.file != '') {
        group.table <- SampleGroupTable.read(group.file)
        groups  <- unique(SampleGroupTable.get.groups(group.table))
        groups  <- groups[group.table$Type != SAMPLE.GROUP.TABLE.RATIO]
        samples <- c()
        for(g in groups) 
            samples <- c(samples, SampleGroupTable.get.source.groups(group.table, g))
        samples <- intersect(samples, colnames(expr))
        expr <- expr[,samples]
    } else {
        group.table <- SampleGroupTable.new()
        group.table <- SampleGroupTable.add.group(group.table, 'all', SAMPLE.GROUP.TABLE.MEDIAN, colnames(expr), 'All samples')
        groups <- SampleGroupTable.get.groups(group.table)
    }
    # Check if sufficient samples for dimensions
    if(ncol(expr) < (dimensions+1)) {
        write.error(cf, sprintf("Not enough samples to compute MDS in %d dimensions; at least %d needed, %d were provided.",
                                dimensions, dimensions+1, ncol(expr)))
        return(INVALID_INPUT)
    }
    # Get colors for plotting
    if(colorFunction != '') {
        colors <- eval(parse(text=sprintf(colorFunction,length(groups))))
        if(length(colors) < length(groups)) {
            write.error(cf, "ColorFunction has less colors than there are groups.")
            return(PARAMETER_ERROR)
        }
    } else {
        colors <- c('black', rainbow(length(groups)-1))
    }

    # Do MDS
    write.log(cf,"Computing multidimensional scaling...")
    distances <- dist(t(expr))
    MDS <- cmdscale(distances,k=dimensions)
    if(ncol(MDS)==0) {
        write.error(cf, "All samples are equal; MDS can not be computed.")
        return(INVALID_INPUT)
    }
    colnames(MDS) <- c("x","y","z")[1:ncol(MDS)]
    if(ncol(MDS) < dimensions) {
        write.log(cf,"Warning: plotting of MDS is not possible due to similaty of samples.")
        plot.MDS <- FALSE
    } else {
        plot.MDS <- TRUE
    }

    # Write MDS output
    CSV.write(get.output(cf, 'mdsCoOrdinates'), MDS, first.cell = "SampleID")
 
    # Prepare and write report output
    out.dir   <- get.output(cf, 'report')
    dir.create(out.dir, recursive=TRUE)
    plot.file <- sprintf('MDSReport-%s-figure.pdf', get.metadata(cf, 'instanceName'))

    # Check if MDS coordinates are valid for plotting
    if(plot.MDS) {
        x.valid <- all(!ceiling(abs(log10(abs(range(MDS[is.finite(MDS[,"x"]),"x"])))))>12)
        y.valid <- all(!ceiling(abs(log10(abs(range(MDS[is.finite(MDS[,"y"]),"y"])))))>12)
        plot.MDS <- x.valid && y.valid
    }
    if (plot.MDS) {
 
        # Initiate plot
        pdf(file.path(out.dir, plot.file))
		if (plot.legend && (group.file != '')) {
		    # Set margins to display legend on right
			par(mar=c(5,1,5,9))
			par(xpd=TRUE)
			pl <-  switch(D, "2" = plot(MDS[,1], MDS[,2], type='n', main='', xlab='', ylab=''),
                             "3" = scatterplot3d(x=MDS[,1], y=MDS[,2], z=MDS[,3],type='n', main='', xlab='', ylab='',zlab='', mar=c(7,1,7,13)))
		} else {
        	pl <-  switch(D, "2" = plot(MDS[,1], MDS[,2], type='n', main='', xlab='', ylab=''),
            	             "3" = scatterplot3d(x=MDS[,1], y=MDS[,2], z=MDS[,3],type='n', main='', xlab='', ylab='',zlab=''))
		}

        # Plot each group (or group with all samples)
		counter <- 1
        plotLegend <- character()
        for (g in groups) {
            samples <- sort(SampleGroupTable.get.source.groups(group.table, g))
            samples <- intersect(samples, colnames(expr))

            if (D=="2") {
                if(plot.names) {
                    text(MDS[samples,"x"],MDS[samples,"y"],samples,col=colors[counter],cex=cex)
                } else {
                    points(MDS[samples,"x"],MDS[samples,"y"],col=colors[counter],cex=cex,pch=pch)
                }
            } else {
                A <- MDS[samples,1]
                B <- MDS[samples,2]
                C <- MDS[samples,3]

                coords <- pl$xyz.convert(A,B,C)
                x <- unlist(coords[1])
                y <- unlist(coords[2])

                if(plot.names) { 
                    text(x,y,samples,col=colors[counter],cex=cex)
                } else {
                    points(x,y,col=colors[counter],cex=cex,pch=pch)
                }
            }
            plotLegend <- c(plotLegend, g)
            counter <- counter + 1
        }
        # Display legend in right margin
        if (plot.legend && (group.file != ''))
            legend(x=par("usr")[2]+1,y=max(par("usr")[3:4]), legend=plotLegend, col=colors[1:(counter-1)], pch=1)
       dev.off()
    } else {
       msg <- 'MDS plotting is not possible due to similaty of samples.'
    }
    tex <- c()
    if (get.parameter(cf, 'pagebreak', 'boolean'))
       tex <- c(tex, '\\clearpage')
    if (nchar(section.type) > 0 && nchar(section.title) > 0)
       tex <- c(tex, sprintf('\\%s{%s}', section.type, section.title))
    if (plot.MDS) {
        tex <- c(tex, latex.figure(plot.file, caption=get.parameter(cf,'caption','string'), image.width=width.cm))
    } else {
        tex <- c(tex, msg)
    }
    latex.write.main(cf, 'report', tex)
    return(0)
}

main(execute)
