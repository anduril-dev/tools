<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>CSV2Latex</name>
    <version>2.3</version>
    <doc>Converts the given comma separated value file to a LaTeX table.
         Table is generated into a separated fragment that is included into a host
         document that may be used as a default template for it. The table fragment may
         be used within other documents as well.
         LaTeX label for the table is table:instanceName, where the instanceName refers to
         the name of the pipeline component used in the network configurations.

         Some columns may represent hyperlinks to external resources. The links
         are constructed based on the <var>refs</var> input. The file has three columns:
         URL, refCol, and valueCol. URL is an URL, which has $ID$ tag in place of the
         reference id. The reference itself has been taken from the refCol column.
         Visible name of the link is taken from the valueCol column, which also
         indicates the target column for the reference declaration. You may create
         links to the document labels by starting your URL with 'latex:'.
         Special targets for the hyperlinks can be established with the URL prefix
         'anchor:'. References to these targets can be constructed using prefix
         'link:tablecell.'. Similar hyperlink targets of other categories may be
         referred by omitting 'tablecell.' part of the prefex and by replacing that
         with the correct anchor category.

         Character escapes available for the <var>listDelim</var> parameter are
         explained in the description of the <a href="../CSVCleaner/index.html">CSVCleaner</a>
         component.
    </doc>
    <author email="Marko.Laakso@Helsinki.FI">Marko Laakso</author>
    <category>Convert</category>
    <category>Latex</category>
    <launcher type="java">
        <argument name="class" value="CSV2Latex" />
    </launcher>
    <inputs>
        <input name="tabledata" type="CSV">
            <doc>Table content</doc>
        </input>
        <input name="refs" type="CSV" optional="true">
            <doc>Reference rules for the hyperlinks</doc>
        </input>
    </inputs>
    <outputs>
        <output name="report" type="Latex">
            <doc>LaTeX representation for the given data.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="columns" type="string" default="">
            <doc>Comma separated list of column selections for the output.
                 The empty default will use all columns.</doc>
        </parameter>
        <parameter name="colFormat" type="string" default="center">
            <doc>LaTeX tabular format for the columns.
                 Special values of 'center', 'left' and 'right' may be used to
                 produce the corresponding uniform alignments of all columns.</doc>
        </parameter>
        <parameter name="numberFormat" type="string" default="">
            <doc>A comma separated list of decimal formats for the columns.
                 Each entry consists of the column name and the Java DecimalFormal
                 pattern separated with equal sign. For example, rounding to three
                 decimals can be done like: myColumn=#0.000. A special keyword of
                 'RAW_LATEX' may be used to show input values as such without any
                 escaping of formatting.</doc>
        </parameter>
        <parameter name="listCols" type="string" default="">
            <doc>Comma separated list of column names. Columns of this list may
                 contain several values separated with commas and the delimiters
                 will be replaced with list delimiters.</doc>
        </parameter>
        <parameter name="listDelim" type="string" default=",\s">
            <doc>Delimiting strings between the values of list valued cell contents</doc>
        </parameter>
        <parameter name="rename" type="string" default="">
            <doc>Comma separated list of column renaming rules (oldname=newname).
                 New names are used in table header but they do not affect the other
                 behaviour of this component.</doc>
        </parameter>
        <parameter name="evenColor" type="string" default="0.96,0.96,0.96">
            <doc>Background color for the even rows. Comma separated list of
                 red, green, and blue intensities [0,1]. Special value of '1,1,1'
                 refers to the default background.</doc>
        </parameter>
        <parameter name="ruler" type="string" default="{}">
            <doc>Latex command for the row separating rulers</doc>
        </parameter>
        <parameter name="dropMissing" type="boolean" default="true">
            <doc>This flag can be turned off in order to generate links with missing
                 texts. Link text are substituted with target identifiers.</doc>
        </parameter>
        <parameter name="skipEmpty" type="boolean" default="false">
            <doc>This flag can be used to replace empty tables with a simple LaTeX comment.</doc>
        </parameter>
        <parameter name="caption" type="string" default="">
            <doc>Caption text for the table.</doc>
        </parameter>
        <parameter name="section" type="string" default="">
            <doc>Section title for the table container or an empty string
                 if no section should be generated.</doc>
        </parameter>
        <parameter name="sectionType" type="string" default="subsection">
            <doc>Type of LaTeX section: usually one of: section, subsection, or subsubsection.
                 No section statement is written if section title is empty.</doc>
        </parameter>
        <parameter name="pageBreak" type="boolean" default="false">
            <doc>Use clear page after the table.</doc>
        </parameter>
        <parameter name="attach" type="boolean" default="false">
            <doc>Include the original data as an attachment</doc>
        </parameter>
        <parameter name="countRows" type="boolean" default="false">
            <doc>Include a row count to the table caption.</doc>
        </parameter>
        <parameter name="hRotate" type="boolean" default="false">
            <doc>Use vertical column names</doc>
        </parameter>
    </parameters>
</component>
