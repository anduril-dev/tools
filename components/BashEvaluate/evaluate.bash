# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh" "$1"

# use these functions to read command file
script=$( getinput command )
scriptpar=$( getparameter script )
toscript=$( getoutput scriptOut )

stdoutfile=$( getoutput stdOut )
stderrfile=$( getoutput stdErr )

out1=$( getoutput out1 )
out2=$( getoutput out2 )
out3=$( getoutput out3 )

folder1=$( getoutput folder1 )
folder2=$( getoutput folder2 )
folder3=$( getoutput folder3 )

array1=$( getinput array1 )
array2=$( getinput array2 )
array3=$( getinput array3 )

arrayOut1=$( getoutput arrayOut1 )
arrayIndex1=$( getoutput _index_arrayOut1 )
arrayOut2=$( getoutput arrayOut2 )
arrayIndex2=$( getoutput _index_arrayOut2 )
arrayOut3=$( getoutput arrayOut3 )
arrayIndex3=$( getoutput _index_arrayOut3 )

touch "$out1" "$out2" "$out3" || echo error creating output files >> $errorfile
mkdir -p "$folder1" "$folder2" "$folder3" || echo error creating output directories >> $errorfile
mkdir -p "${arrayOut1}" "${arrayOut2}" "${arrayOut3}" || echo error creating output array >> $errorfile
echo -e '"Key"'"\t"'"File"' >> "${arrayIndex1}"
echo -e '"Key"'"\t"'"File"' >> "${arrayIndex2}"
echo -e '"Key"'"\t"'"File"' >> "${arrayIndex3}"

param1=$( getparameter param1 )
param2=$( getparameter param2 )
param3=$( getparameter param3 )
param4=$( getparameter param4 )
param5=$( getparameter param5 )
failerr=$( getparameter failOnErr )

var1=$( getinput var1 )
var2=$( getinput var2 )
var3=$( getinput var3 )
var4=$( getinput var4 )
var5=$( getinput var5 )
var6=$( getinput var6 )

param1=${param1//,/\\,}
param2=${param2//,/\\,}
param3=${param3//,/\\,}
param4=${param4//,/\\,}
param5=${param5//,/\\,}
# make sure all commas are escaped
# if folder names contain commas, this still fails.

if [ "$failerr" = "true" ]
then echo "set -e" >> "${toscript}.tmp"
fi
if [ ! -z "$scriptpar" ]
then echo -e source "${ANDURIL_HOME}/lang/bash/functions.sh" \"${1}\"\\n >> "${toscript}.tmp"
     echo -e "${scriptpar}" >> "${toscript}.tmp"
else
    echo -e source "${ANDURIL_HOME}/lang/bash/functions.sh" \"${1}\"\\n >> "${toscript}.tmp"
    cat "${script}" >> "${toscript}.tmp"
fi

exportarraykeys vararray
export_command

# The last \r replace is for DOS format script files... hopefully this works in DOS environment...
sed -e 's,@var1@,"'"$var1"'",g' -e 's,@var2@,"'"$var2"'",g' -e 's,@var3@,"'"$var3"'",g' \
 -e 's,@var4@,"'"$var4"'",g' -e 's,@var5@,"'"$var5"'",g' -e 's,@var6@,"'"$var6"'",g' \
 -e 's,@out1@,"'"$out1"'",g' -e 's,@out2@,"'"$out2"'",g' -e 's,@out3@,"'"$out3"'",g' \
 -e 's,@folder1@,"'"$folder1"'",g' -e 's,@folder2@,"'"$folder2"'",g' -e 's,@folder3@,"'"$folder3"'",g' \
 -e 's,@param1@,"'"$param1"'",g' -e 's,@param2@,"'"$param2"'",g' -e 's,@param3@,"'"$param3"'",g' \
 -e 's,@param4@,"'"$param4"'",g' -e 's,@param5@,"'"$param5"'",g' \
 -e 's,@arrayOut1@,"'"$arrayOut1"'",g' -e 's,@arrayIndex1@,"'"$arrayIndex1"'",g' \
 -e 's,@arrayOut2@,"'"$arrayOut2"'",g' -e 's,@arrayIndex2@,"'"$arrayIndex2"'",g' \
 -e 's,@arrayOut3@,"'"$arrayOut3"'",g' -e 's,@arrayIndex3@,"'"$arrayIndex3"'",g' \
 -e 's,\r$,,' "${toscript}.tmp" > "$toscript"
rm "${toscript}.tmp"

touch "$stdoutfile"
touch "$stderrfile"
cd $( dirname "$1" )
if [ "$( getparameter echoStdOut )" = "true" ]
then bash "$toscript" 2> "$stderrfile" | tee "$stdoutfile"
      exit_status=${PIPESTATUS[0]}
    if [ -s "$stderrfile" ]
    then echo "stdErr:"
         cat "$stderrfile"
    fi
else bash "$toscript" > "$stdoutfile" 2> "$stderrfile" 
     exit_status=$?
fi

if [ "$exit_status" -gt "0" ]
then to_file="$errorfile"
    echo "Exit status: $exit_status" >> "$to_file"
    echo "stdErr: " >> "$to_file"
    cat "$stderrfile" >> "$to_file"
fi

exit $exit_status
