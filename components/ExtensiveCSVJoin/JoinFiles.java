import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.core.utils.Pair;
import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;


public class JoinFiles extends SkeletonComponent {

	private ErrorCode joinFiles(CommandFile cf) throws IOException{
		
		CSVParser filesIn = new CSVParser(cf.getInput("files"));
		
		CSVParser fileTypes = new CSVParser(cf.getInput("fileDefs"));
		
		String [] indexCols = cf.getParameter("indexOfKey").split(",");
		String [] indexColName = new String[indexCols.length];
		
		int threshold = cf.getIntParameter("maxNumOfRows");
		
		int [] indexOfKeyColumns =  new int[indexCols.length];
		for(int i = 0; i < indexCols.length; i++){
			indexOfKeyColumns[i] = Integer.parseInt(indexCols[i]);
		}
		
		ArrayList<Pair<String, ArrayList<String>>> types = new ArrayList<Pair<String, ArrayList<String>>>();
		ArrayList<Pair<String, HashSet<String>>> autoRenameColumns = new ArrayList<Pair<String, HashSet<String>>>();

		while(fileTypes.hasNext()){
		
			String[] typeDefs = fileTypes.next();
			String [] defs = typeDefs[1].split(",");
			types.add(new Pair<String, ArrayList<String>>(typeDefs[0], new ArrayList<String>(defs.length)));

			for(String d : defs){
				types.get(types.size()-1).get2().add(d);
			}
			autoRenameColumns.add(new Pair<String, HashSet<String>>(typeDefs[0], new HashSet<String>()));
			for(String d : typeDefs[2].split(",")){
				autoRenameColumns.get(autoRenameColumns.size()-1).get2().add(d);
			}
			
		}
		
		MyMatrix result = null;
		
		int typeInd = 0;
		for(Pair<String, ArrayList<String>> type : types){
			
			boolean fileExists = false;
			
			while(filesIn.hasNext()){
				
				String [] filePath = filesIn.next();
			
				String fileName = filePath[filesIn.getColumnIndex("File")];
				String fileKey = filePath[filesIn.getColumnIndex("Key")];
			
				if(fileName.matches(".*" + type.get1() + ".*")){
					fileExists = true;
					
					try{
						String delim = testDelimitter(new File(fileName), type.get2(), "\t");
						
						CSVParser joinFile = new CSVParser(new File(fileName), delim, 0);
						
						int rows = 0;
						while(joinFile.hasNext()  && (threshold == -1 || rows < threshold)){
						
							String[] line = joinFile.next();
						
							int addIndexCols = 0;
						
							for(int i : indexOfKeyColumns){
								if(!type.get2().contains(joinFile.getColumnNames()[i])){
									addIndexCols ++;
								}
							}
							String [] selectedColumns = new String[type.get2().size() + addIndexCols];
						
							String [] colNames = new String[selectedColumns.length];
						
							String key = "";
						
							int j = 0;
							for(int i : indexOfKeyColumns){
								key = key + line[i];
							
								if(!type.get2().contains(joinFile.getColumnNames()[i])){
									selectedColumns[j] = line[i];
									if(indexColName[j] == null){
										colNames[j] = joinFile.getColumnNames()[i];		
										indexColName[j] = colNames[j];
									}else{
										colNames[j]=indexColName[j];
									}
									j++;
								}
							}
							
							int i = addIndexCols;
						
							for(String columnName : type.get2()){
								selectedColumns[i] = line[joinFile.getColumnIndex(columnName)];
								
								if(autoRenameColumns.get(typeInd).get2().contains(columnName))
									colNames[i] = columnName + "_" + fileKey;
								else
									colNames[i] = columnName;
							
								i ++;
							}
						
							if(result == null){
								result = new MyMatrix(colNames, selectedColumns, key);
							}else
								result.addRow(colNames, selectedColumns, key);
							rows ++;
						}
					}catch(NullPointerException e){//process empty file
						
						result = createEmptyFile(type.get2(), autoRenameColumns.get(typeInd), fileKey, result);
						
					}catch(IllegalStateException e){ //file is malformated and cannot be read
						System.err.println("ERROR for defition: '"+type.get1()+"' trying to read file "+fileName);
						e.printStackTrace();
						return ErrorCode.INVALID_INPUT;
					}
				}
				
			}
			if(!fileExists){
				result = createEmptyFile(type.get2(), autoRenameColumns.get(typeInd), type.get1(), result);
			}
			filesIn = new CSVParser(cf.getInput("files"));
			typeInd ++;
		}
			
		String[][] data = result.getMatrix();
		
		CSVWriter out = new CSVWriter(result.getColumnNames(), cf.getOutput("join"));
		
		for(int i = 0; i < data.length; i++){
			for(int j = 0; j < data[i].length; j++){
				out.write(data[i][j]);
			}
		}
		out.close();
		
		return ErrorCode.OK;
	}
	
	private String testDelimitter(File file, ArrayList<String> columns, String delim){
		
		try{
		
			CSVParser test = new CSVParser(file, delim, 0);
			for(String col: columns){
				test.getColumnIndex(col);
			}
		}catch(IOException ioe){
			return null;
		}catch(IllegalArgumentException iae){
			if(delim.equals("\t"))
				delim = testDelimitter(file, columns, ",");
			else if(delim.equals(","))
				delim = testDelimitter(file, columns, " ");
			else if(delim.equals(" "))
				delim = testDelimitter(file, columns, "  ");
			else if(delim.equals("  "))
				System.err.println("Cannot determine delimitter for file "+file+". Check column name definitions and file format.");
		}
		return delim;
	}
	
	private MyMatrix createEmptyFile(ArrayList<String> columns, Pair<String, HashSet<String>> rename, String renameString, MyMatrix result){
		String [] columnNames = new String[columns.size()];
		String [] values = new String[columnNames.length];
		int i = 0;
		for(String columnName : columns){
			values[i] = null;
			
			if(rename.get2().contains(columnName))
				columnNames[i] = columnName + "_" + new File(renameString).getName();
			else
				columnNames[i] = columnName;
		
			i ++;
		}
		if(result == null){
			 return new MyMatrix(columnNames, values, "");
		}else
			result.addRow(columnNames, values, "");
		return result;
	}
	@Override
	protected ErrorCode runImpl(CommandFile arg0) throws Exception {
		return joinFiles(arg0);
	}

	public static void main(String [] args){
		new JoinFiles().run(args);
	}
}

