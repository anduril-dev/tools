import static org.junit.Assert.*;

import org.junit.Test;


public class TestMyMatrix {

	@Test
	public void test() {
		MyMatrix mm = new MyMatrix(new String[]{"Col1", "Col2"}, new String[]{"Val1", "Val2"}, "1");
		mm.addRow(new String[]{"Col1", "Col2"}, new String[]{"Val3", "Val4"}, "2");
		mm.addRow(new String[]{"Col1", "Col3"}, new String[]{"Val5", "Val6"}, "3");
		mm.addRow(new String[]{"Col3", "Col4"}, new String[]{"Val7", "Val8"}, "1");
		mm.addRow(new String[]{"Col3", "Col4"}, new String[]{"Val9", "Val10"}, "3");
		mm.addRow(new String[]{"Col3", "Col1"}, new String[]{"Val9", "Val11"}, "4");
		printMatrix(mm);
		//fail("Not yet implemented");
	}
	private void printMatrix(MyMatrix matr){
		for(String col : matr.getColumnNames()){
			System.out.print(col);
			System.out.print("\t");
		}
		System.out.println("");
		
		String[][] data = matr.getMatrix();
		for(int i = 0; i < data.length; i++){
			for(int j = 0; j < data[0].length; j++){
				System.out.print(data[i][j]+"\t");
			}
			System.out.println("");
		}
	}
}
