import java.util.ArrayList;
import java.util.HashMap;


public class MyMatrix {
	ArrayList<String> colNames = new ArrayList<String>();
	int rows = 0;
	int columns = 0;
	ArrayList<ArrayList<String>> matrix = new ArrayList<ArrayList<String>>();
	HashMap<String, ArrayList<Integer>> keys = new HashMap<String, ArrayList<Integer>>();
	
	public MyMatrix(String [] colNames, String [] line, String key) {
		keys.put(key, new ArrayList<Integer>());
		keys.get(key).add(rows);
		
		for(String col : colNames){
			this.colNames.add(col);
		}
		matrix.add(new ArrayList<String>());
		
		for(String cell : line){
			matrix.get(rows).add(cell);
			columns ++;
		}
		rows ++;
	}
	public void addRow(String [] names, String [] line, String key) {
		if(!keys.containsKey(key)){
			addNewRow(key);
		}
		
		int insertRow = keys.get(key).get(0);
		addRow(names, line, key, insertRow);
	}
	private void addNewRow(String key){
		matrix.add(new ArrayList<String>());
		rows ++;
		
		for(int i = 0; i < columns; i++){
			matrix.get(rows-1).add(null);
		}
		keys.put(key, new ArrayList<Integer>());
		keys.get(key).add(rows-1);
	}
	private void addRow(String [] names, String [] line, String key, int insertRow) {
		
		for(int i = 0; i < names.length; i++){
			if(this.colNames.contains(names[i])){
				
				if(matrix.get(insertRow).get(this.colNames.indexOf(names[i])) == null){
				
					matrix.get(insertRow).set(this.colNames.indexOf(names[i]), line[i]);
				
				}else if(!matrix.get(insertRow).get(this.colNames.indexOf(names[i])).equals(line[i])){
					//get next row for this key
					for(int j = 0; j < keys.get(key).size(); j++){
						if(keys.get(key).get(j) == insertRow && j < keys.get(key).size()-1){
							addRow(names, line, key, keys.get(key).get(j+1));
						}else{
							addNewRow(key);
							addRow(names, line, key, keys.get(key).get(keys.get(key).size()-1));
						}
					}
					return;
				}
			}
		}
		for(int i = 0; i < names.length; i++){
			if(!this.colNames.contains(names[i])){
				
				this.colNames.add(names[i]);
				for(int j = 0; j < rows; j++){
					if(j == insertRow)
						matrix.get(j).add(line[i]);
					else
						matrix.get(j).add(null);
				}
				columns ++;
			}
		}
	}	
	public String[] getColumnNames(){
		String [] cols = new String[columns];
		int i = 0;
		for(String name: colNames){
			cols[i] = name;
			i++;
		}
		return cols;
	}
	public String[][] getMatrix(){
		
		String[][] matr = new String[rows][columns];
		
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < columns; j++){
				matr[i][j] = matrix.get(i).get(j);
			}
		}
		return matr;
	}
}
