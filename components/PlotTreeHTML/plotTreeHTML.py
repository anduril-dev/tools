import os,shutil
from anduril.args import *
from anduril.args import cf
from anduril.table import TableReader
from rootedTree import RootedTree

def joins2tree(joins):
    """Constructs a tree from joins given in as iterator of tuples formatted
    as (cluster,child1,child2,...childn)
    """
    pass


def rcluster2tree(rclusters,ycoordType="height"):
    """Transforms the hierarhical tree produced by the R clustering package
    into JSON format tree with appropriate metadata.
    """
    def transform_id(theid):
        if isinstance(theid,str): #do nothing 
            return theid
        elif theid<0: #its a leaf
                return str(-theid)
        else:
            return "cluster_%s"%theid

    tree=RootedTree()
    rclusters=TableReader(rclusters,type="list")

    for fields in rclusters:
        tree.add_join(map(transform_id,fields[:3]))
        tree.add_metadata(transform_id(fields[0]),"height",float(fields[3]))

    assert tree.is_connected()

    tree.calculate_ycoords(ycoordType,"height")

    return tree


def is_numeric(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def add_metadata(metadatafile,tree):
    mt=TableReader(metadatafile,type="Dict")
    node_label_field=mt.fieldnames[0]
    numeric_fields={}
    for fieldname in mt.fieldnames[1:]: numeric_fields[fieldname]=True
    for row in mt:
        node_label=row[node_label_field]
        for key,val in row.items():
            if key!=node_label_field:
                if is_numeric(val):
                    val=float(val)
                else:
                    numeric_fields[fieldname]=False
                tree.add_metadata(node_label,key,val)
    for fieldname,numeric in numeric_fields.iteritems():
        if numeric:
            tree.average_metadata(fieldname)

def tree2json(tree):
    tree.calculate_size_metadata()
    return tree.get_json()

def handle_param(param,isList=False):
    if isList:
        plist=param.split(",")
        return "["+",".join(map(handle_param,plist))+"]"
    else:                
        if param=="": return "null"
        else: 
            try:
                return str(float(param))
            except ValueError:
                return '"'+param+'"'


#read the input data
if treeDataType=="rclusters":
    tree=rcluster2tree(treeStructure,ycoordType=ycoordType)
    if metadata:
        add_metadata(metadata,tree)
    json="["+tree2json(tree)+"]"
elif treeDataType=="json":
    json=open(treeStructure,'r').read()
else:
    raise Exception("Invalid value for treeDataType: "+treeDataType)

#json="\n".join(open("math_map_compact.json",'r'))

#read the template file
template_file='forest_template.html'
template=open(template_file,'rU').read()

#replace the list parameters
for param in ["nodeColorVariables","nodeSizeVariables","nodeLabelVariables","nodeHeightVariables","nodeImageVariables"]:
    template=template.replace('@'+param.upper()+'@',handle_param(cf.get_parameter(param),isList=True))

#replace the non-list parameters
for param in ["defaultNodeColor","defaultNodeSize","minNodeSize","maxNodeSize"]:
    template=template.replace('@'+param.upper()+'@',handle_param(cf.get_parameter(param),isList=False))

#replace the json
template=template.replace('@JSON@',json)

#create the dir for the plot output port
os.mkdir(plot)

#write the index file
outfile=open(os.path.join(plot,'index.html'),'w')
outfile.write(template)
outfile.close()

#copy the style
stylefile=open(os.path.join(plot,"style.css"),'w')
map(lambda l:stylefile.write(l),open("style.css"))

#copy the images
if imageData!=None:
    shutil.copytree(imageData,plot)

