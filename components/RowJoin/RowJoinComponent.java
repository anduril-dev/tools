import java.util.*;
import java.io.*;

import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;

public class RowJoinComponent extends SkeletonComponent {
    // INPUT
    File      inputFile;
    File      outputFile;
    File      tholdFile;
    CSVParser matrix;
    int       PARAM_IDCOLUMN;
    String    PARAM_MATCHCHAR;
    int       PARAM_STARTCOLUMN;
    double    PARAM_FREQLIM;
    boolean   PARAM_IMPUTE;
    boolean   PARAM_UPPER;
    boolean   PARAM_RETAIN;
    
    // OUTPUT
    List<String> ID       = new ArrayList<String>(1000000);
    List<String[]> values = new ArrayList<String[]>(100);
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        outputFile        = cf.getOutput("table");
        inputFile         = cf.getInput("matrix");
        tholdFile         = cf.getInput("thresholds");
        PARAM_IDCOLUMN    = Integer.parseInt(cf.getParameter("idColumn"))-1;
        PARAM_MATCHCHAR   = cf.getParameter("matchMe");
        PARAM_STARTCOLUMN = Integer.parseInt(cf.getParameter("startColumn"))-1;
        PARAM_IMPUTE      = Boolean.parseBoolean(cf.getParameter("impute"));
        PARAM_FREQLIM     = cf.getDoubleParameter("freqLimit");
        PARAM_UPPER       = cf.getBooleanParameter("upper");
        PARAM_RETAIN      = cf.getBooleanParameter("retainSkipped");
        
        CSVParser matrix = new CSVParser(inputFile);
        int bestFreq = 0;
        int divider = 1;
        boolean firsttime = true;
        int totalCols = matrix.getColumnCount()-PARAM_STARTCOLUMN;
        
        CSVWriter writer = null;
        try {
            String[] oldColumns = matrix.getColumnNames();
            String[] columnHeaders = null;
            if (PARAM_RETAIN){
                columnHeaders = new String[oldColumns.length-PARAM_STARTCOLUMN+(PARAM_STARTCOLUMN-PARAM_IDCOLUMN)];
                columnHeaders[0] = oldColumns[PARAM_IDCOLUMN];
                for (int i = PARAM_IDCOLUMN; i < oldColumns.length; i++){
                    columnHeaders[i-PARAM_STARTCOLUMN+(PARAM_STARTCOLUMN-PARAM_IDCOLUMN)] = oldColumns[i];
                }
            } else {
                columnHeaders = new String[oldColumns.length-PARAM_STARTCOLUMN+1];
                columnHeaders[0] = oldColumns[PARAM_IDCOLUMN];
                for (int i = PARAM_STARTCOLUMN; i < oldColumns.length; i++){
                    columnHeaders[i-PARAM_STARTCOLUMN+1] = oldColumns[i];
                }
            }
            writer = new CSVWriter(columnHeaders, outputFile, false);
            cf.writeLog("Starting duplication removal...");
            if (!PARAM_IMPUTE && !cf.inputDefined("thresholds")){
                while (matrix.hasNext()){ // ITERATE over rows
                    String[] row = matrix.next();
                    if (firsttime || row[PARAM_IDCOLUMN].compareTo(ID.get(ID.size()-1)) != 0){
                        String[] line = new String[matrix.getColumnCount()-PARAM_STARTCOLUMN+1];
                        bestFreq = 0;
                        if (!values.isEmpty()){
                            for (int j = 1; j < values.get(values.size()-1).length; j++){
                                writer.write(values.get(values.size()-1)[j], false);
                            }
                        }
                        values.clear();
                        firsttime = false;
                        writer.write(row[PARAM_IDCOLUMN]);
                        if (PARAM_RETAIN){
                            for (int col = PARAM_IDCOLUMN+1; col < PARAM_STARTCOLUMN; col++){
                                writer.write(row[col]);
                            }
                        }
                        ID.add(row[PARAM_IDCOLUMN]);
                        for (int i=PARAM_STARTCOLUMN; i < row.length; i++){
                            line[i-PARAM_STARTCOLUMN+1] = row[i];
                            if (row[i] != null && row[i].compareTo(PARAM_MATCHCHAR) == 0) bestFreq++;
                        }
                        if ((double)bestFreq/(double)totalCols > PARAM_FREQLIM){
                            values.add(line);
                        } else {
                            for (int i=0; i < line.length; i++){
                                line[i] = null;
                            }
                            values.add(line);
                        }
                    } else {
                        int newFreq = 0;
                        String[] line = new String[matrix.getColumnCount()-PARAM_STARTCOLUMN+1];
                        for (int i=PARAM_STARTCOLUMN; i < row.length; i++){
                            line[i-PARAM_STARTCOLUMN+1] = row[i];
                            if (row[i] != null && row[i].compareTo(PARAM_MATCHCHAR) == 0) newFreq++;            
                        }
                        if (newFreq > bestFreq && (double)newFreq/(double)totalCols >= PARAM_FREQLIM){
                            if (values.size() > 0){
                                values.remove(values.size()-1);
                            }
                            values.add(line);
                            bestFreq = newFreq;
                        }
                    }
                }
            } else if (PARAM_IMPUTE){
                while (matrix.hasNext()){ // ITERATE over rows
                    String[] row = matrix.next();
                    if (firsttime || row[PARAM_IDCOLUMN].compareTo(ID.get(ID.size()-1)) != 0){
                        String[] line = new String[matrix.getColumnCount()-PARAM_STARTCOLUMN+1];
                        firsttime = false;
                        if (!values.isEmpty()){
                            for (int j = 1; j < values.get(values.size()-1).length; j++){
                                writer.write(values.get(values.size()-1)[j], false);
                            }
                        }
                        values.clear();
                        divider = 1;
                        writer.write(row[PARAM_IDCOLUMN]);
                        if (PARAM_RETAIN){
                            for (int col = PARAM_IDCOLUMN+1; col < PARAM_STARTCOLUMN; col++){
                                writer.write(row[col]);
                            }
                        }
                        ID.add(row[PARAM_IDCOLUMN]);
                        for (int i=PARAM_STARTCOLUMN; i < row.length; i++){
                            line[i-PARAM_STARTCOLUMN+1] = row[i];
                        }
                        values.add(line);
                    } else {
                        String[] line = new String[matrix.getColumnCount()-PARAM_STARTCOLUMN+1];
                        divider++;
                        for (int i=PARAM_STARTCOLUMN; i < row.length; i++){
                            String prevValue = values.get(values.size()-1)[i-PARAM_STARTCOLUMN+1];
                            if (row[i] != null && prevValue != null){
                                line[i-PARAM_STARTCOLUMN+1] = Double.toString(((divider-1)*Double.parseDouble(prevValue)
                                                                               +Double.parseDouble(row[i]))/divider);
                            } else if (row[i] != null && prevValue == null){
                                line[i-PARAM_STARTCOLUMN+1] = Double.toString(Double.parseDouble(row[i])/divider);
                            } else if (row[i] == null && prevValue != null){
                                line[i-PARAM_STARTCOLUMN+1] = Double.toString((divider-1)*Double.parseDouble(prevValue)/divider);
                            } else {
                                line[i-PARAM_STARTCOLUMN+1] = prevValue;
                            }
                        }
                        values.remove(values.size()-1);
                        values.add(line);
                    }
                }
            } else if (cf.inputDefined("thresholds")){
                CSVParser csvp = new CSVParser(tholdFile);
                String[] tholds = null;
                int iter = 0;
                while (csvp.hasNext()){
                    tholds  = csvp.next();
                    iter++;
                    if (PARAM_UPPER && iter == 1) break;
                    if (!PARAM_UPPER  && iter == 2) break;
                }
                while (matrix.hasNext()){ // ITERATE over rows
                    String[] row = matrix.next();
                    String[] line = new String[matrix.getColumnCount()-PARAM_STARTCOLUMN+1];
                    writer.write(row[PARAM_IDCOLUMN]);
                    if (PARAM_RETAIN){
                        for (int col = PARAM_IDCOLUMN+1; col < PARAM_STARTCOLUMN; col++){
                            writer.write(row[col]);
                        }
                    }
                    for (int i=PARAM_STARTCOLUMN; i < row.length; i++){
                        if (row[i] == null){
                            line[i-PARAM_STARTCOLUMN+1] = null;
                        } else if (PARAM_UPPER && Double.parseDouble(row[i]) > Double.parseDouble(tholds[0])){
                            line[i-PARAM_STARTCOLUMN+1] = PARAM_MATCHCHAR;
                        } else if (!PARAM_UPPER && Double.parseDouble(row[i]) < Double.parseDouble(tholds[0])){
                            line[i-PARAM_STARTCOLUMN+1] = PARAM_MATCHCHAR;
                        } else {
                            line[i-PARAM_STARTCOLUMN+1] = "0";
                        }
                     }
                    for (int j = 1; j < line.length; j++){
                        writer.write(line[j], false);
                    }
                }            
            }
            cf.writeLog("Done removing duplicates...");
        } catch (Exception e) {
            cf.writeLog(e.toString());
            return ErrorCode.ERROR;
        } finally {
            try {
                if (!values.isEmpty()){
                    for (int j = 1; j < values.get(values.size()-1).length; j++){
                        writer.write(values.get(values.size()-1)[j], false);
                    }
                }
                writer.close();
            } catch (NullPointerException e) {
                throw (e);
            }
        }
        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new RowJoinComponent().run(args);
    }
}