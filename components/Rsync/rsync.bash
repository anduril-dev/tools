
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command
set -e

RSYNC=rsync
if ( "$RSYNC" --version | grep -i version )
    then echo "Rsync found."
else
    msg="$RSYNC not found."
    echo $msg >&2
    echo $msg >> "$errorfile"
    echo $slsep >> "$errorfile"
    exit 1
fi

command=$( echo "$parameter_command" | sed -e "s,@inFile@,$input_inFile,g" \
    -e "s,@inFolder@,$input_inFolder,g" \
    -e "s,@outFile@,$output_outFile,g" \
    -e "s,@outFolder@,$output_outFolder,g" )

mkdir "$output_outFolder"
touch "$output_outFile"

echo "$RSYNC" $command
"$RSYNC" $command
