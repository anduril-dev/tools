# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh" "$1"


script=$( getinput script )
scriptpar=$( getparameter command )
toscript=$( getoutput scriptOut )
optScript=$( getinput optScript )
tmpdir=$( gettempdir )
echo 'set datafile separator "	"' > "${tmpdir}/script.gnu"
if [ ! -z "${optScript}" ]
then cat "${optScript}" >> "${tmpdir}/script.gnu"
fi   
# input
csv=$( getinput in )
csvindex=$( getinput _index_in )
# output
plot=$( getoutput out )
plotindex=$( getoutput _index_out )

mkdir -p "${plot}" || echo error creating output array >> "$errorfile"
echo -e '"Key"'"\t"'"File"' >> "${plotindex}"
cd "${plot}"

GNUPLOT=$( which gnuplot )
[ $? -gt 0 ] && {
    echo gnuplot binary not in path >> "$errorfile"
    exit 1
}
$GNUPLOT --version

# Default separator, tab
echo 'set datafile separator "	"' > "${toscript}"
if [ ! -z "$scriptpar" ]
then echo -e "${scriptpar}" >> "${toscript}"
else cat "${script}" >> "${toscript}"
fi

sed -i -e 's,@optScript@,"'"${tmpdir}/script.gnu"'",g' "${toscript}"
# replace input array
IFS=$'\n'
keys=( $( getarraykeys in ) )
files=( $( getarrayfiles in ) )
for (( i=0; i<${#keys[@]};i++ ))
do
    #echo Index: $i Key: ${keys[$i]} File: $( basename ${files[$i]} )
    sed -i -e 's,@input_'${keys[$i]}'@,"'${files[$i]}'",g' "${toscript}"
    sed -i -e 's,@input_'${keys[$i]}'@,"'${files[$i]}'",g' "${tmpdir}/script.gnu"
done
# parse and replace output array
for match in $( grep -ohs "@output_[^\ ]\+@" "${toscript}" "${optScript}" | sed 's, ,\n,g' | sort -u )
do newfile=$( echo $match | sed -e 's,^@output_,,' -e 's,@$,,' | tr -d [:cntrl:] )
   newkey=$( echo $newfile | sed -e 's,\..*,,' | tr -d [:cntrl:] | tr -c [:alnum:] _ )
   echo -e $newkey"\t"$newfile >> "${plotindex}"
   sed -i -e 's,'${match}',"'${newfile}'",g' "${toscript}"
   sed -i -e 's,'${match}',"'${newfile}'",g' "${tmpdir}/script.gnu"
done

export FIT_LOG="${tmpdir}"/fit.log

"$GNUPLOT" "$toscript" 
exit_status=$?

# Generate the indeces if not user generated:
no_array=$( tail -n +2 "$plotindex" | wc -l )
[ $no_array -eq 0 ] && {
    createarrayindex plot
}

if [ ! -z ${optScript} ]
then echo "#=========== optScript ============" >> "$toscript"
     cat "${tmpdir}/script.gnu" >> "$toscript" 
fi

if [ "$exit_status" -gt "0" ]
then to_file="$errorfile"
    echo "Exit status: $exit_status" >> "$to_file"
fi

exit $exit_status
