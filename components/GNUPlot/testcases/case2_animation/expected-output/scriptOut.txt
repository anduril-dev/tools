set datafile separator "	"
set grid
# set output to an animated GIF
set term gif animate
set output "animation.gif"
set title 'Rotation'
set xlabel 'X'
set ylabel 'Y'
set zlabel 'Z'
# set axis limits
set xrange [-8:8]
set yrange [-8:8]
set zrange [0:8]
# do not draw tick marks
unset xtics
unset ytics
unset ztics
# draw axis at zero
set xzeroaxis linetype 1 linecolor rgb "black"
set yzeroaxis linetype 1 linecolor rgb "black"
set zzeroaxis linetype 1 linecolor rgb "black"
set border 0
# Define number of frames, and a step size over 360 degree view point
n=20   
dt=360/n
i=0
set view 60,0
# 3D scatter plot with two data sets, introducing use of shell commands
splot '<( tail -n +2 {{.*}} )'  title "reds" with points pointtype 7 pointsize 3,\
      {{.*}} every ::1 using 3:1:2 title "greens" with points pointtype 7 pointsize 3
# call the other script for iteration
load {{.*}}
#=========== optScript ============
set datafile separator "	"
# Advance and change the view point
i=i+1
set view 60,i*dt
# replotting will save a new frame in the GIF sequence
replot
# reread the file if sequence not over
if (i+1 < n) reread
