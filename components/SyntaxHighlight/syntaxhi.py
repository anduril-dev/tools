#!/usr/bin/python
import anduril
from anduril.args import *
from anduril import *
import os,re,pygments
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.lexer import RegexLexer
from pygments.formatters import HtmlFormatter
from pygments.formatters import ImageFormatter
from pygments.formatters import LatexFormatter
from pygments.formatters.img import FontNotFound

class AndurilLexer(RegexLexer):
    """
    For `AndurilScript <http://www.anduril.org/>`_ source code.
    Anduril version 1.x
    Modifier from JavaLexer
    """

    from pygments.lexer import bygroups, using, this
    from pygments.token import Text, Comment, Operator, Keyword, Name, String, \
     Number, Punctuation
    name = 'Anduril'
    aliases = ['anduril','andurilscript']
    filenames = ['*.and']

    flags = re.MULTILINE | re.DOTALL

    tokens = {
        'root': [
            # method names
            (r'^(\s*(?:[a-zA-Z_][a-zA-Z0-9_\.\[\]<>]*\s+)+?)' # return arguments
             r'([a-zA-Z_][a-zA-Z0-9_]*)'                      # method name
             r'(\s*)(\()',                                    # signature start
             bygroups(using(this), Name.Function, Text, Operator)),
            (r'[^\S\n]+', Text),
            (r'^#.*?\n', Comment.Single),
            (r'//.*?\n', Comment.Single),
            (r'/\*.*?\*/', Comment.Multiline),
            (r'@[a-zA-Z_][a-zA-Z0-9_\.]*', Name.Decorator),
            (r'(break|case|catch|continue|else|for|'
             r'if|record|return|switch)\b',
             Keyword),
            (r'(INPUT|OUTPUT)\b', Keyword.Declaration),
            (r'(boolean|float|int|'
             r'string|record|concat|std|echo|enumerate|exists|fail|'
             r'fRead|iterArray|itercsv|iterdir|length|lookup|makeArray|'
             r'metadata|mod|nRows|pow|quote|range|recordToString|split|'
             r'strReplace|substring|BinaryFile|BinaryFolder|CSV|CSVList|'
             r'ImageList|Matrix|IDList|SetList)\b',
             Keyword.Type),
            (r'(package)(\s+)', bygroups(Keyword.Namespace, Text)),
            (r'(true|false|null)\b', Keyword.Constant),
            (r'(class|interface)(\s+)', bygroups(Keyword.Declaration, Text), 'class'),
            (r'(import)(\s+)', bygroups(Keyword.Namespace, Text), 'import'),
            (r'^(\s*)([rRuU]{,2}"""(?:.|\n)*?""")', bygroups(Text, String.Doc)),
            (r"^(\s*)([rRuU]{,2}'''(?:.|\n)*?''')", bygroups(Text, String.Doc)),
            (r'"(\\\\|\\"|[^"])*"', String),
            (r"(?s)\$?'(\\\\|\\[0-7]+|\\.|[^'\\])*'", String.Single),
            (r"'\\.'|'[^\\]'|'\\u[0-9a-fA-F]{4}'", String.Char),
            (r'(\.)([a-zA-Z_][a-zA-Z0-9_]*)', bygroups(Operator, Name.Attribute)),
            (r'[a-zA-Z_][a-zA-Z0-9_]*:', Name.Label),
            (r'[a-zA-Z_\$][a-zA-Z0-9_]*', Name),
            (r'[~\^\*!%&\[\]\(\)\{\}<>\|+=:;,./?-]', Operator),
            (r'[0-9][0-9]*\.[0-9]+([eE][0-9]+)?[fd]?', Number.Float),
            (r'0x[0-9a-fA-F]+', Number.Hex),
            (r'[0-9]+L?', Number.Integer),
            (r'\n', Text)
        ],
        'class': [
            (r'[a-zA-Z_][a-zA-Z0-9_]*', Name.Class, '#pop')
        ],
        'import': [
            (r'[a-zA-Z0-9_.]+\*?', Name.Namespace, '#pop')
        ],
    }

write_log('Pygments version: '+pygments.__version__)
param_language=param_language.lower()

if param_language=="anduril":
    #lexer=AndurilLexer(stripall=True)
    param_language="scala"
lexer = get_lexer_by_name(param_language, stripall=True)

# HTML
HTML_formatter = HtmlFormatter(linenos=param_rowNumbers, cssclass="highlight")
snippet=highlight(open(input_in, 'r').read(), lexer, HTML_formatter)
style=HtmlFormatter().get_style_defs('.highlight')

writer=open(output_HTML, 'w')
writer.write("""<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<head>
    <title>"""+param_title+"""</title>
    <style type="text/css">
""")
writer.write(style)
writer.write("""
    </style>
</head>
<body>
""")
writer.write(snippet)
writer.write("""
</body></html>
""")
writer.close()

# PNG
try:
    PNG_formatter  = ImageFormatter(image_format="PNG",
                                font_name=param_PNGFont,
                                font_size=param_PNGSize,
                                line_pad=param_PNGPad,
                                line_numbers=param_rowNumbers)
    found_font=True
except FontNotFound as fontErr:
    write_log('Font "'+param_PNGFont+'" not found. Using first matching Mono font.')
    from subprocess import check_output
    font_list= check_output(["fc-list","-f",'%{family}\n'])
    found_font=False
    for default_font in font_list.split('\n'):
        default_font=default_font.split(',')[0]
        if "mono" in default_font.lower():
            try: 
                PNG_formatter  = ImageFormatter(image_format="PNG",
                                            font_name=default_font,
                                            font_size=param_PNGSize,
                                            line_pad=param_PNGPad,
                                            line_numbers=param_rowNumbers)
                found_font=True
                break
            except FontNotFound as fontErr:
                continue
    if found_font:
        write_log('Using font "'+default_font+'"')
    else:
        write_log('Can not find Monospace fonts!')
    
    
os.makedirs(output_image)
writer=open(os.path.join(output_image, param_title+".png") , 'w')
if found_font:
    writer.write(highlight(open(input_in, 'r').read(), lexer, PNG_formatter))
writer.close()

# LaTeX
TeX_formatter  = LatexFormatter(linenos=param_rowNumbers,
                                commandprefix="PYGMENTS")
defs=LatexFormatter(commandprefix="PYGMENTS").get_style_defs()
os.makedirs(output_latex)
writer=open(os.path.join(output_latex, "document.tex") , 'w')
writer.write(defs)
writer.write(highlight(open(input_in, 'r').read(), lexer, TeX_formatter))
writer.close()
