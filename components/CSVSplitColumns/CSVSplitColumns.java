import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.IndexFile;
import org.anduril.component.SkeletonComponent;
import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;


public class CSVSplitColumns extends SkeletonComponent {

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		CSVParser in = new CSVParser(cf.getInput("in"));
		String [] keyCols = cf.getParameter("keyCols").split(",");

		cf.getOutput("out").mkdirs();
        IndexFile indexFile = new IndexFile();
        
        ArrayList<CSVWriter> out = new ArrayList<CSVWriter>(in.getColumnCount()-keyCols.length);
		
		String [] cols = new String[keyCols.length + 1];
		HashSet<String> keyCollection = new HashSet<String>();
		
		for(int i = 0; i < keyCols.length; i++){
			cols[i] = keyCols[i];
			keyCollection.add(keyCols[i]);
		}
		
		for(String column : in.getColumnNames()){
			if(!keyCollection.contains(column)){
				
				cols[cols.length-1] = column;
				
				File outfile = new File(cf.getOutput("out") + "/" +column+".csv");
				
				out.add(new CSVWriter(cols, outfile));
				indexFile.add(column, outfile.getAbsoluteFile());
			}
		}
		
		while(in.hasNext()){
			String [] line = in.next();
			
			int i = 0;
			for(String column : in.getColumnNames()){
				if(!keyCollection.contains(column)){
					
					for(String key : keyCols){
						out.get(i).write(line[in.getColumnIndex(key)]);
					}
					out.get(i).write(line[in.getColumnIndex(column)]);
				
					i++;
				}
			}
			
		}
		
		for(CSVWriter o: out){
			o.close();
		}
		
		indexFile.write(cf, "out");
		
		return ErrorCode.OK;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new CSVSplitColumns().run(args);
	}

}
