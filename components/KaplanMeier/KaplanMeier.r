library(componentSkeleton)
require(survival)

execute <- function(cf) {
  # Initialization...
  line.colors   <- c("blue","green","red","violet","black","yellow","brown","gray","pink")
  width.inches  <- 15 / 2.54
  height.inches <-  9 / 2.54
  statKM        <- data.frame(group  =character(0),
                              factors=character(0),
                              n      =character(0),
                              pValue =numeric(0),
                              plot   =character(0),
                              stringsAsFactors=FALSE)
  p.value.limit <- get.parameter(cf, 'PLimit',       type='float')
  timeOutLimit  <- get.parameter(cf, 'timeOutLimit', type='float')
  printTable    <- get.parameter(cf, 'printTable',   type='boolean')
  conf.int      <- get.parameter(cf, 'confInt',      type='float')
  minCount      <- get.parameter(cf, 'minCount',     type='int')
  minGroups     <- get.parameter(cf, 'minGroups',    type='int')
  sTitle        <- get.parameter(cf, 'title')
  xLabel        <- get.parameter(cf, 'xLabel')
  instanceName  <- get.metadata(cf, 'instanceName')
  out.dir       <- get.output(cf, 'report')
  docName       <- file.path(out.dir, LATEX.DOCUMENT.FILE)
  docBuffer     <- paste('% KaplanMeier curves by', instanceName)

  dir.create(out.dir, recursive=TRUE)

  # Read survival times...
  write.log(cf, "Reading survival times...")
  colTime   <- get.parameter(cf, 'timeCol')
  colStatus <- get.parameter(cf, 'statusCol')
  colGroup  <- get.parameter(cf, 'groupCol')
  survData  <- CSV.read(get.input(cf, 'survival'))
  survData  <- survData[!is.na(survData[,colStatus]),]
  write.log(cf, sprintf("Survival data were available for %i samples.",nrow(survData)))
  survData[,colStatus] <- (survData[,colStatus] == get.parameter(cf, 'eventSymbol'))

  # Prepare conditional model variable output for Cox model
  strataCols   <- get.parameter(cf, 'strataCols', type='string')
  stratas      <- unlist(strsplit(strataCols,","))
  modelVarCols <- get.parameter(cf, 'modelVarCols', type='string')
  modelVars    <- unlist(strsplit(modelVarCols,","))
  contiCols    <- get.parameter(cf, 'contiCols', type='string')
  contiVars    <- unlist(strsplit(contiCols,","))
  if (nchar(modelVarCols) > 0 || nchar(strataCols) > 0){
     statKM <- cbind(statKM, numeric(0), numeric(0), numeric(0), numeric(0), numeric(0))
     colnames(statKM)[6]  <- "pValueCox"
     colnames(statKM)[7]  <- "logLikeP"
     colnames(statKM)[8]  <- "p.zph"
     colnames(statKM)[9]  <- "nonPropVars"
     colnames(statKM)[10] <- paste("coef_", colStatus, sep="")
     if (nchar(modelVarCols) > 0){
         for (var in 1:length(modelVars)){
            statKM <- cbind(statKM, numeric(0))
            colnames(statKM)[10+var] <- paste("coef_", modelVars[var], sep="")
         }
     } else {
         for (var in 1:length(stratas)){
            statKM <- cbind(statKM, numeric(0))
            colnames(statKM)[10+var] <- paste("coef_", stratas[var], sep="")
         }
     }
  }

  # Cut study period after the time out limit
  if (timeOutLimit >= 0) {
     longTimes                          <- (survData[, colTime] > timeOutLimit)
     longTimes[which(is.na(longTimes))] <- FALSE
     survData[longTimes, colTime]       <- timeOutLimit
     survData[longTimes, colStatus]     <- 0
  }

  # Read annotations...
  survAnnot <- get.input(cf, 'annotation')
  if (nchar(survAnnot) > 0) {
     survAnnot  <- CSV.read(survAnnot)
     annot.cols <- get.parameter(cf, 'annotCol')
     if (annot.cols=='*') {
        annot.cols <- colnames(survAnnot)[-1]
     } else {
        annot.cols <- unlist(strsplit(annot.cols,','))
        survAnnot  <- survAnnot[,c(colnames(survAnnot)[1],annot.cols)]
     }
     annot.attr <- paste('\\textit{',latex.quote(annot.cols),'}',sep='')
  } else {
     survAnnot <- NULL
  }

  if ('*' == colGroup) {
     groupNames <- colnames(survData)
     groupNames <- setdiff(groupNames, c(colTime, colStatus))
     groupNames <- setdiff(groupNames, strsplit(get.parameter(cf, 'excludeCol'),',',fixed=TRUE)[[1]])
  } else {
     groupNames <- strsplit(colGroup,',',fixed=TRUE)[[1]]
     if (length(groupNames) < 1) groupNames <- ''
  }

  plotCount <- 1
  for (groupName in groupNames) { # PLOT ITERATION LOOP

  if (groupName %in% stratas || groupName %in% modelVars) next
  # Statistical comparison of survival between the sample groups
  p.value <- NA
  if (nchar(groupName) > 0) {
     # Filter out too small sample groups
     if (minCount > 0) {
        grpF <- as.factor(survData[,groupName])
        grps <- summary(grpF)
        grps <- grps[grps < minCount]
        if (length(grps) > 0) {
           survData[,groupName] <- factor(grpF, levels=setdiff(levels(grpF), names(grps)))
        }
     }
     if (minGroups > 0) {
        grpF <- as.factor(survData[,groupName])
        if (length(levels(grpF)) < minGroups) next # Too few groups left
     }

     surv.frm <- eval(parse(text=sprintf('Surv(%s, %s) ~ %s',colTime,colStatus,groupName)))
     s.test   <- try( survdiff(surv.frm, data=survData), silent=TRUE )
     if (class(s.test) == "try-error") {
        if (p.value.limit < 1) next # Skip all items without statistics
        testMsg <- as.character(s.test)
        testMsg <- substr(testMsg, regexpr(":", testMsg)[1]+1, nchar(testMsg))
        testMsg <- paste(' Log-rank statistics could not be applied (',
                         trim(testMsg), ').', sep='')
     } else {
        p.value <- 1-pchisq(s.test$chisq, length(s.test$n)-1)
        testMsg <- sprintf(' Log-rank probability measure for the equality of these curves is %s.',
                           format(p.value,scientific=TRUE))
     }
  } else {
     surv.frm  <- eval(parse(text=sprintf('Surv(%s, %s) ~ 1',colTime,colStatus)))
     groupName <- 'observations'
     testMsg   <- ''
  }
 
  # Conditional Cox analysis
  p.value.cox <- vector()
  if (nchar(modelVarCols) > 0 || nchar(strataCols) > 0){
     survDataCox <- survData[,c(colTime, colStatus, groupName)]
     colnames(survDataCox) <- c(colTime, colStatus, groupName)
     if (nchar(modelVarCols) > 0){
         survDataCox <- cbind(survDataCox, survData[,modelVars[1:length(modelVars)]])
         colnames(survDataCox)[(length(colnames(survDataCox))-length(modelVars)+1):length(colnames(survDataCox))] <- modelVars[1:length(modelVars)]
     }
     if (nchar(strataCols) > 0){
         survDataCox <- cbind(survDataCox, survData[,stratas[1:length(stratas)]])
         colnames(survDataCox)[(length(colnames(survDataCox))-length(stratas)+1):length(colnames(survDataCox))] <- stratas[1:length(stratas)]
     }
     surv.frm.pre  <- parse(text=sprintf('Surv(%s, %s) ~ %s',colTime,colStatus,groupName))
     # Transform grouping to integer
     if (!is.numeric(survDataCox[,groupName])){
         if (groupName %in% contiVars){
             survDataCox[,groupName] <- as.numeric(survDataCox[,groupName])
         } else {
             survDataCox[,groupName] <- as.character(survDataCox[,groupName])
         }
     }
     newData <- data.frame(unique(survDataCox[,groupName]))
     colnames(newData)[ncol(newData)] <- groupName

     # Modify model variables
     if (nchar(modelVarCols) > 0){
      for (model in 1:length(modelVars)){
        if (!is.numeric(survDataCox[,modelVars[model]])){
           if (modelVars[model] %in% contiVars){
               survDataCox[,modelVars[model]] <- as.numeric(survDataCox[,modelVars[model]])
           } else {
               survDataCox[,modelVars[model]] <- as.character(survDataCox[,modelVars[model]])
           }
        }
        surv.frm.pre <- paste(surv.frm.pre, modelVars[model], sep=" + ")
        newData <- cbind(newData, rep(mean(as.numeric(survDataCox[,modelVars[model]]), na.rm=TRUE), length(unique(survDataCox[,groupName]))))
        colnames(newData)[ncol(newData)] <- modelVars[model]
      }
     }
     if (nchar(strataCols) > 0){
      for (stratum in 1:length(stratas)){
        if (!is.numeric(survDataCox[,stratas[stratum]])){
           if (stratas[stratum] %in% contiVars){
               survDataCox[,stratas[stratum]] <- as.numeric(survDataCox[,stratas[stratum]])
           } else {
               survDataCox[,stratas[stratum]] <- as.character(survDataCox[,stratas[stratum]])
           }
        }
        model.str <- paste("strata(", stratas[stratum], ")", sep="")
        surv.frm.pre <- paste(surv.frm.pre, model.str, sep=" + ")
      }
     }

     if (all(is.na(survDataCox[,groupName]))){
         write.log(cf, sprintf("No non-missing observations available for group %s. Skipping.", groupName))
         next
     }

     surv.frm.cox <- as.formula(surv.frm.pre)
     mod.coxph <- try( coxph(surv.frm.cox, data = survDataCox), silent=TRUE)
     if (class(mod.coxph) == "try-error") {
         print(sprintf('%s: %s', groupName, mod.coxph))
         next
     }
     ans <- summary(mod.coxph)
     zph <- cox.zph(mod.coxph)$table

     groupRows <- which(substring(rownames(zph), 1, nchar(groupName)) == groupName)

     if (length(groupRows) > 0){
        if (all(is.na(zph[groupRows, 'p']))) next
        if (any(zph[groupRows, 'p'] > 0.05)){
            p.value.cox[1] <- ans$coefficients[1,5]
            logLikeP <- ans$logtest['pvalue']
        } else {
            p.value.cox[1] <- NA
            logLikeP <- NA
        }
     } else {
         p.value.cox[1] <- NA
         logLikeP <- NA
     }
     nonPropVars <- paste(rownames(zph)[which(zph[,'p'] <= 0.05)], collapse=",")
     if (nchar(nonPropVars) < 1) nonPropVars <- NA

     for (i in 1:nrow(ans$coefficients)){
        p.value.cox[i+1] <- ans$coefficients[i,1]
     }
     testMsg <- paste(testMsg, "The Cox proportional hazards ratio is", round(exp(p.value.cox[2]), 5), 
                               "with a p-value of", round(p.value.cox[1], 4), ".", sep = " ")
     is.filtered <- (is.na(p.value.cox[1]) || (!is.na(p.value.cox[1]) && p.value.cox[1] > p.value.limit))
  } else {
     is.filtered <- (!is.na(p.value) && p.value > p.value.limit)
  }
  if (conf.int < 0){
    conf.type <- 'none'
  } else {
    conf.type <- 'log'
  }
  # Fit model for plotting
  if (modelVarCols == "" || strataCols == ""){
     fit <- survfit(surv.frm, data=survData, conf.int=conf.int, conf.type=conf.type)
     g.strata <- fit$strata
     g.names  <- sapply(names(g.strata), FUN=function(x){ strsplit(x,'=',fixed=TRUE)[[1]][2] })
     g.legend <- paste(g.names, paste('(',fit$n,')',sep=''))
     out.n <- fit$n
  } else {
     #fit <- survfit(mod.coxph, newdata=newData, conf.int=conf.int)
     labFit <- survfit(surv.frm, data=survData, conf.int=conf.int, conf.type=conf.type)
     fit <- labFit
     g.strata <- labFit$strata
     g.names  <- sapply(names(g.strata), FUN=function(x){ strsplit(x,'=',fixed=TRUE)[[1]][2] })
     g.legend <- paste(g.names, paste('(',labFit$n,')',sep=''))
     out.n <- labFit$n
  }
  # Print number of samples at risk at six even intervals
  if (printTable){
    tPoints <- round(seq(0, max(survData[,colTime], na.rm=TRUE), max(survData[,colTime], na.rm=TRUE)/6))
    fitIntervals <- matrix(NA, nrow=length(fit$n), ncol=7)
    pInd <- numeric(length=2)
    for (s in 1:length(fit$n)){
      if (s == 1){ pInd <- c(1,fit$n[s]) } else { pInd <- c(pInd[2]+1,pInd[2]+fit$n[s]) }
      offsetColsE <- which(fit$n.event[pInd[1]:pInd[2]] > 1)
      offsetColsC <- which(fit$n.censor[pInd[1]:pInd[2]] > 1)
      offset1 <- sum(fit$n.event[pInd[1]:pInd[2]][offsetColsE])-length(offsetColsE)
      offset2 <- sum(fit$n.censor[pInd[1]:pInd[2]][offsetColsC])-length(offsetColsC)
      offset  <- offset1+offset2
      pInd[2] <- pInd[2]-offset
      for (i in 1:length(tPoints)){
        ind <- which(fit$time[pInd[1]:pInd[2]] < tPoints[i])
        if (length(ind) > 0){
          fitIntervals[s,i] <- min(fit$n.risk[pInd[1]:pInd[2]][ind], na.rm=TRUE)
        } else {
          if (tPoints[i] == 0 || (i > 1 & fitIntervals[s,i-1] == fit$n[s])){
            fitIntervals[s,i] <- fit$n[s]
          } else {
            fitIntervals[s,i] <- 0
          }
        }
      }
    }
    colnames(fitIntervals) <- paste('time=', signif(tPoints, 5), sep='')
    rownames(fitIntervals) <- names(fit$strata)
  }

  max.time <- max(fit$time)
  if (length(fit$na.action) > 0) {
     missMsg <- paste(" Total of ",naprint(fit$na.action),".",sep="")
  } else {
     missMsg <- ''
  }


  if (is.filtered) {
    fig.name <- NA
  } else {
    fig.name <- sprintf('%s-KM%s.pdf', instanceName, plotCount)
  }

  # Add statistics...
  statKM.row           <- nrow(statKM)+1
  statKM[statKM.row,1] <- groupName
  statKM[statKM.row,2] <- paste(g.names,collapse=',')
  statKM[statKM.row,3] <- paste(out.n,collapse=',')
  statKM[statKM.row,4] <- p.value
  statKM[statKM.row,5] <- fig.name
  if (nchar(modelVarCols) > 0 || nchar(strataCols) > 0){
     statKM[statKM.row, 6] <- p.value.cox[1]
     statKM[statKM.row, 7] <- logLikeP
     statKM[statKM.row, 8] <- min(zph[groupRows,'p'], na.rm=TRUE)
     statKM[statKM.row, 9] <- nonPropVars
     if (nchar(modelVarCols) > 0){
         for (i in 1:(length(modelVars)+1)){
           statKM[statKM.row, 9+i] <- p.value.cox[i+1]
         }
      } else {
         for (i in 1:(length(stratas)+1)){
           statKM[statKM.row, 9+i] <- p.value.cox[i+1]
         }
     }
  }
  
  if (is.filtered) next # We shall skip plotting this item as the curves do not differ.
  
  # Create graph...
  fig.capt <- sprintf('Each curve represents the relative number of %s survived to the corresponding point of time.',
                      latex.quote(groupName))
  fig.path <- file.path(out.dir, fig.name)
  pdf(fig.path, paper='special', width=width.inches, height=height.inches)
  par(pch=20, cex=0.7, las=2)
  plot(fit,
       mark.time = (modelVarCols == ""),
       col  = line.colors,
       main = sTitle,
       xlab = xLabel)
  if (conf.int > 0) {
     lines(fit, col=line.colors[1:length(out.n)], lty=3, conf.int='only')
     fig.capt <- paste(fig.capt,
                       sprintf('Dashed lines represent %g confidence intervals for', conf.int),
                       'the curves with the corresponding color.')
  }
  if (length(g.legend) > 0) {
     legend(x=max.time/30.0, y=0.1+0.06*length(g.legend), legend=g.legend, col=line.colors, lty=1, title=groupName)
  }
  invisible(dev.off())

  if (is.null(survAnnot)) {
     fig.annot <- ''
  } else {
     fig.annot <- survAnnot[survAnnot[,1]==groupName,-1]
     annot.col <- !is.na(fig.annot)
     fig.annot <- fig.annot[annot.col]
     if (length(fig.annot) > 0) {
        fig.annot <- paste(annot.attr[annot.col],
                           latex.quote(as.character(fig.annot)),
                           sep='=', collapse=', ')
        fig.annot <- paste(' Additional annotations: ',fig.annot, '.', sep='')
     } else {
        fig.annot <- ''
     }
  }
  fig.capt <- paste(fig.capt, missMsg, testMsg, fig.annot, sep='')

  if (printTable){
    fig.table <- latex.tabular(fitIntervals, column.alignment='lccccccc', use.row.names=TRUE, numberFormat='%.d')
    fig.table <- c('\\begin{center}', fig.table, '\\end{center}', '\\FloatBarrier{}', '')
    fig.capt  <- paste(fig.capt, 'Table shows the number of patients at risk at even intervals for each curve.')
  } else {
    fig.table <- ''
  }
  docBuffer <- c(docBuffer,
                 latex.figure(fig.name, caption = fig.capt, quote.capt = FALSE),
                 fig.table,
                 if (plotCount %% 6 == 0) '\\FloatBarrier{}')

  plotCount <- plotCount+1

  } # END OF PLOT ITERATION LOOP

  # Print out LaTeX...
  cat(docBuffer, sep="\n", file=docName)

  # Write statistics...
  CSV.write(get.output(cf, 'statistics'), statKM)

  # Write figure index...
  plotsOut <- statKM[!is.na(statKM[,'plot']),c('group', 'plot')]
  plotsOut <- Array.new(keys  = plotsOut[,1],
                        files = paste('../report/',plotsOut[,2],sep=''))
  dir.create(get.output(cf, 'plots'))
  Array.write(cf, plotsOut, 'plots')

  w <- warnings()
  if (!is.null(w)) {
     write.log(cf, w)
  }
  return(0)
}

main(execute)
