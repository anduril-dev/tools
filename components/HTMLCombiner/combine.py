import sys,os
import anduril
from anduril.args import *
import shutil

def writeArrays(arrayIn, index):
    for a in arrayIn:
        arrayFile = open(arrayIn[a], 'r')
        for line in arrayFile:
            index.write(line)

            
cf = anduril.CommandFile.from_file(sys.argv[1])
website = cf.get_output('out')
os.mkdir(website)

body = None
head = None
if(cf.get_input('body') != None):
    body = open(cf.get_input('body'), 'r')
if(cf.get_input('head') != None):
    head = open(cf.get_input('head'), 'r')

index = open(website+'/index.html', 'w')
index.write('<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\n')
index.write(' \"http://www.w3.org/TR/html4/strict.dtd\">')
index.write('<html lang=\"en\">')
index.write('<head>')
index.write('<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">')


if(cf.get_input('style') != None):
    shutil.copyfile(cf.get_input('style'), website + '/style.css')
    index.write('<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">')

if(cf.get_input('script') != None):
    shutil.copyfile(cf.get_input('script'), website + '/script.js')
    index.write('<script language=\"javascript\" src=\"script.js\"></script>')

if(scriptArray != None):
    for a in scriptArray:
        shutil.copyfile(scriptArray[a], website + '/' + a + '.js')
        index.write('<script language=\"javascript\" src=\"' + a + '.js\"></script>')

if(styleArray != None):
    for a in styleArray:
        shutil.copyfile(styleArray[a], website + '/' + a + '.css')
        index.write('<link rel=\"stylesheet\" type=\"text/css\" href=\"' + a + '.css\">')


if(head != None):
    for line in head:
        index.write(line)

if headArray != None:
    writeArrays(headArray, index)
        
index.write(cf.get_parameter('headPar'))
index.write('</head>')
index.write('<body>')

if(body != None):
    for line in body:
        index.write(line)

if bodyArray != None:
    writeArrays(bodyArray, index)

index.write(cf.get_parameter('bodyPar'))
index.write('</body>')


if(cf.get_input('subFolder') != None):
    for dir in os.listdir(cf.get_input('subFolder')):
        if os.path.isdir(os.path.join(cf.get_input('subFolder'), dir)):
            shutil.copytree(os.path.join(cf.get_input('subFolder'), dir), os.path.join(website, dir))
        elif os.path.isfile(os.path.join(cf.get_input('subFolder'), dir)):
            shutil.copy(os.path.join(cf.get_input('subFolder'), dir), os.path.join(website, dir))
