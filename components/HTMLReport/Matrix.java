import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.primitives.ArrayDoubleList;
import org.apache.commons.collections.primitives.DoubleList;

public class Matrix {
    private String name;
    private String label;
    private int summaryThreshold;
    private List<String> columnNames;
    private List<String> rowNames;
    private ArrayDoubleList data;
    
    public Matrix(String name, String label, int summaryThreshold) {
        this.name = name;
        this.label = label;
        this.summaryThreshold = summaryThreshold;
        this.rowNames = new ArrayList<String>();
        this.data = new ArrayDoubleList();
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getLabel() {
        return this.label;
    }
    
    public List<String> getColumnNames() {
        return this.columnNames;
    }
    
    public void setColumnNames(List<String> names) {
        this.columnNames = names;
    }

    public List<String> getRowNames() {
        return this.rowNames;
    }
    
    public int getNumColumns() {
        if (this.columnNames == null) return 0;
        else return this.columnNames.size();
    }
    
    public int getNumRows() {
        if (this.rowNames == null) return 0;
        else return this.rowNames.size();
    }
    
    public void addRow(String rowName, double[] data) {
        if (data.length != getNumColumns()) {
            String msg = String.format(
                    "Matrix row %s contains an incorrect number of items: expected %d, got %d",
                    rowName, getNumColumns(), data.length);
            throw new IllegalArgumentException(msg);
        }
        this.rowNames.add(rowName);
        for (double d: data) this.data.add(d);
    }
    
    public void trim() {
        this.data.trimToSize();
    }

    public DoubleList getRow(String rowName) {
        final int pos = this.rowNames.indexOf(rowName);
        if (pos < 0) return null;
        
        final int COLUMNS = getNumColumns();
        final int begin = pos*COLUMNS;
        return this.data.subList(begin, begin+COLUMNS);
    }
    
    public DoubleList getColumn(String columnName) {
        final int pos = this.columnNames.indexOf(columnName);
        if (pos < 0) return null;

        final int COLUMNS = getNumColumns();
        final int ROWS = getNumRows();
        
        DoubleList dl = new ArrayDoubleList();
        int index = pos;
        for (int row=0; row<ROWS; row++) {
            dl.add(this.data.get(index));
            index += COLUMNS;
        }
        return dl;
    }
    
    public boolean overThreshold(DoubleList list) {
        return list.size() >= this.summaryThreshold;
    }
    
    public String formatSummary(DoubleList list) {
        final int N = list.size();
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        double sum = 0;
        int numDefined = 0;
        
        for (int i=0; i<N; i++) {
            final double value = list.get(i);
            if (Double.isNaN(value)) continue;
            if (value < min) min = value;
            if (value > max) max = value;
            sum += value;
            numDefined++;
        }
        
        double mean = (numDefined == 0) ? 0 : sum / numDefined;
        
        return String.format("N=%d min=%.2g mean=%.2g max=%.2g",
                numDefined, min, mean, max);
    }
}
