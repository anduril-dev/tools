# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh" "$1"
export_command
set -e

iscmd pandoc || {
    echo pandoc not installed.
    exit 1
}

IFS=$'\n'

PANDOC=$( which pandoc )
"$PANDOC" -v

if [ "$parameter_from" = "CSV" ]; then
    iscmd ncsv || {
      echo ncsv not installed.
      exit 1
    }
    NCSV=$( which ncsv  )
    echo -n "NCSV version: " 
    "$NCSV" -v 2>&1
fi

mkdir "$output_out"
OPTS=()
tmpdir=$( gettempdir )

if [ -e "$input_header" ]
then for f in $( getarrayfiles header )
     do OPTS+=( '-H' "$f" )
     done
fi

if [ -e "$input_after" ]
then for f in $( getarrayfiles after )
     do OPTS+=( '-A' "$f" )
     done
fi

if [ -e "$input_attachment" ]
then writelog "Copying attachments"
    for k in $( getarraykeys attachment ); do 
        cp -rv $( getarrayfile attachment "$k" ) "$tmpdir"/"$k"
        if [ "$parameter_copyAttachments" = "true" ]; then
            cp -rv $( getarrayfile attachment "$k" ) "$output_out"/"$k"
            addarray out "$k" "$k"
        fi
    done
fi

shopt -s nocasematch
cd "$tmpdir"
if [ "$parameter_concatenate" = "true" ]
then true
# Concatenate results in to one output file

    if [ "$parameter_from" = "CSV" ]
    then echo "$NCSV" -s "$parameter_CSVFormat" --cf m -a auto $( getarrayfiles in ) 
         eval "$NCSV" -s "$parameter_CSVFormat" --cf m -a auto $( getarrayfiles in ) > "$tmpdir"/_tmp.md
         INPUTS="$tmpdir"/_tmp.md
         parameter_from=markdown
    else
         INPUTS=( $( getarrayfiles in ) )
    fi
    
    OPTS+=( "--from" ) 
    OPTS+=( "$parameter_from" ) 

    echo "$PANDOC" "${OPTS[@]}" $parameter_switches -o "$output_out"/"$parameter_basename"."$parameter_to" "${INPUTS[@]}"
    eval "$PANDOC" "${OPTS[@]}" $parameter_switches -o "$output_out"/"$parameter_basename"."$parameter_to" "${INPUTS[@]}"
    exitcode=$?
    addarray out "$parameter_basename" "$parameter_basename"."$parameter_to"
    exit $?

else
#  For-loop over input arrays

    in_arr=( $( getarrayfiles in ) )
    exitcode=0
    for k in $( getarraykeys in )
    do  for_from=$parameter_from
        k_i=$( getarraykeyindex in "$k" )
        if [ "$parameter_from" = "CSV" ]
        then echo "$NCSV" -s "$parameter_CSVFormat" --cf m -a auto ${in_arr[$k_i]}
             eval "$NCSV" -s "$parameter_CSVFormat" --cf m -a auto ${in_arr[$k_i]} > "$tmpdir"/"$k".md
             INPUT="$tmpdir"/"$k".md
             for_from="markdown"
        else
             INPUT=${in_arr[$k_i]}
        fi

        echo "$PANDOC" "${OPTS[@]}" --from "$for_from" $parameter_switches -o "$output_out"/"$k"."$parameter_to" "${INPUT}"
        eval "$PANDOC" "${OPTS[@]}" --from "$for_from" $parameter_switches -o "$output_out"/"$k"."$parameter_to" "${INPUT}"
        
        exitcode=$(( $exitcode + $? ))
        addarray out "$k" "$k"."$parameter_to"

    done
    exit $exitcode
fi
