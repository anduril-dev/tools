#!/bin/bash

. $ANDURIL_HOME/lang/bash/generic.sh || {
    echo "ANDURIL_HOME may not be set!"
    exit 1
    }
myDir=$( dirname $0 )
cd $myDir

iscmd hg || {
    echo Mercurial not installed'!'
    exit 1
}

NAME=NiceCSV
URL="https://bitbucket.org/MoonQ/ncsv/"
TGTDIR=../../lib/nicecsv

[[ -d "$TGTDIR" ]] && {
    echo $NAME already installed. Updating.
    cd "$TGTDIR"
    [[ -h ncsv ]] && rm ncsv
    hg pull
    hg up -C
    exit $?
}

hg clone "$URL" "$TGTDIR"

