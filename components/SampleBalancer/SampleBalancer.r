library(componentSkeleton)

execute <- function(cf) {
    class.col <- get.parameter(cf, 'classCol')
    ratio <- as.numeric(get.parameter(cf, 'ratio'))
    input <- CSV.read(get.input(cf, 'in'))
    
    training<-c()
    testing<-c()

    y <- factor(input[,class.col])
    sizes <- table(y)
    train_size<- floor(min(sizes)*ratio)
    if (train_size<3) {
      write.error(cf, "too few samples to balance")
      return(1)
    }
    train_counter<-matrix(0,nc=length(levels(y)))
    colnames(train_counter)<-levels(y)
    test_counter<-matrix(0,nc=length(levels(y)))
    colnames(test_counter)<-levels(y)
    set.seed(as.integer(get.parameter(cf,'seed')))
    row_index<-sample(1:nrow(input))
    for (i in row_index) {
      if (train_counter[,input[i,class.col]] < train_size ) {
         training<-append(training,i)
         train_counter[,input[i,class.col]] <- train_counter[,input[i,class.col]]+1
      } else {
         testing<-append(testing,i)
         test_counter[,input[i,class.col]] <- test_counter[,input[i,class.col]]+1
      }
    }
    write.log(cf, 
           paste("training set contains",train_size,"samples of each class")
      )
    write.log(cf,paste("testing set contains samples:"))
    for (i in colnames(test_counter)) {
      write.log(cf, paste("*",i, ":",test_counter[,i]))
    }

    CSV.write(get.output(cf,'remainder'),input[testing,,drop=F])
    CSV.write(get.output(cf,'balanced'),input[training,,drop=F])

    return(0)
}

main(execute)
