#/usr/bin/env python
import sys
import anduril
from anduril.args import *
import shutil,os,re
import math
from random import Random

def copyfileorfolder(basename,source,target,link):
    ''' Copies a file or folder structure under target folder '''
    if link:
        os.symlink(os.path.join(source,basename),os.path.join(target,basename))
        return
    if os.path.isfile(os.path.join(source,basename)):
        shutil.copyfile(os.path.join(source,basename),os.path.join(target,basename))
        return
    if os.path.isdir(os.path.join(source,basename)):
        shutil.copytree(os.path.join(source,basename),os.path.join(target,basename))
        return
    raise RuntimeError(source+' was neither file nor folder.')

def portorder(inFiles,inFolder,outFolders,N,link):
    ''' Copy files in port order (sparse) '''
    outidx=0
    for row in inFiles:
        copyfileorfolder(row,inFolder,outFolders[outidx],link)
        outidx+=1
        if outidx+1>N:
            outidx=0

def fileorder(inFiles,inFolder,outFolders,N,link):
    ''' Copy files in input file order (sequence)  '''

    bins=[int(math.floor(float(len(inFiles))/float(N)))]*int(N)
    binidx=0
    while sum(bins)<len(inFiles):
        bins[binidx]+=1
        binidx+=1
    offsets=list(offset(bins))
    offsets.insert(0,0)

    for outidx in range(N):
        for f in range(offsets[outidx], offsets[outidx]+bins[outidx]):
            copyfileorfolder(inFiles[f],inFolder,outFolders[outidx],link)

def regexorder(inFiles,inFolder,outFolders,matcher,uniqlabel,link):
    ''' Copy files by regex match '''
    
    for f in inFiles:
        m=matcher.search(f)
        if m:
            outidx=uniqlabel.index(m.group(1))
            copyfileorfolder(f,inFolder,outFolders[outidx],link)

def offset(it):
    total = 0
    for x in it:
        total += x
        yield total

''' Splits a folder input in N outputs '''
inFolder = input_in
outFolders=[]
cf = anduril.CommandFile.from_file(sys.argv[1])
method = order.lower().strip()
if method not in ('sparse','sequence','regexp','random'):
    write_error("Writing order: \""+method+"\" not recognized.")
    sys.exit(1)
# list files, and remove hidden  (.files)
inFiles=sorted(filter(lambda x: not x.startswith('.'), os.listdir(inFolder)))

if method=='regexp':
    matcher=re.compile(param_regexp)
    matches=[]
    skipped=0
    for f in inFiles:
        m=matcher.search(f)
        if m:
            matches.append(m.group(1))
        else:
            skipped+=1
    uniqlabel=sorted(set(matches))
    print("Unique matches",uniqlabel)
    print("Skipped %d files."% skipped)
    for x in uniqlabel:
        output_out.write(x,x)
        outFolders.append(os.path.join(output_out.get_folder(),x))
    for x in outFolders:
        os.mkdir(x)
    regexorder(inFiles,inFolder,outFolders,matcher,uniqlabel,link)
    sys.exit(0)
else:
    for x in range(param_N):
        output_out.write(str(x+1),'folder'+str(x+1))
        outFolders.append(os.path.join(output_out.get_folder(),'folder'+str(x+1)))
        os.mkdir(outFolders[x])

if method=='sparse':
    portorder(inFiles,inFolder,outFolders,N,link)
if method=='sequence':
    fileorder(inFiles,inFolder,outFolders,N,link)
if method=='random':
    Random(param_seed).shuffle(inFiles)
    portorder(inFiles,inFolder,outFolders,N,link)
