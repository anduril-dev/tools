#!/usr/bin/python
import sys, os
from anduril.args import *
import anduril
import subprocess

BBC_PATH = os.path.join(os.path.dirname(__file__), "../../lib/bayesianbiclustering/BBC")

if not os.path.exists(BBC_PATH):
  raise Exception("This library is not correctly installed. Please run the installation script.")

if normalization_method not in ["None", "CSN", "RSN", "IQRN", "SQRN"]:
  raise Exception("normalization_method can only be one of None, CSN, RSN, IQRN or SQRN")

if normalization_alpha_value < 0 or normalization_alpha_value > 100:
  raise Exception("normalization_alpha_value should be between 0-100")

subprocess.check_output([
  BBC_PATH,
  "-i", input_matrix,
  "-k", str(number_of_clusters),
  "-o", bayesian_biclustering_output,
  "-n", normalization_method,
  "-r", str(normalization_alpha_value)])

