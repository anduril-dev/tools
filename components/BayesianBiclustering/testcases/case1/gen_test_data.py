import pandas as pd
import numpy as np

def add_cluster(arr, a, b, c, d, loc, scale):
  for i in range(a, b):
    for j in range(c, d):
      arr[i][j] = np.random.normal(loc=loc, scale=scale)

n_genes = 100
n_conditions = 100

tdata = np.zeros((n_genes, n_conditions))
add_cluster(tdata, 10, 20, 10, 20, 10, 1)
add_cluster(tdata, 50, 85, 10, 20, 30, 1)
add_cluster(tdata, 80, 85, 40, 50, 50, 1)

column_names = ["Exp %i" % i for i in range(n_conditions)]
row_names = ["Gene %i" % i for i in range(n_genes)]

tdf = pd.DataFrame(tdata, columns=column_names, index=row_names)
tdf.to_csv(path_or_buf="input/expression_matrix", sep="\t")

