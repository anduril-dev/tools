#!/bin/bash

NAME=BayesianBiclustering
URL=http://www.people.fas.harvard.edu/~junliu/BBC/BBC_software.zip
TGTDIR=../../lib/bayesianbiclustering

[[ -d "$TGTDIR" ]] && {
  echo $NAME already installed. Reinstalling.
  rm -vfr "$TGTDIR"
}

mkdir "$TGTDIR"
cd "$TGTDIR"

curl $URL --output bbc.zip
unzip bbc.zip

# Patch bug in the code if it still exists
sed -i '69s/gsl_ran_shuffle(r,map,n,sizeof(int));/gsl_ran_shuffle(r,map,n,sizeof(long));/' kmeans.c
make

