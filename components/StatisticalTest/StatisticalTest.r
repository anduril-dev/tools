library(componentSkeleton)

# Test         Y      alternative paired samelength distrib confint
# -----------------------------------------------------------------
# chi-squared  no     no          no     -          count   no
# cor-pearson  mand   yes         no     yes        numeric yes
# cor-spearman mand   yes         no     yes        numeric no
# F-test       mand   yes         no     no         normal  yes
# fisher       no     yes (2x2)   no     -          count   yes
# ks           mand   yes         no     no         numeric no
# shapiro      no     no          no     -          numeric no
# t-test       opt    yes         yes    paired     normal  yes
# wilcoxon     opt    yes         yes    paired     ordinal yes
#
# Y: mand = Y is mandatory, opt = Y is optional
# samelength: paired = if paired=true, then yes
# confint: supports computing condidence intervals (not used currently)

execute <- function(cf) {
    columns <- get.columns(cf)
    if (is.null(columns)) return(INVALID_INPUT)
    
    alternative <- get.alternative(cf)
    paired <- get.parameter(cf, 'paired', 'boolean')
    pvalue.column <- get.parameter(cf, 'pvalueColumn')
    by.row <- get.parameter(cf, 'byRow', 'boolean')
    expected.mean <- get.parameter(cf, 'mean', 'float')
    cont.rows <- get.parameter(cf, 'contingencyRows', 'int')
    
    reference.defined <- length(columns$reference) > 0
    
    # Based on test type, set variables test.function, test.args, use.y,
    # (same.length, use.contingency, p.attribute)
    same.length <- FALSE
    use.contingency <- FALSE
    p.attribute <- 'p.value'
    
    test.type <- get.parameter(cf, 'test')
    if (test.type == 'chi-squared') {
        test.function <- chisq.test
        use.y <- FALSE
        use.contingency <- TRUE
        test.args <- list()
    } else if (test.type == 'cor-pearson') {
        test.function <- cor.test
        use.y <- TRUE
        same.length <- TRUE
        test.args <- list(alternative=alternative, use='pairwise.complete.obs', method='pearson')
    } else if (test.type == 'cor-spearman') {
        test.function <- cor.test
        use.y <- TRUE
        same.length <- TRUE
        test.args <- list(alternative=alternative, use='pairwise.complete.obs', method='spearman')
    } else if (test.type == 'F-test') {
        test.function <- var.test
        use.y <- TRUE
        test.args <- list(alternative=alternative)
    } else if (test.type == 'fisher') {
        test.function <- fisher.test
        use.y <- FALSE
        use.contingency <- TRUE
        test.args <- list(alternative=alternative, conf.int=FALSE) # Disable conf.int for now
    } else if (test.type == 'ks') {
        test.function <- ks.test
        use.y <- TRUE
        test.args <- list(alternative=alternative)
    } else if (test.type == 'shapiro') {
        test.function <- shapiro.test
        use.y <- FALSE
        test.args <- list()
    } else if (test.type == 't-test') {
        test.function <- t.test
        use.y <- reference.defined
        same.length <- paired
        test.args <- list(alternative=alternative, paired=paired, mu=expected.mean)
        if (!use.y) cat(sprintf('Comparing target values agains expected mean %s\n', expected.mean))
    } else if (test.type == 'wilcoxon') {
        test.function <- wilcox.test
        use.y <- reference.defined
        same.length <- paired
        test.args <- list(alternative=alternative, paired=paired, mu=expected.mean)
        if (!use.y) cat(sprintf('Comparing target values agains expected mean %s\n', expected.mean))
    } else {
        write.error(cf, sprintf('Invalid "test" parameter: %s', test.type))
        return(PARAMETER_ERROR)
    }
    
    if (!use.contingency) cont.rows <- NA

    if (use.y && length(columns$reference) == 0) {
        write.error(cf, 'Reference group is needed for this test but is not defined')
        return(PARAMETER_ERROR)
    }
    
    if (!use.y && length(columns$reference) > 0) {
        write.error(cf, 'This test does not use a reference group but reference is defined; set referenceColumns to empty')
        return(PARAMETER_ERROR)
    }
    
    if (use.y && use.contingency) {
        write.error(cf, 'Internal error: when contingency is used, reference (Y) is not used')
        return(1)
    }
    
    if (same.length && length(columns$target) != length(columns$reference)) {
        write.error(cf,
            sprintf('Target and reference groups have different sizes (%d and %d); they must be equal for this test',
            length(columns$target), length(columns$reference)))
        return(PARAMETER_ERROR)
    }
    
    if (by.row) result <- test.by.rows(cf, columns, test.function, use.y, cont.rows, test.args, p.attribute)
    else     result <- test.by.columns(cf, columns, test.function, use.y, cont.rows, test.args, p.attribute)
    if (is.null(result)) return(1)
    colnames(result)[2] <- pvalue.column
    
    correction <- get.parameter(cf, 'correction')
    if (correction != 'none') {
        corrected.column <- get.parameter(cf, 'correctedColumn')
        corrected <- correct.p.values(result[,2], correction, alternative=='twosided')
        result[,corrected.column] <- corrected
        p.col <- 3 # Column for threshold comparison
    } else {
        p.col <- 2
    }
    
    write.idlist(cf, result, 1, p.col)
    
    prefix.column <- get.parameter(cf, 'prefixColumn')
    if (nchar(prefix.column) > 0) {
        output.group <- get.parameter(cf, 'outputSet')
        result <- data.frame(output.group, result, stringsAsFactors=FALSE)
        colnames(result)[1] <- prefix.column
    }
    
    CSV.write(get.output(cf, 'pvalues'), result)
    
    return(0)
}

get.columns <- function(cf) {
    target.group <- get.parameter(cf, 'targetColumns')
    ref.group <- get.parameter(cf, 'referenceColumns')
    
    if (input.defined(cf, 'groups')) {
        groups <- SampleGroupTable.read(get.input(cf, 'groups'))
        target.columns <- SampleGroupTable.get.source.groups(groups, target.group)
        ref.columns <- SampleGroupTable.get.source.groups(groups, ref.group)
        
        if (nchar(target.group) > 0 && is.null(target.columns)) {
            write.error(cf, sprintf('Target group %s not found in "groups"', target.group))
            return(NULL)
        }
        if (nchar(ref.group) > 0 && is.null(ref.columns)) {
            write.error(cf, sprintf('Reference group %s not found in "groups"', ref.group))
            return(NULL)
        }
        columns <- list(target=target.columns, reference=ref.columns)
    } else {
        columns <- list(target=split.trim(target.group, ','),
            reference=split.trim(ref.group, ','))
    }
    
    header <- scan(get.input(cf, 'matrix'), what=character(0), nlines=1, sep='\t', strip.white=TRUE, quiet=TRUE)
    header <- header[2:length(header)]
    if (identical(columns$target, '*')) columns$target <- header
    if (input.defined(cf, 'matrix2')) {
        header2 <- scan(get.input(cf, 'matrix2'), what=character(0), nlines=1, sep='\t', strip.white=TRUE, quiet=TRUE)
        header2 <- header2[2:length(header2)]
        header <- c(header, header2)
        if (identical(columns$reference, '*')) columns$reference <- header2
    }
    if (identical(columns$reference, '*')) columns$reference <- header
    
    target.indices <- match(columns$target, header)
    reference.indices <- match(columns$reference, header)
    if (any(is.na(target.indices))) {
        write.error(cf, 'Target column(s) not found in input matrices')
        return(NULL)
    }
    if (any(is.na(reference.indices))) {
        write.error(cf, 'Reference column(s) not found in input matrices')
        return(NULL)
    }
    
    return(columns)
}

get.alternative <- function(cf) {
    sided <- get.parameter(cf, 'sided')
    if (sided == 'twosided') {
        alternative <- 'two.sided'
    }
    else if (sided == 'greater') {
        alternative <- 'greater'
    }
    else if (sided == 'less') {
        alternative <- 'less'
    } else {
        write.error(cf, sprintf('Invalid "sided" parameter: %s', sided))
        return(PARAMETER_ERROR)
    }
    return(alternative)
}

test.by.rows <- function(cf, columns, test.function, use.y, cont.rows, test.args, p.attribute='p.value') {
    num.lines <- read.num.lines(cf) - 1

    if (input.defined(cf, 'matrix2')) {
        write.error(cf, 'byRow=true currently does not support the matrix2 input')
        return(NULL)
    }
    
    if (!is.na(cont.rows) && (length(columns$target) %% cont.rows) != 0) {
        write.error(cf,
            sprintf('Number of target columns (%d) is not a multiple of contingency rows (%d)',
            length(columns$target), cont.rows))
        return(NULL)
    }
    
    cat(sprintf('Performing %d tests, one for each row\n', num.lines))
    
    conn <- file(get.input(cf, 'matrix'))
    open(conn)
    header <- scan(conn, what=character(0), nlines=1, sep='\t', strip.white=TRUE, quiet=TRUE)
    
    target.indices <- match(columns$target, header) - 1
    if (use.y) reference.indices <- match(columns$reference, header) - 1
    
    ids <- character(num.lines)
    p.values <- rep(NA, num.lines)
    
    i <- 1
    while(TRUE) {
        name.row <- scan(conn, what=character(), n=1, quiet=TRUE)
        if (length(name.row) == 0) break
        values <- as.numeric(scan(conn, nlines=1, what=character(), quiet=TRUE))
        if (length(values) != length(header)-1) {
            write.error(cf, sprintf('Invalid number of items on row %d (%s)', i, name.row))
            return(NULL)
        }
        test.args$x <- values[target.indices]
        if (use.y) test.args$y <- values[reference.indices]
        if (!is.na(cont.rows)) test.args$x <- matrix(test.args$x, nrow=cont.rows)
        p <- try(do.call(test.function, test.args)[[p.attribute]], silent=TRUE)
        ids[i] <- name.row
        if (is.numeric(p)) p.values[i] <- p
        i <- i + 1
    }
    close(conn)
    
    result <- data.frame(id=ids, p=p.values, stringsAsFactors=FALSE)
    colnames(result)[1] <- header[1]
    return(result)
}

test.by.columns <- function(cf, columns, test.function, use.y, cont.rows, test.args, p.attribute='p.value') {
    matrix1 <- Matrix.read(get.input(cf, 'matrix'))
    if (input.defined(cf, 'matrix2')) matrix2 <- Matrix.read(get.input(cf, 'matrix2'))
    else matrix2 <- NULL
    
    if (length(columns$target) == 0) columns$target <- colnames(matrix1)
    if (use.y && length(columns$reference) == 0) {
        if (is.null(matrix2)) {
            write.error(cf, 'If reference columns are not defined, matrix2 must be supplied') 
            return(NULL)
        }
        columns$reference <- colnames(matrix2)
    }
    
    if (use.y && length(columns$target) != length(columns$reference)) {
        write.error(cf, 'Target and reference column lists must have equal length')
        return(NULL)
    }
    
    if (!is.na(cont.rows) && (nrow(matrix1) %% cont.rows) != 0) {
        write.error(cf,
            sprintf('Number of matrix rows (%d) is not a multiple of contingency rows (%d)',
            nrow(matrix1), cont.rows))
        return(NULL)
    }

    if (!use.y || all(columns$target == columns$reference)) ids <- columns$target
    else ids <- sprintf('%s_%s', columns$target, columns$reference)
    result <- data.frame(ID=ids, p=NA_real_, stringsAsFactors=FALSE)
    
    cat(sprintf('Performing %d tests for column combinations\n', nrow(result)))
    
    for (i in 1:length(columns$target)) {
        test.args$x <- matrix1[,columns$target[i]]
        if (use.y) {
            if (is.null(matrix2)) test.args$y <- matrix1[,columns$reference[i]]
            else test.args$y <- matrix2[,columns$reference[i]]
        }
        if (!is.na(cont.rows)) test.args$x <- matrix(test.args$x, nrow=cont.rows)
        p <- try(do.call(test.function, test.args)[[p.attribute]], silent=TRUE)
        if (is.numeric(p)) result[i,2] <- p
    }
    
    return(result)
}

read.num.lines <- function(cf) {
    BUFFER <- 100
    num.lines <- 0
    conn <- file(get.input(cf, 'matrix'))
    open(conn)
    while(TRUE) {
        lns <- readLines(con=conn, n=BUFFER)
        num.lines <- num.lines + length(lns)
        if (length(lns) == 0) break 
    }
    close(conn)
    return(num.lines)
}

correct.p.values <- function(p.values, method, two.sided) {
    present <- !is.na(p.values)
    p.values <- p.values[present]
    
    if (method == 'bonferroni') {
        corrected <- p.adjust(p.values, 'bonferroni')
    } else if (method == 'fdr') {
        corrected <- p.adjust(p.values, 'fdr')
    } else if (method == 'holm') {
        corrected <- p.adjust(p.values, 'holm')
    } else if (method == 'BY') {
        corrected <- p.adjust(p.values, 'BY')        
    } else if (method == 'robustfdr') {
        source('robust-fdr.R')
        if (two.sided) robust.fdr.sides <- 2
        else robust.fdr.sides <- 1
        corrected <- robust.fdr(p.values, sides=robust.fdr.sides)$q
    }
    else {
        stop(paste('Invalid correction parameter:', method))
    }
    
    result <- rep(NA, length(present))
    result[present] <- corrected
    return(result)
}

write.idlist <- function(cf, result.frame, id.column, p.column) {
    threshold <- get.parameter(cf, 'threshold', 'float')
    output.set <- get.parameter(cf, 'outputSet')
    ids <- result.frame[result.frame[,p.column] < threshold, id.column]
    ids <- ids[!is.na(ids)]
    idlist <- SetList.new(output.set, list(ids))
    CSV.write(get.output(cf, 'idlist'), idlist)
}

main(execute)
