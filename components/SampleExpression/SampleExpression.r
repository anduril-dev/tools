library(componentSkeleton)

execute <- function(cf) {
    threshold.type <- get.parameter(cf, 'thresholdType')
    expr <- LogMatrix.read(get.input(cf, 'expr'))
    first.cell <- CSV.read.first.cell(get.input(cf, 'expr'))
    
    if (input.defined(cf, 'groups')) {
        # expr contains channel values, so convert them to log-ratios
        groups <- SampleGroupTable.read(get.input(cf, 'groups'))
        reference.id <- get.parameter(cf, 'referenceGroup')
        # TODO: parameter validation
        reference.groups <- SampleGroupTable.get.source.groups(groups, reference.id)
        control.median <- sapply(1:nrow(expr),
            function(i) median(expr[i,reference.groups], na.rm=TRUE))
        expr <- expr - control.median
        CSV.write(get.output(cf, 'logratio'), expr, first.cell=first.cell)
    } else {
        writeLines(character(), get.output(cf, 'logratio'))
    }
    
    dev.out <- data.frame(empty=NA)
    threshold <- get.parameter(cf, 'threshold', 'float')
    if (threshold.type == 'fold-change' ||
        threshold.type == 'sd'          ||
        threshold.type == 'mad') {
        if (threshold.type == 'fold-change') {
           threshold <- log2(threshold)
        } else if (threshold.type == 'sd') {
           if (!exists('reference.groups')) reference.groups <- 1:ncol(expr)
           SD <- sapply(1:nrow(expr), function(i) sd(expr[i,reference.groups], na.rm=TRUE))
           threshold <- threshold * SD
           dev.out <- sweep(expr, 1, SD, "/")
        } else {
           if (!exists('reference.groups')) reference.groups <- 1:ncol(expr)
           MAD <- sapply(1:nrow(expr), function(i) mad(expr[i,reference.groups], na.rm=TRUE, constant=1))
           threshold <- threshold * MAD
           dev.out <- sweep(expr, 1, MAD, "/")
        }
        over <- (expr >= threshold)
        under <- (expr <= -threshold)
        expr[over] <- 1
        expr[under] <- -1
        expr[(!over) & (!under)] <- 0
    } else if (threshold.type == 'top-most') {
        stopifnot(threshold <= 0.5)
        s <- sort(expr)
        p <- floor(length(s)*threshold)
        if (p > 0) {
          over  <- s[length(s)-p+1]
          under <- s[p]
          rm(s)
          over                     <- (expr >= over)
          under                    <- (expr <= under)
          expr[over]               <- 1
          expr[under]              <- -1
          expr[(!over) & (!under)] <- 0
        } else {
          expr[,] <- 0
        }
    } else {
        write.error(cf, sprintf('Invalid thresholdType value: %s', threshold.type))
        return(PARAMETER_ERROR)
    }
    
    CSV.write(get.output(cf, 'indicator'), expr, first.cell=first.cell)
    CSV.write(get.output(cf, 'deviation'), dev.out, first.cell=first.cell)
    return(0)
}

main(execute)
