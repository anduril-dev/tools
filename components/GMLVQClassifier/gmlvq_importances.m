function importances=gmlvq_importances(cf,classifier, data)

importances=struct('columnheads', [],'data',[]);
importances.columnheads=data.features;

if isfield(classifier.model,'omega')
 % model is GMLVQ
    importances.data=num2csvcell(diag(classifier.model.omega'*classifier.model.omega)');
    return
end

if isfield(classifier.model,'lambda')
 % model is GRLVQ
    importances.data=num2csvcell(classifier.model.lambda);
    return
end

if isfield(classifier.model,'psis')
 % model is LGMLVQ
 importances.columnheads=[{'Prototype'}  importances.columnheads];
 importances.data=cell(length(classifier.model.psis), length(importances.columnheads));
 protoLabels=repmat(classifier.label_coding,  [1 classifier.setting.PrototypesPerClass] )';
 protoLabels=protoLabels(:);
 for p=1:length(classifier.model.psis)
    l=classifier.model.psis{p};
    %classifier.model.psis(1)(p)
    l=num2csvcell(diag(l'*l)');
    importances.data(p,:)=[protoLabels(p) l];
 end
end
