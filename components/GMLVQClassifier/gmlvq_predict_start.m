function prediction=gmlvq_predict_start(cf,classifier,testData)

method=classifier.classifierMethod;

switch method
    case 'LGMLVQ'
    
        prediction = LGMLVQ_classify(testData.data, classifier.model);

    case 'GMLVQ'
        
        prediction = GMLVQ_classify(testData.data, classifier.model);

    case 'GRLVQ'
        
        prediction = GRLVQ_classify(testData.data, classifier.model);
        
    otherwise
        writeerror(cf,['Method ' method ' was not recognized'])
        return
end
