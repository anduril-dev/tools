function confusion=gmlvq_confusion(cf, data, labels)

classColumn=getparameter(cf,'classColumn','string');
confusion=struct('columnheads', [],'data',[]);

if ~iscellcol(data, classColumn)
    confusion.columnheads={'Empty'};
    confusion.data={'NA'};
    return
end

known=getcellcol(data,classColumn,'string');
predicted=getcellcol(data,['Predicted' getparameter(cf,'classColumn','string')],'guess');

[C,order]=confusionmat(known,predicted,'order',labels);

confusion.columnheads=[{ [ getparameter(cf,'classColumn','string') '/Predicted' ] } order' ];
confusion.data=[order num2csvcell(C)];

