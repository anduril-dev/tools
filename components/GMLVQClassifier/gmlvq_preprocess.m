function [trainData,validateData]=gmlvq_preprocess(cf,type,classifier)

classColumn=getparameter(cf,'classColumn','string');
if strcmp(getparameter(cf,'columnsToRemove','string'),'')
    omitColumns={classColumn};
else
    omitColumns=textscan(getparameter(cf,'columnsToRemove','string'),'%s','Delimiter',',');
    omitColumns=omitColumns{1};
    omitColumns{end+1,1}=classColumn;
end

if strcmp(type,'train')
    % preprocess training data
    trainData = struct('data',[],'labels',[],'zscore_model',[],'label_coding',[],'features',[]);
    trainFile=getinput(cf,'data');
    
    trainCSV=readcsvcell(trainFile);
    
    omitIndex=false(1,length(trainCSV.columnheads));
    for i=omitColumns'
        omitIndex=or(omitIndex, strcmp(trainCSV.columnheads, i) );
    end
    trainData.data=cellfun(@(x) str2num(x), trainCSV.data(:,~omitIndex));
    trainData.features=trainCSV.columnheads(~omitIndex);
    trainLabels=getcellcol(trainCSV,classColumn,'string');
    [trainData.label_coding,~,trainData.labels]=unique(trainLabels);
    
    [trainData.data, trainData.zscore_model] = zscoreTransformation(trainData.data);

    % preprocess validation data just like the one before... 
    validateFile=getinput(cf,'testData');
    if strcmp(validateFile,'')
        % if validation not set, use the training data.
        validateData=trainData;
    else
        validateData = struct('data',[],'labels',[]);
        validateCSV=readcsvcell(validateFile);
        
        omitIndex=false(1,length(validateCSV.columnheads));
        for i=omitColumns'
            omitIndex=or(omitIndex, strcmp(validateCSV.columnheads, i) );
        end
        validateData.data=cellfun(@(x) str2num(x), validateCSV.data(:,~omitIndex));
        validateLabels=getcellcol(validateCSV,classColumn,'string');
        validateData.labels=zeros(length(validateLabels),1);
        for i=1:length(trainData.label_coding)
            validateData.labels(strcmp(validateLabels,trainData.label_coding(i)))=i;
        end
        validateData.data = zscoreTransformation(validateData.data,'parameter',trainData.zscore_model);
    
    end
end
if strcmp(type,'predict')
    % preprocess prediction data, named validation for compatibility
    trainData=[];
    validateFile=getinput(cf,'classifyData');

    validateData = struct('data',[]);
    validateCSV=readcsvcell(validateFile);
    
    omitIndex=false(1,length(validateCSV.columnheads));
    for i=omitColumns'
        omitIndex=or(omitIndex, strcmp(validateCSV.columnheads, i) );
    end
    validateData.data=cellfun(@(x) str2num(x), validateCSV.data(:,~omitIndex));
    validateData.data = zscoreTransformation(validateData.data,'parameter',classifier.zscore_model);
    
end



