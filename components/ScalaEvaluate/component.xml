<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>ScalaEvaluate</name>
    <version>1.0</version>
    <doc>
    Executes a Scala script. The script is executed in an environment
    where relevant imports and variable definitions have been made.
    See <a href="http://csbi.ltdk.helsinki.fi/pub/anduril/scaladoc">Anduril Scala API</a>
    for available utility classes. The Table class in that API provides
    I/O support for BED, CSV, FASTA, FASTQ, gzip and VCF formats.

    The following variables are visible during execution. Here, X{1-3}
    refers to variables X1, X2 and X3.
    <table>
	<tr>
	    <th align="left">Variable</th>
	    <th align="left">Kind</th>
	    <th align="left">Class</th>
	    <th align="left">Description</th>
	</tr>
	<tr>
	    <td>table{1-5}</td>
	    <td>val</td>
	    <td>org.anduril.runtime.table.Table</td>
	    <td>Input tables; null if not provided</td>
	</tr>
	<tr>
	    <td>table1.*</td>
	    <td>def</td>
	    <td>Members</td>
	    <td>Members of table1 are imported into main name space</td>
	</tr>
	<tr>
	    <td>table{1-5}File</td>
	    <td>val</td>
	    <td>java.io.File</td>
	    <td>Input files; null if not provided</td>
	</tr>
	<tr>
	    <td>var{1-3}File</td>
	    <td>val</td>
	    <td>java.io.File</td>
	    <td>Generic inputs; null if not provided</td>
	</tr>
	<tr>
	    <td>param{1-5}</td>
	    <td>val</td>
	    <td>java.lang.String</td>
	    <td>Value of extra parameter</td>
	</tr>
	<tr>
	    <td>tableOut</td>
	    <td>var</td>
	    <td>org.anduril.runtime.table.Table</td>
	    <td>If assigned, this table is written to tableOutFile; initially null</td>
	</tr>
	<tr>
	    <td>tableOutFile</td>
	    <td>val</td>
	    <td>java.io.File</td>
	    <td>Output table file</td>
	</tr>
	<tr>
	    <td>optOut{1-3}File</td>
	    <td>val</td>
	    <td>java.io.File</td>
	    <td>Generic outputs</td>
	</tr>
    </table>

    Members of table1 (if defined) can be accessed without "table1." qualifier.
    If any table object is assigned to tableOut, it is written to tableOutFile.
    All outputs are empty if not explictly written to.

    Automatically imported classes include:
    <ul>
        <li>java.io.File</li>
        <li>scala.collection.immutable.SortedMap</li>
        <li>scala.collection.immutable.TreeMap</li>
        <li>scala.collection.mutable.ArrayBuffer</li>
        <li>org.anduril.runtime.table._</li>
        <li>org.anduril.runtime.table.processor.ColumnStatistics</li>
    </ul>
    </doc>
    <author email="kristian.ovaska@helsinki.fi">Kristian Ovaska</author>
    <category>External</category>
      <launcher type="bash">
        <argument name="file" value="ScalaEvaluate.sh" />
    </launcher>
    <!--requires type="jar">anduril-scala.jar</requires-->
    <requires type="jar">commons-primitives-1.0.jar</requires>
    <requires type="jar">commons-math3-3.2.jar</requires>
    <type-parameters>
        <type-parameter name="IN1"  />
        <type-parameter name="IN2"  />
        <type-parameter name="IN3"  />
        <type-parameter name="OUT1" />
        <type-parameter name="OUT2" />
        <type-parameter name="OUT3" />
    </type-parameters>
    <inputs>
	<input name="scriptFile" type="ScalaSource" optional="true">
	    <doc>Scala source file. Either this or the parameter must be given.</doc>
	</input>
        <input name="table1" type="CSV" optional="true">
            <doc>Data table 1, visible as table1 (Table).</doc>
        </input>
        <input name="table2" type="CSV" optional="true">
            <doc>Data table 2, visible as table2 (Table).</doc>
        </input>
        <input name="table3" type="CSV" optional="true">
            <doc>Data table 3, visible as table3 (Table).</doc>
        </input>
        <input name="table4" type="CSV" optional="true">
            <doc>Data table 4, visible as table4 (Table).</doc>
        </input>
        <input name="table5" type="CSV" optional="true">
            <doc>Data table 5, visible as table5 (Table).</doc>
        </input>
        <input name="var1" type="IN1" optional="true" array="generic">
            <doc>Generic input that may be used for folders, arrays and binaries.
                Visible as var1 (File).</doc>
        </input>
        <input name="var2" type="IN2" optional="true" array="generic">
            <doc>Generic input that may be used for folders, arrays and binaries.
                Visible as var2 (File).</doc>
        </input>
        <input name="var3" type="IN3" optional="true" array="generic">
            <doc>Generic input that may be used for folders, arrays and binaries.
                Visible as var3 (File).</doc>
        </input>
    </inputs>
    <outputs>
        <output name="table" type="CSV">
            <doc>Result table.</doc>
        </output>
        <output name="optOut1" type="OUT1" array="generic">
            <doc>An optional output that can be used for custom data types.</doc>
        </output>
        <output name="optOut2" type="OUT2" array="generic">
            <doc>An optional output that can be used for custom data types.</doc>
        </output>
        <output name="optOut3" type="OUT3" array="generic">
            <doc>An optional output that can be used for custom data types.</doc>
        </output>
	<output name="scriptOut" type="ScalaSource">
	    <doc>Final Scala script that was executed. Used for debugging.</doc>
	</output>
    </outputs>
    <parameters>
        <parameter name="scriptStr" type="string" default="">
            <doc>Scala source string. Either this or the input must be given.
	    If both are given, this parameter is appended to the input.</doc>
        </parameter>
	<parameter name="defaultOutput" type="string" default="">
	    <doc>Name of an input port that is written to the output by default.
	    Example: table1. If empty, no table is written to output by default.
	    </doc>
        </parameter>
        <parameter name="scalaFlags" type="string" default="">
            <doc>Extra command line flags for the Scala compiler. If multiple
                flags are given, they are separated by space. Flags to the underlying
                JVM need to be prefixed with -J, e.g., -J-Xprof.</doc>
        </parameter>
	<parameter name="param1" type="string" default="">
	    <doc>Extra parameter, visible as param1 (String).</doc>
        </parameter>
	<parameter name="param2" type="string" default="">
	    <doc>Extra parameter, visible as param2 (String).</doc>
        </parameter>
	<parameter name="param3" type="string" default="">
	    <doc>Extra parameter, visible as param3 (String).</doc>
        </parameter>
	<parameter name="param4" type="string" default="">
	    <doc>Extra parameter, visible as param4 (String).</doc>
        </parameter>
	<parameter name="param5" type="string" default="">
	    <doc>Extra parameter, visible as param5 (String).</doc>
        </parameter>
        <parameter name="format" type="string" default="csv">
            <doc>Override input table file formats.
                The default format for all tables is csv.
                To override the formats of all tables, set the parameter
                to one of bed, csv, fasta, fastq or vcf. To override
                formats for individual tables, use the syntax
                TABLE=FORMAT,TABLE=FORMAT, where TABLE is one of
                table1 to table5 and FORMAT is as above.
                </doc>
        </parameter>
    </parameters>
</component>
