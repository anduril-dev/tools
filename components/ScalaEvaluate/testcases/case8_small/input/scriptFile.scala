// Test tables with 1 column and 2,1,0 rows
val c1 = table1.collectS("A1")
val c2 = table2.collectS("B1")
val c3 = table3.collectS("C1")

table1.readAll()
table2.readAll()
table3.readAll()

assert(c1().deep == Array("x1", "x2").deep)
assert(c2().deep == Array("x3").deep)
assert(c3().deep == Array().deep)
