filter(r => r("A1") != "x2") // skip second row
val c1 = collectS("A1")
val c2 = collectI("A2")
val c3 = collectD("A3")
val c4 = collectS("A4")
readAll()

assert(c1().deep == Array("x1", null, "x4", "x5").deep)
assert(c2().deep == Array(1, 10, -110).deep)
assert(c3()(0) == 3.5)
assert(java.lang.Double.isNaN(c3()(1)))
assert(java.lang.Double.isNaN(c3()(2)))
assert(c3()(3) == 0.7)
assert(c4().deep == Array("y1", "y3", "y4", null).deep)
