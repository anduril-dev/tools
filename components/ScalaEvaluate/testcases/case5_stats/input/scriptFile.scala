val s2 = statistics("A2")
val s3 = statistics("A3", 4)
val sgroup = statisticsGroupBy("A2", row => row.int("G"))
readAll()

assert(s2.max     == 10)
assert(s2.mean    == -33)
assert(s2.median  == 1)
assert(s2.min     == -110)
assert(s2.missing == 2)
assert(s2.n       == 3)
assert(s2.sum     == -99)
assert(Math.abs(s2.sd - 66.835619246027790) < 1e-6)
assert(Math.abs(s2.variance - 4467) < 1e-6)

val eps = 1e-6

assert(s3.max     == 3.5)
assert(Math.abs(s3.mean - 0.7) < eps)
assert(Math.abs(s3.median - 0.7) < eps)
assert(s3.min     == -2.1)
assert(s3.missing == 2)
assert(s3.n       == 3)
assert(Math.abs(s3.sum - 2.1) < eps)
assert(Math.abs(s3.sd - 2.8) < eps)
assert(Math.abs(s3.variance - 7.84) < eps)

val group1 = sgroup.getGroup(1).get
assert(group1.max == 10)
assert(group1.min == 1)
assert(group1.missing == 1)
assert(group1.n == 2)

val group2 = sgroup.getGroup(2).get
assert(group2.max == -110)
assert(group2.min == -110)
assert(group2.missing == 1)
assert(group2.n == 1)
