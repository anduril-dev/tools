tableOut = table1
includeColumnsMatch("A[123]")
// start: A2 =   1   NA 10 NA -110
//        A3 = 3.5 -2.1 NA NA  0.7
modifyColumnS("A1", (v,r) => v + "X")
modifyColumnI("A2", (v,r) => v*2)                  // A2 =    2    NA 20000000 NA -220
modifyColumnI(getColumn("A2"), (v,r) => v+10)      // A2 =   12    NA 20000010 NA -210
modifyColumnD("A3", (v,r) => v*3)                  // A3 = 10.5  -6.3 NA NA  2.1
addColumnD("N1", (r: Row) => r.double("A3")*2)     // N1 =   21 -12.6 NA NA  4.2
addColumnI("N2", (r: Row) => r.rowNumber)          // N2 =    0     1  2  3    4
modifyColumnD("A3", (v,r) => v*r.double("A2"))     // A3 =  126    NA NA NA -441
transform(r => r("A3") = r.double("A3") * 2)       // A3 =  252    NA NA NA -882
modifyColumnS(getColumn("A2"), (v,r) => "|"+v+"|") // A2 =  |12|   NA |20000010| NA |-210| (string)
