library(componentSkeleton)

orderMap <- function(clusters,values, method) {
    
    centers <- sort(eval(parse(
               text=paste('tapply(values,clusters,',
                          method,
                          ',na.rm=TRUE)',sep=''))))
    nClusters <- nrow(centers)
    map <- cbind(rownames(centers),1:nClusters, centers)
    return(map)
}

applyMap <- function(clusters, map) {
    newClusters<-clusters
    for (i in 1:nrow(map)) {
        newClusters[ clusters==map[i,1] ] <- map[i,2]
    }
    return(newClusters)

}

execute <- function(cf) {
    inCSV <- CSV.read(get.input(cf, 'in'))
    
    clusterCol <- get.parameter(cf, 'clusterCol')
    sortCol <- get.parameter(cf, 'sortCol')
    sortMethod <- get.parameter(cf, 'summaryFunction')
    
    #print(inCSV[,clusterCol])
    map <- orderMap(inCSV[,clusterCol],inCSV[,sortCol], sortMethod)
    print(map)
    sorted <- applyMap(inCSV[,clusterCol], map)
    inCSV[,clusterCol]<-sorted
    
    CSV.write(get.output(cf, 'out'), inCSV)
    return(0)
}
main(execute)
