#ifndef _RND_H
#define _RND_H

#include "string.h"

#define STREAM_CYCLIC 1
#define STREAM_PERMUTATION 2
#define STREAM_SELECT_RANDOM 3

void* initialize();
void finalize(void *rng);

double random_double();
int random_int();
int random_int_range(int low, int high);
void shuffle_array_int(int *array, int size);

int new_stream_int(int type, const int *array, size_t n);
int new_stream_int_range(int type, int start, int end, int step);
int next_stream_element(int stream_id);

#endif
