library(componentSkeleton)
library(isa2)

execute <- function(cf) {
  
  thrUp  <- get.parameter(cf, "thrUp", "float")
  thrDown  <- get.parameter(cf, "thrLow", "float")
  thrStep <- get.parameter(cf, "thrStep", "float")
  
  data <- as.matrix(read.delim(get.input(cf, 'in'),
                               header=TRUE, sep= "\t",
                               dec=".", row.names = 1))
  # Set the threshold values for clusters
  th <- seq(thrDown, thrUp, by=thrStep)
  # Do ISA on the data
  ISABiclusters <- isa(data, thr.row=th, thr.col=th)
  # Row clusters
  rows <- ISABiclusters$rows
  # Column clusters
  cols <- ISABiclusters$columns

  labels <- matrix(0., nrow(data), ncol(data))
  ths <- matrix(0., nrow(data), ncol(data))
  for (k in seq_len(ncol(rows))) {
    pr <- outer(rows[, k], cols[,k])
    mask <- pr > ths
    labels[mask] <- k
    ths[mask] <- pr[mask]
  }
  # Change col and row labels
  rowsdf <- as.data.frame(rows)
  colsdf <- as.data.frame(cols)
  colnames(rowsdf) <- gsub("V", "cluster", colnames(rowsdf))
  rownames(rowsdf) <- rownames(data)  
  colnames(colsdf) <- gsub("V", "cluster", colnames(colsdf))
  rownames(colsdf) <- rownames(data)
  
  # Change col and row labels for cluster matrix
  labelsdf <- as.data.frame(labels)
  colnames(labelsdf) <- colnames(data)
  rownames(labelsdf) <- rownames(data)
  
  CSV.write(get.output(cf, 'labels'), labelsdf)
  CSV.write(get.output(cf, 'rowClusters'), rowsdf)
  CSV.write(get.output(cf, 'colClusters'), colsdf)
  
  return(0)
}

main(execute)
