library(componentSkeleton)

cbind.fill<-function(...){
    # Function to bind different lenghts of columns to one matrix
    nm <- list(...) 
    nm<-lapply(nm, as.matrix)
    n <- max(sapply(nm, nrow)) 
    do.call(cbind, lapply(nm, function (x) 
    rbind(x, matrix(, n-nrow(x), ncol(x))))) 
}
remove.outliers<-function(values,limits){
    # Remove values outside the edges (limits)
    if (length(limits)==1) { return(values) }
    
    values <- values[which(values>=min(limits))]
    values <- values[which(values<=max(limits))]
    return(values)
}

execute <- function(cf) {
    table_in = CSV.read(get.input(cf, 'in'))
    valuesNames = split.trim(get.parameter(cf, 'values'),',')
    if (identical(valuesNames, '*')) valuesNames <- colnames(table_in)
    bins = get.parameter(cf, 'bins')
    digits = get.parameter(cf, 'digits','int')
    isFreq = get.parameter(cf, 'frequencyIsRatio','boolean')

    commonScale = get.parameter(cf, 'commonScale','boolean')
    # data for the histogram
    coldata <- table_in[,valuesNames,drop=FALSE]

    skip <- integer()
    for (i in 1:length(valuesNames)) {
        if (!is.numeric(coldata[,valuesNames[i]])) {
            write.log(cf, sprintf('Skipping column %s in y: not numeric', valuesNames[i]))
            skip <- c(skip, i)
        }
    }
    if (length(skip) > 0) valuesNames <- valuesNames[-skip]
    coldata <- coldata[,valuesNames,drop=FALSE]
    values<-as.vector(data.matrix(coldata))
    values[!is.na(values)]
    # number of bins 
    breaks <- eval(parse(text=bins))
    # create breaks for common scale
    if (commonScale) {
        values <- remove.outliers(values, breaks)
        y<-hist(values,breaks=breaks,plot=FALSE)
        breaks<-y$breaks
    }
    # Empty result frames
    table.out<-data.frame()
    optOut1<-data.frame()
    optOut2<-data.frame()
    optOut3<-data.frame()
    cols<-c()
    midcols<-c()

    # for each column, calculate histogram and midpoints
    for (i in 1:ncol(coldata)) {
        values<-coldata[,i]
        values<-values[!is.na(values)]
        if (!commonScale) {
            breaks <- eval(parse(text=bins))
        }
        values <- remove.outliers(values, breaks)
        y<-hist(values,breaks=breaks,plot=FALSE)
        table.out<-cbind.fill(table.out,(y$counts))
        optOut1<-cbind.fill(optOut1,(format(y$mids,digits=digits)))
        optOut2<-cbind.fill(optOut2,y$mids)
        optOut3<-cbind.fill(optOut3,y$breaks)
        if (isFreq) { # Change absolute frequencies to ratio if needed
            table.out[,i]<-table.out[,i]/sum(table.out[,i],na.rm=TRUE)
        }
        cols<-c(cols,valuesNames[i])
        midcols<-c(midcols,paste('MidPoint',valuesNames[i]))
    }

    colnames(table.out)<-cols
    colnames(optOut1)<- midcols
    colnames(optOut2)<- midcols
    colnames(optOut3)<- cols
    
    file=get.output(cf,'out')
    CSV.write(file,table.out)

    file=get.output(cf,'midPointsFormatted')
    CSV.write(file,optOut1)
    file=get.output(cf,'midPoints')
    CSV.write(file,optOut2)
    file=get.output(cf,'breakPoints')
    CSV.write(file,optOut3)
    return(0)
}

main(execute)
