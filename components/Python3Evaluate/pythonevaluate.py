import anduril
from anduril.args import *
import sys
import os


def createFile(f, content=""):
    fid = open(f, "w")
    fid.write(content)
    fid.close()


if table1:
    table1 = anduril.TableReader(table1)
if table2:
    table2 = anduril.TableReader(table2)
if table3:
    table3 = anduril.TableReader(table3)
if table4:
    table4 = anduril.TableReader(table4)
if table5:
    table5 = anduril.TableReader(table5)

tableout = anduril.TableWriter(table, [])

out1File = out1
del out1
out2File = out2
del out2
out3File = out3
del out3

createFile(out1File)
createFile(out2File)
createFile(out3File)

if vararray:
    vars().update(vararray)

# A file is given to exec instead of str to get the
# tracebacks to work
if input_scriptIn:
    exec(open(input_scriptIn, "rt").read())
elif param_script:
    exec(param_script)
else:
    print(
        "Both input script and parameter script unspecified. Must specify one.",
        file=sys.stderr,
    )
    sys.exit(1)

try:
    out1
    createFile(out1File, out1)
except:
    pass
try:
    out2
    createFile(out2File, out2)
except:
    pass
try:
    out3
    createFile(out3File, out3)
except:
    pass
