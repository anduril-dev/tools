library(componentSkeleton)

execute <- function(cf){

    columns         = unlist(strsplit(get.parameter(cf, 'columns'), ","))

    type			= get.parameter(cf, 'fillType')
    outdata.path    = get.output(cf, 'out')
    data        	= CSV.read(get.input(cf, 'in'))
    
    filled <- c()
    
    if(columns[1] == "*")
    	columns <- colnames(data)
    	
    if(FALSE %in% (columns %in% colnames(data))){
    	return(PARAMETER_ERROR)
	}
	    	
	for(col in colnames(data)){
		
		if(col %in% columns && is.numeric(data[,col])){
			
			#remove na from tails and head
			naHead <- 0
			naTail <- 0
			addHead <- TRUE
			
			for(cell in data[,col]){
				if(addHead == TRUE && is.na(cell)){
					naHead <- naHead + 1
				}else if(addHead == TRUE && !is.na(cell)){
					addHead <- FALSE
				}
			}
			addTail <- TRUE
			for(i in c(length(data[,col]):1)){
								
				if(addTail == TRUE && is.na(data[i, col])){
					naTail <- naTail + 1
				}else if(addTail == TRUE && !is.na(data[i, col])){
					addTail <- FALSE
				}
			}
			
			interVector <- data[(naHead+1):(length(data[,col])-naTail), col]
			
			
			if(type == "linear"){
				ap <- approx(interVector, n=length(interVector))
			}else{
				ap <- approx(interVector, n=length(interVector), method="constant")
			}
			#add values for the removed head and tail
			newCol <- c(rep(interVector[1], naHead), ap$y, rep(interVector[length(interVector)], naTail))
			
			if(length(filled) == 0)
				filled <- newCol
			else
				filled <- cbind(filled, newCol)
			
		}else{
			if(length(filled) == 0)
				filled <- data[,col]
			else
				filled <- cbind(filled, data[,col])
		
		}
	}
	colnames(filled) <- colnames(data)
    # Write output.
    CSV.write(outdata.path, filled)
    
    return(0)
}
main(execute)
