import org.anduril.runtime._
import java.io._
import scala.sys.process._

object ScalaScript extends ComponentSkeleton {

	val nIn = 3
	val nOut = nIn
	val nParam = 4

    implicit class FileOps(val file: File) extends AnyVal {
        def write(text: String) = {
            val fw = new FileWriter(file)
            fw.write(text)
            fw.close
        }
    }

    def makeIO(f: String => File, name: String) = {
        for (i <- 1 to nIn; file = f (name + i)) yield {
            val path = 
                if (file == null) "\"\"" 
                else "\"" + file.getAbsolutePath + "\""
            s"val $name$i = $path"
        }
    }

    def run(): ErrorCode = {
        if (paramS("scriptStr").isEmpty && !inputDefined("scriptFile")) {
            writeError("No script provided")
            return ErrorCode.PARAMETER_ERROR
        }

        val ioTools = """
                        |import java.io._
                        |
                        |def iterRead(path: String) = io.Source.fromFile(path).getLines
                        |
                        |def mkFolder(path: String) = new File(path).mkdir()
                        |
                        |def write(path: String, text: String) = {
                        |  val fw = new FileWriter(new File(path))
                        |  fw.write(text)
                        |  fw.close
                        |}
                        |                        
                        |""".stripMargin


        val in = makeIO( input _, "in" ).mkString("\n")
        val out = makeIO( output _, "out" ).mkString("\n")
        val param = (1 to nParam).map { i => 
            val v = if (paramS("param"+i) == null) "\"\"" else "\"" + paramS("param"+i) + "\"" 
            s"val param$i = $v"}.mkString("\n")

        val apiDeclaration =  Array(in, out, param).mkString("\n")

        val script = 
            if (paramS("scriptStr").nonEmpty) paramS("scriptStr")
            else io.Source.fromFile(input("scriptFile")).getLines.mkString("\n")        


        val scriptFile = output("scriptOut")
        scriptFile.write(ioTools + apiDeclaration + script + "\n")
    
        val status = "scala " + scriptFile.getAbsolutePath !   

        for (i <- 1 to nOut ; out = output("out"+i) if !out.exists) {
            "touch " + out.getAbsolutePath !
        }
        

        if (status == 0) ErrorCode.OK 
        else ErrorCode.ERROR
    }

}
