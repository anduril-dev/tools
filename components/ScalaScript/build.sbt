// Makefile for Simple Build Tool (http://www.scala-sbt.org/), version 0.10+
// Compile JAR file: sbt package
// Continuous compilation: sbt ~package
// Build API: sbt doc
// 
// Environment requirements:
// - $ANDURIL_HOME is defined

name <<= baseDirectory(_.getName)

scalaVersion := "2.11.0"

sourceDirectories in Compile := Seq(file("."))

unmanagedClasspath in Compile += file(System.getenv("ANDURIL_HOME")) / "anduril.jar"

unmanagedBase <<= baseDirectory(_ / "../../lib/java")

crossTarget <<= baseDirectory

artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  artifact.name + "." + artifact.extension
}
