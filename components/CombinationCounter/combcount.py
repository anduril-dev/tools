#!/usr/bin/python
import sys
from anduril import *
from anduril.args import *
import itertools

compare = lambda a,b: len(a)==len(b) and len(a)==sum([1 for i,j in zip(a,b) if i==j])
table=TableReader(input_in, column_types=str)

cols=[]
for col in columns.split(","):
    cols.append( table.get_column(col.strip()) )
colu=[]
for c in cols:
    colu.append(sorted(list(set(c))))
coljoin=zip(*cols)
del cols
colujoin=[]
for s in itertools.product(*colu):
    colujoin.append( s )

# calculate occurences of rows of all possible combinations of cols
counts=[]
for c in colujoin:
    counts.append(c+(
        sum([ 1 for x in coljoin if compare(c,x) ]),
        sum([ 1 for x in coljoin if c[0] == x[0] ])
        ) )
ratios=[]
for c in counts:
    ratios.append(float(c[-2])/float(c[-1]))
# Write counts
fields=[x.strip() for x in columns.split(",")]+["Count","Total","Ratio"]
table=TableWriter(output_out,fieldnames=fields)
for r in zip(counts,ratios):
    table.writerow(list(r[0])+[r[1]])

# Write ratios
if len(colu)!=2:
    write_log("Ratio table works only with two input columns")
    open(output_ratios,'w')
    sys.exit(0)
table=TableWriter(output_ratios,
    fieldnames=["/".join([x.strip() for x in columns.split(",")][0:2])] + list(colu[1]))
i=0
for c1 in range(len(colu[0])):
    table.writerow(
        [colu[0][c1]]+
        ratios[i:i+len(colu[1])]
    )
    i+=len(colu[1])

