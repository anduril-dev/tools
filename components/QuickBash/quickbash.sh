# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh" "$1"

# use these functions to read command file
script=$( getinput command )
scriptpar=$( getparameter script )
export tmp=$( gettempdir )
scripttmp="${tmp}/run.sh"

export in=$( getinput in )
export inIndex=$( getinput _index_in )
export out=$( getoutput out )

echo -e source "${ANDURIL_HOME}/lang/bash/functions.sh" \"${1}\"\\n >> "${scripttmp}"
if [ ! -z "$scriptpar" ]; then 
  echo -e "${scriptpar}" >> "${scripttmp}"
else
  cat "${script}" >> "${scripttmp}"
fi

test -e "$inIndex" && {
  test $( wc -l < "$inIndex" ) -lt 100 && {
    exportarraykeys in
  } || { 
    echo "Input array contains more than 100 lines, not populating key variables. You can manually do it with 'exportarraykeys in'" >> "$logfile"
  }
}
export_command

# The last \r replace is for DOS format script files... 
sed -i -e 's,\r$,,' "${scripttmp}" 

cd "$( dirname "$1" )"
bash "$scripttmp" 
