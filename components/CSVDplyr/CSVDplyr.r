library(componentSkeleton)
library(dplyr)

execute <- function(cf) {
    FUNCTIONS<-9
    if (input.defined(cf, 'csv1')) {
        csv1 <- CSV.read(get.input(cf, 'csv1'))
        start.point <- 'csv1'
    }
    if (input.defined(cf, 'csv2')) {
        csv2 <- CSV.read(get.input(cf, 'csv2'))
    }
    if (input.defined(cf, 'csv3')) {
        csv3 <- CSV.read(get.input(cf, 'csv3'))
    }
    if (input.defined(cf, 'in')) {
        array.temp    <- Array.read(cf, 'in')
        for(i in 1:Array.size(array.temp)) {
            key <- Array.getKey(array.temp, i)
            file <- Array.getFile(array.temp, key)
            eval(parse(text=paste("csv.",key,"<-CSV.read(file)",sep="")))
            if (i==1) {
                if (! input.defined(cf, 'csv1')) {
                    start.point <- sprintf("csv.%s", key)
                }
            }
        }
    }
    script  <- get.parameter(cf, 'script', type='string')    
    if(script != "")
      eval(parse(text=script))
    if (! exists("start.point"))
        write.error(cf,"Either csv1 input, or Array in must be defined")
    command <- sprintf("out <- %s", start.point)
    for (i in 1:FUNCTIONS) {
        param.name <- sprintf('function%d', i)
        f <- get.parameter(cf, param.name, type='string')
        if(f != "")
            command <- paste(command, f, sep=" %>% ")
    }
    eval(parse(text=command))
    
    write.table(out, get.output(cf, 'out'), col.names=T,row.names=F,sep="\t",quote=F)
    return(0)
}


main(execute)
