library(componentSkeleton)
library(igraph)

# igraph 0.6 indexes vertices from 1; earlier from 0
if (packageVersion('igraph') >= '0.6') {
    index.base <<- 1
} else {
    index.base <<- 0
}

execute <- function(cf) {
    graph <- read.graph(get.input(cf, 'graph'), format='graphml')

    # Import attributes    
    graph <- import.graph(cf, graph)
    graph <- import.vertex(cf, graph)
    graph <- import.edge(cf, graph)
    
    if ('id' %in% list.vertex.attributes(graph)) {
        graph <- set.vertex.attribute(graph, 'originalID', value=V(graph)$id)
        graph <- my.remove.vertex.attr(graph, 'id')
    }
    
    # Extract attributes
    
    result <- extract.graph(cf, graph)
    if (result != 0) return(result)

    result <- extract.vertex(cf, graph)
    if (result != 0) return(result)
    
    result <- extract.edge(cf, graph)
    if (result != 0) return(result)

    # Remove special id attribute from vertices
    graph <- my.remove.vertex.attr(graph, 'id')
    igraph::write.graph(graph, get.output(cf, 'graph'), format='graphml')
    return(0)
}

my.remove.vertex.attr <- function(graph, attr.name) {
    while (attr.name %in% list.vertex.attributes(graph)) {
        graph <- remove.vertex.attribute(graph, attr.name)
    }
    return(graph)
}

### Import functions ###################################################

import.graph <- function(cf, graph) {
    if (!input.defined(cf, 'graphAttributes')) return(graph)
    
    in.frame <- CSV.read(get.input(cf, 'graphAttributes'))
    if (nrow(in.frame) > 0) {
        for (i in 1:nrow(in.frame)) {
            attr.name <- in.frame[i, 1]
            value <- in.frame[i, 2]
            graph <- set.graph.attribute(graph, attr.name, value)
        }
    }
    
    return(graph)
}

import.vertex <- function(cf, graph) {
    if (!input.defined(cf, 'vertexAttributes')) return(graph)

    in.frame <- CSV.read(get.input(cf, 'vertexAttributes'))
    graph.ids <- as.character(get.vertex.attribute(graph, get.parameter(cf,'idAttrib')))
    in.ids <- as.character(in.frame[,1])
    rownames(in.frame) <- in.ids
    ids <- intersect(graph.ids, in.ids)
    if (length(ids) != length(in.ids)) {
        write.log(cf,
            sprintf('Warning: the following node IDs are not present in the GraphML file: %s',
            paste(setdiff(in.ids, ids), collapse=', ')))
    }
    
    vertices <- V(graph)[match(ids, graph.ids)-1+index.base]
    stopifnot(length(ids) == length(vertices))

    if (nrow(in.frame) >= 1 && length(vertices) > 0) {
        for (col.index in 2:ncol(in.frame)) {
            attr.name <- colnames(in.frame)[col.index]
            values <- get.vertex.attribute(graph, attr.name, index=vertices)
            if (is.null(values)) {
                # This is a bug fix for the numerical columns with missing values
                graph <- set.vertex.attribute(graph, attr.name, value=0)
                graph <- set.vertex.attribute(graph, attr.name, value=NA)
                values <- rep(NA, length(vertices))
            }
            new.values <- in.frame[ids, col.index]
            mask <- !is.na(new.values)
            values[mask] <- new.values[mask]
            stopifnot(length(values) == length(vertices))
            graph <- set.vertex.attribute(graph, attr.name, vertices, value=values)
        }
    }

    return(graph)    
}

import.edge <- function(cf, graph) {
    if (!input.defined(cf, 'edgeAttributes')) return(graph)
    
    in.frame <- CSV.read(get.input(cf, 'edgeAttributes'))
    edge.frame <- get.edges(graph, E(graph))
    
    idAttr <- get.parameter(cf, 'idAttrib')
    if (nrow(in.frame) >= 1 && ncol(in.frame) > 2) {
        vertIDs <- get.vertex.attribute(graph,idAttr)
        for (row.index in 1:nrow(in.frame)) {
            id1 <- in.frame[row.index, 1]
            id2 <- in.frame[row.index, 2]

            vert1 <- V(graph)[vertIDs == id1]
            vert2 <- V(graph)[vertIDs == id2]
            
            if (length(vert1) == 0) {
                write.log(cf,
                    sprintf('Warning: vertex %s is not present in the GraphML file',
                        id1))
                next
            }
            if (length(vert2) == 0) {
                write.log(cf,
                    sprintf('Warning: vertex %s is not present in the GraphML file',
                        id2))
                next
            }
            
            if (is.directed(graph)) {
                matches <- edge.frame[,1] == vert1 & edge.frame[,2] == vert2
            } else {
                matches <- (edge.frame[,1] == vert1 & edge.frame[,2] == vert2) |
                    (edge.frame[,1] == vert2 & edge.frame[,2] == vert1)
            }
            edges <- E(graph)[matches]
            if (length(edges) == 0) {
                write.log(cf,
                    sprintf('Warning: edge %s, %s is not present in the GraphML file',
                        id1, id2))
                next
            }
            
            for (col.index in 3:ncol(in.frame)) {
                attr.name <- colnames(in.frame)[col.index]
                
                if (is.null(get.edge.attribute(graph, attr.name, index=edges))) {
                    # This is a bug fix for the numerical columns with missing values
                    graph <- set.edge.attribute(graph, attr.name, value=0)
                    graph <- set.edge.attribute(graph, attr.name, value=NA)
                }
                new.value <- in.frame[row.index, col.index]
                values <- rep(new.value, length(edges))
                if (is.na(new.value)) next
                graph <- set.edge.attribute(graph, attr.name, edges, value=values)
            }
        }
    }
    
    return(graph)
}


### Export functions ###################################################

extract.graph <- function(cf, graph) {
    attrs <- list.graph.attributes(graph)
    if (length(attrs) == 0) {
        fr <- data.frame(Name=character(0), Value=character(0), stringsAsFactors=FALSE)
    } else {
        fr <- data.frame(Name=attrs, Value=NA, stringsAsFactors=FALSE)
        for (i in 1:length(attrs)) {
            value <- get.graph.attribute(graph, attrs[i])
            fr[i, 'Value'] <- value
        }
    }
    CSV.write(get.output(cf, 'graphAttributes'), fr)
    return(0)
}

extract.vertex <- function(cf, graph) {
    empty <- rep(NA, vcount(graph))
    fr <- data.frame(Vertex=empty, stringsAsFactors=FALSE)
    
    row.index <- 1
    for (vertex in V(graph)) {
        fr[row.index, 'Vertex'] <- paste('n', vertex - index.base, sep='')

        for (attr.name in list.vertex.attributes(graph)) {
            value <- get.vertex.attribute(graph, attr.name, vertex)
            fr[row.index, attr.name] <- value
        }
        
        row.index <- row.index + 1
    }
    if (nrow(fr) == 0){
        attrs <- list.vertex.attributes(graph)
        fr2 <- matrix(NA, nrow=0, ncol=length(attrs))
        colnames(fr2) <- attrs
        fr <- cbind(fr, as.data.frame(fr2))
    }
    CSV.write(get.output(cf, 'vertexAttributes'), fr)
    return(0)
}

extract.edge <- function(cf, graph) {
    empty <- rep(NA, ecount(graph))
    fr <- data.frame(Vertex1=empty, Vertex2=empty, stringsAsFactors=FALSE)
    
    row.index <- 1
    for (edge in E(graph)) {
        vertices <- get.edge(graph, edge)
        from <- paste('n', vertices[1] - index.base, sep='')
        to <- paste('n', vertices[2] - index.base, sep='')
        fr[row.index, 'Vertex1'] <- from
        fr[row.index, 'Vertex2'] <- to
        
        for (attr.name in list.edge.attributes(graph)) {
            value <- get.edge.attribute(graph, attr.name, edge)
            fr[row.index, attr.name] <- value
        }
        
        row.index <- row.index + 1
    }
    if (nrow(fr) == 0){
        attrs <- list.edge.attributes(graph)
        fr2 <- matrix(NA, nrow=0, ncol=length(attrs))
        colnames(fr2) <- attrs
        fr <- cbind(fr, as.data.frame(fr2))
    }
    
    CSV.write(get.output(cf, 'edgeAttributes'), fr)
    return(0)
}

main(execute)
