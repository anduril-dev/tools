<component>

<name>TSNE</name>
<version>1.1</version>
<doc>This component performs dimensionality reduction with an R wrapper of the C++ implementation of Barnes-Hut-SNE as described in http://lvdmaaten.github.io/tsne/. Remember to cite his paper whenever you use this component.</doc>
<author email="julia.casado@helsinki.fi">Julia Casado</author>
<category>Multivariate Statistics</category>
<launcher type="R">
<argument name="file" value="tsne.R"/>
</launcher>
<requires>R</requires>
<requires name="installer" optional="false">Rtsne
<resource type="bash">install.bash</resource>
</requires>
<inputs>
<input name="in" type="CSV">
<doc>A numeric matrix on which dimensionality reduction will be applied. Rows represent datapoints, e.g. cells/patients, and columns represent the dimensions, e.g. features or markers, that we want to reduce.</doc>
</input>
</inputs>

<outputs>
<output name="out" type="CSV">
<doc>A numeric matrix with two or three columns, depending on dims parameter.</doc>
</output>
<output name="entropy" type="CSV">
<doc>A matrix of entropy estimates in natural-base units (nats) for each sample. The column "orig" is an estimate of entropy in the original space, while "lots" is an estimate of the Kullback-Leibler divergence from the output to the input space.</doc>
</output>
</outputs>

<parameters>
<parameter name="dims" type="int" default="2">
<doc>Output dimensionality. Possible values are 2 or 3 because the original method was developed for visualization purposes and not thoroughly tested for larger dimensionality.</doc>
</parameter>
<parameter name="initial_dims" type="int" default="50">
<doc>Number of dimensions in a preliminary step of dimensionality reduction using PCA. Only read if parameter pca is true.</doc>
</parameter>
<parameter name="perplexity" type="int" default="30">
<doc>It is a measure of information that in this case can be used as the number of nearest neighbors k that is employed in many manifold learners. If the visualization out of the output shows most of the points clustered like a ball means that the perplexity parameter was too high. It will depend on the size and structure of the data.</doc>
</parameter>
<parameter name="theta" type="float" default="0">
<doc>Variable for Speed/accuracy trade-off. Higher theta means shorter running time and less accuracy of the results. Change only if the dataset is really really big.</doc>
</parameter>
<parameter name="check_duplicates" type="boolean" default="false">
<doc>It is best to check for duplicates with previous components because for big files this check-up will take too long time.</doc>
</parameter>
<parameter name="pca" type="boolean" default="false">
<doc>Recommended for big files, over 5000 datapoints and 100 features. If true, it will run first basic PCA to reduce the dimensions. May result in poor performance for small datasets.</doc>
</parameter>
<parameter name="max_iter" type="int" default="1000">
<doc>Number of iterations.</doc>
</parameter>
<parameter name="num_threads" type="int" default="1">
<doc>Number of threads to use.</doc>
</parameter>
<parameter name="cost_tol" type="float" default="1.48e-8">
<doc>Tolerance for cost function stall.</doc>
</parameter>
<parameter name="is_distance" type="boolean" default="false">
<doc>Indicates whether the input is a distance matrix. In the documentation at the time of creating this component it warns that is an experimental feature. Use at own risk.</doc>
</parameter>
<parameter name="seed" type="int" default="-1">
<doc>Seed number to make test cases reproducible. If null, the system generates one every time.</doc>
</parameter>
<parameter name="entropy_fast" type="boolean" default="true">
<doc>Use fast and cheap entropy approximation.</doc>
</parameter>
<parameter name="verbose" type="boolean" default="true">
<doc>Log the tsne process to terminal</doc>
</parameter>
</parameters>

</component>
