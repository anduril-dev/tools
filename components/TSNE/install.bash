#!/bin/bash
set -e

destdir='../../lib/rtsne'
mkdir -p "$destdir"
R --slave <<EOF
install.packages("Rtsne", lib = '$destdir')
EOF
