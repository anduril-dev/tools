library(componentSkeleton)

execute <- function(cf) {
    matr <- Matrix.read(get.input(cf, 'matr'))
    
    present <- sapply(1:nrow(matr), function(i) all(!is.na(matr[i,])))
    cor.method <- get.parameter(cf, 'method')
    
    # First compute correlation matrix r and write it to output, then
    # compute r^2 matrix and use it for plotting.
    cor.matrix <- cor(matr[present,], method=cor.method)
    colnames(cor.matrix) <- colnames(matr)
    rownames(cor.matrix) <- colnames(matr)
    CSV.write(get.output(cf, 'correlation'), cor.matrix)
    cor.matrix <- cor.matrix ^ 2
    out.dir <- get.output(cf, 'report')
    dir.create(out.dir, recursive=TRUE)
    
    pdf.filename <- sprintf('CorrelationReport-%s-heatmap.pdf', get.metadata(cf, 'instanceName'))
    pdf(file.path(out.dir, pdf.filename))
    color <- gray.colors(1000, 0, 1, gamma=2.2)
    # Clip to range 0..1 just in case.
    cor.matrix[cor.matrix < 0] <- 0
    cor.matrix[cor.matrix > 1] <- 1
    heatmap(cor.matrix, Rowv=NA, Colv=NA, symm=TRUE, zlim=c(0, 1), scale='none', col=color)	
    dev.off()
    
    caption <- paste(
        sprintf('Heat map of correlation coefficients $r^2$ using %d samples and %d measurements for each sample.',
            ncol(cor.matrix), length(which(present))),
        'Black color denotes low correlation ($r^2$ near zero) and',
        'white color high correlation ($r^2$ near one).',
        sprintf('Correlation method: %s.', cor.method))
    if (any(!present)) {
        caption <- paste(caption,
            sprintf('There were %d rows with a missing value in at least one sample.', length(which(!present))),
            'Such rows were filtered out and are not included in the measurement count above.')
    }

    if (get.parameter(cf, 'showTable', 'boolean')) {
        if (get.parameter(cf, 'rotateLabels', 'boolean')) {
            # This use the rotating LaTeX package.
            colnames(cor.matrix) <- paste('\\begin{sideways}',
                latex.quote(colnames(cor.matrix)), '\\end{sideways}')
        }
        tex <- latex.table(cor.matrix,
                           paste('l', rep('r', ncol(cor.matrix)), sep='', collapse=''),
                           caption='Correlation coefficients ($r^2$) between samples.',
                           use.row.names=TRUE,
                           escape=FALSE)
    } else {
        tex <- ''
    }

    tex <- c('\\subsection{Correlation between samples}',
             latex.figure(pdf.filename, caption=caption, quote.capt=FALSE),
             tex,
             '\\clearpage')
    latex.write.main(cf, 'report', tex)
    return(0)
}

main(execute)
