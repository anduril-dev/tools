#!/usr/bin/python
import sys
from anduril.args import *
import os
import shutil
import re

exec(preprocess) #there is a possiblity of name collisions!

exts=[i.lower().strip() for i in extensions.split(',')]
DIGIT=re.compile(r"\d+")
compare = lambda a,b: len(a)==len(b) and len(a)==sum([1 for i,j in zip(a,b) if i==j])
pattern=re.compile( filePattern )

def getfilelist(path):
    ''' Returns a list of files '''
    list=os.listdir(path)
    files=[]
    for fName in list:
        if fName[0] not in ['.','_']:
            files.append(fName)
    return files

def dotjoin(s, testList):
    ''' Joins a list of strings with dots, removing bad extensions, creating unique file names. '''
    inSplit=s.split('.')
    for c in range(1,len(inSplit)-1):
        if inSplit[c].lower() in exts:
            inSplit[c]=''
    inSplit=[ x for x in inSplit if x is not '' ]
    return uniquename(testList,inSplit)

def uniquename(testList,newitem):
    ''' Returns a unique new list item, in the fashion of  "string"_1 ... '''
    if not ".".join(newitem) in testList:
        return ".".join(newitem)
    i=1
    if len(newitem)==1:
        newitem=[newitem[0], str(i)]
    if len(newitem)==2:
        if DIGIT.match(newitem[1]):
            while ".".join(newitem) in testList:
                newitem=[newitem[0], str(i)]
                i+=1
            return ".".join(newitem)
        else:
            newitem=[newitem[0], str(i), newitem[1]]
        
    while ".".join(newitem) in testList:
        newitem=newitem[0:-2]+[str(i), newitem[-1]]
        i+=1
    return ".".join(newitem)

os.mkdir(output_out)
if input_in is not None:
    inList=getfilelist(input_in)
    outList=[]
    for inName in inList:
        if not pattern.match(inName):
            continue
        inPath=os.path.join(input_in,inName)
        if eval(remove):
            continue
        outName=dotjoin(inName, outList)
        name=outName
        outName=str(eval(rename))
        outList.append(outName)
        print(inName+' -> '+outName)
        if param_link:
            os.symlink(inPath,os.path.join(output_out,outName))
        else:
            shutil.copy2(os.path.join(input_in,inName), os.path.join(output_out,outName))

if input_inArray is not None:
    outList=[]
    for inElement in input_inArray.items():
        if not pattern.match(inElement[0]):
            continue
        inName=inElement[0]
        inPath=inElement[1]
        if eval(remove):
            continue
        outName=dotjoin(inName, outList)
        name=outName
        outName=str(eval(rename))
        outList.append(outName)
        print(inName+' -> '+outName)
        output_outArray.write(outName,inPath)






