library(componentSkeleton)

MAX.FRAGMENTS <- 9

execute <- function(cf) {
    outDir   <- get.output(cf, 'document')
    useFlush <- get.parameter(cf, 'strictBorders', type='boolean')
    results  <- get.parameter(cf, 'head')
    if (nchar(results) == 0) {
       results <- character()
    }

    if (get.parameter(cf, 'pagebreak', type='boolean')) {
        results <- c(results, '\\clearpage{}')
    }
    section <- get.parameter(cf, 'sectionTitle')
    if (nchar(section) > 0) {
        results <- c(results,
                     sprintf('\\%s{%s}', get.parameter(cf, 'sectionType'), section),
                     sprintf('\\label{%s}',   get.metadata(cf, 'instanceName')))
    }

    for (i in 1:MAX.FRAGMENTS) {
        input.name <- paste('latex', i, sep='')
        if (input.defined(cf, input.name)) {
            dir.name <- get.input(cf, input.name)
            docfile <- file.path(dir.name, LATEX.DOCUMENT.FILE)
            if (!file.exists(docfile)) {
                write.error(cf, sprintf('Input %s: main LaTeX file %s not found', input.name, LATEX.DOCUMENT.FILE))
                return(1)
            }
            if (useFlush) {
               results <- c(results, '\\FloatBarrier{}', textfile.read(docfile))
            } else {
               results <- c(results, textfile.read(docfile))
            }
            copy.dir(dir.name, outDir, exclude.names=LATEX.DOCUMENT.FILE)
        }
    }
    
    # Latex-combine input array.
    if (input.defined(cf, 'array')) {
        array <- Array.read(cf, 'array')
        if (Array.size(array) > 0)
        for (i in 1:Array.size(array)) {
            texDir  <- Array.getFile(array, i)
			docfile <- file.path(texDir, LATEX.DOCUMENT.FILE)
			if (!file.exists(docfile)) {
				write.error(cf, sprintf('Input %s: main LaTeX file %s not found', texDir, LATEX.DOCUMENT.FILE))
				return(1)
			}
			if (useFlush) {
			   results <- c(results, '\\FloatBarrier{}', textfile.read(docfile))
			} else {
			   results <- c(results, textfile.read(docfile))
			}
			copy.dir(texDir, outDir, exclude.names=LATEX.DOCUMENT.FILE)
        }
    }

    if (nchar(get.parameter(cf, 'tail')) > 0) {
        results <- c(results, get.parameter(cf, 'tail'))
    }
    if (!file.exists(outDir)) {
        dir.create(outDir)
    }
    if (input.defined(cf, 'resources')) {
       array <- Array.read(cf, 'resources')
       if (Array.size(array) > 0)
       for (i in 1:Array.size(array)) {
           docFile <- Array.getFile(array, i)
           file.copy(docFile, outDir)
       }
    }

    docfile <- file.path(outDir, LATEX.DOCUMENT.FILE)
    if (length(results) == 0) results <- ''
    textfile.write(docfile, paste(results, collapse='\n'))
    return(0)
}

main(execute)
