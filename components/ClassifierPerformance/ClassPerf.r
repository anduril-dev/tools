library(componentSkeleton)

# Sokolova, M., & Lapalme, G. (2009). A systematic analysis of performance measures for classification tasks. Information Processing and Management, 45, p. 427-437.
# http://rali.iro.umontreal.ca/rali/sites/default/files/publis/SokolovaLapalme-JIPM09.pdf

two_class <- function(pred_table) {
  tp<-pred_table[1,1]
  fp<-pred_table[1,2]
  fn<-pred_table[2,1]
  tn<-pred_table[2,2]
  
  accuracy<-   (tp+tn)/(tp+tn+fp+fn)
  precision<-  (tp)/(tp+fp)
  sensitivity<-(tp)/(tp+fn)
  specificity<-(tn)/(fp+tn)
  error_rate<- (fp+fn)/(tp+tn+fp+fn)
  bsq<-1
  f1score<- ( bsq + 1 )*tp/( ( bsq + 1 )*tp + bsq*fn +fp )  #  F1 score
  auc<- 0.5*( precision + specificity )

  measures<-cbind(
            accuracy,
            precision,
            sensitivity,
            specificity,
            error_rate,
            f1score,
            auc)
  colnames(measures)<-c(
            'Accuracy',
            'Precision',
            'Sensitivity',
            'Specificity',
            'ErrorRate',
            'F1Score',
            'AUC')
  return(measures)
}

multi_class <- function(pred_table) {
  precision<-c()
  sensitivity<-c()
  specificity<-c()
  accuracy<-c()
  error_rate<-c()
  tp <- tn <- fp <- fn <- c()  
  conditions<-rownames(pred_table)
  for (cond in 1:length(conditions)) {
    tp[cond] <- pred_table[cond,cond]
    fn[cond] <- sum(pred_table[cond,-cond])
    tn[cond] <- sum(pred_table[-cond,-cond])
    fp[cond] <- sum(pred_table[-cond,cond])
    
    mu_table<- rbind( cbind( tp[cond], fn[cond]),cbind( fp[cond], tn[cond]))
    rownames(mu_table)<-c("Positive","Negative")
    colnames(mu_table)<-c("Positive","Negative")
    print(paste("Class:",conditions[cond]))
    print(mu_table)
    
    accuracy[cond]<-   (tp[cond]+tn[cond])/(tp[cond]+tn[cond]+fp[cond]+fn[cond])
    precision[cond]<-  (tp[cond])/(tp[cond]+fp[cond])
    sensitivity[cond]<-(tp[cond])/(tp[cond]+fn[cond])
    specificity[cond]<-(tn[cond])/(fp[cond]+tn[cond])
    error_rate[cond]<- (fp[cond]+fn[cond])/(tp[cond]+tn[cond]+fp[cond]+fn[cond])

  }
  
  accuracy_M <- mean(accuracy,na.rm=T)
  precision_M <- mean(precision,na.rm=T)
  sensitivity_M <- mean(sensitivity,na.rm=T)
  specificity_M <- mean(specificity,na.rm=T)
  error_rate_M <- mean(error_rate,na.rm=T)

  accuracy_m <- sum(tp+tn) / sum(tp+tn+fp+fn)
  precision_m <- sum(tp) / sum(tp+fp)
  sensitivity_m <- sum(tp) / sum(tp+fn)
  specificity_m <- sum(tn) / sum(fp+tn)
  error_rate_m <- sum(fp+fn) / sum(tp+tn+fp+fn)
  
  bsq<-1
 
  f1score_m   <- ( bsq + 1 )*precision_m*sensitivity_m/(bsq*precision_m + sensitivity_m)  #  F1 score micro
  f1score_M   <- ( bsq + 1 )*precision_M*sensitivity_M/(bsq*precision_M + sensitivity_M)  #  average per-class F1 score
  
  measures<-cbind(
            accuracy_m,
            precision_m,
            sensitivity_m,
            specificity_m,
            error_rate_m,
            f1score_m,
            accuracy_M,
            precision_M,
            sensitivity_M,
            specificity_M,
            error_rate_M,
            f1score_M)
  colnames(measures)<-c(
            'Accuracy_u',
            'Precision_u',
            'Sensitivity_u',
            'Specificity_u',
            'ErrorRate_u',
            'F1Score_u',
            'Accuracy_M',
            'Precision_M',
            'Sensitivity_M',
            'Specificity_M',
            'ErrorRate_M',
            'F1Score_M'
            )
  return(measures)
  
}

execute <- function(cf) {
    #inputs
    name <- get.parameter(cf, 'name')
    predCol <- get.parameter(cf, 'predCol')
    classCol <- get.parameter(cf, 'classCol')
    conditions <- split.trim(get.parameter(cf, 'conditions'), ',')
    input <- CSV.read(get.input(cf, 'in'))

    if (identical(conditions, '*')) {
      conditions<-sort(unique(append(input[,classCol],input[,predCol])))
    }
    
    pred_table<-matrix(ncol=length(conditions),nrow=length(conditions))
    colnames(pred_table)<-conditions
    rownames(pred_table)<-conditions
    for (condx in 1:length(conditions)) {
      for (condy in 1:length(conditions)) {
        value<-sum(input[,classCol]==conditions[condy] & input[,predCol]==conditions[condx])
        pred_table[condx,condy]<-value
    }}
    print(pred_table)
    #output
    out <- get.output(cf, 'out')

    if (length(conditions)==2) {
      measures<-two_class(pred_table)
    } else { 
      measures<-multi_class(pred_table)
    }

    measures<-cbind(name, measures)
    colnames(measures)[1]<-'Name'
    CSV.write(out, measures)


}
main(execute)
