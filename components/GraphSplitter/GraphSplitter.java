import java.io.File;
import java.util.LinkedList;

import edu.uci.ics.jung.graph.Graph;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.IndexFile;
import org.anduril.component.SkeletonComponent;

import org.anduril.javatools.graphMetrics.GraphML;
import org.anduril.javatools.graphMetrics.SplitGraph;


public class GraphSplitter extends SkeletonComponent {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	GraphSplitter().run(args);
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		File	arrayDir=cf.getOutput("out");
		IndexFile	Idx=new	IndexFile();
		String	splitType=cf.getParameter("splitType");
		int	i=0;
		
		int	minimalSizeFilter=Integer.valueOf(cf.getParameter("filterLessThan"));
		arrayDir.mkdirs();
		GraphML reader = new GraphML(cf.getInput("in"));
		reader.loadGraph();
		Graph<String,String> graph = reader.getGraph();
		LinkedList<Graph<String,String>>	graphs = null;
		if(splitType.equalsIgnoreCase("connected"))
			graphs=SplitGraph.conComponents(graph);
		if(splitType.equalsIgnoreCase("strongly"))
			graphs=SplitGraph.strongConComponents(graph);
		for(Graph<String,String>	subGraph:graphs)
		{
			if(subGraph.getVertexCount()>=minimalSizeFilter)
			{
				String	subGraphName="subgraph"+String.format("%04d", i++);
				File	subGraphFile=new	File(arrayDir.getAbsolutePath()+File.separator+subGraphName+".graphml");
				reader.saveGraph(subGraph, subGraphFile);
				
				Idx.add(subGraphName, new File(subGraphName+".graphml"));
			}
		}
		Idx.write(cf, "out");
		return ErrorCode.OK;
	}

}
