library(componentSkeleton)

execute <- function(cf) {
  data  <- CSV.read(get.input(cf, 'data'))
  dK    <- data[,1]
  data  <- as.matrix(data[,2:ncol(data)])

  pairs <- CSV.read(get.input(cf, 'pairs'))
  p1Col <- get.parameter(cf, 'pairCol1')
  p2Col <- get.parameter(cf, 'pairCol2')
  pCols <- colnames(pairs)
  if (p1Col == '') p1Col <- pCols[1]
  if (p2Col == '') p2Col <- pCols[2]
  pairs <- pairs[,c(p1Col,p2Col)]

  method <- get.parameter(cf, 'method')
  if (method == 'cor-spearman') method <- 'spearman'
  else
  if (method == 'cor-pearson')  method <- 'pearson'
  else
  stop(sprintf('Invalid method: %s', method))

  limU <- get.parameter(cf, 'rUpMin',   'float')
  limD <- get.parameter(cf, 'rDownMax', 'float')
  limP <- get.parameter(cf, 'pLimit',   'float')

  rTable <- data.frame(ID1=character(0),
                       ID2=character(0),
                       r  =numeric(0),
                       p  =numeric(0),
                       stringsAsFactors = FALSE)
  misses <- character(0);
  rI     <- 1
  for (i in 1:nrow(pairs)) {
      p1 <- data[dK==pairs[i,1],]
      if (length(p1) < 1) { misses <- union(misses,pairs[i,1]); next }
      p2 <- data[dK==pairs[i,2],]
      if (length(p2) < 1) { misses <- union(misses,pairs[i,2]); next }
      corRes <- try( cor.test(p1, p2, method=method, continuity=TRUE), silent=TRUE )
      if (class(corRes) == "try-error") next
      if (is.na(corRes$p.value)) next
      if ((corRes$p.value <= limP) &&
          ((corRes$estimate <= limD) || (corRes$estimate >= limU))) {
         rTable[rI, 1:2] <- pairs[i,]
         rTable[rI, 3]   <- corRes$estimate
         rTable[rI, 4]   <- corRes$p.value
         rI <- rI + 1
      }
  }

  if (length(misses) > 0) {
     write.log(cf, sprintf('No data for: %s.', paste(misses,collapse=', ')))
  }

  CSV.write(get.output(cf, 'stats'),   rTable)
  CSV.write(get.output(cf, 'skipped'), data.frame(ID=misses,stringsAsFactors=FALSE))
  return(0)
}

main(execute)
