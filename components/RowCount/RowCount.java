import java.io.File;
import java.io.IOException;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;
import org.anduril.core.network.Component;
import org.anduril.asser.io.CSVParser;

/**
 * This component determines if the input CSV has small, medium, or
 * large number or rows.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 1.83
 */
public class RowCount extends SkeletonComponent {

    static public final String INPUT_DATA     = "relation";
    static public final String OUTPUT_DIMS    = "dimensions";
    static public final String OUTPUT_CATEG   = "category";
    static public final String PARAM_LIMIT1   = "limit1";
    static public final String PARAM_LIMIT2   = "limit2";
    static public final String PARAM_ROW_PROP = "rowProp";
    static public final String PARAM_COL_PROP = "colProp";

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        File      inputFile = cf.getInput(INPUT_DATA);
        CSVParser in        = new CSVParser(inputFile);
        String    myName    = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
        int       limit1    = cf.getIntParameter(PARAM_LIMIT1);
        int       limit2    = cf.getIntParameter(PARAM_LIMIT2);
        int       rowCount  = 0;
        String    category;

        // Count rows...
        while (in.hasNext()) {
            in.next(); // We may just ignore the actual content!
            rowCount++;
        }

        // Select category...
        if (rowCount < limit1)
            category = "small";
        else if ((rowCount < limit2) || (limit2 < 0))
            category = "medium";
        else
            category = "large";
        cf.writeLog(String.format("Input contains %d rows. Category selection is %s.", rowCount, category));
        Tools.writeString(cf.getOutput(OUTPUT_CATEG), category);

        String rowProp = cf.getParameter(PARAM_ROW_PROP).trim();
        String colProp = cf.getParameter(PARAM_COL_PROP).trim();
        if (rowProp.length() < 1) rowProp = myName+".rows";
        if (colProp.length() < 1) colProp = myName+".cols";
        Tools.writeString(cf.getOutput(OUTPUT_DIMS),
                          rowProp+" = "+rowCount+'\n'+
                          colProp+" = "+in.getColumnCount()+'\n');

        return ErrorCode.OK;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new RowCount().run(argv);
    }

}
