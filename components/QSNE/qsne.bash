#!/bin/bash

# set up Anduril stuff
source "$ANDURIL_HOME/lang/bash/functions.sh" "$1"

## build up the arguments

# set up an array
args=()

# list the standard parameters and their switches
declare -A params
params=([dims]='d' [perplexity]='p' [max_iter]='k' [cost_tol]='t' [rank]='m'
	[num_threads]='T')

# add the standard parameters
for param in "${!params[@]}"; do
	key="${params[$param]}"
	arg="$(getparameter "$param")"
	[ -n "$arg" ] && args+=("-$key$arg")
done

# perplexity range
perp_range="$(getparameter 'perplexity_range')"
if [ "$perp_range" -gt 0 ]; then
	perp="$(getparameter 'perplexity')"
	arg="$( echo "$perp - .5*$perp_range" | bc ):$( echo "$perp + .5*$perp_range" | bc )"
	args+=("-p$arg")
fi

# compatibility mode?
arg="$(getparameter 'compat')"
[ "$arg" = "true" ] && args+=('-C')

# initial guess?
arg="$(getinput 'init')"
[ -n "$arg" ] && args+=("-i$arg")

# output file?
arg="$(getoutput 'out')"
[ -n "$arg" ] && args+=("-o$arg")

# add input file
arg="$(getinput 'in')"
args+=("$arg")

## execute

# execute
path="$(dirname "$0")"
"$path/../../lib/qsne/qsne" "${args[@]}"
