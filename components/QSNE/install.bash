#!/bin/bash

# get Anduril tools
. "$ANDURIL_HOME"/lang/bash/generic.sh || {
	echo "\$ANDURIL_HOME not set!" >&2
	exit 1
}

# halt on error
set -e 

# install q-SNE
qsne_git='https://bitbucket.org/anthakki/qsne/'
qsne_path="$(dirname "$0")/../../lib/qsne/"
if [ ! -x "$qsne_path/qsne" ]; then
	git clone "$qsne_git" "$qsne_path"
	(cd "$qsne_path" && make)
fi
