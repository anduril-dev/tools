import edu.uci.ics.jung.graph.Graph;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.javatools.graphMetrics.ConvertDirected2Undirected;
import org.anduril.javatools.graphMetrics.GraphML;


public class Directed2Undirected extends SkeletonComponent {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	Directed2Undirected().run(args);
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		GraphML reader = new GraphML(cf.getInput("in"));
		reader.loadGraph();
		Graph<String,String> directed = reader.getGraph();
		Graph<String,String>	underlying=ConvertDirected2Undirected.convert(directed);
		reader.saveGraph(underlying, cf.getOutput("out"));
		
		return ErrorCode.OK;
	}
	
	

}
