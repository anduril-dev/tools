import sys,os
import anduril
from anduril.args import *
from lxml import html
import shutil

body = open(body, 'w')
head = open(head, 'w')
style = open(style, 'w')
script = open(script, 'w')

def getElements(htmlSource, body, head, style, script):
    doc = html.parse(htmlSource)
       
    for elt in doc.iter('body'):
        for child in elt.iterdescendants():
            if(child.tag == extractBody or extractBody == ''):
                if(child.getparent().tag == 'body'):
                    body.write(unicode(html.tostring(child)).encode('utf8'))
            
    for elt in doc.iter('head'):
        for child in elt.iterdescendants():
            if(child.tag == 'script' and child.text != None):
                script.write(unicode(child.text).encode('utf8'))
            elif(child.tag == 'style'):
                style.write(unicode(child.text).encode('utf8'))
            elif(child.tag == extractHead or extractHead == ''):
                if(child.getparent().tag == 'head'):
                    head.write(unicode(html.tostring(child)).encode('utf8'))
                
getElements(html1, body, head, style, script)
if(html2 != None):
    getElements(html2, body, head, style, script)

if(htmlArray != None):
    for a in htmlArray:
        getElements(htmlArray[a], body, head, style, script)
    
body.close()
head.close()
style.close()
script.close()
