import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;


public class Excel2Text extends SkeletonComponent {

	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(cf.getInput("in")));
		BufferedWriter out = new BufferedWriter(new FileWriter(cf.getOutput("out")));

		FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		HSSFSheet curSheet = null;

		String sheet = cf.getParameter("sheet");
		if (sheet.equals("")){
			curSheet = wb.getSheetAt(0);
		} else {
			curSheet = wb.getSheet(sheet);
		}

		try {
			for (Row row : curSheet){
				for (Cell cell : row){
					String outCell = null;
					if (cell != null){
						if (cell.getCellType() == HSSFCell.CELL_TYPE_FORMULA){
							CellValue cellValue = evaluator.evaluate(cell);
							if (cellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
								outCell = Double.toString(cellValue.getNumberValue());
							}
						} else if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
							outCell = Double.toString(cell.getNumericCellValue());
						} else if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
							outCell = cell.getStringCellValue().trim();
						}
					}
					if(outCell != null){
						out.write(outCell);
					}
					out.write("\t");
				}
				out.write("\n");
			}
		} finally {
			if (out != null) out.close();
		}
		return ErrorCode.OK;

	}
    public static void main(String[] args) {
        new Excel2Text().run(args);
    }
}
