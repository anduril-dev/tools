library(componentSkeleton)
library(tidyverse)

readArray <- function(cf, arrayName){
    x <- Array.read(cf, arrayName)
    size <- Array.size(x)
    keys <- map_chr(c(1:size), ~ Array.getKey(x, .x))
    files <- map_chr(c(1:size), ~ Array.getFile(x, .x))
    array <- map(files, CSV.read)
    names(array) <- keys
    return(array)
}

execute <- function(cf) {
    ## read inputs 

    if (input.defined(cf, 'csv1')) {
        csv1 <- CSV.read(get.input(cf, 'csv1'))
    }
    if (input.defined(cf, 'csv2')) {
        csv2 <- CSV.read(get.input(cf, 'csv2'))
    }
    if (input.defined(cf, 'csv3')) {
        csv3 <- CSV.read(get.input(cf, 'csv3'))
    }
    if (input.defined(cf, 'array1')) {
        array1 <- readArray(cf, "array1")
    }

    if (input.defined(cf, 'array2')) {
        array2 <- readArray(cf, "array2")
    }
    if (input.defined(cf, 'array3')) {
        array3 <- readArray(cf, "array3")
    }
    if (input.defined(cf, 'var1')) {
        var1 <- get.input(cf, 'var1')
    }
    if (input.defined(cf, 'var2')) {
        var2 <- get.input(cf, 'var2')
    }    
    if (input.defined(cf, 'var3')) {
        var3 <- get.input(cf, 'var3')
    }


    ## create folder output
    folderOut <- get.output(cf, "folderOut")
    dir.create(folderOut)

    ## execute the script
    script  <- get.parameter(cf, 'script', type='string')    
    if(script != "")
      eval(parse(text=script))

    
    ## execute the pipe
    FUNCTIONS <- 9
    for (i in 1:FUNCTIONS) {
        param.name <- sprintf('function%d', i)
        f <- get.parameter(cf, param.name, type='string')
        if(f != ""){
            if(i == 1){
                command <- f
            } else {
            command <- paste(command, f, sep=" %>% ")
            }
        }
    }
    eval(parse(text = command))
    
    ## if nothing is written to output, return an error
    if(!exists("csvOut") & !exists("arrayOut") & length(list.files(folderOut))==0){
      write.error(cf,"Nothing to be written in outputs. Redirect your functions to one of csvOut, arrayOut or folderOut")
    }
    
    ## write the csvOut
    if(!exists("csvOut")){
        csvOut <- data.frame()
    }
    write.table(csvOut, get.output(cf, 'csvOut'), col.names=T, row.names=F, sep="\t", quote=F) 

    
    ## write the arrayOut
    ## code from REvaluate
    array.out.object <- Array.new()
    array.out.dir <- get.output(cf, 'arrayOut')
    dir.create(array.out.dir)
    if (exists('arrayOut')) {        
        for(i in 1:length(arrayOut)) {
            key = names(arrayOut)[i]
            filename = paste(key, ".csv", sep="")
            #CSV.write(paste(array.out.dir, "/", filename, sep=""), arrayOut[[i]])
            write.table(arrayOut[[i]], file = paste(array.out.dir, "/", filename, sep=""), col.names=T, row.names= F, sep = "\t", quote = F)
            array.out.object <- Array.add(array.out.object, key, filename)
        }        
    }

    Array.write(cf, array.out.object, 'arrayOut')  
   
    return(0)
}
main(execute)