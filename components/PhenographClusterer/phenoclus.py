import csv, sys
from anduril.args import *
import phenograph

removeColumns=[x.strip() for x in param_columnsToRemove.split(",")]
r=csv.reader(open(input_in,'rt'),dialect='excel-tab')
data=[]
header=next(r,None)

removeIndex=[i for i,x in enumerate(header) if x in removeColumns]
if param_idColumn:
    idIndex=header.index(param_idColumn)
    removeIndex.append(idIndex)

idColumn=[]
for rowI,row in enumerate(r):
    if param_idColumn:
        idColumn.append(row[idIndex])
    else:
        idColumn.append(str(rowI+1))
    data.append([float(x) for i,x in enumerate(row) if i not in removeIndex])
    
com,gra,Q = phenograph.cluster(data,
            k=param_k, directed=param_directed, prune=param_prune, 
            min_cluster_size=param_minClusterSize, jaccard=param_jaccard,
            primary_metric=param_metric, n_jobs=param_nJobs, q_tol=param_qTol, 
            louvain_time_limit=param_louvainTimeLimit)

if not param_idColumn:
    param_idColumn="index"
w=csv.writer(open(output_out,'w'),dialect='excel-tab')
w.writerow((param_idColumn,'PhenoCluster',))
[w.writerow((i,x)) for i,x in zip(idColumn,com)]

w=csv.writer(open(output_graph,'w'),dialect='excel-tab')
w.writerow(('Start','End','Weight',))
cgra=gra.tocoo()
for i,j,v in zip(cgra.row, cgra.col, cgra.data):
    w.writerow((idColumn[j],idColumn[i],v))

