#!/bin/bash

python3 -c "import phenograph" || {
  [[ $UID -eq 0 ]] && pip3 install setuptools && pip3 install git+https://github.com/jacoblevine/phenograph.git
  [[ $UID -gt 0 ]] && pip3 install --user setuptools && pip3 install --user git+https://github.com/jacoblevine/phenograph.git
}
