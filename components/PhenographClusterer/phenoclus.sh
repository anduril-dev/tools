source "$ANDURIL_HOME/lang/bash/functions.sh" "$1"
export_command
set -e

[[ $parameter_directed = "true" ]] && { param_directed="--directed"; } || { param_directed=""; }
[[ $parameter_prune = "true" ]] && { param_prune="--prune"; } || { param_prune=""; }
[[ $parameter_jaccard = "false" ]] && { param_jaccard="--no_jaccard"; } || { param_jaccard=""; }
python3 phenoclus.py -C "$parameter_columnsToRemove" \
                     -k $parameter_k $param_directed $param_prune $param_jaccard \
                     --min_cluster_size "$parameter_minClusterSize" \
                     --primary_metric "$parameter_metric" \
                     --n_jobs "$parameter_nJobs" \
                     --q_tol "$parameter_qTol" \
                     --louvain_time_limit "$parameter_louvainTimeLimit" \
                     --idColumn "$parameter_idColumn" \
                     --labelFile "$output_out" \
                     --graphFile "$output_graph" \
                     "$input_in" > "$logfile"

