library(componentSkeleton)
library(survminer)
library(ggthemes)
library(survival)
library(ggplot2)
library(cowplot)
library(broom)

execute <- function(cf) {
    csv <- read.table(get.input(cf, 'in'),header = T, sep = "\t")
    
    timeColumn <- get.parameter(cf, "timeColumn",type = "string")
    statusColumn <- get.parameter(cf, "statusColumn",type = "string")
    groupColumn <- get.parameter(cf, "groupColumn",type = "string")
    eventText <- get.parameter(cf, "eventText", type = "string")
    showPval <- get.parameter(cf, "showPval", type = "boolean")
    showRiskTable <- get.parameter(cf, "showRiskTable", type = "boolean")    
    xlabel <- get.parameter(cf, "xlabel", type = "string")
    ylabel <- get.parameter(cf, "ylabel", type = "string")
    otherOptions <- get.parameter(cf, "otherOptions", type = "string")
    ggplotExtra <- get.parameter(cf, "ggplotExtra", type = "string")
    strataCols <- get.parameter(cf, "strataCols", type = "string")
    
    runCox <- ifelse(strataCols != "",T,F)

    if(length(setdiff(c(timeColumn, statusColumn, groupColumn),colnames(csv)))>0)
        stop("make sure you have the correct time, status and group columns")
        
    if(runCox){
       strata <- unlist(strsplit(strataCols, ","))    
      if(length(setdiff(strata,colnames(csv)))>0)
        stop("make sure you have the correct columns")
    }
    
    csv[,statusColumn] <- as.character(csv[,statusColumn])
    if(sum(csv[,statusColumn] == eventText) == 0)
      stop("Are you sure you have eventText spelled correctly?")
    csv[,statusColumn] <- csv[,statusColumn] == eventText  
    
    csv[,groupColumn] <- as.factor(csv[,groupColumn])
    
    surv.model <- eval(parse(text = sprintf('survfit(Surv(%s, %s) ~ %s, data = csv)',timeColumn, statusColumn, groupColumn)))
    write.table(surv_pvalue(surv.model, data=csv), file = get.output(cf, "survPvalue"), col.names=T, row.names=F, sep= "\t",quote=F)
    
    if(runCox){
      strata <- paste(strata, collapse = " + ")
      cox.model <- eval(parse(text = sprintf('coxph(Surv(%s, %s) ~ %s + %s, data = csv)',timeColumn, statusColumn, groupColumn, strata)))
    }
    gcall <- "ggsurvplot(surv.model, data = csv, censor =T,"
    mainParams <- " pval = showPval, risk.table = showRiskTable, xlab = xlabel, ylab = ylabel"
    if( otherOptions == ""){
      gcall <- paste(gcall,mainParams, ")")
    } else {        
      gcall <- paste(gcall, mainParams, ",",otherOptions, ")")
    }
    g <- eval(parse(text = gcall))
    
    if(ggplotExtra != ""){
      g$plot <- g$plot + eval(parse(text = ggplotExtra))
    }
    
    write.table(g$data.survtable, file = get.output(cf, "RiskTable"), col.names=T, row.names=F, sep= "\t",quote=F)
    
    plotDirectory <- get.output(cf, "plots")
    dir.create(plotDirectory)
    setwd(plotDirectory)
    if(showRiskTable){
      gg <- plot_grid(g$plot, g$table, nrow = 2, rel_heights = c(3,1))    
      ggsave(gg, file = "survivalCurve.pdf")
    } else {
      ggsave(g$plot, file = "survivalCurve.pdf")
    }
    if(runCox){
      forest <- ggforest(cox.model, data = csv)
      ggsave(forest, file = "forestPlot.pdf")
      write.table(tidy(cox.model), file = get.output(cf, "CoxStats"), col.names=T, row.names=F, sep= "\t",quote=F)
      saveRDS(list(KM = g, forest = forest), file = get.output(cf, "plotObject"))
    } else {
      touch.output(cf, "CoxStats")
      saveRDS(list(KM = g), file = get.output(cf, "plotObject"))
    }    
    return(0)
}


main(execute)
