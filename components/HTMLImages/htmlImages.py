import sys,os
import anduril
import anduril.table
from anduril.args import *
import shutil
#import PythonMagick
import subprocess,csv

def choiceListGenerator(csvFile,chooseCols, imgCol):
    if csvFile==None:
        return ("","")

    reader=anduril.table.TableReader(csvFile,type='dict',column_types=str)
    chooseNames=[x.strip() for x in chooseCols.split(",")]
    DDMenus=""
    js='''<script language="javascript">
function chooseImage() {
  var imgFile="";
  var imgMatches=new Array();
'''
    for i,name in enumerate(chooseNames):
        DDMenus+='\n<span class="nameTitle">%s:</span><br><select id="%s" onchange="chooseImage()" onclick="chooseImage()">\n'%(name,name)
        DDMenus+='\n'.join(
                  ['  <option value="%s">%s</option>'%(x,x) for x in set(reader.get_column(name))]
                )
        DDMenus+='\n</select><br>\n'
        js+='  var node%i=document.getElementById("%s");\n'%(i,name)
        js+='  var selection%i=node%i.options[node%i.selectedIndex].value;\n'%(i,i,i)
    initialImage=None;
    for row in reader:
        if initialImage==None: initialImage=row[imgCol]
        js+='\n  if ('
        js+=' && '.join(
              ['( selection%i=="%s")'%(i,row[x]) for i,x in enumerate(chooseNames)]
            )
        js+=') { imgMatches.push("%s"); }'%(row[imgCol])
    js+='''
  
  if (imgMatches.length>1) {
    document.getElementById("imageSelector").innerHTML="";
    imgFile=imgMatches[0];
    var selector=document.createElement("select");
    selector.setAttribute("id","imageDropdown"); 
    selector.setAttribute("onchange","chooseFromImageList()");
    for (var m=0; m<imgMatches.length; m++){
        var opt=document.createElement("option");
        opt.setAttribute("value",m);
        opt.innerHTML=imgMatches[m];
        selector.appendChild(opt);
    }
    document.getElementById("imageSelector").appendChild(selector);
  
  } else if (imgMatches.length==1) {
    document.getElementById("imageSelector").innerHTML="";
    imgFile=imgMatches[0];
  } else {
    document.getElementById("imageSelector").innerHTML="Combination not found.";
    imgFile="";
  }
  setImage(imgFile);
}
function chooseFromImageList(){
    var selectionImage=document.getElementById("imageDropdown").options[document.getElementById("imageDropdown").selectedIndex].innerHTML;
    setImage(selectionImage);
}
function setImage(s){
  var imgNode=document.getElementById("selectedImage");
  var linkNode=document.getElementById("selectedLink");
  imgNode.title=s;
  if (s.toLowerCase().endsWith(".pdf")) {
    imgNode.src=s+".png";
    linkNode.setAttribute("href",s);
  } else {
    imgNode.src=s;
    linkNode.setAttribute("href",s);
  }
}
document.getElementById("imageSelector").innerHTML="Make a selection";
</script>'''
    return (js,DDMenus)

inImages = input_in
outImages = output_out
os.mkdir(outImages)

(js,DDMenus)=choiceListGenerator(input_menuChoices, param_choiceCols, param_imageCol)

if input_css!=None:
    style=open(input_css, 'r').read()
else:
    style=open('default.css', 'r').read()
index = open(outImages+'/index.html', 'w')
index.write('''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>%s</title>
<style>
  body { text-align:center; }
  %s
</style>
</head>
'''%(param_title,style))

pageWidth = int(param_pageWidth)
width = float(100)/int(param_columns)

index.write('''<body>
<!-- HTMLImages snippet begins -->
<div style="width: %s%%; display: inline-block;">
<h1>%s</h1>
'''%(pageWidth,param_head))

def noChoices():

    pdfList = ""
    for imageFile in sorted(os.listdir(inImages)):
        if os.path.isfile(os.path.join(inImages, imageFile)):
            base, extension = os.path.splitext(imageFile)
            writeImage = False
            if(getImages==True):
                fileName=imageFile
                if(extension == '.pdf' and convertPdf == True) :
                    fileName = base + ".jpg"
                    convargs=['convert','-density','300x300',os.path.join(inImages, imageFile)+'[0]','-background','white','-flatten','-quality','97',os.path.join(outImages, fileName)]
                    convp=subprocess.call(convargs)
                elif(extension <> '.pdf'):   
                    shutil.copy(os.path.join(inImages, imageFile), os.path.join(outImages, imageFile))
                index.write('<a href="%s"><img src="%s" width="%s%%"/></a>'%(fileName,fileName,str(width)))
            if(extension == '.pdf' and getPdf):
                shutil.copy(os.path.join(inImages, imageFile), os.path.join(outImages, imageFile))
                pdfList = pdfList + '<p><a href=\"'+imageFile+'\" >'+imageFile+'</a></p>'    
            #except IOError:
            #    continue

    index.write('''</div>
%s
<!-- HTMLImages snippet ends -->
</body></html>'''%(pdfList,))
    
def withChoices():

    
    index.write('''
<div id="menuArea">%s</div>
<div id="imageArea">
    <div id="imageSelector"></div>
    <a id="selectedLink" target="_blank"><img id="selectedImage"/></a>
</div>
    %s
    '''%(DDMenus,js))
    
    for imageFile in sorted(os.listdir(inImages)):
        if os.path.isfile(os.path.join(inImages, imageFile)):
            base, extension = os.path.splitext(imageFile)
            writeImage = False
            if(getImages==True):
                fileName=imageFile
                if(extension == '.pdf' and convertPdf == True):
                    fileName = imageFile + ".png"
                    convargs=['convert','-density','300x300',os.path.join(inImages, imageFile)+'[0]','-background','white','-flatten',os.path.join(outImages, fileName)]
                    convp=subprocess.call(convargs)
                elif(extension <> '.pdf'):   
                    shutil.copy(os.path.join(inImages, imageFile), os.path.join(outImages, imageFile))
            if(extension == '.pdf' and getPdf):
                shutil.copy(os.path.join(inImages, imageFile), os.path.join(outImages, imageFile))

    index.write('''</div>
<!-- HTMLImages snippet ends -->
</body></html>''')

if input_menuChoices==None:
    noChoices()
else:
    withChoices()
