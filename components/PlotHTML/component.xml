<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>PlotHTML</name>
    <version>0.1</version>
    <doc>
    Creates scatter, line or bar plots where Y and X coordinates come from JSON files.
    Coordinates for the Y axis must always be provided; values for the X axis
    are optional. If X coordinates are missing, integers 1 to N are used.
    
    Each column may be plot to the same image or to different images. Creates also a pairs plot, which is
    each Y column ploted against all other Y columns. The X columns are discarded when using the pairs plot.
    
    Plotting configuration help: http://humblesoftware.com/flotr2/documentation 
    
    Use the provided templates to modify the output.
    </doc>
    <author email="ville.rantanen@helsinki.fi">Ville Rantanen</author>
    <category>HTML</category>
    <category>Plot</category>
    <launcher type="python">
        <argument name="file" value="PlotHTML.py" />
        <argument name="source" value="foot_bar.htmls" />
        <argument name="source" value="foot_line.htmls" />
        <argument name="source" value="foot_scatter.htmls" />
        <argument name="source" value="foot_matrixdiagrams.htmls" />
    </launcher>
    <requires type="manual">Python</requires>
    <inputs>
        <input name="data" type="CSV">
            <doc>Data for plotting</doc>
        </input>
        <input name="template" type="TextFile" optional="true">
            <doc>Template for data handling</doc>
        </input>
    </inputs>
    <outputs>
        <output name="plot" type="HTMLFile">
            <doc>The plot.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="width" type="string" default="800px">
            <doc>Width of each plot in px.</doc>
        </parameter>
        <parameter name="height" type="string" default="600px">
            <doc>Height of each plot in px.</doc>
        </parameter>
        <parameter name="title" type="string" default="Plot">
            <doc>Image title. The caption may contain one
            or two %s placeholders, which are replaced with the current column
            names in "y" and "x" (if defined). Column names are not used when multiple plots are created
            to the same image (imageType = 'single').</doc>
        </parameter>
        <parameter name="yLabel" type="string" default="y">
            <doc>X axis label. The label may contain one placeholder,
            which is replaced with the current column name in "y".</doc>
        </parameter>
        <parameter name="xLabel" type="string" default="x">
            <doc>X axis label. The label may contain one placeholder,
            which is replaced with the current column name in "x",
            or if not defined, in "y".</doc>
        </parameter>
        <parameter name="yLow" type="string" default="null">
            <doc>Low bound of the Y axis. </doc>
        </parameter>
        <parameter name="yHigh" type="string" default="null">
            <doc>High bound of the Y axis. </doc>
        </parameter>
        <parameter name="xLow" type="string" default="null">
            <doc>Low bound of the X axis. </doc>
        </parameter>
        <parameter name="xHigh" type="string" default="null">
            <doc>High bound of the X axis. </doc>
        </parameter>
        <parameter name="pInf" type="float" default="2e100">
            <doc>Substitution value for infinitely great values</doc>
        </parameter>
        <parameter name="nInf" type="float" default="-2e100">
            <doc>Substitution value for infinitely small values</doc>
        </parameter>
        <parameter name="plotType" type="string" default="p">
            <doc>Plot type p=points, l=lines, h=bars. m=matrixdiagram If input template is used, it overrides this selection.</doc>
        </parameter>
        <parameter name="pngImage" type="boolean" default="true">
       		<doc>Include download link for PNG image</doc>
        </parameter>
        <parameter name="legendPosition" type="string" default="off">
       		<doc>Legend position 'bottomleft' or 'topright' etc (Can not be used with the pairs plot)</doc>
        </parameter>
        <parameter name="drawX" type="boolean" default="true">
            <doc>If false, do not print X axis ticks and numbers.</doc>
        </parameter>
        <parameter name="colors" type="string" default="[&quot;red&quot;,&quot;blue&quot;,&quot;green&quot;]">
       		<doc>Custom function for colors. Example ["red", "blue", "green"].</doc>
        </parameter>
        
    </parameters>
</component>
